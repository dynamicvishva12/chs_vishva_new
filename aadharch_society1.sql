-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2020 at 10:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aadharch_society1`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounting`
--

CREATE TABLE `accounting` (
  `a_id` int(10) NOT NULL,
  `accouting_id` varchar(100) NOT NULL,
  `accounting_charges` int(10) NOT NULL,
  `s-date` date NOT NULL,
  `accounting_recipt` text DEFAULT NULL,
  `status` enum('active','deactive') NOT NULL DEFAULT 'active',
  `s-indent` varchar(10) DEFAULT NULL,
  `s-des` varchar(200) DEFAULT NULL,
  `member_id` varchar(60) DEFAULT NULL,
  `bank_name` varchar(60) DEFAULT NULL,
  `ifsc` varchar(60) DEFAULT NULL,
  `micr` varchar(60) DEFAULT NULL,
  `transaction_no` varchar(60) DEFAULT NULL,
  `vaucher_name` varchar(50) DEFAULT NULL,
  `vaucher_no` varchar(100) DEFAULT NULL,
  `cheque_no` int(22) DEFAULT NULL,
  `cr_dr` varchar(10) DEFAULT NULL,
  `payment_mode` varchar(50) DEFAULT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accounting_groups`
--

CREATE TABLE `accounting_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `account_type` varchar(30) NOT NULL,
  `extra` varchar(100) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accounting_opening_balance`
--

CREATE TABLE `accounting_opening_balance` (
  `id` int(50) NOT NULL,
  `accounting_id` int(10) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `opening_balance` varchar(25) NOT NULL DEFAULT '0',
  `account_nature` varchar(2) NOT NULL DEFAULT 'Dr',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accounting_types`
--

CREATE TABLE `accounting_types` (
  `id` int(50) NOT NULL,
  `accounting_id` int(10) NOT NULL,
  `accounting_type` varchar(100) NOT NULL DEFAULT 'income',
  `accounting_name` text NOT NULL,
  `Account_group` varchar(50) DEFAULT NULL,
  `opening_balance` varchar(25) NOT NULL DEFAULT '0',
  `type_ledger` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `a-id` int(11) NOT NULL,
  `a-username` varchar(100) NOT NULL,
  `a-albumname` varchar(100) NOT NULL,
  `al-description` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`a-id`, `a-username`, `a-albumname`, `al-description`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(10, 'Gokul-A-3-10', 'society da album', 'djsddjsidjidsdisj', '', '', '2019-01-23 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Gokul-A-3-10', 'Wedding', 'Pics', '', '', '2018-06-14 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Gokul-A-3-10', 'Party', 'Hello', '', '', '2018-06-15 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Gokul-A-3-10', 'Test', 'Djye', '', '', '2018-06-15 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Gokul-A-3-10', 'Independence day', 'Society celebration ', '', '', '2018-08-14 00:00:00', '', '0000-00-00 00:00:00'),
(71, 'Gokul-A-3-10', 'Diwali', 'Cvb', '', '', '2018-08-20 00:00:00', '', '0000-00-00 00:00:00'),
(72, 'Gokul-A-3-10', 'Ganesh festival', 'Vahah', '', '', '2018-08-20 00:00:00', '', '0000-00-00 00:00:00'),
(74, 'Gokul-A-0-1', 'Ccvvv', 'Fvhh', '', '', '2018-08-20 00:00:00', '', '0000-00-00 00:00:00'),
(77, 'Gokul-A-0-1', 'Picnic', 'Picnic', '', '', '2018-09-05 00:00:00', '', '0000-00-00 00:00:00'),
(81, 'Gokul-A-0-1', 'Sureshjani album', 'My album', '', '', '2019-01-22 00:00:00', '', '0000-00-00 00:00:00'),
(82, 'Gokul-A-3-10', 'Testing', 'Ttt', '', '', '2019-02-07 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `album_photos`
--

CREATE TABLE `album_photos` (
  `id` int(11) NOT NULL,
  `albumId` varchar(55) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `d_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `doc_name` text NOT NULL,
  `audit` text NOT NULL,
  `userId` varchar(55) NOT NULL,
  `designation` varchar(55) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `booking_name` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `booking_name`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Marriage Hall', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Garden', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Open Space', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `booking_slot`
--

CREATE TABLE `booking_slot` (
  `b_id` int(12) NOT NULL,
  `booking_id` int(12) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `details` text NOT NULL,
  `approval` varchar(25) NOT NULL DEFAULT 'Not Approved',
  `reject_reason` varchar(250) NOT NULL,
  `cancel_reqest` varchar(250) NOT NULL,
  `approvedBy` varchar(55) NOT NULL,
  `cancelApprovedBy` varchar(55) NOT NULL,
  `booking_dscr` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_slot`
--

INSERT INTO `booking_slot` (`b_id`, `booking_id`, `user_name`, `from_date`, `to_date`, `from_time`, `to_time`, `details`, `approval`, `reject_reason`, `cancel_reqest`, `approvedBy`, `cancelApprovedBy`, `booking_dscr`, `date_creation`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 2, 'Gokul-A-3-10', '2019-05-22', '2019-05-24', '06:00:00', '07:00:00', '22-05-2018+24-05-2018 6 AM+7 AM', 'Cancel', '', 'dsaas', '', '', 'birthday party', '2019-02-08 03:44:48', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, 1, 'Gokul-A-0-1', '2019-02-15', '2019-02-20', '17:00:00', '22:00:00', '2019-02-15+2019-02-20 17PM+22PM', 'Cancel', '', 'Some reason', '', '', 'jcjjcjf', '2019-02-13 01:55:50', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, 2, 'Gokul-A-0-1', '2019-02-28', '2019-02-28', '18:00:00', '23:00:00', '2019-02-28+2019-02-28 18PM+23PM', 'Approved', '', '', '', '', 'new booking', '2019-02-13 02:13:10', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, 2, 'Gokul-A-0-1', '2019-02-12', '2019-02-16', '05:00:00', '10:00:00', '2019-02-12+2019-02-16 AM+AM', 'Cancel', '', 'I have a few', '', '', 'hdhdh', '2019-02-08 05:54:52', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 1, 'Gokul-A-3-10', '2019-03-13', '2019-03-14', '17:00:00', '18:00:00', '2018-12-10+2018-12-11 17PM+17PM', 'Not Approved', 'I Will always Love you when', '', '', '', 'marriage', '2019-02-08 03:43:06', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 2, 'Gokul-A-0-1', '2019-02-27', '2019-02-28', '17:00:00', '20:00:00', '2019-01-31+2019-01-31 17PM+20PM', 'Rejected', '', '', '', '', 'my booking', '2020-05-21 00:00:00', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:16:41'),
(27, 1, 'Gokul-A-0-1', '2019-02-28', '2019-02-28', '15:00:00', '18:00:00', '2019-02-28+2019-02-28 15PM+19PM', 'Not Approved', '', '', '', '', 'my sister wedding', '2019-02-13 01:51:20', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 2, 'Gokul-A-3-10', '2019-06-16', '2019-06-16', '04:00:00', '10:00:00', '2019-06-16+2019-06-16 AM+AM', 'Not Approved', '', '', '', '', 'Birthday Party', '2019-06-15 12:12:20', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 1, 'Gokul-A-3-10', '2019-07-20', '2019-07-20', '15:00:00', '15:00:00', '2019-07-20+2019-07-20 15PM+15PM', 'Not Approved', '', '', '', '', 'test', '2019-07-19 04:41:16', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, 2, 'Gokul-A-3-10', '2019-07-21', '2019-07-21', '12:00:00', '12:00:00', '2019-07-21+2019-07-21 12PM+12PM', 'Not Approved', '', '', '', '', 'test', '2020-05-21 00:00:00', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:11:25'),
(38, 1, 'Gokul-A-3-10', '2019-07-23', '2019-07-23', '11:00:00', '11:00:00', '2019-07-23+2019-07-23 AM+AM', 'Not Approved', '', '', '', '', 'party', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(41, 1, 'Gokul-A-0-1', '2019-07-30', '2019-07-30', '12:00:00', '12:00:00', '2019-07-30+2019-07-30 12PM+12PM', 'Approved', '', '', '', '', 'wedding', '2019-07-29 12:24:35', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(42, 2, 'Gokul-A-3-10', '2019-07-30', '2019-07-30', '14:00:00', '16:00:00', '2019-07-30+2019-07-30 14PM+16PM', 'Pending Cancel', '', 'Cancel my birthday party for some reason.', '', '', 'Birthday Party', '2019-07-29 12:23:16', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(43, 1, 'Gokul-A-3-10', '2020-06-30', '2020-07-31', '12:00:00', '12:00:00', '2020-06-30+2020-07-31 12PM+12PM', 'Approved', '', '', '', '', 'test', '2020-05-07 00:00:00', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:58:40'),
(45, 1, 'Gokul-A-3-10', '2019-12-31', '2020-01-01', '14:00:00', '14:00:00', '2019-12-31+2020-01-01 14PM+14PM', 'Cancel', '', 'Test', '', '', 'testing data for cases', '2019-12-30 02:38:18', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `b-id` int(11) NOT NULL,
  `buildingname` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `complain_chat`
--

CREATE TABLE `complain_chat` (
  `user-id` int(11) NOT NULL,
  `user-keyvalue` varchar(11) NOT NULL,
  `user-message` varchar(1000) NOT NULL,
  `s-date` varchar(25) NOT NULL,
  `s-admin` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `sr_no` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `email` varchar(65) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demo_req`
--

CREATE TABLE `demo_req` (
  `sr_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `soc_name` varchar(125) NOT NULL,
  `email` varchar(150) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `designation_parameter`
--

CREATE TABLE `designation_parameter` (
  `id` int(11) NOT NULL,
  `short_name` varchar(50) NOT NULL,
  `descrption` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directory`
--

CREATE TABLE `directory` (
  `d-id` int(11) NOT NULL,
  `d-topic` varchar(255) NOT NULL,
  `d-body` varchar(255) NOT NULL,
  `created-date` datetime NOT NULL,
  `mobile` varchar(13) NOT NULL,
  `important` varchar(30) NOT NULL,
  `directory_type` varchar(255) NOT NULL DEFAULT 'type1',
  `addimg` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directory`
--

INSERT INTO `directory` (`d-id`, `d-topic`, `d-body`, `created-date`, `mobile`, `important`, `directory_type`, `addimg`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Rahul Waghmare ', 'chairman', '2020-05-08 10:36:12', '8433649256', 'saurabh@gmail.com', 'type1', 'hyundai-elite-i20.jpg', 'Active', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:12'),
(2, 'ravi more', 'corporater', '0000-00-00 00:00:00', '8286919525', 'ravi@gmail.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'Mahesh Gosavi', 'D D Registar', '0000-00-00 00:00:00', '9874526535', 'maheshgosavi78@gmail.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Surendra Deshmukh', 'BMMC-Water Department', '0000-00-00 00:00:00', '9702568545', 'waterdept@bmmc.in', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, 'sreeraj nair', 'secretory', '2020-05-08 14:47:14', '9545687512', 'sree_nair@gmail.com', 'type1', '', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:14'),
(34, 'rahul shinde', 'water department', '0000-00-00 00:00:00', '7845965875', 'rahu@bmc.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 'jayvant patil', 'electrical department', '0000-00-00 00:00:00', '7845965855', 'jayu@bmc.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 'kalpana joshi', 'IS ofiicer', '0000-00-00 00:00:00', '7845968485', 'kalpana@isi.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, 'roma dsoza', 'co-secretory', '0000-00-00 00:00:00', '9512674622', 'dynamicvishvateam@gmail.com', 'type1', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(49, 'Rahul', 'Dv admin', '2019-01-24 00:00:00', '8421345046', 'rahul@gmail.com', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(50, 'Tiger', 'God father', '2019-03-01 12:02:42', '9797979797', 'god@ghm.com', 'type2', '', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(57, 'Rahul Waghmare ', 'chairman', '2020-04-28 19:13:59', '8433649256', 'saurabh@gmail.com', 'type1', 'hyundai-elite-i20.jpg', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:13:59'),
(74, 'suresh Jani', 'treasurer', '2019-02-14 03:19:31', '9920478558', 'suresh_jani@gmail.com', 'type1', 'hyundai-elite-i20.jpg', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(75, 'Mansoon', 'Bd', '2020-04-29 18:22:33', '8546312848', 'gsg@gsh.com', 'type2', '', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:22:33'),
(76, 'Shivam Roy', 'Joint District Head', '2019-07-15 11:25:28', '8655358218', 'shivam@gmail.com', 'type2', '', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(77, 'SOURABH', 'ba', '2019-07-15 00:00:00', '8898325696', 'sOURABH@GMAIL.COM', 'type2', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(78, 'rohan chalke', 'committee members', '2020-04-28 19:49:54', '9820045612', 'chalke.r@gmail.com', 'type1', '', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:54'),
(79, 'dynamic Vishva', 'committee members', '2020-04-05 05:57:53', '8104241208', 'dynamicvishva@gmail.com', 'type1', '', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(80, 'Sachin Tendulkar', 'commitee member', '0000-00-00 00:00:00', '8879504125', 'dynamicvishvateam1@gmail.com', 'type1', '', 'Active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(89, 'dynamic Vishva', 'chairman', '2020-04-28 18:36:39', '8104241208', 'dynamicvishva@gmail.com', 'type1', '', 'Active', 'Gokul-A-3-10', '2020-04-28 18:36:39', '', '0000-00-00 00:00:00'),
(101, 'Purushottam Thite', 'committee members', '2020-04-28 19:49:58', '8879299537', 'purushottamthite2690@gmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-04-28 19:06:57', 'Gokul-A-3-10', '2020-04-28 19:49:58'),
(102, 'Rajendra Khot', 'committee members', '2020-04-28 19:50:15', '9725648955', 'rajendra@rediffmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-04-28 19:11:56', 'Gokul-A-3-10', '2020-04-28 19:50:15'),
(103, 'sailesh dhaman', 'chairman', '2020-05-08 14:47:19', '987045123', 'roshanbajag@gmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-04-28 19:13:59', 'Gokul-A-3-10', '2020-05-08 14:47:19'),
(104, 'Test', 'wetrwer', '2020-04-29 18:18:21', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:18:21', '', '0000-00-00 00:00:00'),
(107, 'Test', 'wetrwer', '2020-04-29 18:21:28', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:21:28', '', '0000-00-00 00:00:00'),
(108, 'Test', 'wetrwer', '2020-04-29 18:21:37', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:21:37', '', '0000-00-00 00:00:00'),
(109, 'Test', 'wetrwer', '2020-04-29 18:23:32', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:23:32', '', '0000-00-00 00:00:00'),
(110, 'Test', 'wetrwer', '2020-04-29 18:23:47', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:23:47', '', '0000-00-00 00:00:00'),
(111, 'Test', 'wetrwer', '2020-04-29 18:24:07', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:24:07', '', '0000-00-00 00:00:00'),
(112, 'Test', 'wetrwer', '2020-04-29 18:24:18', '9887867656', 'example@gmail.com', 'type2', '', '', 'Gokul-A-3-10', '2020-04-29 18:24:18', '', '0000-00-00 00:00:00'),
(113, 'rohan chalke', 'chairman', '2020-05-08 14:47:08', '9820045612', 'chalke.r@gmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-05-08 10:17:30', 'Gokul-A-3-10', '2020-05-08 14:47:08'),
(114, 'Purushottam Thite', 'chairman', '2020-05-08 14:47:03', '8879299537', 'purushottamthite2690@gmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-05-08 10:22:29', 'Gokul-A-3-10', '2020-05-08 14:47:03'),
(115, 'Rajendra Khot', 'committee members', '2020-05-08 10:23:51', '9725648955', 'rajendra@rediffmail.com', 'type1', '', 'Inactive', 'Gokul-A-3-10', '2020-05-08 10:23:44', 'Gokul-A-3-10', '2020-05-08 10:23:51'),
(116, 'Jhethalal Gada', 'chairman', '2020-05-08 10:24:05', '9820341452', 'jhethag@ygmail.com', 'type1', '', 'Active', 'Gokul-A-3-10', '2020-05-08 10:24:05', '', '0000-00-00 00:00:00'),
(117, 'Rajendra Khot', 'chairman', '2020-05-08 10:36:12', '9725648955', 'rajendra@rediffmail.com', 'type1', '', 'Active', 'Gokul-A-3-10', '2020-05-08 10:36:12', '', '0000-00-00 00:00:00'),
(118, 'Sunil Kadam', 'committee members', '2020-05-08 10:46:07', '9920085632', 'sunil@yahoo.com', 'type1', '', 'Active', 'Gokul-A-3-10', '2020-05-08 10:46:07', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `d_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `doc_name` text NOT NULL,
  `document` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doc_type`
--

CREATE TABLE `doc_type` (
  `dc-id` int(11) NOT NULL,
  `dc_typename` varchar(20) NOT NULL,
  `dc_description` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_account`
--

CREATE TABLE `email_account` (
  `sr_no` int(11) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_account`
--

INSERT INTO `email_account` (`sr_no`, `email_id`, `password`, `host`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'noreply@chsvishva.com', 'Cv123456', 'www.dynamicvishva.in', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `family`
--

CREATE TABLE `family` (
  `f_id` int(9) NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `name` varchar(150) NOT NULL,
  `relation` varchar(150) DEFAULT NULL,
  `blood_type` varchar(60) NOT NULL,
  `dob` varchar(60) DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `donorCard` varchar(10) DEFAULT 'No',
  `contact` varchar(10) NOT NULL,
  `wish_to_donate` varchar(5) NOT NULL DEFAULT 'YES',
  `occc` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `family`
--

INSERT INTO `family` (`f_id`, `user_name`, `name`, `relation`, `blood_type`, `dob`, `gender`, `donorCard`, `contact`, `wish_to_donate`, `occc`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Gokul-A-0-1', 'Reena Jani', 'Mother', 'A+', '01-06-1960', 'Female', 'No', '9768541230', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Gokul-A-0-1', 'Rupali Jani', 'Daughter', 'A+', '05-08-1965', 'Female', 'No', '9920456518', '', '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Gokul-A-0-1', 'Raj', 'Son', 'A+', '18-10-1970', 'Male', 'No', '9821475263', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Gokul-A-0-2', 'Damini Todi', 'Mother', 'AB+', '24-08-1960', 'female', 'No', '9920456253', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Gokul-A-0-2', 'Sneha Todi', 'Sister', 'C+', '08-06-1989', 'female', 'No', '9821753689', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Gokul-A-0-2', 'Sachin Todi', 'Son', 'AB+', '23-08-1985', 'male', 'No', '9768751232', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Gokul-A-0-3', 'Rajashree Nair', 'Wife', 'B+', '05-02-1970', 'female', 'No', '9652145366', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Gokul-A-0-3', 'Priya Nair', 'Daughter', 'AB+', '16-06-1975', 'female', 'No', '9852637810', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'Gokul-A-1-4', 'Robin Dsouza', 'Husband', 'A+', '31-02-1982', 'male', 'No', '9821536984', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Gokul-A-1-4', 'Ravin Dsouza', 'Son', 'O-', '26-06-1989', 'male', 'No', '9924567810', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Gokul-A-1-5', 'Mayuri Ghai', 'Wife', 'O+', '24-08-1990', 'female', 'no', '9820657412', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'Gokul-A-1-5', 'Manish Ghai', 'Son', 'O-', '03-08-2000', 'male', 'no', '', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Gokul-A-1-5', 'Meena Ghai', 'Daughter', 'A+', '08-11-1995', 'female', 'no', '', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Gokul-A-1-5', 'Rajesh Ghai', 'Brother', 'AB+', '12-12-1992', 'female', 'no', '9062746222', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Gokul-A-1-6', 'Neeta Kamble', 'Wife', 'AB+', '31-05-1990', 'female', 'no', '9768415632', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'Gokul-A-1-6', 'Divya kamble', 'Mother', 'O-', '17-06-1958', 'female', 'yes', '9920457862', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Gokul-A-1-6', 'Ravi Kamble', 'Father', 'B+', '05-20-1955', 'male', 'yes', '9820348562', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Gokul-A-2-7', 'Radhika Rohan Chalke', 'Wife', 'O+', '08-02-1972', 'female', 'no', '8169541452', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Gokul-A-2-8', 'Sanjana Sameer Shedge', 'Wife', 'AB+', '1995/09/07', 'Female', 'No', '8000000000', 'No', 'Student', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Gokul-A-2-8', 'Suyash Sameer Shedge', 'Son', 'O-', '01-01-1980', 'male', 'no', '9820021418', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, 'Gokul-A-2-9', 'Dhanshree Damani', 'Wife', 'A+', '16-02-1979', 'female', 'no', '9875216555', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, 'Gokul-A-2-9', 'Bhagyashree Damani', 'Dughter', 'AB+', '16-08-1999', 'female', 'no', '9920454662', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, 'Gokul-A-2-9', 'Rohit Damani', 'Son', 'O-', '12-12-2006', 'male', 'no', '9820378562', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'Gokul-B-0-1', 'Anjali Sachin Tendulkar', 'Wife', 'A+', '01-01-1981', 'female', 'no', '9776254783', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'Gokul-B-0-1', 'Arjun S Tendulkar', 'Son', 'AB+', '01-01-1996', 'female', 'no', '9920457862', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Gokul-B-0-1', 'Sara Sachin Tendulkar', 'Daughter', 'O-', '05-01-1993', 'male', 'no', '9820348562', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Gokul-B-0-2', 'Daya Jethalal Gada', 'Wife', 'O+', '05-11-1914', 'female', 'no', '8898303373', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'Gokul-B-0-2', 'Dipendra Jethalal Gada', 'Son', 'AB+', '11-03-1998', 'male', 'no', '8584657485', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, 'Gokul-B-0-2', 'Champaklal Gada', 'Father', 'A+', '02-02-1969', 'male', 'no', '9594684255', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'Gokul-B-0-3', 'Madhvi Aatmaraam Bhide', 'Wife', 'A-', '15-08-1963', 'female', 'no', '9856247555', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'Gokul-B-0-3', 'Sonali Aatmaraam Bhide', 'Daughter', 'AB+', '04-03-1984', 'female', 'no', '7442689222', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, 'Gokul-B-1-4', 'Seema Khot', 'Wife', 'A+', '25-03-1978', 'female', 'no', '9875621553', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, 'Gokul-B-1-4', 'Kamakshi Khot', 'Daughter', 'AB+', '11-04-2005', 'female', 'no', '', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, 'Gokul-B-1-4', 'Anil Khot', 'Son', 'O+', '03-08-2010', 'male', 'no', '', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 'Gokul-B-1-5', 'Sunita Sunil Kadam', 'Wife', 'O-', '07-07-1968', 'female', 'no', '9862574963', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 'Gokul-B-1-5', 'Vikrant Sunil Kadam', 'Son', 'A+', '02-02-1985', 'male', 'no', '8745965211', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, 'Gokul-B-1-5', 'Vedant Sunil Kadam', 'Son', 'O+', '16-08-1990', 'male', 'no', '7598652322', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, 'Gokul-B-1-6', 'Meenakshi Divesh Gadge', 'Wife', 'O-', '14-09-1979', 'female', 'no', '7025689541', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(39, 'Gokul-B-1-6', 'Mansi Divesh Gadge', 'Daughter', 'A+', '06-06-1998', 'female', 'no', '920166844', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(40, 'Gokul-B-1-6', 'Vinay Divesh Gadge', 'Son', 'O+', '19-09-2004', 'male', 'no', '9925684122', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(41, 'Gokul-B-2-7', 'Priya Aniket Jagtap', 'Wife', 'AB+', '22-09-1990', 'female', 'no', '9920147569', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(42, 'Gokul-B-2-7', 'Dipesh Sunil Jagtap', 'Brother', 'A+', '01-06-1991', 'male', 'no', '9756321855', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(43, 'Gokul-B-2-8', 'Ketki Sangram Desai', 'Wife', 'O+', '11-08-1992', 'female', 'no', '9723658444', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(44, 'Gokul-B-2-8', 'Pooja Vilas Desai', 'Mother', 'AB+', '07-02-1960', 'female', 'no', '8025498353', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(45, 'rent-Gokul-A-3-10', 'puru', 'self', 'A+', '2000-06-08', 'Male', NULL, '8655352098', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(46, 'rent-Gokul-A-3-10', 'rakesh', 'self', 'A-', '1997-01-16', 'Female', NULL, '8655352098', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(47, 'rent-Gokul-A-3-10', 'shruti', 'self', 'A+', '1999-02-10', 'Male', NULL, '8457895874', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(49, 'rent-Gokul-A-3-10', 'ramesh', 'Father', 'A+', '1974-02-07', 'Male', '', '8655352098', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(56, 'Gokul-A-3-10', 'Ronak', 'Brother', 'B-', '1998-05-26', 'Male', 'No', '8655352098', 'Yes', 'Social Worker', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(57, 'rent-Gokul-A-3-10', 'krishna', 'Grand Mother', 'AB+', '2018-03-21', 'Male', '', '8286919525', 'no', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(58, 'Grand_Square_Complex-A-6-601', 'manjit thind', 'self', 'A+', '1995-05-26', 'Male', NULL, '8652548798', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(59, 'rent-Gokul-A-0-1', 'roshan', 'self', 'A+', '1995-05-25', 'Male', NULL, '8655352098', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(60, 'rent-Gokul-A-0-1', 'roshan', 'self', 'A+', '1995-05-25', 'Male', NULL, '8655352098', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(61, 'Gokul-A-3-10', 'sarvesh', 'Father', 'A-', '1975-05-14', 'Male', 'NO', '8655352098', 'YES', 'business', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(62, 'Gokul-A-3-10', 'shwhw', 'Wife', 'A-', '2018/06/08', 'Male', 'No', '6464646464', 'No', 'Service', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(63, 'Gokul-A-3-10', 'gdvvshs', 'Sister', 'B+', '2018/06/08', 'Male', 'Yes', '6866868998', 'No', 'Social Servant', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(64, 'Gokul-A-3-10', 'Raju rastogi', 'Sister', 'A+', '2018/06/08', 'Male', 'Yes', '8958565426', 'Yes', 'Social Worker', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(65, 'Gokul-A-3-10', 'sysudu', 'Uncle', 'A+', '2018/06/08', 'Male', 'Yes', '6754646865', 'Yes', 'Service', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(66, 'Gokul-A-3-10', 'saurabh', 'Wife', 'A-', '1990/06/08', 'Male', 'No', '6659987840', 'Yes', 'Service', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(67, 'Gokul-A-3-10', 'Sourabh Jadhav', 'Brother', 'A+', '2018/06/01', 'Male', 'Yes', '8898325696', 'No', 'Business', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(68, 'Gokul-A-3-10', 'Sandesh ', 'Uncle', 'A-', '1991/08/07', 'Male', 'No', '7977249582', 'No', 'Student', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(69, 'Gokul-A-3-10', 'gsha', 'Father', 'A+', '2018/06/11', 'Male', 'Yes', '6164646464', 'No', 'Student', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(70, 'Gokul-A-3-10', 'vwhshaj', 'Brother', 'AB-', '2018/06/12', 'Male', 'Yes', '8978787878', 'Yes', 'Service', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(71, 'Gokul-A-3-10', 'sarvesh gurav', 'Son', 'A+', '2018/08/09', 'Female', 'No', '9797923980', 'Yes', 'Business', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(72, 'Gokul-A-0-1', 'Suresh jani', 'Self', 'O+', '23-08-2000', 'Male', 'No', '8542365412', 'YES', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(73, 'Gokul-A-1-5', 'ujjwala baban jadhav', 'Mother', 'A+', '1970/09/01', 'Female', 'Yes', '8898325696', 'Yes', 'Housewife', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(74, 'Gokul-A-3-10', 'Rahul', 'Son', '', '1993/03/07', 'Male', 'Yes', '', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(75, 'Gokul-A-3-10', 'Tesy', 'Son', 'B-', '2019/03/18', 'Male', 'No', '8655248545', 'No', 'Service', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(76, 'Gokul-A-3-10', 'Sourabh Baban Jadhav', 'Brother', 'A+', '1994/04/20', 'Male', 'No', '8898325696', 'Yes', 'Business', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(77, 'Gokul-A-3-10', 'Test', 'Brother', '', '2019/07/01', 'Male', '', '', '', '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(78, 'Gokul-A-3-10', 'Test', 'Brother', '', '2019/07/01', 'Male', '', '', '', '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(79, 'Gokul-A-0-1', 'Raaj Jani', 'Son', '', '2019/07/25', 'Male', '', '', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(80, 'Gokul-A-0-1', 'Test2', 'Brother', '', '2019/07/31', 'Male', '', '', '', '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(81, 'Gokul-A-0-1', 'Test', 'Brother', '', '2019/08/01', 'Male', '', '', '', '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(82, 'Gokul-A-0-1', 'Priya', 'Sister', '', '2019/08/01', 'Female', '', '', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `f-id` int(11) NOT NULL,
  `f-album-name` varchar(20) NOT NULL,
  `f-uploads` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`f-id`, `f-album-name`, `f-uploads`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(23, 'Swami Aarati', 'Desert.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'mahashivratri', 'Penguins.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'sai baba ki jai', 'Tulips.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Swami Aarati', 'Penguins.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Swami Aarati', 'Tulips.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(70, 'society da album', '1529041065224.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(73, 'society da album', '1529041065494.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(75, 'society da album', '1529041065584.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(79, 'Vshsh', '1529042549225.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(82, 'Party', '1529334418143.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(87, 'Independence day', '1534231150499.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(94, 'Fff', '1534416276469.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(95, 'Fff', '1534416276220.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(96, 'Fff', '1534416276690.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(100, 'Ganesh festival', '1534767692705.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(101, 'Ccvvv', '1534769765519.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(113, 'Diwali', '1534833910616.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(142, 'Test', '1535093181523.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(155, 'Picnic', '1536152097190.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(160, 'Wedding', '1536210731607.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(163, 'Test', '1536211259107.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(164, 'Test', '1536211323138.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(167, 'Test', '1536211775502.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(171, 'Wedding', '1536214100048.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(172, 'Wedding', '1536214100178.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(173, 'Party', '1542822085309.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(175, 'Sureshjani album', '1548145815402.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(176, 'Wedding', '1549533143091.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(187, 'Party', '1549537933492.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(188, 'Party', '1549537935294.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(189, 'Party', '1549537938786.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(190, 'Party', '1549537941095.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(191, 'Wedding', '1549538054245.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(192, 'Wedding', '1549538054987.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(193, 'Wedding', '1549538055472.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(194, 'Wedding', '1549538054804.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(198, 'Party', '1549538550626.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(199, 'Party', '1549538552526.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(200, 'Party', '1549538551766.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(201, 'Party', '1549538690642.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(202, 'Party', '1549538689784.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(203, 'Party', '1549538690372.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(204, 'Ganesh festival', '1549538795687.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(205, 'Picnic', '1549538906127.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(206, 'Picnic', '1549538925875.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(207, 'Picnic', '1549538925123.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `fms_admin_tocken`
--

CREATE TABLE `fms_admin_tocken` (
  `srid` int(11) NOT NULL,
  `tocken` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fms_tocken`
--

CREATE TABLE `fms_tocken` (
  `srid` int(11) NOT NULL,
  `tocken` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fms_tocken`
--

INSERT INTO `fms_tocken` (`srid`, `tocken`, `username`) VALUES
(1, 'cpXjbKZAEgA:APA91bFm6myTT5F2QtWHWh436FvjPytbEV3xymj090ntFDUrx0qMI1XWgY52sSycycCCfy2E1BhI0noSoSta1q4xv4Qoz_cIaoy4NOYtmRviC01C_-cFROFrFHFMQT0zzbSeMvJhnGPJ', 'Gokul-A-1-5'),
(2, 'eSJMk6SJpSU:APA91bGPpXtz8-9C2jm6hME3VRyYSf5GHnzK7RjL9G1AwG1tMuqJiICjo7Bmt04aCoaWpYr2HNJfvPptbpH7S2Q7kWJZCK4jXNTWakry1D75nv4bdiHcEmVz2_OLWN4feCBq9nAdANwL', 'Gokul-A-1-5'),
(3, 'cpXjbKZAEgA:APA91bFm6myTT5F2QtWHWh436FvjPytbEV3xymj090ntFDUrx0qMI1XWgY52sSycycCCfy2E1BhI0noSoSta1q4xv4Qoz_cIaoy4NOYtmRviC01C_-cFROFrFHFMQT0zzbSeMvJhnGPJ', 'Gokul-A-3-10'),
(4, 'en21TuQH0Kc:APA91bGuULaU-PjzKN1OELh_SD_7S_xEivGb4LNJQCxqY-VAD-MDORnsQAXsGa0D0zUMPuN0ddpfFb8vCgjISKIDMC2wet5_ajFIZT1w2hR0J2gBNzvhvynyWpUrPnq9g9ilqDgC0LjW', 'Gokul-A-3-10'),
(5, 'eWBFKuvG5TA:APA91bE_Du9V4Asef9OZYPg6BB8O3dGrNo5k42GhH5f0H0CtVMqo4kKMk4qOO0X-zANlDzT2-E0jsxSJ0xlHBGqgy6juUwW-gSs_aVAfXZuIrE8EbHJz5EOr3YaD6hF4hPOHxHrjttMO', 'Gokul-A-3-10'),
(6, 'fapBW2zIaQg:APA91bFGYBBlrI-VkL6jNrd4PK8UbCCyP5pEeyd_FXsrqFJnP1mJX7_W9MUtlvPYOG0Q7eCIlucLvYnoFqc3_waJWPlll0MDGL6nUotm4Y2nGGj0xtOzyIuPri2T0kl1B8_IWecfDZeL', 'Gokul-A-3-10'),
(7, 'eNDjaUaanmY:APA91bG4kyEe3oHtSaQjLu1t2ilJQShUm1ASTDeaefck7AT0T06vHy_cCCWzBCGUq3ynjy6PAnAc9WM4dLQPhg19LrT0Kz9eLJs5wQom9iA6X2mqboecVjdKcGm-v4-xH6XrNE8bytyL', 'Gokul-A-3-10'),
(8, 'c00iwVKArbk:APA91bH1ImIEYDiaY1WOMjBJX9mirqPMCeN5X88kSc9C1Q447TARZrs8INlCzPZdRpAefxT6sjPhIx0Cws1AbR-jAoifL0fN2jd_qNT71857IWcRTQehmHp5qbHj_uxptPF_ReaDcAuV', 'Gokul-A-3-10'),
(9, 'cpXjbKZAEgA:APA91bFm6myTT5F2QtWHWh436FvjPytbEV3xymj090ntFDUrx0qMI1XWgY52sSycycCCfy2E1BhI0noSoSta1q4xv4Qoz_cIaoy4NOYtmRviC01C_-cFROFrFHFMQT0zzbSeMvJhnGPJ', 'Gokul-A-3-10'),
(10, 'c00iwVKArbk:APA91bH1ImIEYDiaY1WOMjBJX9mirqPMCeN5X88kSc9C1Q447TARZrs8INlCzPZdRpAefxT6sjPhIx0Cws1AbR-jAoifL0fN2jd_qNT71857IWcRTQehmHp5qbHj_uxptPF_ReaDcAuV', 'Gokul-A-3-10'),
(11, 'c_eaAvkWigE:APA91bFW00suCrSxgFJN-4RTPMCm-tgps6GoDvgodxKd7k8y2kYttUoTVxeeFDSuVr-PpSeM4E-8YljnOT95dQ3sbGAAQMnHB8_P64lu9_IVAyea31gM3FA4F87ycoyT-ChKGVQJnrNy', 'Gokul-A-3-10'),
(12, 'fwucNOdB-k4:APA91bENytOo864rh03YUGhUhaQADw_aZWzPPtyfRJMcSFJb1I35SO3_Uj3SjJ5LfLtfSwMrzi1Qh3wVn-GduVVz4BdfSfTyvT0TmpvfKiu01LFC7Up1Q0DoWT8-nR7O5Eg0C5YE43iA', 'Gokul-A-3-10'),
(13, 'eDgDnzFXFXA:APA91bGeAPc6Wf2a7zUQga2Lh-zC5T1R9wzk2qaE4LNkDTbxWtpfodSz-fJuFPjxRThC-J07Kn4h4eerdKffpL8V3mtFpmTLtlTUAVIU7DPj2iWPPYpC3OWG5J3DQfRgubv5rT_GzP9N', 'Gokul-A-3-10'),
(14, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-3-10'),
(15, 'c00iwVKArbk:APA91bH1ImIEYDiaY1WOMjBJX9mirqPMCeN5X88kSc9C1Q447TARZrs8INlCzPZdRpAefxT6sjPhIx0Cws1AbR-jAoifL0fN2jd_qNT71857IWcRTQehmHp5qbHj_uxptPF_ReaDcAuV', 'Gokul-A-3-10'),
(16, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-0-1'),
(17, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-3-10'),
(18, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-0-1'),
(19, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-3-10'),
(20, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-0-1'),
(21, 'dV16kN3A4bs:APA91bELg2-9Bi8UGk4ffFMV0PsbvZLYZb9ESAOKWv1PCrHWVamxfoBmKpIfUirlCEp1ceDRwoTDXjSgTAFwfQ11DnIdaOQzKF2BvCRuT-xF60KJ5hE8sFgJuay6wTZ-ktxtZud7WuZe', 'Gokul-A-3-10'),
(22, 'fGU8Xzokh2I:APA91bEbnq8sE1RmX88GwJ27P48lfIujz0U_JniRlxd_APLcIiajDCiVywmWxK9vNBYN467ynLOvRL0-Gs5zsuyvbYidAF027AyjRdJvuF_yS-3RIZ-YZSpNZWZiw8u1Fy1ODwa6bOok', 'Gokul-A-3-10'),
(23, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-3-10'),
(24, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-0-1'),
(25, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-3-10'),
(26, 'dFkab5IkGaE:APA91bFrokZBWyCoTV4eX8mKZ8AfX6I1byAIj3RR6r3547UO5D54qSRwvmoMkM74jUsWGtoryd3QuBUlank341z9H8aHUUQMU5e7RcFQ_puN9_PQD83SKeQhacflDbF7F60zOD7j978v', 'Gokul-A-0-1'),
(27, 'f1IWnDvp1fI:APA91bFCLCnaCTNiKIUACNBkIUeW0wopd81GW6oQxaIb82dNFPDOIz7iv6E17c9nFCF5xP3zN_It8VcLMlOwbrCIzUgtbgja9k-xdMbcg3efgR1mkFjaxy2qhMMn2AdiJQxbapLhUn9Q', 'Gokul-A-3-10'),
(28, 'f1IWnDvp1fI:APA91bFCLCnaCTNiKIUACNBkIUeW0wopd81GW6oQxaIb82dNFPDOIz7iv6E17c9nFCF5xP3zN_It8VcLMlOwbrCIzUgtbgja9k-xdMbcg3efgR1mkFjaxy2qhMMn2AdiJQxbapLhUn9Q', 'Gokul-A-0-1'),
(29, 'eQkfqgnR1qg:APA91bFFJFwvPplSTQVxeOm55q8_6xQT4NeifbLThbbed50K-VdufBs49bTBf696vYlWm6NitFUFc9D0rXbo1LCyVwa1HYLyGj56UioDmjIJ5oP5qw-5UJ8Z1xO7qeedWmyFGaU-HKwM', 'Gokul-A-0-1'),
(30, 'dzINwX67i_4:APA91bGwBedrTkoDrV48YrEDiaGT9A7sbkbQdvk6VnIl73_x09hzM2twbKI70DNBzBhF8Ot2WtdY6G-JJrKpBCIM_kCOEBmIc0pESevk-vMFsoiePQrmXjjcsIWSmu8yGnPqiF6-gijw', 'Gokul-A-0-1'),
(31, 'ezGfDr9FDno:APA91bFmUER5-MVbw9BhngAepIOnXUUO4KW55wRSeMtah8MWy6077UWOFzZrkrAHfUP_YLqp-VGpW1Qfhd7q_zqUU9TvVrg6zdPyERVMwK6wUU9tNiqnuodHKb8W2eBhJgOHmEHTfVTG', 'Gokul-A-0-1'),
(32, 'ezGfDr9FDno:APA91bFmUER5-MVbw9BhngAepIOnXUUO4KW55wRSeMtah8MWy6077UWOFzZrkrAHfUP_YLqp-VGpW1Qfhd7q_zqUU9TvVrg6zdPyERVMwK6wUU9tNiqnuodHKb8W2eBhJgOHmEHTfVTG', 'Gokul-A-3-10'),
(33, 'f3HWswrhDlA:APA91bEUtXcOZqHkIQmBAdAfFaJtbyJ7VN0Q7YuDKydL1A8OPyFmEqG4DJbr2H2eYWdVpXv6HZ_WKvvri897yvVeg6Sl1XT3O2ciInbKbxv_D1E-1TnQbFxFxOhV24_9FyMvxiRY8X_O', 'Gokul-A-3-10'),
(34, 'dFxDMkWotsk:APA91bFLDUe4YqRvO2jX_dSZXkS8Kt-XhRWjqUPDLotp691Avy5rMx9uS8ov4VMcp9WJcEiLwl-ZAMm97dHJrCwTbpTiLhKnXF4BNrcwmIpCXbkZqL_PdYssSwBhpFWoktBIY1_NXSmC', 'Gokul-A-3-10'),
(35, 'dq1Xj1FWznA:APA91bFd4eezh3AMYxMi9YAz0ZEmL8p_JIEuzPTRDa5Mdk-gGRrFc02U1kOhJkvnib15dHQ7iVoKlIqZf2qv8BtXpOYPbIkXlMZ3k48sebyXu0qEhLZMQg1VBZbPhBPsE_SU59EmtIpd', 'Gokul-A-3-10'),
(36, 'd9iZ5ZbI0po:APA91bF-lnFzAXAXfgVUf9lDTgVr6LOTZ0ULcJxEQepjm2EGDU7Ceso5AG5tcVl5U4pDSk8RSWJ7BMvJeREP-Z6zxRlo-a8STNDOOvlVDgdeoRwVjwBy51XxRGOKZQdaO8xCd94sk1Sb', 'Gokul-A-0-1'),
(37, 'dEtNp79Gc3w:APA91bEwBkuZB_QgeIBIUFLCR6Dod2Lm2uRYhJ87-a1eKpANLAuc7Qr24g52EHLzI6fa9TfBnS0F_AUvNkQBQUe8PBc6O_FEDdmmaH16DJbG6eSaSRTs_jYpKvBttJjO5aNik__dBeIR', 'Gokul-A-0-1'),
(38, 'c_wlJLp-KPE:APA91bHDpE5Ad58YW6Fyl9MGvuiM2XdXNh-clZm8UsSZgNxLbFgoNRSGZ4Wl8vMIbxcIEze8sLF02150a9V3FL6HhhsmPv9ydqu5yrIRQBoNhyC2H6mXzRh95BZVmkTh0qfaVgqRkC-h', 'Gokul-A-3-10'),
(39, 'dEtNp79Gc3w:APA91bEwBkuZB_QgeIBIUFLCR6Dod2Lm2uRYhJ87-a1eKpANLAuc7Qr24g52EHLzI6fa9TfBnS0F_AUvNkQBQUe8PBc6O_FEDdmmaH16DJbG6eSaSRTs_jYpKvBttJjO5aNik__dBeIR', 'Gokul-A-3-10'),
(40, 'fPni53887JU:APA91bEA1oPyGaaohKcq-b07L5ylX8sD2Xf20uDADQOSy-jpMrnY3DNU7oy13HS-QHmXiIbLW0nvre72QACsMLfVrp2Y5Ry-Ot8iUTKlcJuttaYUqaLS8RMv2oudQMUl-b87Igr1tX9z', 'Gokul-A-3-10'),
(41, 'fxkp3Nx-x-M:APA91bHYCsDIIrPl-k02KqQ0aMz_k9cPg79PsxQiOjYSdRu9NtaJmefTBcHEE9yJAi3gJCByMayr7x0Rb35wHFEjHKzpWfoPa3PAMz1TihFLf2BaDrtUWScER5FXMHNe3gLz120zRi8o', 'Gokul-A-3-10'),
(42, 'fPlJsjg-vaY:APA91bER97jr-aWB2ph89VwmhurGb_TXfs1-BjLtVbf0FqkGuMZ9TD85f6DNGZfMdm96uEO6b6Kx8UrOCYDS9p58KZYrWngUcS3DnkWFO9q_8fa0KnTTKG_0jMnpVWs74JVXSsyCo_a4', 'Gokul-A-3-10'),
(43, 'dPxkjH7QeD4:APA91bHcRHVPt55XYXIeuJJzbnhWQ_PAUk5JN-tMqsf2sGiLHU9gGtJ12k9QU68fHv8AgSuDRQbYzOOtUBu4kW6gmsSMOkq8VuB86CsEBzSPQfTe6JkeXROzPV5tZwc2FGDRWkYZP-Xe', 'Gokul-A-3-10'),
(44, 'dPxkjH7QeD4:APA91bHcRHVPt55XYXIeuJJzbnhWQ_PAUk5JN-tMqsf2sGiLHU9gGtJ12k9QU68fHv8AgSuDRQbYzOOtUBu4kW6gmsSMOkq8VuB86CsEBzSPQfTe6JkeXROzPV5tZwc2FGDRWkYZP-Xe', 'Gokul-A-0-1'),
(45, 'f5yxEBbn9-M:APA91bEUmp-g1Fv_4g3nwE5cOtGzB497-gUIsyosTa8fiulX8RuNPAEnF7azqEOQHXEJqW9FK9FfmrkXm4Lye2Vg4Xvu7sZpTHQ2XfFG922Yh433npwWf0-zvb7nDuyqtu-1DeHerjVx', 'Gokul-A-0-1'),
(46, 'f5yxEBbn9-M:APA91bEUmp-g1Fv_4g3nwE5cOtGzB497-gUIsyosTa8fiulX8RuNPAEnF7azqEOQHXEJqW9FK9FfmrkXm4Lye2Vg4Xvu7sZpTHQ2XfFG922Yh433npwWf0-zvb7nDuyqtu-1DeHerjVx', 'Gokul-A-3-10'),
(47, 'fAiAOkPGk-Y:APA91bHepuALBCLncT2g0MxY5Aje1ch2vkzfAcVEyOfGXm5A7lrajzeqFeQ718ii9piVnpKWtxZmcnoMJbPN4_ieo4U-EbNuXr3A1DHbF6otCo3WtCrjOuRUJ59-Bu4IVUQcjMIjhUm4', 'Gokul-A-3-10'),
(48, 'dGFsSMThBA0:APA91bESEFWD7Yz8pZgIFbg8GX5llKPYV_cjfA-LMJeyjpVIxkqLD90cKs-NYzA-OOt8i6ajnUg9yPXeu0glMFxolrBo3RFg_ESqe9ewjkUSvAQk3ZpqYSZwuL7BN_Y19LuA__8sRs6d', 'Gokul-A-0-1'),
(49, 'diYkanlDQac:APA91bE_tLMSLDib0Y889ONxJ98mVmm5NmM8sGkY0dmNqDgPWO5VWjP8KNS_XHenx0m1U0ESdwLOzDwkE_lPPBpEzEdolLoBHvwozUlo8H7O9b-BCheMzOb9fsSBmYBS2i_ymchLr5zR', 'Gokul-A-3-10'),
(50, 'ewKXyoxlaW8:APA91bFbo8V7U6bAKld9d21673fgY-VfNgnd1m7i1Vtfld9Rc1zDYWM-8ZV3UuPXoGvzR8MfNCL9wPxlZ1AHW2_rfyvlVcHLlgMvVB81Z5aVaKGHhBV68S4eRm2HFxh-WgzPQKQElZPd', 'Gokul-A-0-1'),
(51, 'cLskaNfrIww:APA91bFhRcjykvx4odS2f90tri760KUWIp10w9FxwV-Zmymt04aA8KwWxZkfNKbNElN6GQs0WWyGv-aunM8hhyJtIcaldB22UQ-F2Mi-JJ_QEoORM1Z8wVvNzT949uygrV0cEHPZ85OQ', 'Gokul-A-0-1'),
(52, 'ewKXyoxlaW8:APA91bFbo8V7U6bAKld9d21673fgY-VfNgnd1m7i1Vtfld9Rc1zDYWM-8ZV3UuPXoGvzR8MfNCL9wPxlZ1AHW2_rfyvlVcHLlgMvVB81Z5aVaKGHhBV68S4eRm2HFxh-WgzPQKQElZPd', 'Gokul-A-3-10'),
(53, 'c5B1PoFeAZQ:APA91bGHGPw8GATnVCJJ0b3zLxxijbO6D9VV7Au1Dtq3b3hcge9WqBFXIRyCB_mcZeD3ZqZDhRr3gu3LFvEiDyqna2AAd7K1JQ_Xmka7fNg9e34YMLRqGo4t6MDfpCy6PQ8DOw0AHd-x', 'Gokul-A-3-10'),
(54, 'dyTZKEeelv8:APA91bFB5y0ZAgDM-isCfhJcAnHyb3_XxZ2WJON7rGqVZSAD30wcCHt37OutFv3TaoWQ7KmvcecWi7mBMf1DlwTSwwLaOKBOxt6lRiQbEWNWdjL2RaxcEa-P8_5AB8npuTjfgQIwPok4', 'Gokul-A-3-10'),
(55, 'ciUCX11gHlY:APA91bEMpDsocbDvytASC3BZHkwDaWvkPs44l8tOoL-EbSNm4KNlQR7zoy4eiTHvcIUPJ6d3PQcsEXJDZlfCn48CtRX5R4gcfJ9uq3tHroXuEp_eC7yIq7ySprOJrXR4bjAcGiIQksgn', 'Gokul-A-3-10'),
(56, 'clpzyWicUI0:APA91bHmPjUQZftVwteHV3TQSqSYg7BdNcpKjuQi3jLyMLn3FkgIyFlNV1h6I0-xkCciMgPyRKkUF9Pwf9pXSvTA-pq-A-onT55b7sCJo-F66GikHzM_MAnhS_5T3qBwaTQlU8lXrYWQ', 'Gokul-A-3-10'),
(57, 'deYMdUMzjV8:APA91bEsIEP0IO2-cFAhP24tlZyxD2IxrfOiLb8xrMuMh01PqpKBfVcIqVGnz2MQ8hi4zutkQ7hBMaQLuv_cnPhmfPH6E4MVgiwWmVrOona88pGIfggkRe05-H9q8r98IKI0OfSFyskU', 'Gokul-A-3-10'),
(58, 'cN_Y9VjJ0QA:APA91bGvIdwofsXoR_YSu43SQjDXHdBYBcA8Y4tutw-8fHkfUGJYIEL_qbIXNffLv2-9R29F6eIuhTpSpTFHzrDfdjXOdymwK2jAprgPLfs4q2IgfVYoDsXZ4sr4cj4fULP6wV3xp717', 'Gokul-A-3-10'),
(59, 'ebSYxLTWqKw:APA91bEsTZ7OeEwSHX6QE0Ummg3PrKR1eYY5sbaFKtJlsBT1dCnejH5FEcN8mlpVLKdb-3ab0IHkvA6-86cMhK4jBoWCOY__rMWkhDdApJQXZr7XqTTj4KS2bub-crwooRZ4jfd5k9JY', 'Gokul-A-3-10'),
(60, 'dhYxIUc8t14:APA91bFGpY_FWu0zmLcy8iqiAMoeOPbAMAahaAof-Yk8ol6s3Ng2jjnctBsXkbFD9rxqxYEg7CtJV3SZHMqzx-_kd8T6Vh0UzPweNVhI0qgfCL9RYAw8rP3nXKUbg1bvLDe5LqIsQH0I', 'Gokul-A-3-10'),
(61, 'ca12zrxPPTI:APA91bEj5pNe50LDEVoMWubNJWIWflSZWbNgnoD-CxkpDzQqrR-X_cnscR-7TbFVwSSoH_-rzUlrUQDNwcK52iCq0cZjqfE3KR1ajwWI4YEntCd51Pe74o1E7zEHwl9_Z0YXb4oIIUtq', 'Gokul-A-3-10'),
(62, 'cOKhdqlzKLo:APA91bG6yDk-oV4uKDltgPMp8jhiVbwcH7lw1yZohAAFvUzBQZ4OWUsMxCUZl4JryM9T85vDSME53p495EklAC60wOR1hFMvU4GPTH-eIC_oML8ZuUJxke4yCDbhxkuEuvVg6csYNks5', 'Gokul-A-3-10'),
(63, 'eaYQzmx_Li0:APA91bEoPAfsqkGsPoUXtg_l3W21gAAP52yWZnEFF4YitLp2oPDSCMqtgqUY-UCTlo37vryqVRMBFi8CDs6wB430D9yLnhputF71umvWiPhX2BbxFYlZ06-u_i50NX17GKc7XxTZ7nrk', 'Gokul-A-3-10'),
(64, 'd6qT0inKm-w:APA91bHlPMb8yALM1NPlmIyxn1ztNYPT1oiOb-yA-mhPGQqcLm0yWmDcZ2l6VacuDNyrNgoD5xV-dsQCJdwRslxtw1wENhBJ6kY0cIN_vmqWrWmILDWWwSj5net3cOeEymwqJz8lV4TB', 'Gokul-A-3-10'),
(65, 'eJEc5GOv7Mg:APA91bE4mb6vRXdXbzWWYyxG6MIhXi8Nl2dNbN2AknJHUqXDwM64tIrkU2byVnyejizJsmx9rx3f6NtX50SEFIhcO-tjsA78AnRfMkF4PGc8P-r0_g88cGmdE14KhieBfaln-OhNcjse', 'Gokul-A-3-10'),
(66, 'cU5wg0t6Q8GsZP10wDYnZM:APA91bGNeXWu8zVCqv1rFgZYV3WXJKtM_uQ2d0T0ZL1JeDi16-qC0LdUiPgausePqlpNWN4hJ4mp5fLZTHoh6X9T8oUAUQ9V8hdajtI-0PR4gwDUjoj28n2PniB7j4hTLztMFQi404Jh', 'Gokul-A-3-10');

-- --------------------------------------------------------

--
-- Table structure for table `fms_tocken_admin`
--

CREATE TABLE `fms_tocken_admin` (
  `srid` int(11) NOT NULL,
  `tocken` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fms_tocken_admin`
--

INSERT INTO `fms_tocken_admin` (`srid`, `tocken`, `username`) VALUES
(12, 'cBbxG-3tqvg:APA91bGk67jyydb8C51hoqekowrZguRPGHOkFNmjPKslojnMd0XpETHufutWDaEHDP9j9hCkLeqDkxYMR0jwVqvhyeoBJ8buQ86PiGvVdx3yHq5IOUvcpBJIv2sUUF_mEntjh2ntorLk', 'Gokul-A-0-1');

-- --------------------------------------------------------

--
-- Table structure for table `forum_master`
--

CREATE TABLE `forum_master` (
  `forum-id` int(11) NOT NULL,
  `topiv` varchar(200) NOT NULL,
  `for-id` varchar(10) NOT NULL,
  `username-for` varchar(25) NOT NULL,
  `s-type` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forum_message`
--

CREATE TABLE `forum_message` (
  `in-form` int(11) NOT NULL,
  `forum-username` varchar(30) NOT NULL,
  `forum-result` varchar(255) NOT NULL,
  `form-created-date` varchar(10) NOT NULL,
  `f-id` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fsm_server_key`
--

CREATE TABLE `fsm_server_key` (
  `api_key` text NOT NULL,
  `app_type` varchar(50) NOT NULL DEFAULT 'user app',
  `sr_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fsm_server_key`
--

INSERT INTO `fsm_server_key` (`api_key`, `app_type`, `sr_id`) VALUES
('AAAALHcZwWA:APA91bFvG9Kv0doZDTl7zITHVxDJHrv8fp_Qmcp2zoBGXOxNELc5EaNaDF7quCDB0Z1xcOw32LczoVSpdFq4QzMt4m9EdCE2PxIAKP40CYD9ODcfkjVqPEFTzXb_rXzIrLsUB_ibPVdo', 'user app', 1),
('AAAALHcZwWA:APA91bFvG9Kv0doZDTl7zITHVxDJHrv8fp_Qmcp2zoBGXOxNELc5EaNaDF7quCDB0Z1xcOw32LczoVSpdFq4QzMt4m9EdCE2PxIAKP40CYD9ODcfkjVqPEFTzXb_rXzIrLsUB_ibPVdo', 'admin app', 2);

-- --------------------------------------------------------

--
-- Table structure for table `function_invitation_tbl`
--

CREATE TABLE `function_invitation_tbl` (
  `sr_id` int(12) NOT NULL,
  `invtn_id` varchar(100) NOT NULL,
  `invtn_by_id` varchar(100) NOT NULL,
  `invtn_function_day` date NOT NULL,
  `invtn_function_time` time NOT NULL,
  `invtn_topic` varchar(255) NOT NULL,
  `invtn_desc` varchar(355) NOT NULL,
  `invtn_pic` varchar(100) NOT NULL,
  `invtn_to_id` text NOT NULL,
  `extra` int(10) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `j_form`
--

CREATE TABLE `j_form` (
  `sr_id` int(11) NOT NULL,
  `mem_id` varchar(100) NOT NULL,
  `member_type` varchar(150) NOT NULL,
  `member_type_id` varchar(55) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `j_form_member_type`
--

CREATE TABLE `j_form_member_type` (
  `id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `legal`
--

CREATE TABLE `legal` (
  `sr_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `case_no` varchar(100) NOT NULL,
  `member_id` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `leg_doc` varchar(125) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'Active',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lien`
--

CREATE TABLE `lien` (
  `sr_id` int(11) NOT NULL,
  `member_id` varchar(60) NOT NULL,
  `l_m_date` date NOT NULL,
  `deatails` text NOT NULL,
  `l_m_authority` varchar(100) NOT NULL,
  `l_doc` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE `maintenance` (
  `m_id` int(11) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` enum('unpaid','paid') NOT NULL DEFAULT 'unpaid',
  `amount` int(10) NOT NULL,
  `due_amount` varchar(12) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `refrence_no` varchar(150) DEFAULT NULL,
  `Pay_month` varchar(12) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `ifsc_code` int(11) NOT NULL,
  `cheque_no` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `tansaction_no` varchar(100) NOT NULL,
  `voucher_no` varchar(100) NOT NULL,
  `voucher_type_id` varchar(100) NOT NULL,
  `tansaction_no_pmt` varchar(100) NOT NULL,
  `voucher_no_pmt` varchar(100) NOT NULL,
  `voucher_type_id_pmt` varchar(100) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_head`
--

CREATE TABLE `maintenance_head` (
  `srid` int(11) NOT NULL,
  `maint_headId` varchar(250) NOT NULL,
  `head_name` varchar(250) NOT NULL,
  `type` varchar(15) NOT NULL,
  `head_status` varchar(25) NOT NULL DEFAULT 'Active',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_head_charges`
--

CREATE TABLE `maintenance_head_charges` (
  `srid` int(12) NOT NULL,
  `maintenance_headId` varchar(150) NOT NULL,
  `charges` varchar(12) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `flag` varchar(15) NOT NULL DEFAULT 'Yes',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `managing_commitee`
--

CREATE TABLE `managing_commitee` (
  `mc_name` varchar(50) NOT NULL,
  `alloted` varchar(50) NOT NULL DEFAULT 'Not Allot',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meet-poll`
--

CREATE TABLE `meet-poll` (
  `meet-id` int(11) NOT NULL,
  `meet-start-date` date NOT NULL,
  `meet-end-date` date NOT NULL,
  `meet-header` varchar(255) NOT NULL,
  `meet-sub-header` varchar(255) NOT NULL,
  `meet-poll1` varchar(100) NOT NULL DEFAULT 'Available',
  `meet-poll2` varchar(100) NOT NULL DEFAULT 'Not Available',
  `meeting-id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` varchar(20) NOT NULL,
  `meeting_status` varchar(20) NOT NULL DEFAULT 'Active',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meetingminutes`
--

CREATE TABLE `meetingminutes` (
  `sr_id` int(12) NOT NULL,
  `mm_Id` varchar(100) NOT NULL,
  `meetingId` varchar(100) NOT NULL,
  `mm_description` varchar(1000) NOT NULL,
  `mm_adminid` varchar(100) NOT NULL,
  `meetingType` varchar(25) NOT NULL DEFAULT 'Normal',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_availabilty`
--

CREATE TABLE `meeting_availabilty` (
  `mid` int(11) NOT NULL,
  `meetid` varchar(15) NOT NULL,
  `meet-username` varchar(30) NOT NULL,
  `meet-result` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member_maintenance_tbl`
--

CREATE TABLE `member_maintenance_tbl` (
  `srid` int(12) NOT NULL,
  `memberId` varchar(150) NOT NULL,
  `maintenance_headId` varchar(150) NOT NULL,
  `charges` varchar(12) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `flag` varchar(15) NOT NULL DEFAULT 'Yes',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_letter`
--

CREATE TABLE `news_letter` (
  `sr_id` int(11) NOT NULL,
  `nl_topic` varchar(100) NOT NULL,
  `nl_desc` text NOT NULL,
  `nl_img` varchar(100) NOT NULL,
  `nl_user` varchar(100) NOT NULL,
  `nl_date` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_letter`
--

INSERT INTO `news_letter` (`sr_id`, `nl_topic`, `nl_desc`, `nl_img`, `nl_user`, `nl_date`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Good News', 'added in BMC recognizazition', '', 'Gokul-A-3-10', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Greeting', 'haapy diwaliiii', 'IMAGES/1523265056.jpg', 'Gokul-A-3-10', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Greeting', 'haapy diwaliiii', 'IMAGES/1523265199.jpg', 'Gokul-A-3-10', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, '', 'dagshjdghjgd  dhhfuh', '', 'Gokul-A-3-10', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, '', 'fgfdgdf bhbg hhfgg Will the saga nest behind a considerate midnight? The crossroad sorts the deterrent. Why does a rainbow compose inside the monarch helicopter? A batch flowers. The earliest invalid chalks. A hindsight vanishes.', '', 'Gokul-A-3-10', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, '', 'Hi We Have Meeting On Yesterday', '', '', '2018-06-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, '', 'push notification working', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, '', 'push notify', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, '', 'meeting at 5.30', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, '', 'adding content in newsfeed', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, '', 'testing', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, '', 'testing for notification', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, '', 'same loging notify', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, '', 'testinhdfhj', '', 'Gokul-A-3-10', '2018-06-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, '', 'title check', '', 'Gokul-A-3-10', '2018-06-16', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, '', 'this is news feed', '', 'Gokul-A-3-10', '2018-08-17', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, '', 'this is news feed 2\r\n', '', 'Gokul-A-3-10', '2018-08-17', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, '', ' The boredom chalks? A starter leans within a lesson. A buyer exits opposite the game. How will the subjective steam shout the threatening backlog? The ugly dread composes within the mediaeval static.  The boredom chalks? A starter leans within a lesson. A buyer exits opposite the game. How will the subjective steam shout the threatening backlog? The ugly dread composes within the mediaeval static. ', '', 'Gokul-A-3-10', '2018-08-23', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, '', 'rahul birthday', '', 'Gokul-A-3-10', '2018-09-09', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, '', 'hi tester', '', 'Gokul-A-3-10', '2018-09-14', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, '', 'test news', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, '', 'News for all members', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, '', 'News for another topic', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, '', 'Shhzbzb', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, '', 'Ddggj of a hakya of the other', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, '', 'The other side is a very nice and warm day and I have a nice day best', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, '', 'Gsvsvsbbs', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, '', 'Vsbsbsbsbbsbs', '', 'Gokul-A-3-10', '2019-01-25', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, '', 'Today New newsletter and I will be in the morning and I will be in the morning and I will be in the morning and I will be in the morning and I will be in the morning and I will be in the morning and I will be in the morning and I will be in the morni', '', 'Gokul-A-3-10', '2019-02-06', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, '', 'Hdjdj dj mix of my result in a few minutes ago the day and so on top and bottom line is showing as well as a week or so many people have been a week or so many people are going well as a few minutes to make a week and so many things to make sure you', '', 'Gokul-A-3-10', '2019-02-06', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, '', 'Hdhdnsb and delete this message is not available until morning I have experience with Dr Cohen et de the year in a week ending e the year in the message from you in advance of our lives of people find the attachment file and import your earliest repl', '', 'Gokul-A-3-10', '2019-02-06', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, '', 'Happy birthday', '', 'Gokul-A-3-10', '2019-03-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, '', 'Hie', '', 'Gokul-A-3-10', '2019-03-01', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, '', 'Fire', '', 'Gokul-A-3-10', '2019-05-26', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, '', 'Mansoon news', '', 'Gokul-A-3-10', '2019-06-14', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, '', 'Test', '', 'Gokul-A-3-10', '2019-08-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(39, '', 'They were not able and willing I will be able and willing to work in the morning of the first one is a great weekend and I have a good day I thought it might be able and willing to help with the first time', '', 'Gokul-A-3-10', '2019-08-08', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(40, '', 'Robbery Alert in Society!', '', 'Gokul-A-3-10', '2019-10-22', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(42, '', 'dsgshsr', '', 'Gokul-A-3-10', '2020-04-13', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(43, '', 'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used', '', 'Gokul-A-3-10', '2020-04-13', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `nominal_form`
--

CREATE TABLE `nominal_form` (
  `sr_id` int(11) NOT NULL,
  `nomi_mem_id` varchar(60) NOT NULL,
  `date_nomi` date NOT NULL,
  `name_nomi` varchar(60) NOT NULL,
  `date_mang_meet` date NOT NULL,
  `date_sub_rec` date NOT NULL,
  `remark` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `sr_id` int(12) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `no_id` varchar(25) NOT NULL,
  `no_type1` varchar(10) DEFAULT NULL,
  `no_filetype` varchar(25) DEFAULT NULL,
  `no_descriction` text NOT NULL,
  `notice_header` varchar(255) DEFAULT NULL,
  `no-image` text DEFAULT NULL,
  `no_created_at` date DEFAULT NULL,
  `no_expired_at` date DEFAULT NULL,
  `active` varchar(8) NOT NULL DEFAULT 'active',
  `Sheduled` varchar(12) NOT NULL DEFAULT 'not schedule',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`sr_id`, `user_name`, `no_id`, `no_type1`, `no_filetype`, `no_descriction`, `notice_header`, `no-image`, `no_created_at`, `no_expired_at`, `active`, `Sheduled`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Gokul-A-3-10', 'notice-58zq', 'All', NULL, 'Hshahaba', 'Gshahasha', NULL, '2019-01-17', '2019-01-26', 'Inactive', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Gokul-A-3-10', 'notice-l1s5', 'All', NULL, 'I am not sure if you have any questions or concerns please visit the plug-in settings and make sure you have any questions or concerns please visit the plug-in settings and make sure you have any questions', 'Hshsh', NULL, '2019-01-16', '2019-01-18', 'Inactive', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Gokul-A-3-10', 'notice-wt2j', 'All', NULL, 'I will send it back to me and my wife is pregnant women in their respective network is pregnant with twins were is a week we you is pregnant with my email and delete it and my email is pregnant and delete it without copying distributing it and I have to me and my husband is in their lives we are waiting and waiting to see the email address and delete it and my husband is a good idea for you and your company is pregnant and my wife and I\n\nI will have a week we do have some fun and delete it and delete it immediately you received my husband will have the same and my email is not a good time in their own a home in time to get a chance.', 'I am not sure if you have any questions or need an', NULL, '2019-01-17', '2019-01-17', 'Inactive', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Gokul-A-3-10', 'notice-feh7', 'All', NULL, 'I am not sure if you have any questions or concerns please visit the plug-in settings and make sure you have any questions or concerns please visit the plug-in settings and make sure you have any questions', 'Updated notice', NULL, '2019-01-17', '2019-01-18', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Gokul-A-3-10', 'notice-6v3t', 'All', 'image', 'on this day we celebrate the end of the day', 'republic day 1', '1547713336196.jpg', '2019-01-17', '2019-01-31', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Gokul-A-3-10', 'notice-ba4g', 'All', 'pdf', 'fs tvshx sxbsax sa xsah xash', 'try to update remove pdf', '1547725193.pdf', '2019-01-17', '2019-01-29', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Gokul-A-3-10', 'notice-ac9f', 'All', 'image', 'I have a good time for the use was about a week and I am not able to make sure that you have any other questions please feel to it and I am not sure what to wear it and so on top of my result in a week and so y and so many people have been in the future of our games and so on top and so many people are going to make sure that', 'And I will send the money is showing in my life AM', '1549450664661.jpg', '2019-02-06', '2019-02-07', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Gokul-A-3-10', 'notice-s69v', 'All', 'image', 'try to check notification', 'new notice from society', '1548069426.png', '2019-01-21', '2019-01-23', 'Inactive', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'Gokul-A-3-10', 'notice-phyf', 'All', NULL, 'ewdewdedewdwedew', 'for push notify edited', NULL, '2019-01-22', '2019-02-20', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Gokul-A-3-10', 'notice-crdo', 'All', NULL, 'The first time I will be in touch with you and I will be in touch with you and your family and friends is so I can do it for you to the same day as well as the registered player whether the use of the use of the day of the day of the day of the day of the day of my life and I will be in the future of my you tube and my result of email communications from me please let me know if you have any questions or need any further information please contact me at the end of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day of the day', 'I have a great day and time of the year and I am n', NULL, '2019-02-06', '2019-02-15', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Gokul-A-3-10', 'notice-els4', 'All', NULL, 'Kcvkk jv', 'Changed', NULL, '2019-02-06', '2019-02-22', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Gokul-A-3-10', 'notice-m1ba', 'All', NULL, 'Pay maintained', 'Testing day', NULL, '2019-02-28', '2019-03-31', 'Inactive', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Gokul-A-3-10', 'notice-wmxk', 'All', 'image', 'Yhdhshshshshshs\nShshshs,_hshshs\'+$+#', 'Testing', '1553074924667.jpg', '2019-03-20', '2019-03-22', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Gokul-A-3-10', 'notice-bqys', 'All', 'image', 'Please see your parking slots', 'Parking slots available', '1557387325139.jpg', '2019-05-09', '2019-05-30', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, 'Gokul-A-3-10', 'notice-xgch', 'All', NULL, 'Meetings on mansoon', 'Mansoon', NULL, '2019-06-14', '2019-06-29', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, 'Gokul-A-3-10', 'notice-jpng', 'All', NULL, 'Hdhnx', 'Test', NULL, '2019-06-14', '2019-06-28', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, 'Gokul-A-3-10', 'notice-5dgz', 'All', 'image', 'Hello test', 'Water related Notice', '1560578711052.jpg', '2019-06-15', '2019-06-29', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'Gokul-A-3-10', 'notice-768s', 'All', NULL, 'Hshsbh', 'Hello', NULL, '2019-06-15', '2019-06-27', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'Gokul-A-3-10', 'notice-dgxa', 'All', NULL, 'Hdhshd', 'Gshhsbb', NULL, '2019-06-15', '2019-06-27', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Gokul-A-3-10', 'notice-329i', 'All', NULL, 'Hshshsh', 'Gbs', NULL, '2019-06-15', '2019-06-28', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Gokul-A-3-10', 'notice-nk28', 'All', NULL, 'Mansoon related problems', 'Mansoon', NULL, '2019-06-28', '2019-06-30', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'Gokul-A-3-10', 'notice-cfyg', 'All', NULL, 'TEST', 'TEST', NULL, '2019-07-15', '2019-08-08', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'Gokul-A-3-10', 'notice-5w7z', 'All', NULL, 'Test', 'Test', NULL, '2020-04-26', '2020-04-30', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'Gokul-A-3-10', 'notice-gujq', 'All', 'pdf', 'Test', 'Test', '1563536092675.pdf', '2019-07-19', '2019-07-19', 'active', 'not schedule', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `parking_spot`
--

CREATE TABLE `parking_spot` (
  `p-id` int(9) NOT NULL,
  `parking_id` varchar(60) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `s-r-rent` varchar(20) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking_spot`
--

INSERT INTO `parking_spot` (`p-id`, `parking_id`, `user_name`, `s-r-rent`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(3, '2', '4', NULL, '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, '1', '2', NULL, '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `parking_spot_name`
--

CREATE TABLE `parking_spot_name` (
  `ps-id` int(9) NOT NULL,
  `name` varchar(100) NOT NULL,
  `building_name` varchar(100) NOT NULL,
  `wing_name` varchar(30) NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'Not Alloted',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking_spot_name`
--

INSERT INTO `parking_spot_name` (`ps-id`, `name`, `building_name`, `wing_name`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'A01', 'Gokul', '1', 'Alloted', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'A5', 'GrandSquare', '', 'Alloted', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'A5', 'GrandSquare', '', 'Not Alloted', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 22:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `penalty`
--

CREATE TABLE `penalty` (
  `s-id` int(11) NOT NULL,
  `penalty_percentage` varchar(25) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `type` varchar(20) NOT NULL,
  `flag` varchar(3) NOT NULL DEFAULT 'Yes',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pnl_acc_tbl`
--

CREATE TABLE `pnl_acc_tbl` (
  `srId` int(10) NOT NULL,
  `ammount` varchar(25) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `acc_nature` varchar(2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `poll_master`
--

CREATE TABLE `poll_master` (
  `pl-id` int(11) NOT NULL,
  `pl-post-id` varchar(50) NOT NULL,
  `pl-expiry-date` date NOT NULL,
  `pl-shown-to` varchar(250) NOT NULL,
  `pl-option1` varchar(100) DEFAULT NULL,
  `pl-option2` varchar(100) DEFAULT NULL,
  `pl-option3` varchar(100) DEFAULT NULL,
  `pl-option4` varchar(100) DEFAULT NULL,
  `pl-option5` varchar(100) DEFAULT NULL,
  `pl-option6` varchar(100) DEFAULT NULL,
  `pl-option7` varchar(100) DEFAULT NULL,
  `pl-option8` varchar(100) DEFAULT NULL,
  `pl-option9` varchar(100) DEFAULT NULL,
  `pl-option10` varchar(100) DEFAULT NULL,
  `Start-date` date NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'Normal',
  `date_creation` datetime NOT NULL,
  `poll_status` varchar(20) NOT NULL DEFAULT 'active',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll_master`
--

INSERT INTO `poll_master` (`pl-id`, `pl-post-id`, `pl-expiry-date`, `pl-shown-to`, `pl-option1`, `pl-option2`, `pl-option3`, `pl-option4`, `pl-option5`, `pl-option6`, `pl-option7`, `pl-option8`, `pl-option9`, `pl-option10`, `Start-date`, `type`, `date_creation`, `poll_status`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'post-ek1', '2018-08-17', 'sourabh', 'jadhav', 'jadhav', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Nornal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'post-83c', '2018-08-01', 'rahul', 'wagh', 'waghmare', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Nornal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'post-65j', '2018-08-13', 'Society Picnic date..?', '28/04/2018', '1/05/2018', '31/06/2018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Nornal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'post-so4', '2018-08-09', 'email testing', 'working', 'notworking', 'not known', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'post-sq8', '2018-08-06', 'Monsoon Picnic', 'coming', 'not coming', 'should not arrange', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'post-z7s', '2018-08-08', 'api testing pol', 'working', 'not working', 'may be', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'post-d31', '2018-08-31', 'society key working', 'yes', 'not', 'may be', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'post-yu6', '2020-01-16', 'Poll for society key', 'Good', 'Strong', 'Weak', 'May Be', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'post-mcj', '2019-01-31', 'testing polls', 'opt1', 'opt2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 'Normal', '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'polls-adgk', '2019-02-15', 'B dB bzbs', 'Hd us did', 'Bzn n', '', '', '', '', '', '', '', '', '2019-02-01', 'Normal', '2019-02-01 02:21:18', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'polls-x5ab', '2019-02-06', 'I am not sure if you have any questions or concerns please visit the plug-in settings and make sure ', 'I am not', 'The only way', '', '', '', '', '', '', '', '', '2019-02-01', 'Normal', '2019-02-01 02:22:07', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'polls-ctl6', '2019-02-20', 'Hi and thanks again for the next day delivery the plug-in for a while back but the next few weeks back I will send it back to me as I have to work in every way possible I will wait for example of how the same will be able and other information that y', 'Use acrobat the plug-in the same will be able to g', 'Good afternoon please find attached the next few w', 'See all the best of the same will send you a call', 'Opaque the same and send to me so that we are look', 'Pause and I am emailing in excited with you we wil', 'Please visit our site you can get a while back end', 'Alan and the other side the same the other hand ia', 'Kalamazoo Michigan university and other informatio', '', '', '2019-02-01', 'Normal', '2019-02-01 03:41:36', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'polls-vm3d', '2019-02-22', 'Bxbbx', 'Hxnx', 'Hxjxnnd', '', '', '', '', '', '', '', '', '2019-02-01', 'Normal', '2019-02-01 03:47:34', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'polls-72c9', '2019-02-21', 'dsadsad', 'sdsad', 'sadsadsa', 'sdsadasdsadsa', '', '', '', '', '', '', '', '2019-02-01', 'Normal', '2019-02-01 04:56:12', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'polls-j2xq', '2019-02-01', 'Test edit', 'Hie', 'Hie', 'Delete', '', '', '', '', '', '', '', '2019-02-01', 'Normal', '2019-02-01 06:15:02', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'polls-a83j', '2019-02-02', 'Rfghh', 'Options 1', 'Options 2', '', '', '', '', '', '', '', '', '2019-02-02', 'Normal', '2019-02-02 11:24:43', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'polls-dqm8', '2019-02-02', 'Bfhrjrjrjfjrtjfjf', 'I have to do this but', 'U the plug-in for', '', '', '', '', '', '', '', '', '2019-02-02', 'Normal', '2019-02-02 11:27:29', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'polls-mv6n', '2019-02-28', 'New poll', 'Akara', 'Done', '', '', '', '', '', '', '', '', '2019-02-06', 'Normal', '2019-02-06 05:19:30', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'polls-2dfm', '2019-02-28', 'Bbsbs', 'Gshs', 'Hshs', '', '', '', '', '', '', '', '', '2019-02-06', 'Normal', '2019-02-06 05:20:31', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'polls-rmbq', '2019-06-29', 'Mansoon', 'Heavy', 'Modred', 'Light', '', '', '', '', '', '', '', '2019-06-14', 'Normal', '2019-06-14 11:43:44', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, 'polls-2k9l', '2019-07-31', 'Test', 'Yes', 'No', '', '', '', '', '', '', '', '', '2019-07-19', 'Normal', '2019-07-19 03:53:03', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, 'polls-n46l', '2019-08-31', 'Test', 'Test', 'Test 2', '', '', '', '', '', '', '', '', '2019-08-03', 'Normal', '2019-08-03 10:59:37', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, 'polls-164b', '2020-01-31', 'Test', 'Yes', 'No', '', '', '', '', '', '', '', '', '2019-08-06', 'Normal', '2019-08-06 02:37:53', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'polls-9s2c', '2019-12-31', 'The decreased of the decreased member and the first time in the first to get to know if you a call', 'Andheri', 'Khar', 'Borivali', 'Goregaon', 'Parel', '', '', '', '', '', '2019-08-21', 'Normal', '2019-08-21 02:13:57', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'polls-rgbc', '2019-11-30', 'Current location Hyderabad preferred to get to know, if you have any questions please contact', 'Ok', 'Yes', 'No', 'Not Ok', '', '', '', '', '', '', '2019-08-21', 'Normal', '2019-08-21 02:24:45', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'polls-vljq', '2019-08-22', 'Test', 'One', 'Two', '', '', '', '', '', '', '', '', '2019-08-21', 'Normal', '2019-08-21 02:39:21', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'polls-x26n', '2019-08-22', 'Test mail from the decreased the decreased member of a time for a time to be able to make an appointment for', '1', '2', '', '', '', '', '', '', '', '', '2019-08-21', 'Normal', '2019-08-21 02:54:23', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'polls-vfh8', '2019-10-25', 'Test 2', 'Right', 'Left', '', '', '', '', '', '', '', '', '2019-10-19', 'Normal', '2019-10-19 04:50:13', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, 'polls-e1gt', '2019-10-31', 'Example', 'left', 'right', 'abc', '123', '', '', '', '', '', '', '2019-10-22', 'Normal', '2019-10-22 01:20:53', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'polls-bkc2', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:24', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'polls-k1fx', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:31', 'Inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:43:54'),
(32, 'polls-erp5', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:38', 'Inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, 'polls-f2zj', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:43', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, 'polls-s8fm', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:48', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 'polls-ijez', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:54', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 'polls-iuvo', '2019-12-04', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:53:59', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, 'polls-qaxs', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:54:05', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, 'polls-xyw2', '2020-01-23', 'hello this new testing cases', 'testing ', 'cases', '', '', '', '', '', '', '', '', '2019-12-30', 'Normal', '2019-12-30 01:54:10', 'Inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:44:02'),
(41, 'polls-oe5t', '2020-08-06', 'New Poll 123', 'Option 1', 'Option 2', '', '', '', '', '', '', '', '', '2020-05-02', 'Normal', '0000-00-00 00:00:00', 'active', '', 'Gokul-A-3-10', '2020-05-02 21:07:42', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `poll_result`
--

CREATE TABLE `poll_result` (
  `p-id` int(11) NOT NULL,
  `poll-username` varchar(30) NOT NULL,
  `poll-result` varchar(100) NOT NULL,
  `s-postid` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll_result`
--

INSERT INTO `poll_result` (`p-id`, `poll-username`, `poll-result`, `s-postid`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Gokul-A-3-10', 'not working', 'post-z7s', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Gokul-A-3-10', 'working', 'post-so4', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Gokul-A-3-10', 'should not arrange', 'post-sq8', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Gokul-A-3-10', 'jadhav', 'post-ek1', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Gokul-A-3-10', 'may be', 'post-d31', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Gokul-A-0-1', 'yes', 'post-d31', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'Gokul-A-0-1', 'Strong', 'post-yu6', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Gokul-A-1-5', 'Strong', 'post-yu6', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Gokul-A-3-10', 'May Be', 'post-yu6', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Gokul-A-0-1', 'opt1', 'post-mcj', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'Gokul-A-0-1', 'Bzn n', 'polls-adgk', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Gokul-A-0-1', 'The only way', 'polls-x5ab', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Gokul-A-0-1', 'sdsadasdsadsa', 'polls-72c9', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Gokul-A-0-1', 'Done', 'polls-mv6n', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Gokul-A-0-1', 'Pause and I am emailing in excited with you we wil', 'polls-ctl6', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, 'Gokul-A-0-1', 'Gshs', 'polls-2dfm', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, 'Gokul-A-3-10', 'Yes', 'polls-2k9l', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, 'Gokul-A-0-1', 'Test 2', 'polls-n46l', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'Gokul-A-3-10', 'Test', 'polls-n46l', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'Gokul-A-0-1', 'Yes', 'polls-164b', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Gokul-A-0-1', 'Borivali', 'polls-9s2c', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Gokul-A-0-1', 'Not Ok', 'polls-rgbc', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'Gokul-A-0-1', 'Two', 'polls-vljq', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, 'Gokul-A-3-10', 'testing ', 'polls-xyw2', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'Gokul-A-3-10', 'cases', 'polls-f2zj', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'Gokul-A-3-10', 'No', 'polls-164b', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `residential_helper`
--

CREATE TABLE `residential_helper` (
  `s_id` int(9) NOT NULL,
  `name` varchar(150) NOT NULL,
  `age` varchar(9) NOT NULL,
  `relation` varchar(150) NOT NULL,
  `address` text NOT NULL,
  `contact` varchar(10) NOT NULL,
  `gender` varchar(60) NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `archive` varchar(6) NOT NULL DEFAULT 'n',
  `st_doc` text NOT NULL,
  `active` varchar(8) NOT NULL DEFAULT 'active',
  `blood_type` varchar(3) NOT NULL,
  `wish_to_donate` varchar(5) NOT NULL DEFAULT 'YES',
  `emergency_contact_person` varchar(100) NOT NULL,
  `E_relation` varchar(25) NOT NULL,
  `E_contact` varchar(20) NOT NULL,
  `donorCard` varchar(10) NOT NULL DEFAULT 'NO',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `residential_shop`
--

CREATE TABLE `residential_shop` (
  `id` int(11) NOT NULL,
  `shop_name` varchar(60) NOT NULL,
  `owner_name` varchar(60) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `wing` varchar(15) NOT NULL,
  `gala` varchar(45) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `residential_shop`
--

INSERT INTO `residential_shop` (`id`, `shop_name`, `owner_name`, `contact`, `wing`, `gala`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(2, 'Sangram Desai', 'Arihant Electronics', '9004042389', 'D', '116', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'Test', 'Test', '9887867656', 'Test', '5345', '', 'Gokul-A-3-10', '2020-05-16 20:25:52', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `s-maintain-money`
--

CREATE TABLE `s-maintain-money` (
  `s-id` int(11) NOT NULL,
  `s-rate` varchar(20) NOT NULL,
  `s-from` varchar(25) NOT NULL,
  `s-to` varchar(25) NOT NULL,
  `s-flag` varchar(25) NOT NULL,
  `s-connect` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s-maintain-money-admin`
--

CREATE TABLE `s-maintain-money-admin` (
  `s-id` int(11) NOT NULL,
  `s-admin` varchar(100) NOT NULL,
  `s-connect` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s-r-buzzar`
--

CREATE TABLE `s-r-buzzar` (
  `bz-id` int(10) NOT NULL,
  `bz-title` varchar(55) NOT NULL,
  `bz-owner` varchar(30) NOT NULL,
  `bz-description` text NOT NULL,
  `bz-loction` varchar(50) NOT NULL,
  `bz-prize` varchar(10) NOT NULL,
  `bz-imge` text NOT NULL,
  `bz-date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deactive-bazz` enum('active','deactive') NOT NULL DEFAULT 'active',
  `intrested` varchar(25) DEFAULT NULL,
  `nego` varchar(15) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s-r-complain`
--

CREATE TABLE `s-r-complain` (
  `c-id` int(11) NOT NULL,
  `c-username` varchar(100) NOT NULL,
  `c-cmplain` varchar(255) NOT NULL,
  `c-post` varchar(255) NOT NULL,
  `member_time` datetime DEFAULT NULL,
  `complain_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s-r-event`
--

CREATE TABLE `s-r-event` (
  `ec_time` time NOT NULL,
  `ev_id` int(11) NOT NULL,
  `ev_name` varchar(30) NOT NULL,
  `ev_desc` text NOT NULL,
  `ev-image` text DEFAULT NULL,
  `ev_created` date NOT NULL,
  `ev_time` date NOT NULL,
  `sheduled` varchar(20) NOT NULL DEFAULT 'not scheduled',
  `file_type` varchar(25) DEFAULT NULL,
  `event_id` varchar(150) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s-r-event`
--

INSERT INTO `s-r-event` (`ec_time`, `ev_id`, `ev_name`, `ev_desc`, `ev-image`, `ev_created`, `ev_time`, `sheduled`, `file_type`, `event_id`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
('13:00:00', 1, 'Guru Nanak jayanti', 'organizing lunger on guru nanak jayanti', '', '2018-03-03', '2018-03-16', 'not scheduled\r\n', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('12:00:00', 2, 'Ganesh Jayanti', 'Organize Mahaprasad in society Temple', '27629070_1342792792532782_1577652443623088885_o.jpg', '2018-03-03', '2018-03-22', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('23:40:00', 10, 'email testing', 'email is woeking', '', '2018-05-18', '2018-05-24', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('23:00:00', 4, 'Holi', 'Holika Dahan in Society', 'cloth1.jpg', '2018-03-03', '2018-03-27', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('20:00:00', 5, 'Navratri', 'Dandiya in Society', 'cloth3.jpg', '2018-03-03', '2018-03-31', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('22:10:00', 6, 'sou', 'sou', '', '2018-03-22', '2018-12-20', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('11:58:00', 7, 'event non schedule', 'ehjhsdfhshf', '', '2018-04-11', '2018-04-30', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 53, 'New event', 'Bzbbsnsb sjsjsj sbshsh', NULL, '2019-01-22', '2019-01-30', 'not scheduled', NULL, 'event-6b4l', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('04:00:00', 9, 'Celebartion of Labour Day', 'To Celebarate International Labour Day in Society Premiese', '', '2018-04-19', '2018-05-01', 'not scheduled', '', '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 37, 'New from web pannel', 'HsnnejdDiduduDiidjdjdOeieieiri', NULL, '2019-01-21', '1970-01-01', 'not scheduled', NULL, 'event-dh37', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 14, 'Dhjdb', 'Hdhdnsb', NULL, '2019-01-18', '2019-01-30', 'not scheduled', NULL, 'event-87eo', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 15, 'Pdfevent', 'Gshdbddb', '1547814487723.pdf', '2019-01-18', '2019-01-23', 'not scheduled', 'pdf', 'event-opqw', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 16, 'Pdfevent', 'Gshdbddb', '1547814487723.pdf', '2019-01-18', '2019-01-23', 'not scheduled', 'pdf', 'event-opqw', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 48, 'not working', 'asdsdskjkkm', NULL, '2019-01-22', '2019-01-24', 'not scheduled', NULL, 'event-dtnw', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 49, 'sadsafdsa', 'sdsadsadsa', NULL, '2019-01-22', '2019-01-24', 'not scheduled', NULL, 'event-w685', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 50, 'sadsad', 'sdsadsa', NULL, '2019-01-22', '2019-01-24', 'not scheduled', NULL, 'event-5u8r', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 44, 'new society event which update', 'this is new event from society by admin', NULL, '2019-01-21', '2019-01-26', 'not scheduled', NULL, 'event-xhcq', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 45, 'Akshay Kumar and update jan', 'Akshay Kumar and I have been working on the same thing for the last two years and I have been working on the same f and I have been working on the project management role since the first time I had the opportunity to work with you on your recent work', '1548067765.pdf', '2019-01-22', '2019-01-26', 'not scheduled', 'pdf', 'event-9m82', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 25, 'Akshay Kumar and update done', 'Akshay Kumar and I have been working on the same thing for the last two years and I have been working on the same f and I have been working on the project management role since the first time I had the opportunity to work with you on your recent work', '1548065746686.jpg', '2019-01-21', '2019-01-26', 'not scheduled', 'image', 'event-7trw', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 52, 'sdsaddsfsdf', 'sdadsahkj', NULL, '2019-01-22', '2019-01-25', 'not scheduled', NULL, 'event-kdhl', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 56, 'my ekjwe', 'fwefwef', NULL, '2019-01-24', '2019-01-25', 'not scheduled', NULL, 'event-nchb', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 57, 'ewrwer', 'erewrwe', NULL, '2019-01-24', '2019-01-31', 'not scheduled', NULL, 'event-twkn', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 58, ' ', ' ', NULL, '2019-01-24', '2019-01-25', 'not scheduled', NULL, 'event-biwk', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 63, 'Republic day', 'Republic Day is coming from the future of fashion with orange and black changla of the other day I will send it back to where you can get the other side I am looking forward your website and it back in the other hand I will send the future of our cha', '1548429155471.jpg', '2019-01-25', '2019-01-26', 'not scheduled', 'image', 'event-zmce', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 62, 'almost not done by me', 'almost done by me', '1547878739.jpg', '2019-01-24', '2019-01-24', 'not scheduled', 'image', 'event-qbxa', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 64, 'This is pdf event', 'Bsbsbbs Shah of our games with the other side of our games with the future of fashion and beauty and the other side of our games with the future of our games with the future please unsubscribe here newsletter of fresh trends and black changla of the', '1548429292353.pdf', '2019-01-25', '2019-01-30', 'not scheduled', 'pdf', 'event-1f4q', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 65, 'This is without attachment eve', 'The future please unsubscribe with the other side of our games with the future of our games with the future of our chai ki evening and black changla vatel of our games with a hakya the future of fashion and I will send you a very nice and warm the fu', NULL, '2019-01-25', '2019-01-31', 'not scheduled', NULL, 'event-s7oc', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 66, 'New subscribe now to see if we', 'The morning of June so that we can do it in my email is strictly forbidden to the morning to get to see if you have a nice to see you then and now I have been working with you to the morning and then delete this communication in the morning of thFrom', '1549516387565.jpg', '2019-02-07', '2019-02-28', 'not scheduled', 'image', 'event-l89b', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 67, 'New pdf event', 'The morning and don\'t forget to do the same is pregnant women and I am not sure what you are waiting and delete the message to the morning and delete all its attachments and I am not a good day I am a week ending is pregnant with my email address and', '1549516638801.pdf', '2019-02-07', '2019-02-22', 'not scheduled', 'pdf', 'event-54fz', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 68, 'Bxb', 'Bzbzb', NULL, '2019-02-07', '2019-02-23', 'not scheduled', NULL, 'event-fhb1', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 69, 'Jdjd the morning of the year a', 'Hdhdnsb the morning of the morning of the year and', NULL, '2019-02-07', '2019-02-21', 'not scheduled', NULL, 'event-p916', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 70, 'Ydshdhshs', 'Hshshshsh\nShshshshs\nDhshwgdggehshshg', '1553075007505.jpg', '2019-03-20', '2019-03-22', 'not scheduled', 'image', 'event-pwk6', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 71, 'Test', 'Test', NULL, '2019-05-02', '2019-05-10', 'not scheduled', NULL, 'event-uoep', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 72, 'Hanuman Jayanti', 'It is compulsory for all', '1557481977673.jpg', '2019-05-10', '2019-05-23', 'not scheduled', 'image', 'event-2zex', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 73, 'CHS launch event', 'Make your society online with us', '1558324505223.jpg', '2019-05-20', '2019-06-28', 'not scheduled', 'image', 'event-r18x', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 74, 'CHS Vishva launch event', 'Make society smart', '1558350168790.jpg', '2019-05-20', '2019-05-23', 'not scheduled', 'image', 'event-we1d', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 75, 'Save water', 'Save water', '1558351134448.jpg', '2019-05-20', '2019-05-23', 'not scheduled', 'image', 'event-omiw', 'Inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 76, 'Save Water', 'Save Water', '1558414343281.jpg', '2019-05-21', '2019-05-24', 'not scheduled', 'image', 'event-dspm', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 77, 'Mansoon', 'Man\'s mansoon', NULL, '2019-06-14', '2019-06-28', 'not scheduled', NULL, 'event-79q5', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 78, 'TEST', 'TEST', NULL, '2019-07-15', '2020-02-10', 'not scheduled', NULL, 'event-2fqm', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 79, 'Test', 'Testing the flat is located the first one to comment on the flat is given to me that I have to get to the first one to comment I am a little more than happy to get a.', NULL, '2019-08-08', '2019-10-25', 'scheduled', NULL, 'event-vkjy', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 81, 'Test2', 'Second test ', NULL, '2019-10-22', '2019-04-08', 'not scheduled', NULL, 'event-vn54', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 82, 'holi', 'Holi - Holi is the festival of love or colors that signifies the victory of superior over immoral. Holi festival is commemorate on February end or starting March.', '1586514223.png', '2020-04-10', '2020-04-09', 'not scheduled', 'image', 'event-14uz', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 83, 'holi', 'Holi - Holi is the festival of love or colors that signifies the victory of superior over immoral. Holi festival is commemorate on February end or starting March.', '1586514229.png', '2020-04-10', '2020-04-09', 'not scheduled', 'image', 'event-albz', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('00:00:00', 84, 'Test', 'Test', NULL, '2020-05-08', '2020-04-30', 'not scheduled', NULL, 'event-tplh', '', 'Gokul-A-3-10', '2020-05-08 00:51:05', '', '0000-00-00 00:00:00'),
('00:00:00', 85, 'Test', 'Test', NULL, '2020-05-08', '2020-05-22', 'not scheduled', NULL, 'event-h7w6', '', 'Gokul-A-3-10', '2020-05-08 00:52:22', '', '0000-00-00 00:00:00'),
('00:00:00', 86, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-v7n1', '', 'Gokul-A-3-10', '2020-05-08 01:01:39', '', '0000-00-00 00:00:00'),
('00:00:00', 87, 'Test', 'Test', NULL, '2020-05-08', '2020-05-22', 'not scheduled', NULL, 'event-pkju', '', 'Gokul-A-3-10', '2020-05-08 01:05:35', '', '0000-00-00 00:00:00'),
('00:00:00', 88, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-tsu6', '', 'Gokul-A-3-10', '2020-05-08 01:07:47', '', '0000-00-00 00:00:00'),
('00:00:00', 89, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-1csl', '', 'Gokul-A-3-10', '2020-05-08 01:23:25', '', '0000-00-00 00:00:00'),
('00:00:00', 90, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-mfj3', '', 'Gokul-A-3-10', '2020-05-08 01:24:13', '', '0000-00-00 00:00:00'),
('00:00:00', 91, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-1qfp', '', 'Gokul-A-3-10', '2020-05-08 01:25:30', '', '0000-00-00 00:00:00'),
('00:00:00', 92, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-hd8u', '', 'Gokul-A-3-10', '2020-05-08 01:26:39', '', '0000-00-00 00:00:00'),
('00:00:00', 93, 'Test', 'Test', NULL, '2020-05-08', '2020-05-15', 'not scheduled', NULL, 'event-ry7v', '', 'Gokul-A-3-10', '2020-05-08 01:29:18', '', '0000-00-00 00:00:00'),
('00:00:00', 94, 'Test', 'Test', NULL, '2020-05-08', '2020-05-22', 'not scheduled', NULL, 'event-152o', '', 'Gokul-A-3-10', '2020-05-08 01:30:04', '', '0000-00-00 00:00:00'),
('00:00:00', 95, 'Test', 'Test', NULL, '2020-05-08', '2020-05-09', 'not scheduled', NULL, 'event-2br3', '', 'Gokul-A-3-10', '2020-05-08 01:31:33', '', '0000-00-00 00:00:00'),
('00:00:00', 96, 'Test', 'Test', NULL, '2020-05-08', '2020-05-16', 'not scheduled', NULL, 'event-9ypv', '', 'Gokul-A-3-10', '2020-05-08 01:32:26', '', '0000-00-00 00:00:00'),
('00:00:00', 97, 'Test', 'Test', NULL, '2020-05-08', '2020-05-16', 'not scheduled', NULL, 'event-mhx7', '', 'Gokul-A-3-10', '2020-05-08 01:33:42', '', '0000-00-00 00:00:00'),
('00:00:00', 98, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-kwc9', '', 'Gokul-A-3-10', '2020-05-08 01:34:10', '', '0000-00-00 00:00:00'),
('00:00:00', 99, 'Test', 'Test', NULL, '2020-05-08', '2020-05-09', 'not scheduled', NULL, 'event-8rzc', '', 'Gokul-A-3-10', '2020-05-08 01:34:38', '', '0000-00-00 00:00:00'),
('00:00:00', 100, 'Test', 'Test', NULL, '2020-05-08', '2020-05-09', 'not scheduled', NULL, 'event-zmh6', '', 'Gokul-A-3-10', '2020-05-08 01:35:04', '', '0000-00-00 00:00:00'),
('00:00:00', 101, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-9gdp', '', 'Gokul-A-3-10', '2020-05-08 01:37:08', '', '0000-00-00 00:00:00'),
('00:00:00', 102, 'Test', 'Test', NULL, '2020-05-08', '2020-05-29', 'not scheduled', NULL, 'event-bqos', '', 'Gokul-A-3-10', '2020-05-08 01:52:20', '', '0000-00-00 00:00:00'),
('00:00:00', 103, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-agrb', '', 'Gokul-A-3-10', '2020-05-08 01:53:48', '', '0000-00-00 00:00:00'),
('00:00:00', 104, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-xrd2', '', 'Gokul-A-3-10', '2020-05-08 01:54:48', '', '0000-00-00 00:00:00'),
('00:00:00', 105, 'Test', 'Test', NULL, '2020-05-08', '2020-05-22', 'not scheduled', NULL, 'event-ozfq', '', 'Gokul-A-3-10', '2020-05-08 01:58:12', '', '0000-00-00 00:00:00'),
('00:00:00', 106, 'Test', 'Test', NULL, '2020-05-08', '2020-05-16', 'not scheduled', NULL, 'event-ihc6', '', 'Gokul-A-3-10', '2020-05-08 02:00:10', '', '0000-00-00 00:00:00'),
('00:00:00', 107, 'Test', 'Test', NULL, '2020-05-08', '2020-05-08', 'not scheduled', NULL, 'event-3ier', '', 'Gokul-A-3-10', '2020-05-08 02:03:39', '', '0000-00-00 00:00:00'),
('00:00:00', 108, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-dwnj', '', 'Gokul-A-3-10', '2020-05-08 02:07:29', '', '0000-00-00 00:00:00'),
('00:00:00', 109, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-vps1', '', 'Gokul-A-3-10', '2020-05-08 02:08:18', '', '0000-00-00 00:00:00'),
('00:00:00', 110, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-fgcv', '', 'Gokul-A-3-10', '2020-05-08 02:08:59', '', '0000-00-00 00:00:00'),
('00:00:00', 111, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-8agc', '', 'Gokul-A-3-10', '2020-05-08 02:10:13', '', '0000-00-00 00:00:00'),
('00:00:00', 112, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-skm1', '', 'Gokul-A-3-10', '2020-05-08 02:11:51', '', '0000-00-00 00:00:00'),
('00:00:00', 113, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-dtva', '', 'Gokul-A-3-10', '2020-05-08 02:13:37', '', '0000-00-00 00:00:00'),
('00:00:00', 114, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-n3o4', '', 'Gokul-A-3-10', '2020-05-08 02:15:46', '', '0000-00-00 00:00:00'),
('00:00:00', 115, 'Test', 'Test', NULL, '2020-05-08', '2020-05-22', 'not scheduled', NULL, 'event-hmsi', 'active', 'Gokul-A-3-10', '2020-05-08 02:17:00', '', '0000-00-00 00:00:00'),
('00:00:00', 116, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-twk6', 'active', 'Gokul-A-3-10', '2020-05-08 02:18:53', '', '0000-00-00 00:00:00'),
('00:00:00', 117, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-x6v5', 'active', 'Gokul-A-3-10', '2020-05-08 02:19:27', '', '0000-00-00 00:00:00'),
('00:00:00', 118, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-apb1', 'active', 'Gokul-A-3-10', '2020-05-08 02:24:55', '', '0000-00-00 00:00:00'),
('00:00:00', 119, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-hb52', 'Inactive', 'Gokul-A-3-10', '2020-05-08 02:25:39', 'Gokul-A-3-10', '2020-05-08 02:49:30'),
('00:00:00', 120, 'Test', 'Test', NULL, '2020-05-08', '2020-05-30', 'not scheduled', NULL, 'event-1jyf', 'Inactive', 'Gokul-A-3-10', '2020-05-08 02:26:52', 'Gokul-A-3-10', '2020-05-08 02:49:26'),
('00:00:00', 121, 'Test', 'Test', NULL, '2020-05-08', '2020-05-23', 'not scheduled', NULL, 'event-iefs', 'Inactive', 'Gokul-A-3-10', '2020-05-08 02:27:24', 'Gokul-A-3-10', '2020-05-08 02:49:07'),
('00:00:00', 122, 'Test', 'Test', '1588886846.png', '2020-05-08', '2020-05-30', 'not scheduled', 'image', 'event-vzlo', 'Inactive', 'Gokul-A-3-10', '2020-05-08 02:57:26', 'Gokul-A-3-10', '2020-05-08 02:58:03'),
('00:00:00', 124, 'Test123', 'Test', NULL, '2020-05-11', '2020-05-22', 'not scheduled', NULL, 'event-ojv4', '', 'Gokul-A-3-10', '2020-05-11 12:15:09', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `s-r-reply`
--

CREATE TABLE `s-r-reply` (
  `po-id` int(11) NOT NULL,
  `rp-id` int(11) NOT NULL,
  `s-r-username` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `s-r-user`
--

CREATE TABLE `s-r-user` (
  `s-r-id` int(11) NOT NULL,
  `s-r-fname` varchar(20) NOT NULL,
  `s-r-lname` varchar(20) NOT NULL,
  `s-r-username` varchar(30) NOT NULL,
  `s-r-email` varchar(60) NOT NULL,
  `s-r-password` varchar(255) NOT NULL,
  `s-r-mobile` varchar(11) NOT NULL,
  `s-r-active` enum('active','inactive') NOT NULL DEFAULT 'active',
  `s-r-onetime` varchar(10) NOT NULL,
  `s-r-confirm` enum('confirm','notconfirm') NOT NULL DEFAULT 'confirm',
  `s-r-approve` varchar(20) NOT NULL DEFAULT 'notapprove',
  `s-r-appartment` varchar(100) NOT NULL,
  `s-r-wing` varchar(10) NOT NULL,
  `s-r-floor` varchar(10) NOT NULL,
  `s-r-flat` varchar(10) NOT NULL,
  `s-r-age` varchar(5) NOT NULL,
  `s-r-blood` varchar(5) NOT NULL,
  `s-r-bod` varchar(10) NOT NULL,
  `s-r-gender` varchar(10) NOT NULL,
  `s-r-senior-citizen` varchar(13) NOT NULL,
  `s-r-flat-occupy` varchar(20) NOT NULL,
  `s-r-flat-area` varchar(10) NOT NULL,
  `s-r-hobby` varchar(20) NOT NULL,
  `s-r-language` varchar(20) NOT NULL,
  `s-r-ocuupation` varchar(200) NOT NULL,
  `s-r-owner-name` varchar(30) NOT NULL DEFAULT 'owner',
  `rent-or-owner` varchar(20) NOT NULL,
  `no-of-vehicle` varchar(5) NOT NULL,
  `no-of-family-member` varchar(25) NOT NULL,
  `s-r-profile` text NOT NULL,
  `s-r-type` varchar(25) NOT NULL DEFAULT 'All',
  `bazzar-no` int(50) NOT NULL DEFAULT 0,
  `notice-no` int(50) NOT NULL DEFAULT 0,
  `polls-no` int(50) NOT NULL DEFAULT 0,
  `activity-no` int(50) NOT NULL DEFAULT 0,
  `complian-no` int(50) NOT NULL DEFAULT 0,
  `album-no` int(50) NOT NULL DEFAULT 0,
  `directory-no` int(50) NOT NULL DEFAULT 0,
  `forum-no` int(50) NOT NULL DEFAULT 0,
  `meeting_no` int(10) NOT NULL DEFAULT 0,
  `booking_no` int(11) NOT NULL DEFAULT 0,
  `reject` varchar(20) NOT NULL DEFAULT 'Not Reject',
  `reject_reason` varchar(100) NOT NULL,
  `inactive_reason` varchar(100) NOT NULL,
  `active_reason` varchar(100) NOT NULL,
  `disabled_user` varchar(100) NOT NULL,
  `disabled_reason` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s-r-user`
--

INSERT INTO `s-r-user` (`s-r-id`, `s-r-fname`, `s-r-lname`, `s-r-username`, `s-r-email`, `s-r-password`, `s-r-mobile`, `s-r-active`, `s-r-onetime`, `s-r-confirm`, `s-r-approve`, `s-r-appartment`, `s-r-wing`, `s-r-floor`, `s-r-flat`, `s-r-age`, `s-r-blood`, `s-r-bod`, `s-r-gender`, `s-r-senior-citizen`, `s-r-flat-occupy`, `s-r-flat-area`, `s-r-hobby`, `s-r-language`, `s-r-ocuupation`, `s-r-owner-name`, `rent-or-owner`, `no-of-vehicle`, `no-of-family-member`, `s-r-profile`, `s-r-type`, `bazzar-no`, `notice-no`, `polls-no`, `activity-no`, `complian-no`, `album-no`, `directory-no`, `forum-no`, `meeting_no`, `booking_no`, `reject`, `reject_reason`, `inactive_reason`, `active_reason`, `disabled_user`, `disabled_reason`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Rahul', 'Waghmare', 'Gokul-A-3-10', 'saurabh@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9773254783', 'active', '945318', 'confirm', 'approve', 'Gokul', 'A', '3', '10', '26', 'A+', '27-10-2017', 'Male', '1', '450', '500', 'cricket', 'Marathi', 'Business', 'owner', 'Rent', '2', 'Prakash', '1564737985896.jpg', 'admin', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', 'Active', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(2, 'suresh', 'Jani', 'Gokul-A-0-1', 'suresh_jani@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9920478558', 'inactive', '', 'confirm', 'approve', 'Gokul', 'A', '0', '1', '', 'A+', '25-02-1955', 'Male', '', '450', '500', '', 'Hindi', 'Business', 'owner', 'Rent', '', 'Ramesh', '1564571884681.jpg', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', 'disabled', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(3, 'jatin', 'todi', 'Gokul-A-0-2', 'jatin_todi@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9668521036', 'inactive', '', 'confirm', 'approve', 'Gokul', 'A', '0', '2', '', 'AB+', '17-05-1990', 'Male', '', '', '', '', '', 'Service-Rolta pvlt', 'owner', '', '', 'P', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(4, 'sreeraj', 'nair', 'Gokul-A-0-3', 'sree_nair@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9545687512', 'inactive', '', 'confirm', 'approve', 'Gokul', 'A', '0', '3', '', 'B+', '17-08-1972', 'Male', '', '450', '500', '', '', 'Service-vara utd', 'owner', '', '', 'S', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(5, 'roma', 'dsoza', 'Gokul-A-1-4', 'dynamicvishvateam@gmail.com', 'c3fb37909d398f646387bef207be49b4', '8655352098', 'active', '', 'confirm', 'approve', 'Gokul', 'A', '1', '4', '', 'O+', '25-08-1985', 'Male', '', '450', '500', '', 'Marathi', 'Teacher', 'owner', '', '', 'M', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(6, 'dynamic', 'Vishva', 'Gokul-A-1-5', 'saurabh@gmail.com', 'c3fb37909d398f646387bef207be49b4', '8104241208', 'active', '839145', 'confirm', 'approve', 'Gokul', 'A', '1', '5', '', 'O+', '25-08-1985', 'Male', '', '450', '500', '', 'Marathi', 'Business', 'owner', '', '', 'Jarden', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', 'inactive this person', 'active this person', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(7, 'swaroop', 'kamble', 'Gokul-A-1-6', 'swaroop_kamble@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9478526910', 'inactive', '', 'confirm', 'approve', 'Gokul', 'A', '1', '6', '', 'A+', '21-06-1985', 'Male', '', '450', '500', '', '', 'Home decore', 'owner', '', '', 'O', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', 'disabled', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(8, 'rohan', 'chalke', 'Gokul-A-2-7', 'chalke.r@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9820045612', 'inactive', '', 'confirm', 'approve', 'Gokul', 'A', '2', '7', '', 'B+', '28-01-1969', 'Male', '', '450', '500', '', '', 'Doctor - lilavati hospital', 'owner', '', '', 'krishna', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', 'disabled', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(9, 'Purushottam', 'Thite', 'Gokul-A-2-8', 'purushottamthite2690@gmail.com', 'c3fb37909d398f646387bef207be49b4', '8879299537', 'active', '139547', 'confirm', 'approve', 'Gokul', 'A', '2', '8', '', 'A-', '14-07-1978', 'Male', '', '450', '500', '', 'Marathi', 'Business', 'owner', '', '', 'B', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(10, 'sailesh', 'dhaman', 'Gokul-A-2-9', 'roshanbajag@gmail.com', 'c3fb37909d398f646387bef207be49b4', '987045123', 'active', '', 'confirm', 'approve', 'Gokul', 'A', '2', '9', '', 'AB+', '18-09-1977', 'Male', '', '450', '500', '', '', 'Buisness-Sailesh Engineering', 'owner', '', '', 'K', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(11, 'Sachin', 'Tendulkar', 'Gokul-B-0-1', 'dynamicvishvateam1@gmail.com', 'c3fb37909d398f646387bef207be49b4', '8879504125', 'inactive', '', 'confirm', 'approve', 'Gokul', 'B', '0', '1', '', 'B+', '20-06-1984', 'Male', '', '350', '400', '', '', 'Criket-MCA', 'owner', '', '', 'R', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(12, 'Jhethalal', 'Gada', 'Gokul-B-0-2', 'jhethag@ygmail.com', 'c3fb37909d398f646387bef207be49b4', '9820341452', 'active', '', 'confirm', 'approve', 'Gokul', 'B', '0', '2', '', 'A-', '10-03-1979', 'Male', '', '350', '400', '', '', 'Business-Gada Electronics', 'owner', '', '', 'Champaklal', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(13, 'Athmaram', 'Bhide', 'Gokul-B-0-3', 'bhide_ram@ygmail.com', 'c3fb37909d398f646387bef207be49b4', '9821748596', 'inactive', '', 'confirm', 'approve', 'Gokul', 'B', '0', '3', '', 'O-', '02-10-1960', 'Male', '', '350', '400', '', '', 'Teacher-Bhide Classes', 'owner', '', '', 'm', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(14, 'Rajendra', 'Khot', 'Gokul-B-1-4', 'rajendra@rediffmail.com', 'c3fb37909d398f646387bef207be49b4', '9725648955', 'active', '', 'confirm', 'approve', 'Gokul', 'B', '1', '4', '16-6-', 'A+', '', 'male', '', '350', '400', '', '', 'service-CBI', 'owner', '', '', 's', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(15, 'Sunil', 'Kadam', 'Gokul-B-1-5', 'sunil@yahoo.com', 'c3fb37909d398f646387bef207be49b4', '9920085632', 'active', '', 'confirm', 'approve', 'Gokul', 'B', '1', '5', '22-12', 'AB+', '', 'male', '', '350', '400', '', '', 'service-Rajiv pvt ltd', 'owner', '', '', 'r', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(16, 'Divesh', 'Ghadge', 'Gokul-B-1-6', 'purushottamthite2690@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9875851223', 'inactive', '', 'confirm', 'approve', 'Gokul', 'B', '1', '6', '13-06', 'O-', '', 'male', '', '350', '400', '', '', 'service-LIC of India', 'owner', '', '', 'k', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', 'disabled', 'we wannt disabled', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(17, 'Aniket', 'Jagtap', 'Gokul-B-2-7', 'aniket_sunil@jc.com', 'c3fb37909d398f646387bef207be49b4', '9920563855', 'inactive', '', 'confirm', 'approve', 'Gokul', 'B', '2', '7', '', 'O+', '22-12-1987', 'Male', '', '350', '400', '', '', 'Business-JC Homes', 'owner', '', '', 'Sunil', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(18, 'Sangram', 'Desai', 'Gokul-B-2-8', 'sangram@fortis.com', 'c3fb37909d398f646387bef207be49b4', '9768145263', 'active', '', 'confirm', 'approve', 'Gokul', 'B', '2', '8', '', 'AB+', '07-09-1990', 'Male', '', '350', '400', '', '', 'Docter-Fortis Hospital mulund', 'owner', '', '', 'Anil', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38'),
(19, 'Shreeja', 'Bagwe', 'Gokul-B-2-9', 'shreeja@gmail.com', 'c3fb37909d398f646387bef207be49b4', '9820157855', 'inactive', '', 'confirm', 'approve', 'Gokul', 'B', '2', '9', '', 'B+', '12-06-1990', 'female', '', '350', '400', '', '', 'Teacher-our lady highschool', 'owner', '', '', 'Shrinath', '', 'All', 0, 54, 0, 8, 48, 7, 0, 47, 48, 0, 'Not Reject', '', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 12:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `schedular`
--

CREATE TABLE `schedular` (
  `sr_id` int(11) NOT NULL,
  `schedular_for` varchar(150) NOT NULL,
  `schedular_fid` varchar(100) NOT NULL,
  `schedular_date` date NOT NULL,
  `schedular_creation` datetime NOT NULL,
  `schedular_close` varchar(12) NOT NULL DEFAULT 'open',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedular`
--

INSERT INTO `schedular` (`sr_id`, `schedular_for`, `schedular_fid`, `schedular_date`, `schedular_creation`, `schedular_close`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(6, 'Notice', '900278627', '2018-04-11', '2019-03-20 10:57:00', 'closed', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Notice', '900278627', '2018-04-20', '2019-03-20 10:00:00', 'closed', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Notice', 'notice-wmxk', '2019-03-20', '2019-03-20 03:12:57', 'close', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'Event', 'event-pwk6', '2019-03-20', '2019-03-20 03:14:03', 'close', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Event', 'event-uoep', '2019-05-05', '2019-05-02 12:36:56', 'close', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Notice', 'notice-jpng', '2019-06-21', '2019-06-14 11:57:59', 'close', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Notice', 'notice-nk28', '2019-06-30', '2019-06-28 11:55:49', 'close', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'Event', 'event-vkjy', '2019-10-23', '2019-08-08 01:44:26', 'closed', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Maintenance bill', '', '2019-02-12', '0000-00-00 00:00:00', 'open', '', 'Gokul-A-3-10', '2020-05-09 22:35:27', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `share_master`
--

CREATE TABLE `share_master` (
  `sr_id` int(11) NOT NULL,
  `date_allot_share` date NOT NULL,
  `cashbook_follo` varchar(100) NOT NULL,
  `share_certi_no` varchar(100) NOT NULL,
  `no_share` varchar(100) NOT NULL,
  `name_member` varchar(100) NOT NULL,
  `date_of_trf` date NOT NULL,
  `cashbook_jr_follow` varchar(100) NOT NULL,
  `share_cert_no` varchar(100) NOT NULL,
  `share_value_tr` varchar(100) NOT NULL,
  `name_transfer` varchar(100) NOT NULL,
  `auth_sign` varchar(100) NOT NULL,
  `Remark` text NOT NULL,
  `share_value` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sms_account`
--

CREATE TABLE `sms_account` (
  `srid` int(11) NOT NULL,
  `Mail_count` varchar(25) NOT NULL,
  `sms_count` varchar(25) NOT NULL,
  `date_creation` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway`
--

CREATE TABLE `sms_gateway` (
  `sr_id` int(12) NOT NULL,
  `api_key` varchar(250) NOT NULL,
  `sender_name` varchar(6) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'Active',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_account`
--

CREATE TABLE `society_account` (
  `sa_id` int(11) NOT NULL,
  `expected_maintenance` int(11) NOT NULL DEFAULT 0,
  `recieved_maintenance` int(11) NOT NULL DEFAULT 0,
  `Total_income` int(11) NOT NULL DEFAULT 0,
  `total_Expenses` int(11) NOT NULL DEFAULT 0,
  `Monthly_balance` int(11) NOT NULL DEFAULT 0,
  `Month` varchar(10) DEFAULT NULL,
  `year` varchar(5) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_address_master`
--

CREATE TABLE `society_address_master` (
  `soc_id` varchar(30) NOT NULL,
  `soc_add_id` varchar(35) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `address3` varchar(100) NOT NULL,
  `address4` varchar(100) NOT NULL,
  `taluka` varchar(50) NOT NULL,
  `district` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `pincode` int(6) NOT NULL,
  `country` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_assets`
--

CREATE TABLE `society_assets` (
  `sr_id` int(10) NOT NULL,
  `asset_id` varchar(100) NOT NULL,
  `asset_name` varchar(100) NOT NULL,
  `asset_descr` varchar(500) NOT NULL,
  `asset_pic` varchar(255) NOT NULL,
  `asset_cost` varchar(10) NOT NULL,
  `asset_date` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_bank_master`
--

CREATE TABLE `society_bank_master` (
  `bank_id` varchar(40) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_branch` varchar(30) NOT NULL,
  `bank_micr` varchar(20) NOT NULL,
  `bank_ifsc` varchar(20) NOT NULL,
  `bank_account_number` bigint(20) NOT NULL,
  `bank_address` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_brodcast_tbl`
--

CREATE TABLE `society_brodcast_tbl` (
  `sr_id` int(12) NOT NULL,
  `recipents_list` text NOT NULL,
  `message` varchar(160) NOT NULL,
  `brodcast_type` varchar(20) NOT NULL,
  `adminId` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_log`
--

CREATE TABLE `society_log` (
  `sr_id` int(12) NOT NULL,
  `logModule` varchar(50) NOT NULL,
  `logDescription` varchar(255) NOT NULL,
  `logTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `society_log`
--

INSERT INTO `society_log` (`sr_id`, `logModule`, `logDescription`, `logTime`, `createdBy`, `createdDatetime`) VALUES
(1, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:21:39'),
(2, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:38:39'),
(3, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:38:53'),
(4, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:40:36'),
(5, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:42:48'),
(6, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:43:30'),
(7, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:43:45'),
(8, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:44:16'),
(9, 'LOGIN', 'Access denied! User not active', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:50:40'),
(10, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:52:37'),
(11, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-23 21:55:05'),
(12, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-23 21:58:31'),
(13, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-23 21:59:54'),
(14, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', '8888888888', '2020-04-27 11:53:02'),
(15, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-27 11:53:09'),
(16, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-27 11:56:28'),
(17, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', '8888888888', '2020-04-27 14:22:13'),
(18, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', '8888888888', '2020-04-27 14:22:21'),
(19, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-27 14:22:48'),
(20, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 10:43:13'),
(21, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 11:00:30'),
(22, 'LOGIN', 'Access denied! User not active', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-28 16:07:55'),
(23, 'LOGIN', 'Access denied! User not active', '2016-08-02 18:30:00', 'Saurabh@gmail.com', '2020-04-28 16:08:03'),
(24, 'LOGIN', 'Successfully Logged In', '2016-10-08 18:30:00', 'Gokul-A-3-10', '2020-04-28 16:10:09'),
(25, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2016-10-10 18:30:00', 'Gokul-A-3-10', '2020-04-28 16:10:11'),
(26, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2016-10-30 18:30:00', 'Gokul-A-3-10', '2020-04-28 16:10:31'),
(27, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:51:58'),
(28, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:56:15'),
(29, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:57:14'),
(30, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:57:44'),
(31, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:59:22'),
(32, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 16:59:27'),
(33, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 17:00:39'),
(34, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 17:00:43'),
(35, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2017-04-02 18:30:00', 'Gokul-A-3-10', '2020-04-28 17:04:03'),
(36, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 17:08:51'),
(37, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2017-11-24 18:30:00', 'Gokul-A-3-10', '2020-04-28 17:11:25'),
(38, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 17:11:56'),
(39, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:21:47'),
(40, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:36:02'),
(41, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:36:49'),
(42, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:38:17'),
(43, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:38:20'),
(44, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:38:38'),
(45, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:39:18'),
(46, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:39:20'),
(47, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:39:37'),
(48, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:39:45'),
(49, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:39:47'),
(50, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:40:13'),
(51, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:40:21'),
(52, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:40:23'),
(53, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:40:30'),
(54, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:57:02'),
(55, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:58:51'),
(56, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:58:53'),
(57, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:58:58'),
(58, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:59:53'),
(59, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 18:59:55'),
(60, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2019-01-03 18:30:00', 'Gokul-A-3-10', '2020-04-28 19:01:04'),
(61, 'MANAGING_COMMITEE', 'Successfully Added', '2019-01-17 18:30:00', 'Gokul-A-3-10', '2020-04-28 19:01:18'),
(62, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2019-01-18 18:30:00', 'Gokul-A-3-10', '2020-04-28 19:01:19'),
(63, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:01:58'),
(64, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:02:31'),
(65, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:02:32'),
(66, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:03:37'),
(67, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:03:42'),
(68, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:03:44'),
(69, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:06:54'),
(70, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:06:57'),
(71, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:06:59'),
(72, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:11:51'),
(73, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:11:56'),
(74, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:11:56'),
(75, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:13:56'),
(76, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:14:18'),
(77, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:14:18'),
(78, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-28 19:14:51'),
(79, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-28 19:14:57'),
(80, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-28 19:15:05'),
(81, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-04-28 19:16:53'),
(82, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-04-28 19:17:04'),
(83, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:19:21'),
(84, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:19:23'),
(85, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:19:33'),
(86, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:21:28'),
(87, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:50'),
(88, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:54'),
(89, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:55'),
(90, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:58'),
(91, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:49:58'),
(92, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:50:15'),
(93, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:50:15'),
(94, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-28 19:50:32'),
(95, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 15:56:13'),
(96, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 15:56:15'),
(97, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 15:57:01'),
(98, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 15:58:41'),
(99, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:00:52'),
(100, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '2016-01-30 18:30:00', 'Gokul-A-3-10', '2020-04-29 16:01:31'),
(101, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:38:03'),
(102, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:38:12'),
(103, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:38:51'),
(104, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:39:00'),
(105, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 16:41:14'),
(106, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:14:50'),
(107, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:14:54'),
(108, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:17:07'),
(109, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:17:34'),
(110, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:18:21'),
(111, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:18:21'),
(112, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:20:17'),
(113, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:21:28'),
(114, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:21:37'),
(115, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:21:37'),
(116, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:22:29'),
(117, 'GOVERNMENT_BODY', 'Government Body Has Removed Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:22:33'),
(118, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:22:33'),
(119, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:23:32'),
(120, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:23:32'),
(121, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:23:47'),
(122, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:23:47'),
(123, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:24:07'),
(124, 'GOVERNMENT_BODY', 'Government Body Added Successfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:24:18'),
(125, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:24:18'),
(126, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:25:32'),
(127, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:26:13'),
(128, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:26:29'),
(129, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:26:33'),
(130, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:27:27'),
(131, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-29 18:27:40'),
(132, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-30 12:05:53'),
(133, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-04-30 13:26:03'),
(134, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-01 08:08:51'),
(135, 'Forget Password', 'INVALID One time Password', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 08:49:18'),
(136, 'Forget Password', 'INVALID One time Password', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 08:50:13'),
(137, 'Forget Password', 'INVALID One time Password', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:31:52'),
(138, 'Forget Password', 'INVALID One time Password', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:34:11'),
(139, 'Forget Password', 'Password Changed Successfully', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:38:28'),
(140, 'Forget Password', 'Password Changed Successfully', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:38:56'),
(141, 'Forget Password', 'Password Changed Successfully', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:39:37'),
(142, 'Forget Password', 'Password Changed Successfully', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-01 09:40:46'),
(143, 'LOGIN', 'Unauthorize, Password doesn\'t match!!!', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-02 11:11:52'),
(144, 'LOGIN', 'Unauthorize, Password doesn\'t match!!!', '2011-12-07 18:30:00', 'Saurabh@gmail.com', '2020-05-02 11:12:08'),
(145, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 11:14:08'),
(146, 'LOGIN', 'Successfully Logged In', '2016-02-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:02:07'),
(147, 'POLL', 'Fetch Poll home page data', '2016-02-10 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:02:11'),
(148, 'POLL', 'Fetch Poll home page data', '2016-03-23 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:03:24'),
(149, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:03:32'),
(150, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:03:44'),
(151, 'POLL', 'Fetch Poll home page data', '2016-07-07 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:07:08'),
(152, 'POLL', 'Fetch Poll home page data', '2016-07-09 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:07:10'),
(153, 'POLL', 'Fetch Poll home page data', '2016-07-09 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:07:10'),
(154, 'POLL', 'Fetch Poll home page data', '2016-07-09 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:07:10'),
(155, 'POLL', 'Fetch Poll home page data', '2016-07-10 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:07:11'),
(156, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:07:48'),
(157, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:07:49'),
(158, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:07:50'),
(159, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:07:50'),
(160, 'POLL', 'Fetch Poll home page data', '2016-08-04 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:05'),
(161, 'POLL', 'Fetch Poll home page data', '2016-08-05 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:06'),
(162, 'POLL', 'Fetch Poll home page data', '2016-08-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:07'),
(163, 'POLL', 'Fetch Poll home page data', '2016-08-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:07'),
(164, 'POLL', 'Fetch Poll home page data', '2016-08-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:07'),
(165, 'POLL', 'Fetch Poll home page data', '2016-08-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:07'),
(166, 'POLL', 'Fetch Poll home page data', '2016-08-07 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:08:08'),
(167, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:08:35'),
(168, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:08:35'),
(169, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:08:36'),
(170, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:08:36'),
(171, 'POLL', 'Fetch Poll home page data', '2016-10-04 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:10:05'),
(172, 'POLL', 'Fetch Poll home page data', '2016-12-30 18:30:00', 'Gokul-A-3-10', '2020-05-02 16:12:31'),
(173, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:12:57'),
(174, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:13:26'),
(175, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:14:37'),
(176, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:22:28'),
(177, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:23:23'),
(178, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:24:35'),
(179, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:24:50'),
(180, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:25:42'),
(181, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:26:18'),
(182, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:27:42'),
(183, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:28:28'),
(184, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:29:05'),
(185, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:29:41'),
(186, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:33:03'),
(187, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:33:24'),
(188, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:37:36'),
(189, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:37:59'),
(190, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:38:35'),
(191, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:38:43'),
(192, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:41:34'),
(193, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:54:03'),
(194, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:54:17'),
(195, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:55:36'),
(196, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:55:42'),
(197, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:56:30'),
(198, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:58:08'),
(199, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:58:25'),
(200, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:58:38'),
(201, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:59:16'),
(202, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 16:59:30'),
(203, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:00:45'),
(204, 'POLL', 'Fetch Poll home page data', '2017-01-25 18:30:00', 'Gokul-A-3-10', '2020-05-02 17:01:26'),
(205, 'POLL', 'Fetch Poll home page data', '2017-02-04 18:30:00', 'Gokul-A-3-10', '2020-05-02 17:02:05'),
(206, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:02:57'),
(207, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:03:34'),
(208, 'POLL', 'Fetch Poll home page data', '2017-08-27 18:30:00', 'Gokul-A-3-10', '2020-05-02 17:08:28'),
(209, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:14:40'),
(210, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:15:39'),
(211, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:16:02'),
(212, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:16:22'),
(213, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:17:29'),
(214, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:17:49'),
(215, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:18:54'),
(216, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:19:31'),
(217, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:20:27'),
(218, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:20:43'),
(219, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:20:57'),
(220, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:22:30'),
(221, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:26:41'),
(222, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:33:14'),
(223, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:33:38'),
(224, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:34:10'),
(225, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:38:19'),
(226, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:39:06'),
(227, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:42:07'),
(228, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:43:45'),
(229, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:43:47'),
(230, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(231, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(232, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(233, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(234, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(235, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(236, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:48'),
(237, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:44:49'),
(238, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:45:34'),
(239, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:45:53'),
(240, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:47:07'),
(241, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:47:08'),
(242, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:48:40'),
(243, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:56:41'),
(244, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 17:57:00'),
(245, 'POLL', 'Fetch Poll home page data', '2018-01-06 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:01:07'),
(246, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:04:33'),
(247, 'POLL', 'Fetch Poll home page data', '2018-05-24 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:05:25'),
(248, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:05:55'),
(249, 'POLL', 'Fetch Poll home page data', '2018-06-25 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:06:26'),
(250, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:07:00'),
(251, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:07:43'),
(252, 'POLL', 'Fetch Poll home page data', '2018-08-28 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:08:29'),
(253, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:09:59'),
(254, 'POLL', 'Fetch Poll home page data', '2018-11-16 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:11:17'),
(255, 'POLL', 'Fetch Poll home page data', '2018-12-19 18:30:00', 'Gokul-A-3-10', '2020-05-02 18:12:20'),
(256, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:13:21'),
(257, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:14:18'),
(258, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:15:50'),
(259, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:15:56'),
(260, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:21:10'),
(261, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:22:48'),
(262, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:43:23'),
(263, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:43:49'),
(264, 'POLL', 'Polls removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:43:54'),
(265, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:43:55'),
(266, 'POLL', 'Polls removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:44:02'),
(267, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:44:02'),
(268, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:44:05'),
(269, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:44:52'),
(270, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:45:07'),
(271, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:45:53'),
(272, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:46:03'),
(273, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:46:11'),
(274, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:46:14'),
(275, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:46:40'),
(276, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:47:12'),
(277, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:55:58'),
(278, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:56:08'),
(279, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:56:28'),
(280, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:56:37'),
(281, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:56:46'),
(282, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 18:56:57'),
(283, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:21:48'),
(284, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:24:26'),
(285, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:25:52'),
(286, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:25:58'),
(287, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:26:12'),
(288, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:26:16'),
(289, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:26:18'),
(290, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:31:05'),
(291, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:31:45'),
(292, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:46'),
(293, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:46'),
(294, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:46'),
(295, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:46'),
(296, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(297, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(298, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(299, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(300, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(301, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(302, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(303, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(304, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:33:47'),
(305, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:34:07'),
(306, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:34:07'),
(307, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:34:07'),
(308, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:37:24'),
(309, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 19:37:24'),
(310, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 20:57:10'),
(311, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 20:57:17'),
(312, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 20:57:19'),
(313, 'POLL', 'Fetch Poll home page data', '2021-04-01 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:04:02'),
(314, 'POLL', 'Fetch Poll home page data', '2021-04-11 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:04:12'),
(315, 'POLL', 'Fetch Poll home page data', '2021-04-14 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:04:15'),
(316, 'POLL', 'Fetch Poll home page data', '2021-04-14 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:04:15'),
(317, 'POLL', 'Fetch Poll home page data', '2021-04-14 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:04:15'),
(318, 'POLL', 'Fetch Poll home page data', '2021-08-15 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:08:16'),
(319, 'POLL', 'Fetch Poll home page data', '2021-08-20 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:08:21'),
(320, 'POLL', 'Fetch Poll home page data', '2021-09-10 18:30:00', 'Gokul-A-3-10', '2020-05-02 21:09:11'),
(321, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-02 21:17:23'),
(322, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 08:10:46'),
(323, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 08:10:51'),
(324, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 09:36:51'),
(325, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 11:52:34'),
(326, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 11:53:45'),
(327, 'VENDOR', 'Society vendor added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 11:57:35'),
(328, 'LOGIN', 'User doesn\'t exists!!!', '0000-00-00 00:00:00', '8888888888', '2020-05-06 09:01:41'),
(329, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 09:01:46'),
(330, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 18:45:00'),
(331, 'VENDOR', 'Residential Vendor Added', '2020-05-11 18:30:00', 'Gokul-A-3-10', '2020-05-06 20:05:12'),
(332, 'VENDOR', 'Residential Vendor Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:41:31'),
(333, 'VENDOR', 'Residential Vendor Removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:49:34'),
(334, 'VENDOR', 'Society Vendor Removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:50:29'),
(335, 'VENDOR', 'Residential Vendor Removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:53:07'),
(336, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:53:15'),
(337, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:54:28'),
(338, 'VENDOR', 'Residential Vendor Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:57:25'),
(339, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 09:51:43'),
(340, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 16:31:25'),
(341, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:55:35'),
(342, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:56:04'),
(343, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:56:33'),
(344, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:58:02'),
(345, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 18:59:13'),
(346, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-07 23:22:19'),
(347, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-tplh', '2020-05-08 00:51:28'),
(348, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-h7w6', '2020-05-08 00:52:44'),
(349, 'EVENT', 'Events has been created', '2001-02-01 18:30:00', 'event-v7n1', '2020-05-08 01:02:02'),
(350, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-pkju', '2020-05-08 01:05:58'),
(351, 'EVENT', 'Events has been created', '2001-08-08 18:30:00', 'event-tsu6', '2020-05-08 01:08:09'),
(352, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-1csl', '2020-05-08 01:23:47'),
(353, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-mfj3', '2020-05-08 01:24:35'),
(354, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-1qfp', '2020-05-08 01:25:52'),
(355, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-ry7v', '2020-05-08 01:29:40'),
(356, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-152o', '2020-05-08 01:30:26'),
(357, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-kwc9', '2020-05-08 01:34:11'),
(358, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-zmh6', '2020-05-08 01:35:04'),
(359, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-9gdp', '2020-05-08 01:37:08'),
(360, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-bqos', '2020-05-08 01:52:20'),
(361, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-3ier', '2020-05-08 02:03:39'),
(362, 'EVENT', 'Events has been created', '2002-10-12 18:30:00', 'event-8agc', '2020-05-08 02:10:13'),
(363, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-skm1', '2020-05-08 02:11:51'),
(364, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-dtva', '2020-05-08 02:13:37'),
(365, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-x6v5', '2020-05-08 02:19:27'),
(366, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-apb1', '2020-05-08 02:24:55'),
(367, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-hb52', '2020-05-08 02:25:39'),
(368, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-1jyf', '2020-05-08 02:26:52'),
(369, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-iefs', '2020-05-08 02:27:24'),
(370, 'EVENT', 'Event removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 02:49:07'),
(371, 'EVENT', 'Event removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 02:49:26'),
(372, 'EVENT', 'Event removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 02:49:30'),
(373, 'EVENT', 'Events has been created', '0000-00-00 00:00:00', 'event-vzlo', '2020-05-08 02:57:26'),
(374, 'EVENT', 'Event removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 02:58:03'),
(375, 'ALBUM', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 04:41:16'),
(376, 'ALBUM', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 04:41:38'),
(377, 'ALBUM', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 04:42:23'),
(378, 'ALBUM', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 04:45:31'),
(379, 'ALBUM', 'Sucessfully Deleted', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 04:46:48'),
(380, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:08:40'),
(381, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:17:24'),
(382, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:17:40'),
(383, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:17:40'),
(384, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:17:53'),
(385, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:18:00'),
(386, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'Saurabh@gmail.com', '2020-05-08 10:18:10'),
(387, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:19:41'),
(388, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:20:20'),
(389, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:22:20'),
(390, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:22:23'),
(391, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:22:39'),
(392, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:22:39'),
(393, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:22:56'),
(394, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:32'),
(395, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:36'),
(396, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:44'),
(397, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:45'),
(398, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:51'),
(399, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:23:51'),
(400, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:24:15'),
(401, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:24:16'),
(402, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:04'),
(403, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:06'),
(404, 'MANAGING_COMMITEE', 'sucessfully Swap', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:21'),
(405, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:21'),
(406, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:36:30'),
(407, 'LOGIN', 'Username is incorrect', '0000-00-00 00:00:00', 'saurabh@gmail.com', '2020-05-08 10:37:29'),
(408, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:45:54'),
(409, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:45:56'),
(410, 'MANAGING_COMMITEE', 'Successfully Added', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:46:07'),
(411, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:46:07'),
(412, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:46:46'),
(413, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 10:47:03'),
(414, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 11:16:52'),
(415, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:43:59'),
(416, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:44:41'),
(417, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:46:07'),
(418, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:46:57'),
(419, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:03'),
(420, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:03'),
(421, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:08'),
(422, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:08'),
(423, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:14'),
(424, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:14'),
(425, 'MANAGING_COMMITEE', 'successfully removed', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:19'),
(426, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:19'),
(427, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:47:54'),
(428, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:48:03'),
(429, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:48:09'),
(430, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-08 14:48:12'),
(431, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-09 19:22:39'),
(432, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-10 21:00:02'),
(433, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 08:47:32'),
(434, 'NEWSFEED', 'News has been posted', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 09:31:10'),
(435, 'NEWSFEED', 'News removed sucessfully', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-11 09:31:18'),
(436, 'LOGIN', 'Successfully Logged In', '2012-02-18 18:30:00', 'Gokul-A-3-10', '2020-05-11 12:02:19'),
(437, 'EVENT', 'Events has been created', '2012-10-31 18:30:00', 'event-ht86', '2020-05-11 12:11:01'),
(438, 'EVENT', 'Event has been created', '0000-00-00 00:00:00', 'event-ojv4', '2020-05-11 12:15:31'),
(439, 'LOGIN', 'Successfully Logged In', '2023-09-25 18:30:00', 'Gokul-A-3-10', '2020-05-15 23:09:26'),
(440, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-15 23:14:21'),
(441, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 01:57:17'),
(442, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 10:37:56'),
(443, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 11:46:46'),
(444, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 13:44:05'),
(445, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-16 19:14:57'),
(446, 'LOGIN', 'Successfully Logged In', '2017-05-31 18:30:00', 'Gokul-A-3-10', '2020-05-19 17:06:01'),
(447, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:27:15'),
(448, 'PENDING_APPROVAL', 'membership status has been approve', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:32:57'),
(449, 'PENDING_APPROVAL', 'membership status has been reject', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:41:10'),
(450, 'PENDING_APPROVAL', 'membership status has been reject', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:48:35'),
(451, 'PENDING_APPROVAL', 'membership status has been reject', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:51:09'),
(452, 'PENDING_APPROVAL', 'membership status has been approve', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 19:58:19'),
(453, 'ACTIVE_MEMBER', 'membership status has been inactive', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 20:39:37'),
(454, 'ACTIVE_MEMBER', 'membership status has been disabled', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-19 20:40:07'),
(455, 'LOGIN', 'Successfully Logged In', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 10:53:35'),
(456, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 10:53:45'),
(457, 'LOGIN', 'Successfully Logged In', '2013-04-02 18:30:00', 'Gokul-A-3-10', '2020-05-21 13:04:03'),
(458, 'BOOKING_REQUEST', 'Booking $status', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:11:34'),
(459, 'BOOKING_REQUEST', 'Booking Rejected', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:16:45');
INSERT INTO `society_log` (`sr_id`, `logModule`, `logDescription`, `logTime`, `createdBy`, `createdDatetime`) VALUES
(460, 'MANAGING_COMMITEE', 'Fetch Managing Commitee List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:36:25'),
(461, 'GOVERNMENT_BODY', 'Fetch Government Body List', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:36:29'),
(462, 'POLL', 'Fetch Poll home page data', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:01');

-- --------------------------------------------------------

--
-- Table structure for table `society_master`
--

CREATE TABLE `society_master` (
  `sr_id` int(11) NOT NULL,
  `soc_name` varchar(150) NOT NULL,
  `soc_register_no` varchar(50) NOT NULL,
  `soc_pan` varchar(50) NOT NULL,
  `soc_gst` varchar(50) NOT NULL,
  `soc_address` varchar(500) NOT NULL,
  `bill_note` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_sos`
--

CREATE TABLE `society_sos` (
  `sr_id` int(11) NOT NULL,
  `sos_name` varchar(100) NOT NULL,
  `sos_contact` varchar(10) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_user_request`
--

CREATE TABLE `society_user_request` (
  `sr_id` int(10) NOT NULL,
  `usr_reqId` varchar(100) NOT NULL,
  `usr_reqSubject` varchar(500) NOT NULL,
  `usr_reqMeessage` varchar(1000) NOT NULL,
  `usr_reqDate` date NOT NULL,
  `usr_reqUsername` varchar(100) NOT NULL,
  `usr_reqStatus` varchar(20) NOT NULL,
  `usr_reqmailid` varchar(100) NOT NULL,
  `usr_reqadmin` varchar(100) DEFAULT NULL,
  `usr_reqIdadminDate` date DEFAULT NULL,
  `usr_reqreply` varchar(500) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `society_vendor_list`
--

CREATE TABLE `society_vendor_list` (
  `vendorId` varchar(100) NOT NULL,
  `vendorName` varchar(100) NOT NULL,
  `vendor_pic` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `society_vendor_list`
--

INSERT INTO `society_vendor_list` (`vendorId`, `vendorName`, `vendor_pic`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
('vendorId_01', 'Home Cleaning', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEn8PjL-0W_mqLSr5nZy3qMJWK3S-AqBTk0Ld7o8zIMRVfGk2HgQ', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_02', 'Pest Control', 'https://qualitypestcontrolomaha.com/wp-content/uploads/2017/01/cropped-logo-web2.png', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_03', 'Appliances', 'http://siddharthenterprises.net/wp-content/uploads/021-900x576.jpg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_04', 'Laundry Service', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9AvcUVmtTIRDhPLYlH-Mg0ZnwGLVJ_vIAzWcs2S0ayCqWPQzo1w', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_05', 'Carpet Care', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjnkfnXkntnAm0k0cvowO8W7dpR1J8JAyZt5P7XyQBKfKdvi2o', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_06', 'Health Care', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzSWzh2xub0sfRMWIs1GussLhbbwm-U7FSFYrvIQ-tJVk1PoZheg', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('vendorId_07', 'Sofa care', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCIOsC_-X6PLGvSfONMtqBsqxe2BUE-fECGz2DXD08dJIBKPf32A', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `soc_rules_and_regulation`
--

CREATE TABLE `soc_rules_and_regulation` (
  `sr_id` int(12) NOT NULL,
  `file_name` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soc_staff`
--

CREATE TABLE `soc_staff` (
  `id` int(11) NOT NULL,
  `vendor_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `extra` varchar(255) DEFAULT NULL,
  `extra1` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soc_staff`
--

INSERT INTO `soc_staff` (`id`, `vendor_type`, `name`, `address`, `mobile_no`, `extra`, `extra1`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Cleaning Staff', 'Riya Shedge', 'Bandra', '9865742650', '10:30', '16:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Cleaning Staff', 'Rajeev Sharma', 'andheri', '9587413322', '07:00', '12:30', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Appliance', 'Prathamesh Sawant', 'Vikroli', '9920856596', '14:00', '17:30', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Pest Control', 'Gaurav Gupta', 'Vasai', '8081659322', '12:00', '17:00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `soc_vendor`
--

CREATE TABLE `soc_vendor` (
  `id` int(11) NOT NULL,
  `vendor_type` varchar(255) NOT NULL,
  `contract_start` date NOT NULL,
  `contract_end` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `charges` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `vendor_status` varchar(20) NOT NULL DEFAULT 'active',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soc_vendor`
--

INSERT INTO `soc_vendor` (`id`, `vendor_type`, `contract_start`, `contract_end`, `name`, `address`, `mobile_no`, `charges`, `date_creation`, `vendor_status`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(4, 'Security', '2018-04-20', '2019-04-20', 'Ram Jain', 'Andheri', '9545879655', 4500, '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Security', '2018-03-03', '2021-03-03', 'Abhay Raaj', 'Vashi', '9987684265', 500, '0000-00-00 00:00:00', 'inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Security', '2018-03-15', '2019-03-15', 'Rohit Menon', 'Parel', '7520859628', 400, '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Security', '2018-05-09', '2020-05-09', 'Umesh Jadhav', 'Opp Domestic Airport, Navpada, Vile Parle East, Vile Parle, Mumbai, Maharashtra 400099', '9920467326', 950, '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Security', '2018-12-05', '2018-12-25', 'sourabh', '5132', '8888888888', 53125312, '0000-00-00 00:00:00', 'inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Security', '2019-01-31', '2019-01-31', 'DVTEST', 'dvvsvd', '9879797979', 454, '0000-00-00 00:00:00', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(12, 'Pest Control', '2019-01-31', '2019-04-25', 'saurabh mishra', 'bijnor tal-bikaner district rajasthan', '9494949494', 4510, '2019-01-30 03:27:39', 'inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 10:33:54'),
(13, 'Gardener', '2019-01-31', '2019-02-27', 'Zbsg', 'Hdhch\nUdud', '8656565646', 4579, '2019-01-30 04:27:54', 'inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Gardener', '2019-02-01', '2019-02-01', 'Gsha', 'Shava', '9797848484', 979464, '2019-01-30 04:28:29', 'inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'CCTV', '2019-10-25', '2020-11-01', 'Kajal', 'Bhandup', '7878787878', 200, '2019-10-22 03:40:02', 'inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-04 10:33:51'),
(16, 'Cleaning Staff', '2020-05-04', '2020-05-30', 'Test Vendor', 'Test', '9564574553', 343, '2020-05-04 11:57:35', 'inactive', 'Inactive', 'Gokul-A-3-10', '2020-05-04 11:57:35', 'Gokul-A-3-10', '2020-05-06 20:50:29');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `s_id` int(9) NOT NULL,
  `name` varchar(150) NOT NULL,
  `age` varchar(9) NOT NULL,
  `relation` varchar(150) NOT NULL,
  `address` text NOT NULL,
  `contact` varchar(10) NOT NULL,
  `gender` varchar(60) NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `archive` varchar(6) NOT NULL DEFAULT 'n',
  `st_doc` text NOT NULL,
  `blood_type` varchar(3) NOT NULL,
  `wish_to_donate` varchar(5) NOT NULL DEFAULT '''YES''',
  `emergency_contact_person` varchar(100) NOT NULL,
  `E_relation` varchar(25) NOT NULL,
  `E_contact` varchar(20) NOT NULL,
  `donorCard` varchar(10) NOT NULL DEFAULT '''NO''',
  `createdBy` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`s_id`, `name`, `age`, `relation`, `address`, `contact`, `gender`, `user_name`, `archive`, `st_doc`, `blood_type`, `wish_to_donate`, `emergency_contact_person`, `E_relation`, `E_contact`, `donorCard`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`, `status`) VALUES
(1, 'Onkar Dalvi', '25', 'maid', 'malad', '8898248704', 'male', 'Gokul-A-0-1', 'n', '', 'A+', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(2, 'Akshay Jadhav', '37', 'Driver', 'Andheri', '8898547896', 'male', 'Gokul-A-0-2', 'n', '', 'B+', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(3, 'Kalpana Naik', '40', 'maid', 'Boriwali', '8898123456', 'female', 'Gokul-B-1-4', 'n', '', 'AB+', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(4, 'Anushka Sharma', '25', 'maid', 'Kurla', '9869188456', 'female', 'Gokul-A-2-8', 'n', '', 'O+', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(5, 'Akshay Pawar', '50', 'Driver', 'Ghatkopar', '8879542163', 'male', 'Gokul-B-2-9', 'n', '', 'O-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(6, 'Prashik Khandare', '32', 'maid', 'Powai', '8459623178', 'male', 'Gokul-B-0-3', 'n', '', 'AB-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(7, 'Satish Gurav', '22', 'Driver', 'Chembur', '7864512325', 'male', 'Gokul-A-2-7', 'n', '', 'B-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(8, 'Pooja Labde', '27', 'maid', 'mahim', '9869188754', 'female', 'Gokul-B-2-8', 'n', '', 'A-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(9, 'Akshay Gawade', '42', 'maid', 'Parle', '8898220494', 'male', 'Gokul-A-2-9', 'n', '', 'O-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(10, 'Deepika Patel', '45', 'maid', 'Dadar', '8879654024', 'female', 'Gokul-A-1-4', 'n', '', 'B+', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(11, 'Nitesh Desai', '35', 'Driver', 'Churchgate', '8898458704', 'male', 'Gokul-B-0-2', 'n', '', 'A-', 'YES', '', '', '', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(16, 'Ashok Tambe', '30', 'Servant', '15/6 Ananat Vasudev Chawl Andheri east', '8169141452', 'Female', 'Gokul-A-3-10', 'n', '2ndphase.pdf', 'B+', 'Yes', 'Hahaha', 'Gshah', '6464640848', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(17, 'Ashokavate Yadav', '30', 'Electrician', 'raheja complex,svroad,andheri,kurla,mumbai,maharashtra,400059', '8655352098', 'Male', 'Gokul-A-3-10', 'n', '1529040720854.jpg', 'O-', 'No', 'roshan', 'brother', '8877998585', 'No', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(18, 'lamxi ayerr', '28', 'Waitress', 'rakmenta,chakala,svroad,andheri,mumbai,maharashtra,400058', '8169141452', 'Male', 'Gokul-A-3-10', 'n', '2ndphase (2).pdf', 'A+', 'YES', 'roshan', 'brother', '8877998585', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(19, 'ramabai', '32', 'Servant ', 'dharavi,dharavi,dharavi,sion,mumbai,maharashtra,400059', '8655203598', 'Female', 'Gokul-A-3-10', 'n', '2ndphase (1).pdf', 'A+', 'YES', 'roshan', 'brother', '8877998585', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(20, 'kamala', '31', 'Waitress', 'malad,west,mumbbai,mumbai,mumbai,maharashtra,400089', '8844996677', 'Female', 'Gokul-A-3-10', 'n', 'Lighthouse.jpg', 'O+', 'NO', 'kamal', 'son', '8745154875', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(21, 'puru', '25', 'Servant ', 'malad,west,mumbbai,mumbai,thane,maharashtra,400089', '8844996677', 'Male', 'Gokul-A-3-10', 'n', '1528698673_Chrysanthemum.jpg', 'A-', 'NO', 'kamal', 'son', '8745154875', 'NO', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(27, 'vwgshs', '94', 'Waitress', 'vsvsvsbs/vsvzvs//Maharashtra/979797', '6494948787', 'Male', 'Gokul-A-3-10', 'n', '1528878502694.jpg', 'A+', 'Yes', 'agagava', 'hsgsvshs', '9464945484', 'No', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(28, 'Roshan', '25', 'Servant', 'marol,Andheei,,Maharashtra,400059', '9800008665', 'Male', 'Gokul-A-3-10', 'n', '1534506308403.jpg', 'B+', 'No', 'Saurabh', 'nothing', '8850056666', 'No', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(29, 'Rohit Gaikwad', '17', 'Servant', 'andheri,andheri,,Maharashtra,123456', '1234567890', 'Male', 'Gokul-A-0-1', 'n', '1536153535284.jpg', 'O-', 'Yes', 'Arjun', 'brother', '1234567890', 'No', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(30, 'kanata bai', '35', 'Servant', 'marol,mumbai,,Maharashtra,400059', '8897694843', 'Female', 'Gokul-A-1-5', 'n', '1536153536254.jpg', 'B-', 'Yes', 'sachin', 'brothers ', '7694863648', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(31, 'sanjay gade', '45', 'Servant', 'marol police camp,andheri,,Maharashtra,400059', '8898325696', 'Male', 'Gokul-A-2-8', 'n', '1539252976814.jpg', 'AB+', 'Yes', 'sachin gadre', 'brother', '8898325696', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(32, 'Ganpat', '35', 'Waitress', 'marol pipe line,Andheri,,Maharashtra,400059', '8104241208', 'Male', 'Gokul-A-3-10', 'n', '1560579405308.jpg', 'A+', 'Yes', 'chimabai', 'wife', '8868523158', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'inactive'),
(33, 'Reena', '25', 'Waitress', 'Andheri,Mumbai,,Maharashtra,400059', '9853698533', 'Female', 'Gokul-A-0-1', 'n', '1564043831346.jpg', 'Blo', 'Wish ', 'Sham', 'Son', '9958746398', 'Donar Card', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(34, 'test', '25', 'Waitress', 'test,test,,Maharashtra,405588', '9855755558', 'Female', 'Gokul-A-0-1', 'n', '1564118771189.jpg', 'Don', '', 'test', 'test', '9856999999', '', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(35, 'tst', '56', 'Servant', 'gs,sv,,Maharashtra,213848', '9454884848', 'Male', 'Gokul-A-3-10', 'n', '1564650690766.jpg', 'B+', 'Yes', 'bvava', 'agag', '9494694949', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active'),
(36, 'puru', '24', 'Doctor', 'vfdc,gigj,,Maharashtra,400606', '9565656565', 'Male', 'Gokul-A-0-1', 'n', '1564651532477.jpg', 'A+', 'Yes', 'gdhdhf', 'jfhdhdhd', '9565575542', 'Yes', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(10) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub-standard`
--

CREATE TABLE `sub-standard` (
  `s-id` int(11) NOT NULL,
  `sinking_fund` int(11) NOT NULL,
  `parking_charges` int(11) NOT NULL,
  `repairs_and_maintenence Fees` int(11) NOT NULL,
  `water_charges` varchar(25) NOT NULL,
  `electricity_charges` int(4) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `flag` varchar(3) NOT NULL DEFAULT 'Yes',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tents`
--

CREATE TABLE `tents` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(25) NOT NULL,
  `file` varchar(255) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `s_email` varchar(70) NOT NULL,
  `blood` varchar(4) NOT NULL,
  `s_dob` date NOT NULL,
  `gender` varchar(8) NOT NULL,
  `s_hobbies` varchar(50) NOT NULL,
  `s_language` varchar(50) NOT NULL,
  `s_occ` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `isActive` varchar(10) NOT NULL DEFAULT 'InActive',
  `profile_image` varchar(255) DEFAULT NULL,
  `owner` varchar(255) NOT NULL,
  `reject` varchar(50) NOT NULL DEFAULT 'Not Reject',
  `accept_reason` varchar(100) NOT NULL,
  `reject_reason` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `useradmin1`
--

CREATE TABLE `useradmin1` (
  `user-id` int(11) NOT NULL,
  `user-username` varchar(50) NOT NULL,
  `user-keyvalue` varchar(10) NOT NULL,
  `user-value` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `useradmin2`
--

CREATE TABLE `useradmin2` (
  `user-id` int(11) NOT NULL,
  `user-keyvalue` int(11) NOT NULL,
  `user-message` varchar(500) NOT NULL,
  `id-yes` varchar(10) NOT NULL DEFAULT 'no',
  `s-date` varchar(10) NOT NULL,
  `s-admin` varchar(25) NOT NULL,
  `seen` varchar(50) NOT NULL DEFAULT 'not seen',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useradmin2`
--

INSERT INTO `useradmin2` (`user-id`, `user-keyvalue`, `user-message`, `id-yes`, `s-date`, `s-admin`, `seen`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(17, 149865, '      hello\r\n    ', 'yes', '2018-08-14', 'Gokul-A-0-1', 'seen', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(19, 149865, 'hi', 'yes', '2018-08-14', 'Gokul-A-3-10', 'seen', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(20, 149865, 'hi', 'yes', '2018-09-14', 'Gokul-A-3-10', 'seen', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(21, 149865, 'Hello', 'yes', '2019-10-22', 'Gokul-A-3-10', 'seen', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(22, 149865, 'Testing', 'yes', '2019-10-22', 'Gokul-A-3-10', 'seen', '', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(23, 149865, 'Hii', 'yes', '2020-05-09', 'Gokul-A-3-10', 'seen', '', 'Gokul-A-3-10', '2020-05-09 19:48:51', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(24, 149865, 'Hello bro', 'yes', '2020-05-09', 'Gokul-A-3-10', 'seen', '', 'Gokul-A-3-10', '2020-05-09 19:49:23', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(25, 149865, 'Hii', 'yes', '2020-05-09', 'Gokul-A-3-10', 'seen', '', 'Gokul-A-3-10', '2020-05-09 19:50:18', 'Gokul-A-3-10', '2020-05-21 13:37:18'),
(26, 149865, 'Hii', 'yes', '2020-05-09', 'Gokul-A-3-10', 'seen', '', 'Gokul-A-3-10', '2020-05-09 19:50:26', 'Gokul-A-3-10', '2020-05-21 13:37:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_arrears`
--

CREATE TABLE `user_arrears` (
  `sr_id` int(12) NOT NULL,
  `user_id` varchar(500) NOT NULL,
  `advance_amount` varchar(25) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_arrears1`
--

CREATE TABLE `user_arrears1` (
  `sr_id` int(12) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `Arrears_amount_pending` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_bank_master`
--

CREATE TABLE `user_bank_master` (
  `bk_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `account_no` varchar(20) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `ifsc` varchar(20) NOT NULL,
  `bank_name` varchar(25) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_maint`
--

CREATE TABLE `user_maint` (
  `srid` int(50) NOT NULL,
  `sinking_fund` int(11) NOT NULL,
  `parking_charges` int(11) NOT NULL,
  `repairs_and_maintenence Fees` int(11) NOT NULL,
  `water_charges` varchar(25) NOT NULL,
  `electricity_charges` int(4) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `user_name` varchar(65) NOT NULL,
  `flat_area` varchar(25) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vechical`
--

CREATE TABLE `vechical` (
  `v_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `v-type` varchar(25) NOT NULL,
  `v_numplate` text DEFAULT NULL,
  `v-park` varchar(11) DEFAULT NULL,
  `spot_name` varchar(50) DEFAULT NULL,
  `document_bro` varchar(100) DEFAULT NULL,
  `make_model` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vechical`
--

INSERT INTO `vechical` (`v_id`, `user_name`, `v-type`, `v_numplate`, `v-park`, `spot_name`, `document_bro`, `make_model`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(1, 'Gokul-A-0-1', '4-Wheeler', 'Mh02 8898', NULL, NULL, NULL, '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Gokul-A-0-2', '4-Wheeler', 'Mh02 7856', '1', 'A01', NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Gokul-A-0-3', '2-Wheeler', 'Mh02 9456', NULL, NULL, NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Gokul-A-1-4', '4-Wheeler', 'Mh03 2487', '2', 'A5', NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Gokul-A-1-5', '4-Wheeler', 'Mh05 1234', NULL, NULL, NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Gokul-A-1-6', '2-Wheeler', 'Mh11 6894', NULL, NULL, NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Gokul-A-2-7', '4-Wheeler', 'Mh09 1248', NULL, NULL, NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Gokul-A-2-8', '2-Wheeler', 'Mh01 3214', NULL, NULL, NULL, '', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'Gokul-A-2-9', '4-Wheeler', 'Mh01 9874', NULL, NULL, NULL, '', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Gokul-A-3-10', '4-wheeler', 'mh04bc4545', NULL, NULL, '2ndphase (1).pdf', 'hondacity', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Gokul-A-3-10', '2 wheeler', 'mh02AS9195', NULL, NULL, 'Tulips.jpg', 'pulser200', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Gokul-A-3-10', 'Two Wheeler', 'Mh03bx9933', NULL, NULL, '1528880985937.jpg', 'Yamaha Fz v2.0', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Gokul-A-3-10', 'Two Wheeler', 'Mh 02 cv 123', NULL, NULL, '1534412971393.jpg', 'Yamaha', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Gokul-A-3-10', 'Two Wheeler', 'bhj677', NULL, NULL, '1534508324226.jpg', 'yamaha', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Gokul-A-3-10', 'Two Wheeler', 'dio', NULL, NULL, '1536153477790.jpg', 'Honda', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(21, 'Gokul-A-3-10', 'Four Wheeler', 'testing', NULL, NULL, '1544435082301.jpg', 'testing', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(22, 'Gokul-A-3-10', 'Two Wheeler', 'mh021908', NULL, NULL, '1554720730778.jpg', 'Honda', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(23, 'Gokul-A-3-10', 'Two Wheeler', 'MH02 AS 4288', NULL, NULL, '1560579533035.jpg', 'Honda Dio', 'active', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(24, 'Gokul-A-0-1', 'Two Wheeler', 'test', NULL, NULL, '', 'Test', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(25, 'Gokul-A-0-1', 'Two Wheeler', 'test', NULL, NULL, '1564117915581.jpg', 'Test', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Gokul-A-0-1', 'Two Wheeler', 'test', NULL, NULL, '', 'Test', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Gokul-A-0-1', 'Four Wheeler', 'mh 04 bc 123', NULL, NULL, '', 'Ford', 'inactive', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `d-id` int(11) NOT NULL,
  `dir-name` varchar(30) NOT NULL,
  `div-add` text NOT NULL,
  `dir-timming` varchar(25) NOT NULL,
  `day_selected` varchar(55) NOT NULL,
  `dir-contact` varchar(11) NOT NULL,
  `dir-service` varchar(300) NOT NULL,
  `dir-visit` varchar(10) NOT NULL,
  `dir-person` varchar(100) NOT NULL,
  `bazar_status` varchar(20) NOT NULL DEFAULT 'active',
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`d-id`, `dir-name`, `div-add`, `dir-timming`, `day_selected`, `dir-contact`, `dir-service`, `dir-visit`, `dir-person`, `bazar_status`, `status`, `createdBy`, `createdDatetime`, `updatedBy`, `updatedDatetime`) VALUES
(4, 'vendorId_05', 'thane', '11 AM - 01 PM', '', '8694578921', 'carpet care', '900', 'manish rane', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'vendorId_04', 'dombiwali', '12 PM - 3 PM', '', '8879217056', 'dry wash', '2000', 'onkar patil', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'vendorId_03', 'santacruz east', '8 AM - 6 PM', '', '9821555555', 'kitchen spa', '2000', 'Vijay Govekar', 'inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:53:07'),
(8, 'vendorId_05', 'sakinaka east', '3 AM - 3 PM', '', '8655447788', 'pests', '500', 'pest service', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'vendorId_01', 'sakinaka West', '1 PM - 2 PM', '', '8655447788', 'hospital', '3250', 'ashirwad', 'inactive', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'vendorId_02', 'sakinaka east', '3 PM - 11 PM', '', '8655447788', 'sofa', '500', 'dV', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'vendorId_03', 'Roop Industrial Compound', '5 PM - 12 AM', '', '8655447788', 'floor,rooof,,home,bed', '200', 'kalvani homes', 'inactive', 'Inactive', '', '0000-00-00 00:00:00', 'Gokul-A-3-10', '2020-05-06 20:49:34'),
(12, 'vendorId_05', 'Roop Industrial Compound, Gala No. G6 E, Andheri Kurla Road, Sakinaka Mumbai 400072', '2 PM - 11 PM', '', '8655447788', 'for test', '100', 'raju mathle', 'active', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'vendorId_04', 'Hxdhxbn', '12 AM - 12 AM', '', '9535674479', 'Dgjfdufdd', '450', 'Shxhh', 'inactive', '', '', '2019-01-31 02:40:31', '', '0000-00-00 00:00:00'),
(14, 'vendorId_04', 'Hshshh', '12 AM - 12 AM', '', '9565656598', 'Hsjjs', '545', 'Ushhshs', 'inactive', '', '', '2019-01-31 02:42:15', '', '0000-00-00 00:00:00'),
(15, 'vendorId_04', 'Chdf', '02 PM - 08 PM', '', '8686888888', 'Chchvjvjhbk', '9696699', 'Xggx', 'inactive', '', '', '2019-01-31 02:49:24', '', '0000-00-00 00:00:00'),
(16, 'vendorId_04', 'Chdf', '02 PM - 08 PM', '', '8686888888', 'Chchvjvjhbk', '9696699', 'Xggx', 'active', '', '', '2019-01-31 02:49:25', '', '0000-00-00 00:00:00'),
(17, 'vendorId_01', 'Hshsb s', '03 PM - 09 PM', '', '8454894646', 'AC vavavabavaga', '49', 'Hd Bsd', 'inactive', '', '', '2019-01-31 03:43:07', '', '0000-00-00 00:00:00'),
(18, 'vendorId_06', 'sakinaka', '4 PM - 11 PM', '', '9899797979', 'dsaasdas', '45', 'health care', 'active', '', '', '2019-01-31 04:06:12', '', '0000-00-00 00:00:00'),
(19, 'vendorId_06', 'Marol', '9 AM - 6 PM', '', '7080852591', 'health care', '500', 'Ashi Singh', 'active', '', '', '2019-10-22 03:31:33', '', '0000-00-00 00:00:00'),
(20, 'vendorId_03', 'Mankhurd', '9 AM - 10 PM', '', '7258964762', 'appliances', '10', 'Jyoti', 'inactive', '', '', '2019-10-22 03:33:09', '', '0000-00-00 00:00:00'),
(21, 'vendorId_01', 'Test Vendor Address', '4 AM - 5 PM', '', '9767546547', 'Testing', '450', 'New Vendor', 'active', '', 'Gokul-A-3-10', '2020-05-06 20:05:12', '', '0000-00-00 00:00:00'),
(23, 'vendorId_01', 'Test Vendor Address', '3 AM - 4 PM', '', '9767546547', 'Testing', '450', 'New Vendor', 'active', '', 'Gokul-A-3-10', '2020-05-06 20:41:31', '', '0000-00-00 00:00:00'),
(24, 'vendorId_01', 'Test Vendor Address', '2 PM - 6 PM', '', '9767546547', 'Testing', '450', 'New Vendor', 'active', '', 'Gokul-A-3-10', '2020-05-06 20:57:24', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `sr_id` int(25) NOT NULL,
  `visitor_id` varchar(100) NOT NULL,
  `member_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `no_person` varchar(100) NOT NULL,
  `date_visit` date NOT NULL,
  `from_time` time NOT NULL,
  `visitor_type` varchar(30) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `visitor_contact` varchar(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visitor_type`
--

CREATE TABLE `visitor_type` (
  `sr_id` int(12) NOT NULL,
  `vistor_type` varchar(50) NOT NULL,
  `type_id` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'active',
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wing`
--

CREATE TABLE `wing` (
  `wing-id` int(11) NOT NULL,
  `wingname` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `createdBy` varchar(55) NOT NULL,
  `createdDatetime` datetime NOT NULL,
  `updatedBy` varchar(55) NOT NULL,
  `updatedDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounting`
--
ALTER TABLE `accounting`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `accounting_groups`
--
ALTER TABLE `accounting_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounting_opening_balance`
--
ALTER TABLE `accounting_opening_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounting_types`
--
ALTER TABLE `accounting_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`a-id`);

--
-- Indexes for table `album_photos`
--
ALTER TABLE `album_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `booking_slot`
--
ALTER TABLE `booking_slot`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`b-id`);

--
-- Indexes for table `complain_chat`
--
ALTER TABLE `complain_chat`
  ADD PRIMARY KEY (`user-id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`sr_no`);

--
-- Indexes for table `demo_req`
--
ALTER TABLE `demo_req`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `designation_parameter`
--
ALTER TABLE `designation_parameter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directory`
--
ALTER TABLE `directory`
  ADD PRIMARY KEY (`d-id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `doc_type`
--
ALTER TABLE `doc_type`
  ADD PRIMARY KEY (`dc-id`);

--
-- Indexes for table `email_account`
--
ALTER TABLE `email_account`
  ADD PRIMARY KEY (`sr_no`);

--
-- Indexes for table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`f-id`);

--
-- Indexes for table `fms_admin_tocken`
--
ALTER TABLE `fms_admin_tocken`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `fms_tocken`
--
ALTER TABLE `fms_tocken`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `fms_tocken_admin`
--
ALTER TABLE `fms_tocken_admin`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `forum_master`
--
ALTER TABLE `forum_master`
  ADD PRIMARY KEY (`forum-id`);

--
-- Indexes for table `forum_message`
--
ALTER TABLE `forum_message`
  ADD PRIMARY KEY (`in-form`);

--
-- Indexes for table `fsm_server_key`
--
ALTER TABLE `fsm_server_key`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `function_invitation_tbl`
--
ALTER TABLE `function_invitation_tbl`
  ADD PRIMARY KEY (`sr_id`),
  ADD UNIQUE KEY `invtn_id` (`invtn_id`);

--
-- Indexes for table `j_form`
--
ALTER TABLE `j_form`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `j_form_member_type`
--
ALTER TABLE `j_form_member_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legal`
--
ALTER TABLE `legal`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `lien`
--
ALTER TABLE `lien`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `maintenance_head`
--
ALTER TABLE `maintenance_head`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `maintenance_head_charges`
--
ALTER TABLE `maintenance_head_charges`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `meet-poll`
--
ALTER TABLE `meet-poll`
  ADD PRIMARY KEY (`meet-id`);

--
-- Indexes for table `meetingminutes`
--
ALTER TABLE `meetingminutes`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `meeting_availabilty`
--
ALTER TABLE `meeting_availabilty`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `member_maintenance_tbl`
--
ALTER TABLE `member_maintenance_tbl`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `news_letter`
--
ALTER TABLE `news_letter`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `nominal_form`
--
ALTER TABLE `nominal_form`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `parking_spot`
--
ALTER TABLE `parking_spot`
  ADD PRIMARY KEY (`p-id`);

--
-- Indexes for table `parking_spot_name`
--
ALTER TABLE `parking_spot_name`
  ADD PRIMARY KEY (`ps-id`);

--
-- Indexes for table `penalty`
--
ALTER TABLE `penalty`
  ADD PRIMARY KEY (`s-id`);

--
-- Indexes for table `pnl_acc_tbl`
--
ALTER TABLE `pnl_acc_tbl`
  ADD PRIMARY KEY (`srId`);

--
-- Indexes for table `poll_master`
--
ALTER TABLE `poll_master`
  ADD PRIMARY KEY (`pl-id`);

--
-- Indexes for table `poll_result`
--
ALTER TABLE `poll_result`
  ADD PRIMARY KEY (`p-id`);

--
-- Indexes for table `residential_helper`
--
ALTER TABLE `residential_helper`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `residential_shop`
--
ALTER TABLE `residential_shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s-maintain-money`
--
ALTER TABLE `s-maintain-money`
  ADD PRIMARY KEY (`s-id`);

--
-- Indexes for table `s-maintain-money-admin`
--
ALTER TABLE `s-maintain-money-admin`
  ADD PRIMARY KEY (`s-id`);

--
-- Indexes for table `s-r-buzzar`
--
ALTER TABLE `s-r-buzzar`
  ADD PRIMARY KEY (`bz-id`);

--
-- Indexes for table `s-r-complain`
--
ALTER TABLE `s-r-complain`
  ADD PRIMARY KEY (`c-id`);

--
-- Indexes for table `s-r-event`
--
ALTER TABLE `s-r-event`
  ADD PRIMARY KEY (`ev_id`);

--
-- Indexes for table `s-r-reply`
--
ALTER TABLE `s-r-reply`
  ADD PRIMARY KEY (`rp-id`);

--
-- Indexes for table `s-r-user`
--
ALTER TABLE `s-r-user`
  ADD PRIMARY KEY (`s-r-id`);

--
-- Indexes for table `schedular`
--
ALTER TABLE `schedular`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `share_master`
--
ALTER TABLE `share_master`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `sms_account`
--
ALTER TABLE `sms_account`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_account`
--
ALTER TABLE `society_account`
  ADD PRIMARY KEY (`sa_id`);

--
-- Indexes for table `society_address_master`
--
ALTER TABLE `society_address_master`
  ADD PRIMARY KEY (`soc_add_id`,`soc_id`);

--
-- Indexes for table `society_assets`
--
ALTER TABLE `society_assets`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_bank_master`
--
ALTER TABLE `society_bank_master`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `society_brodcast_tbl`
--
ALTER TABLE `society_brodcast_tbl`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_log`
--
ALTER TABLE `society_log`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_master`
--
ALTER TABLE `society_master`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_sos`
--
ALTER TABLE `society_sos`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_user_request`
--
ALTER TABLE `society_user_request`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `soc_rules_and_regulation`
--
ALTER TABLE `soc_rules_and_regulation`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `soc_staff`
--
ALTER TABLE `soc_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soc_vendor`
--
ALTER TABLE `soc_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub-standard`
--
ALTER TABLE `sub-standard`
  ADD PRIMARY KEY (`s-id`);

--
-- Indexes for table `tents`
--
ALTER TABLE `tents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useradmin1`
--
ALTER TABLE `useradmin1`
  ADD PRIMARY KEY (`user-id`);

--
-- Indexes for table `useradmin2`
--
ALTER TABLE `useradmin2`
  ADD PRIMARY KEY (`user-id`);

--
-- Indexes for table `user_arrears`
--
ALTER TABLE `user_arrears`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `user_arrears1`
--
ALTER TABLE `user_arrears1`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `user_bank_master`
--
ALTER TABLE `user_bank_master`
  ADD PRIMARY KEY (`bk_id`);

--
-- Indexes for table `user_maint`
--
ALTER TABLE `user_maint`
  ADD PRIMARY KEY (`srid`);

--
-- Indexes for table `vechical`
--
ALTER TABLE `vechical`
  ADD PRIMARY KEY (`v_id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`d-id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `visitor_type`
--
ALTER TABLE `visitor_type`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `wing`
--
ALTER TABLE `wing`
  ADD PRIMARY KEY (`wing-id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounting`
--
ALTER TABLE `accounting`
  MODIFY `a_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accounting_groups`
--
ALTER TABLE `accounting_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accounting_opening_balance`
--
ALTER TABLE `accounting_opening_balance`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accounting_types`
--
ALTER TABLE `accounting_types`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `a-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `album_photos`
--
ALTER TABLE `album_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audit`
--
ALTER TABLE `audit`
  MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking_slot`
--
ALTER TABLE `booking_slot`
  MODIFY `b_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `b-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complain_chat`
--
ALTER TABLE `complain_chat`
  MODIFY `user-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `sr_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `demo_req`
--
ALTER TABLE `demo_req`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designation_parameter`
--
ALTER TABLE `designation_parameter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `directory`
--
ALTER TABLE `directory`
  MODIFY `d-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `doc_type`
--
ALTER TABLE `doc_type`
  MODIFY `dc-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_account`
--
ALTER TABLE `email_account`
  MODIFY `sr_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `family`
--
ALTER TABLE `family`
  MODIFY `f_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `f-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `fms_admin_tocken`
--
ALTER TABLE `fms_admin_tocken`
  MODIFY `srid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fms_tocken`
--
ALTER TABLE `fms_tocken`
  MODIFY `srid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `fms_tocken_admin`
--
ALTER TABLE `fms_tocken_admin`
  MODIFY `srid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `forum_master`
--
ALTER TABLE `forum_master`
  MODIFY `forum-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `forum_message`
--
ALTER TABLE `forum_message`
  MODIFY `in-form` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fsm_server_key`
--
ALTER TABLE `fsm_server_key`
  MODIFY `sr_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `function_invitation_tbl`
--
ALTER TABLE `function_invitation_tbl`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `j_form`
--
ALTER TABLE `j_form`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `j_form_member_type`
--
ALTER TABLE `j_form_member_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `legal`
--
ALTER TABLE `legal`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lien`
--
ALTER TABLE `lien`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maintenance_head`
--
ALTER TABLE `maintenance_head`
  MODIFY `srid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maintenance_head_charges`
--
ALTER TABLE `maintenance_head_charges`
  MODIFY `srid` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meet-poll`
--
ALTER TABLE `meet-poll`
  MODIFY `meet-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meetingminutes`
--
ALTER TABLE `meetingminutes`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meeting_availabilty`
--
ALTER TABLE `meeting_availabilty`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `member_maintenance_tbl`
--
ALTER TABLE `member_maintenance_tbl`
  MODIFY `srid` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_letter`
--
ALTER TABLE `news_letter`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `nominal_form`
--
ALTER TABLE `nominal_form`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `parking_spot`
--
ALTER TABLE `parking_spot`
  MODIFY `p-id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `parking_spot_name`
--
ALTER TABLE `parking_spot_name`
  MODIFY `ps-id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penalty`
--
ALTER TABLE `penalty`
  MODIFY `s-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pnl_acc_tbl`
--
ALTER TABLE `pnl_acc_tbl`
  MODIFY `srId` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `poll_master`
--
ALTER TABLE `poll_master`
  MODIFY `pl-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `poll_result`
--
ALTER TABLE `poll_result`
  MODIFY `p-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `residential_helper`
--
ALTER TABLE `residential_helper`
  MODIFY `s_id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `residential_shop`
--
ALTER TABLE `residential_shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `s-maintain-money`
--
ALTER TABLE `s-maintain-money`
  MODIFY `s-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s-maintain-money-admin`
--
ALTER TABLE `s-maintain-money-admin`
  MODIFY `s-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s-r-buzzar`
--
ALTER TABLE `s-r-buzzar`
  MODIFY `bz-id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s-r-complain`
--
ALTER TABLE `s-r-complain`
  MODIFY `c-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s-r-event`
--
ALTER TABLE `s-r-event`
  MODIFY `ev_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `s-r-reply`
--
ALTER TABLE `s-r-reply`
  MODIFY `rp-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `s-r-user`
--
ALTER TABLE `s-r-user`
  MODIFY `s-r-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `schedular`
--
ALTER TABLE `schedular`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `share_master`
--
ALTER TABLE `share_master`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_account`
--
ALTER TABLE `sms_account`
  MODIFY `srid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_account`
--
ALTER TABLE `society_account`
  MODIFY `sa_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_assets`
--
ALTER TABLE `society_assets`
  MODIFY `sr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_brodcast_tbl`
--
ALTER TABLE `society_brodcast_tbl`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_log`
--
ALTER TABLE `society_log`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;

--
-- AUTO_INCREMENT for table `society_master`
--
ALTER TABLE `society_master`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_sos`
--
ALTER TABLE `society_sos`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `society_user_request`
--
ALTER TABLE `society_user_request`
  MODIFY `sr_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `soc_rules_and_regulation`
--
ALTER TABLE `soc_rules_and_regulation`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `soc_staff`
--
ALTER TABLE `soc_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `soc_vendor`
--
ALTER TABLE `soc_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `s_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub-standard`
--
ALTER TABLE `sub-standard`
  MODIFY `s-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tents`
--
ALTER TABLE `tents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `useradmin1`
--
ALTER TABLE `useradmin1`
  MODIFY `user-id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `useradmin2`
--
ALTER TABLE `useradmin2`
  MODIFY `user-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_arrears`
--
ALTER TABLE `user_arrears`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_arrears1`
--
ALTER TABLE `user_arrears1`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_bank_master`
--
ALTER TABLE `user_bank_master`
  MODIFY `bk_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_maint`
--
ALTER TABLE `user_maint`
  MODIFY `srid` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vechical`
--
ALTER TABLE `vechical`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `d-id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `sr_id` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitor_type`
--
ALTER TABLE `visitor_type`
  MODIFY `sr_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wing`
--
ALTER TABLE `wing`
  MODIFY `wing-id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
