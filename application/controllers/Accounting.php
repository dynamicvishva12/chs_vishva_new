<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
        $this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='ACCOUNTING';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->accounting_tbl=$socTables['accounting_tbl'];
		$this->society_bank_master_tbl=$socTables['society_bank_master_tbl'];
		$this->maintenance_tbl=$socTables['maintenance_tbl'];
		$this->accounting_types_tbl=$socTables['accounting_types_tbl'];
		$this->accounting_groups_tbl=$socTables['accounting_groups_tbl'];
		$this->accounting_opening_balance_tbl=$socTables['accounting_opening_balance_tbl'];
		$this->user_arrears_tbl=$socTables['user_arrears_tbl'];
		$this->members_advance_log_tbl=$socTables['members_advance_log_tbl'];
		$this->pnl_acc_tbl=$socTables['pnl_acc_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Accounting';

		// $expiration_time=(int)(time()-60*60);

		// if($this->input->cookie('submit_type_cookie')!="")
  //   		$this->input->set_cookie('submit_type_cookie', '', $expiration_time);
    }

    public function home()
    {
    	$breadcrumbArr[0]['name']="Accounting";
    	$breadcrumbArr[0]['link']=base_url()."accounting/home";
    	$breadcrumbArr[0]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$flag=0;

		$currmon= date("m");

		if($currmon>=4) 
		{
			$curryear= date("Y");
		}
		else
		{
			$curryear= date("Y")-1;
		}

		$submit_type=$this->input->post('submit_type');

		if(!empty($submit_type))
		{
	    	$expiration_time=(int)(time()+60*60);

	    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);
	    }
		// $fetch = mysqli_query($conn, "SELECT * FROM `accounting`  ORDER BY `a_id` DESC LIMIT 1
		// ");
		// $acchead=mysqli_fetch_assoc($fetch);

		// include '../database_connection/s-conn.php';

	 //    if($type=="banking")
	 //    {
	 //        $sql_qry="SELECT * FROM `accounting_types` WHERE `Account_group`=(SELECT `id` FROM `accounting_groups` WHERE `name`='Bank Account') OR `Account_group`=(SELECT `id` FROM `accounting_groups` WHERE `name`='Cash In Hand')";
	 //    }

	 //    if($type=="all")
	 //    {
	 //    	$sql_qry="SELECT * FROM `accounting_types`";
	 //    }

	 //    $fetch_head=mysqli_query($conn, $sql_qry);
	 //    if(mysqli_num_rows($fetch_head)>0)
	 //    {
	 //        while($head_data=mysqli_fetch_assoc($fetch_head))
	 //        {
	 //            $result[]=$head_data;
	 //        }
	 //    }

		$voucher_type="Reciept Voucher";
		$voucher_initial="RV_";
		// $vouc_data=fetch_vnum($soc_key,$userName,$voucher_type,$voucher_initial);

		// $fetch_group=mysqli_query($conn,"SELECT * FROM `accounting_groups` ORDER BY `name` ASC");

		$acc_grp_orderByArr['`accounting_groups`.`name`']="ASC";

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`accounting_groups`.`id`, `accounting_groups`.`name`, `accounting_groups`.`account_type`, `accounting_groups`.`extra`", $acc_grp_condtnArr=array(), $acc_grp_likeCondtnArr=array(), $acc_grp_joinArr=array(), $singleRow=FALSE, $acc_grp_orderByArr, $acc_grp_groupByArr=array(), $whereInArray=array(), $acc_grp_customWhereArray=array(), $backTicks=TRUE);

		$accounting_group_data=$query['userdata'];

		$this->data['accounting_group_data']=$accounting_group_data;

		// $data_qry=mysqli_query($conn,"SELECT `bank_id`, `bank_name` FROM `society_bank_master`");

		$query=$this->Mcommon->getRecords($tableName=$this->society_bank_master_tbl, $colNames="`society_bank_master`.`bank_id`, `society_bank_master`.`bank_name`", $bnk_condtnArr=array(), $bnk_likeCondtnArr=array(), $bnk_joinArr=array(), $singleRow=FALSE, $bnk_orderByArr=array(), $bnk_groupByArr=array(), $whereInArray=array(), $bnk_customWhereArray=array(), $backTicks=TRUE);

		$bank_data=$query['userdata'];

		$this->data['bank_data']=$bank_data;

		// $data_qry=mysqli_query($conn, "SELECT `s-r-fname` AS usrFname, `s-r-lname` AS usrLname , `s-r-username`, `s-r-flat-area`,(SELECT `accounting_id` FROM `accounting_types` WHERE `accounting_name`=`s-r-user`.`s-r-username` AND `accounting_type`='Assets') AS accId FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

		$bnk_condtnArr['`s-r-user`.`s-r-approve`']='approve';
		$bnk_orNotLikeArray['`s-r-user`.`disabled_user`']='disabled';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="`s-r-user`.`s-r-fname` AS usrFname, `s-r-user`.`s-r-lname` AS usrLname , `s-r-user`.`s-r-username`, `s-r-user`.`s-r-flat-area`,(SELECT `accounting_types`.`accounting_id` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_name` = `s-r-user`.`s-r-username` AND `accounting_types`.`accounting_type`='Assets' LIMIT 1) AS accId", $bnk_condtnArr, $bnk_likeCondtnArr=array(), $bnk_joinArr=array(), $singleRow=FALSE, $bnk_orderByArr=array(), $bnk_groupByArr=array(), $whereInArray=array(), $bnk_customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $bnk_orNotLikeArray, $limit="");

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

        $currmon=date("m");
        
        if ($currmon>=4) 
        {
        	$curryear= date("Y");
        }
        else
        {
        	$curryear= date("Y")-1;
        }  
            
        $value2=date("m");
        $value3=date("Y");
        $value4=$value3-1;

        if(($value2=="1") || ($value2=="2") || ($value2=="3"))
        {
	        // $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr_customWhereArray[]="(( YEAR(`accounting`.`s-date`)='".$value4."' AND MONTH(`accounting`.`s-date`)>'4') OR (YEAR(`accounting`.`s-date`)='".$value3."' AND MONTH(`accounting`.`s-date`)<='".$value2."' ))";

	        $acc_tr_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`transaction_no`", $acc_tr_condtnArr=array(), $acc_tr_likeCondtnArr=array(), $acc_tr_joinArr=array(), $singleRow=TRUE, $acc_tr_orderByArr, $acc_tr_groupByArr=array(), $whereInArray=array(), $acc_tr_customWhereArray, $backTicks=TRUE);

			$acchead=$query['userdata'];

	        // $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='$voucher_type' AND (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr1_condtnArr["`accounting`.`vaucher_name`"]=$voucher_type;

	        $acc_tr1_customWhereArray[]="(( YEAR(`accounting`.`s-date`)='".$value4."' AND MONTH(`accounting`.`s-date`)>'4') OR (YEAR(`accounting`.`s-date`)='".$value3."' AND MONTH(`accounting`.`s-date`)<='".$value2."' ))";

	        $acc_tr1_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`vaucher_no`", $acc_tr1_condtnArr, $acc_tr1_likeCondtnArr=array(), $acc_tr1_joinArr=array(), $singleRow=TRUE, $acc_tr1_orderByArr, $acc_tr1_groupByArr=array(), $whereInArray=array(), $acc_tr1_customWhereArray, $backTicks=TRUE);

			$acchead_voc=$query['userdata'];
        }
        else
        {
	        // $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";

	        $acc_tr_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`transaction_no`", $acc_tr_condtnArr=array(), $acc_tr_likeCondtnArr=array(), $acc_tr_joinArr=array(), $singleRow=TRUE, $acc_tr_orderByArr, $acc_tr_groupByArr=array(), $whereInArray=array(), $acc_tr_customWhereArray, $backTicks=TRUE);

			$acchead=$query['userdata'];

	        // $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='$voucher_type' AND ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr1_condtnArr["`accounting`.`vaucher_name`"]=$voucher_type;

	        $acc_tr1_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";

	        $acc_tr1_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`vaucher_no`", $acc_tr1_condtnArr, $acc_tr1_likeCondtnArr=array(), $acc_tr1_joinArr=array(), $singleRow=TRUE, $acc_tr1_orderByArr, $acc_tr1_groupByArr=array(), $whereInArray=array(), $acc_tr1_customWhereArray, $backTicks=TRUE);

			$acchead_voc=$query['userdata'];
        }
        
        if($acchead['transaction_no']=="")
        {
	        $acchead['transaction_no'];
	        $iddd=1;
        }
        else
        {
	        $vall=str_split($acchead['transaction_no'],10);
	        $iddd=$vall[1]+1;
        }

        $vouc_data['trannumber']="trn_".$curryear."-0".$iddd;
        
        if($acchead_voc['vaucher_no']=="")
        {
	        $acchead_voc['vaucher_no'];
	        $iddd_voc=1;
        }
        else
        {
	        $vall_voc=str_split($acchead_voc['vaucher_no'],9);
	        $iddd_voc=$vall_voc[1]+1;
        }
        
        $voucher=$voucher_type;
        $vouc_data['voucherno']=$voucher_initial."".$curryear."-0".$iddd_voc;

		$trannumber =$vouc_data['trannumber'];
		$voucherno=$vouc_data['voucherno'];

		$this->data['trannumber']=$trannumber;
		$this->data['voucherno']=$voucherno;

		$acc_orderByArr['`accounting`.`a_id`']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="*", $acc_condtnArr=array(), $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr, $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$acchead=$query['userdata'];

		$acchead['transaction_no'];
		
		if($acchead['transaction_no']=="")
		{
			$acchead['transaction_no'];
			$iddd=1;
		}
		else
		{
			$vall=explode("-", $acchead['transaction_no']);
			$iddd=$vall[1]+1;
		}

		if (isset($_POST['voucher'])) 
		{
			$bank="1";
			$voucher=$this->input->post('voucher');
			$flag=1;
			// $vfetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE `vaucher_name` = '$voucher' ORDER BY `a_id` DESC LIMIT 1");
			// $vacchead=mysqli_fetch_assoc($vfetch);

			$acc_condtnArr['`accounting`.`vaucher_name`']=$voucher;
			$acc_orderByArr['`accounting`.`a_id`']='DESC';

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="*", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr, $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$vacchead=$query['userdata'];

			$currmon= date("m");
			if ($currmon>=4) 
			{
				$curryear= date("Y");
			}
			else
			{
				$curryear= date("Y")-1;
			}

			if($vacchead['vaucher_no']=="")
			{
				$vacchead['vaucher_no'];
				$viddd=$voucher.$curryear."0-"."1";
			}
			else
			{
				$vall1=explode("-", $vacchead['vaucher_no']);
				$viddd=$vall1[1]+1;
				$viddd=$voucher.$curryear."0-".$viddd;
			}
		}

		if(isset($_POST['bank_acc'])) 
		{
			$bank="2";
			$voucher=$this->input->post('voucher');
			$bank_acc=$this->input->post('bank_acc');
			$useri1=$this->input->post('user');

			$flag=1;
			// $vfetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE `vaucher_name` = '$voucher' ORDER BY `a_id` DESC LIMIT 1");
			// $vacchead=mysqli_fetch_assoc($vfetch);

			$acc_condtnArr['`accounting`.`vaucher_name`']=$voucher;
			$acc_orderByArr['`accounting`.`a_id`']='DESC';

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="*", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr, $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$vacchead=$query['userdata'];

			$currmon= date("m");
			if ($currmon>=4)
			{
				$curryear= date("Y");
			}
			else
			{
				$curryear= date("Y")-1;
			}

			if($vacchead['vaucher_no']=="")
			{
				$vacchead['vaucher_no'];
				$viddd=$voucher.$curryear."0-"."1";
			}
			else
			{
				$vall1=explode("-", $vacchead['vaucher_no']);
				$viddd=$vall1[1]+1;
				$viddd=$voucher.$curryear."0-".$viddd;
			}
		}

		if(isset($_POST['sub_account'])) 
		{
			$userna="";
			$trannumber1=$this->input->post('trannumber');
			$voucher1=$this->input->post('voucher');
			$voc_no01=$this->input->post('voc_no0');

			if($voucher1=="RV_" || $voucher1=="BT_") 
			{
				$account11=$this->input->post('bank_acc');
			}
			else
			{
				$account11=$this->input->post('account1');
			}

			if($voucher1 == "PV_")
			{
				$account21=$this->input->post('bank_acc1');
			}
			elseif($voucher1 == "RV_")
			{
				$account21=$this->input->post('user');
				$user = $this->input->post('user');
			}
			else
			{
				$account21=$this->input->post('account2');
			}


			$Dr1=$this->input->post('Dr');
			$Cr1=$this->input->post('Cr');
			//$account21=$_POST['account2'];
			$narratn1=$this->input->post('narratn');
			$Payment1=$this->input->post('Payment');
			$amount1=$this->input->post('amount');

			if (isset($_POST['s-date'])) 
			{
				$s_date=$this->input->post('s-date');
			}
			else
			{
				$s_date="";
			}

			if ($this->input->post('Payment')=="bank") 
			{
				$chkno=$this->input->post('chkno');
				$Micr=$this->input->post('Micr');
				$ifsccode=$this->input->post('ifsccode');
				$bank_name=$this->input->post('bank_name');
			}
			else
			{
				$chkno="";
				$Micr="";
				$ifsccode="";
				$bank_name="";
			}

			if(isset($_FILES['myfile']))
			{
				$accpic=$_FILES['myfile']['name'];
				$t_tmp=$_FILES['myfile']['tmp_name'];
				if ($accpic!="") 
				{
					$temp = explode(".", $_FILES["myfile"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);

					$fileexe = strtolower(pathinfo($accpic,PATHINFO_EXTENSION));
					$store=FCPATH."assets/img/".$newfilename;
					if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
					{
						move_uploaded_file($t_tmp, $store);
					}
					else
					{
						echo $this->Mfunctional->IALERT('Plz Upload only Image file even gif also not Accecptable');
					}
				}
			}

			$curr_date="2017-04-20";

			// $drinsert = mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`,`s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`,`cr_dr`) VALUES ('$account11', '$amount1', '$curr_date', '$store', '$s_date', '$narratn1', '$userna', '$bank_name', '$ifsccode', '$Micr', '$trannumber1', '$voucher1', '$voc_no01', '$chkno','$Dr1')")or die(mysqli_error($conn));

			// $crinsert = mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`,`s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`,`cr_dr`) VALUES ('$account21', '$amount1', '$curr_date', '$store', '$s_date', '$narratn1', '$userna', '$bank_name', '$ifsccode', '$Micr', '$trannumber1', '$voucher1', '$voc_no01', '$chkno','$Cr1')")or die(mysqli_error($conn));

			$accInsertArr[]=array(
				'accouting_id'=>$account11,
				'accounting_charges'=>$amount1,
				's-date'=>$curr_date,
				'accounting_recipt'=>$store,
				's-indent'=>$s_date,
				's-des'=>$narratn1,
				'member_id'=>$userna,
				'bank_name'=>$bank_name,
				'ifsc'=>$ifsccode,
				'micr'=>$Micr,
				'transaction_no'=>$trannumber1,
				'vaucher_name'=>$voucher1,
				'vaucher_no'=>$voc_no01,
				'cheque_no'=>$chkno,
				'cr_dr'=>$Dr1,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $accInsertArr, $returnType="");

			$accInsertStatus=$query['status'];

			$acc1InsertArr[]=array(
				'accouting_id'=>$account21,
				'accounting_charges'=>$amount1,
				's-date'=>$curr_date,
				'accounting_recipt'=>$store,
				's-indent'=>$s_date,
				's-des'=>$narratn1,
				'member_id'=>$userna,
				'bank_name'=>$bank_name,
				'ifsc'=>$ifsccode,
				'micr'=>$Micr,
				'transaction_no'=>$trannumber1,
				'vaucher_name'=>$voucher1,
				'vaucher_no'=>$voc_no01,
				'cheque_no'=>$chkno,
				'cr_dr'=>$Cr1,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $acc1InsertArr, $returnType="");

			if($accInsertStatus==TRUE)
			{
				if ($voucher1=="RV_") 
				{
					// $bankfetch = mysqli_query($conn,"SELECT * FROM `society_bank_master` WHERE `bank_id`= '$account11'");
					// $bankdetails = mysqli_fetch_assoc($bankfetch);

					$bk_condtnArr['`society_bank_master`.`bank_id`']=$account11;

					$query=$this->Mcommon->getRecords($tableName=$this->society_bank_master_tbl, $colNames="*", $bk_condtnArr, $likeCondtnArr=array(), $bk_joinArr=array(), $singleRow=TRUE, $bk_orderByArr, $bk_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

					$bankdetails=$query['userdata'];

					foreach($this->input->post('month') as $key => $value)
					{
						$test1= explode("_", $value);

						$monthno = $test1[0];
						$yearno = $test1[1];
						$date1=date("Y-m-d");
						$startday=strtotime($date1);
						$month=date("F",$startday);

						//$change=mysqli_query($conn,"UPDATE `maintance` SET `status`='paid',`Pay_month`='$month',`bank_name`='".$bankdetails['bank_name']."',`branch_name`='".$bankdetails['bank_branch']."',`cheque_no`='$chkno',`date`='$date1' ,`voucher_no1` = '$voc_no01',`tansaction_no1` = '$trannumber1',`voucher_name1` = '$voucher1' WHERE `user_name`='$user' AND MONTH(`end_day`)='$monthno' AND YEAR(`end_day`)='$yearno' AND `status`='unpaid' ")or die(mysqli_error($conn));

						$mntUpdateArr=array(
							'status'=>'paid',
							'Pay_month'=>$month,
							'bank_name'=>$bankdetails['bank_name'],
							'branch_name'=>$bankdetails['bank_branch'],
							'cheque_no'=>$chkno,
							'date'=>$date1,
							'voucher_no1'=>$voc_no01,
							'tansaction_no1'=>$trannumber1,
							'voucher_name1'=>$voucher1,
							'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
						);

						$mntUpdtCondtnArr['maintenance.user_name']=$user;
						$mntUpdtCondtnArr['MONTH(`maintenance`.`end_day`)']=$monthno;
						$mntUpdtCondtnArr['YEAR(`maintenance`.`end_day`)']=$yearno;
						$mntUpdtCondtnArr['`maintenance`.`status`']='unpaid';

						$query=$this->Mcommon->update($tableName=$this->maintenance_tbl, $mntUpdateArr, $mntUpdtCondtnArr, $likeCondtnArr=array());
					}
					//header('Location:20mar_account_add.php');
				}
			}
		}

		if(isset($_POST['post-id1'])) 
		{
			$accountty=$this->input->post('accountty');

			if($this->input->post('accountty')=="income") 
			{
				// $fetch=mysqli_query($conn,"SELECT * FROM `accounting_types` WHERE `accounting_type`='$accountty' ORDER BY `accounting_id` DESC LIMIT 1");

				$acc_condtnArr['`accounting_types`.`accounting_type`']=$accountty;
				$acc_orderByArr['`accounting_types`.`accounting_id`']='DESC';

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="*", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr, $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$result=$query['userdata'];
			}
			else
			{
				// $fetch=mysqli_query($conn,"SELECT * FROM `accounting_types` WHERE `accounting_type`='$accountty' ORDER BY `accounting_id` DESC LIMIT 1");

				$acc_condtnArr['`accounting_types`.`accounting_type`']=$accountty;
				$acc_orderByArr['`accounting_types`.`accounting_id`']='DESC';

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="*", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr, $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$result=$query['userdata'];
			}

			$result['accounting_id'];
			$accid=$result['accounting_id']+1;

			$accname=$this->input->post('first-name');
			$accgroup=$this->input->post('accountgrp');

			// $acc_insert=mysqli_query($conn,"INSERT INTO `accounting_types`(`accounting_id`, `accounting_type`, `accounting_name`, `Account_group`) VALUES ('$accid','$accountty','$accname','$accgroup')");

			$accTypeInsertArr[]=array(
				'accouting_id'=>$accid,
				'accounting_type'=>$accountty,
				'accounting_name'=>$accname,
				'Account_group'=>$accgroup,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_types_tbl, $accTypeInsertArr, $returnType="");

			$accTypeInsertStatus=$query['status'];

			if($accTypeInsertStatus==TRUE) 
			{
				echo "<html><body><script>alert('Inserted successfully')</script></body></html>";
				redirect(base_url()."accounting/home", 'refresh');
			}
		}

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id, accounting_types.accounting_name", $acc_condtnArr=array(), $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=FALSE, $acc_orderByArr=array(), $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$accounting_head_data=$query['userdata'];

		$this->data['accounting_head_data']=$accounting_head_data;

		// $acc1_condtnArr['accounting_types.Account_group']="(SELECT `id` FROM ".$this->accounting_groups_tbl." WHERE `name`='Bank Account') OR `Account_group`=(SELECT `id` FROM ".$this->accounting_groups_tbl." WHERE `name`='Cash In Hand')";
		$acc1_customWhereArray['accounting_types.Account_group']="accounting_types.Account_group=(SELECT `id` FROM ".$this->accounting_groups_tbl." WHERE `name`='Bank Account') OR `Account_group`=(SELECT `id` FROM ".$this->accounting_groups_tbl." WHERE `name`='Cash In Hand')";

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id, accounting_types.accounting_name", $acc1_condtnArr=array(), $likeCondtnArr=array(), $acc1_joinArr=array(), $singleRow=FALSE, $acc1_orderByArr=array(), $acc1_groupByArr=array(), $whereInArray=array(), $acc1_customWhereArray, $backTicks=TRUE);

		$banking_account_head=$query['userdata'];

		$this->data['banking_account_head']=$banking_account_head;

		$bank_condtnArr['`accounting_types`.`Account_group`']="2";

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="`accounting_types`.`accounting_id`, `accounting_types`.`accounting_name`, (SELECT COUNT(`society_bank_master`.`bank_id`) FROM ".$this->society_bank_master_tbl." WHERE `society_bank_master`.`bank_id`=`accounting_types`.`accounting_id`) AS bankCount", $bank_condtnArr, $likeCondtnArr=array(), $bank_joinArr=array(), $singleRow=FALSE, $bank_orderByArr=array(), $bank_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$fetch_tbl_acc_bank=$query['userdata'];

		$this->data['fetch_tbl_acc_bank']=$fetch_tbl_acc_bank;

		$query=$this->Mcommon->getRecords($tableName=$this->society_bank_master_tbl, $colNames="`society_bank_master`.`bank_id`, `society_bank_master`.`bank_account_number`, `society_bank_master`.`bank_name`, `society_bank_master`.`bank_branch`, `society_bank_master`.`bank_ifsc`, `society_bank_master`.`bank_micr`", $bank_det_condtnArr=array(), $likeCondtnArr=array(), $bank_det_joinArr=array(), $singleRow=FALSE, $bank_det_orderByArr=array(), $bank_det_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$fetch_tbl_details_bank=$query['userdata'];

		$this->data['fetch_tbl_details_bank']=$fetch_tbl_details_bank;

		$todays=date("Y-m-d");

    	// $fetch_acc_head=mysqli_query($conn,"SELECT `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`,  (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND ('$todays' BETWEEN `from_date` AND `to_date`)) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND ('$todays' BETWEEN `from_date` AND `to_date`)) AS type_ledger,(SELECT `name` FROM `accounting_groups` WHERE `accounting_groups`.`id`=`accounting_types`.`Account_group` OR `accounting_groups`.`name`=`accounting_types`.`Account_group` LIMIT 1) AS Groupname,(SELECT `extra` FROM `accounting_groups` WHERE `accounting_groups`.`id`=`accounting_types`.`Account_group` LIMIT 1) AS Groupnature FROM `accounting_types` ORDER BY `id` ASC ");

    	$accHd_orderByArr['`accounting_types`.`id`']="ASC";

    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="`id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`,  (SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND ('".$todays."' BETWEEN `from_date` AND `to_date`)) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND ('$todays' BETWEEN `from_date` AND `to_date`)) AS type_ledger,(SELECT `name` FROM ".$this->accounting_groups_tbl." WHERE `accounting_groups`.`id`=`accounting_types`.`Account_group` OR `accounting_groups`.`name` = `accounting_types`.`Account_group` LIMIT 1) AS Groupname,(SELECT `extra` FROM ".$this->accounting_groups_tbl." WHERE `accounting_groups`.`id`=`accounting_types`.`Account_group` LIMIT 1) AS Groupnature", $accHd_condtnArr=array(), $likeCondtnArr=array(), $accHd_joinArr=array(), $singleRow=FALSE, $accHd_orderByArr, $accHd_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$fetch_tbl_acc_head=$query['userdata'];

		$this->data['fetch_tbl_acc_head']=$fetch_tbl_acc_head;

		 // $sql_qry="SELECT * FROM `accounting_types` WHERE `Account_group`=(SELECT `id` FROM `accounting_groups` WHERE `name`='Bank Account') OR `Account_group`=(SELECT `id` FROM `accounting_groups` WHERE `name`='Cash In Hand')";

		$this->data['from_date_day_bk']=$from_date_day_bk=$this->input->post('from_date');
		$this->data['to_date_day_bk']=$to_date_day_bk=$this->input->post('to_date');

		if($from_date_day_bk!="" || $to_date_day_bk!="")
		{
			$this->data['from_date']=$from_date=date("Y-m-d",strtotime($from_date_day_bk));
	       	$this->data['to_date']=$to_date=date("Y-m-d",strtotime($to_date_day_bk));

			if(isset($_GET['AccId']))
			{
				$Acc_id=$this->input->get('AccId');
				// $ladger_data=fetch_ladger_data($soc_key, $Acc_id, $from_date, $to_date);
	   
				if($Acc_id=="all")
				{
					// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `cr_dr`=( CASE 
					// WHEN `vaucher_name`='Payment Voucher' THEN 'Dr'
					// WHEN `vaucher_name`='Reciept Voucher' THEN 'Cr' 
					// WHEN `vaucher_name`='Contra Voucher' THEN 'Dr' 
					// WHEN `vaucher_name`='Journal Voucher' THEN 'Dr' 
					// END ) AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");

					// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
					// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
					// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
					// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
					// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
					// 	END)";

					$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
						WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
						WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
						WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
						WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
						END))";
					$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

					$acchd_orderByArr['`accounting`.`s-date`']="DESC";
					$acchd_orderByArr['`accounting`.`a_id`']="DESC";

					$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

					$ladger_data=$query['userdata']; 
			   }
			   else
			   {
			   		// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` =   `accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `accouting_id`='$accId' AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");  
			   		$acchd_condtnArr['`accounting`.`accouting_id`']=$Acc_id;

			   		$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

					$acchd_orderByArr['`accounting`.`s-date`']="DESC";
					$acchd_orderByArr['`accounting`.`a_id`']="DESC";

					$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr, $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

					$ladger_data=$query['userdata'];  
			   }
			}
			elseif(isset($_GET['GroupId']))
			{
				$GroupId=$this->input->get('GroupId');
				// $ladger_data=fetch_ladger_data_Group($soc_key, $GroupId, $from_date, $to_date);

				$a_u_condtnArr['`accounting_types`.`Account_group`']=$GroupId;

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id, accounting_types.accounting_name", $a_u_condtnArr, $likeCondtnArr=array(), $a_u_joinArr=array(), $singleRow=FALSE, $a_u_orderByArr=array(), $a_u_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$ladger_data=$query['userdata'];
			}
			else
			{
				// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
				// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
				// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
				// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
				// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
				// 	END)";

				$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
					WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
					WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
					WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
					WHEN `accounting`.`vaucher_name`='Journa	l Voucher' THEN 'Dr' 
					END))";

				$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

				$acchd_orderByArr['`accounting`.`s-date`']="DESC";
				$acchd_orderByArr['`accounting`.`a_id`']="DESC";

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

				$ladger_data=$query['userdata']; 
			}

			$this->data['ladger_data']=$ladger_data;

			// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `vaucher_no`='$vaucharId' ");   

			// echo "SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting`";

			// $voc_condtnArr['`accounting_types`.`vaucher_no`']=$vaucharId;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` = `accounting`.`accouting_id` LIMIT 1) AS headName", $voc_condtnArr=array(), $likeCondtnArr=array(), $voc_joinArr=array(), $singleRow=FALSE, $voc_orderByArr=array(), $voc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$voucher_data=$query['userdata'];

			$voucherArr=array();

			if(!empty($voucher_data))
			{
				foreach($voucher_data AS $e_voc) 
				{
					$voucherArr[$e_voc['vaucher_no']]=$e_voc;
				}
			}

			$this->data['voucherArr']=$voucherArr;
		}

    	$this->data['view']="accounting/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function account_entry()
    {
    	$this->db->trans_start();

    	$submit_type=$this->input->post('submit_type');

    	$expiration_time=(int)(time()+60*60);
    	
    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);

		$acc_trannumber=$this->input->post('acc_trannumber');
		$acc_voucher_type=$this->input->post('acc_voucher_type');
		$acc_vno=$this->input->post('acc_vno');
		$acc_dr=$this->input->post('acc_dr');
		$account_head_dr=$this->input->post('account_head_dr');
		$acc_bank_head_dr=$this->input->post('acc_bank_head_dr');
		$acc_cr=$this->input->post('acc_cr');
		$account_head_cr=$this->input->post('account_head_cr');
		$bank_head_cr=$this->input->post('bank_head_cr');
		$acc_narratn=$this->input->post('acc_narratn');
		$acc_Payment=$this->input->post('acc_Payment');
		$acc_amount=$this->input->post('acc_amount');
		$acc_s_date=$this->input->post('acc_s_date');
		$acc_chkno=$this->input->post('acc_chkno');
		$acc_bank_name=$this->input->post('acc_bank_name');
		$acc_ifsccode=$this->input->post('acc_ifsccode');
		$acc_Micr=$this->input->post('acc_Micr');

		if(($acc_voucher_type=="Payment Voucher") || ($acc_voucher_type=="Journal Voucher"))
		{
			$acc_head_dr=$account_head_dr;
		}
		else
		{
			$acc_head_dr=$acc_bank_head_dr;
		}

		if(($acc_voucher_type=="Payment Voucher") || ($acc_voucher_type=="Contra Voucher"))
		{
			$acc_head_cr=$bank_head_cr;
		}
		else
		{
			$acc_head_cr=$account_head_cr;
		}

		if(isset($_FILES['acc_myfile']))
		{
			$accpic=$_FILES['acc_myfile']['name'];
			$t_tmp=$_FILES['acc_myfile']['tmp_name'];

			if($accpic!="") 
			{
				$temp = explode(".", $_FILES["acc_myfile"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);

				$fileexe = strtolower(pathinfo($accpic,PATHINFO_EXTENSION));
				$store=FCPATH."assets/img/".$newfilename;
				if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
				{
					move_uploaded_file($t_tmp, $store);
				}
			}
		}

		$response="";

		if(!empty($acc_trannumber) && !empty($acc_voucher_type) && !empty($acc_vno) && !empty($acc_dr) && !empty($acc_cr) && !empty($acc_head_dr) && !empty($acc_head_cr))
		{

			// $result=account_entry($soc_key, $userName, $acc_trannumber, $acc_voucher_type, $acc_vno, $acc_dr, $acc_cr, $acc_narratn, $acc_Payment, $acc_amount, $acc_s_date, $acc_chkno, $acc_bank_name, $acc_ifsccode, $acc_Micr, $acc_head_dr, $acc_head_cr, $newfilename);

			// function account_entry($soc_key,$user_Name,$acc_trannumber,$acc_voucher_type,$acc_vno,$acc_dr,$acc_cr,$acc_narratn,$acc_Payment,$acc_amount,$acc_s_date,$acc_chkno,$acc_bank_name,$acc_ifsccode,$acc_Micr,$acc_head_dr,$acc_head_cr,$newfilename)

			$current_date=date("Y-m-d");

			// $recipt_qry_dr=mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cr_dr`, `s-des`, `cheque_no`, `accounting_recipt`,`s-indent`,`bank_name`, `ifsc`, `micr`,`payment_mode`) VALUES ('$acc_head_dr', '$acc_amount', '$current_date', '$acc_trannumber', '$acc_voucher_type', '$acc_vno', 'Dr', '$acc_narratn', '$acc_chkno', '$newfilename', '$acc_s_date', '$acc_bank_name', '$acc_ifsccode', '$acc_Micr', '$acc_Payment')");

			$rcptDrInsertArr[]=array(
				'accouting_id'=>$acc_head_dr,
				'accounting_charges'=>$acc_amount,
				'transaction_no'=>$acc_trannumber,
				'vaucher_name'=>$acc_voucher_type,
				'vaucher_no'=>$acc_vno,
				'cr_dr'=>'Dr',
				's-des'=>$acc_narratn,
				'cheque_no'=>$acc_chkno,
				'accounting_recipt'=>$newfilename,
				's-indent'=>$acc_s_date,
				'bank_name'=>$acc_bank_name,
				'ifsc'=>$acc_ifsccode,
				'micr'=>$acc_Micr,
				'payment_mode'=>$acc_Payment,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $rcptDrInsertArr, $returnType="");

			$rcptDrQuery=$query['status'];

			if($rcptDrQuery==TRUE)
			{
				// $recipt_qry_cr=mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `transaction_no`, `vaucher_name`, `vaucher_no`,`cr_dr`,`s-des`, `cheque_no`, `accounting_recipt`,`s-indent`,`bank_name`, `ifsc`, `micr`,`payment_mode`) VALUES ('$acc_head_cr', '$acc_amount', '$current_date','$acc_trannumber','$acc_voucher_type','$acc_vno','Cr','$acc_narratn','$acc_chkno','$newfilename','$acc_s_date','$acc_bank_name','$acc_ifsccode','$acc_Micr','$acc_Payment')")or die(mysqli_error($conn));

				$rcptCrInsertArr[]=array(
					'accouting_id'=>$acc_head_dr,
					'accounting_charges'=>$acc_amount,
					's-date'=>$current_date,
					'transaction_no'=>$acc_trannumber,
					'vaucher_name'=>$acc_voucher_type,
					'vaucher_no'=>$acc_vno,
					'cr_dr'=>'Cr',
					's-des'=>$acc_narratn,
					'cheque_no'=>$acc_chkno,
					'accounting_recipt'=>$newfilename,
					's-indent'=>$acc_s_date,
					'bank_name'=>$acc_bank_name,
					'ifsc'=>$acc_ifsccode,
					'micr'=>$acc_Micr,
					'payment_mode'=>$acc_Payment,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $rcptCrInsertArr, $returnType="");

				$rcptCrQuery=$query['status'];

				if($rcptCrQuery==TRUE)
				{
					$response="Entry genrated sucessfully";
				}
			}

			if(!empty($response))
			{
				$this->session->set_flashdata('success_msg', 'Added sucessfully');
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/home", 'refresh');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!');
			redirect(base_url()."accounting/home", 'refresh');
		}
		else
		{
			redirect(base_url()."accounting/home", 'refresh');
		}  
    }

    public function add_account_head()
    {
		$head_name=$this->input->post('head_name');
		$account_group=$this->input->post('account_group');
		$amount=$this->input->post('amount');
		$cr_dr=$this->input->post('cr_dr');

		$submit_type=$this->input->post('submit_type');

		if(!empty($submit_type))
		{
	    	$expiration_time=(int)(time()+60*60);
	    	
	    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);
	    }
		// Array ( [head_name] => Accounts Charges [account_group] => 8 [amount] => 0 [cr_dr] => Dr [add_account_head] => ) 
		if(!empty($head_name) && !empty($account_group) && !empty($amount) && !empty($cr_dr))
		{
			$currmon=date("m");

			if($currmon>=4) 
			{
				$curryear=date("Y")."-04-01";
				$nxt_yr=(date("Y")+1)."-03-31";
			}
			else
			{
				$curryear=(date("Y")-1)."-04-01";
				$nxt_yr=date("Y")."-03-31";
			}

			// $accounting_type=fetch_account_groupId($soc_key, $account_group);

			// function fetch_account_groupId($soc_key, $group_id)

			// $fetch_group=mysqli_query($conn, "SELECT `account_type` FROM `accounting_groups` WHERE `id`='$account_group'");

			$accounting_type="";

			$acc_condtnArr['`accounting_groups`.`id`']=$account_group;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="accounting_groups.account_type", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr=array(), $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$fetch_group=$query['userdata'];

			if(!empty($fetch_group))
			{
				$accounting_type=$fetch_group['account_type'];
			}
			
			// $accounting_id=fetch_account_headLatest($soc_key, $accounting_type);

			// function fetch_account_headLatest($soc_key, $acc_type)

			$accounting_id="";

			if($accounting_type=="Assets")
			{
				$id=3000;
			}

			if($accounting_type=="Liability")
			{
				$id=4000;
			}

			if($accounting_type=="income")
			{
				$id=1000;
			}

			if($accounting_type=="expenses")
			{
				$id=2000;
			}
			if($accounting_type=="Suspnse")
			{
				$id=5000;
			}

			// $fetch_id=mysqli_query($conn, "SELECT `accounting_id` FROM `accounting_types` WHERE `accounting_type`='$accounting_type' ORDER BY `accounting_id` DESC LIMIT 1");

			$acc1_condtnArr['`accounting_types`.`accounting_type`']=$accounting_type;
			$acc1_orderByArr['`accounting_types`.`accounting_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id", $acc1_condtnArr, $likeCondtnArr=array(), $acc1_joinArr=array(), $singleRow=TRUE, $acc1_orderByArr, $acc1_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$fetch_id=$query['userdata'];

			if(!empty($fetch_id))
			{
				$id=$fetch_id['accounting_id'];
			}

			$accounting_id=++$id;

			// $insert_head=mysqli_query($conn,"INSERT INTO `accounting_types`(`accounting_id`, `accounting_type`, `accounting_name`, `Account_group`,`opening_balance`, `type_ledger`) VALUES ('$accounting_id','$accounting_type','$head_name','$account_group','$amount','$cr_dr')");

			$headInsertArr[]=array(
				'accounting_id'=>$accounting_id,
				'accounting_type'=>$accounting_type,
				'accounting_name'=>$head_name,
				'Account_group'=>$account_group,
				'opening_balance'=>$amount,
				'type_ledger'=>$cr_dr,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_types_tbl, $headInsertArr, $returnType="");

			$headInsertQuery=$query['status'];

			$response="";

			if($headInsertQuery==TRUE)
			{
				// $opening_bal_head=mysqli_query($conn,"INSERT INTO `accounting_opening_balance`(`accounting_id`, `from_date`, `to_date`, `opening_balance`,`account_nature`) VALUES ('$accounting_id','$curryear','$nxt_yr','$amount','$cr_dr')");

				$opBalInsertArr[]=array(
					'accounting_id'=>$accounting_id,
					'from_date'=>$curryear,
					'to_date'=>$nxt_yr,
					'opening_balance'=>$amount,
					'account_nature'=>$cr_dr,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->accounting_opening_balance_tbl, $opBalInsertArr, $returnType="lastInsertId");

				$opBalInsertQuery=$query['userdata']['insertedId'];

				$last_id = $opBalInsertQuery;
				$response="Account head added sucessfully";
			}

			if(!empty($response))
			{
				$this->session->set_flashdata('success_msg', 'Opening Balance has added sucessfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!');
				redirect(base_url()."accounting/home", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/home", 'refresh');
		}
    }

    public function create_group()
    {
    	$submit_type=$this->input->post('submit_type');

		if(!empty($submit_type))
		{
	    	$expiration_time=(int)(time()+60*60);
	    	
	    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);
	    }

		$group_name=$this->input->post('group_name');
		$group_type=$this->input->post('group_type');

		$response="";

		if(!empty($group_name) && !empty($group_type))
		{
			// $result=add_account_group($soc_key,$group_name,$group_type);

		    // $add_group=mysqli_query($conn,"INSERT INTO `accounting_groups` (`name`, `account_type`) VALUES ('$group_name','$group_type')");

		    $accGrpInsertArr[]=array(
				'name'=>$group_name,
				'account_type'=>$group_type,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_groups_tbl, $accGrpInsertArr, $returnType="");

			$accGrpInsertQuery=$query['status'];

		    if($accGrpInsertQuery==TRUE)
		    {
		    	$response="Group added sucessfully";
		    }

			if(!empty($response))
			{
				$this->session->set_flashdata('success_msg', 'Group has added sucessfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!');
				redirect(base_url()."accounting/home", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/home", 'refresh');
		}
    }

    public function add_bank()
    {
    	$submit_type=$this->input->post('submit_type');

		if(!empty($submit_type))
		{
	    	$expiration_time=(int)(time()+60*60);
	    	
	    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);
	    }

		$head_bk_name=$this->input->post('head_bk_name');
		$bankName=$this->input->post('bank_name');
		$bank_branch=$this->input->post('bank_br');
		$bank_account_number=$this->input->post('bk_acc_no');
		$bank_ifsc=$this->input->post('bk_ifsc');
		$bank_micr=$this->input->post('bk_micr');
		$bk_balance=$this->input->post('bk_balance');

		// $insert_bank=mysqli_query($conn,"INSERT INTO `society_bank_master`(`bank_id`, `bank_name`, `bank_branch`, `bank_micr`, `bank_ifsc`, `bank_account_number`) VALUES ('$head_bk_name','$bankName', '$bank_branch', '$bank_micr', '$bank_ifsc', '$bank_account_number')");

		$bankInsertArr[]=array(
			'bank_id'=>$head_bk_name,
			'bank_name'=>$bankName,
			'bank_branch'=>$bank_branch,
			'bank_micr'=>$bank_micr,
			'bank_ifsc'=>$bank_ifsc,
			'bank_account_number'=>$bank_account_number,
			'createdBy'=>$this->user_name,
			'createdDatetime'=>$this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->society_bank_master_tbl, $bankInsertArr, $returnType="");

		$bankInsertQuery=$query['status'];

	    if($bankInsertQuery==TRUE)
	    {
			$this->session->set_flashdata('success_msg', 'Bank has added sucessfully');
			redirect(base_url()."accounting/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!');
			redirect(base_url()."accounting/home", 'refresh');
		}
    }

    public function maintainence_accounting()
    {
    	$this->db->trans_start();

    	$submit_type=$this->input->post('submit_type');

    	$expiration_time=(int)(time()+60*60);
    	
    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);

		$trannumber=$this->input->post('trannumber');
		$rv_name=$this->input->post('rv_name');
		$vc_number=$this->input->post('vc_number');
		$Dr=$this->input->post('Dr');
		$bank_head=$this->input->post('bank_head');
		$Cr=$this->input->post('Cr');
		$maint_member=$this->input->post('maint_member');
		$lentharr=$this->input->post('lentharr');
		$total_amt=$this->input->post('total_amt');
		$narratn=$this->input->post('narratn');
		$Payment=$this->input->post('Payment');
		$paying_amt=$this->input->post('paying_amt');
		$bal_amt=$this->input->post('bal_amt');
		$rcpdate=$this->input->post('s-date');
		$chkno=$this->input->post('chkno');
		$bank_name=$this->input->post('bank_name');
		$ifsccode=$this->input->post('ifsccode');
		$Micr=$this->input->post('Micr');

        $current_date=date("Y-m-d");

        // $recipt_qry=mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cr_dr`, `s-des`, `member_id`, `cheque_no`, `accounting_recipt`, `s-indent`, `bank_name`, `ifsc`, `micr`, `payment_mode`) VALUES ('$bank_head', '$paying_amt', '$current_date', '$trannumber', '$rv_name', '$vc_number', 'Dr', '$narratn', '$maint_member', '$chkno', '', '$rcpdate', '$bank_name', '$ifsccode', '$Micr', '$Payment')");

        $rcptInsertArr[]=array(
			'accouting_id'=>$bank_head,
			'accounting_charges'=>$paying_amt,
			's-date'=>$current_date,
			'transaction_no'=>$trannumber,
			'vaucher_name'=>$rv_name,
			'vaucher_no'=>$vc_number,
			'cr_dr'=>'Dr',
			's-des'=>$narratn,
			'member_id'=>$maint_member,
			'cheque_no'=>$chkno,
			'accounting_recipt'=>'',
			's-indent'=>$rcpdate,
			'bank_name'=>$bank_name,
			'ifsc'=>$ifsccode,
			'micr'=>$Micr,
			'payment_mode'=>$Payment,
			'createdBy'=>$this->user_name,
			'createdDatetime'=>$this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $rcptInsertArr, $returnType="");
        
        // $acc_qry=mysqli_query($conn, "SELECT `accounting_id` FROM `accounting_types` WHERE `accounting_name`='$maint_member' AND `accounting_type`='Assets' AND `Account_group`='13' ");

        // $acc_user=mysqli_fetch_assoc($acc_qry);
        
        // $accId=$acc_user['accounting_id'];

        $a_u_condtnArr['`accounting_types`.`accounting_name`']=$maint_member;
		$a_u_condtnArr['`accounting_types`.`accounting_type`']="Assets";
		$a_u_condtnArr['`accounting_types`.`Account_group`']="13";

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id", $a_u_condtnArr, $likeCondtnArr=array(), $a_u_joinArr=array(), $singleRow=TRUE, $a_u_orderByArr=array(), $a_u_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$acc_user=$query['userdata'];
        
        $rcptInsertQuery1=FALSE;

        if(!empty($acc_user))
        {
        	$accId=$acc_user['accounting_id'];
	        // $recipt_qry=mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `transaction_no`, 
	        // `vaucher_name`, `vaucher_no`,`cr_dr`,`s-des`,`member_id`, `cheque_no`, `accounting_recipt`,`s-indent`,`bank_name`, `ifsc`, `micr`,`payment_mode`) VALUES
	        // ('$accId', '$paying_amt', '$current_date','$trannumber','$rv_name','$vc_number','Cr','$narratn','$maint_member','$chkno','','$rcpdate','$bank_name','$ifsccode','$Micr','$Payment')");

	        $rcptInsertArr1[]=array(
				'accouting_id'=>$accId,
				'accounting_charges'=>$paying_amt,
				's-date'=>$current_date,
				'transaction_no'=>$trannumber,
				'vaucher_name'=>$rv_name,
				'vaucher_no'=>$vc_number,
				'cr_dr'=>'Cr',
				's-des'=>$narratn,
				'member_id'=>$maint_member,
				'cheque_no'=>$chkno,
				'accounting_recipt'=>'',
				's-indent'=>$rcpdate,
				'bank_name'=>$bank_name,
				'ifsc'=>$ifsccode,
				'micr'=>$Micr,
				'payment_mode'=>$Payment,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $rcptInsertArr1, $returnType="");

			$rcptInsertQuery1=$query['status'];
		}
        
        if($rcptInsertQuery1==TRUE)
        {
            $pay_month=date("F",strtotime($current_date));
            // $fetch_maint_bill=mysqli_query($conn,"SELECT `tansaction_no` FROM `maintance` WHERE `user_name`='$maint_member' AND `status`='unpaid' ORDER BY `start_day` ASC LIMIT $lentharr");

            $bill_condtnArr['`maintenance`.`user_name`']=$maint_member;
			$bill_condtnArr['`maintenance`.`status`']="unpaid";
			$bill_orderByArr['`maintenance`.`start_day`']="ASC";

			$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.tansaction_no", $bill_condtnArr, $likeCondtnArr=array(), $bill_joinArr=array(), $singleRow=FALSE, $bill_orderByArr, $bill_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit=$lentharr);

			$fetch_maint_bill=$query['userdata'];

            if(!empty($fetch_maint_bill))
            {
                // while($fetch_maint=mysqli_fetch_assoc($fetch_maint_bill))
                foreach($fetch_maint_bill AS $e_maint)
                {
                    $tansaction_no=$e_maint['tansaction_no'];

                    // $update_bill_status=mysqli_query($conn,"UPDATE `maintance` SET `status`='Paid',
                    // `Pay_month`='$pay_month',`bank_name`='$bank_name',`cheque_no`='$chkno',`date`='$current_date',`tansaction_no1`='$trannumber',
                    // `voucher_no1`='$vc_number',`voucher_name1`='$rv_name' WHERE `user_name`='$maint_member' AND `tansaction_no`='$tansaction_no'");
                    $bill_UpdateArr=array();

                    $bill_UpdateArr=array(
						'status'=>'Paid',
						'Pay_month'=>$pay_month,
						'bank_name'=>$bank_name,
						'cheque_no'=>$chkno,
						'date'=>$current_date,
						'tansaction_no1'=>$trannumber,
						'voucher_no1'=>$vc_number,
						'voucher_name1'=>$rv_name,
						'updatedBy'=>$this->user_name,
						'updatedDatetime'=>$this->curr_datetime
					);

					$bill_condtnArr['maintenance.user_name']=$maint_member;
					$bill_condtnArr['maintenance.tansaction_no']=$tansaction_no;

					$query=$this->Mcommon->update($tableName=$this->maintenance_tbl, $bill_UpdateArr, $bill_condtnArr, $likeCondtnArr=array());
                }
            }
            
            // $advance_user=mysqli_query($conn,"UPDATE `user_arrears` SET `Arrears_amount_pending`='$bal_amt',`date_creation`='$log_date' WHERE `user_id`='$maint_member'");
            $advUserUpdateArr=array();

            $advUserUpdateArr=array(
				'Arrears_amount_pending'=>$bal_amt,
				'date_creation'=>$this->curr_datetime,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$advUserCondtnArr['user_arrears.user_id']=$maint_member;

			$query=$this->Mcommon->update($tableName=$this->user_arrears_tbl, $advUserUpdateArr, $advUserCondtnArr, $likeCondtnArr=array());
           
            // $advance_user_log=mysqli_query($conn,"INSERT INTO `members_advance_log`(`user_id`, `advance_amount`, `creation_date`) VALUES ('$maint_member','$bal_amt','$log_date')");
			$mbrInsertArr=array();

            $mbrInsertArr[]=array(
				'user_id'=>$maint_member,
				'advance_amount'=>$bal_amt,
				'creation_date'=>$this->curr_datetime,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->members_advance_log_tbl, $mbrInsertArr, $returnType="");

			$responseData=TRUE;
        }
        else
        {
        	$responseData=FALSE;
        }

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
			redirect(base_url()."accounting/home", 'refresh');
		}
		else
		{
			if($responseData==TRUE)
				$this->session->set_flashdata('success_msg', 'Reciept has generated successfully');
			else
				$this->session->set_flashdata('error_msg', 'Something went wrong! Reciept Not Generated.');

			redirect(base_url()."accounting/home", 'refresh');
		}   
    }

    public function day_book()
    {
    	// $this->data['from_date']=$from_date=date("Y-m-01");
     //   	$this->data['to_date']=$to_date=date("Y-m-t");

       	$this->data['from_date']=$from_date=date("Y-m-d",strtotime($this->input->post('from_date')));
       	$this->data['to_date']=$to_date=date("Y-m-d",strtotime($this->input->post('to_date')));

		if(isset($_GET['AccId']))
		{
			$Acc_id=$this->input->get('AccId');
			// $ladger_data=fetch_ladger_data($soc_key, $Acc_id, $from_date, $to_date);
   
			if($Acc_id=="all")
			{
				// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `cr_dr`=( CASE 
				// WHEN `vaucher_name`='Payment Voucher' THEN 'Dr'
				// WHEN `vaucher_name`='Reciept Voucher' THEN 'Cr' 
				// WHEN `vaucher_name`='Contra Voucher' THEN 'Dr' 
				// WHEN `vaucher_name`='Journal Voucher' THEN 'Dr' 
				// END ) AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");

				// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
				// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
				// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
				// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
				// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
				// 	END)";

				$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
					WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
					WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
					WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
					WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
					END))";
				$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

				$acchd_orderByArr['`accounting`.`s-date`']="DESC";
				$acchd_orderByArr['`accounting`.`a_id`']="DESC";

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

				$ladger_data=$query['userdata']; 
		   }
		   else
		   {
		   		// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` =   `accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `accouting_id`='$accId' AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");  
		   		$acchd_condtnArr['`accounting`.`accouting_id`']=$Acc_id;

		   		$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

				$acchd_orderByArr['`accounting`.`s-date`']="DESC";
				$acchd_orderByArr['`accounting`.`a_id`']="DESC";

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr, $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

				$ladger_data=$query['userdata'];  
		   }
		}
		elseif(isset($_GET['GroupId']))
		{
			$GroupId=$this->input->get('GroupId');
			// $ladger_data=fetch_ladger_data_Group($soc_key, $GroupId, $from_date, $to_date);

			$a_u_condtnArr['`accounting_types`.`Account_group`']=$GroupId;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id, accounting_types.accounting_name", $a_u_condtnArr, $likeCondtnArr=array(), $a_u_joinArr=array(), $singleRow=FALSE, $a_u_orderByArr=array(), $a_u_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$ladger_data=$query['userdata'];
		}
		else
		{
			// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
			// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
			// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
			// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
			// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
			// 	END)";

			$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
				WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
				WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
				WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
				WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
				END))";

			$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

			$acchd_orderByArr['`accounting`.`s-date`']="DESC";
			$acchd_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

			$ladger_data=$query['userdata']; 
		}

		$this->data['ladger_data']=$ladger_data;

		// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `vaucher_no`='$vaucharId' ");   

		// echo "SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting`";

		// $voc_condtnArr['`accounting_types`.`vaucher_no`']=$vaucharId;

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` = `accounting`.`accouting_id` LIMIT 1) AS headName", $voc_condtnArr=array(), $likeCondtnArr=array(), $voc_joinArr=array(), $singleRow=FALSE, $voc_orderByArr=array(), $voc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$voucher_data=$query['userdata'];

		$voucherArr=array();

		if(!empty($voucher_data))
		{
			foreach($voucher_data AS $e_voc) 
			{
				$voucherArr[$e_voc['vaucher_no']]=$e_voc;
			}
		}

		$this->data['voucherArr']=$voucherArr;

    	$this->data['view']="accounting/day_book";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function update_acc()
    {
    	$this->data['updateType']=$updateType=$this->uri->segment('3');
    	$this->data['updateId']=$updateId=$this->uri->segment('4');

    	$expiration_time=(int)(time()+60*60);

    	if($updateType=='updateAccId')
	    	$updateMenu="op_bal_tab";
	    elseif($updateType=='updateGroupId')
	    	$updateMenu="grp_tab";
	    elseif($updateType=='updateBankId')
	    	$updateMenu="bank_tab";
    	
    	$this->input->set_cookie('submit_type_cookie', $updateMenu, $expiration_time);
	    $this->input->set_cookie('submit_updatetype_cookie', $updateType, $expiration_time);
	    $this->input->set_cookie('submit_updateid_cookie', $updateId, $expiration_time);

	    if($updateType=='updateAccId')
	    	$pageMenu="Opening Balance";
	    elseif($updateType=='updateGroupId')
	    	$pageMenu="Groups";
	    elseif($updateType=='updateBankId')
	    	$pageMenu="Bank Details";

	    $breadcrumbArr[0]['name']="Accounting";
    	$breadcrumbArr[0]['link']=base_url()."accounting/home";
    	$breadcrumbArr[0]['active']=TRUE;

	    $breadcrumbArr[1]['name']=$pageMenu;
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$acc_grp_orderByArr['`accounting_groups`.`name`']="ASC";

		$query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`accounting_groups`.`id`, `accounting_groups`.`name`, `accounting_groups`.`account_type`, `accounting_groups`.`extra`", $acc_grp_condtnArr=array(), $acc_grp_likeCondtnArr=array(), $acc_grp_joinArr=array(), $singleRow=FALSE, $acc_grp_orderByArr, $acc_grp_groupByArr=array(), $whereInArray=array(), $acc_grp_customWhereArray=array(), $backTicks=TRUE);

		$accounting_group_data=$query['userdata'];

		$this->data['accounting_group_data']=$accounting_group_data;

    	$fetch_tbl_accId_head=array();
    	$accounting_groupId_data=array();
    	$fetch_tbl_details_bankId=array();

    	if($updateType=='updateAccId')
    	{
    		$todays=date("Y-m-d");
	       	$updateAccId=$updateId;

	       $acchd_condtnArr['`accounting_types`.`accounting_id`']=$updateAccId;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="`id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`, (SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND ('".$todays."' BETWEEN `from_date` AND `to_date`)) AS opening_balance,(SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND ('$todays' BETWEEN `from_date` AND `to_date`)) AS type_ledger,(SELECT `name` FROM ".$this->accounting_groups_tbl." WHERE `accounting_groups`.`id` = `accounting_types`.`Account_group` OR `accounting_groups`.`name` = `accounting_types`.`Account_group` LIMIT 1) AS Groupname,(SELECT `extra` FROM ".$this->accounting_groups_tbl." WHERE `accounting_groups`.`id` = `accounting_types`.`Account_group` LIMIT 1) AS Groupnature", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=TRUE, $acchd_orderByArr=array(), $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

			$fetch_tbl_accId_head=$query['userdata']; 
	    }

	    $this->data['fetch_tbl_accId_head']=$fetch_tbl_accId_head;

	    if($updateType=='updateGroupId')
	    { 
	        $updateGroupId=$updateId;

	        $grp_condtnArr['`accounting_groups`.`id`']=$updateGroupId;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`", $grp_condtnArr, $likeCondtnArr=array(), $grp_joinArr=array(), $singleRow=TRUE, $grp_orderByArr=array(), $grp_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$accounting_groupId_data=$query['userdata'];
	    }

	    $this->data['accounting_groupId_data']=$accounting_groupId_data;

	    if($updateType=='updateBankId')
	    { 
	        $updateBankId=$updateId;

	        $bank_condtnArr['`society_bank_master`.`bank_id`']=$updateBankId;

			$query=$this->Mcommon->getRecords($tableName=$this->society_bank_master_tbl, $colNames="`society_bank_master`.`bank_id`, `society_bank_master`.`bank_name`, `society_bank_master`.`bank_branch`, `society_bank_master`.`bank_account_number`, `society_bank_master`.`bank_ifsc`, `society_bank_master`.`bank_micr`", $bank_condtnArr, $likeCondtnArr=array(), $bank_joinArr=array(), $singleRow=TRUE, $bank_orderByArr=array(), $bank_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$fetch_tbl_details_bankId=$query['userdata'];
	    }

	    $this->data['fetch_tbl_details_bankId']=$fetch_tbl_details_bankId;

	    $this->data['view']="accounting/update_acc";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }    

    public function add_last_pnl()
    {
    	$submit_type=$this->input->post('submit_type');

		if(!empty($submit_type))
		{
	    	$expiration_time=(int)(time()+60*60);
	    	
	    	$this->input->set_cookie('submit_type_cookie', $submit_type, $expiration_time);
	    }

	    $breadcrumbArr[0]['name']="Accounting";
    	$breadcrumbArr[0]['link']=base_url()."accounting/home";
    	$breadcrumbArr[0]['active']=TRUE;

	    $breadcrumbArr[1]['name']="Add Last PnL";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

	    $this->form_validation->set_rules('cr_dr', 'CR DR', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
	    	$this->data['view']="accounting/home";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
			$to_day=date("Y-m-d");
			$pnl=$this->input->post('pnl');
            $cr_dr=$this->input->post('cr_dr');

			// $fetch_pnl=mysqli_query("SELECT `srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType FROM `pnl_acc_tbl` WHERE '$to_day' BETWEEN `from_date` AND `to_date`");

			$pnl_customWhereArray[]="( '".$to_day."' BETWEEN `from_date` AND `to_date`)";

			$query=$this->Mcommon->getRecords($tableName=$this->pnl_acc_tbl, $colNames="`srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType", $pnl_condtnArr=array(), $likeCondtnArr=array(), $pnl_joinArr=array(), $singleRow=FALSE, $pnl_orderByArr=array(), $pnl_groupByArr=array(), $whereInArray=array(), $pnl_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

			$pnl_sql=$query['userdata']; 

			$response="";

			if(count($pnl_sql)==0)
			{
				$currmon= date("m");
				if ($currmon>=4) 
				{
					$curryear= date("Y")."-04-01";
					$nxt_yr=(date("Y")+1)."-03-31";
				}
				else
				{
					$curryear= (date("Y")-1)."-04-01";
					$nxt_yr=date("Y")."-03-31";
				}

				// $update_pnl=mysqli_query($conn,"INSERT INTO `pnl_acc_tbl`(`ammount`, `from_date`, `to_date`, `acc_nature`) VALUES ('$pnl','$curryear','$nxt_yr','$cr_dr')");

				$pnlInsertArr1[]=array(
					'ammount'=>$pnl,
					'from_date'=>$curryear,
					'to_date'=>$nxt_yr,
					'acc_nature'=>$cr_dr,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->pnl_acc_tbl, $pnlInsertArr1, $returnType="");

				if($query['status']==TRUE)
				{
					$response="pnl updated";
				}
			}

			if($response!="")
			{
				$this->session->set_flashdata('success_msg', 'PnL has generated successfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! PnL not Updated.');
				redirect(base_url()."accounting/home", 'refresh');
			}
		}
    }

    public function update_acchead()
    {
    	$response="";
    	$updateType=$this->input->cookie('submit_updatetype_cookie');
	    $updateId=$this->input->cookie('submit_updateid_cookie');
    	$todays=date("Y-m-d");

		$headId=$this->input->post('headId');
		$head_name=$this->input->post('head_name');
		$account_group=$this->input->post('account_group');
		$amount=$this->input->post('amount');
		$cr_dr=$this->input->post('cr_dr');

		// Array ( [head_name] => Accounts Charges [account_group] => 8 [amount] => 0 [cr_dr] => Dr [add_account_head] => ) 
		if(!empty($head_name) && !empty($account_group) && !empty($amount) && !empty($cr_dr))
		{
			// $fetch_group=mysqli_query($conn,"SELECT `account_type` FROM `accounting_groups` WHERE `id`='$group_id'");

			$acc_condtnArr['`accounting_groups`.`id`']=$account_group;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="accounting_groups.account_type", $acc_condtnArr, $likeCondtnArr=array(), $acc_joinArr=array(), $singleRow=TRUE, $acc_orderByArr=array(), $acc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$fetch_group=$query['userdata'];

			$accounting_type=$fetch_group['account_type'];

			// $insert_head=mysqli_query($conn,"UPDATE `accounting_types` SET `accounting_type`='$accounting_type',`accounting_name`='$head_name',`Account_group`='$account_group',`opening_balance`='$amount',`type_ledger`='$cr_dr' WHERE `accounting_id`='$headId'");

			$accTyUpdateArr=array(
				'accounting_type'=>$accounting_type,
				'accounting_name'=>$head_name,
				'Account_group'=>$account_group,
				'opening_balance'=>$amount,
				'type_ledger'=>$cr_dr,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$accTyCondtnArr['accounting_types.accounting_id']=$headId;

			$query=$this->Mcommon->update($tableName=$this->accounting_types_tbl, $accTyUpdateArr, $accTyCondtnArr, $likeCondtnArr=array());

			if($query['status']==TRUE)
			{
				// $opening_bal_head=mysqli_query($conn,"UPDATE `accounting_opening_balance` SET `opening_balance`='$amount',`account_nature`='$cr_dr' WHERE `accounting_id`='$headId' AND ('$todays' BETWEEN `from_date` AND `to_date`)");

				$accTyUpdateArr1=array(
					'opening_balance'=>$amount,
					'account_nature'=>$cr_dr,
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$accTyCondtnArr1['accounting_opening_balance.accounting_id']=$headId;

				$accTycustomWhereArray1[]="( '".$todays."' BETWEEN `from_date` AND `to_date`)";

				$query=$this->Mcommon->update($tableName=$this->accounting_opening_balance_tbl, $accTyUpdateArr1, $accTyCondtnArr1, $likeCondtnArr=array(), $accTycustomWhereArray1);

				$response="Account head added sucessfully";
			}

			if(!empty($response))
			{	
				$this->session->set_flashdata('success_msg', 'Opening Balance has updated successfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Opening Balance not updated.');
				redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
		}
    }

    public function update_group()
    {
    	$response="";
    	$updateType=$this->input->cookie('submit_updatetype_cookie');
	    $updateId=$this->input->cookie('submit_updateid_cookie');

		$groupId=$this->input->post('groupId');
		$group_name=$this->input->post('group_name');
		$group_type=$this->input->post('group_type');

		if(!empty($groupId) && !empty($group_name) && !empty($group_type))
		{
			// $result=update_data_Group($soc_key,$groupId,$group_name,$group_type);

			// $update_group=mysqli_query($conn,"UPDATE `accounting_groups` SET `name`='$group_name',`account_type`='$group_type' WHERE `id`='$groupId'");

			$accGrpUpdateArr=array(
				'name'=>$group_name,
				'account_type'=>$group_type,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$accGrpCondtnArr['accounting_groups.id']=$groupId;

			$query=$this->Mcommon->update($tableName=$this->accounting_groups_tbl, $accGrpUpdateArr, $accGrpCondtnArr, $likeCondtnArr=array());

			if($query['status']==TRUE)
			{	
				$this->session->set_flashdata('success_msg', 'Group has updated successfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Group not updated.');
				redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
		}
    }

    public function update_bank()
    {
    	$updateType=$this->input->cookie('submit_updatetype_cookie');
	    $updateId=$this->input->cookie('submit_updateid_cookie');

		$bankId=$this->input->post('bankId');
		$bank_name=$this->input->post('bank_name');
		$bank_br=$this->input->post('bank_br');
		$bk_acc_no=$this->input->post('bk_acc_no');
		$bk_ifsc=$this->input->post('bk_ifsc');
		$bk_micr=$this->input->post('bk_micr');

		if(!empty($bankId) && !empty($bank_name) && !empty($bank_br) && !empty($bk_acc_no) && !empty($bk_ifsc) && !empty($bk_micr))
		{
			// $result=update_data_bank($soc_key,$bankId,$bank_name,$bank_br,$bk_acc_no,$bk_ifsc,$bk_micr);

			// $update_bank=mysqli_query($conn,"UPDATE `society_bank_master` SET `bank_name`='$bank_name',`bank_branch`='$bank_br',`bank_micr`='$bk_micr',`bank_ifsc`='$bk_ifsc',`bank_account_number`='$bk_acc_no' WHERE `bank_id`='$bankId'") or die(mysqli_error($conn));

			$bnkUpdateArr=array(
				'bank_name'=>$bank_name,
				'bank_branch'=>$bank_br,
				'bank_micr'=>$bk_micr,
				'bank_ifsc'=>$bk_ifsc,
				'bank_account_number'=>$bk_acc_no,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$bnkCondtnArr['society_bank_master.bank_id']=$bankId;

			$query=$this->Mcommon->update($tableName=$this->society_bank_master_tbl, $bnkUpdateArr, $bnkCondtnArr, $likeCondtnArr=array());

			if($query['status']==TRUE)
			{	
				$this->session->set_flashdata('success_msg', 'Bank Information has updated successfully');
				redirect(base_url()."accounting/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Bank Information not updated.');
				redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."accounting/update_acc/".$updateType."/".$updateId, 'refresh');
		}
    }

    public function view_data()
    {
    	if(isset($_GET['AccId']))
		{ 
			$pageMenu='Account head'; 
			$submitType="op_bal_tab";
		}
		elseif(isset($_GET['GroupId']))
		{ 
			$pageMenu='Group';	
			$submitType="grp_tab";
		}
		else
		{ 
			$pageMenu='Day Book'; 
			$submitType="daybook_tab";
		}  

		if(!empty($submitType))
		{
	    	$expiration_time=(int)(time()+60*60);
	    	
	    	$this->input->set_cookie('submit_type_cookie', $submitType, $expiration_time);
	    }

    	$breadcrumbArr[0]['name']="Accounting";
    	$breadcrumbArr[0]['link']=base_url()."accounting/home";
    	$breadcrumbArr[0]['active']=TRUE;

	    $breadcrumbArr[1]['name']=$pageMenu;
    	$breadcrumbArr[1]['link']=base_url()."accounting/home";
    	$breadcrumbArr[1]['active']=TRUE;

    	$breadcrumbArr[2]['name']=ucwords("View ".$pageMenu);
    	$breadcrumbArr[2]['link']="javascript:void(0);";
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	$this->data['submitType']=$submitType;

    	$viewType="";
	    $typeId="";
	    $getStringParams="";

    	if(!empty($this->input->get()))
    	{
	    	$viewType=key($this->input->get());
	    	$typeId=$this->input->get($viewType);
			
			$getStringParams="?".$viewType."=".$typeId;
		}

		$this->data['getStringParams']=$getStringParams;

		$this->form_validation->set_rules('from_date', 'From Date', 'trim|required');
		$this->form_validation->set_rules('to_date', 'To Date', 'trim|required');

    	$this->data['from_date_day_bk']=$from_date_day_bk=$this->input->post('from_date');
		$this->data['to_date_day_bk']=$to_date_day_bk=$this->input->post('to_date');

		$ladger_data=array();

		if($from_date_day_bk!="" || $to_date_day_bk!="")
		{
			$this->data['from_date']=$from_date=date("Y-m-d",strtotime($from_date_day_bk));
	       	$this->data['to_date']=$to_date=date("Y-m-d",strtotime($to_date_day_bk));

			if(isset($_GET['AccId']))
			{
				$Acc_id=$this->input->get('AccId');
				// $ladger_data=fetch_ladger_data($soc_key, $Acc_id, $from_date, $to_date);
	   
				if($Acc_id=="all")
				{
					// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `cr_dr`=( CASE 
					// WHEN `vaucher_name`='Payment Voucher' THEN 'Dr'
					// WHEN `vaucher_name`='Reciept Voucher' THEN 'Cr' 
					// WHEN `vaucher_name`='Contra Voucher' THEN 'Dr' 
					// WHEN `vaucher_name`='Journal Voucher' THEN 'Dr' 
					// END ) AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");

					// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
					// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
					// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
					// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
					// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
					// 	END)";

					$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
						WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
						WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
						WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
						WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
						END))";
					$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

					$acchd_orderByArr['`accounting`.`s-date`']="DESC";
					$acchd_orderByArr['`accounting`.`a_id`']="DESC";

					$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

					$ladger_data=$query['userdata']; 
			   }
			   else
			   {
			   		// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` =   `accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `accouting_id`='$accId' AND `s-date` BETWEEN '$from_date' AND '$to_date' ORDER BY `s-date` DESC,`a_id` DESC");  
			   		$acchd_condtnArr['`accounting`.`accouting_id`']=$Acc_id;

			   		$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

					$acchd_orderByArr['`accounting`.`s-date`']="DESC";
					$acchd_orderByArr['`accounting`.`a_id`']="DESC";

					$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr, $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

					$ladger_data=$query['userdata'];  
			   }
			}
			elseif(isset($_GET['GroupId']))
			{
				$GroupId=$this->input->get('GroupId');
				// $ladger_data=fetch_ladger_data_Group($soc_key, $GroupId, $from_date, $to_date);

				$a_u_condtnArr['`accounting_types`.`Account_group`']=$GroupId;

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="accounting_types.accounting_id, accounting_types.accounting_name", $a_u_condtnArr, $likeCondtnArr=array(), $a_u_joinArr=array(), $singleRow=FALSE, $a_u_orderByArr=array(), $a_u_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$group_data=$query['userdata'];

				$resultArr=array();
				$resultData=array();

				if(!empty($group_data))
				{
					foreach($group_data AS $e_grp)
					{
						$e_grp_Id=$e_grp['accounting_id'];
						$acchd_condtnArr['`accounting`.`accouting_id`']=$e_grp_Id;

				   		$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

						$acchd_orderByArr['`accounting`.`s-date`']="DESC";
						$acchd_orderByArr['`accounting`.`a_id`']="DESC";

						$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr, $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

						$grpData=$query['userdata'];  

						$ladger_data=array_merge($resultArr, $grpData);
					}
				}
			}
			else
			{
				// $acchd_condtnArr['`accounting`.`cr_dr`']="(CASE 
				// 	WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
				// 	WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
				// 	WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
				// 	WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
				// 	END)";

				$acchd_customWhereArray[]="( `accounting`.`cr_dr`=(CASE 
					WHEN `accounting`.`vaucher_name`='Payment Voucher' THEN 'Dr'
					WHEN `accounting`.`vaucher_name`='Reciept Voucher' THEN 'Cr' 
					WHEN `accounting`.`vaucher_name`='Contra Voucher' THEN 'Dr' 
					WHEN `accounting`.`vaucher_name`='Journal Voucher' THEN 'Dr' 
					END))";

				$acchd_customWhereArray[]="( `accounting`.`s-date` BETWEEN '".$from_date."' AND '".$to_date."')";

				$acchd_orderByArr['`accounting`.`s-date`']="DESC";
				$acchd_orderByArr['`accounting`.`a_id`']="DESC";

				$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id` LIMIT 1) AS headName", $acchd_condtnArr=array(), $likeCondtnArr=array(), $acchd_joinArr=array(), $singleRow=FALSE, $acchd_orderByArr, $acchd_groupByArr=array(), $whereInArray=array(), $acchd_customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

				$ladger_data=$query['userdata']; 
			}

			$this->data['ladger_data']=$ladger_data;

			// $fetch_acc_head_bank=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting` WHERE `vaucher_no`='$vaucharId' ");   

			// echo "SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`,(SELECT `accounting_name` FROM `accounting_types` WHERE `accounting_types`.`accounting_id`=`accounting`.`accouting_id`) AS headName FROM `accounting`";

			// $voc_condtnArr['`accounting_types`.`vaucher_no`']=$vaucharId;

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`, (SELECT `accounting_name` FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`accounting_id` = `accounting`.`accouting_id` LIMIT 1) AS headName", $voc_condtnArr=array(), $likeCondtnArr=array(), $voc_joinArr=array(), $singleRow=FALSE, $voc_orderByArr=array(), $voc_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$voucher_data=$query['userdata'];

			$voucherArr=array();

			if(!empty($voucher_data))
			{
				foreach($voucher_data AS $e_voc) 
				{
					$voucherArr[$e_voc['vaucher_no']]=$e_voc;
				}
			}

			$this->data['voucherArr']=$voucherArr;
		}

		if($this->form_validation->run() == FALSE)
		{
	    	$this->data['view']="accounting/view_data";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
	    	$this->data['view']="accounting/view_data";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
    }
}

?>