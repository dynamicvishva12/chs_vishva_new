<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Albums extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='ALBUM';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->album_tbl=$socTables['album_tbl'];
		$this->files_tbl=$socTables['files_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Albums';
    }

    public function list()
   	{
   		$breadcrumbArr[0]['name']="Albums";
    	$breadcrumbArr[0]['link']=base_url()."albums/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Album List";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$albm_orderByArr['`files`.`f-id`']='ASC';
		$albm_orderByArr['files.f-id']='ASC';
		$albm_groupByArr=array('album.a-albumname');

		$albm_joinArr[]=array("tbl"=>$this->files_tbl, "condtn"=>"album.a-albumname = files.f-album-name", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->album_tbl, $colNames="album.a-id AS albumnId, album.a-albumname AS albumnName, album.createdDatetime AS albumnCreateddate, album.al-description AS albumnDesc, CONCAT('".$this->assets_url."IMAGESDRIVE/', `files`.`f-uploads`) AS albumnFilename, album.a-username AS albumnUser , COUNT(`files`.`f-album-name`) AS total_count", $albm_condtnArr=array(), $likeCondtnArr=array(), $albm_joinArr, $singleRow=FALSE, $albm_orderByArr, $albm_groupByArr, $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$album_data=$query['userdata'];

		$this->data['album_data']=$album_data;

   		$this->data['view']="albums/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
   	}

   	public function gallery()
   	{
   		$this->data['album_name']=$album_name=$this->uri->segment('3');

   		$album_name=str_replace("_", " ", $album_name);

   		$breadcrumbArr[0]['name']="Albums";
    	$breadcrumbArr[0]['link']=base_url()."albums/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Album List";
    	$breadcrumbArr[1]['link']=base_url()."albums/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']=ucfirst($album_name);
    	$breadcrumbArr[2]['link']="javascript:void(0);";
    	$breadcrumbArr[2]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

   		$glry_condtnArr['files.f-album-name']=$album_name;
		$glry_orderByArr['files.f-id']='ASC';

		$query=$this->Mcommon->getRecords($tableName=$this->files_tbl, $colNames="files.f-id, CONCAT('".$this->assets_url."IMAGESDRIVE/Album/', files.`f-uploads`) AS albumnFilename", $glry_condtnArr, $likeCondtnArr=array(), $glry_joinArr=array(), $singleRow=FALSE, $glry_orderByArr, $glry_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$gallery_data=$query['userdata'];

		$this->data['gallery_data']=$gallery_data;

   		$this->data['view']="albums/gallery";
		$this->load->view('layouts/layout/main_layout', $this->data);
   	}

   	public function delete_photo()
   	{
   		$this->db->trans_start();

   		$album_name=$this->uri->segment('3');
   		$photo_name=$this->uri->segment('4');

   		// print_r($photo_name);
   		// die();

   		$response="";

   		if(!empty($photo_name))
   		{
   			$glry_condtnArr['files.f-id']=$photo_name;

			$query=$this->Mcommon->getRecords($tableName=$this->files_tbl, $colNames="files.f-uploads", $glry_condtnArr, $likeCondtnArr=array(), $glry_joinArr=array(), $singleRow=TRUE, $glry_orderByArr=array(), $glry_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$gallery_data=$query['userdata'];

			if(!empty($gallery_data))
			{
				$file=$gallery_data['f-uploads'];

				$file_path=$this->assets_url."IMAGESDRIVE/Album/".$file;

				if(file_exists($file_path))
				{
					if(!unlink($file_path))
					{
       					$response="File Not deleted";
       					$this->session->set_flashdata('warning_msg', 'File Not deleted');
					}
				}
				else
				{
					$response="File doesn't exists";
					$this->session->set_flashdata('warning_msg', "File doesn't exists");
				}

				$condtnArr['f-album-name']=$album_name;
				$condtnArr['f-id']=$photo_name;

				$query=$this->Mcommon->delete($tableName=$this->files_tbl, $condtnArr, $likeCondtnArr=array(), $whereInArray=array());

				$delResponse=$query['status'];

				if($delResponse==TRUE)
				{
					$response="Sucessfully Deleted";
					$this->session->set_flashdata('success_msg', 'Sucessfully Deleted');
				}
			}
   		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! File Not Deleted.');
		}
		else
		{
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
			redirect(base_url()."albums/gallery/".$album_name, 'refresh');
		}     

   	}
}

?>