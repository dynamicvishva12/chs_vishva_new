<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->data['base_url']=$this->config->item('base_url');
		$this->data['assets_url']=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['js']='layouts/include/js';

		$this->load->model('Mcommon', '', TRUE);
		$this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		if($_SERVER['SERVER_NAME']=='localhost')
		{
			$admin_db="aadharch_society_parameter";
		}
		else
		{
			$admin_db="aadharch_society_parameter";
		}

		$this->society_list=$admin_db.".society_list";
		$this->society_table=$admin_db.".society_table";

		$this->soc_database=$this->session->userdata('_society_database');

		if(isset($this->soc_database))
			$soc_db=$this->soc_database;
		else
			$soc_db="";

		if($soc_db!='')
		{
			$this->s_r_user_tbl=$soc_db.".s-r-user";
			$this->directory_tbl=$soc_db.".directory";
			$this->society_master_tbl=$soc_db.".society_master";
			$this->visitors_tbl=$soc_db.".visitors";
		}

		$this->logModule='LOGIN';
		$this->curr_datetime=date('Y-m-d H:i:s');
    }

	public function login()
	{
		$_society_key = $this->session->userdata('_society_key');
        $_society_database = $this->session->userdata('_society_database');
        $_society_status = $this->session->userdata('_society_status');
        $_user_name = $this->session->userdata('_user_name');
        $_user_type = $this->session->userdata('_user_type');
        $_designation_id = $this->session->userdata('_designation_id');

        if($_society_key!='' && $_society_status!='' && $_user_name!='' && $_user_type!='' && $_designation_id!='' && $_society_database!='')
        {
            redirect(base_url().'dashboard', 'refresh');
        }

		// $this->session->sess_destroy();
		$this->form_validation->set_rules('email', 'E-mail', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('societykey', 'Society Key', 'trim|required');

		if($this->form_validation->run() == FALSE)
        {
			$this->data['view']="auth/login";
			$this->load->view('layouts/layout/login_layout', $this->data);
		}
		else
		{
			$email=$this->input->post('email');
			$password=md5($this->input->post('password'));
			$societykey=md5($this->input->post('societykey'));

			$condtnArr['society_table.society_key']=$societykey;

			$query=$this->Mcommon->getRecords($tableName=$this->society_table, $colNames="society_table.society_key AS _society_key, society_table.society_database AS _society_database, society_table.status AS _society_status", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE);

			$soc_result=$query['userdata'];

			if(!empty($soc_result))
			{
				$this->session->set_userdata($soc_result);
				$this->load->database();

				$this->authenticate_user($email, $password);
			}
			else
			{
				// $this->Mlogger->log($logModule=$this->logModule, $logDescription="Invalid Society Key!", $userId=$email);

				$this->session->set_flashdata('msg', "Invalid Society Key!"); 
				redirect(base_url()."auth/login", 'refresh');
			}
		}
	}

	function authenticate_user($email, $password)
	{
		$_society_key = $this->session->userdata('_society_key');
		$_society_database = $this->session->userdata('_society_database');

		$this->s_r_user_tbl=$_society_database.".s-r-user";
		$this->directory_tbl=$_society_database.".directory";
		$this->society_master_tbl=$_society_database.".society_master";

		$condtnArr['s-r-user.s-r-email']=$email;

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, s-r-user.s-r-password, s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-profile, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.status", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userData=$query['userdata'];

		$isLogin=FALSE;

		if(!empty($userData))
		{
			$usr_name=$userData['s-r-username'];
			$usr_password=$userData['s-r-password'];
			$usr_fname=$userData['s-r-fname'];
			$usr_lname=$userData['s-r-lname'];
			$usr_email=$userData['s-r-email'];
			$usr_mobile=$userData['s-r-mobile'];
			$usr_profile=$userData['s-r-profile'];
			$usr_status=$userData['status'];

			if($usr_status=='Active')
			{
				if($password==$usr_password)
				{
					$dir_condtnArr['directory.important']=$email;
					$dir_condtnArr['directory.directory_type']='type1';
					$dir_condtnArr['directory.status']='Active';

					$customWhereArray[]="(directory.d-body='treasurer' OR directory.d-body='chairman' OR directory.d-body='secretory' OR directory.d-body='co-secretory')";

					$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="directory.d-id, directory.d-body", $dir_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray, $backTicks=TRUE);

					$dirData=$query['userdata'];

					if(!empty($dirData))
					{
						$query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="society_master.soc_name, society_master.soc_register_no, society_master.soc_address, society_master.bill_note", $sm_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE);

						$socMasterData=$query['userdata'];

						if(!empty($socMasterData))
						{
							$sess_data['_soceity_name']=$_soceity_name=$socMasterData['soc_name'];
						}

						$sess_data['_user_type']=$_user_type='admin';
						$sess_data['_user_profile']=$_user_profile=$usr_profile;
						$sess_data['_user_name']=$_user_name=$usr_name;
						$sess_data['_user_fname']=$_user_fname=$usr_fname;
						$sess_data['_user_lname']=$_user_lname=$usr_lname;
						$sess_data['_user_email']=$_user_email=$usr_email;
						$sess_data['_user_mobile']=$_user_mobile=$usr_mobile;
						$sess_data['_designation_id']=$_designation_id=$dirData['d-id'];
						$sess_data['_designation']=$_designation=$dirData['d-body'];

						$isLogin=TRUE;
						$this->session->set_userdata($sess_data);

						$this->Mlogger->log($logModule=$this->logModule, $logDescription='Successfully Logged In', $userId=$usr_name);
					}
					else
					{
						$return_msg="Username is incorrect";
					}
				}
				else
				{
					$return_msg="Unauthorize, Password doesn't match!!!";
				}
			}
			else
			{
				$return_msg="Access denied! User not active";
			}
		}
		else
		{
			$return_msg="User doesn't exists!!!";
		}

		if($isLogin)
		{
			redirect(base_url()."dashboard/", 'refresh');
		}
		else
		{
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$return_msg, $userId=$email);

			$this->session->unset_userdata('_society_key');
			$this->session->unset_userdata('_society_database');

			$this->session->set_flashdata('msg', $return_msg); 
			redirect(base_url()."auth/login", 'refresh');
		}

	}

	public function forgot_password()
	{
		$this->form_validation->set_rules('Email_id', 'Register Email', 'trim|required');
		$this->form_validation->set_rules('soc_key', 'Society Key', 'trim|required');

		if($this->form_validation->run() == FALSE)
        {
			$this->load->view('auth/forgot_password', $this->data);
		}
		else
		{
			$email=$this->input->post('Email_id');
			$soc_key=md5($this->input->post('soc_key'));

			if(!filter_var($email, FILTER_VALIDATE_EMAIL)) 
			{
				echo "<html><body><script>alert('Email format is invalid')</script></body></html>";
				redirect(base_url()."auth/forgot_password", 'refresh');
			}

			$query=$this->Msociety->verify_society($soc_key);

			$soc_check=$query['userdata'];
			
			if(!empty($soc_check))
			{
				$society_key=$soc_check['society_key'];

				$this->Msociety->set_society_session($society_key);

				$_society_database = $this->session->userdata('_society_database');

				$this->s_r_user_tbl=$_society_database.".s-r-user";

				$user_condtnArr['s-r-user.s-r-email']=$email;

				$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-email", $user_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$user_data=$query['userdata'];

				if(!empty($user_data))
				{
					$this->load->library('Auth_operation');
					$emailResponse=$this->auth_operation->forget_password($email, $society_key);

					if($emailResponse==TRUE)
					{
						$result="Successfully One Time Password Sent";
						$this->Mlogger->log($logModule="Forget Password", $logDescription=$result, $userId=$email);
					}
					else
					{
						$result="";
					}
				}	
				else
				{
					$result="Email Id Not Found";
					$this->Mlogger->log($logModule="Forget Password", $logDescription=$result, $userId=$email);
				}

				if(empty($result))
				{
					echo "<html><body><script>alert('Something went wrong')</script></body></html>";
				}
				else
				{
					if($result=="Email Id Not Found")
					{
						echo "<html><body><script>alert('Email Id Not Found')</script></body></html>";
						redirect(base_url()."auth/forgot_password", 'refresh');
					}
					else
					{
						$this->session->set_userdata('verify_email', $email);
						$this->session->set_userdata('verify_society_key', $society_key);

						echo "<html><body><script>alert('One Time Password Sent')</script></body></html>";
						redirect(base_url()."auth/onetime_password", 'refresh');
					}
				}
			}
			else
			{
				echo "<html><body><script>alert('Society key invalid')</script></body></html>";
				redirect(base_url()."auth/forgot_password", 'refresh');
			}
		}
	}

	public function onetime_password()
	{
		$this->form_validation->set_rules('Otp', 'OTP', 'trim|required');

		if($this->form_validation->run() == FALSE)
        {
			$this->load->view('auth/onetime_password', $this->data);
		}
		else
		{
			$Otp=$this->input->post('Otp');
			$verify_email=$this->session->userdata('verify_email');
			$verify_society_key=md5($this->session->userdata('verify_society_key'));

			$user_condtnArr['s-r-user.s-r-onetime']=$Otp;

			$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-email", $user_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$user_data=$query['userdata'];

			if(!empty($user_data))
			{
				$result="OTP match";
			}
			else
			{
				$result="INVALID One time Password";
			}

			$userUpdateArr=array(
	            's-r-onetime'=>"",
	            'updatedBy'=>'Admin',
	            'updatedDatetime'=>$this->curr_datetime
	        );

	        $updt_condtnArr['s-r-user.s-r-email']=$verify_email;
	        $updt_condtnArr['s-r-user.s-r-onetime']=$Otp;

	        $this->Mcommon->update($tableName=$this->s_r_user_tbl, $userUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

			$this->Mlogger->log($logModule="Forget Password", $logDescription=$result, $userId=$verify_email);

			if(empty($result))
			{
				echo "<html><body><script>alert('Something went wrong')</script></body></html>";
			}
			else
			{
				if($result=="INVALID One time Password")
				{
					echo "<html><body><script>alert('INVALID One time Password')</script></body></html>";
					redirect(base_url()."auth/onetime_password", 'refresh');
				}
				else
				{
					echo "<html><body><script>alert('OTP match')</script></body></html>";
					redirect(base_url()."auth/change_password", 'refresh');
				}
			}
		}
	}

	public function resend_otp()
	{
		$verify_email=$this->session->userdata('verify_email');
		$verify_society_key=md5($this->session->userdata('verify_society_key'));

		$this->load->library('Auth_operation');
		$emailResponse=$this->auth_operation->resend_otp($verify_email, $verify_society_key);

		if($emailResponse==TRUE)
		{
			$result="One Time Password Sent";

			echo "<html><body><script>alert('One Time Password Sent')</script></body></html>";

			$this->Mlogger->log($logModule="Forget Password", $logDescription=$result, $userId=$verify_email);
			redirect(base_url()."auth/onetime_password", 'refresh');
		}
		else
		{
			$result="One Time Password Not Sent";
			echo "<html><body><script>alert('One Time Password Not Sent')</script></body></html>";
		}
	}

	public function change_password()
	{
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE)
        {
			$this->load->view('auth/change_password', $this->data);
		}
		else
		{
			$password=$this->input->post('password');
			$verify_email=$this->session->userdata('verify_email');
			$verify_society_key=md5($this->session->userdata('verify_society_key'));

			// $result=ChangePass($_SESSION['femail'],md5($change_password),md5($_SESSION['soc_key']));

			$this->load->library('Auth_operation');
			$emailResponse=$this->auth_operation->change_password($verify_email, $password, $verify_society_key);

			if($emailResponse==TRUE)
			{
				echo "<html><body><script>alert('Something went wrong')</script></body></html>";
			}
			else
			{
				$this->Mlogger->log($logModule="Forget Password", $logDescription="Password Changed Successfully", $userId=$verify_email);
				echo "<html><body><script>alert('Password Changed Successfully')</script></body></html>";
				// redirect(base_url(), 'refresh');
			}
		}
	}

	public function visitors()
	{
		// $fetch= mysqli_query($conn,"SELECT * FROM `visitors` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `visitors`.`member_id` ORDER BY `sr_id` DESC")or die(mysqli_error($conn));

		$vstr_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`visitors`.`member_id`=`s-r-user`.`s-r-username`", "type"=>"left");

		$vstr_orderByArr['`sr_id`']="DESC";

		$query=$this->Mcommon->getRecords($tableName=$this->visitors_tbl, $colNames="member_id, s-r-fname, s-r-lname, name, no_person, date_visit, from_time, reason, `s-r-user`.`status`", $vstr_condtnArr=array(), $likeCondtnArr=array(), $vstr_joinArr, $singleRow=FALSE, $vstr_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $visitorsArr=$query['userdata'];

        $this->data['visitorsArr']=$visitorsArr; 

        $this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
        $this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

        $this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

        $this->data['society_userdata']=society_userdata();

        $this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
        $this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
        $this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
        $this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->data['view']="auth/visitors";
        $this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function connect_gatekeeper()
    {
        $societykey=$this->session->userdata('_society_key');

        $condtnArr['society_list.society_key']=$societykey;

        $query=$this->Mcommon->getRecords($tableName=ADMIN_DB.'.`society_list`', $colNames="society_list.society_key AS _gatekeeper_key, society_list.society_dbname AS _gatekeeper_database", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE);

        $gtkpr_result=$query['userdata'];

        if(!empty($gtkpr_result))
        {
            $this->session->set_userdata($gtkpr_result);
            $this->load->database();

            redirect(base_url().'gatekeeper/visitors/list', 'refresh');
        }
    }

	public function logout()
	{
		$this->session->unset_userdata('_society_key');
		$this->session->unset_userdata('_society_database');
		$this->session->unset_userdata('_society_status');
		$this->session->unset_userdata('_user_type');
		$this->session->unset_userdata('_user_profile');
		$this->session->unset_userdata('_user_name');
		$this->session->unset_userdata('_user_fname');
		$this->session->unset_userdata('_user_lname');
		$this->session->unset_userdata('_user_type');
		$this->session->unset_userdata('_user_email');
		$this->session->unset_userdata('_user_mobile');
		$this->session->unset_userdata('_designation_id');
		$this->session->unset_userdata('_designation');
		$this->session->unset_userdata('_gatekeeper_key');
		$this->session->unset_userdata('_gatekeeper_database');

		redirect(base_url()."auth/login", 'refresh');
	}
}
