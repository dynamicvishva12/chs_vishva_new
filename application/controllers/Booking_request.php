<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_request extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='BOOKING_REQUEST';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->booking_tbl=$socTables['booking_tbl'];
		$this->booking_slot_tbl=$socTables['booking_slot_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Booking Request';
    }

    public function list()
   	{
   		$breadcrumbArr[0]['name']="Booking Request";
    	$breadcrumbArr[0]['link']=base_url()."booking_request/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Booking Request List";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

   		$this->data['sess_designation']=$sess_designation=$this->session->userdata('_designation');
		$this->data['sess_designation_id']=$sess_designation_id=$this->session->userdata('_designation_id');
		$this->data['sess_user_name']=$sess_user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$sess_user_profile=$this->session->userdata('_user_profile');

   		$mem_condtnArr['s-r-user.s-r-approve']='approve';
		$mem_condtnArr['s-r-user.s-r-active']='active';
		// $mem_condtnArr['s-r-user.status']='Active';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname AS usrFname, s-r-user.s-r-lname AS usrLname, s-r-user.s-r-username AS usrId, s-r-user.s-r-mobile AS usrMobile, s-r-user.s-r-appartment AS usrAppartment, s-r-user.s-r-wing AS usrWing, s-r-user.s-r-flat AS usrFlat, s-r-user.s-r-email AS usrMail, (SELECT `directory`.`d-body` FROM ".$this->directory_tbl." WHERE directory.important=`s-r-user`.`s-r-email` AND directory.directory_type='type1' AND directory.status='Active' LIMIT 1) AS designation", $mem_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		// $fetch=mysqli_query($conn,"SELECT b_id AS bookingId,user_name 
  //       AS bookingusr,DATE_FORMAT(date,'%d/%m/%Y') AS bookFromDate,DATE_FORMAT(date1,'%d/%m/%Y') AS bookToDate,
  //       TIME_FORMAT(from_time,'%h %p') AS bookFromtime,TIME_FORMAT(to_time,'%h %p') AS bookTotime,approval 
  //       AS bookStatus,booking_time.reject_reason AS bookRejectReason,
  //       cancel_reqest,CONCAT(s-r-fname,' ',s-r-lname) AS fullName,booking_name,booking_dscr FROM ((booking_time LEFT JOIN s-r-user 
  //       ON booking_time.user_name=s-r-user.s-r-username) LEFT JOIN 
  //       booking ON booking.booking_id=booking_time.booking_id) WHERE booking_time.date>='$to_date' ORDER BY 
  //       booking_time.date ASC");

		$to_date=date("Y-m-d");

		$bkg_condtnArr['booking_slot.from_date >=']=$to_date;
		$bkg_orderByArr['booking_slot.from_date']='ASC';

		$bkg_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"booking_slot.user_name=s-r-user.s-r-username", "type"=>"left");
		$bkg_joinArr[]=array("tbl"=>$this->booking_tbl, "condtn"=>"booking.booking_id=booking_slot.booking_id", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->booking_slot_tbl, $colNames="booking_slot.b_id AS bookingId, booking_slot.user_name AS bookingusr, DATE_FORMAT(booking_slot.from_date, '%d/%m/%Y') AS bookFromDate, DATE_FORMAT(booking_slot.to_date, '%d/%m/%Y') AS bookToDate, TIME_FORMAT(booking_slot.from_time, '%h %p') AS bookFromtime, TIME_FORMAT(booking_slot.to_time,'%h %p') AS bookTotime, booking_slot.approval AS bookStatus, booking_slot.reject_reason AS bookRejectReason, booking_slot.cancel_reqest, CONCAT(`s-r-user`.`s-r-fname`, ' ', `s-r-user`.`s-r-lname`) AS fullName, booking.booking_name, booking_slot.booking_dscr", $bkg_condtnArr=array(), $likeCondtnArr=array(), $bkg_joinArr, $singleRow=FALSE, $bkg_orderByArr, $res_vnd_groupByArr=array(), $res_vnd_whereInArray=array(), $res_vnd_customWhereArray=array(), $backTicks=TRUE);

		$booking_data=$query['userdata'];

		$this->data['booking_data']=$booking_data;

		$this->data['view']="booking_request/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
   	}

   	public function add_managing_commitee()
   	{
		$name=$this->input->post('name');
		$post=$this->input->post('post');

		if(!empty($name) && !empty($post))
		{
			$url = "http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/CHS_Admin_Api/add_committe_mem_api.php";

			$ch = curl_init( $url );
			# Setup request to send json via POST.
			$login_data = json_encode($value);


			curl_setopt( $ch, CURLOPT_HTTPHEADER,  array("societyKey: ".$this->society_key,"name: ".$name,"post: ".$post));//for headers

			curl_setopt( $ch, CURLOPT_POSTFIELDS, $login_data );//for jeson

			curl_setopt($ch, CURLOPT_PORT, $_SERVER['SERVER_PORT']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

			$response = curl_exec($ch);

			$result = json_decode($response);

			$output= (array) $result; 

			// print_r($output);

			if(!empty($output))
			{
				echo "<html><body><script>alert('Member added sucessfully')</script></body></html>";
				redirect(base_url()."managing_commitee/list", 'refresh');
			}
			else
			{
				echo "<html><body><script>alert('went wrong')</script></body></html>";
			}
		}
		else
		{
			echo "<html><body><script>alert('Field can not be empty')</script></body></html>";
		}
	}

	public function change_booking_status()
	{
		$this->db->trans_start();

		if(empty($this->input->get()))
			redirect(base_url()."booking_request/list", 'refresh');

		$status=$this->input->get('status');
		$userId=$this->input->get('bookusr');
		$bookingId=$this->input->get('id');
		$reason=$this->input->get('reason');

		// $result=change_booking_status($userName, $societyKey, $status, $userId, $bookingId, $reason);
		// change_booking_status($username,$soc_key,$status,$userId,$bookingId,$reason)

		$reason=str_replace("'","\'",$reason);


		$to_date=date("Y-m-d");
		if($status=="Rejected")
		{
			$push_title="Your booking Rejected";
			$push_body=$reason;

			// $fetch=mysqli_query($conn,"UPDATE `booking_time` SET `approval`='$status',`reject_reason`='$reason',`date_creation`='$log_date' WHERE `b_id`='$bookingId' AND `user_name`='$userId'");

			$bkg_UpdateArr=array(
				'approval'=>$status,
				'reject_reason'=>$reason,
				'date_creation'=>$to_date,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['booking_slot.b_id']=$bookingId;
			$updt_condtnArr['booking_slot.user_name']=$userId;

			$query=$this->Mcommon->update($tableName=$this->booking_slot_tbl, $bkg_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
		}
		else
		{
			$push_title="Booking status";
			$push_body="Your booking has been ".$status;

			// $fetch=mysqli_query($conn,"UPDATE `booking_time` SET `approval`='$status',`date_creation`='$log_date' WHERE `b_id`='$bookingId' AND `user_name`='$userId'");

			$bkg_UpdateArr=array(
				'approval'=>$status,
				'reject_reason'=>$reason,
				'date_creation'=>$to_date,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['booking_slot.b_id']=$bookingId;
			$updt_condtnArr['booking_slot.user_name']=$userId;

			$query=$this->Mcommon->update($tableName=$this->booking_slot_tbl, $bkg_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
		}

		$result="";

		if($query['status']==TRUE)
		{
			$result="Booking status updated";
			$push_type="Booking"; 

			$this->load->library('Push_notify');

            $notifyArr['userType']='user app';
            $notifyArr['userName']=$userId;
            $notifyArr['title']=$push_title;
            $notifyArr['body']=$push_body;
            $notifyArr['pushType']=$push_type;

            $notifyRes=$this->push_notify->notification($notifyArr);
		}

		if(empty($result))
		{
			$response='Booking not '.$status;
			$this->session->set_flashdata('error_msg', 'Something went wrong! Booking not '.$status);
		}
		else
		{
			$response='Booking '.$status;
			$this->session->set_flashdata('success_msg', 'Booking '.$status);
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."booking_request/list", 'refresh');
		}
		else
		{
			redirect(base_url()."booking_request/list", 'refresh');
		}   
	}

	public function reject_booking()
	{
		$this->db->trans_start();

		if(empty($this->input->post()))
			redirect(base_url()."booking_request/list", 'refresh');

		$status=$this->input->post('status');
		$userId=$this->input->post('bookusr');
		$bookingId=$this->input->post('id');
		$reason=$this->input->post('reason');

		// $result=change_booking_status($userName, $societyKey, $status, $userId, $bookingId, $reason);
		// change_booking_status($username,$soc_key,$status,$userId,$bookingId,$reason)

		$reason=str_replace("'","\'",$reason);


		$to_date=date("Y-m-d");
		if($status=="Rejected")
		{
			$push_title="Your booking Rejected";
			$push_body=$reason;

			// $fetch=mysqli_query($conn,"UPDATE `booking_time` SET `approval`='$status',`reject_reason`='$reason',`date_creation`='$log_date' WHERE `b_id`='$bookingId' AND `user_name`='$userId'");

			$bkg_UpdateArr=array(
				'approval'=>$status,
				'reject_reason'=>$reason,
				'date_creation'=>$to_date,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['booking_slot.b_id']=$bookingId;
			$updt_condtnArr['booking_slot.user_name']=$userId;

			$query=$this->Mcommon->update($tableName=$this->booking_slot_tbl, $bkg_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
		}
		else
		{
			$push_title="Booking status";
			$push_body="Your booking has been ".$status;

			// $fetch=mysqli_query($conn,"UPDATE `booking_time` SET `approval`='$status',`date_creation`='$log_date' WHERE `b_id`='$bookingId' AND `user_name`='$userId'");

			$bkg_UpdateArr=array(
				'approval'=>$status,
				'reject_reason'=>$reason,
				'date_creation'=>$to_date,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['booking_slot.b_id']=$bookingId;
			$updt_condtnArr['booking_slot.user_name']=$userId;

			$query=$this->Mcommon->update($tableName=$this->booking_slot_tbl, $bkg_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
		}

		$result="";

		if($query['status']==TRUE)
		{
			$result="Booking status updated";
			$push_type="Booking"; 

			$this->load->library('Push_notify');

            $notifyArr['userType']='user app';
            $notifyArr['userName']=$userId;
            $notifyArr['title']=$push_title;
            $notifyArr['body']=$push_body;
            $notifyArr['pushType']=$push_type;

            $notifyRes=$this->push_notify->notification($notifyArr);
		}

		if(empty($result))
		{
			$response='Booking not '.$status;
			$this->session->set_flashdata('error_msg', 'Something went wrong! Booking not '.$status);
		}
		else
		{
			$response='Booking '.$status;
			$this->session->set_flashdata('success_msg', 'Booking '.$status);
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."booking_request/list", 'refresh');
		}
		else
		{
			redirect(base_url()."booking_request/list", 'refresh');
		}   
	}

	public function approve_confirm()
	{
		$this->sec_data['id']=$id=$this->input->get('id');
		$this->sec_data['url']=$url=$this->input->get('url');
		$this->sec_data['status']=$status=$this->input->get('status');
		$this->sec_data['bookusr']=$bookusr=$this->input->get('bookusr');
		$this->load->view('booking_request/approve_confirm', $this->sec_data);
	}
}

?>