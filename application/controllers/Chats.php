<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chats extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='CHAT';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->useradmin2_tbl=$socTables['useradmin2_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Chats';
    }

	public function home()
	{
		$breadcrumbArr[0]['name']="Chats";
    	$breadcrumbArr[0]['link']=base_url()."chats/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Chat list";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
		// $fetch=mysqli_query($conn,"SELECT * FROM `useradmin2` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `useradmin2`.`s-admin` WHERE `s-admin` NOT LIKE 'Gokul-A-3-10'  GROUP BY `s-admin` ORDER BY `user-id` DESC  ");

		// $chat_likeCondtnArr['`useradmin2`.`s-admin` ']='Gokul-A-3-10';
		$chat_customWhereArray=array("`useradmin2`.`s-admin` NOT LIKE 'Gokul-A-3-10'");
		$chat_orderByArr['`useradmin2`.`user-id`']='DESC';
		$chat_orderByArr['`useradmin2`.`createdDatetime`']='DESC';
		$chat_groupByArr=array('useradmin2.s-admin');

		$chat_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `useradmin2`.`s-admin`", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->useradmin2_tbl, $colNames="`useradmin2`.`s-admin`, `s-r-user`.`s-r-fname`, `s-r-user`.`s-r-lname`, `s-r-user`.`s-r-username`, `s-r-user`.`s-r-profile`, `useradmin2`.`user-message`, `useradmin2`.`user-keyvalue`, `useradmin2`.`createdDatetime`", $chat_condtnArr=array(), $chat_likeCondtnArr=array(), $chat_joinArr, $singleRow=FALSE, $chat_orderByArr, $chat_groupByArr, $chat_whereInArray=array(), $chat_customWhereArray, $backTicks=FALSE);

		$chat_arr=$query['userdata'];

		$this->data['chat_arr']=$chat_arr;

		// $fetch1=mysqli_query($conn,"SELECT * FROM `useradmin2` WHERE `s-admin`='".$chatnot['s-admin']."'");
		$msg_groupByArr=array('`useradmin2`.`s-admin`');

		$query=$this->Mcommon->getRecords($tableName=$this->useradmin2_tbl, $colNames="`useradmin2`.`s-admin`, count(`useradmin2`.`s-admin`) AS msg_count", $msg_condtnArr=array(), $msg_likeCondtnArr=array(), $msg_joinArr=array(), $singleRow=FALSE, $msg_condtnArrerByArr=array(), $msg_groupByArr, $msg_whereInArray=array(), $msg_customWhereArray=array(), $backTicks=TRUE);

		$msg_arr=$query['userdata'];

		$msg_count_arr=array();

		if(!empty($msg_arr))
		{
			foreach($msg_arr AS $e_msg)
			{
				$msg_count_arr[$e_msg['s-admin']]=$e_msg['msg_count'];
			}
		}

		$this->data['msg_count_arr']=$msg_count_arr;

		$this->data['view']="chats/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function chat()
	{
		$this->data['chatid']=$chatid=$this->input->post('user_Id');
		$this->data['chatuname']=$chatuname=$this->input->post('user_name');
		$this->data['profile']=$profile=$this->input->post('profile');
		$this->data['user']=$user=$this->input->post('user');

		// $chatup=mysqli_query($conn, "UPDATE `useradmin2` SET `seen`='seen' WHERE `user-keyvalue`='$chatid'");

		$chatUpdateArr=array(
			'seen'=>'seen',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$chatUpdtCondtnArr['useradmin2.user-keyvalue']=$chatid;

		$query=$this->Mcommon->update($tableName=$this->useradmin2_tbl, $chatUpdateArr, $chatUpdtCondtnArr, $likeCondtnArr=array());

		// $s_username = $_SESSION['username'];
  //       $show_key = $_GET['chatid'];

  //       $very = mysqli_query($conn,"SELECT * FROM `useradmin2` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `useradmin2`.`s-admin` WHERE `user-keyvalue` = '$show_key'  ORDER BY `useradmin2`.`user-id` ASC
  //       ");

		$chat_condtnArr['`useradmin2`.`user-keyvalue`']=$chatid;
		$chat_orderByArr['`useradmin2`.`user-id`']='ASC';

		$chat_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `useradmin2`.`s-admin`", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->useradmin2_tbl, $colNames="`useradmin2`.`s-admin`, `s-r-user`.`s-r-profile`, `useradmin2`.`user-message`, `useradmin2`.`s-date`, `s-r-user`.`s-r-profile`, `useradmin2`.`createdDatetime`", $chat_condtnArr, $chat_likeCondtnArr=array(), $chat_joinArr, $singleRow=FALSE, $chat_orderByArr, $chat_groupByArr=array(), $chat_whereInArray=array(), $chat_customWhereArray=array(), $backTicks=TRUE);

		$chat_arr=$query['userdata'];

		$this->data['chat_arr']=$chat_arr;

		$chat_condtnArr1['`useradmin2`.`user-keyvalue`']=$chatid;
		$chat_condtnArr1['`s-r-user`.`s-r-username`']=$user;
		$chat_orderByArr1['`useradmin2`.`user-id`']='ASC';

		$chat_joinArr1[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `useradmin2`.`s-admin`", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->useradmin2_tbl, $colNames="`useradmin2`.`s-admin`, `s-r-user`.`s-r-profile`, `useradmin2`.`user-message`, `useradmin2`.`s-date`, `s-r-user`.`s-r-profile`, `useradmin2`.`createdDatetime`", $chat_condtnArr1, $chat_likeCondtnArr=array(), $chat_joinArr1, $singleRow=TRUE, $chat_orderByArr1, $chat_groupByArr=array(), $chat_whereInArray=array(), $chat_customWhereArray=array(), $backTicks=TRUE);

		$userData=$query['userdata'];

		$this->data['userData']=$userData;

		// $this->data['view']="chats/chat";
		$this->load->view("chats/chat", $this->data);
	}

	public function send_message()
	{
		// $this->load->library('External_functionality');
		$date = date('Y-m-d');

		$user_messsage=$this->input->post('show');

		if(preg_match("/^[a-zA-Z0-9\s,-]*$/", $user_messsage))
	    {
			$user_msg = trim($user_messsage);
			$user_msg = stripcslashes($user_msg);
			$user_msg = htmlspecialchars($user_msg);
	    }
	    else
	    {
	    	$user_msg=$this->input->post('show');
	    }

	    $curr_db=$this->load->database('soc_db', TRUE);

		// $mes = NORMAL(mysqli_real_escape_string($user_msg));
		$mes = mysqli_real_escape_string($curr_db->conn_id, $user_msg);


		$id = $this->input->post('id');
		$name1 = $this->input->post('admin');

		if(!empty($mes)) 
		{
			// $run = mysqli_query($conn, "INSERT INTO `useradmin2`(`user-keyvalue`, `user-message`, `s-date`,`s-admin`)
			// VALUES ('$id','$mes','$date','$s_username')");

			$msgInsertArr[]=array(
				'user-keyvalue'=>$id,
				'user-message'=>$mes,
				's-date'=>$date,
				's-admin'=>$this->user_name,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->useradmin2_tbl, $msgInsertArr, $returnType="");

			$msgInsertStatus=$query['status'];

			if($msgInsertStatus==TRUE)
			{
				// $update = mysqli_query($conn, "UPDATE `useradmin2` SET `id-yes`='yes' WHERE `user-keyvalue` = '$id'");

				$msgUpdateArr=array(
					'id-yes'=>'yes',
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$updt_condtnArr['useradmin2.user-keyvalue']=$id;

				$query=$this->Mcommon->update($tableName=$this->useradmin2_tbl, $msgUpdateArr, $updt_condtnArr, $likeCondtnArr=array());
		              
				if($query['status']==TRUE)
				{
					$this->session->set_flashdata('success_msg', 'Message Sent');
				}
				else
				{
					$this->session->set_flashdata('error_msg', 'Something went wrong! Message has not sent.');
				}
			}
		}

	}
}

?>