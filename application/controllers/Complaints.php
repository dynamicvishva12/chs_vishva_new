<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaints extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='COMPLAINTS';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->s_r_complain_tbl=$socTables['s_r_complain_tbl'];
		$this->complain_chat_tbl=$socTables['complain_chat_tbl'];
		$this->directory_tbl=$socTables['directory_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Complaints';
    }

    public function home()
    {
    	$breadcrumbArr[0]['name']="Complaints";
    	$breadcrumbArr[0]['link']=base_url()."complaints/complaint_list";
    	$breadcrumbArr[0]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	// $query = mysqli_query($conn,"SELECT `c-cmplain` AS compSubjet, `c-post` AS compId, DATE_FORMAT(date(`member_time`),'%d-%m-%Y') AS compTime, `complain_status` AS compStatus,`s-r-complain`.`c-username` AS compUsr,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName,(SELECT COUNT(*) FROM `s-r-complain` WHERE `s-r-complain`.`c-username`= `s-r-user`.`s-r-username`) AS noComp FROM `s-r-complain` LEFT join `s-r-user` ON `s-r-user`.`s-r-username`=`s-r-complain`.`c-username` GROUP BY `c-username`");

    	$cmp_groupByArr=array('s-r-complain`.`c-username`');

		$cmp_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username`=`s-r-complain`.`c-username`", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_complain_tbl, $colNames="`c-cmplain` AS compSubjet, `c-post` AS compId, DATE_FORMAT(date(`member_time`),'%d-%m-%Y') AS compTime, `complain_status` AS compStatus, `s-r-complain`.`c-username` AS compUsr, CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName, (SELECT COUNT(*) FROM ".$this->s_r_complain_tbl." WHERE `s-r-complain`.`c-username`= `s-r-user`.`s-r-username`) AS noComp", $cmp_condtnArr=array(), $cmp_likeCondtnArr=array(), $cmp_joinArr, $singleRow=FALSE, $cmp_orderByArr=array(), $cmp_groupByArr, $cmp_whereInArray=array(), $cmp_customWhereArray=array(), $backTicks=TRUE);

		$complain_data=$query['userdata'];

		$this->data['complain_data']=$complain_data;

   		$this->data['view']="complaints/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function all_complaint_list()
    {
	    $this->data['usrId']=$usrId=$this->uri->segment('3');

	    $breadcrumbArr[0]['name']="Complaints";
    	$breadcrumbArr[0]['link']=base_url()."complaints/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="All Complaints";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

	    $this->session->set_userdata('compluser', $usrId);

    	// $query = mysqli_query($conn,"SELECT `c-cmplain` AS compSubjet, `c-post` AS compId, (`member_time`) AS compTime, `complain_status` AS compStatus,`s-r-complain`.`c-username` AS compUsr,(SELECT `user-message` FROM `complain_chat` WHERE `user-keyvalue`=`s-r-complain`.`c-post` ORDER BY `s-date` DESC LIMIT 1) AS compDscr,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName,(SELECT COUNT(*) FROM `s-r-complain` WHERE `s-r-complain`.`c-username`= `s-r-user`.`s-r-username`) AS noComp FROM (`s-r-complain` LEFT join `s-r-user` ON `s-r-user`.`s-r-username`=`s-r-complain`.`c-username`) WHERE `s-r-complain`.`c-username`='$compusr' ORDER BY `s-r-complain`.`member_time` DESC");

    	$cmp_condtnArr['`s-r-complain`.`c-username`']=$usrId;
    	$cmp_orderByArr['`s-r-complain`.`member_time`']="DESC";

		$cmp_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username`=`s-r-complain`.`c-username`", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_complain_tbl, $colNames="`c-cmplain` AS compSubjet, `c-post` AS compId, (`member_time`) AS compTime, `complain_status` AS compStatus,`s-r-complain`.`c-username` AS compUsr,(SELECT `user-message` FROM ".$this->complain_chat_tbl." WHERE `user-keyvalue`=`s-r-complain`.`c-post` ORDER BY `s-date` DESC LIMIT 1) AS compDscr,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName, `s-r-user`.`s-r-email` AS email_id,(SELECT COUNT(*) FROM ".$this->s_r_complain_tbl." WHERE `s-r-complain`.`c-username`= `s-r-user`.`s-r-username`) AS noComp", $cmp_condtnArr, $cmp_likeCondtnArr=array(), $cmp_joinArr, $singleRow=FALSE, $cmp_orderByArr, $cmp_groupByArr=array(), $cmp_whereInArray=array(), $cmp_customWhereArray=array(), $backTicks=TRUE);

		$complain_data=$query['userdata'];

		$this->data['complain_data']=$complain_data;

		$compId=$this->input->get('c_id');
		$c_name=$this->input->get('c_name');

		if($compId!="" && $c_name!="")
		{
			$user=$this->session->userdata('compluser');

			$this->session->set_userdata('complainid', $compId);
			$this->session->set_userdata('complainname', $c_name);

			$cmpy_groupByAr=array('`complain_chat`.`user-id`');
			$cmpy_condtnArr['`complain_chat`.`user-keyvalue`']=$compId;
	    	$cmpy_orderByArr['`complain_chat`.`user-id`']="ASC";

			$cmpy_joinArr[]=array("tbl"=>$this->s_r_complain_tbl, "condtn"=>"`complain_chat`.`user-keyvalue` = `s-r-complain`.`c-post`", "type"=>"left");
			$cmpy_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`complain_chat`.`s-admin`=`s-r-user`.`s-r-username`", "type"=>"left");
			$cmpy_joinArr[]=array("tbl"=>$this->directory_tbl, "condtn"=>"`s-r-user`.`s-r-email`=`directory`.`important`", "type"=>"left");

			$query=$this->Mcommon->getRecords($tableName=$this->complain_chat_tbl, $colNames="`complain_chat`.`user-keyvalue` AS compId, `s-r-complain`.`c-cmplain` AS compSubjet, `complain_chat`.`user-message` AS compDscr , DATE_FORMAT(`complain_chat`.`s-date`, '%d %b %Y') AS compTime, `s-r-complain`.`complain_status` AS compStatus, `complain_chat`.`s-admin` AS CompUserId, `directory`.`d-body` AS designation, `directory`.`d-topic` AS fulName", $cmpy_condtnArr, $cmpy_likeCondtnArr=array(), $cmpy_joinArr, $singleRow=FALSE, $cmpy_orderByArr, $cmpy_groupByAr, $cmpy_whereInArray=array(), $cmpy_customWhereArray=array(), $backTicks=TRUE, $cmpy_customOrWhereArray=array(), $cmpy_orNotLikeArray=array(), $limit="3");

			$complain_chat_data=$query['userdata'];
		
			$this->data['complain_chat_data']=$complain_chat_data;

			$this->data['comp_data']=$comp_data=count($complain_chat_data);
		}

   		$this->data['view']="complaints/all_complaint_list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function send_complaint()
    {
    	$this->data['usrId']=$usrId=$this->uri->segment('3');

		$messageReply = $this->input->post('chatter_message');
		$compUser=$this->session->userdata('compluser');
		$compSubject=$this->session->userdata('complainname');
		$compId=$this->session->userdata('complainid');

		if(!empty($this->society_key) && !empty($this->user_name) && !empty($messageReply) && !empty($compUser) && !empty($compSubject) && !empty($compId))
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
			    	$result="";
					$messageReply=str_replace("'", "\'", $messageReply);

					// $update = mysqli_query($conn, "UPDATE `s-r-complain` SET `complain_status`='interim reply' WHERE `c-post`='$compId' AND `complain_status`='Pending'");

					$cmp_UpdateArr=array(
						'complain_status'=>'interim reply',
						'status'=>'Inactive',
						'updatedBy'=>$this->user_name,
						'updatedDatetime'=>$this->curr_datetime
					);

					$cmp_condtnArr['`s-r-complain`.`c-post`']=$compId;
					$cmp_condtnArr['`s-r-complain`.`complain_status`']='Pending';

					$query=$this->Mcommon->update($tableName=$this->s_r_complain_tbl, $cmp_UpdateArr, $cmp_condtnArr, $likeCondtnArr=array());
			              
					if($query['status']==TRUE)
					{
						// $query = mysqli_query($conn,"INSERT INTO `complain_chat`(`user-keyvalue`, `user-message`, `s-date`, `s-admin`) VALUES ('$compId','$messageReply','$log_date','$userName')");
						$cmpInsertArr=array();
						$cmpInsertArr[]=array(
							'user-keyvalue'=>$compId,
							'user-message'=>$messageReply,
							's-date'=>$this->curr_datetime,
							's-admin'=>$this->user_name,
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->complain_chat_tbl, $cmpInsertArr, $returnType="");

						$cmpInsertStatus=$query['status'];

						if($cmpInsertStatus==TRUE)
						{
							$result="reply send sucessfully";

							// $logModule="Compalin";
							// $logDescription=$result;
							// $user_id=$compId;
						    
					  		// $log_entry=mysqli_query($conn,"INSERT INTO `society_log`(`logModule`, `logDescription`, `user_id`, `logTime`) VALUES ('$logModule','$logDescription','$user_id','$log_date')");

					    	$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$compId);

					    	$this->load->library('Push_notify');

					    	$push_title=$compSubject;
					    	$push_body="Admin reply on your complain";
					    	$push_type="Complaint";

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']=$compUser;
				            $notifyArr['title']=$push_title;
				            $notifyArr['body']=$push_body;
				            $notifyArr['pushType']=$push_type;
				            $notifyArr['complaintId']=$compId;
				            $notifyArr['priority']=false;

				            $notifyRes=$this->push_notify->notification($notifyArr);
						}
        			}

					if(empty($result))
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! Reply not posted.');
						// redirect(base_url()."complaints/complaint_chat?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
						redirect(base_url()."complaints/all_complaint_list/".$usrId."?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
					}
					else
					{
						$this->session->set_flashdata('success_msg', 'Reply has been Sent successfully');
						// redirect(base_url()."complaints/complaint_chat?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
						redirect(base_url()."complaints/all_complaint_list/".$usrId."?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			// redirect(base_url()."complaints/complaint_list", 'refresh');
			// redirect(base_url()."complaints/complaint_chat?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
			redirect(base_url()."complaints/all_complaint_list/".$usrId."?c_id=".$compId."&&c_name=".$compSubject, 'refresh');
		}
    }
}


?>