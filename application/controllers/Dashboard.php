<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->data['base_url']=$this->config->item('base_url');
		$this->data['assets_url']=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['js']='layouts/include/js';																					
		$this->data['footer']='layouts/include/footer';

		$this->data['page_section']='Dashboard';
    }

	public function index()
	{
		$breadcrumbArr[0]['name']="Dashboard";
    	$breadcrumbArr[0]['link']=base_url()."dashboard/";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Home";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$this->data['view']="dashboard";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}
}
