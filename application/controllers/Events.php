<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='EVENT';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->s_r_event_tbl=$socTables['s_r_event_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Events';
    }

    public function list()
   	{
   		$breadcrumbArr[0]['name']="Events";
    	$breadcrumbArr[0]['link']=base_url()."events/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Event List";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

   		$today_date = date('Y-m-d');

   		$event_condtnArr['s-r-event.sheduled']='not scheduled';
		$event_condtnArr['s-r-event.ev_time >=']=$today_date;
		$event_condtnArr['s-r-event.status']='active';
		$event_orderByArr['s-r-event.ev_id']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_event_tbl, $colNames="s-r-event.event_id As eventId, s-r-event.file_type As eventFileType, s-r-event.ev_desc As eventDesc, s-r-event.ev_name As eventTitle, CONCAT('".$this->assets_url."IMAGESDRIVE/Event/', `s-r-event`.`ev-image`) As eventFilename, DATE_FORMAT(`s-r-event`.`ev_time`,'%d %b, %Y') As eventDate", $event_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $event_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$event_data=$query['userdata'];

		$this->data['event_data']=$event_data;

   		$this->data['view']="events/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
   	}

    public function add_event()
    {
		$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('ev_Description', 'Event Description', 'trim|required');
		$this->form_validation->set_rules('ev_Date', 'Event Date', 'trim|required');
		$this->form_validation->set_rules('name', 'Event Title', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."events/list", 'refresh');
		}
		else
		{
			$this->db->trans_start();

			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
					$callfrom="WEB";
					$evdate= date("Y-m-d",strtotime($this->input->post('ev_Date')));
					$ev_descriction=$this->input->post('ev_Description');
					$ev_title=$this->input->post('name');

					if(!empty($_FILES['myfile']['name'])) 
					{
						$file_Type="image";

						$post_image= $_FILES['myfile']['name'];
						$t_tmp=$_FILES['myfile']['tmp_name']; 
						$temp = explode(".", $_FILES["myfile"]["name"]);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						$fileexe = strtolower(pathinfo($post_image, PATHINFO_EXTENSION));
						$store=FCPATH."assets/IMAGESDRIVE/Event/".$newfilename;

						if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
						{
							move_uploaded_file($t_tmp, $store);
							
							if(end($temp)=="pdf")
							{
								$file_Type="pdf";
							}
						}
						else
						{
							$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
							redirect(base_url()."events/list", 'refresh');
						}
					}
					else
					{
						$newfilename=$file_Type=NULL;
					}

					$ev_name=str_replace("'","\'",$ev_title);
					$ev_desc=str_replace("'","\'",$ev_descriction);

					function base64_to_jpeg($base64_string, $output_file) 
					{
						$ifp = fopen( $output_file, 'wb' ); 

						fwrite( $ifp, base64_decode( $base64_string ) );

						fclose( $ifp );
						return( $output_file );
					}

					if($callfrom=="APP")
					{
						if(!empty($encodedString))
						{
							$pic1=FCPATH."assets/IMAGESDRIVE/Event/".$newfilename;
							$image1 = base64_to_jpeg( $encodedString, $pic1 );
						}
					}


					$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
					$onetime = substr($random,0,4);
					$postid = "event-".$onetime;
					$today_date = date('Y-m-d');
					if(!empty($newfilename))
					{
						$eventInsertArr[]=array(
							'ev_name'=>$ev_title,
							'ev_desc'=>$ev_desc,
							'ev-image'=>$newfilename,
							'ev_created'=>$today_date,
							'ev_time'=>$evdate,
							'file_type'=>$file_Type,
							'event_id'=>$postid,
							'status'=>'active',
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);
					}
					else
					{
						$eventInsertArr[]=array(
							'ev_name'=>$ev_title,
							'ev_desc'=>$ev_desc,
							'ev-image'=>NULL,
							'ev_created'=>$today_date,
							'ev_time'=>$evdate,
							'file_type'=>NULL,
							'event_id'=>$postid,
							'status'=>'active',
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);
					}

					$query=$this->Mcommon->insert($tableName=$this->s_r_event_tbl, $eventInsertArr, $returnType="");

					$eventInsertStatus=$query['status'];

					$result='';

					if($eventInsertStatus==TRUE)
					{
						$setUpdateArr=array(
							'`activity-no`'=>'`activity-no`+1',
							'updatedBy'=> "'".$this->user_name."'",
							'updatedDatetime'=> "'".$this->curr_datetime."'"
						);

						$query=$this->Mcommon->setUpdate($tableName=$this->s_r_user_tbl, $setUpdateArr, $eventUpdtCondtnArr=array(), $likeCondtnArr=array());

						$result='Events has been created';

						$subject="New Society Event ";
			            $message="Dear User, <br><br><br>


						New Event is Created <br><br>
						".$ev_title."<br><br>

						Do Check in Event tab.. <br><br><br>

						Best Regards,<br>
						CHSVishva<br><br><br>

						Note : Please do not reply back to this mail. This is sent from an unattended  <br>             
						mail box. Please mark all your queries / responses to info@chsvishva.com ";

			            $alt_message="This is the body in plain text for non-HTML mail clients";

			            $email="dynamicvishvateam@gmail.com";

			            $mailDataArr['to']=$email;
			            $mailDataArr['subject']=$subject;
			            $mailDataArr['alt_message']=$alt_message;
			            $mailDataArr['message']=$message;

			            $this->load->library('Email_sending');

			            $response=$this->email_sending->send($mailDataArr);

			            $responseStatus=$response['status'];
			            $responseMsg=$response['message'];

			            $this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

			            $this->load->library('Push_notify');

			            $notifyArr['userType']='user app';
			            $notifyArr['userName']='';
			            $notifyArr['title']='Society Event ';
			            $notifyArr['body']=$ev_title;
			            $notifyArr['pushType']='Events';

			            $notifyRes=$this->push_notify->notification($notifyArr);
					}

					if(empty($result))
					{
						$response='Event not added';
						$this->session->set_flashdata('error_msg', 'Something went wrong! Event has not added.');
					}
					else
					{
						$response='Event  added';
						$this->session->set_flashdata('success_msg', 'Events has been added successfully');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
				
				redirect(base_url()."events/list", 'refresh');
			}
			else
			{
				redirect(base_url()."events/list", 'refresh');
			}     
		}
    }

    public function edit_event()
    {
    	$this->data['eventId']=$eventId=$this->uri->segment('3');

    	$breadcrumbArr[0]['name']="Events & Albums";
    	$breadcrumbArr[0]['link']=base_url()."events/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Events";
    	$breadcrumbArr[1]['link']=base_url()."events/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Event";
    	$breadcrumbArr[2]['link']="javascript:void(0);";
    	$breadcrumbArr[2]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('name', 'Event Title', 'trim|required');
		$this->form_validation->set_rules('ev_Description', 'Event Description', 'trim|required');
		$this->form_validation->set_rules('ev_Date', 'Event Date', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			$event_condtnArr['s-r-event.event_id']=$eventId;

			$query=$this->Mcommon->getRecords($tableName=$this->s_r_event_tbl, $colNames="`s-r-event`.`event_id` As eventId, `s-r-event`.`file_type` As eventFileType, `s-r-event`.`ev_desc` As eventDesc, `s-r-event`.`ev_name` As eventTitle, CONCAT('".$this->assets_url."IMAGESDRIVE/Event/', `s-r-event`.`ev-image`) As eventFilename, `s-r-event`.`ev_time` As eventDate", $event_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $event_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$event_data=$query['userdata'];

			$this->data['event_data']=$event_data;

			$this->data['view']="events/edit_event";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
			$this->db->trans_start();
			
			$eventId = $this->input->post('eventId');
			$post_expiredt= date("Y-m-d",strtotime($this->input->post('ev_Date')));
			$ev_descripction=$this->input->post('ev_Description');
			$ev_title=$this->input->post('name');
			$nameold=$this->input->post('nameold');
			$imaged_status=$this->input->post('image_status');

			if($this->input->post('image_status')=="unchanged")
			{
				$newfilename=$nameold;
				$file_Type=$this->input->post('nofiletype');
			}
			else
			{
				if(!empty($_FILES['myfile']['name'])) 
				{
					$file_Type="image";
					$post_image= $_FILES['myfile']['name'];
					$t_tmp=$_FILES['myfile']['tmp_name'];
					$temp = explode(".", $_FILES["myfile"]["name"]);

					$newfilename = round(microtime(true)) . '.' . end($temp);
					$fileexe = strtolower(pathinfo($post_image, PATHINFO_EXTENSION));
					// $store="../IMAGES/Event/".$newfilename;
					$store=FCPATH."assets/IMAGESDRIVE/Event/".$newfilename;

					if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
					{
						move_uploaded_file($t_tmp, $store);
						if(end($temp)=="pdf")
						{
							$file_Type="pdf";
						}
					}
					else
					{
						$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
						redirect(base_url()."events/edit_event/".$eventId, 'refresh');
					}
				}
				else
				{
					$newfilename=$file_Type=NULL;
					$imaged_status="Removed";
				}
			}  

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($post_expiredt) && !empty($ev_descripction) && !empty($ev_title) && !empty($eventId))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						$callfrom="WEB";

						// $result=edit_event($userName,$soc_key,$eventId,$ev_title,$ev_descripction,$newfilename,$encodedString,$post_expiredt,$file_Type,$callfrom,$imaged_status);

						// function edit_event($username,$soc_key,$eventId,$ev_name,$ev_desc,$evpic,$encodedString,$ev_time,$filetype,$callfrom,$imaged_status)

						$ev_title=str_replace("'", "\'", $ev_title);
						$ev_desc=str_replace("'", "\'", $ev_descripction);

						// include '../database_connection/s-conn.php';
						function base64_to_jpeg($base64_string, $output_file) 
						{
							$ifp = fopen( $output_file, 'wb' ); 

							fwrite( $ifp, base64_decode( $base64_string ) );

							fclose( $ifp );
							return($output_file);
						}

						if($callfrom=="APP")
						{
							if(!empty($encodedString))
							{
								// $pic1='../IMAGES/Event/'.$newfilename;
								$pic1=FCPATH."assets/IMAGESDRIVE/Event/".$newfilename;
								$image1 = base64_to_jpeg( $encodedString, $pic1 );
							}
						}

						$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
						$onetime = substr($random,0,4);
						$postid = "event-".$onetime;
						$today_date = date('Y-m-d');

						if(!empty($newfilename) && ($imaged_status=="changed" || $imaged_status=="unchanged"))
						{
							$eventInsertArr[]=array(
								'ev_name'=>$ev_title,
								'ev_desc'=>$ev_desc,
								'ev-image'=>$newfilename,
								'ev_created'=>$today_date,
								'ev_time'=>$post_expiredt,
								'file_type'=>$file_Type,
								'event_id'=>$postid,
								'createdBy'=>$this->user_name,
								'createdDatetime'=>$this->curr_datetime
							);
						}
						else
						{
							$eventInsertArr[]=array(
								'ev_name'=>$ev_title,
								'ev_desc'=>$ev_desc,
								'ev-image'=>NULL,
								'ev_created'=>$today_date,
								'ev_time'=>$post_expiredt,
								'file_type'=>NULL,
								'event_id'=>$postid,
								'createdBy'=>$this->user_name,
								'createdDatetime'=>$this->curr_datetime
							);
						}

						$query=$this->Mcommon->insert($tableName=$this->s_r_event_tbl, $eventInsertArr, $returnType="");

						$eventInsertStatus=$query['status'];

						$result='';

						if($eventInsertStatus==TRUE)
						{
							// $value = mysqli_query($conn,"DELETE FROM `s-r-event` WHERE `event_id`='$eventId'")or die("error..".mysqli_error($conn));

							$delCondtnArr['s-r-event.event_id']=$eventId;

							$query=$this->Mcommon->delete($tableName=$this->s_r_event_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

							$result='Event has been updated';

							$subject="New Society Event ";
				            $message="Dear User, <br><br><br>

							New Event is Created <br><br>
							".$ev_title."<br><br>

							Do Check in Event tab.. <br><br><br>

							Best Regards,<br>
							CHSVishva<br><br><br>

							Note : Please do not reply back to this mail. This is sent from an unattended  <br>             
							mail box. Please mark all your queries / responses to info@chsvishva.com ";

				            $alt_message="This is the body in plain text for non-HTML mail clients";

				            $email="dynamicvishvateam@gmail.com";

				            $mailDataArr['to']=$email;
				            $mailDataArr['subject']=$subject;
				            $mailDataArr['alt_message']=$alt_message;
				            $mailDataArr['message']=$message;

				            $this->load->library('Email_sending');

				            $response=$this->email_sending->send($mailDataArr);

				            $responseStatus=$response['status'];
				            $responseMsg=$response['message'];

				            $this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

				            $this->load->library('Push_notify');

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']='';
				            $notifyArr['title']='Society Event ';
				            $notifyArr['body']=$ev_title;
				            $notifyArr['pushType']='Events';

				            $notifyRes=$this->push_notify->notification($notifyArr);
						}

						if(empty($result))
						{
							$response='Event not updated';
							$this->session->set_flashdata('error_msg', 'Something went wrong! Event has not updated.');
						}
						else
						{
							$response='Event updated';
							$this->session->set_flashdata('success_msg', 'Event has been updated successfully');
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
				redirect(base_url()."events/list", 'refresh');
			}
			else
			{
				redirect(base_url()."events/list", 'refresh');
			}     
		}
    }

    public function delete_event()
    {
    	$this->db->trans_start();

    	$eventId=$this->uri->segment('3');

    	 $eventUpdateArr=array(
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['s-r-event.event_id']=$eventId;

		$query=$this->Mcommon->update($tableName=$this->s_r_event_tbl, $eventUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

		$eventUpdtStatus=$query['status'];

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!!!, Event not removed');
			redirect(base_url()."events/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Event has been removed successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription='Event removed sucessfully', $userId=$this->user_name);
			redirect(base_url()."events/list", 'refresh');
		}     
    }
}

?>