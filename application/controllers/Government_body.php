<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Government_body extends Society_core
{
    public function __construct()
    {
    	parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');

		$this->logModule='GOVERNMENT_BODY';

		$socTables=$this->society_lib->socTables();

		$this->directory_tbl=$socTables['directory_tbl'];
		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Government Body';
    }

	public function list()
	{
		$breadcrumbArr[0]['name']="Government Body";
    	$breadcrumbArr[0]['link']=base_url()."government_body/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Government Body List";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$this->data['sess_user_profile']=$sess_user_profile=$this->session->userdata('_user_profile');
		
		$this->Mlogger->log($logModule=$this->logModule, $logDescription='Fetch Government Body List', $userId=$this->user_name);

		$gov_condtnArr['directory.directory_type']='type2';
		$gov_condtnArr['directory.status']='Active';
		$orderByArr['directory.d-topic']='ASC';

		$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="d-id AS directoryId , d-topic AS directoryName, d-body AS directoryDesignation,mobile AS directoryMobile, important  AS directoryMail", $gov_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$govBodyArr=$query['userdata'];

		$this->data['govBodyArr']=$govBodyArr;

		$this->data['view']="government_body/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_gov_body()
	{
		$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('post', 'Designation', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('E-mail', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('Owner-mobile', 'Mobile Number', 'trim|required|exact_length[10]|numeric');

		if($this->form_validation->run() == FALSE)
		{
			// $this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			$this->list();
		}
		else
        {
        	$this->db->trans_start();

			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];
			
			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

				$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

				if(empty($uservalidate))
				{
					$this->session->set_flashdata('warning_msg', 'User ID Invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
				else
				{
					$gov_name=$this->input->post('name');
					$gov_post=$this->input->post('post');
					$gov_mobile=$this->input->post('Owner-mobile');
					$gov_mail=$this->input->post('E-mail');

					$callfrom="WEB";

			        $govInsertArr[]=array(
						'd-topic'=>$gov_name,
						'd-body'=>$gov_post,
						'created-date'=>$this->curr_datetime,
						'mobile'=>$gov_mobile,
						'important'=>$gov_mail,
						'directory_type'=>'type2',
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->directory_tbl, $govInsertArr, $returnType="lastInsertId");

					$dirInsertStatus=$query['status'];

					if($dirInsertStatus==TRUE)
					{
						$response="Government Body has been added successfully";
						$this->session->set_flashdata('success_msg', 'Government Body has been added successfully');
					}
					else
					{
						$response="Government Body has not added";
						$this->session->set_flashdata('error_msg', 'Something went wrong! Government Body has not added.');
					}

					$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
				redirect(base_url()."government_body/list", 'refresh');
			}
			else
			{
				redirect(base_url()."government_body/list", 'refresh');
			}      
		}  
	}

	public function delete_gov_body()
	{
		$this->db->trans_start();
		$EntryId=$this->uri->segment('3');

		if($EntryId!='')
		{
			$dirUpdateArr=array(
				'status'=>'Inactive',
				'created-date'=>$this->curr_datetime,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['directory.d-id']=$EntryId;

			$query=$this->Mcommon->update($tableName=$this->directory_tbl, $dirUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

			$dirUpdtStatus=$query['status'];

			if($dirUpdtStatus==TRUE)
			{
				$response="Government Body Has Removed Successfully";

				$this->session->set_flashdata('success_msg', 'Government Body has been removed successfully');

				$user_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"directory.important=s-r-user.s-r-email", "type"=>"left");

				$user_condtnArr['directory.d-id']=$EntryId;

				$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="s-r-user.s-r-username AS userId", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr, $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$user_data=$query['userdata'];

				if(!empty($user_data))
				{
					$userDataId=$user_data['userId'];

					$query=$this->Msociety->get_api_key($app_type='user app');

					$app_key=$query['api_key'];

					define( 'API_ACCESS_KEY', $app_key);

					$fetch_user=$this->Msociety->get_fms_tockens($userDataId);

					if(!empty($fetch_user))
					{
						foreach($fetch_user AS $user_dt)
						{
							$push_body="you are not a committee member of your society ";
							$push_title="Important";
							$push_type="Greeting";          
							$typedata=array
							(
								'body' => $push_body,
								'title'	=> $push_title,
								'type' => $push_type,
								'priority' => true
							);
							$value=$user_dt['tocken'];
							$fields = array
							(
								'to' => $value,
								'data' => $typedata
							);
							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
							);
							// 			print_r($typedata);
							// 			print_r($fields);
							// 			print_r($headers);
							#Send Repo/nse To FireBase Server	
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$output = curl_exec($ch);
							curl_close( $ch );
						}
					}
				}
			}
			else
			{
				$response='Government Body Not Removed';
				$this->session->set_flashdata('error_msg', 'Something went wrong! Government Body Not Removed.');
			}
		}
		else
		{
			$response='Government Body Not Removed';
			$this->session->set_flashdata('error_msg', 'Something went wrong! Government Body Not Removed.');
		}
		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
			redirect(base_url()."government_body/list", 'refresh');
		}
		else
		{
			redirect(base_url()."government_body/list", 'refresh');
		}      
	}
}
