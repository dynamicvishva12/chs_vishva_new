<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Managing_commitee extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->society_db = $this->session->userdata('_society_database');
		$this->society_key=$this->session->userdata('_society_key');
		$this->user_name=$this->session->userdata('_user_name');

		$this->logModule='MANAGING_COMMITEE';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->directory_tbl=$socTables['directory_tbl'];
		$this->fsm_server_key_tbl=$socTables['fsm_server_key_tbl'];
		$this->fms_tocken_tbl=$socTables['fms_tocken_tbl'];
		$this->fms_tocken_admin_tbl=$socTables['fms_tocken_admin_tbl'];
		$this->society_master_tbl=$socTables['society_master_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Managing Commitee';
    }

	public function list()
	{
		$breadcrumbArr[0]['name']='Managing Commitee';
    	$breadcrumbArr[0]['link']=base_url()."managing_commitee/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']='Managing Commitee List';
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$this->data['sess_designation']=$sess_designation=$this->session->userdata('_designation');
		$this->data['sess_designation_id']=$sess_designation_id=$this->session->userdata('_designation_id');
		$this->data['sess_user_name']=$sess_user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$sess_user_profile=$this->session->userdata('_user_profile');

		$this->Mlogger->log($logModule=$this->logModule, $logDescription='Fetch Managing Commitee List', $userId=$this->user_name);

		$usr_status="approve";
		$sql_query="SELECT `directory`.`d-id` AS directoryId , `directory`.`d-topic` AS directoryName, `directory`.`d-body` AS directoryDesignation, `directory`.`mobile` AS directoryMobile, `directory`.`important` AS directoryMail, CONCAT('".$this->assets_url."IMAGES/', `directory`.`addimg`) AS addimg FROM ".$this->society_db.".directory WHERE `directory`.`directory_type`='type1' AND `directory`.`status`='Active' ORDER BY 
		Case `directory`.`d-body` 
		when 'chairman' then 1 
		when 'secretory' then 2 
		when 'co-secretory' then 3 
		when 'treasurer' then 4 
		when 'committee members' then 5 
		END";

		$query=$this->Mcommon->plainQuery($sql_query);

		$this->data['managing_commitee_list']=$managing_commitee_list=$query['userdata'];

		$mem_condtnArr['s-r-user.s-r-approve']='approve';
		$mem_condtnArr['s-r-user.s-r-active']='active';
		// $mem_condtnArr['s-r-user.status']='Active';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname AS usrFname, s-r-user.s-r-lname AS usrLname, s-r-user.s-r-username AS usrId, s-r-user.s-r-mobile AS usrMobile, s-r-user.s-r-appartment AS usrAppartment, s-r-user.s-r-wing AS usrWing, s-r-user.s-r-flat AS usrFlat, s-r-user.s-r-email AS usrMail, (SELECT `directory`.`d-body` FROM ".$this->directory_tbl." WHERE directory.important=`s-r-user`.`s-r-email` AND directory.directory_type='type1' AND directory.status='Active' LIMIT 1) AS designation", $mem_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		$this->data['view']="managing_commitee/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_commitee()
	{		
		$this->db->trans_start();

		$mang_name=$this->input->post('name');
		$mang_post=$this->input->post('post');
		$result="";

		if(!empty($this->society_key) && !empty($this->user_name) && !empty($mang_post) && !empty($mang_name))
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	               $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
					// $result=Society_managing_othercomitte_add($mang_name, $mang_post, $soc_key, $userName);

					// Society_managing_othercomitte_add($userId,$post,$soc_key,$userName)

					$user_condtnArr['s-r-user.s-r-username']=$mang_name;

					$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.s-r-profile", $user_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

					$user_data=$query['userdata'];

					$name = $user_data['s-r-fname']." ".$user_data['s-r-lname'];
					$description = $mang_post;
					$mobile = $user_data['s-r-mobile'];
					$value = $user_data['s-r-email'];
					$type="type1";
					$accpic=$user_data['s-r-profile'];

       				$dir_condtnArr['directory.important']=$value;
       				$dir_condtnArr['directory.status']='Active';
       				$dir_condtnArr['directory.directory_type']='type1';

					$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="directory.important", $dir_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

					$dir_data=$query['userdata'];

					if(empty($dir_data))
					{
						$dirInsertArr[]=array(
							'd-topic'=>$name,
							'd-body'=>$description,
							'created-date'=>$this->curr_datetime,
							'mobile'=>$mobile,
							'important'=>$value,
							'directory_type'=>$type,
							'addimg'=>$accpic,
							'status'=>'Active',
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->directory_tbl, $dirInsertArr, $returnType="lastInsertId");

						$dirInsertStatus=$query['status'];

						if($dirInsertStatus==TRUE)
						{
							// $dirId=$query['userdata']['insertedId'];

							$result="Successfully Added";

							$query=$this->Msociety->get_api_key($app_type='user app');

							$app_key=$query['api_key'];

							define( 'API_ACCESS_KEY', $app_key);

							$fetch_user=$this->Msociety->get_fms_tockens($mang_name);

							// $api_condtnArr['fsm_server_key.app_type']='user app';

							// $query=$this->Mcommon->getRecords($tableName=$this->fsm_server_key_tbl, $colNames="fsm_server_key.api_key", $api_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

							// $fetch_api_key=$query['userdata'];

							// $app_key=$fetch_api_key['api_key'];

							// define( 'API_ACCESS_KEY', $app_key);

							// $tocken_condtnArr['fms_tocken.username']=$mang_name;

							// $query=$this->Mcommon->getRecords($tableName=$this->fms_tocken_tbl, $colNames="fms_tocken.tocken, fms_tocken.username", $tocken_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

							// $fetch_user=$query['userdata'];

							if(!empty($fetch_user))
							{
								foreach($fetch_user AS $user_dt)
								{
									$push_body="Congratulation ...!!! <br> you are new ".$mang_post." of our society..";
									$push_title="Greeting";
									$push_type="Greeting";          
									$typedata=array
									(
										'body' 	=> $push_body,
										'title'	=> $push_title,
										'type' => $push_type,
										'priority' => true
									);
									$value=$user_dt['tocken'];
									$fields = array
									(
										'to' => $value,
										'data' => $typedata
									);

									$headers = array
									(
										'Authorization: key=' . API_ACCESS_KEY,
										'Content-Type: application/json'
									);
									//	print_r($typedata);
									//	print_r($fields);
									//	print_r($headers);
									#Send Repo/nse To FireBase Server	
									$ch = curl_init();
									curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
									curl_setopt( $ch,CURLOPT_POST, true );
									curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
									curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
									curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
									$arr[]=$output = curl_exec($ch);
									//print_r($output);
									curl_close( $ch );
								}
							}

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
						}
					}
					else
					{
						$this->session->set_flashdata('warning_msg', 'Operation not perform, memmber already in committe.');
					}

					if(empty($result))
					{
					 	$this->session->set_flashdata('warning_msg', 'Operation not perform.');
					}
					else
					{
					    if($result=='Successfully Added')
					    {
				 			$this->session->set_flashdata('success_msg', 'Member has been successfully added');
					    }
					    else
					    {
					        $this->session->set_flashdata('error_msg', 'Member has not added');
					    }
					}
				}
			}
			else
			{
    			$this->session->set_flashdata('warning_msg', 'Society key invalid');
    			redirect(base_url()."auth/logout", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
		}  

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."managing_commitee/list", 'refresh');
		}
		else
		{
			redirect(base_url()."managing_commitee/list", 'refresh');
		}     
	}

	public function add_managing()
	{
		$this->db->trans_start();

		$mang_name=$this->input->post('name');
		$mang_post=$this->input->post('post');
		$oldId=$this->input->post('oldId');
		$result="";
   
		if(!empty($this->society_key) && !empty($this->user_name) && !empty($mang_post) && !empty($mang_name) && !empty($oldId))
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

				$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

				if(empty($uservalidate))
				{
					$this->session->set_flashdata('warning_msg', 'User ID Invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
				else
				{
					// $fetch = mysqli_query($conn,"SELECT * FROM `s-r-user` WHERE `s-r-username` = '$userId'");
					// $user = mysqli_fetch_assoc($fetch);
					// $name = $user['s-r-fname']." ".$user['s-r-lname'];
					// $description = $post;
					// $mobile = $user['s-r-mobile'];
					// $value = $user['s-r-email'];
					// $type="type1";
					// $accpic=$user['s-r-profile'];

					$user_condtnArr['s-r-user.s-r-username']=$mang_name;

					$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.s-r-profile", $user_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

					$user_data=$query['userdata'];

					$name = $user_data['s-r-fname']." ".$user_data['s-r-lname'];
					$description = $mang_post;
					$mobile = $user_data['s-r-mobile'];
					$value = $user_data['s-r-email'];
					$type="type1";
					$accpic=$user_data['s-r-profile'];

					// $qry_check=mysqli_query($conn,"SELECT * FROM `directory` WHERE `important`='$value' AND `dir_status`='Active' AND `directory_type`='type1'");
					// if(mysqli_num_rows($qry_check)==0){

					$dir_condtnArr['directory.important']=$value;
       				$dir_condtnArr['directory.status']='Active';
       				$dir_condtnArr['directory.directory_type']='type1';

					$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="directory.important", $dir_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

					$dir_data=$query['userdata'];

					// $query =mysqli_query($conn,"INSERT INTO `directory` (`d-topic`, `d-body`, `created-date`, `mobile`, `important`,`directory_type`,`addimg`)
					// VALUES ('$name', '$description', '$log_date', '$mobile', '$value','$type','$accpic')")or die("error..".mysqli_error($conn));
					// if($query){

					if(empty($dir_data))
					{
						$dirInsertArr[]=array(
							'd-topic'=>$name,
							'd-body'=>$description,
							'created-date'=>$this->curr_datetime,
							'mobile'=>$mobile,
							'important'=>$value,
							'directory_type'=>$type,
							'addimg'=>$accpic,
							'status'=>'Active',
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->directory_tbl, $dirInsertArr, $returnType="lastInsertId");

						$dirInsertStatus=$query['status'];

						if($dirInsertStatus==TRUE)
						{
							// $result=Society_managing_comitte_add($mang_name,$mang_post,$oldId,$soc_key,$userName);

							// function Society_managing_comitte_add($userId,$post,$drid,$soc_key,$userName)

							// $up_qry=mysqli_query($conn,"UPDATE `directory` SET `dir_status`='Inactive',`created-date`='$log_date' WHERE `d-id`='$drid' AND `d-body`='$post'")or die("error..".mysqli_error($conn));

							$dirUpdateArr=array(
								'status'=>'Inactive',
								'created-date'=>$this->curr_datetime,
								'updatedBy'=>$this->user_name,
								'updatedDatetime'=>$this->curr_datetime
							);

							$updt_condtnArr['directory.d-id']=$oldId;
							$updt_condtnArr['directory.d-body']=$mang_post;

							$query=$this->Mcommon->update($tableName=$this->directory_tbl, $dirUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

							$result="sucessfully Swap";

							$query=$this->Msociety->get_api_key($app_type='user app');

							$app_key=$query['api_key'];

							define( 'API_ACCESS_KEY', $app_key);

							$fetch_user=$this->Msociety->get_fms_tockens($mang_name);

							if(!empty($fetch_user))
							{
								foreach($fetch_user AS $user_dt)
								{
									$push_body="Congratulation ...!!! <br> you are new ".$mang_post." of our society..";
									$push_title="Greeting";
									$push_type="Greeting";          
									$typedata=array
									(
										'body' 	=> $push_body,
										'title'	=> $push_title,
										'type' => $push_type,
										'priority' => true
									);
									$value=$user_dt['tocken'];
									$fields = array
									(
										'to' => $value,
										'data' => $typedata
									);

									$headers = array
									(
										'Authorization: key=' . API_ACCESS_KEY,
										'Content-Type: application/json'
									);
									//	print_r($typedata);
									//	print_r($fields);
									//	print_r($headers);
									#Send Repo/nse To FireBase Server	
									$ch = curl_init();
									curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
									curl_setopt( $ch,CURLOPT_POST, true );
									curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
									curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
									curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
									$output = curl_exec($ch);
									//print_r($output);
									curl_close( $ch );
								}
							}

							$query=$this->Msociety->get_api_key($app_type='admin app');

							$app_key_admin=$query['api_key'];

							define( 'API_ACCESS_KEY_ADMIN', $app_key_admin);

							$fetch_user_arr=$this->Msociety->get_fms_tockens($this->user_name);

							if(!empty($fetch_user_arr))
							{
								foreach($fetch_user_arr AS $admin_dt)
								{
									$push_title="Important";
									$push_body="Seems you are not ".$mang_post." of your society..";
									$push_type="logout";          
									$typedata1=array
									(
										'body' => $push_body,
										'title'	=> $push_title,
										'type' => $push_type,
										'priority' => true
									);
									$value_admin=$admin_dt['tocken'];
									$fields1 = array
									(
										'to' => $value_admin,
										'data' => $typedata1
									);
									$headers1 = array
									(
										'Authorization: key=' . API_ACCESS_KEY_ADMIN,
										'Content-Type: application/json'
									);
									// print_r($typedata1);
									// 		print_r($fields1);
									// 		print_r($headers1);
									#Send Reponse To FireBase Server	
									$ch1 = curl_init();
									curl_setopt( $ch1,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
									curl_setopt( $ch1,CURLOPT_POST, true );
									curl_setopt( $ch1,CURLOPT_HTTPHEADER, $headers1 );
									curl_setopt( $ch1,CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch1,CURLOPT_SSL_VERIFYPEER, false );
									curl_setopt( $ch1,CURLOPT_POSTFIELDS, json_encode( $fields1 ) );
									$output1 = curl_exec($ch1 );
									//print_r($output1);
									curl_close( $ch1 );
								}
							}

							$fcm_del_condtnArr['fms_tocken_admin.username']=$this->user_name;

							$query=$this->Mcommon->delete($tableName=$this->fms_tocken_admin_tbl, $fcm_del_condtnArr, $likeCondtnArr=array(), $whereInArray=array());

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
						}
					}
					else
					{
						$this->session->set_flashdata('warning_msg', 'Operation not perform, memmber already in committe.');
					}

					if(empty($result))
					{
						$this->session->set_flashdata('warning_msg', 'Operation not perform, Swap not done.');
					}
					else
					{
						if($result=='sucessfully Swap')
						{
							$this->session->set_flashdata('success_msg', 'Member has been successfully swapped');
						}
						else
						{
							$this->session->set_flashdata('error_msg', 'Member has not swapped.');
						}
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
		}      

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."managing_commitee/list", 'refresh');
		}
		else
		{
			redirect(base_url()."managing_commitee/list", 'refresh');
		}          
	}

	public function delete_commitee()
	{
		$this->db->trans_start();

		$deleteIdmanag=$this->uri->segment('3');
		$response="";

		if($deleteIdmanag!='')
		{
			$dirUpdateArr=array(
				'status'=>'Inactive',
				'created-date'=>$this->curr_datetime,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['directory.d-id']=$deleteIdmanag;

			$query=$this->Mcommon->update($tableName=$this->directory_tbl, $dirUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

			$dirUpdtStatus=$query['status'];

			if($dirUpdtStatus==TRUE)
			{
				$response="successfully removed";

				$user_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"directory.important=s-r-user.s-r-email", "type"=>"left");

				$user_condtnArr['directory.d-id']=$deleteIdmanag;

				$query=$this->Mcommon->getRecords($tableName=$this->directory_tbl, $colNames="s-r-user.s-r-username AS userId", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr, $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$user_data=$query['userdata'];

				if(!empty($user_data))
				{
					$userDataId=$user_data['userId'];

					$query=$this->Msociety->get_api_key($app_type='user app');

					$app_key=$query['api_key'];

					define( 'API_ACCESS_KEY', $app_key);

					$fetch_user=$this->Msociety->get_fms_tockens($userDataId);

					if(!empty($fetch_user))
					{
						foreach($fetch_user AS $user_dt)
						{
							$push_body="you are not a committee member of your society ";
							$push_title="Important";
							$push_type="Greeting";          
							$typedata=array
							(
								'body' 	=> $push_body,
								'title'	=> $push_title,
								'type' => $push_type,
								'priority' => true
							);
							$value=$user_dt['tocken'];
							$fields = array
							(
								'to' => $value,
								'data' => $typedata
							);
							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
							);
							// 			print_r($typedata);
							// 			print_r($fields);
							// 			print_r($headers);
							#Send Repo/nse To FireBase Server	
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$output = curl_exec($ch);
							//  	print_r($output);
							curl_close( $ch );
						}
					}
				}
			}

			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Member has been not removed');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."managing_commitee/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Member has been removed successfully...');
			redirect(base_url()."managing_commitee/list", 'refresh');
		}          
	}
}
