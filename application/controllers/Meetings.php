<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meetings extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='MEETINGS';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->meetid_tbl=$socTables['meetid_tbl'];
		$this->meet_poll_tbl=$socTables['meet_poll_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Meetings';
    }

    public function home()
    {
    	$today_date = date('Y-m-d');

    	$breadcrumbArr[0]['name']="Notices & Meetings";
    	$breadcrumbArr[0]['link']=base_url()."meetings/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Meetings";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		// $fetch=mysqli_query($conn,"SELECT `meet-header` As meetingTitle, 
		// `meet-sub-header` As meetingDescr, (SELECT COUNT(*) FROM `meetid`
		// WHERE `meetid`=meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll1`)
		// As Available, (SELECT COUNT(*) FROM `meetid` WHERE `meetid`=meeting.`meeting-id` 
		// AND `meet-result`=meeting.`meet-poll2`) As Notavailable, `meeting-id` As meetingId,
		// DATE_FORMAT(`start_date`,'%d/%m/%Y') As meeting_date, `start_time` As meeting_time FROM `meet-poll` meeting WHERE
		// DATE_FORMAT(`start_date`,'%Y-%m-%d')>='$today_date' AND `meeting_status`='Active' ORDER BY `meet-id` DESC");

    	$mtg_condtnArr["DATE_FORMAT(`start_date`,'%Y-%m-%d')>="]=$today_date;
    	$mtg_condtnArr['`meeting_status`']='Active';
    	$mtg_orderByArr['`meet-id`']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->meet_poll_tbl." AS meeting", $colNames="`meet-header` As meetingTitle, `meet-sub-header` As meetingDescr, (SELECT COUNT(*) FROM ".$this->meetid_tbl." WHERE `meetid`=meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll1`) As Available, (SELECT COUNT(*) FROM ".$this->meetid_tbl." WHERE `meetid` = meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll2`) As Notavailable, `meeting-id` As meetingId, DATE_FORMAT(`start_date`,'%d/%m/%Y') As meeting_date, `start_time` As meeting_time",$mtg_condtnArr, $mtg_likeCondtnArr=array(), $mtg_joinArr=array(), $singleRow=FALSE, $mtg_orderByArr, $mtg_groupByArr=array(), $mtg_whereInArray=array(), $mtg_customWhereArray=array(), $backTicks=TRUE);

		$meeting_data=$query['userdata'];

		$this->data['meeting_data']=$meeting_data;

   		$this->data['view']="meetings/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function meeting_result()
    {
    	$head=$this->uri->segment('3');
	    $opt=$this->uri->segment('4');

	    $breadcrumbArr[0]['name']="Notices & Meetings";
    	$breadcrumbArr[0]['link']=base_url()."meetings/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Meetings";
    	$breadcrumbArr[1]['link']=base_url()."meetings/home";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="View Result";
    	$breadcrumbArr[2]['link']="javascript:void(0)";
    	$breadcrumbArr[2]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
	    // $meeting_attending=meeting_attending($head,$opt,$societyKey);

	    // function meeting_attending($head,$opt,$soc_key)

	    // $fetch=mysqli_query($conn,"SELECT (SELECT `s-r-fname` FROM `s-r-user` WHERE `s-r-username`=`meetid`.`meet-username`) As fname,(SELECT `s-r-lname` FROM `s-r-user` WHERE `s-r-username`=`meetid`.`meet-username`) As lname FROM `meetid`  WHERE `meet-result` ='$opt' AND `meetid` ='$head'");

	    $mtg_condtnArr["meet-result"]=$opt;
    	$mtg_condtnArr['`meetid`']=$head;

		$query=$this->Mcommon->getRecords($tableName=$this->meetid_tbl, $colNames="(SELECT `s-r-fname` FROM ".$this->s_r_user_tbl." WHERE `s-r-username`=`meetid`.`meet-username`) As fname, (SELECT `s-r-lname` FROM ".$this->s_r_user_tbl." WHERE `s-r-username`=`meetid`.`meet-username`) As lname",$mtg_condtnArr, $mtg_likeCondtnArr=array(), $mtg_joinArr=array(), $singleRow=FALSE, $mtg_orderByArr=array(), $mtg_groupByArr=array(), $mtg_whereInArray=array(), $mtg_customWhereArray=array(), $backTicks=TRUE);

		$meeting_attending=$query['userdata'];

		$this->data['meeting_attending']=$meeting_attending;

    	$this->data['view']="meetings/meeting_result";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function add_meeting()
    {
    	$this->form_validation->set_rules('meet1', 'Meeting Topic', 'trim|required');
    	$this->form_validation->set_rules('meet2', 'Meeting Agenda', 'trim|required');
    	$this->form_validation->set_rules('meet3', 'Meeting Date', 'trim|required');
    	$this->form_validation->set_rules('meet_time', 'Meeting Time', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			// $this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			// redirect(base_url()."vendor/society_vendor", 'refresh');
			$this->home();
		}
		else
		{
	    	$this->db->trans_start();

			$meet_title = $this->input->post('meet1');
			$meet_desc = $this->input->post('meet2');
			$start_time = date('H:i A', strtotime($this->input->post('meet_time')));
			$start_date = date("Y-m-d",strtotime($this->input->post('meet3')));
			$result="";
			$response="";

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($meet_title) && !empty($meet_desc) && !empty($start_date) && !empty($start_time))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						$meet_title=str_replace("'", "\'", $meet_title);

						$meet_desc=str_replace("'", "\'", $meet_desc);

						$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
						$onetime = substr($random,0,4);
						$postid = "meet-".$onetime;
						$today_date = date('Y-m-d');
						$meet_enddt=date('Y-m-d', strtotime($start_date . "-1 days"));

						// $query ="INSERT INTO `meet-poll` (`meet-start-date`, `meet-end-date`, `meet-header`, `meet-sub-header`, `start_time`, `start_date`, `meeting-id`)
						// VALUES ('$today_date', '$meet_enddt', '$meet_title', '$meet_desc', '$start_time', '$start_date', '$postid')";

						// $fire=mysqli_query($conn,$query);

						$mtg_InsertArr[]=array(
							'meet-start-date' => $today_date,
							'meet-end-date' => $meet_enddt,
							'meet-header' => $meet_title,
							'meet-sub-header' => $meet_desc,
							'start_time' => $start_time,
							'start_date' => $start_date,
							'meeting-id' => $postid,
							'createdBy' => $this->user_name,
							'createdDatetime' => $this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->meet_poll_tbl, $mtg_InsertArr, $returnType="");

						$mtg_InsertStatus=$query['status'];

						// if($fire)
						if($mtg_InsertStatus==TRUE)
						{
							// $value = mysqli_query($conn,"UPDATE `s-r-user` SET `meeting_no` = `meeting_no`+1 ")or die("error..".mysqli_error($conn));

							$setUpdateArr=array(
								'`meeting_no`'=>'`meeting_no`+1',
								'updatedBy'=> "'".$this->user_name."'",
								'updatedDatetime'=> "'".$this->curr_datetime."'"
							);

							$query=$this->Mcommon->setUpdate($tableName=$this->s_r_user_tbl, $setUpdateArr, $updtCondtnArr=array(), $likeCondtnArr=array());

							$result='Meeting poll has been created';

							// $logModule="Meeting";
							// $logDescription=$result;
							// $user_id=$postid;

							// $log_entry=mysqli_query($conn,"INSERT INTO `society_log`(`logModule`, `logDescription`, `user_id`, `logTime`) VALUES ('$logModule','$logDescription','$user_id','$log_date')");
							// $push_body=$meet_title." on ".date("d/m/Y",strtotime($start_date))." ".$start_time;
							// $push_title="New Meeting ";
							// $push_type="Meeting";

							//echo "push_notify($push_body,$push_title,$push_type,$soc_key)";
							// push_notify($push_body,$push_title,$push_type,$soc_key);

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

				            $this->load->library('Push_notify');

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']='';
				            $notifyArr['title']='New Meeting ';
				            $notifyArr['body']=$meet_title." on ".date("d/m/Y",strtotime($start_date))." ".$start_time;
				            $notifyArr['pushType']='Meeting';

				            $notifyRes=$this->push_notify->notification($notifyArr);
						}

						if(empty($result))
						{
							$response='Meeting not added';
						}
						else
						{
							$response='Meeting  added';
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
		            redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Meeting has not added.');
				redirect(base_url()."meetings/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Your changes has been successfully done.');
				redirect(base_url()."meetings/home", 'refresh');
			}     
		}
    }

    public function edit_meeting()
    {
    	$meetingId=$this->uri->segment('3');
	    $result="";
	    $response="";

	    $breadcrumbArr[0]['name']="Notices & Meetings";
    	$breadcrumbArr[0]['link']=base_url()."meetings/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Meetings";
    	$breadcrumbArr[1]['link']=base_url()."meetings/home";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Meeting";
    	$breadcrumbArr[2]['link']="javascript:void(0)";
    	$breadcrumbArr[2]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
		// $fetch=mysqli_query($conn,"SELECT `meet-header` As meetingTitle, 
		// `meet-sub-header` As meetingDescr, (SELECT COUNT(*) FROM `meetid`
		// WHERE `meetid`=meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll1`)
		// As Available, (SELECT COUNT(*) FROM `meetid` WHERE `meetid`=meeting.`meeting-id` 
		// AND `meet-result`=meeting.`meet-poll2`) As Notavailable, `meeting-id` As meetingId,
		// `start_date` As meeting_date, `start_time` As meeting_time FROM `meet-poll` meeting WHERE `meeting_status`='Active' AND `meeting-id`='$meetId' ORDER BY `meet-id` DESC");
    	
    	$this->form_validation->set_rules('meet1', 'Meeting Topic', 'trim|required');
    	$this->form_validation->set_rules('meet2', 'Meeting Agenda', 'trim|required');
    	$this->form_validation->set_rules('meet3', 'Meeting Date', 'trim|required');
    	$this->form_validation->set_rules('meet_time', 'Meeting Time', 'trim|required');
	    $this->form_validation->set_rules('meetId', 'Meeting ID', 'trim');
		$this->form_validation->set_rules('meet6', 'Day Time', 'trim');

		if($this->form_validation->run() == FALSE)
		{
	    	$mtg_condtnArr["meeting_status"]='Active';
	    	$mtg_condtnArr['`meeting-id`']=$meetingId;
	    	$mtg_orderByArr['`meet-id`']='DESC';

			$query=$this->Mcommon->getRecords($tableName=$this->meet_poll_tbl." AS meeting", $colNames=" `meet-header` As meetingTitle, `meet-sub-header` As meetingDescr, (SELECT COUNT(*) FROM ".$this->meetid_tbl." WHERE `meetid`=meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll1`) As Available, (SELECT COUNT(*) FROM ".$this->meetid_tbl." WHERE `meetid`=meeting.`meeting-id` AND `meet-result`=meeting.`meet-poll2`) As Notavailable, `meeting-id` As meetingId, `start_date` As meeting_date, `start_time` As meeting_time",$mtg_condtnArr, $mtg_likeCondtnArr=array(), $mtg_joinArr=array(), $singleRow=TRUE, $mtg_orderByArr, $mtg_groupByArr=array(), $mtg_whereInArray=array(), $mtg_customWhereArray=array(), $backTicks=TRUE);

			$meeting_data=$query['userdata'];

			$this->data['meeting_data']=$meeting_data;

	   		$this->data['view']="meetings/edit_meeting";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
			$this->db->trans_start();

			$meetId = $this->input->post('meetId');
			$meet_title = $this->input->post('meet1');
			$meet_desc = $this->input->post('meet2');
			$start_time =date('H:i A', strtotime($this->input->post('meet_time')));
			$start_date = date("Y-m-d", strtotime($this->input->post('meet3')));

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($meet_title) && !empty($meet_desc) && !empty($start_date) && !empty($start_time))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						// $result=edit_meeting($userName, $soc_key, $meetingId, $topic, $meetingDetails, $meettime, $meetdate);
						// function edit_meeting($username, $soc_key, $meetId, $meet_title, $meet_desc, $start_time, $start_date)

						$meet_title=str_replace("'","\'",$meet_title);

						$meet_desc=str_replace("'","\'",$meet_desc);
						$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
						$onetime = substr($random,0,4);
						$postid = "meet-".$onetime;
						$today_date = date('Y-m-d');
						$meet_enddt=date('Y-m-d',strtotime($start_date . "-1 days"));

						// $query ="INSERT INTO `meet-poll` (`meet-start-date`, `meet-end-date`, `meet-header`, `meet-sub-header`, `start_time`, `start_date`, `meeting-id`)
						// VALUES ('$today_date', '$meet_enddt', '$meet_title', '$meet_desc', '$start_time', '$start_date', '$postid')";
						// $fire =mysqli_query($conn,$query);

						$mtg_InsertArr[]=array(
							'meet-start-date' => $today_date,
							'meet-end-date' => $meet_enddt,
							'meet-header' => $meet_title,
							'meet-sub-header' => $meet_desc,
							'start_time' => $start_time,
							'start_date' => $start_date,
							'meeting-id' => $postid,
							'createdBy' => $this->user_name,
							'createdDatetime' => $this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->meet_poll_tbl, $mtg_InsertArr, $returnType="");

						$mtg_InsertStatus=$query['status'];

						// if($fire)
						if($mtg_InsertStatus==TRUE)
						{
							// $value = mysqli_query($conn,"DELETE FROM `meet-poll` WHERE `meeting-id`='$meetId'")or die("error..".mysqli_error($conn));

							// $value1 = mysqli_query($conn,"DELETE FROM `meetid` WHERE `meetid`='$meetId'")or die("error..".mysqli_error($conn));

							$poll_del_condtnArr['`meet-poll`.`meeting-id`']=$meetId;

							$query=$this->Mcommon->delete($tableName=$this->meet_poll_tbl, $poll_del_condtnArr, $likeCondtnArr=array(), $whereInArray=array());

							$meet_del_condtnArr['`meetid`.`meetid`']=$meetId;

							$query=$this->Mcommon->delete($tableName=$this->meetid_tbl, $meet_del_condtnArr, $likeCondtnArr=array(), $whereInArray=array());

							$result='Meeting poll has been updated';

							// $logModule="Meeting";
							// $logDescription=$result;
							// $user_id=$postid;

							// $log_entry=mysqli_query($conn,"INSERT INTO `society_log`(`logModule`, `logDescription`, `user_id`, `logTime`) VALUES ('$logModule','$logDescription','$user_id','$log_date')");

							// $push_body=$meet_title." on ".date("d/m/Y",strtotime($start_date))." ".$start_time;
							// $push_title="New Meeting ";
							// $push_type="Meeting";

							// push_notify($push_body,$push_title,$push_type,$soc_key);

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

				            $this->load->library('Push_notify');

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']='';
				            $notifyArr['title']='New Meeting ';
				            $notifyArr['body']=$meet_title." on ".date("d/m/Y",strtotime($start_date))." ".$start_time;
				            $notifyArr['pushType']='Meeting';

				            $notifyRes=$this->push_notify->notification($notifyArr);
						}

						if(empty($result))
						{
							$response='Meeting not added';
						}
						else
						{
							$response='Meeting updated added';
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
	            	redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Meeting has not edited.');
				redirect(base_url()."meetings/home", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Your changes has been successfully done.');
				redirect(base_url()."meetings/home", 'refresh');
			}     
		}
    }

    public function delete_meeting()
    {
    	$meetId=$this->uri->segment('3');
    	$result="";

    	// $value = mysqli_query($conn,"UPDATE `notices` SET `active`= 'Inactive' WHERE `no_id`='$noticeId'");

    	$noc_UpdateArr=array(
			'meeting_status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['`meet-poll`.`meeting-id`']=$meetId;

		$query=$this->Mcommon->update($tableName=$this->meet_poll_tbl, $noc_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
              
    	// if($value)
		if($query['status']==TRUE)
    	{
    		$result="Meeting removed sucessfully";
    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$meetId);
    	}

		if(empty($result))
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Meetings has not removed.');
			redirect(base_url()."meetings/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Meeting has removed');
			redirect(base_url()."meetings/home", 'refresh');
		}
    }
}

?>