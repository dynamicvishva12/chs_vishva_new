<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_letter extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='NEWSFEED';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->family_tbl=$socTables['family_tbl'];
		$this->news_letter_tbl=$socTables['news_letter_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Newsletter';
    }

	public function home()
	{
		$breadcrumbArr[0]['name']="Newsletter";
    	$breadcrumbArr[0]['link']=base_url()."news_letter/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Newsletter list";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
		// $fetch = mysqli_query($conn,"SELECT * FROM `family`");

		// $fetch = mysqli_query($conn,"SELECT `name`,`dob`,`user_name` FROM `family`");
                        
		// while($bday = mysqli_fetch_assoc($fetch))
		// {
		// 	if (date("m",strtotime($bday['dob']))==date("m") && date("d",strtotime($bday['dob']))==date("d") ) {

		// 	$result[]=$bday;
		// }

		$query=$this->Mcommon->getRecords($tableName=$this->family_tbl, $colNames="family.name, family.dob, family.user_name", $bthdy_condtnArr=array(), $likeCondtnArr=array(), $bthdy_joinArr=array(), $singleRow=FALSE, $bthdy_orderByArr=array(), $bthdy_groupByArr=array(), $bthdy_whereInArray=array(), $bthdy_customWhereArray=array(), $backTicks=TRUE);

		$bthday_arr=$query['userdata'];

		$birthday_data=array();

		if(!empty($bthday_arr))
		{
			foreach($bthday_arr AS $bday)
			{
				if(date("m",strtotime($bday['dob']))==date("m") && date("d",strtotime($bday['dob']))==date("d"))
				{
					$birthday_data[]=$bday;
				}
			}
		}

		$this->data['birthday_data']=$birthday_data;

		$today_date = date('Y-m-d');
        // $fetch=mysqli_query($conn,"SELECT `sr_id`as nwId,`nl_desc` FROM `news_letter` WHERE `nl_date` = '$today_date'  ORDER BY `nl_date` DESC");

		$nwltr_condtnArr['news_letter.nl_date']=$today_date;
		$nwltr_orderByArr['news_letter.nl_date']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->news_letter_tbl, $colNames="news_letter.sr_id AS nwId, news_letter.nl_desc", $nwltr_condtnArr, $likeCondtnArr=array(), $nwltr_joinArr=array(), $singleRow=FALSE, $nwltr_orderByArr, $nwltr_groupByArr=array(), $nwltr_whereInArray=array(), $nwltr_customWhereArray=array(), $backTicks=TRUE);

		$news_data=$query['userdata'];

		$this->data['news_data']=$news_data;

		$this->data['view']="news_letter/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_news_letter()
	{
		$this->db->trans_start();

		$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			redirect(base_url()."news_letter/home", 'refresh');
		}
		else
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
					$newsDesc=$this->input->post('description');

					// $result=Add_newsfeed($userName, $soc_key, $newsDesc);

					// function Add_newsfeed($username,$soc_key,$newsDesc)

					$newsDesc=str_replace("'","\'",$newsDesc);
					$today_date = date('Y-m-d');

					// $insertquery="INSERT INTO `news_letter` (`nl_desc`,`nl_user`, `nl_date`) VALUES ('$newsDesc','$username', '$today_date')";

					// $fire=mysqli_query($conn,$insertquery);

					$nwInsertArr[]=array(
						'nl_desc'=>$newsDesc,
						'nl_user'=>$this->user_name,
						'nl_date'=>$today_date,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->news_letter_tbl, $nwInsertArr, $returnType="");

					$nwInsertStatus=$query['status'];

					if($nwInsertStatus==TRUE)
					{
						$response='News has been posted';

						$this->load->library('Push_notify');

			            $notifyArr['userType']='user app';
			            $notifyArr['userName']='';
			            $notifyArr['title']='Society Newsfeed ';
			            $notifyArr['body']=$newsDesc;
			            $notifyArr['pushType']='NewsFeed';

			            $notifyRes=$this->push_notify->notification($notifyArr);

						$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
					}
					else
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! News not added.');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."news_letter/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'News has been added successfully');
			redirect(base_url()."news_letter/home", 'refresh');
		}     
	}

	public function delete_news_letter()
	{
		$this->db->trans_start();

		$newsId=$this->uri->segment('3');

		// $result=delete_newsId($userName, $societyKey, $newsId);

		// function delete_newsId($username,$soc_key,$newsId)

		// $value = mysqli_query($conn,"DELETE FROM `news_letter` WHERE `sr_id`='$newsId'");

		$delCondtnArr['news_letter.sr_id']=$newsId;

		$query=$this->Mcommon->delete($tableName=$this->news_letter_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==TRUE)
		{
			$this->Mlogger->log($logModule=$this->logModule, $logDescription="News removed sucessfully", $userId=$this->user_name);
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! News not removed.');
			redirect(base_url()."news_letter/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'News removed sucessfully');
			redirect(base_url()."news_letter/home", 'refresh');
		}   
	}
}

?>