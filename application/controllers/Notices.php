<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notices extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='NOTICES';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->notices_tbl=$socTables['notices_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Notices';
    }

    public function list()
    {
    	$today_date = date('Y-m-d');

    	$breadcrumbArr[0]['name']="Notices & Meetings";
    	$breadcrumbArr[0]['link']=base_url()."notices/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Notices";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	// $fetch=mysqli_query($conn,"SELECT `no_id` As noticeId,`no_filetype` As noticeFileType, `no_descriction` As noticeDesc, `notice_header` As noticeTitle, CONCAT('http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/IMAGES/Notice/',`no-image`) As noticeFilename, DATE_FORMAT(`no_expired_at`,'%d %b, %Y') As noticeDate FROM `notices` WHERE `Sheduled`='not schedule' AND `no_expired_at`>='$today_date' AND `active`='active' ORDER BY `sr_id` DESC");

    	$noc_condtnArr['notices.Sheduled']='not schedule';
    	$noc_condtnArr['notices.no_expired_at >=']=$today_date;
    	$noc_condtnArr['notices.active']='active';
    	$noc_orderByArr['notices.sr_id']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->notices_tbl, $colNames="`no_id` As noticeId, `no_filetype` As noticeFileType, `no_descriction` As noticeDesc, `notice_header` As noticeTitle, no_type1, CONCAT('".$this->assets_url."IMAGESDRIVE/Notice/',`no-image`) As noticeFilename, DATE_FORMAT(`no_expired_at`,'%d %b, %Y') As noticeDate", $noc_condtnArr, $noc_likeCondtnArr=array(), $noc_joinArr=array(), $singleRow=FALSE, $noc_orderByArr, $noc_groupByArr=array(), $noc_whereInArray=array(), $noc_customWhereArray=array(), $backTicks=TRUE);

		$notice_data=$query['userdata'];

		$this->data['notice_data']=$notice_data;

   		$this->data['view']="notices/notice_list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function add()
    {
		$this->form_validation->set_rules('hnotice', 'Notice Title', 'trim|required');
    	$this->form_validation->set_rules('expdt', 'Notice Expiry Date', 'trim|required');
		$this->form_validation->set_rules('dnotice', 'Notice Description', 'trim|required');
		$this->form_validation->set_rules('radios', 'Notice type', 'trim|required');
		$this->form_validation->set_rules('myfile', 'Document', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			// $this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			// redirect(base_url()."vendor/society_vendor", 'refresh');
			$this->list();
		}
		else
		{
	    	$this->db->trans_start();

			$expiryDate= date("Y-m-d",strtotime($this->input->post('expdt')));
			$noticeDesc=$this->input->post('dnotice');
			$noticeTopic=$this->input->post('hnotice');
			$response="";

			if(!empty($_FILES['myfile']['name'])) 
			{
				$filetype="image";
				$post_image= $_FILES['myfile']['name'];
				$t_tmp=$_FILES['myfile']['tmp_name'];
				$temp = explode(".", $_FILES["myfile"]["name"]);
				$docName = round(microtime(true)) . '.' . end($temp);
				$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
				$store=FCPATH."assets/IMAGESDRIVE/Notice/".$docName;

				if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
				{
					move_uploaded_file($t_tmp, $store);
					if(end($temp)=="pdf")
					{
						$filetype="pdf";
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
					redirect(base_url()."notices/list", 'refresh');
				}

			}
			else
			{
				$docName=$filetype=NULL;
			}

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($expiryDate) && !empty($noticeDesc) && !empty($noticeTopic))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						// $result=Add_notice($userName,$soc_key,$no_title,$no_descriction,$post_expiredt,$encodedString,$newfilename,$file_Type,$callfrom);

						// function Add_notice($username,$soc_key,$noticeTopic,$noticeDesc,$expiryDate,$encodedString,$docName,$filetype,$callfrom)


						$noticeTopic=str_replace("'","\'",$noticeTopic);

						$noticeDesc=str_replace("'","\'",$noticeDesc);

						// include '../database_connection/s-conn.php';

						function base64_to_jpeg($base64_string, $output_file) 
						{
							$ifp = fopen( $output_file, 'wb' ); 
							fwrite( $ifp, base64_decode( $base64_string ) );
							fclose( $ifp );
							return( $output_file );
						}

						if(!empty($encodedString))
						{
							$pic1=FCPATH.'assets/IMAGESDRIVE/Notice/'.$docName;
							$image1 = base64_to_jpeg( $encodedString, $pic1 );
						}


						$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
						$onetime = substr($random,0,4);
						$postid = "notice-".$onetime;
						$today_date = date('Y-m-d');

						if(!empty($docName))
						{
							// $insertquery="INSERT INTO `notices` (`user_name`, `no_id`, `no_type1`, `no_descriction`,`no-image`, `no_created_at`, `no_expired_at`,`notice_header`, `no_filetype`) VALUES ('$username', '$postid', 'All', '$noticeDesc','$docName','$today_date','$expiryDate','$noticeTopic','$filetype')";

							$noc_InsertArr[]=array(
								'user_name' => $this->user_name,
								'no_id' => $postid,
								'no_type1' => 'All',
								'no_descriction' => $noticeDesc,
								'no-image' => $docName,
								'no_created_at' => $today_date,
								'no_expired_at' => $expiryDate,
								'notice_header' => $noticeTopic,
								'no_filetype' => $filetype,
								'createdBy' => $this->user_name,
								'createdDatetime' => $this->curr_datetime
							);
						}
						else
						{
							// $insertquery="INSERT INTO `notices` (`user_name`, `no_id`, `no_type1`, `no_descriction`,`no-image`, `no_created_at`, `no_expired_at`,`notice_header`, `no_filetype`) VALUES ('$username', '$postid', 'All', '$noticeDesc',NULL,'$today_date','$expiryDate','$noticeTopic',NULL)";

							$noc_InsertArr[]=array(
								'user_name' => $this->user_name,
								'no_id' => $postid,
								'no_type1' => 'All',
								'no_descriction' => $noticeDesc,
								'no-image' => NULL,
								'no_created_at' => $today_date,
								'no_expired_at' => $expiryDate,
								'notice_header' => $noticeTopic,
								'no_filetype' => NULL,
								'createdBy' => $this->user_name,
								'createdDatetime' => $this->curr_datetime
							);
						}
						// $fire=mysqli_query($conn,$insertquery);

						$query=$this->Mcommon->insert($tableName=$this->notices_tbl, $noc_InsertArr, $returnType="");

						$noc_InsertStatus=$query['status'];

						if($noc_InsertStatus==TRUE)
						{
							// $value = mysqli_query($conn,"UPDATE `s-r-user` SET `notice-no` = `notice-no`+1 ");

							$setUpdateArr=array(
								'`notice-no`'=>'`notice-no`+1',
								'updatedBy'=> "'".$this->user_name."'",
								'updatedDatetime'=> "'".$this->curr_datetime."'"
							);

							$query=$this->Mcommon->setUpdate($tableName=$this->s_r_user_tbl, $setUpdateArr, $updtCondtnArr=array(), $likeCondtnArr=array());

							$result='Notice has been created';

							$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-email", $usr_condtnArr=array(), $usr_likeCondtnArr=array(), $usr_joinArr=array(), $singleRow=FALSE, $usr_orderByArr=array(), $usr_groupByArr=array(), $usr_whereInArray=array(), $usr_customWhereArray=array(), $backTicks=TRUE);

							$usr_data=$query['userdata'];

							if(!empty($usr_data))
							{
								foreach($usr_data AS $e_dt)
								{
									$subject="New Society Notice ";
						            $message="Dear User, <br><br><br>


									New Notice is Created <br><br>
									".$noticeTopic."<br><br>

									Do Check in Notice tab.. <br><br><br>

									Best Regards,<br>
									CHSVishva<br><br><br>

									Note : Please do not reply back to this mail. This is sent from an unattended  <br>             
									mail box. Please mark all your queries / responses to info@chsvishva.com ";

						            $alt_message="This is the body in plain text for non-HTML mail clients";

						            $email=$e_dt['s-r-email'];

						            $mailDataArr['to']=$email;
						            $mailDataArr['subject']=$subject;
						            $mailDataArr['alt_message']=$alt_message;
						            $mailDataArr['message']=$message;

						            $this->load->library('Email_sending');

						            $response=$this->email_sending->send($mailDataArr);

						            $responseStatus=$response['status'];
						            $responseMsg=$response['message'];
								}
							}

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

				            $this->load->library('Push_notify');

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']='';
				            $notifyArr['title']='Society Notice ';
				            $notifyArr['body']=$noticeTopic;
				            $notifyArr['pushType']='Notices';

				            $notifyRes=$this->push_notify->notification($notifyArr);
						}

						if(empty($result))
						{
							$response='Notice not added';
						}
						else
						{
							$response='Notice  added';
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Notice has not added.');
				redirect(base_url()."notices/list", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Notice has been added successfully');
				redirect(base_url()."notices/list", 'refresh');
			}     
		}
    }

    public function edit()
    {
	    $noticeId=$this->uri->segment('3');
	    $response="";

	    $breadcrumbArr[0]['name']="Notices & Meetings";
    	$breadcrumbArr[0]['link']=base_url()."notices/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Notices";
    	$breadcrumbArr[1]['link']=base_url()."notices/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Notice";
    	$breadcrumbArr[2]['link']="javascript:void(0)";
    	$breadcrumbArr[2]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	// $fetch=mysqli_query($conn,"SELECT `no_id` As noticeId,`no_filetype` As noticeFileType, `no_descriction` As noticeDesc, `notice_header` As noticeTitle, CONCAT('http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/IMAGES/Notice/',`no-image`) As noticeFilename,`no_expired_at` As noticeDate FROM `notices` WHERE `no_id`='$noticeId'");

	    $this->form_validation->set_rules('radios', 'Notice Type', 'trim|required');
		$this->form_validation->set_rules('hnotice', 'Title', 'trim|required');
		$this->form_validation->set_rules('expdt', 'Expiry Date', 'trim|required');
		$this->form_validation->set_rules('dnotice', 'Detailed Notice', 'trim|required');
		$this->form_validation->set_rules('myfile', 'Document', 'trim');

		if($this->form_validation->run() == FALSE)
		{
	    	$noc_condtnArr['notices.no_id']=$noticeId;

			$query=$this->Mcommon->getRecords($tableName=$this->notices_tbl, $colNames="`no_id` As noticeId, `no_filetype` As noticeFileType, `no_descriction` As noticeDesc, `notice_header` As noticeTitle, CONCAT('".$this->assets_url."IMAGESDRIVE/Notice/',`no-image`) As noticeFilename,`no_expired_at` As noticeDate", $noc_condtnArr, $noc_likeCondtnArr=array(), $noc_joinArr=array(), $singleRow=TRUE, $noc_orderByArr=array(), $noc_groupByArr=array(), $noc_whereInArray=array(), $noc_customWhereArray=array(), $backTicks=TRUE);

			$notice_data=$query['userdata'];

			$this->data['notice_data']=$notice_data;

	   		$this->data['view']="notices/edit_notice";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
			$this->db->trans_start();

			$noticeId = $this->input->post('noticeId');
			$post_expiredt= date("Y-m-d",strtotime($this->input->post('expdt')));
			$no_title=$this->input->post('hnotice');
			$no_descriction=$this->input->post('dnotice');
			$nameold=$this->input->post('nameold');
			$imaged_status=$this->input->post('image_status');

			if($_POST['image_status']=="unchanged")
			{
				$newfilename=$nameold;
				$file_Type=$this->input->post('nofiletype');
			}
			else
			{
				if(!empty($_FILES['myfile']['name'])) 
				{
					$file_Type="image";
					$post_image= $_FILES['myfile']['name'];
					$t_tmp=$_FILES['myfile']['tmp_name'];
					$temp = explode(".", $_FILES["myfile"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
					$store=FCPATH."assets/IMAGESDRIVE/Notice/".$newfilename;

					if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
					{
						move_uploaded_file($t_tmp, $store);

						if(end($temp)=="pdf")
						{
							$file_Type="pdf";
						}
					}
					else
					{
						$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
						redirect(base_url()."notices/edit/".$noticeId, 'refresh');
					}
				}
				else
				{
					$newfilename=$file_Type=NULL;
					$imaged_status="Removed";
				}
			} 

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($post_expiredt) && !empty($no_descriction) && !empty($no_title) && !empty($noticeId))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                // echo "<html><body><script>alert('userId invalid')</script></body></html>";

		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						$callfrom="WEB";
						// $result=edit_notice($userName, $soc_key, $noticeId, $no_title, $no_descriction, $post_expiredt, $encodedString, $newfilename, $file_Type, $callfrom, $imaged_status);

						// function edit_notice($username, $soc_key, $noticeId, $noticeTopic, $noticeDesc, $expiryDate, $encodedString, $docName, $filetype, $callfrom, $imaged_status)

						$no_title=str_replace("'","\'",$no_title);

						$no_descriction=str_replace("'","\'",$no_descriction);

						function base64_to_jpeg($base64_string, $output_file) 
						{
							$ifp = fopen( $output_file, 'wb' ); 
							fwrite( $ifp, base64_decode( $base64_string ) );
							fclose( $ifp );
							return( $output_file );
						}

						if($callfrom=="APP")
						{
							if(!empty($encodedString) && $imaged_status=="changed")
							{
								$pic1=FCPATH.'assets/IMAGESDRIVE/Notice/'.$newfilename;
								$image1 = base64_to_jpeg( $encodedString, $pic1 );
							}
						}

						$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
						$onetime = substr($random,0,4);
						$postid = "notice-".$onetime;
						$today_date = date('Y-m-d');

						if(!empty($newfilename) && ($imaged_status=="changed" || $imaged_status=="unchanged"))
						{
							// $insertquery="INSERT INTO `notices` (`user_name`, `no_id`, `no_type1`, `no_descriction`,`no-image`, `no_created_at`, `no_expired_at`,`notice_header`, `no_filetype`) VALUES ('$this->user_name', '$postid', 'All', '$no_descriction','$newfilename','$today_date','$post_expiredt','$no_title','$file_Type')";

							$noc_InsertArr[]=array(
								'user_name' => $this->user_name,
								'no_id' => $postid,
								'no_type1' => 'All',
								'no_descriction' => $no_descriction,
								'no-image' => $newfilename,
								'no_created_at' => $today_date,
								'no_expired_at' => $post_expiredt,
								'notice_header' => $no_title,
								'no_filetype' => $file_Type,
								'createdBy' => $this->user_name,
								'createdDatetime' => $this->curr_datetime
							);
						}
						else
						{
							// $insertquery="INSERT INTO `notices` (`user_name`, `no_id`, `no_type1`, `no_descriction`,`no-image`, `no_created_at`, `no_expired_at`,`notice_header`, `no_filetype`) VALUES ('$this->user_name', '$postid', 'All', '$no_descriction',NULL,'$today_date','$post_expiredt','$no_title',NULL)";

							$noc_InsertArr[]=array(
								'user_name' => $this->user_name,
								'no_id' => $postid,
								'no_type1' => 'All',
								'no_descriction' => $no_descriction,
								'no-image' => NULL,
								'no_created_at' => $today_date,
								'no_expired_at' => $post_expiredt,
								'notice_header' => $no_title,
								'no_filetype' => NULL,
								'createdBy' => $this->user_name,
								'createdDatetime' => $this->curr_datetime
							);
						}

						// $fire=mysqli_query($conn, $insertquery);

						// if($fire)
						// {
						$query=$this->Mcommon->insert($tableName=$this->notices_tbl, $noc_InsertArr, $returnType="");

						$noc_InsertStatus=$query['status'];

						if($noc_InsertStatus==TRUE)
						{
							// $value = mysqli_query($conn,"DELETE FROM `notices` WHERE `no_id`='$noticeId'");

							$delCondtnArr['notices.no_id']=$noticeId;

							$query=$this->Mcommon->delete($tableName=$this->notices_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

							$result='Notice has been created';

							// $fetch=mysqli_query($conn,"SELECT * FROM `email_account`");
							// $email_data=mysqli_fetch_assoc($fetch);
							// $host=$email_data['host'];
							// $emailid=$email_data['email_id'];
							// $epassword=$email_data['password'];

							// ini_set("display_errors","On");
							// include_once("email/class.phpmailer.php");
							// $mail = new PHPMailer();
							// $mail->IsSMTP();                               // set mailer to use SMTP
							// $mail->Host = "$host";                  // specify main and backup server
							// $mail->SMTPAuth = true;                        // turn on SMTP authentication
							// $mail->Username = "$emailid";   // SMTP username
							// $mail->Password = "$epassword";                  // SMTP password

							// $mail->From = "$emailid";
							// $mail->FromName = "CHSVishva";


							// $ufetch=mysqli_query($conn,"SELECT `s-r-email` FROM `s-r-user`");
							// while($usermail=mysqli_fetch_assoc($ufetch))
							// {
							// 	$usermailid=$usermail['s-r-email'];
							// 	$mail->AddAddress("$usermailid");
							// }
							// //$mail->AddAddress("dynamicvishvateam@gmail.com");

							// $mail->WordWrap = 50;                       // set word wrap to 50 characters
							// $mail->IsHTML(true);                                // set email format to HTML

							// $mail->Subject = "New Society Notice ";
							// $mail->Body    = "Dear User, <br><br><br>


							// New Notice is Created <br><br>
							// $no_title<br><br>

							// Do Check in Notice tab.. <br><br><br>

							// Best Regards,<br>
							// CHSVishva<br><br><br>

							// Note : Please do not reply back to this mail. This is sent from an unattended  <br>             
							// mail box. Please mark all your queries / responses to info@chsvishva.com ";
							// $mail->AltBody = "This is the body in plain text for non-HTML mail clients";

							// if(!$mail->Send())
							// {
							// 	echo "Message could not be sent. <p>";
							// 	echo "Mailer Error: " . $mail->ErrorInfo;
							// 	exit;
							// }

							// $logModule="Notice";
							// $logDescription=$result;
							// $user_id=$postid;
							// $log_entry=mysqli_query($conn,"INSERT INTO `society_log`(`logModule`, `logDescription`, `user_id`, `logTime`) VALUES ('$logModule','$logDescription','$user_id','$log_date')");

							$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-email", $usr_condtnArr=array(), $usr_likeCondtnArr=array(), $usr_joinArr=array(), $singleRow=FALSE, $usr_orderByArr=array(), $usr_groupByArr=array(), $usr_whereInArray=array(), $usr_customWhereArray=array(), $backTicks=TRUE);

							$usr_data=$query['userdata'];

							if(!empty($usr_data))
							{
								foreach($usr_data AS $e_dt)
								{
									$subject="New Society Notice ";
						            $message="Dear User, <br><br><br>


									New Notice is Created <br><br>
									".$no_title."<br><br>

									Do Check in Notice tab.. <br><br><br>

									Best Regards,<br>
									CHSVishva<br><br><br>

									Note : Please do not reply back to this mail. This is sent from an unattended  <br>             
									mail box. Please mark all your queries / responses to info@chsvishva.com ";

						            $alt_message="This is the body in plain text for non-HTML mail clients";

						            $email=$e_dt['s-r-email'];

						            $mailDataArr['to']=$email;
						            $mailDataArr['subject']=$subject;
						            $mailDataArr['alt_message']=$alt_message;
						            $mailDataArr['message']=$message;

						            $this->load->library('Email_sending');

						            $response=$this->email_sending->send($mailDataArr);

						            $responseStatus=$response['status'];
						            $responseMsg=$response['message'];
								}
							}

							$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$postid);

							// $push_body=$no_title;
							// $push_title="Sociery Notice ";
							// $push_type="Notices";
							// include '../CHS_Admin_push_notify/push_notification.php';

							// //echo "push_notify($push_body,$push_title,$push_type,$soc_key)";
							// push_notify($push_body, $push_title, $push_type, $soc_key);

							$this->load->library('Push_notify');

				            $notifyArr['userType']='user app';
				            $notifyArr['userName']='';
				            $notifyArr['title']='Society Notice ';
				            $notifyArr['body']=$no_title;
				            $notifyArr['pushType']='Notices';

				            // $notifyRes=$this->push_notify->notification($notifyArr);
						}

						if(empty($result))
						{
							$response='Notice not added';
						}
						else
						{
							$response='Notice  updated';
						}
					}
				}
				else
				{
					// echo "<html><body><script>alert('Society key invalid')</script></body></html>";

					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Notice has not edited.');
				redirect(base_url()."notices/edit/".$noticeId, 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Your changes has been successfully done.');
				redirect(base_url()."notices/list", 'refresh');
			}     
		}
    }

    public function delete()
    {
    	$noticeId=$this->uri->segment('3');
    	$result="";

    	// $value = mysqli_query($conn,"UPDATE `notices` SET `active`= 'Inactive' WHERE `no_id`='$noticeId'");

    	$noc_UpdateArr=array(
			'active'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['notices.no_id']=$noticeId;

		$query=$this->Mcommon->update($tableName=$this->notices_tbl, $noc_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
              
    	// if($value)
		if($query['status']==TRUE)
    	{
    		$result="Notice removed sucessfully";
    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$noticeId);
    	}

		if(empty($result))
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Notice has not removed.');
			redirect(base_url()."notices/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Notice has removed');
			redirect(base_url()."notices/list", 'refresh');
		}
    }
}

?>