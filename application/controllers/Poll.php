<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poll extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->society_db = $this->session->userdata('_society_database');
		$this->society_key=$this->session->userdata('_society_key');
		$this->user_name=$this->session->userdata('_user_name');

		$this->logModule='POLL';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->directory_tbl=$socTables['directory_tbl'];
		$this->poll_master_tbl=$socTables['poll_master_tbl'];
		$this->poll_result_tbl=$socTables['poll_result_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Poll';
    }

	public function home()
	{
		$breadcrumbArr[0]['name']="Poll";
    	$breadcrumbArr[0]['link']=base_url()."poll/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Home";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$this->data['sess_designation']=$sess_designation=$this->session->userdata('_designation');
		$this->data['sess_designation_id']=$sess_designation_id=$this->session->userdata('_designation_id');
		$this->data['sess_user_name']=$sess_user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$sess_user_profile=$this->session->userdata('_user_profile');

		$this->Mlogger->log($logModule=$this->logModule, $logDescription='Fetch Poll home page data', $userId=$this->user_name);

		// $query =mysqli_query($conn,"SELECT pl-post-id AS pollId, DATE_FORMAT(pl-expiry-date,'%d-%m-%Y') AS pollExp, pl-shown-toAS pollName,Start-date AS pollCreated FROM poll-indicate WHERE poll_status='active' ORDER BY DATE_FORMAT(pl-expiry-date,'%Y-%m-%d') DESC");	

		$p_mstr_condtnArr['poll_master.poll_status']='active';
		$p_mstr_orderByArr["`poll_master`.`pl-expiry-date`"]='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->poll_master_tbl, $colNames="poll_master.pl-post-id AS pollId, DATE_FORMAT(`poll_master`.`pl-expiry-date`,'%d-%m-%Y') AS pollExp, poll_master.pl-shown-to AS pollName, poll_master.Start-date AS pollCreated", $p_mstr_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $p_mstr_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$p_mstr_data=$query['userdata'];

		$this->data['p_mstr_data']=$p_mstr_data;

		$poll_option_arr=array();
		$poll_res=array();

		if(!empty($p_mstr_data))
		{
			$pollIdArr=array_unique(array_column($p_mstr_data, 'pollId'));

	        $p_data_whereInArray['poll_master.pl-post-id']=$pollIdArr;

			$query=$this->Mcommon->getRecords($tableName=$this->poll_master_tbl, $colNames="poll_master.pl-post-id, poll_master.pl-option1 AS pollOpt1, poll_master.pl-option2 AS pollOpt2, poll_master.pl-option3 AS pollOpt3, poll_master.pl-option4 AS pollOpt4, poll_master.pl-option5 AS pollOpt5, poll_master.pl-option6 AS pollOpt6, poll_master.pl-option7 AS pollOpt7, poll_master.pl-option8 AS pollOpt8, poll_master.pl-option9 AS pollOpt9, poll_master.pl-option10 AS pollOpt10", $p_data_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $p_data_groupByArr=array(), $p_data_whereInArray, $customWhereArray=array(), $backTicks=TRUE);

			$p_data=$query['userdata'];

			if(!empty($p_data))
			{
				foreach($p_data AS $each_pdt)
				{
					$all_options=array_slice($each_pdt, 1);
					$poll_option_arr[$each_pdt['pl-post-id']]=$all_options;
				}

				foreach($poll_option_arr AS $ePid=>$optArr)
				{
					foreach($optArr AS $e_p_opt)
					{
						$customOrWhereArray[]='(poll_result.poll-result="'.$e_p_opt.'" AND poll_result.s-postid="'.$ePid.'") ';
					}
				}

		       	$p_res_groupByArr=array('poll_result.s-postid', 'poll_result.poll-result');

				$query=$this->Mcommon->getRecords($tableName=$this->poll_result_tbl, $colNames="poll_result.s-postid, poll_result.poll-result AS poll_option, COUNT(`poll_result`.`s-postid`) AS total_votes", $p_res_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $p_res_groupByArr, $p_res_whereInArray=array(), $p_res_customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray);

				$p_res=$query['userdata'];

				$poll_res=array();

				if(!empty($p_res))
				{
					foreach($p_res AS $e_p_res)
					{
						$poll_res[$e_p_res['s-postid']][$e_p_res['poll_option']]=$e_p_res['total_votes'];
					}
				}
			}
		}

		$this->data['poll_option_arr']=$poll_option_arr;
		$this->data['poll_res']=$poll_res;

		$this->data['view']="poll/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_poll()
	{
		$this->db->trans_start();

		$option1="";
		$option2="";
		$option3="";
		$option4="";
		$option5="";
		$option6="";
		$option7="";
		$option8="";
		$option9="";
		$option10="";

		if(!empty($this->input->post('poll1'))){
		    $option1=str_replace("'","\'",$this->input->post('poll1'));
		}
		if(!empty($this->input->post('poll2'))){
		    $option2=str_replace("'","\'",$this->input->post('poll2'));
		}
		if(!empty($this->input->post('poll3'))){
		    $option3=str_replace("'","\'",$this->input->post('poll3'));
		}
		if(!empty($this->input->post('poll4'))){
		    $option4=str_replace("'","\'",$this->input->post('poll4'));
		}
		if(!empty($this->input->post('poll5'))){
		    $option5=str_replace("'","\'",$this->input->post('poll5'));
		}
		if(!empty($this->input->post('poll6'))){
		    $option6=str_replace("'","\'",$this->input->post('poll6'));
		}
		if(!empty($this->input->post('poll7'))){
		    $option7=str_replace("'","\'",$this->input->post('poll7'));
		}
		if(!empty($this->input->post('poll8'))){
		    $option8=str_replace("'","\'",$this->input->post('poll8'));
		}
		if(!empty($this->input->post('poll9'))){
		    $option9=str_replace("'","\'",$this->input->post('poll9'));
		}
		 if(!empty($this->input->post('poll10'))){
		     $option10=str_replace("'","\'",$this->input->post('poll10'));
		}
		$topic=str_replace("'","\'",$this->input->post('poll_header'));
		$expdate=date("Y-m-d",strtotime($this->input->post('expr')));
		 
		if(!empty($this->society_key) && !empty($this->user_name) && !empty($topic) && !empty($expdate) && !empty($option1) && !empty($option2))
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

				$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
				{
					// $result=Add_Society_polls($userName, $soc_key, $expdate, $topic, $option1, $option2, $option3, $option4, $option5, $option6, $option7, $option8, $option9, $option10);

					// function Add_Society_polls($username,$soc_key,$expdate, $poll_header, $poll_option1, $poll_option2, $poll_option3, $poll_option4, $poll_option5, $poll_option6, $poll_option7, $poll_option8, $poll_option9, $poll_option10)


					$poll_header=str_replace("'","\'",$topic);

					$poll_option1=str_replace("'","\'",$option1);
					$poll_option2=str_replace("'","\'",$option2);
					$poll_option3=str_replace("'","\'",$option3);
					$poll_option4=str_replace("'","\'",$option4);
					$poll_option5=str_replace("'","\'",$option5);
					$poll_option6=str_replace("'","\'",$option6);
					$poll_option7=str_replace("'","\'",$option7);
					$poll_option8=str_replace("'","\'",$option8);
					$poll_option9=str_replace("'","\'",$option9);
					$poll_option10=str_replace("'","\'",$option10);

					$random = str_shuffle('abcdefghijklmnopqrstuvwxyz123456789');
					$onetime = substr($random,0,4);
					$postid = "polls-".$onetime;
					$today_date = date('Y-m-d');

					$pollInsertArr[]=array(
						'pl-post-id'=>$postid,
						'pl-expiry-date'=>$expdate,
						'pl-shown-to'=>$topic,
						'pl-option1'=>$poll_option1,
						'pl-option2'=>$poll_option2,
						'pl-option3'=>$poll_option3,
						'pl-option4'=>$poll_option4,
						'pl-option5'=>$poll_option5,
						'pl-option6'=>$poll_option6,
						'pl-option7'=>$poll_option7,
						'pl-option8'=>$poll_option8,
						'pl-option9'=>$poll_option9,
						'pl-option10'=>$poll_option10,
						'Start-date'=>$today_date,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->poll_master_tbl, $pollInsertArr, $returnType="");

					if($query['status']==TRUE)
					{
						$result='Society poll has been created';

						$pollUpdateArr=array(
							'polls-no'=>'`polls-no`+1',
							'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
						);

						$query=$this->Mcommon->update($tableName=$this->s_r_user_tbl, $pollUpdateArr, $updt_condtnArr=array(), $likeCondtnArr=array());

						$subject="New Society Poll ";
			            $message="Dear User, <br><br><br>


	                    New Poll is Created Do Check in Poll tab..<br><br> 

	                    Best Regards,<br>
	                    CHSVishva<br><br><br>

	                    Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

			            $alt_message="This is the body in plain text for non-HTML mail clients";

			            $email="dynamicvishvateam@gmail.com";

			            $mailDataArr['to']=$email;
			            $mailDataArr['subject']=$subject;
			            $mailDataArr['alt_message']=$alt_message;
			            $mailDataArr['message']=$message;

			            $this->load->library('Email_sending');

			            $response=$this->email_sending->send($mailDataArr);

			            $responseStatus=$response['status'];
			            $responseMsg=$response['message'];

			            $this->load->library('Push_notify');

			            $notifyArr['userType']='user app';
			            $notifyArr['userName']='';
			            $notifyArr['title']='Society Poll ';
			            $notifyArr['body']=$topic;
			            $notifyArr['pushType']='Poll';

			            $notifyRes=$this->push_notify->notification($notifyArr);
					}
					else
					{
						$result='';
					}

					if(empty($result))
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! Poll not added.');
					}
					else
					{
						$this->session->set_flashdata('success_msg', 'Poll has been added successfully');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
		    	redirect(base_url()."auth/logout", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."poll/home", 'refresh');
		}
		else
		{
			redirect(base_url()."poll/home", 'refresh');
		}     
	}

	public function delete()
	{
		$this->db->trans_start();

		$pollsId=$this->uri->segment('3');

		// $value = mysqli_query($conn,"UPDATE `poll-indicate` SET `poll_status`= 'Inactive' WHERE `pl-post-id`='$pollId'");

		$pollUpdateArr=array(
			'poll_status'=>'Inactive',
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$pollCondtnArr['poll_master.pl-post-id']=$pollsId;

		$query=$this->Mcommon->update($tableName=$this->poll_master_tbl, $pollUpdateArr, $pollCondtnArr, $likeCondtnArr=array());

		if($query['status']==TRUE)
		{
			$result="Removed";
			$this->Mlogger->log($logModule=$this->logModule, $logDescription='Polls removed sucessfully', $userId=$this->user_name);
		}

		if(empty($result))
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Poll not deleted.');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Poll has been deleted successfully');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."poll/home", 'refresh');
		}
		else
		{
			redirect(base_url()."poll/home", 'refresh');
		}     
	}
}
