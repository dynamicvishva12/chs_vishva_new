<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reg_docs extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->society_db = $this->session->userdata('_society_database');
		$this->society_key=$this->session->userdata('_society_key');
		$this->user_name=$this->session->userdata('_user_name');

		$this->logModule='REGISTERS_AND_DOCUMENTS';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->share_register_tbl=$socTables['share_register_tbl'];
		$this->transfer_share_tbl=$socTables['transfer_share_tbl'];
		$this->class_member_register_tbl=$socTables['class_member_register_tbl'];
		$this->nominee_register_tbl=$socTables['nominee_register_tbl'];
		$this->audit_tbl=$socTables['audit_tbl'];
		$this->document_tbl=$socTables['document_tbl'];
		$this->j_form_tbl=$socTables['j_form_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Registers & Documents';
    }

	public function home()
	{
		$this->data['view']="reg_docs/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function jform()
	{
		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="J Form";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/jform";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$user_orderByArr['`s-r-user`.`s-r-username`']='ASC';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="`s-r-user`.`s-r-fname` AS usrFname, `s-r-user`.`s-r-lname` AS usrLname , `s-r-user`.`s-r-username`", $user_condtnArr=array(), $user_likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr, $user_groupByArr=array(), $whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $user_orNotLikeArray=array(), $limit="");

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		// $fetch=mysqli_query($conn,"SELECT * FROM `class_member_register` LEFT join `s-r-user` ON `class_member_register`.`mem_id` = `s-r-user`.`s-r-username` WHERE `active`='active'");

		$jform_condtnArr['j_form.status']='active';

		$jform_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`j_form`.`mem_id` = `s-r-user`.`s-r-username`", "type"=>"left");

		// $query=$this->Mcommon->getRecords($tableName=$this->j_form_tbl, $colNames="mem_id, member_type, na_mem, relation, dt_db, email, landline, contact, class_doc, s-r-fname, s-r-lname", $jform_condtnArr, $likeCondtnArr=array(), $jform_joinArr, $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$query=$this->Mcommon->getRecords($tableName=$this->j_form_tbl, $colNames="sr_id, mem_id, member_type, s-r-fname, s-r-lname, j_member_name_1, j_member_address_1, j_member_name_2, j_member_address_2, j_member_name_3, j_member_address_3, j_member_name_4, j_member_address_4, j_member_name_5, j_member_address_5, j_member_name_6, j_member_address_6, j_member_name_7, j_member_address_7, j_member_name_8, j_member_address_8, j_member_name_9, j_member_address_9, j_member_name_10, j_member_address_10", $jform_condtnArr, $likeCondtnArr=array(), $jform_joinArr, $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$jform_arr=$query['userdata'];

		$this->data['jform_arr']=$jform_arr;

		$this->data['view']="reg_docs/jform";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_jform()
	{
		$this->db->trans_start();

		$member=$this->input->post('member');
		$classname=$this->input->post('classname');
		
		$mem_InsertArr[0]=array(
			'mem_id' => $member,
			'member_type' => $classname,
			'createdBy' => $this->user_name,
			'createdDatetime' => $this->curr_datetime
		);

		$j_member_name_arr=$this->input->post('j_member_name');
		$j_member_address_arr=$this->input->post('j_member_address');

		if(!empty($j_member_name_arr))
		{
			foreach($j_member_name_arr AS $i_mbr=>$e_mbr_name)
			{
				$mbr_index=$i_mbr+1;
				$mem_InsertArr[0]['j_member_name_'.$mbr_index]=$e_mbr_name;
				$mem_InsertArr[0]['j_member_address_'.$mbr_index]=$j_member_address_arr[$i_mbr];
			}
		}
		
		// $fire=mysqli_query($conn,$insertquery);

		$query=$this->Mcommon->insert($tableName=$this->j_form_tbl, $mem_InsertArr, $returnType="");

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Member not added.');
			redirect(base_url()."reg_docs/jform", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Member has been added successfully');
			redirect(base_url()."reg_docs/jform", 'refresh');
		}
	}

	public function edit_jform()
	{
		$this->data['id']=$id=$this->uri->segment('3');

		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="J Form";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/jform";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit J Form";
    	$breadcrumbArr[2]['link']=base_url()."reg_docs/edit_jform/".$id;
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$user_orderByArr['`s-r-user`.`s-r-username`']='ASC';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="`s-r-user`.`s-r-fname` AS usrFname, `s-r-user`.`s-r-lname` AS usrLname , `s-r-user`.`s-r-username`", $user_condtnArr=array(), $user_likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr, $user_groupByArr=array(), $whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $user_orNotLikeArray=array(), $limit="");

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		// $fetch=mysqli_query($conn,"SELECT * FROM `class_member_register` LEFT join `s-r-user` ON `class_member_register`.`mem_id` = `s-r-user`.`s-r-username` WHERE `active`='active'");

		$jform_condtnArr['j_form.status']='active';
		$jform_condtnArr['j_form.sr_id']=$id;

		$jform_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`j_form`.`mem_id` = `s-r-user`.`s-r-username`", "type"=>"left");

		// $query=$this->Mcommon->getRecords($tableName=$this->j_form_tbl, $colNames="mem_id, member_type, na_mem, relation, dt_db, email, landline, contact, class_doc, s-r-fname, s-r-lname", $jform_condtnArr, $likeCondtnArr=array(), $jform_joinArr, $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$query=$this->Mcommon->getRecords($tableName=$this->j_form_tbl, $colNames="sr_id, mem_id, member_type, s-r-fname, s-r-lname, j_member_name_1, j_member_address_1, j_member_name_2, j_member_address_2, j_member_name_3, j_member_address_3, j_member_name_4, j_member_address_4, j_member_name_5, j_member_address_5, j_member_name_6, j_member_address_6, j_member_name_7, j_member_address_7, j_member_name_8, j_member_address_8, j_member_name_9, j_member_address_9, j_member_name_10, j_member_address_10", $jform_condtnArr, $likeCondtnArr=array(), $jform_joinArr, $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$jform_arr=$query['userdata'];

		$this->data['jform_arr']=$jform_arr;

		$this->form_validation->set_rules('member', 'Member', 'trim');
		$this->form_validation->set_rules('classname', 'Type', 'trim');

		if($this->form_validation->run() == FALSE)
		{
			$this->data['view']="reg_docs/edit_jform";
			$this->load->view('layouts/layout/main_layout', $this->data);
		}
		else
		{
			$this->db->trans_start();

			$member_id=$this->input->post('member_id');

			for($j=1; $j<11; $j++)
			{
				$remove_mbrs_arr['j_member_name_'.$j]="";
				$remove_mbrs_arr['j_member_address_'.$j]="";
			}

			$remove_mbrs_condtnArr['j_form.sr_id']=$member_id;

			$query=$this->Mcommon->update($tableName=$this->j_form_tbl, $remove_mbrs_arr, $remove_mbrs_condtnArr, $likeCondtnArr=array());

			$member=$this->input->post('member');
			$classname=$this->input->post('classname');
			
			$updateArr=array(
				'mem_id' => $member,
				'member_type' => $classname,
				'createdBy' => $this->user_name,
				'createdDatetime' => $this->curr_datetime
			);

			$j_member_name_arr=$this->input->post('j_member_name');
			$j_member_address_arr=$this->input->post('j_member_address');

			if(!empty($j_member_name_arr))
			{
				foreach($j_member_name_arr AS $i_mbr=>$e_mbr_name)
				{
					$mbr_index=$i_mbr+1;
					$updateArr['j_member_name_'.$mbr_index]=$e_mbr_name;
					$updateArr['j_member_address_'.$mbr_index]=$j_member_address_arr[$i_mbr];
				}
			}

			$updt_condtnArr['j_form.sr_id']=$member_id;

			$query=$this->Mcommon->update($tableName=$this->j_form_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Member not updated.');
				redirect(base_url()."reg_docs/jform", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Member has been updated successfully');
				redirect(base_url()."reg_docs/jform", 'refresh');
			}
		}
	}

	public function del_jform()
	{
		$this->db->trans_start();

		$id=$this->uri->segment('3');

		$updateArr=array(
			'status' => "Inactive",
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['j_form.sr_id']=$id;

		$query=$this->Mcommon->update($tableName=$this->j_form_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Member not removed.');
			redirect(base_url()."reg_docs/jform", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Member has been removed successfully');
			redirect(base_url()."reg_docs/jform", 'refresh');
		}
	}

	public function audit_formo()
	{
		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Audit Reports & Form O";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/audit_formo";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
		// $fetch=mysqli_query($conn,"SELECT * FROM `class_member_register` LEFT join `s-r-user` ON `class_member_register`.`mem_id` = `s-r-user`.`s-r-username` WHERE `active`='active'");

		// $query = mysqli_query($conn,"SELECT `d_id`, `user_name`, `doc_name`, CONCAT('http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/IMAGES/society_doc/',`audit`) AS `audit` FROM `audit` WHERE `user_name`='admin' ORDER BY  `date_creation` DESC");

		$adt_condtnArr['audit.user_name']='admin';
		$adt_orderByArr['audit.createdDatetime']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->audit_tbl, $colNames="`d_id`, `user_name`, `doc_name`, CONCAT('".$this->assets_url."IMAGESDRIVE/society_doc/',`audit`) AS `audit`", $adt_condtnArr, $adt_likeCondtnArr=array(), $adt_joinArr=array(), $singleRow=FALSE, $adt_orderByArr, $adt_groupByArr=array(), $adt_whereInArray=array(), $adt_customWhereArray=array(), $backTicks=TRUE);

		$auditData=$query['userdata'];

		$this->data['auditData']=$auditData;

		$formo_condtnArr['document.doc_name']='O form';
		$formo_condtnArr['document.user_name']='admin';

		$query=$this->Mcommon->getRecords($tableName=$this->document_tbl, $colNames="`user_name`, `doc_name`, CONCAT('".$this->assets_url."IMAGESDRIVE/',`document`) AS `document`", $formo_condtnArr, $formo_likeCondtnArr=array(), $formo_joinArr=array(), $singleRow=FALSE, $formo_orderByArr=array(), $formo_groupByArr=array(), $formo_whereInArray=array(), $formo_customWhereArray=array(), $backTicks=TRUE);

		$formoData=$query['userdata'];

		$this->data['formoData']=$formoData;

		$this->data['view']="reg_docs/audit_formo";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function upload_audit()
	{
		$this->db->trans_start();

		$post_title = $this->input->post('docname');
		$post_image= $_FILES['myfile']['name'];
		$t_tmp=$_FILES['myfile']['tmp_name'];
		$temp = explode(".", $_FILES["myfile"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
		$store=FCPATH."assets/IMAGESDRIVE/society_doc/".$newfilename;

		if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
		{
			move_uploaded_file($t_tmp, $store);

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($post_title) && !empty($newfilename))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }

					// $result=add_audit($userName,$soc_key,$post_title,$newfilename);
					$auditName=$post_title;
					$auditPic=$newfilename;

					$adt_InsertArr[]=array(
						'user_name' => 'admin',
						'doc_name' => $auditName,
						'audit' => $auditPic,
						'createdBy' => $this->user_name,
						'createdDatetime' => $this->curr_datetime
					);
					// $fire=mysqli_query($conn,$insertquery);

					$query=$this->Mcommon->insert($tableName=$this->audit_tbl, $adt_InsertArr, $returnType="");

					$adt_InsertStatus=$query['status'];

					if($adt_InsertStatus==FALSE)
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! Audit Report not uploaded');
					}
					else
					{
						$this->session->set_flashdata('success_msg', 'Audit Report has been uploaded successfully');
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}
		} 
		else
		{
			$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."reg_docs/audit_formo", 'refresh');
		}
		else
		{
			redirect(base_url()."reg_docs/audit_formo", 'refresh');
		}   
	}	

	public function upload_formo()
	{
		$this->db->trans_start();

		$post_title = $this->input->post('docname');
		$post_image= $_FILES['myfile']['name'];
		$t_tmp=$_FILES['myfile']['tmp_name'];
		$temp = explode(".", $_FILES["myfile"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
		$store=FCPATH."assets/IMAGESDRIVE/".$newfilename;

		if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
		{
			move_uploaded_file($t_tmp, $store);

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($post_title) && !empty($newfilename))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }

					// $result=add_audit($userName,$soc_key,$post_title,$newfilename);

					$doc_InsertArr[]=array(
						'user_name' => 'admin',
						'doc_name' => 'O form',
						'document' => $newfilename,
						'createdBy' => $this->user_name,
						'createdDatetime' => $this->curr_datetime
					);
					// $fire=mysqli_query($conn,$insertquery);

					$query=$this->Mcommon->insert($tableName=$this->document_tbl, $doc_InsertArr, $returnType="");

					$doc_InsertStatus=$query['status'];

					if($doc_InsertStatus==FALSE)
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! Form O document not uploaded');
					}
					else
					{
						$this->session->set_flashdata('success_msg', 'Form O document has been uploaded successfully');
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			}
		} 
		else
		{
			$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."reg_docs/audit_formo", 'refresh');
		}
		else
		{
			redirect(base_url()."reg_docs/audit_formo", 'refresh');
		}   
	}

	public function share_register()
	{
		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Share Register";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/share_register";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, s-r-user.s-r-password, s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-profile, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.status", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userData=$query['userdata'];

		$this->data['userData']=$userData;$shReg_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"s-r-user.s-r-username = share_register_tbl.memberId", "type"=>"left");
		$shReg_joinArr[]=array("tbl"=>$this->transfer_share_tbl, "condtn"=>"transfer_share_tbl.share_reg_id = share_register_tbl.shareRegisterId AND transfer_share_tbl.status='Active'", "type"=>"left");

		$shReg_condtnArr['share_register_tbl.status']='Active';
		$shReg_groupByArr=array("share_register_tbl.shareRegisterId");

		$query=$this->Mcommon->getRecords($tableName=$this->share_register_tbl, $colNames="share_register_tbl.shareRegisterId, share_register_tbl.dateOfAllotedShares, share_register_tbl.cashbookFolio, share_register_tbl.shareCertNo, share_register_tbl.noOfShares, share_register_tbl.valueOfShares, share_register_tbl.memberId, s-r-user.s-r-fname, s-r-user.s-r-lname, transfer_share_tbl.transfer_date, transfer_share_tbl.cashbook_jr_folio, transfer_share_tbl.share_cert_no, transfer_share_tbl.share_value, transfer_share_tbl.transferee, transfer_share_tbl.authority_sign, transfer_share_tbl.remarks, COUNT(transfer_share_tbl.share_reg_id) AS count_share_reg", $shReg_condtnArr, $shReg_likeCondtnArr=array(), $shReg_joinArr, $singleRow=FALSE, $shReg_orderByArr=array(), $shReg_groupByArr, $shReg_whereInArray=array(), $shReg_customWhereArray=array(), $backTicks=TRUE);

		$shRegData=$query['userdata'];

		$this->data['shRegData']=$shRegData;

		$this->data['view']="reg_docs/share_register";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_share_register()
	{
		$dateOfAllotedShares=date("Y-m-d", strtotime($this->input->post('dateofallot')));
		$cashbookFolio=$this->input->post('CashBook');
		$shareCertNo=$this->input->post('certno');
		$noOfShares=$this->input->post('shareno');
		$valueOfShares=$this->input->post('sharevalue');
		$memberId=$this->input->post('WhomShare');

		$insertArr[]=array(
			'dateOfAllotedShares' => $dateOfAllotedShares,
			'cashbookFolio' => $cashbookFolio,
			'shareCertNo' => $shareCertNo,
			'noOfShares' => $noOfShares,
			'valueOfShares' => $valueOfShares,
			'memberId' => $memberId,
			'status' => "Active",
			'createdBy' => $this->user_name,
			'createdDatetime' => $this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->share_register_tbl, $insertArr, $returnType="");

		$insertStatus=$query['status'];

		if($insertStatus==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Share Register has not added.');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Share Register has been added successfully');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
	}

	public function edit_share_register()
	{
		$shareRegisterId=$this->input->post('shareRegisterId');
		$dateOfAllotedShares=date("Y-m-d", strtotime($this->input->post('edit_dateofallot')));
		$cashbookFolio=$this->input->post('edit_CashBook');
		$shareCertNo=$this->input->post('edit_certno');
		$noOfShares=$this->input->post('edit_shareno');
		$valueOfShares=$this->input->post('edit_sharevalue');
		$memberId=$this->input->post('edit_WhomShare');

		$updateArr=array(
			'dateOfAllotedShares' => $dateOfAllotedShares,
			'cashbookFolio' => $cashbookFolio,
			'shareCertNo' => $shareCertNo,
			'noOfShares' => $noOfShares,
			'valueOfShares' => $valueOfShares,
			'memberId' => $memberId,
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['share_register_tbl.shareRegisterId']=$shareRegisterId;

		$query=$this->Mcommon->update($tableName=$this->share_register_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$updtStatus=$query['status'];

		if($updtStatus==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Share Register has not updated.');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Share Register has been updated successfully');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
	}

	public function del_share_register()
	{
		$this->db->trans_start();

		$shareRegisterId=$this->uri->segment('3');

		$transUpdateArr=array(
			'status' => "Inactive",
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$trans_updt_condtnArr['transfer_share_tbl.share_reg_id']=$shareRegisterId;

		$query=$this->Mcommon->update($tableName=$this->transfer_share_tbl, $transUpdateArr, $trans_updt_condtnArr, $likeCondtnArr=array());

		$updateArr=array(
			'status' => "Inactive",
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['share_register_tbl.shareRegisterId']=$shareRegisterId;

		$query=$this->Mcommon->update($tableName=$this->share_register_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Share Register has not removed.');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Share Register has been removed successfully');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
	}

	public function trans_share_register()
	{
		$this->db->trans_start();

		$shareRegisterId=$this->input->post('transShareRegisterId');

		$updateArr=array(
			'status' => "Inactive",
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['transfer_share_tbl.share_reg_id']=$shareRegisterId;

		$query=$this->Mcommon->update($tableName=$this->transfer_share_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$transfer_date=date("Y-m-d", strtotime($this->input->post('transfer_date')));
		$cashbook_folio=$this->input->post('cashbook_folio');
		$share_cert_no=$this->input->post('share_cert_no');
		$share_value=$this->input->post('share_value');
		$transferee=$this->input->post('transferee');
		$authority_sign=$this->input->post('authority_sign');
		$remarks=$this->input->post('remarks');

		$insertArr[]=array(
			'share_reg_id' => $shareRegisterId,
			'transfer_date' => $transfer_date,
			'cashbook_jr_folio' => $cashbook_folio,
			'share_cert_no' => $share_cert_no,
			'share_value' => $share_value,
			'transferee' => $transferee,
			'authority_sign' => $authority_sign,
			'remarks' => $remarks,
			'status' => "Active",
			'createdBy' => $this->user_name,
			'createdDatetime' => $this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->transfer_share_tbl, $insertArr, $returnType="");

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Share has not transferred.');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Share has been transferred successfully');
			redirect(base_url()."reg_docs/share_register", 'refresh');
		}     
	}

	public function nomi_register()
	{
		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Nomination Register";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/nomi_register";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, s-r-user.s-r-password, s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-profile, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.status", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userData=$query['userdata'];

		$this->data['userData']=$userData;

		$nmReg_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"s-r-user.s-r-username = nominee_register_tbl.nomi_member_id", "type"=>"left");

		$nmReg_condtnArr['nominee_register_tbl.status']='Active';
		$nmReg_groupByArr=array("nominee_register_tbl.nominee_register_id");

		$query=$this->Mcommon->getRecords($tableName=$this->nominee_register_tbl, $colNames="nominee_register_tbl.nominee_register_id, nominee_register_tbl.nomi_member_id, nominee_register_tbl.nomi_name, nominee_register_tbl.mang_date, nominee_register_tbl.nomi_date, nominee_register_tbl.subs_revocation_date, nominee_register_tbl.remarks, s-r-user.s-r-username, s-r-user.s-r-fname, s-r-user.s-r-lname", $nmReg_condtnArr, $nmReg_likeCondtnArr=array(), $nmReg_joinArr, $singleRow=FALSE, $nmReg_orderByArr=array(), $nmReg_groupByArr, $nmReg_whereInArray=array(), $nmReg_customWhereArray=array(), $backTicks=TRUE);

		$nmRegData=$query['userdata'];

		$this->data['nmRegData']=$nmRegData;

		$this->data['view']="reg_docs/nomi_register";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_nomi_register()
	{
		$nomi_member_id=$this->input->post('nomi_member_id');
		$nomi_name=$this->input->post('nomi_name');
		$mang_date=date("Y-m-d", strtotime($this->input->post('mang_date')));
		$nomi_date=date("Y-m-d", strtotime($this->input->post('nomi_date')));
		$subs_revocation_date=date("Y-m-d", strtotime($this->input->post('subs_revocation_date')));
		$remarks=$this->input->post('remarks');

		$insertArr[]=array(
			'nomi_member_id' => $nomi_member_id,
			'nomi_name' => $nomi_name,
			'mang_date' => $mang_date,
			'nomi_date' => $nomi_date,
			'subs_revocation_date' => $subs_revocation_date,
			'remarks' => $remarks,
			'status' => "Active",
			'createdBy' => $this->user_name,
			'createdDatetime' => $this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->nominee_register_tbl, $insertArr, $returnType="");

		$insertStatus=$query['status'];

		if($insertStatus==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Nominee Register has not added.');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Nominee Register has been added successfully');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
	}

	public function edit_nomi_register()
	{
		$nomiRegisterId=$this->input->post('nomiRegisterId');
		$nomi_member_id=$this->input->post('edit_nomi_member_id');
		$nomi_name=$this->input->post('edit_nomi_name');
		$mang_date=date("Y-m-d", strtotime($this->input->post('edit_mang_date')));
		$nomi_date=date("Y-m-d", strtotime($this->input->post('edit_nomi_date')));
		$subs_revocation_date=date("Y-m-d", strtotime($this->input->post('edit_subs_revocation_date')));
		$remarks=$this->input->post('edit_remarks');

		$updateArr=array(
			'nomi_member_id' => $nomi_member_id,
			'nomi_name' => $nomi_name,
			'mang_date' => $mang_date,
			'nomi_date' => $nomi_date,
			'subs_revocation_date' => $subs_revocation_date,
			'remarks' => $remarks,
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['nominee_register_tbl.nominee_register_id']=$nomiRegisterId;

		$query=$this->Mcommon->update($tableName=$this->nominee_register_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$updtStatus=$query['status'];

		if($updtStatus==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Nominee Register has not updated.');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Nominee Register has been updated successfully');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
	}

	public function del_nomi_register()
	{
		$this->db->trans_start();

		$nomiRegisterId=$this->uri->segment('3');

		$updateArr=array(
			'status' => "Inactive",
			'updatedBy' => $this->user_name,
			'updatedDatetime' => $this->curr_datetime
		);

		$updt_condtnArr['nominee_register_tbl.nominee_register_id']=$nomiRegisterId;

		$query=$this->Mcommon->update($tableName=$this->nominee_register_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Nominee Register has not removed.');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Nominee Register has been removed successfully');
			redirect(base_url()."reg_docs/nomi_register", 'refresh');
		}
	}

	public function other_docs()
	{
		$breadcrumbArr[0]['name']="Registers & Documents";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Other Documents";
    	$breadcrumbArr[1]['link']=base_url()."reg_docs/other_docs";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$condtnArr['document.user_name']="admin";

		$query=$this->Mcommon->getRecords($tableName=$this->document_tbl, $colNames="document.doc_name, document.document", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$docsArr=$query['userdata'];

		$this->data['docsArr']=$docsArr;

		$this->data['view']="reg_docs/other_docs";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function upload_other_docs()
	{
		$user="admin";
		$post_title = $this->input->post('docname');
		$post_image= $_FILES['myfile']['name'];
		$t_tmp=$_FILES['myfile']['tmp_name'];
		$fileexe = strtolower(pathinfo($post_image, PATHINFO_EXTENSION));
		$store=FCPATH."assets/IMAGESDRIVE/".$post_image;

		if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
		{
			if(move_uploaded_file($t_tmp, $store))
			{
				// $fire=mysqli_query($conn,"INSERT INTO `document` (`user_name`, `doc_name`, `document`) VALUES ('$user', '$post_title', '$store') ")or die("error...".mysqli_error($conn));

				$insertArr[]=array(
					'user_name' => $user,
					'doc_name' => $post_title,
					'document' => $post_image,
					'status' => "Active",
					'createdBy' => $this->user_name,
					'createdDatetime' => $this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->document_tbl, $insertArr, $returnType="");

				$insertStatus=$query['status'];

				if($insertStatus==TRUE)
				{
					$this->session->set_flashdata('success_msg', 'Document has been uploaded successfully');
					redirect(base_url()."reg_docs/other_docs", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Document has not uploaded.');
				redirect(base_url()."reg_docs/other_docs", 'refresh');
			}

			// if($fire) 
			// {
			// 	DALERT('sucessss...........');
			// 	echo "<script>window.open('society_document.php','_self')</script>";
			// 	// mysqli_close($conn);
			// }
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
			redirect(base_url()."reg_docs/other_docs", 'refresh');
		}
	}
}
