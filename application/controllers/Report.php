<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
        $this->load->model('Mfunctional', '', TRUE);
        $this->load->model('Mlogger', '', TRUE);

        $this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
        $this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

        $this->data['css']='layouts/include/css';
        $this->data['side_nav']='layouts/include/side_nav';     
        $this->data['navbar_menu']='layouts/include/navbar_menu';   
        $this->data['footer']='layouts/include/footer';
        $this->data['js']='layouts/include/js';

        $this->data['society_userdata']=society_userdata();

        $this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
        $this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
        $this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
        $this->data['sess_user_profile']=$this->session->userdata('_user_profile');

        $this->logModule='REPORT';

        $socTables=$this->society_lib->socTables();

        $this->s_r_user_tbl=$socTables['s_r_user_tbl'];
        $this->staff_tbl=$socTables['staff_tbl'];
        $this->family_tbl=$socTables['family_tbl'];
        $this->accounting_groups_tbl=$socTables['accounting_groups_tbl'];
        $this->accounting_types_tbl=$socTables['accounting_types_tbl'];
        $this->accounting_opening_balance_tbl=$socTables['accounting_opening_balance_tbl'];
        $this->accounting_tbl=$socTables['accounting_tbl'];
        $this->pnl_acc_tbl=$socTables['pnl_acc_tbl'];
        $this->society_master_tbl=$socTables['society_master_tbl'];
        $this->penalty_tbl=$socTables['penalty_tbl'];
        $this->member_maintenance_tbl=$socTables['penalty_tbl'];
        $this->maintenance_head_tbl=$socTables['maintenance_head_tbl'];
        $this->maintenance_tbl=$socTables['maintenance_tbl'];

        $this->curr_datetime=date('Y-m-d H:i:s');

        $this->data['page_section']='Reports';
    }

    public function blood_group()
    {
        $breadcrumbArr[0]['name']="Reports";
        $breadcrumbArr[0]['link']=base_url()."report/blood_group";
        $breadcrumbArr[0]['active']=FALSE;

        $breadcrumbArr[1]['name']="Blood Groups";
        $breadcrumbArr[1]['link']="javascript:void(0)";
        $breadcrumbArr[1]['active']=TRUE;

        $this->data['breadcrumbArr']=$breadcrumbArr;

        $sel_blood_grp="";

        if($this->input->post('blood')!="")
            $sel_blood_grp=$this->input->post('blood');

        $this->data['sel_blood_grp']=$sel_blood_grp;

        $result=array();

        if($this->input->post('blood'))
        {
            $bgid=$this->input->post('blood');

            if($bgid=='show') 
            {
                $query=$this->Mcommon->plainQuery($sql_query="SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact` FROM ".$this->staff_tbl." UNION SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact` FROM ".$this->family_tbl."");

                $result=$query['userdata'];

                // $query ="SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact` FROM `staff`
                // UNION
                // SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact`
                // FROM `family`";
            }
            else
            {
                $query=$this->Mcommon->plainQuery($sql_query="SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact` FROM ".$this->staff_tbl." WHERE `blood_type`='".$bgid."' UNION SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact`  FROM ".$this->family_tbl." WHERE `blood_type`='".$bgid."'");

                $result=$query['userdata'];

                // $query ="SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact`
                // FROM `staff` WHERE `blood_type`='$bgid'
                // UNION
                // SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact`
                // FROM `family` WHERE `blood_type`='$bgid'";
            }

            // $fire =mysqli_query($conn,$query);
        }                
        else 
        {
            $query=$this->Mcommon->plainQuery($sql_query="SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact` FROM ".$this->staff_tbl." UNION SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact` FROM ".$this->family_tbl."");

            $result=$query['userdata'];

            // $query = "SELECT `blood_type` As Type,`user_name`,`wish_to_donate`, `name`, `blood_type`, `contact` FROM `staff`
            // UNION
            // SELECT `blood_type`,`user_name`, `name`,`wish_to_donate`, `blood_type`, `contact`
            // FROM `family`";
            // $fire = mysqli_query($conn, $query) or die("error..." . mysqli_error($conn));
        }

        $this->data['result']=$result;

        $this->data['view']="report/blood_group";
        $this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function trail_balance()
    {
        $breadcrumbArr[0]['name']="Reports";
        $breadcrumbArr[0]['link']=base_url()."report/trail_balance";
        $breadcrumbArr[0]['active']=FALSE;

        $breadcrumbArr[1]['name']="Trail Balance";
        $breadcrumbArr[1]['link']="javascript:void(0)";
        $breadcrumbArr[1]['active']=TRUE;

        $this->data['breadcrumbArr']=$breadcrumbArr;

        $currmon=date("m");

        if($currmon>=4) 
        {
            $curryear= date("Y")."-04-01";
            $nxt_yr=(date("Y")+1)."-03-31";
        }
        else
        {
            $curryear= (date("Y")-1)."-04-01";
            $nxt_yr=date("Y")."-03-31";
        }

        $m_date1= date("Y-m-01");
        $m_date2=date("Y-m-t");

        if($this->input->post('financial'))
        {
            $financial=$this->input->post('financial');
            $years_fn=explode("_", $financial);
            $curryear= $years_fn[0];
            $nxt_yr=$years_fn[1];
        }

        $this->data['curryear']=$curryear;
        $this->data['nxt_yr']=$nxt_yr;

        $from_date=$curryear;
        $todate=$nxt_yr;

        // $account_assets_group_sql=mysqli_query($conn,"SELECT `id`, `name`, `account_type`, `extra` FROM `accounting_groups`");

        $query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`, `extra`", $acc_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $account_assets_group_sql=$query['userdata'];

        if(!empty($account_assets_group_sql))
        {
            // while($account_assets_group_data=mysqli_fetch_assoc($account_assets_group_sql))
            foreach($account_assets_group_sql AS $account_assets_group_data)
            {
                $group_arr=array();
                $result_head=array();
                $gdrsum=$gcrsum=0;

                $account_assets_group_Id=$account_assets_group_data['id'];

                $account_assets_group_Name=$account_assets_group_data['name'];

                $account_assets_group_cr_dr=$account_assets_group_data['extra'];

                // $account_head=mysqli_query($conn,"SELECT (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS type_ledger,`accounting_name`, `accounting_id` FROM `accounting_types` WHERE `Account_group`='$account_assets_group_Id'");

                $accHd_condtnArr['`accounting_types`.`Account_group`']=$account_assets_group_Id;

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="(SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS type_ledger, `accounting_name`, `accounting_id`", $accHd_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $account_head=$query['userdata'];

                // if(mysqli_num_rows($account_head)>0)
                if(!empty($account_head))
                {
                    // while($head_data=mysqli_fetch_assoc($account_head))
                    foreach($account_head AS $head_data)
                    {
                        // print_r($head_data);
                        $drsum=$crsum=0;
                        $trailData=array();
                        $headId=$head_data['accounting_id'];
                        $headName=$head_data['accounting_name'];

                        // $trail_sql=mysqli_query($conn,"SELECT `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode` FROM `accounting` WHERE `accouting_id`='fdgfdgf' AND `s-date` BETWEEN '$from_date' AND '$todate'");

                        $trail_condtnArr['`accounting`.`accouting_id`']='fdgfdgf';
                        $trail_customWhereArray[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";

                        $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames=" `a_id`, `accouting_id`, `accounting_charges`, `s-date`, `accounting_recipt`, `status`, `s-indent`, `s-des`, `member_id`, `bank_name`, `ifsc`, `micr`, `transaction_no`, `vaucher_name`, `vaucher_no`, `cheque_no`, `cr_dr`, `payment_mode`", $trail_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $trail_customWhereArray, $backTicks=TRUE);

                        $trail_sql=$query['userdata'];

                        $trail_sql1[]=$query['userdata'];

                        // if(mysqli_num_rows($trail_sql)>0)
                        if(!empty($trail_sql))
                        {
                            // while($trailData=mysqli_fetch_assoc($trail_sql))
                            foreach($trail_sql AS $trailData)
                            {
                                if($trailData['cr_dr']=="Dr")
                                {
                                    $drsum+= $trailData['accounting_charges'];
                                }
                                else
                                {
                                    $crsum+= $trailData['accounting_charges'];
                                }
                            }
                        }

                        if($head_data['type_ledger']=="Dr")
                        {
                            $drsum+= $head_data['opening_balance'];
                        }
                        else
                        {
                            $crsum+= $head_data['opening_balance'];
                        }
                        $head_data['drsum']=$drsum;
                        $head_data['crsum']=$crsum;
                        $head_data['ladger_type']="head";
                        $result_head[]=$head_data;
                        $gdrsum+=$drsum;
                        $gcrsum+=$crsum;
                    }
                }

                $result[]=array("accounting_name" => $account_assets_group_Name ,"accounting_id" => '-', "drsum" => $gdrsum, "crsum" => $gcrsum,"ladger_type"=> "Group");

                foreach($result_head as $key => $value)
                {
                    $result[]=$value;
                }
            }
        }

        // $pnl_sql=mysqli_query($conn,"SELECT `srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType FROM `pnl_acc_tbl` WHERE `from_date`='$from_date' AND `to_date`='$todate'");

        $pnl_condtnArr['`pnl_acc_tbl`.`from_date`']=$from_date;
        $pnl_condtnArr['`pnl_acc_tbl`.`to_date`']=$todate;

        $query=$this->Mcommon->getRecords($tableName=$this->pnl_acc_tbl, $colNames="`srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType", $pnl_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $pnl_sql=$query['userdata'];

        // if(mysqli_num_rows($pnl_sql)>0)
        if(!empty($pnl_sql))
        {
            $pnl_data=$pnl_sql;
            $pdrsum="";
            $pcrsum="";

            if($pnl_data['ladgerType']=="Dr")
            {
                $pdrsum=$pnl_data['openBal'];
            }
            else
            {
                $pcrsum=$pnl_data['openBal'];
            }

            $result[]=array("accounting_name" => "Profit & Loss A/c" ,"accounting_id" => '-', "drsum" => $pdrsum, "crsum" => $pcrsum,"ladger_type"=> "Group");
        }

        $this->data['trail_balance_data']=$result;

        $this->data['view']="report/trail_balance";
        $this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function profit_loss()
    {
        $breadcrumbArr[0]['name']="Reports";
        $breadcrumbArr[0]['link']=base_url()."report/profit_loss";
        $breadcrumbArr[0]['active']=FALSE;

        $breadcrumbArr[1]['name']="Profit & Loss";
        $breadcrumbArr[1]['link']="javascript:void(0)";
        $breadcrumbArr[1]['active']=TRUE;

        $this->data['breadcrumbArr']=$breadcrumbArr;

        $currmon=date("m");

        if($currmon>=4) 
        {
            $curryear= date("Y")."-04-01";
            $nxt_yr=(date("Y")+1)."-03-31";
        }
        else
        {
            $curryear= (date("Y")-1)."-04-01";
            $nxt_yr=date("Y")."-03-31";
        }

        $m_date1= date("Y-m-01");
        $m_date2=date("Y-m-t");

        if($this->input->post('pnl_search'))
        {
            $financial=$this->input->post('financial');
            $years_fn=explode("_", $financial);
            $curryear= $years_fn[0];
            $nxt_yr=$years_fn[1];
        }

        $this->data['curryear']=$curryear;
        $this->data['nxt_yr']=$nxt_yr;

        $from_date=$curryear;
        $todate=$nxt_yr;

        $this->form_validation->set_rules('financial', 'Financial', 'trim');

        // $account_assets_group_sql=mysqli_query($conn,"SELECT `id`, `name`, `account_type`, `extra`,(SELECT SUM(`opening_balance`) FROM `accounting_types` WHERE `accounting_types`.`Account_group`=`accounting_groups`.`id`) AS opening_balance FROM `accounting_groups` WHERE `name`='Direct Income'");

        $acc_condtnArr['`name`']='Direct Income';

        $query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`, `extra`, (SELECT SUM(`opening_balance`) FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`Account_group` = `accounting_groups`.`id`) AS opening_balance", $acc_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $account_assets_group_sql=$query['userdata'];
        
        // if(mysqli_num_rows($account_assets_group_sql)>0)
        if(!empty($account_assets_group_sql))
        {
            // while($account_assets_group_data=mysqli_fetch_assoc($account_assets_group_sql))
            foreach($account_assets_group_sql AS $account_assets_group_data)
            {
                $flag=0;
                $arr_assets=array();
                $ass_total=$crr_ass="0";
                $account_assets_group_Id=$account_assets_group_data['id'];
                $account_assets_group_Name=$account_assets_group_data['name'];
                $account_assets_group_opening_balance=$account_assets_group_data['opening_balance'];

                if(empty($account_assets_group_opening_balance))
                {
                    $account_assets_group_opening_balance="0";
                }
                $account_assets_group_cr_dr=$account_assets_group_data['extra'];
            
                $account_assets_group[]=$account_assets_group_data;
            
                if($account_assets_group_Name=="Sundry Debitors")
                {
                    $flag=1;
                }
            
                // $account_assets_group_head_sql=mysqli_query($conn,"SELECT (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group` FROM `accounting_types` WHERE `Account_group`='$account_assets_group_Id'");

                $accHd_condtnArr['`accounting_types`.`Account_group`']=$account_assets_group_Id;

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="(SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`", $accHd_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $account_assets_group_head_sql=$query['userdata'];

                // if(mysqli_num_rows($account_assets_group_head_sql)>0)
                if(!empty($account_assets_group_head_sql))
                {
                    // while($account_assets_group_head_data=mysqli_fetch_assoc($account_assets_group_head_sql))
                    foreach($account_assets_group_head_sql AS $account_assets_group_head_data)
                    {
                        $account_assets_group_headId=$account_assets_group_head_data['accounting_id'];
                        $trail_bal=0; 
                        $trail_ass_Data=array();

                        // $trail_ass_sql=mysqli_query($conn,"SELECT SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature FROM `accounting` WHERE `accouting_id`='$account_assets_group_headId' AND `s-date` BETWEEN '$from_date' AND '$todate'");

                        $trail_condtnArr['`accounting`.`accouting_id`']=$account_assets_group_headId;
                        $trail_customWhereArray[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";

                        $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames=" SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature", $trail_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $trail_customWhereArray, $backTicks=TRUE);

                        $trail_ass_sql=$query['userdata'];

                        // if(mysqli_num_rows($trail_ass_sql)>0)
                        if(!empty($trail_ass_sql))
                        {
                            $trail_ass_Data=$trail_ass_sql;

                            if(!empty($trail_ass_Data['accountNature']))
                            {
                                if($trail_ass_Data['accountNature']=="Cr")
                                {
                                    $trail_bal-=$trail_ass_Data['AccBal']; 
                                }
                                else
                                {
                                    $trail_bal+=$trail_ass_Data['AccBal']; 
                                }
                            }
                        }

                        $account_assets_group_head_Name=$account_assets_group_head_data['accounting_name'];
                        $account_assets_group_head_cr_dr=$account_assets_group_head_data['type_ledger'];

                        if(empty($account_assets_head_group_cr_dr))
                        {
                            $account_assets_group_head_cr_dr=$account_assets_group_data['extra'];
                        }

                        $account_assets_group_head_opening_balance=$account_assets_group_head_data['opening_balance'];
                        
                        if($flag=="0")
                        {
                             $arr_assets[]=array(
                                "Name"=>$account_assets_group_head_Name,
                                "balance"=>$account_assets_group_head_opening_balance,
                                "cr_dr"=>$account_assets_group_head_cr_dr,
                                "balanceCrr"=>$trail_bal,
                                "heading"=>"head"
                            );
                        }
                   
                        $ass_total+=$account_assets_group_head_opening_balance;
                        $crr_ass+=$trail_bal;
                    }
                }
            
                $result_assets[]=array(
                    "Name"=>$account_assets_group_Name,
                    "balance"=>$ass_total,
                    "cr_dr"=>$account_assets_group_cr_dr,
                    "balanceCrr"=>$crr_ass,
                    "heading"=>"Group"
                ); 
            
                foreach($arr_assets as $key => $value)
                {
                    $result_assets[]=array(
                        "Name"=>$value['Name'],
                        "balance"=>$value['balance'],
                        "cr_dr"=>$value['cr_dr'],
                        "balanceCrr"=>$value['balanceCrr'],
                        "heading"=>"head"
                    );
                }
            }
        }

        // $account_lib_group_sql=mysqli_query($conn,"SELECT `id`, `name`, `account_type`, `extra`,(SELECT SUM(`opening_balance`) FROM `accounting_types` WHERE `accounting_types`.`Account_group`=`accounting_groups`.`id`) AS opening_balance FROM `accounting_groups` WHERE `name`='Direct Expenses'");
        
        $acc_grp_condtnArr['`accounting_groups`.`name`']="Direct Expenses";

        $query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`, `extra`, (SELECT SUM(`opening_balance`) FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`Account_group` = `accounting_groups`.`id`) AS opening_balance", $acc_grp_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $account_lib_group_sql=$query['userdata'];

        if(!empty($account_lib_group_sql))
        {
            foreach($account_lib_group_sql AS $account_lib_group_data)
            {
                $arr_lib=array();
                $lib_total=$crr_lib="0";
                $account_lib_group_Id=$account_lib_group_data['id'];
                $account_lib_group_Name=$account_lib_group_data['name'];
                $account_lib_group_opening_balance=$account_lib_group_data['opening_balance'];

                if(empty($account_lib_group_opening_balance))
                {
                    $account_lib_group_opening_balance="0";
                }
                $account_lib_group_cr_dr=$account_lib_group_data['extra'];
            
                //$account_assets_group[]=$account_lib_group_data;
                // $account_lib_group_head_sql=mysqli_query($conn,"SELECT (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS type_ledger,`id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group` FROM `accounting_types` WHERE `Account_group`='$account_lib_group_Id'");

                $accHd_condtnArr1['`accounting_types`.`Account_group`']=$account_lib_group_Id;

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="(SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`", $accHd_condtnArr1, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $account_lib_group_head_sql=$query['userdata'];

                if(!empty($account_lib_group_head_sql))
                {
                    // while($account_lib_group_head_data=mysqli_fetch_assoc($account_lib_group_head_sql))
                    foreach($account_lib_group_head_sql AS $account_lib_group_head_data)
                    {
                        $account_lib_group_headId=$account_lib_group_head_data['accounting_id'];
                        $trail_lib_bal=0;  

                        // $trail_lib_sql=mysqli_query($conn,"SELECT `accouting_id`, SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature FROM `accounting` WHERE `accouting_id`='$account_lib_group_headId' AND `s-date` BETWEEN '$from_date' AND '$todate'");

                        $acc_condtnArr1['`accouting_id`']=$account_lib_group_headId;
                        $acc_customWhereArray1[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";

                        $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accouting_id`, SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature", $acc_condtnArr1, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $acc_customWhereArray1, $backTicks=TRUE);

                        $trail_lib_sql=$query['userdata'];

                        // if(mysqli_num_rows($trail_lib_sql)>0)
                        if(!empty($trail_lib_sql))
                        {
                            $trail_lib_Data=$trail_lib_sql;

                            if(!empty($trail_lib_Data['accountNature']))
                            {
                                if($trail_lib_Data['accountNature']=="Dr")
                                {
                                    $trail_lib_bal-=$trail_lib_Data['AccBal']; 
                                }
                                else
                                {
                                    $trail_lib_bal+=$trail_lib_Data['AccBal']; 
                                }
                            }
                        }
                    
                        $account_lib_group_head_Name=$account_lib_group_head_data['accounting_name'];
                        $account_lib_group_head_cr_dr=$account_lib_group_head_data['type_ledger'];
                        if(empty($account_lib_group_head_cr_dr))
                        {
                            $account_lib_group_head_cr_dr=$account_lib_group_data['extra'];
                        }

                        $account_lib_group_head_opening_balance=$account_lib_group_head_data['opening_balance'];
                        $arr_lib[]=array(
                            "Name"=>$account_lib_group_head_Name,
                            "balance"=>$account_lib_group_head_opening_balance,
                            "cr_dr"=>$account_lib_group_head_cr_dr,
                            "balanceCrr"=>$trail_lib_bal,
                            "heading"=>"head"
                        );

                        $lib_total+=$account_lib_group_head_opening_balance;
                        $crr_lib+=$trail_lib_bal;    
                    }
                }

                $result_lib[]=array(
                    "Name"=>$account_lib_group_Name,
                    "balance"=>$lib_total,
                    "cr_dr"=>$account_lib_group_cr_dr,
                    "balanceCrr"=>$crr_lib,
                    "heading"=>"Group"
                ); 
            
                foreach($arr_lib as $keys => $values)
                {
                    $result_lib[]=array(
                        "Name"=>$values['Name'],
                        "balance"=>$values['balance'],
                        "cr_dr"=>$values['cr_dr'],
                        "balanceCrr"=>$values['balanceCrr'],
                        "heading"=>"head"
                    );
                }
            }
        }

        $asset_size=sizeof($result_assets);
        // print_r(json_encode($result_assets));
        $lib_size=sizeof($result_lib);

        $count_sum=$asset_size;
        if($asset_size<$lib_size)
        {
            $count_sum=$lib_size;
        }
        elseif($asset_size>$lib_size)
        {
            $count_sum=$asset_size;
        }
        
        for($i=0;$i<$count_sum;$i++)
        {
            if(isset($result_assets[$i]['Name']))
                $account_assets_group_Name=$result_assets[$i]['Name'];
            else
                $account_assets_group_Name="";
                
            if(isset($result_assets[$i]['balance']))
                $account_assets_group_opening_balance=$result_assets[$i]['balance'];
            else
                $account_assets_group_opening_balance="0";
                
            if(isset($result_assets[$i]['balance']))
                $account_assets_group_current_balance=$result_assets[$i]['balanceCrr'];
            else
                $account_assets_group_current_balance="0";
                
            if(isset($result_assets[$i]['cr_dr']))
                $account_assets_group_cr_dr=$result_assets[$i]['cr_dr'];
            else
                $account_assets_group_cr_dr="";
                
            if(isset($result_assets[$i]['heading']))
                $account_heading=$result_assets[$i]['heading'];
            else
                $account_heading="";
            
            if(isset($result_lib[$i]['Name']))
                $account_lib_group_Name=$result_lib[$i]['Name'];
            else
                $account_lib_group_Name="";
                
            if(isset($result_lib[$i]['balance']))
                $account_lib_group_opening_balance=$result_lib[$i]['balance'];
            else
                $account_lib_group_opening_balance="0";
                
            if(isset($result_lib[$i]['balance']))
                $account_lib_group_curr_balance=$result_lib[$i]['balanceCrr'];
            else
                $account_lib_group_curr_balance="0";
                
            if(isset($result_lib[$i]['cr_dr']))
                $account_lib_group_cr_dr=$result_lib[$i]['cr_dr'];
            else
                $account_lib_group_cr_dr="";
                
            if(isset($result_lib[$i]['heading']))
                $account_lib_heading=$result_lib[$i]['heading'];
            else
                $account_lib_heading="";
                
            // $account_assets_group_Name=$result_assets[$i]['Name'];
            // $account_assets_group_opening_balance=$result_assets[$i]['balance'];
            // $account_assets_group_current_balance=$result_assets[$i]['balanceCrr'];
            // $account_assets_group_cr_dr=$result_assets[$i]['cr_dr'];
            // $account_heading=$result_assets[$i]['heading'];
            // $account_lib_group_Name=$result_lib[$i]['Name'];
            // $account_lib_group_opening_balance=$result_lib[$i]['balance'];
            // $account_lib_group_curr_balance=$result_lib[$i]['balanceCrr'];
            // $account_lib_group_cr_dr=$result_lib[$i]['cr_dr'];
            // $account_lib_heading=$result_lib[$i]['heading'];

            $result[]=array(
                "asst_Name"=>$account_assets_group_Name,
                "asst_balance"=>$account_assets_group_opening_balance,
                "asst_crr_balance"=>$account_assets_group_current_balance,
                "asst_cr_dr"=>$account_assets_group_cr_dr,
                "asst_heading"=>$account_heading,
                "lib_Name"=>$account_lib_group_Name,
                "lib_balance"=>$account_lib_group_opening_balance,
                "lib_crr_balance"=>$account_lib_group_curr_balance,
                "lib_cr_dr"=>$account_lib_group_cr_dr,
                "lib_heading"=>$account_lib_heading
            ); 
        }

        $this->data['pnl_sheet_data']=$result;

        $query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="`soc_name`, `soc_register_no`, `soc_address`, `bill_note`", $soc_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $soc_details=$query['userdata'];

        $this->data['soc_details']=$soc_details;

        if($this->form_validation->run() == FALSE)
        {
            $this->data['view']="report/profit_loss";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
            $this->data['view']="report/profit_loss";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
    }

    public function balance_sheet()
    {
        $breadcrumbArr[0]['name']="Reports";
        $breadcrumbArr[0]['link']=base_url()."report/balance_sheet";
        $breadcrumbArr[0]['active']=FALSE;

        $breadcrumbArr[1]['name']="Balance Sheet";
        $breadcrumbArr[1]['link']="javascript:void(0)";
        $breadcrumbArr[1]['active']=TRUE;

        $this->data['breadcrumbArr']=$breadcrumbArr;

        $currmon=date("m");

        if($currmon>=4) 
        {
            $curryear= date("Y")."-04-01";
            $nxt_yr=(date("Y")+1)."-03-31";
        }
        else
        {
            $curryear= (date("Y")-1)."-04-01";
            $nxt_yr=date("Y")."-03-31";
        }

        $m_date1= date("Y-m-01");
        $m_date2=date("Y-m-t");

        if($this->input->post('Bal_search'))
        {
            $financial=$this->input->post('financial');
            $years_fn=explode("_", $financial);
            $curryear= $years_fn[0];
            $nxt_yr=$years_fn[1];
        }

        $this->data['curryear']=$curryear;
        $this->data['nxt_yr']=$nxt_yr;

        $from_date=$curryear;
        $todate=$nxt_yr;

        $this->form_validation->set_rules('financial', 'Financial', 'trim');

        // $account_assets_group_sql=mysqli_query($conn,"SELECT `id`, `name`, `account_type`, `extra`,(SELECT SUM(`opening_balance`) FROM `accounting_types` WHERE `accounting_types`.`Account_group`=`accounting_groups`.`id`) AS opening_balance FROM `accounting_groups` WHERE `account_type`='Assets'");

        $acc_condtnArr['`account_type`']='Assets';

        $query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`, `extra`, (SELECT SUM(`opening_balance`) FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`Account_group` = `accounting_groups`.`id`) AS opening_balance", $acc_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $account_assets_group_sql=$query['userdata'];

        // if(mysqli_num_rows($account_assets_group_sql)>0)
        if(!empty($account_assets_group_sql))
        {
            // while($account_assets_group_data=mysqli_fetch_assoc($account_assets_group_sql))
            foreach($account_assets_group_sql AS $account_assets_group_data)
            {
           
                $flag=0;
                $arr_assets=array();
                $ass_total=$crr_ass="0";
                $account_assets_group_Id=$account_assets_group_data['id'];
                $account_assets_group_Name=$account_assets_group_data['name'];
                $account_assets_group_opening_balance=$account_assets_group_data['opening_balance'];

                if(empty($account_assets_group_opening_balance))
                {
                    $account_assets_group_opening_balance="0";
                }

                $account_assets_group_cr_dr=$account_assets_group_data['extra'];
                $account_assets_group[]=$account_assets_group_data;
            
                if($account_assets_group_Name=="Sundry Debitors")
                {
                    $flag=1;
                }
            
                // $account_assets_group_head_sql=mysqli_query($conn,"SELECT (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group` FROM `accounting_types` WHERE `Account_group`='$account_assets_group_Id'");

                $accHd_condtnArr['`accounting_types`.`Account_group`']=$account_assets_group_Id;

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="(SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`", $accHd_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $account_assets_group_head_sql=$query['userdata'];

                // if(mysqli_num_rows($account_assets_group_head_sql)>0)
                if(!empty($account_assets_group_head_sql))
                {
                    // while($account_assets_group_head_data=mysqli_fetch_assoc($account_assets_group_head_sql))
                    foreach($account_assets_group_head_sql AS $account_assets_group_head_data)
                    {
                        $account_assets_group_headId=$account_assets_group_head_data['accounting_id'];
                        $trail_bal=0; 
                        $trail_ass_Data=array();

                        // $trail_ass_sql=mysqli_query($conn,"SELECT SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature FROM `accounting` WHERE `accouting_id`='$account_assets_group_headId' AND `s-date` BETWEEN '$from_date' AND '$todate'");

                        $trail_condtnArr['`accounting`.`accouting_id`']=$account_assets_group_headId;
                        $trail_customWhereArray[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";

                        $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames=" SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature", $trail_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $trail_customWhereArray, $backTicks=TRUE);

                        $trail_ass_sql=$query['userdata'];

                        // if(mysqli_num_rows($trail_ass_sql)>0)
                        if(!empty($trail_ass_sql))
                        {
                            $trail_ass_Data=$trail_ass_sql;

                            if(!empty($trail_ass_Data['accountNature']))
                            {
                                if($trail_ass_Data['accountNature']=="Cr")
                                {
                                    $trail_bal-=$trail_ass_Data['AccBal']; 
                                }
                                else
                                {
                                    $trail_bal+=$trail_ass_Data['AccBal']; 
                                }
                            }
                        }

                        $account_assets_group_head_Name=$account_assets_group_head_data['accounting_name'];
                        $account_assets_group_head_cr_dr=$account_assets_group_head_data['type_ledger'];
                        if(empty($account_assets_head_group_cr_dr))
                        {
                            $account_assets_group_head_cr_dr=$account_assets_group_data['extra'];
                        }
                        $account_assets_group_head_opening_balance=$account_assets_group_head_data['opening_balance'];
                    
                        if($flag=="0")
                        {
                            $arr_assets[]=array(
                                "Name"=>$account_assets_group_head_Name,
                                "balance"=>$account_assets_group_head_opening_balance,
                                "cr_dr"=>$account_assets_group_head_cr_dr,
                                "balanceCrr"=>$trail_bal,
                                "heading"=>"head"
                            );
                        }
                        $ass_total+=$account_assets_group_head_opening_balance;
                        $crr_ass+=$trail_bal;
                    }
                }
            
                $result_assets[]=array(
                    "Name"=>$account_assets_group_Name,
                    "balance"=>$ass_total,
                    "cr_dr"=>$account_assets_group_cr_dr,
                    "balanceCrr"=>$crr_ass,
                    "heading"=>"Group"
                ); 

                foreach($arr_assets as $key => $value)
                {
                    $result_assets[]=array(
                        "Name"=>$value['Name'],
                        "balance"=>$value['balance'],
                        "cr_dr"=>$value['cr_dr'],
                        "balanceCrr"=>$value['balanceCrr'],
                        "heading"=>"head"
                    );
                }
            }
        }
    
        // $account_lib_group_sql=mysqli_query($conn,"SELECT `id`, `name`, `account_type`, `extra`,(SELECT SUM(`opening_balance`) FROM `accounting_types` WHERE `accounting_types`.`Account_group`=`accounting_groups`.`id`) AS opening_balance FROM `accounting_groups` WHERE `account_type`='Liability'");

        $acc_condtnArr1['`account_type`']='Liability';

        $query=$this->Mcommon->getRecords($tableName=$this->accounting_groups_tbl, $colNames="`id`, `name`, `account_type`, `extra`, (SELECT SUM(`opening_balance`) FROM ".$this->accounting_types_tbl." WHERE `accounting_types`.`Account_group` = `accounting_groups`.`id`) AS opening_balance", $acc_condtnArr1, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $account_lib_group_sql=$query['userdata'];

        // if(mysqli_num_rows($account_lib_group_sql)>0)
        if(!empty($account_lib_group_sql))
        {
            // while($account_lib_group_data=mysqli_fetch_assoc($account_lib_group_sql))
            foreach($account_lib_group_sql AS $account_lib_group_data)
            {
                $arr_lib=array();
                $lib_total=$crr_lib="0";
                $account_lib_group_Id=$account_lib_group_data['id'];
                $account_lib_group_Name=$account_lib_group_data['name'];
                $account_lib_group_opening_balance=$account_lib_group_data['opening_balance'];

                if(empty($account_lib_group_opening_balance))
                {
                    $account_lib_group_opening_balance="0";
                }
                $account_lib_group_cr_dr=$account_lib_group_data['extra'];
            
                // $account_lib_group_head_sql=mysqli_query($conn,"SELECT (SELECT `opening_balance` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS opening_balance,(SELECT `account_nature` FROM `accounting_opening_balance` WHERE `accounting_opening_balance`.`accounting_id`=`accounting_types`.`accounting_id` AND (`from_date`='$from_date' AND `to_date`='$todate')) AS type_ledger,`id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group` FROM `accounting_types` WHERE `Account_group`='$account_lib_group_Id'");

                $accHd_condtnArr1['`accounting_types`.`Account_group`']=$account_lib_group_Id;

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_types_tbl, $colNames="(SELECT `opening_balance` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS opening_balance, (SELECT `account_nature` FROM ".$this->accounting_opening_balance_tbl." WHERE `accounting_opening_balance`.`accounting_id` = `accounting_types`.`accounting_id` AND (`from_date`='".$from_date."' AND `to_date`='".$todate."')) AS type_ledger, `id`, `accounting_id`, `accounting_type`, `accounting_name`, `Account_group`", $accHd_condtnArr1, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $account_lib_group_head_sql=$query['userdata'];

                // if(mysqli_num_rows($account_lib_group_head_sql)>0)
                if(!empty($account_lib_group_head_sql))
                {
                    // while($account_lib_group_head_data=mysqli_fetch_assoc($account_lib_group_head_sql))
                    foreach($account_lib_group_head_sql AS $account_lib_group_head_data)
                    {
                        $account_lib_group_headId=$account_lib_group_head_data['accounting_id'];
                        $trail_lib_bal=0;  

                        // $trail_lib_sql=mysqli_query($conn,"SELECT `accouting_id`, SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature FROM `accounting` WHERE `accouting_id`='$account_lib_group_headId' AND `s-date` BETWEEN '$from_date' AND '$todate'");

                        $trail_condtnArr1['`accounting`.`accouting_id`']=$account_lib_group_headId;
                        $trail_customWhereArray1[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";

                        $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames=" SUM(`accounting_charges`) AS AccBal, `cr_dr` AS accountNature", $trail_condtnArr1, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $trail_customWhereArray1, $backTicks=TRUE);

                        $trail_lib_sql=$query['userdata'];

                        // if(mysqli_num_rows($trail_lib_sql)>0)
                        if(!empty($trail_lib_sql))
                        {
                            $trail_lib_Data=$trail_lib_sql;

                            if(!empty($trail_lib_Data['accountNature']))
                            {
                                if($trail_lib_Data['accountNature']=="Dr")
                                {
                                    $trail_lib_bal-=$trail_lib_Data['AccBal']; 
                                }
                                else
                                {
                                    $trail_lib_bal+=$trail_lib_Data['AccBal']; 
                                }
                            }
                        }
                
                        $account_lib_group_head_Name=$account_lib_group_head_data['accounting_name'];
                        $account_lib_group_head_cr_dr=$account_lib_group_head_data['type_ledger'];

                        if(empty($account_lib_group_head_cr_dr))
                        {
                            $account_lib_group_head_cr_dr=$account_lib_group_data['extra'];
                        }

                        $account_lib_group_head_opening_balance=$account_lib_group_head_data['opening_balance'];

                        $arr_lib[]=array(
                            "Name"=>$account_lib_group_head_Name,
                            "balance"=>$account_lib_group_head_opening_balance,
                            "cr_dr"=>$account_lib_group_head_cr_dr,
                            "balanceCrr"=>$trail_lib_bal,
                            "heading"=>"head"
                        );

                        $lib_total+=$account_lib_group_head_opening_balance;
                        $crr_lib+=$trail_lib_bal;  
                    }
                }

                $result_lib[]=array(
                    "Name"=>$account_lib_group_Name,
                    "balance"=>$lib_total,
                    "cr_dr"=>$account_lib_group_cr_dr,
                    "balanceCrr"=>$crr_lib,
                    "heading"=>"Group"
                ); 
            
                foreach($arr_lib as $keys => $values)
                {
                    $result_lib[]=array(
                        "Name"=>$values['Name'],
                        "balance"=>$values['balance'],
                        "cr_dr"=>$values['cr_dr'],
                        "balanceCrr"=>$values['balanceCrr'],
                        "heading"=>"head"
                    );
                }
            }
        }
    
        // $pnl_sql=mysqli_query($conn,"SELECT `srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType FROM `pnl_acc_tbl` WHERE `from_date`='$from_date' AND `to_date`='$todate'");

        // $pnl_customWhereArray[]="`s-date` BETWEEN '".$from_date."' AND '".$todate."'";
        $pnl_condtnArr['`pnl_acc_tbl`.`from_date`']=$from_date;
        $pnl_condtnArr['`pnl_acc_tbl`.`to_date`']=$todate;

        $query=$this->Mcommon->getRecords($tableName=$this->pnl_acc_tbl, $colNames="`srId`, `ammount` AS openBal, `from_date`, `to_date`, `acc_nature` AS ladgerType", $pnl_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $pnl_customWhereArray=array(), $backTicks=TRUE);

        $pnl_sql=$query['userdata'];

        // if(mysqli_num_rows($pnl_sql)>0)
        if(!empty($pnl_sql))
        {
            $pnl_data=$pnl_sql;

            if($pnl_data['ladgerType']=="Dr")
            {
                $result_assets[]=array(
                    "Name"=>"Profit & Loss A/c",
                    "balance"=>$pnl_data['openBal'],
                    "balanceCrr"=>'0',
                    "cr_dr"=>$pnl_data['ladgerType'],
                    "heading"=>"Group"
                );
            }
            else
            {
                $result_lib[]=array(
                    "Name"=>"Profit & Loss A/c",
                    "balance"=>$pnl_data['openBal'],
                    "cr_dr"=>$pnl_data['ladgerType'],
                    "balanceCrr"=>'0',
                    "heading"=>"Group"
                );
            }
        }

        $asset_size=sizeof($result_assets);
        $lib_size=sizeof($result_lib);

        $count_sum=$asset_size;
        if($asset_size<$lib_size)
        {
            $count_sum=$lib_size;
        }elseif($asset_size>$lib_size)
        {
            $count_sum=$asset_size;
        }
        
        for($i=0;$i<$count_sum;$i++)
        {
            if(isset($result_assets[$i]['Name']))
                $account_assets_group_Name=$result_assets[$i]['Name'];
            else
                $account_assets_group_Name="";
                
            if(isset($result_assets[$i]['balance']))
                $account_assets_group_opening_balance=$result_assets[$i]['balance'];
            else
                $account_assets_group_opening_balance="0";
                
            if(isset($result_assets[$i]['balance']))
                $account_assets_group_current_balance=$result_assets[$i]['balanceCrr'];
            else
                $account_assets_group_current_balance="0";
                
            if(isset($result_assets[$i]['cr_dr']))
                $account_assets_group_cr_dr=$result_assets[$i]['cr_dr'];
            else
                $account_assets_group_cr_dr="";
                
            if(isset($result_assets[$i]['heading']))
                $account_heading=$result_assets[$i]['heading'];
            else
                $account_heading="";
            
            if(isset($result_lib[$i]['Name']))
                $account_lib_group_Name=$result_lib[$i]['Name'];
            else
                $account_lib_group_Name="";
                
            if(isset($result_lib[$i]['balance']))
                $account_lib_group_opening_balance=$result_lib[$i]['balance'];
            else
                $account_lib_group_opening_balance="0";
                
            if(isset($result_lib[$i]['balance']))
                $account_lib_group_curr_balance=$result_lib[$i]['balanceCrr'];
            else
                $account_lib_group_curr_balance="0";
                
            if(isset($result_lib[$i]['cr_dr']))
                $account_lib_group_cr_dr=$result_lib[$i]['cr_dr'];
            else
                $account_lib_group_cr_dr="";
                
            if(isset($result_lib[$i]['heading']))
                $account_lib_heading=$result_lib[$i]['heading'];
            else
                $account_lib_heading="";
                
            // $account_assets_group_Name=$result_assets[$i]['Name'];
            // $account_assets_group_opening_balance=$result_assets[$i]['balance'];
            // $account_assets_group_current_balance=$result_assets[$i]['balanceCrr'];
            // $account_assets_group_cr_dr=$result_assets[$i]['cr_dr'];
            // $account_heading=$result_assets[$i]['heading'];
            // $account_lib_group_Name=$result_lib[$i]['Name'];
            // $account_lib_group_opening_balance=$result_lib[$i]['balance'];
            // $account_lib_group_curr_balance=$result_lib[$i]['balanceCrr'];
            // $account_lib_group_cr_dr=$result_lib[$i]['cr_dr'];
            // $account_lib_heading=$result_lib[$i]['heading'];
           
            $result[]=array(
                "asst_Name"=>$account_assets_group_Name,
                "asst_balance"=>$account_assets_group_opening_balance,
                "asst_crr_balance"=>$account_assets_group_current_balance,
                "asst_cr_dr"=>$account_assets_group_cr_dr,
                "asst_heading"=>$account_heading,
                "lib_Name"=>$account_lib_group_Name,
                "lib_balance"=>$account_lib_group_opening_balance,
                "lib_crr_balance"=>$account_lib_group_curr_balance,
                "lib_cr_dr"=>$account_lib_group_cr_dr,
                "lib_heading"=>$account_lib_heading
            ); 
        }

        $this->data['balance_sheet_data']=$result;

        $query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="`soc_name`, `soc_register_no`, `soc_address`, `bill_note`", $soc_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $soc_details=$query['userdata'];

        $this->data['soc_details']=$soc_details;

        if($this->form_validation->run() == FALSE)
        {
            $this->data['view']="report/balance_sheet";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
            $this->data['view']="report/balance_sheet";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
    }

    public function collection_register()
    {
        $breadcrumbArr[0]['name']="Reports";
        $breadcrumbArr[0]['link']=base_url()."report/collection_register";
        $breadcrumbArr[0]['active']=FALSE;

        $breadcrumbArr[1]['name']="Monthly Collection Register";
        $breadcrumbArr[1]['link']="javascript:void(0)";
        $breadcrumbArr[1]['active']=TRUE;

        $this->data['breadcrumbArr']=$breadcrumbArr;

        $currmon=date("m");

        if($currmon>=4) 
        {
            $curryear= date("Y")."-04-01";
            $nxt_yr=(date("Y")+1)."-03-31";
        }
        else
        {
            $curryear= (date("Y")-1)."-04-01";
            $nxt_yr=date("Y")."-03-31";
        }

        $m_date1= date("Y-m-01");
        $m_date2=date("Y-m-t");

        if($this->input->post('collection_search'))
        {
            $financial=$this->input->post('financial');
            $years_fn=explode("_",$financial);
            $m_date1= $years_fn[0];
            $m_date2=$years_fn[1];
        }

        $this->data['curryear']=$curryear;
        $this->data['m_date1']=$m_date1;
        $this->data['m_date2']=$m_date2;

        $fromdate=$m_date1;
        $todate=$m_date2;

        $this->form_validation->set_rules('financial', 'Financial', 'trim');

        // $fetch_maint_user=mysqli_query($conn,"SELECT `user_name`, `end_day`,`tansaction_no` FROM `maintance` WHERE `start_day`='$fromdate' AND `end_day`='$todate'");
        $resultsCash=array();

        $usr_condtnArr['`maintenance`.`start_day`']=$fromdate;
        $usr_condtnArr['`maintenance`.`end_day`']=$todate;

        $query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="`user_name`, `end_day`,`tansaction_no`", $usr_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $pnl_customWhereArray=array(), $backTicks=TRUE);

        $fetch_maint_user=$query['userdata'];

        if(!empty($fetch_maint_user))
        {
            // while($maint_user=mysqli_fetch_assoc($fetch_maint_user))
            foreach($fetch_maint_user AS $maint_user)
            {
                $value3="Unpaid";
                $todate=$maint_user['end_day'];
                $transactionId=$maint_user['tansaction_no'];
                $user_name=$maint_user['user_name'];
                // $result[]=Fetch_maintenan_reg($end_day,$tansaction_no,$value3,$user_name,$userName,$soc_key);
                // function Fetch_maintenan_reg($todate,$transactionId,$value3,$userId,$username,$soc_key)
                $fromdate=date("Y-m-d");
                $result = array(); 
                // $soc_bill_fetch=mysqli_query($conn,"SELECT `soc_name`, `soc_register_no`, `soc_address`, `bill_note` FROM `society_master`");

                $query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="`soc_name`, `soc_register_no`, `soc_address`, `bill_note`", $soc_bill_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $soc_bill_fetch=$query['userdata'];

                if(!empty($soc_bill_fetch))
                {
                    $result['society_details']=$soc_bill_fetch;
                }

                // $select = "SELECT `status`, `s-r-fname`, `s-r-lname`, DATE_FORMAT(`due_date`,'%d %b %Y') AS duedate,`tansaction_no` AS tansaction_no1, `voucher_name` AS voucher_no1, `amount` AS total, DATE_FORMAT(`end_day`,'%d %b %Y') AS `end_day`, DATE_FORMAT(`start_day`,'%d %b %Y') AS `start_day`, `due_amount`, CONCAT(`s-r-floor`,`s-r-flat`) AS flatNo, `start_day` AS biiEnd FROM `maintance` LEFT JOIN `s-r-user` ON `maintance`.`user_name`=`s-r-user`.`s-r-username` WHERE `maintance`.`tansaction_no`='$transactionId' AND  `maintance`.`user_name`='$userId'";

                $mnt_condtnArr['`maintance`.`tansaction_no`']=$transactionId;
                $mnt_condtnArr['`maintance`.`user_name`']=$userId;

                $mnt_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`maintance`.`user_name`=`s-r-user`.`s-r-username`", "type"=>"left");

                $query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="`status`, `s-r-fname`, `s-r-lname`, DATE_FORMAT(`due_date`,'%d %b %Y') AS duedate,`tansaction_no` AS tansaction_no1, `voucher_name` AS voucher_no1, `amount` AS total, DATE_FORMAT(`end_day`,'%d %b %Y') AS `end_day`, DATE_FORMAT(`start_day`,'%d %b %Y') AS `start_day`, `due_amount`, CONCAT(`s-r-floor`,`s-r-flat`) AS flatNo, `start_day` AS biiEnd", $mnt_condtnArr, $likeCondtnArr=array(), $mnt_joinArr, $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $get_value=$query['userdata'];
                // $fire=mysqli_query($conn,$select);
                // $get_value=mysqli_fetch_assoc($fire);

                $result['flatNo']=$get_value['flatNo'];
                $result['status']=$get_value['status'];
                $result['s-r-fname']=$get_value['s-r-fname'];
                $result['s-r-lname']=$get_value['s-r-lname'];
                $result['bill_dt']=$get_value['start_day'];
                $result['duedate']=$get_value['duedate'];
                $result['total']=$get_value['total'];
                $result['principal_arrears']=$get_value['due_amount'];
                $result['tansaction_no1']=$get_value['tansaction_no1'];
                $result['voucher_no1']=$get_value['voucher_no1'];
                $billDt=$get_value['date'];
                $billendDt=$get_value['biiEnd'];

                $result['intrestAmount']=0;

                // $data_qry=mysqli_query($conn,"SELECT `head_name` AS Particular, (SELECT `charges` FROM `member_maintenance_tbl` 
                // WHERE `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('$fromdate' between 
                // `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('$todate' between 
                // `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS charges FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC ");

                $mntHd_condtnArr['`maintenance_head`.`head_status`']='Active';
                $mntHd_orderByArr['`maintenance_head`.`srid`']='ASC';

                $query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="`head_name` AS Particular, (SELECT `charges` FROM ".$this->member_maintenance_tbl." WHERE `memberId`='".$userId."' AND `maintenance_headId` = `maintenance_head`.`maint_headId` AND ('".$fromdate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('".$todate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS charges", $mntHd_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $mntHd_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $data_qry=$query['userdata'];

                // while($headerdata=mysqli_fetch_assoc($data_qry))
                if(!empty($data_qry))
                {
                    foreach($data_qry AS $headerdata)
                    {
                        $maint_perticular[]=$headerdata;
                    }
                }

                $total=0;
                if(!empty($maint_perticular))
                {
                    foreach($maint_perticular as $key => $value)
                    {
                        $result['MaintenanceParticular'][]=array(
                            'Particular'=>$value['Particular'],
                            'Charges'=>$value['charges']
                        );
                        $total+=$value['charges'];
                    }
                }

                // $fetch_arres=mysqli_query($conn,"SELECT `advance_amount` FROM `members_advance_log` WHERE `creation_date`<'$billendDt' AND `user_id`='$userId' ORDER BY `creation_date` DESC LIMIT 1");

                $advAmt_condtnArr['`members_advance_log`.`creation_date` <']=$billendDt;
                $advAmt_condtnArr['`members_advance_log`.`user_id`']=$userId;
                $advAmt_orderByArr['`members_advance_log`.`creation_date`']='DESC';

                $query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="`advance_amount`", $advAmt_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $advAmt_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="");

                $advAmtData=$query['userdata'];

                if(!empty($advAmtData))
                {
                    $result['MaintenanceAdvance']=$advAmtData['advance_amount'];
                }
                else
                {
                    $result['MaintenanceAdvance']=0;
                }

                $result['MaintenanceSubTotal']=$total;

                // $maint_intrest_fetch=mysqli_query($conn, "SELECT `penalty_percentage` FROM `penalty` WHERE `flag`='Yes'");
                // $maint_intrest=mysqli_fetch_assoc($maint_intrest_fetch);

                $pnlty_condtnArr['`penalty`.`flag`']='Yes';

                $query=$this->Mcommon->getRecords($tableName=$this->penalty_tbl, $colNames="`penalty_percentage`", $pnlty_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $pnlty_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $maint_intrest=$query['userdata'];

                $result['intrest']=$maint_intrest['penalty_percentage'];
                //  $result['intrestAmount']=$intrest;//value came from db
                $result['totalArrears']=$result['principal_arrears']+$result['intrestAmount'];
                $result['MaintenanceGrandTotal']=$result['MaintenanceSubTotal']+$result['totalArrears'];
                //$result['MaintenanceParticular'][]=array('Particular'=>NULL,'Charges'=>NULL);
                $result['start_day']=$get_value['start_day'];
                $result['end_day']=$get_value['end_day'];
                $result['inwords']=inWords($result['MaintenanceGrandTotal']);
                $result['receipt']=NULL;

                // $reciptdatasql=mysqli_query($conn,"SELECT `transaction_no`, DATE_FORMAT(`s-date`,'%d %b %Y') AS Recipt_date,`accounting_charges`, `payment_mode`, `cheque_no`, (SELECT `tansaction_no` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS Billnum, (SELECT `start_day` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS billperiod FROM `accounting` WHERE `vaucher_name`='Reciept Voucher' AND `member_id`='$userId' AND `s-date`<'$billendDt' AND `cr_dr`='Cr' ORDER BY `a_id` DESC LIMIT 1");

                $accnt_condtnArr['`vaucher_name`']='Reciept Voucher';
                $accnt_condtnArr['`member_id`']=$userId;
                $accnt_condtnArr['`s-date` <']=$billendDt;
                $accnt_condtnArr['`cr_dr`']='Cr';
                $accnt_orderByArr['`a_id`']='DESC';

                $query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`transaction_no`, DATE_FORMAT(`s-date`,'%d %b %Y') AS Recipt_date,`accounting_charges`, `payment_mode`, `cheque_no`, (SELECT `tansaction_no` FROM ".$this->maintenance_tbl." WHERE `user_name`='".$userId."' ORDER BY `start_day` DESC LIMIT 1) AS Billnum, (SELECT `start_day` FROM ".$this->maintenance_tbl." WHERE `user_name`='".$userId."' ORDER BY `start_day` DESC LIMIT 1) AS billperiod", $accnt_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $accnt_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

                $recipt_fetch=$query['userdata'];

                if(!empty($recipt_fetch))
                {
                    $result['receipt']=array(
                        "transaction_no"=>$recipt_fetch['transaction_no'],
                        "Recipt_date"=>$recipt_fetch['Recipt_date'],
                        "Recipt_amount"=>$recipt_fetch['accounting_charges'],
                        "Recipt_amountWords"=>$this->Mfunctional->inWords($recipt_fetch['accounting_charges']),
                        "payment_mode"=>$recipt_fetch['payment_mode'],
                        "cheque_no"=>$recipt_fetch['cheque_no'],
                        "Billnum"=>$recipt_fetch['Billnum'],
                        "billperiod"=>date("F",strtotime($recipt_fetch['billperiod'])),
                        "Cheque_date"=>date("d-m-Y",strtotime($recipt_fetch['billperiod']))
                    );
                }

                $resultsCash=$result;
            }  
        }

        $this->data['resultsCash']=$resultsCash;

        if(!empty($resultsCash))
            $MaintenanceParticular=array_column($resultsCash, "MaintenanceParticular");
        else
            $MaintenanceParticular=array();

        $this->data['MaintenanceParticular']=$MaintenanceParticular;

        // $data_qry=mysqli_query($conn,"SELECT  `maint_headId`, `head_name` FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

        $mnt_hd_condtnArr['`head_status`']="Active";
        $mnt_hd_orderByArr['`srid`']="ASC";

        $query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="`maint_headId`, `head_name`", $mnt_hd_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $mnt_hd_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $headers_view=$query['userdata'];

        $this->data['headers_view']=$headers_view;

        $query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="`soc_name`, `soc_register_no`, `soc_address`, `bill_note`", $soc_condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $soc_details=$query['userdata'];

        $this->data['soc_details']=$soc_details;        

        if($this->form_validation->run() == FALSE)
        {
            $this->data['view']="report/collection_register";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
            $this->data['view']="report/collection_register";
            $this->load->view('layouts/layout/main_layout', $this->data);
        }
    }
}

?>