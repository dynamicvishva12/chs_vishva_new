<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedular extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='SCHEDULAR';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->notices_tbl=$socTables['notices_tbl'];
		$this->s_r_event_tbl=$socTables['s_r_event_tbl'];
		$this->schedular_tbl=$socTables['schedular_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Schedular';
    }

	public function home()
	{
		$breadcrumbArr[0]['name']="Schedular";
    	$breadcrumbArr[0]['link']=base_url()."schedular/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Schedular List";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$schdate = date("Y-m-d");
		// $fetch=mysqli_query($conn,"SELECT * FROM `notices` WHERE `no_expired_at` >= '$schdate' AND `Sheduled`='not scheduled' ");

		$notices_condtnArr['notices.no_expired_at >= ']=$schdate;
		$notices_condtnArr['notices.Sheduled']='not scheduled';

		$query=$this->Mcommon->getRecords($tableName=$this->notices_tbl, $colNames="notices.no_id, notices.notice_header", $notices_condtnArr, $likeCondtnArr=array(), $notices_joinArr=array(), $singleRow=FALSE, $notices_orderByArr=array(), $notices_groupByArr=array(), $notices_whereInArray=array(), $notices_customWhereArray=array(), $backTicks=TRUE);

		$notices_arr=$query['userdata'];

		$this->data['notices_arr']=$notices_arr;

		$schdate = date("Y-m-d");
		// $fetch=mysqli_query($conn,"SELECT * FROM `s-r-event` WHERE `ev_created` >= '$schdate' AND `Sheduled`='not scheduled'");

		$ev_condtnArr['s-r-event.ev_created >= ']=$schdate;
		$ev_condtnArr['s-r-event.Sheduled']='not scheduled';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_event_tbl, $colNames="s-r-event.ev_id, s-r-event.ev_name", $ev_condtnArr, $likeCondtnArr=array(), $ev_joinArr=array(), $singleRow=FALSE, $ev_orderByArr=array(), $ev_groupByArr=array(), $ev_whereInArray=array(), $ev_customWhereArray=array(), $backTicks=TRUE);

		$event_arr=$query['userdata'];

		$this->data['event_arr']=$event_arr;

		// $fetch= mysqli_query($conn,"SELECT * FROM ((`shedular` LEFT JOIN `s-r-event` ON `s-r-event`.`ev_id`=`shedular`.`shedular_fid`) LEFT JOIN `notices` ON `notices`.`no_id`=`shedular`.`shedular_fid`) ORDER BY `shedular_date` DESC");
		// while ($nomie=mysqli_fetch_assoc($fetch)) {

		$sch_orderByArr['schedular.schedular_date']='DESC';

		$sch_joinArr[]=array("tbl"=>$this->s_r_event_tbl, "condtn"=>"s-r-event.ev_id=schedular.schedular_fid", "type"=>"left");
		$sch_joinArr[]=array("tbl"=>$this->notices_tbl, "condtn"=>"notices.no_id=schedular.schedular_fid", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->schedular_tbl, $colNames="schedular.sr_id, schedular.schedular_for, schedular.schedular_date, schedular.schedular_close, s-r-event.ev_name, notices.notice_header", $sch_condtnArr=array(), $likeCondtnArr=array(), $sch_joinArr, $singleRow=FALSE, $sch_orderByArr, $sch_groupByArr=array(), $sch_whereInArray=array(), $sch_customWhereArray=array(), $backTicks=TRUE);

		$schedular_arr=$query['userdata'];

		$this->data['schedular_arr']=$schedular_arr;

		$this->data['view']="schedular/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_schedule()
	{
		$this->db->trans_start();

		$schedular_for=$this->input->post('schedular_for');
		$sdate=date("Y-m-d",strtotime($this->input->post('sdate')));

		$sched_id="";
		if($schedular_for=='Notice') 
		{
			$sched_id=$this->input->post('schedulr_nid');
			// $Nupdate=mysqli_query($conn,"UPDATE `notices` SET `no_created_at`='$sdate',`Sheduled`='scheduled' WHERE `no_id`='$sched_id'")or die("error...".mysqli_error($conn));

			$noticeUpdateArr=array(
				'no_created_at'=>$sdate,
				'Sheduled'=>'scheduled',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$noticeUpdtCondtnArr['notices.no_id']=$sched_id;

			$query=$this->Mcommon->update($tableName=$this->notices_tbl, $noticeUpdateArr, $noticeUpdtCondtnArr, $likeCondtnArr=array());
		}

		if($schedular_for=='Event') 
		{
			$sched_id=$this->input->post('schedulr_eid');
			// $Nupdate=mysqli_query($conn,"UPDATE `s-r-event` SET `ev_created`='$sdate',`sheduled`='scheduled' WHERE `ev_id`='$sched_id'")or die("error...".mysqli_error($conn));

			$evUpdateArr=array(
				'ev_created'=>$sdate,
				'sheduled'=>'scheduled',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$evUpdtCondtnArr['s-r-event.ev_id']=$sched_id;

			$query=$this->Mcommon->update($tableName=$this->s_r_event_tbl, $evUpdateArr, $evUpdtCondtnArr, $likeCondtnArr=array());
		}

		$f_time = $this->input->post('f_time');
		$t_time = $this->input->post('f_time');

		// $fire=mysqli_query($conn,"INSERT INTO `shedular` (`schedular_for`, `shedular_date`, `shedular_time`, `shedular_time1`, `shedular_close`,`shedular_fid`) VALUES ('$schedular_for', '$sdate', '$f_time', '$t_time', 'open','$sched_id')")or die("error...".mysqli_error($conn));

		$schInsertArr[]=array(
			'schedular_for'=>$schedular_for,
			'schedular_date'=>$sdate,
			// 'schedular_time'=>$f_time,
			// 'schedular_time1'=>$t_time,
			'schedular_close'=>'open',
			'schedular_fid'=>$sched_id,
			'createdBy'=>$this->user_name,
			'createdDatetime'=>$this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->schedular_tbl, $schInsertArr, $returnType="");

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Schedule not added.');
			redirect(base_url()."schedular/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Schedule has been successfully added.');
			redirect(base_url()."schedular/home", 'refresh');
		}     
	}

	public function delete_schedule()
	{
		$this->db->trans_start();

		$idd=$this->uri->segment('3');

		// $sch=mysqli_query($conn,"SELECT * FROM `shedular` WHERE `shedular`.`sr_id` = '$idd'");
		// $csch=mysqli_fetch_assoc($sch);

		$sch_condtnArr['schedular.sr_id']=$idd;

		$query=$this->Mcommon->getRecords($tableName=$this->schedular_tbl, $colNames="schedular.schedular_for, schedular.schedular_fid", $sch_condtnArr, $likeCondtnArr=array(), $sch_joinArr=array(), $singleRow=TRUE, $sch_orderByArr=array(), $sch_groupByArr=array(), $sch_whereInArray=array(), $sch_customWhereArray=array(), $backTicks=TRUE);

		$csch=$query['userdata'];

		if ($csch['schedular_for']=='Notice') 
		{
			$sched_id=$csch['schedular_fid'];
			// $Eupdate=mysqli_query($conn,"UPDATE `notices` SET `Sheduled`='not scheduled' WHERE `no_id`='$sched_id'")or die("error...".mysqli_error($conn));

			$noticeUpdateArr=array(
				'Sheduled'=>'not scheduled',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$noticeUpdtCondtnArr['notices.no_id']=$sched_id;

			$query=$this->Mcommon->update($tableName=$this->notices_tbl, $noticeUpdateArr, $noticeUpdtCondtnArr, $likeCondtnArr=array());
		}

		if ($csch['schedular_for']=='Event') 
		{
			$sched_id=$csch['schedular_fid'];
			// $Nupdate=mysqli_query($conn,"UPDATE `s-r-event` SET `sheduled`='not scheduled' WHERE `ev_id`='$sched_id'")or die("error...".mysqli_error($conn));

			$evUpdateArr=array(
				'ev_created'=>$sdate,
				'sheduled'=>'not scheduled',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$evUpdtCondtnArr['s-r-event.ev_id']=$sched_id;

			$query=$this->Mcommon->update($tableName=$this->s_r_event_tbl, $evUpdateArr, $evUpdtCondtnArr, $likeCondtnArr=array());
		}

		// $deleteq = mysqli_query($conn,"DELETE FROM `shedular` WHERE `sr_id`='$idd'");

		$delSchcondtnArr['schedular.sr_id']=$idd;

		$query=$this->Mcommon->delete($tableName=$this->schedular_tbl, $delSchcondtnArr, $likeCondtnArr=array(), $whereInArray=array());

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Schedule not deleted.');
			redirect(base_url()."schedular/home", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Schedule deleted successfully');
			redirect(base_url()."schedular/home", 'refresh');
		}   
	}
}
?>