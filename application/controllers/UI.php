<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UI extends CI_Controller
{
    public function __construct()
    {
         parent::__construct();

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';																			
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';
    }

    public function index()
    {
    	$this->load->view('layouts/layout/main_layout', $this->data);
    }
}
?>