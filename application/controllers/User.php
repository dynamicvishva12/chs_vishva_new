<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='USER';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->vechical_tbl=$socTables['vechical_tbl'];
		$this->family_tbl=$socTables['family_tbl'];
		$this->staff_tbl=$socTables['staff_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='User';
    }

    public function user_profile()
    {
    	$id=$this->uri->segment('3');

    	$breadcrumbArr[0]['name']="User";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="User Profile"; 
    	$breadcrumbArr[1]['link']=base_url()."user/user_profile/".$id;
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	// $fetch=mysqli_query($conn,"SELECT * FROM `s-r-user` WHERE `s-r-username`='$id'");
    	$user_condtnArr['s-r-user.s-r-username']=$id;

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-profile, s-r-user.s-r-fname, s-r-user.no-of-family-member, s-r-user.s-r-lname, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.s-r-appartment, s-r-user.s-r-wing, s-r-user.s-r-flat, s-r-user.s-r-bod, s-r-user.s-r-language, s-r-user.s-r-blood", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr=array(), $singleRow=TRUE, $user_orderByArr=array(), $user_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userdetails=$query['userdata'];

		$this->data['userdetails']=$userdetails;

		// $runrows = mysqli_query($conn,"SELECT * FROM `family` WHERE `user_name` ='$id' AND `active`='active'")or die("error".mysqli_error($conn));

		$fmly_condtnArr['family.user_name']=$id;
		$fmly_condtnArr['family.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->family_tbl, $colNames="family.relation, family.name, family.relation, family.blood_type, family.dob, family.contact", $fmly_condtnArr, $likeCondtnArr=array(), $fmly_joinArr=array(), $singleRow=FALSE, $fmly_orderByArr=array(), $fmly_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userfamily=$query['userdata'];

		$this->data['userfamily']=$userfamily;

		// $runrows = mysqli_query($conn,"SELECT * FROM `staff` WHERE `user_name` ='$id' AND `active`='active'")or die("error".mysqli_error($conn));

		$staff_condtnArr['staff.user_name']=$id;
		$staff_condtnArr['staff.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->staff_tbl, $colNames="staff.name, staff.age, staff.relation, staff.address, staff.contact, staff.st_doc", $staff_condtnArr, $likeCondtnArr=array(), $staff_joinArr=array(), $singleRow=FALSE, $staff_orderByArr=array(), $staff_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userstaff=$query['userdata'];

		$this->data['userstaff']=$userstaff;

		$vch_condtnArr['vechical.user_name']=$id;
		$vch_condtnArr['vechical.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->vechical_tbl, $colNames="vechical.v_id, vechical.v-type, vechical.make_model, vechical.v_numplate, vechical.v-park", $vch_condtnArr, $likeCondtnArr=array(), $vch_joinArr=array(), $singleRow=FALSE, $vch_orderByArr=array(), $vch_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$uservehical=$query['userdata'];

		$this->data['uservehical']=$uservehical;
		
		$this->data['view']="user/user_profile";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }
}

?>