<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='VENDOR';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->soc_vendor_tbl=$socTables['soc_vendor_tbl'];
		$this->society_vendor_list_tbl=$socTables['society_vendor_list_tbl'];
		$this->vendor_tbl=$socTables['vendor_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Vendor';
    }

	public function society_vendor()
	{
		$breadcrumbArr[0]['name']="Vendor";
    	$breadcrumbArr[0]['link']=base_url()."vendor/society_vendor";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Society Vendor";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$soc_vnd_condtnArr['soc_vendor.vendor_status']='active';
		$soc_vnd_orderByArr['soc_vendor.id']='DESC';

		$query=$this->Mcommon->getRecords($tableName=$this->soc_vendor_tbl, $colNames="soc_vendor.id AS vendorId, soc_vendor.vendor_type, DATE_FORMAT(soc_vendor.contract_start,'%d %b, %Y') As contract_start, DATE_FORMAT(soc_vendor.contract_end,'%d %b, %Y') As contract_end, soc_vendor.name AS vendoName, soc_vendor.address AS vendorAddress, soc_vendor.mobile_no, soc_vendor.charges", $soc_vnd_condtnArr, $likeCondtnArr=array(), $soc_vnd_joinArr=array(), $singleRow=FALSE, $soc_vnd_orderByArr, $soc_vnd_groupByArr=array(), $soc_vnd_whereInArray=array(), $soc_vnd_customWhereArray=array(), $backTicks=TRUE);

		$soc_vnd_arr=$query['userdata'];

		$this->data['soc_vnd_arr']=$soc_vnd_arr;

		$this->data['view']="vendor/society_vendor";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_society_vendor()
	{
		$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('vendor_Sdate', 'Start Date', 'trim|required');
		$this->form_validation->set_rules('vendor_Edate', 'End Date', 'trim|required');
		$this->form_validation->set_rules('vendor_type', 'Vendor Type', 'trim|required');
		$this->form_validation->set_rules('vendor_name', 'Vendor Name', 'trim|required');
		$this->form_validation->set_rules('vendor_address', 'Vendor Address', 'trim|required');
		$this->form_validation->set_rules('vendor_mobile', 'Vendor Mobile', 'trim|required|exact_length[10]|numeric');
		$this->form_validation->set_rules('charges', 'Charges', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE)
		{
			// $this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			// redirect(base_url()."vendor/society_vendor", 'refresh');
			$this->society_vendor();
		}
		else
		{
			$this->db->trans_start();

			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
			    	$vendor_Sdate= date("Y-m-d",strtotime($this->input->post('vendor_Sdate')));
					$vendor_Edate= date("Y-m-d",strtotime($this->input->post('vendor_Edate')));

					$vendor_type=$this->input->post('vendor_type');
					$vendor_name=$this->input->post('vendor_name');
					$vendor_address=$this->input->post('vendor_address');
					$vendor_mobile=$this->input->post('vendor_mobile');
					$charges=$this->input->post('charges');

					$soc_vnd_InsertArr[]=array(
						'vendor_type'=>$vendor_type,
						'contract_start'=>$vendor_Sdate,
						'contract_end'=>$vendor_Edate,
						'name'=>$vendor_name,
						'address'=>$vendor_address,
						'mobile_no'=>$vendor_mobile,
						'charges'=>$charges,
						'date_creation'=>$this->curr_datetime,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->soc_vendor_tbl, $soc_vnd_InsertArr, $returnType="");

					$soc_vnd_InsertStatus=$query['status'];

					if($soc_vnd_InsertStatus==FALSE)
					{
						$response='Society vendor not added';
						$this->session->set_flashdata('error_msg', 'Something went wrong! Society vendor has not added.');
					}
					else
					{
						$response='Society vendor added';
						$this->session->set_flashdata('success_msg', 'Society vendor has been added successfully');
						$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!');
				redirect(base_url()."vendor/society_vendor", 'refresh');
			}
			else
			{
				redirect(base_url()."vendor/society_vendor", 'refresh');
			}   
		}
	}

	public function delete_society_vendor()
	{
		$this->db->trans_start();

		$vendorId=$this->uri->segment('3');

		$soc_vnd_UpdateArr=array(
			'vendor_status'=>'inactive',
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['soc_vendor.id']=$vendorId;

		$query=$this->Mcommon->update($tableName=$this->soc_vendor_tbl, $soc_vnd_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
              
		if($query['status']==TRUE)
		{
			$response='Society Vendor Removed';
			$this->session->set_flashdata('success_msg', 'Society vendor has been removed successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
		}
		else
		{
			$response='Society Vendor Not Removed';
			$this->session->set_flashdata('error_msg', 'Something went wrong! Society vendor has not Removed.');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!');
			redirect(base_url()."vendor/society_vendor", 'refresh');
		}
		else
		{
			redirect(base_url()."vendor/society_vendor", 'refresh');
		}     
	}

	public function residential_vendor()
	{
		$breadcrumbArr[0]['name']="Vendor";
    	$breadcrumbArr[0]['link']=base_url()."vendor/residential_vendor";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Residential Vendor";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$res_vnd_condtnArr['vendor.bazar_status']='active';
		$res_vnd_orderByArr['vendor.d-id']='DESC';

		$res_vnd_joinArr[]=array("tbl"=>$this->society_vendor_list_tbl, "condtn"=>"vendor.dir-name=society_vendor_list.vendorId", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->vendor_tbl, $colNames="vendor.d-id AS venId, society_vendor_list.vendorName AS venType, vendor.div-add AS venAdd, vendor.dir-timming AS venTime, vendor.dir-contact AS venContact, vendor.dir-service As venService, vendor.dir-visit AS venCharges, vendor.dir-person AS venOwner", $res_vnd_condtnArr, $likeCondtnArr=array(), $res_vnd_joinArr, $singleRow=FALSE, $res_vnd_orderByArr, $res_vnd_groupByArr=array(), $res_vnd_whereInArray=array(), $res_vnd_customWhereArray=array(), $backTicks=TRUE);

		$res_vnd_arr=$query['userdata'];

		$this->data['res_vnd_arr']=$res_vnd_arr;

		$query=$this->Mcommon->getRecords($tableName=$this->society_vendor_list_tbl, $colNames="society_vendor_list.vendorId AS vendorId, society_vendor_list.vendorName AS vendorType , society_vendor_list.vendor_pic AS vendorPic", $res_vnd_ty_condtnArr=array(), $likeCondtnArr=array(), $res_vnd_ty_joinArr=array(), $singleRow=FALSE, $res_vnd_ty_orderByArr=array(), $res_vnd_ty_groupByArr=array(), $res_vnd_ty_whereInArray=array(), $res_vnd_ty_customWhereArray=array(), $backTicks=TRUE);

		$res_vnd_ty_arr=$query['userdata'];

		$this->data['res_vnd_ty_arr']=$res_vnd_ty_arr;

		$this->data['view']="vendor/residential_vendor";
		$this->load->view('layouts/layout/main_layout', $this->data);
	}

	public function add_residential_vendor()
	{
		$this->form_validation->set_rules('society_key', 'Society Key', 'trim|required');
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('ventype', 'Vendor Type', 'trim|required');
		$this->form_validation->set_rules('venname', 'Vendor Name', 'trim|required');
		$this->form_validation->set_rules('venfrom', 'From Time', 'trim|required');
		$this->form_validation->set_rules('vento', 'To Time', 'trim|required');
		$this->form_validation->set_rules('venmobile', 'Vendor Mobile', 'trim|required|exact_length[10]|numeric');
		$this->form_validation->set_rules('venservice', 'Vendor Service', 'trim|required');
		$this->form_validation->set_rules('vencharge', 'Service/Visiting Charge', 'trim|required|numeric');

		if($this->form_validation->run() == FALSE)
		{
			// $this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
			// redirect(base_url()."vendor/residential_vendor", 'refresh');
			$this->residential_vendor();
		}
		else
		{
			$this->db->trans_start();

			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];

			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

	    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

			    if(empty($uservalidate))
			    {
	                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
	                redirect(base_url()."auth/logout", 'refresh');
			    }
			    else
			    {
					$ventype=$this->input->post('ventype');
					$venname=$this->input->post('venname');
					$venAddress=$this->input->post('venAddress');
					$venfrom=date('h:i A', strtotime($this->input->post('venfrom')));
					$vento=date('h:i A', strtotime($this->input->post('vento')));
					$venmobile=$this->input->post('venmobile');
					$venservice=$this->input->post('venservice');
					$vencharge=$this->input->post('vencharge');

					$vendor_timing=$venfrom." - ".$vento;

            		$vendorInsertArr[]=array(
						'dir-name'=>$ventype,
						'div-add'=>$venAddress,
						'dir-timming'=>$vendor_timing,
						'dir-contact'=>$venmobile,
						'dir-service'=>$venservice,
						'dir-visit'=>$vencharge,
						'dir-person'=>$venname,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->vendor_tbl, $vendorInsertArr, $returnType="");

					$vendorInsertStatus=$query['status'];

					if($vendorInsertStatus==TRUE)
					{
						$response='Residential Vendor Added';
						$this->session->set_flashdata('success_msg', 'Residential vendor has been added successfully');
						$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
					}
					else
					{
						$response='Residential Vendor Not Added';
						$this->session->set_flashdata('error_msg', 'Something went wrong! Residential vendor has not added.');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!');
				redirect(base_url()."vendor/residential_vendor", 'refresh');
			}
			else
			{
				redirect(base_url()."vendor/residential_vendor", 'refresh');
			} 
		}   
	}

	public function delete_residential_vendor()
	{
		$this->db->trans_start();

		$vendorId=$this->uri->segment('3');

		$res_vnd_UpdateArr=array(
			'bazar_status'=>'inactive',
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['vendor.d-id']=$vendorId;

		$query=$this->Mcommon->update($tableName=$this->vendor_tbl, $res_vnd_UpdateArr, $updt_condtnArr, $likeCondtnArr=array());
              
		if($query['status']==TRUE)
		{
			$response='Residential Vendor Removed';
			$this->session->set_flashdata('success_msg', 'Residential vendor has been removed successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
		}
		else
		{
			$response='Residential Vendor Not Removed';
			$this->session->set_flashdata('error_msg', 'Something went wrong! Residential vendor has not Removed.');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!');
			redirect(base_url()."vendor/residential_vendor", 'refresh');
		}
		else
		{
			redirect(base_url()."vendor/residential_vendor", 'refresh');
		}     
	}
}
