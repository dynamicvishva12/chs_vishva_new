<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expected_visitors extends Gatekeeper_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='Expected Visitors';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->visitor_type_tbl=$socTables['visitor_type_tbl'];
		$this->expectedvisitors_tbl=$socTables['expectedvisitors_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Gatekeeper';
    }

    public function list()
    {
    	$today_date = date('Y-m-d');

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Expected Visitors";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/expected_visitors/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$vstr_condtnArr['expectedvisitors.status']="Active";

    	$vstr_joinArr[]=array("tbl"=>$this->visitor_type_tbl, "condtn"=>"visitor_type.type_id = expectedvisitors.visitor_type", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->expectedvisitors_tbl, $colNames="expectedvisitors.visitor_id, expectedvisitors.name, expectedvisitors.no_person, expectedvisitors.date_visit, expectedvisitors.from_time, expectedvisitors.visitor_type, expectedvisitors.reason, expectedvisitors.visitor_contact, expectedvisitors.date_birth, expectedvisitors.gender, expectedvisitors.visitor_loc, expectedvisitors.vis_email, expectedvisitors.vis_vehicalno, expectedvisitors.reason, visitor_type.vistor_type", $vstr_condtnArr, $vstr_likeCondtnArr=array(), $vstr_joinArr, $vstr_singleRow=FALSE, $vstr_orderByArr=array(), $vstr_groupByArr=array(), $vstr_whereInArray=array(), $vstr_customWhereArray=array(), $vstr_backTicks=TRUE);

		$visitors_data=$query['userdata'];

		$this->data['visitors_data']=$visitors_data;

   		$this->data['view']="gatekeeper/expected_visitors/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }    

    public function edit_visitor()
    {
    	$this->data['visitorId']=$visitorId=$this->uri->segment('4');
    	$this->data['visitor_type']=$visitor_type=$this->uri->segment('5');
    	$result="";

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Visitors";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/visitors/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Visitor"; 
    	$breadcrumbArr[2]['link']=base_url()."gatekeeper/visitors/edit_visitor/".$visitorId."/".$visitor_type;
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$vstr_condtnArr['expectedvisitors.visitor_id']=$visitorId;
    	$vstr_condtnArr['expectedvisitors.status']="Active";

		$query=$this->Mcommon->getRecords($tableName=$this->expectedvisitors_tbl, $colNames="expectedvisitors.visitor_id, expectedvisitors.name, expectedvisitors.no_person, expectedvisitors.date_visit, expectedvisitors.from_time, expectedvisitors.visitor_type, expectedvisitors.reason, expectedvisitors.visitor_contact, expectedvisitors.date_birth, expectedvisitors.gender, expectedvisitors.visitor_loc, expectedvisitors.vis_email, expectedvisitors.vis_vehicalno, expectedvisitors.reason", $vstr_condtnArr, $vstr_likeCondtnArr=array(), $vstr_joinArr=array(), $vstr_singleRow=TRUE, $vstr_orderByArr=array(), $vstr_groupByArr=array(), $vstr_whereInArray=array(), $vstr_customWhereArray=array(), $vstr_backTicks=TRUE);

		$visitors_data=$query['userdata'];

		$this->data['visitors_data']=$visitors_data;

		$vstrTy_condtnArr['visitor_type.status']="active";

		$query=$this->Mcommon->getRecords($tableName=$this->visitor_type_tbl, $colNames="visitor_type.type_id, visitor_type.vistor_type", $vstrTy_condtnArr, $vstrTy_likeCondtnArr=array(), $vstrTy_joinArr=array(), $vstrTy_singleRow=FALSE, $vstrTy_orderByArr=array(), $vstrTy_groupByArr=array(), $vstrTy_whereInArray=array(), $vstrTy_customWhereArray=array(), $vstrTy_backTicks=TRUE);

		$visitorTypeArr=$query['userdata'];

		$this->data['visitorTypeArr']=$visitorTypeArr;

    	$this->form_validation->set_rules('visitorId', 'Visitor ID', 'trim|required');

    	if($this->form_validation->run() == FALSE)
        {		
			$this->data['view']="gatekeeper/expected_visitors/edit_expected_visitor";
			$this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
        	$this->db->trans_start();

        	$visitor_Id=$this->input->post('visitorId');
        	$name=$this->input->post('name');
        	$gender=$this->input->post('gender');
        	$visitor_loc=$this->input->post('visitor_loc');
        	$no_person=$this->input->post('no_person');
        	$visitor_contact=$this->input->post('visitor_contact');
        	$vis_email=$this->input->post('vis_email');
        	$visit_type=$this->input->post('visit_type');
        	$reason=$this->input->post('reason');
        	$vis_vehicalno=$this->input->post('vis_vehicalno');

			$updateArr=array(
				'name'=>$name, 
				'gender'=>$gender, 
				'visitor_loc'=>$visitor_loc, 
				'no_person'=>$no_person, 
				'visitor_contact'=>$visitor_contact, 
				'vis_email'=>$vis_email, 
				'visitor_type'=>$visit_type, 
				'reason'=>$reason, 
				'vis_vehicalno'=>$vis_vehicalno, 
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['expectedvisitors.visitor_id']=$visitor_Id;

			$query=$this->Mcommon->update($tableName=$this->expectedvisitors_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Visitor has not updated.');
				redirect(base_url()."gatekeeper/expected_visitors/list", 'refresh');
			}
			else
			{
				$result="Expected Visitor updated successfully";
	    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
				$this->session->set_flashdata('success_msg', 'Visitor has been updated sucessfully');
				redirect(base_url()."gatekeeper/expected_visitors/list", 'refresh');
			}
		}
    }

    public function delete_visitor()
    {
    	$visitorId=$this->uri->segment('4');

		$updateArr=array(
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['expectedvisitors.visitor_id']=$visitorId;

		$query=$this->Mcommon->update($tableName=$this->expectedvisitors_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$result=$query['status'];

		if($result==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Expected Visitor has not removed.');
			redirect(base_url()."gatekeeper/expected_visitors/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Expected Visitor has removed');
			redirect(base_url()."gatekeeper/expected_visitors/list", 'refresh');
		}
    }
}

?>