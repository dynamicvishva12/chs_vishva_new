<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gatekeeper_society_staff extends Gatekeeper_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='SOCIETY_STAFF';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->society_staff_tbl=$socTables['society_staff_tbl'];
		$this->staff_entry_tbl=$socTables['staff_entry_tbl'];
		$this->staff_type_tbl=$socTables['staff_type_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Gatekeeper';
    }

    public function list()
    {
    	$today_date = date('Y-m-d');

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Society Staff";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/gatekeeper_society_staff/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$soc_condtnArr['soc_staff.status']="Active";

    	$soc_joinArr[]=array("tbl"=>$this->staff_type_tbl, "condtn"=>"staff_type_tbl.id = soc_staff.vendor_type", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->society_staff_tbl, $colNames="soc_staff.staff_id, soc_staff.vendor_type, soc_staff.name, soc_staff.address, soc_staff.mobile_no, soc_staff.Emergency_contact, CONCAT('".$this->assets_url."IMAGESDRIVE/gatekeeper_society_staff/', soc_staff.staff_pic) As staff_pic, staff_type_tbl.staff_type_name", $soc_condtnArr, $soc_likeCondtnArr=array(), $soc_joinArr, $soc_singleRow=FALSE, $soc_orderByArr=array(), $soc_groupByArr=array(), $soc_whereInArray=array(), $soc_customWhereArray=array(), $soc_backTicks=TRUE);

		$soc_staff_data=$query['userdata'];

		$this->data['soc_staff_data']=$soc_staff_data;

		$socty_condtnArr['staff_type_tbl.status']="Active";

		$query=$this->Mcommon->getRecords($tableName=$this->staff_type_tbl, $colNames="staff_type_tbl.id, staff_type_tbl.staff_type_name", $socty_condtnArr, $socty_likeCondtnArr=array(), $socty_joinArr=array(), $socty_singleRow=FALSE, $socty_orderByArr=array(), $socty_groupByArr=array(), $socty_whereInArray=array(), $socty_customWhereArray=array(), $socty_backTicks=TRUE);

		$soc_staff_type_data=$query['userdata'];

		$this->data['soc_staff_type_data']=$soc_staff_type_data;

   		$this->data['view']="gatekeeper/gatekeeper_society_staff/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }    

    public function add_staff()
    {
    	$this->db->trans_start();

    	$name=$this->input->post('name');
    	$contact_no=$this->input->post('contact_no');
    	$emergency_contact_no=$this->input->post('emergency_contact_no');
    	$staff_type=$this->input->post('staff_type');
    	$address=$this->input->post('address');
    	$post_image= $_FILES['staff_img']['name'];
		$t_tmp=$_FILES['staff_img']['tmp_name'];
		$temp = explode(".", $_FILES["staff_img"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
		$store=FCPATH."assets/IMAGESDRIVE/gatekeeper_society_staff/".$newfilename;

		if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg")
		{
			move_uploaded_file($t_tmp, $store);

			$soc_condtnArr['soc_staff.status']="Active";

			$query=$this->Mcommon->getRecords($tableName=$this->society_staff_tbl, $colNames="soc_staff.staff_id", $soc_condtnArr, $soc_likeCondtnArr=array(), $soc_joinArr=array(), $soc_singleRow=TRUE, $soc_orderByArr=array(), $soc_groupByArr=array(), $soc_whereInArray=array(), $soc_customWhereArray=array(), $soc_backTicks=TRUE);

			$soc_staff_data=$query['userdata'];

			$newStaffId=1;

			if(!empty($soc_staff_data))
				$newStaffId=$soc_staff_data['staff_id']+1;

			$insertArr[]=array(
				'staff_id' => $newStaffId,
				'name' => $name,
				'mobile_no' => $contact_no,
				'Emergency_contact' => $emergency_contact_no,
				'vendor_type' => $staff_type,
				'address' => $address,
				'staff_pic' => $newfilename,
				'createdBy' => $this->user_name,
				'createdDatetime' => $this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->society_staff_tbl, $insertArr, $returnType="");

			$insertStatus=$query['status'];

			if($insertStatus==FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff has not added.');
			}
			else
			{
				$result="Society staff has been added successfully";
				$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
				$this->session->set_flashdata('success_msg', 'Society staff has been added successfully');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Plz Upload only Image even gif also not Accecptable');
			redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!!!');
			redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
		}
		else
		{
			redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
		}
    }

    public function edit_staff()
    {
    	$this->data['staffId']=$staffId=$this->uri->segment('4');
    	$result="";

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Society Staff";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/gatekeeper_society_staff/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Society Staff"; 
    	$breadcrumbArr[2]['link']=base_url()."gatekeeper/gatekeeper_society_staff/edit_staff/".$staffId;
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$soc_condtnArr['soc_staff.staff_id']=$staffId;
    	$soc_condtnArr['soc_staff.status']="Active";

		$query=$this->Mcommon->getRecords($tableName=$this->society_staff_tbl, $colNames="soc_staff.staff_id, soc_staff.vendor_type, soc_staff.name, soc_staff.address, soc_staff.mobile_no, soc_staff.Emergency_contact, CONCAT('".$this->assets_url."IMAGESDRIVE/gatekeeper_society_staff/', soc_staff.staff_pic) As staff_pic, soc_staff.address", $soc_condtnArr, $soc_likeCondtnArr=array(), $soc_joinArr=array(), $soc_singleRow=TRUE, $soc_orderByArr=array(), $soc_groupByArr=array(), $soc_whereInArray=array(), $soc_customWhereArray=array(), $soc_backTicks=TRUE);

		$soc_staff_data=$query['userdata'];

		$this->data['soc_staff_data']=$soc_staff_data;

		$socty_condtnArr['staff_type_tbl.status']="Active";

		$query=$this->Mcommon->getRecords($tableName=$this->staff_type_tbl, $colNames="staff_type_tbl.id, staff_type_tbl.staff_type_name", $socty_condtnArr, $socty_likeCondtnArr=array(), $socty_joinArr=array(), $socty_singleRow=FALSE, $socty_orderByArr=array(), $socty_groupByArr=array(), $socty_whereInArray=array(), $socty_customWhereArray=array(), $socty_backTicks=TRUE);

		$soc_staff_type_data=$query['userdata'];

		$this->data['soc_staff_type_data']=$soc_staff_type_data;

    	$this->form_validation->set_rules('staffId', 'Staff ID', 'trim|required');

    	if($this->form_validation->run() == FALSE)
        {		
			$this->data['view']="gatekeeper/gatekeeper_society_staff/edit_staff";
			$this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
        	$this->db->trans_start();

        	$staff_Id=$this->input->post('staffId');
        	$name=$this->input->post('name');
	    	$contact_no=$this->input->post('contact_no');
	    	$emergency_contact_no=$this->input->post('emergency_contact_no');
	    	$staff_type=$this->input->post('staff_type');
	    	$address=$this->input->post('address');
			$nameold=$this->input->post('nameold');
	    	$imaged_status=$this->input->post('image_status');

	    	if($imaged_status=="unchanged")
			{
				$newfilename=$nameold;
			}
			else
			{
				if(!empty($_FILES['staff_img']['name'])) 
				{
					$post_image= $_FILES['staff_img']['name'];
					$t_tmp=$_FILES['staff_img']['tmp_name'];
					$temp = explode(".", $_FILES["staff_img"]["name"]);
					$newfilename = round(microtime(true)) . '.' . end($temp);
					$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
					$store=FCPATH."assets/IMAGESDRIVE/gatekeeper_society_staff/".$newfilename;

					if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg")
					{
						move_uploaded_file($t_tmp, $store);
					}
					else
					{
						$this->session->set_flashdata('warning_msg', 'Plz Upload only Image even gif also not Accecptable');
						redirect(base_url()."gatekeeper/gatekeeper_society_staff/edit_staff/".$staffId, 'refresh');
					}
				}
				else
				{
					$newfilename=NULL;
					$imaged_status="Removed";
				}
			}

			if(!empty($newfilename) && ($imaged_status=="changed" || $imaged_status=="unchanged"))
			{
				$updateArr=array(
					'name' => $name,
					'mobile_no' => $contact_no,
					'Emergency_contact' => $emergency_contact_no,
					'vendor_type' => $staff_type,
					'address' => $address,
					'staff_pic' => $newfilename,
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);
			}
			else
			{
				$updateArr=array(
					'name' => $name,
					'mobile_no' => $contact_no,
					'Emergency_contact' => $emergency_contact_no,
					'vendor_type' => $staff_type,
					'address' => $address,
					'staff_pic' => NULL,
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);
			}

			$updt_condtnArr['soc_staff.staff_id']=$staff_Id;

			$query=$this->Mcommon->update($tableName=$this->society_staff_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

			$updateStatus=$query['status'];

			if($updateStatus==FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff has not updated.');
			}
			else
			{
				$result="Society staff has been updated successfully";
				$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
				$this->session->set_flashdata('success_msg', 'Society staff has been updated successfully');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff has not updated.');
				redirect(base_url()."gatekeeper/gatekeeper_society_staff/edit_staff/".$staffId, 'refresh');
			}
			else
			{
				$result="Society staff updated successfully";
	    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
				$this->session->set_flashdata('success_msg', 'Society staff has been updated sucessfully');
				redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
			}
		}
    }

    public function delete_staff()
    {
    	$staffId=$this->uri->segment('4');

		$updateArr=array(
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['soc_staff.staff_id']=$staffId;

		$query=$this->Mcommon->update($tableName=$this->society_staff_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$result=$query['status'];

		if($result==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff has not removed.');
			redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
		}
		else
		{
			$result="Society staff has removed";
	    	$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
			$this->session->set_flashdata('success_msg', 'Society staff has removed');
			redirect(base_url()."gatekeeper/gatekeeper_society_staff/list", 'refresh');
		}
    }
}

?>