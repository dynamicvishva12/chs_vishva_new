<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regular_visitors extends Gatekeeper_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='REGULAR_VISITORS';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->visitor_tbl=$socTables['visitor_tbl'];
		$this->late_mark_visitor_tbl=$socTables['late_mark_visitor_tbl'];
		$this->visitor_type_tbl=$socTables['visitor_type_tbl'];
		$this->visitor_entry_tbl=$socTables['visitor_entry_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Gatekeeper';
    }

    public function list()
    {
    	$today_date = date('Y-m-d');

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Visitors";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/regular_visitors/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$vstr_condtnArr['visitor_tbl.visitor_regular']="1";
    	$vstr_condtnArr['visitor_tbl.visitorStatus']="Active";

    	$vstr_joinArr[]=array("tbl"=>$this->visitor_type_tbl, "condtn"=>"visitor_type.type_id = visitor_tbl.visitor_type", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->visitor_tbl, $colNames="visitor_tbl.visitor_id, visitor_tbl.visitor_name, visitor_tbl.visitor_location, visitor_tbl.visitor_unit_no, visitor_tbl.visitor_total_person, visitor_tbl.visitor_contact, visitor_tbl.visitor_vehical, visitor_type.vistor_type", $vstr_condtnArr, $vstr_likeCondtnArr=array(), $vstr_joinArr, $vstr_singleRow=FALSE, $vstr_orderByArr=array(), $vstr_groupByArr=array(), $vstr_whereInArray=array(), $vstr_customWhereArray=array(), $vstr_backTicks=TRUE);

		$visitors_data=$query['userdata'];

		$this->data['visitors_data']=$visitors_data;

   		$this->data['view']="gatekeeper/regular_visitors/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }    

    public function edit_visitor()
    {
    	$this->data['visitorId']=$visitorId=$this->uri->segment('4');

    	$breadcrumbArr[0]['name']="Gatekeeper";
    	$breadcrumbArr[0]['link']="javascript:void(0)";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Visitors";
    	$breadcrumbArr[1]['link']=base_url()."gatekeeper/regular_visitors/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Visitor"; 
    	$breadcrumbArr[2]['link']=base_url()."gatekeeper/regular_visitors/edit_visitor/".$visitorId;
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$vstr_condtnArr['visitor_tbl.visitor_regular']="1";
    	$vstr_condtnArr['visitor_tbl.visitorStatus']="Active";

    	$vstr_joinArr[]=array("tbl"=>$this->visitor_type_tbl, "condtn"=>"visitor_type.type_id = visitor_tbl.visitor_type", "type"=>"left");
    	$vstr_joinArr[]=array("tbl"=>$this->visitor_entry_tbl, "condtn"=>"visitor_entry_tbl.visitor_id = visitor_tbl.visitor_id", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->visitor_tbl, $colNames="visitor_tbl.visitor_id, visitor_tbl.visitor_name, visitor_tbl.visitor_location, visitor_tbl.visitor_unit_no, visitor_tbl.visitor_total_person, visitor_tbl.visitor_contact, visitor_tbl.visitor_vehical, visitor_tbl.visitor_type, visitor_type.vistor_type", $vstr_condtnArr, $vstr_likeCondtnArr=array(), $vstr_joinArr, $vstr_singleRow=TRUE, $vstr_orderByArr=array(), $vstr_groupByArr=array(), $vstr_whereInArray=array(), $vstr_customWhereArray=array(), $vstr_backTicks=TRUE);

		$visitors_data=$query['userdata'];

		$this->data['visitors_data']=$visitors_data;

		$vstrTy_condtnArr['visitor_type.status']="active";

		$query=$this->Mcommon->getRecords($tableName=$this->visitor_type_tbl, $colNames="visitor_type.type_id, visitor_type.vistor_type", $vstrTy_condtnArr, $vstrTy_likeCondtnArr=array(), $vstrTy_joinArr=array(), $vstrTy_singleRow=FALSE, $vstrTy_orderByArr=array(), $vstrTy_groupByArr=array(), $vstrTy_whereInArray=array(), $vstrTy_customWhereArray=array(), $vstrTy_backTicks=TRUE);

		$visitorTypeArr=$query['userdata'];

		$this->data['visitorTypeArr']=$visitorTypeArr;

    	$this->form_validation->set_rules('visitorId', 'Visitor ID', 'trim|required');

    	if($this->form_validation->run() == FALSE)
        {		
			$this->data['view']="gatekeeper/regular_visitors/edit_visitor";
			$this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
        	$this->db->trans_start();

        	$visitor_Id=$this->input->post('visitorId');
        	$name=$this->input->post('name');
        	$location=$this->input->post('location');
        	$unit_number=$this->input->post('unit_number');
        	$total_person=$this->input->post('total_person');
        	$contact_no=$this->input->post('contact_no');
        	$visitor_type=$this->input->post('visit_type');
        	$vehicle=$this->input->post('vehicle');

	    	$updateArr=array(
				'visitor_name'=>$name, 
				'visitor_location'=>$location, 
				'visitor_unit_no'=>$unit_number, 
				'visitor_total_person'=>$total_person, 
				'visitor_contact'=>$contact_no, 
				'visitor_vehical'=>$vehicle, 
				'visitor_type'=>$visitor_type, 
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$updt_condtnArr['visitor_tbl.visitor_id']=$visitor_Id;

			$query=$this->Mcommon->update($tableName=$this->visitor_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Regular Visitor has not updated.');
				redirect(base_url()."gatekeeper/regular_visitors/list", 'refresh');
			}
			else
			{
				$result="Regular Visitor updated sucessfully";
	    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
				$this->session->set_flashdata('success_msg', 'Regular Visitor has been updated sucessfully');
				redirect(base_url()."gatekeeper/regular_visitors/list", 'refresh');
			}
		}
    }

    public function delete_visitor()
    {
    	$visitorId=$this->uri->segment('4');

    	$updateArr=array(
			'visitorStatus'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['visitor_tbl.visitor_id']=$visitorId;

		$query=$this->Mcommon->update($tableName=$this->visitor_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());
              
    	// if($value)
		if($query['status']==TRUE)
    	{
    		$result="Regular Visitor removed sucessfully";
    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
    	}

		if($query['status']==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Regular Visitor has not removed.');
			redirect(base_url()."gatekeeper/regular_visitors/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Regular Visitor has removed');
			redirect(base_url()."gatekeeper/regular_visitors/list", 'refresh');
		}
    }
}

?>