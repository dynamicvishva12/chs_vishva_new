<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='LEGAL';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->legal_tbl=$socTables['legal_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Legal';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Lien & Legal";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Legal"; 
    	$breadcrumbArr[1]['link']=base_url()."lien_legal/legal/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$usr_condtnArr['`s-r-user`.`s-r-approve`']='approve';
		$usr_condtnArr['`s-r-user`.`s-r-active`']='active';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="`s-r-fname` AS usrFname, `s-r-lname` AS usrLname, `s-r-username` AS usrId, `s-r-mobile` AS usrMobile, `s-r-appartment` AS usrAppartment, `s-r-wing` AS usrWing, `s-r-flat` AS usrFlat,`s-r-email` AS usrMail,(SELECT `d-body` FROM ".$this->directory_tbl." WHERE `important`=`s-r-user`.`s-r-email` AND `directory_type`='type1' AND `status`='Active' LIMIT 1) AS designation", $usr_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$memberArr=$query['userdata'];

		$this->data['memberArr']=$memberArr;

		// $query = mysqli_query($conn,"SELECT `sr_id`, `date`, `case_no`, `member_id`, `details`, CONCAT('http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/IMAGES/society_lin_legal/',`leg_doc`) AS `leg_doc`,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName FROM `legal` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `legal`.`member_id` WHERE `status`='Active' ORDER BY  `date_creation` DESC");

		$leg_condtnArr['`legal`.`status`']='Active';
		$leg_orderByArr['`legal`.`date_creation`']='DESC';

		$leg_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `legal`.`member_id`", "type"=>"left");

    	$query=$this->Mcommon->getRecords($tableName=$this->legal_tbl, $colNames="`sr_id`, `date`, `case_no`, `member_id`, `details`, CONCAT('".$this->assets_url."/IMAGESDRIVE/society_lin_legal/',`leg_doc`) AS `leg_doc`,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName", $leg_condtnArr, $likeCondtnArr=array(), $leg_joinArr, $singleRow=FALSE, $leg_orderByArr, $user_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$legalData=$query['userdata'];

		$this->data['legalData']=$legalData;
		
		$this->data['view']="lien_legal/legal/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function add_legal()
    {
    	$this->db->trans_start();

		$member = $this->input->post('member');
		$legal_caseno = $this->input->post('legal_caseno');
		$legal_date = $this->input->post('legal_date');
		$legal_detail = $this->input->post('legal_detail');
		$post_image= $_FILES['legal_myfile']['name'];
		$t_tmp=$_FILES['legal_myfile']['tmp_name'];
		$temp = explode(".", $_FILES["legal_myfile"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
		$store=FCPATH."assets/IMAGESDRIVE/society_lin_legal/".$newfilename;

		if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
		{
			move_uploaded_file($t_tmp, $store);

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($member) && !empty($newfilename) && !empty($legal_caseno) && !empty($legal_date) && !empty($legal_detail))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						$insertArr[]=array(
							'date' => $legal_date,
							'case_no' => $legal_caseno,
							'member_id' => $member,
							'details' => $legal_detail,
							'leg_doc' => $newfilename,
							'date_creation' => $this->curr_datetime,
							'createdBy' => $this->user_name,
							'createdDatetime' => $this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->legal_tbl, $insertArr, $returnType="");

						$insertStatus=$query['status'];

						if($insertStatus==FALSE)
						{
							$this->session->set_flashdata('error_msg', 'Something went wrong! Legal has not added.');
						}
						else
						{
							$result="Legal has been added successfully";
    						$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
							$this->session->set_flashdata('success_msg', 'Legal has been added successfully');
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
				redirect(base_url()."lien_legal/legal/list", 'refresh');
			}
		} 
		else
		{
			$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
			redirect(base_url()."lien_legal/legal/list", 'refresh');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
			redirect(base_url()."lien_legal/legal/list", 'refresh');
		}
		else
		{
			redirect(base_url()."lien_legal/legal/list", 'refresh');
		}   
    }

    public function edit_legal()
    {
    	$legalId = $this->uri->segment('4');

    	$breadcrumbArr[0]['name']="Lien & Legal";
    	$breadcrumbArr[0]['link']="javascript:void(0);";
    	$breadcrumbArr[0]['active']=TRUE;

    	$breadcrumbArr[1]['name']="Legal"; 
    	$breadcrumbArr[1]['link']=base_url()."lien_legal/legal/list";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[2]['name']="Edit Legal"; 
    	$breadcrumbArr[2]['link']=base_url()."lien_legal/legal/edit_legal/".$legalId;
    	$breadcrumbArr[2]['active']=FALSE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$usr_condtnArr['`s-r-user`.`s-r-approve`']='approve';
		$usr_condtnArr['`s-r-user`.`s-r-active`']='active';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="`s-r-fname` AS usrFname, `s-r-lname` AS usrLname, `s-r-username` AS usrId, `s-r-mobile` AS usrMobile, `s-r-appartment` AS usrAppartment, `s-r-wing` AS usrWing, `s-r-flat` AS usrFlat,`s-r-email` AS usrMail,(SELECT `d-body` FROM ".$this->directory_tbl." WHERE `important`=`s-r-user`.`s-r-email` AND `directory_type`='type1' AND `status`='Active' LIMIT 1) AS designation", $usr_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$memberArr=$query['userdata'];

		$this->data['memberArr']=$memberArr;

		// $query = mysqli_query($conn,"SELECT `sr_id`, `date`, `case_no`, `member_id`, `details`, CONCAT('http://dynamicvishva.in/CHS_Vishva_SocietyKey/chsworldadmin/IMAGES/society_lin_legal/',`leg_doc`) AS `leg_doc`,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName FROM `legal` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `legal`.`member_id` WHERE `status`='Active' ORDER BY  `date_creation` DESC");

		$leg_condtnArr['`legal`.`sr_id`']=$legalId;
		$leg_condtnArr['`legal`.`status`']='Active';
		$leg_orderByArr['`legal`.`date_creation`']='DESC';

		$leg_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `legal`.`member_id`", "type"=>"left");

    	$query=$this->Mcommon->getRecords($tableName=$this->legal_tbl, $colNames="`sr_id`, `date`, `case_no`, `member_id`, `details`, CONCAT('".$this->assets_url."/IMAGESDRIVE/society_lin_legal/',`leg_doc`) AS `leg_doc`,CONCAT(`s-r-fname`,' ',`s-r-lname`) AS fullName", $leg_condtnArr, $likeCondtnArr=array(), $leg_joinArr, $singleRow=TRUE, $leg_orderByArr, $user_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$legalData=$query['userdata'];

		$this->data['legalData']=$legalData;
		
		$this->form_validation->set_rules('legalId', 'Legal ID', 'trim|required');

    	if($this->form_validation->run() == FALSE)
        {		
			$this->data['view']="lien_legal/legal/edit_legal";
			$this->load->view('layouts/layout/main_layout', $this->data);
        }
        else
        {
	    	$this->db->trans_start();

			$legalId = $this->input->post('legalId');
			$member = $this->input->post('member');
			$legal_caseno = $this->input->post('legal_caseno');
			$legal_date = $this->input->post('legal_date');
			$legal_detail = $this->input->post('legal_detail');
			$image_status = $this->input->post('image_status');
			$post_image= $_FILES['legal_myfile']['name'];
			$t_tmp=$_FILES['legal_myfile']['tmp_name'];
			$temp = explode(".", $_FILES["legal_myfile"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
			$fileexe = strtolower(pathinfo($post_image,PATHINFO_EXTENSION));
			$store=FCPATH."assets/IMAGESDRIVE/society_lin_legal/".$newfilename;

			if(!empty($_FILES['legal_myfile']['name'])) 
			{
				$docName=$newfilename;
				if($fileexe=="png" || $fileexe=="jpg" || $fileexe=="jpeg" || $fileexe =="pdf")
				{
					if($post_image!="" && $image_status=='changed')
					{
						move_uploaded_file($t_tmp, $store);
					}
				} 
				else
				{
					$this->session->set_flashdata('warning_msg', 'Plz Upload only Image and pdf file even gif also not Accecptable');
					redirect(base_url()."lien_legal/legal/list", 'refresh');
				}
			}
			else
			{
				$docName=NULL;
			}

			if(!empty($this->society_key) && !empty($this->user_name) && !empty($member) && !empty($newfilename) && !empty($legal_caseno) && !empty($legal_date) && !empty($legal_detail))
			{
				$query=$this->Msociety->verify_society($this->society_key);

				$soc_check=$query['userdata'];

				if(!empty($soc_check))
				{
					$soc_key=$soc_check['society_key'];
					$soc_name=$soc_check['society_name'];

		    		$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

		    		$uservalidate=$query['userdata'];

				    if(empty($uservalidate))
				    {
		                $this->session->set_flashdata('warning_msg', 'User ID Invalid');
		                redirect(base_url()."auth/logout", 'refresh');
				    }
				    else
				    {
						if($post_image=="" && $image_status=='unchanged')
				    	{
				    		$legalUpdateArr=array(
								'date' => $legal_date,
								'case_no' => $legal_caseno,
								'member_id' => $member,
								'details' => $legal_detail,
								'updatedBy'=>$this->user_name,
								'updatedDatetime'=>$this->curr_datetime
							);
				    	}
				    	else
				    	{
				    		$legalUpdateArr=array(
								'date' => $legal_date,
								'case_no' => $legal_caseno,
								'member_id' => $member,
								'details' => $legal_detail,
								'leg_doc' => $docName,
								'updatedBy'=>$this->user_name,
								'updatedDatetime'=>$this->curr_datetime
							);
				    	}

						$legalUpdt_condtnArr['`legal`.`sr_id`']=$legalId;

						$query=$this->Mcommon->update($tableName=$this->legal_tbl, $legalUpdateArr, $legalUpdt_condtnArr, $likeCondtnArr=array());

						$updateStatus=$query['status'];

						if($updateStatus==FALSE)
						{
							$this->session->set_flashdata('error_msg', 'Something went wrong! Legal has not updated.');
						}
						else
						{
							$result="Legal has been updated successfully";
    						$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
							$this->session->set_flashdata('success_msg', 'Legal has been updated successfully');
						}
					}
				}
				else
				{
					$this->session->set_flashdata('warning_msg', 'Society key invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
				redirect(base_url()."lien_legal/legal/list", 'refresh');
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('warning_msg', 'Something went wrong!!!');
				redirect(base_url()."lien_legal/legal/list", 'refresh');
			}
			else
			{
				redirect(base_url()."lien_legal/legal/list", 'refresh');
			}   
		}
    }

    public function delete_legal()
    {
    	$leinId=$this->uri->segment('4');
    	$result="";	

    	// $value = mysqli_query($conn,"UPDATE `notices` SET `active`= 'Inactive' WHERE `no_id`='$noticeId'");

    	$updateArr=array(
			'status'=>'Inactive',
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$updt_condtnArr['legal.sr_id']=$leinId;

		$query=$this->Mcommon->update($tableName=$this->legal_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());
              
    	// if($value)
		if($query['status']==TRUE)
    	{
    		$result="Legal removed sucessfully";
    		$this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
    	}

		if(empty($result))
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Legal has not removed.');
			redirect(base_url()."lien_legal/legal/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Legal has removed');
			redirect(base_url()."lien_legal/legal/list", 'refresh');
		}
    }
}

?>