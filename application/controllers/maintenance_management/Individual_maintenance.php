<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Individual_maintenance extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='INDIVIDUAL_MAINTENANCE';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->maintenance_head_tbl=$socTables['maintenance_head_tbl'];
		$this->maintenance_head_charges_tbl=$socTables['maintenance_head_charges_tbl'];
		$this->member_maintenance_tbl=$socTables['member_maintenance_tbl'];
		$this->maintenance_tbl=$socTables['maintenance_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Individual Maintenance';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Maintenance Management";
    	$breadcrumbArr[0]['link']=base_url()."maintenance_management/individual_maintenance/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Individual Maintenance";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	
    	$condtnArr['maintenance_head.head_status']='Active';
    	$orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$headers_view=$query['userdata'];

		$this->data['headers_view']=$headers_view;


		$particularList=array();
		$result=array();
		// $data_qry=mysqli_query($conn, "SELECT * FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

		$user_orNotLikeArray['s-r-user.disabled_user']='disabled';
		$user_condtnArr['s-r-user.s-r-approve']='approve';

		$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, s-r-user.s-r-fname, s-r-user.s-r-lname", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr=array(), $user_groupByArr=array(), $user_whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $user_customOrWhereArray=array(), $user_orNotLikeArray);

		$user_info=$query['userdata'];

		if(!empty($user_info))
		{
			// while($no_user=mysqli_fetch_assoc($data_qry))
			foreach($user_info AS $no_user) 
			{
				$usrId=$no_user['s-r-username'];
				$userName=$no_user['s-r-fname']." ".$no_user['s-r-lname'];
				$particularList['usrId']=$usrId;  
				$particularList['userName']=$userName;

				// $result1=mysqli_query($conn,"SELECT `maint_headId`, `head_name`, (SELECT `from_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS from_date,(SELECT `to_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS to_date FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

				$chrgs_condtnArr['maintenance_head.head_status']='Active';
		    	$chrgs_orderByArr['maintenance_head.srid']='ASC';

		    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name, (SELECT maintenance_head_charges.from_date FROM ".$this->maintenance_head_charges_tbl." WHERE maintenance_head_charges.maintenance_headId=maintenance_head.maint_headId ORDER BY maintenance_head_charges.srid DESC LIMIT 1) AS from_date, (SELECT maintenance_head_charges.to_date FROM ".$this->maintenance_head_charges_tbl." WHERE maintenance_head_charges.maintenance_headId=maintenance_head.maint_headId ORDER BY maintenance_head_charges.srid DESC LIMIT 1) AS to_date", $chrgs_condtnArr, $chrgs_likeCondtnArr=array(), $chrgs_joinArr=array(), $singleRow=FALSE, $chrgs_orderByArr, $chrgs_groupByArr=array(), $chrgs_whereInArray=array(), $chrgs_customWhereArray=array(), $backTicks=TRUE, $chrgs_customOrWhereArray=array(), $chrgs_orNotLikeArray=array());

				$charges=$query['userdata'];

				if(!empty($charges))
				{
					// while($data1=mysqli_fetch_assoc($result1))
					foreach($charges AS $data1) 
					{
						$maint_head_name=$data1['head_name'];
						$maint_headId=$data1['maint_headId'];
						$from_date=date("Y-m-d");
						$particularList['to_date']=$to_date=$data1['to_date'];
						//  echo "SELECT `Charges` FROM `member_maintenance_tbl` WHERE `memberId`='$usrId' AND `maintenance_headId`='$maint_headId' AND `from_date`<='$from_date' AND `to_date`='$to_date' ORDER BY `member_maintenance_tbl`.`srid` ASC LIMIT 1";
						// $result11=mysqli_query($conn,"SELECT `Charges`,`from_date` FROM `member_maintenance_tbl` WHERE `memberId`='$usrId' AND `maintenance_headId`='$maint_headId' AND `from_date`<='$from_date' AND `to_date`='$to_date' ORDER BY`member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1");

						$mbr_mnt_condtnArr['member_maintenance_tbl.memberId']=$usrId;
						$mbr_mnt_condtnArr['member_maintenance_tbl.maintenance_headId']=$maint_headId;
						$mbr_mnt_condtnArr['member_maintenance_tbl.from_date <=']=$from_date;
						$mbr_mnt_condtnArr['member_maintenance_tbl.to_date']=$to_date;

				    	$mbr_mnt_orderByArr['member_maintenance_tbl.from_date']='DESC';
				    	$mbr_mnt_orderByArr['member_maintenance_tbl.srid']='DESC';

				    	$query=$this->Mcommon->getRecords($tableName=$this->member_maintenance_tbl, $colNames="member_maintenance_tbl.from_date, member_maintenance_tbl.Charges", $mbr_mnt_condtnArr, $mbr_mnt_likeCondtnArr=array(), $mbr_mnt_joinArr=array(), $singleRow=TRUE, $mbr_mnt_orderByArr, $mbr_mnt_groupByArr=array(), $mbr_mnt_whereInArray=array(), $mbr_mnt_customWhereArray=array(), $backTicks=TRUE, $mbr_mnt_customOrWhereArray=array(), $mbr_mnt_orNotLikeArray=array());

						$maintdata=$query['userdata'];

						if(!empty($maintdata))
						{
							// while($maintdata=mysqli_fetch_assoc($result11))
							$particularList['from_date']=$maintdata['from_date'];
							$output[$usrId][]=$maintdata['Charges'];
							// }    
						}
					}
				}

				if(!empty($output))
				{
					foreach(end($output) as $col=> $val)
					{
						$particularList[$usrId][]=$val;
					}
				}

				$result[]=$particularList;
			}
		}

		$this->data['list_view']=$result;

		$this->data['view']="maintenance_management/individual_maintenance/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function edit()
    {
    	$this->data['userId']=$userId=$this->input->get('username');
    	$this->data['fromdate']=$fromdate=$this->input->get('fromdate');
    	$this->data['todate']=$todate=$this->input->get('todate');

		// $data_qry=mysqli_query($conn,"SELECT `srid`,`maint_headId`, `head_name`, (SELECT `charges` FROM `member_maintenance_tbl` 
		// WHERE `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' 
		// AND `to_date`='$todate' ORDER BY `srid` DESC LIMIT 1) AS charges, (SELECT `from_date` FROM `member_maintenance_tbl` WHERE 
		// `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' AND 
		// `to_date`='$todate' ORDER BY `srid` DESC LIMIT 1) AS from_date, (SELECT `to_date` FROM `member_maintenance_tbl` WHERE 
		// `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' AND `to_date`='$todate' 
		// ORDER BY `srid` DESC LIMIT 1) AS to_date FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC ");

		// while($headerdata=mysqli_fetch_assoc($data_qry))
		// {
		// 	$result[]=$headerdata;
		// }

		$condtnArr['maintenance_head.head_status']='Active';
    	$orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.srid, maintenance_head.maint_headId, maintenance_head.head_name, (SELECT member_maintenance_tbl.charges FROM ".$this->member_maintenance_tbl." WHERE member_maintenance_tbl.memberId='".$userId."' AND member_maintenance_tbl.maintenance_headId=maintenance_head.maint_headId AND member_maintenance_tbl.from_date='".$fromdate."' AND member_maintenance_tbl.to_date='".$todate."' ORDER BY member_maintenance_tbl.srid DESC LIMIT 1) AS charges, (SELECT member_maintenance_tbl.from_date FROM ".$this->member_maintenance_tbl." WHERE member_maintenance_tbl.memberId='".$userId."' AND member_maintenance_tbl.maintenance_headId=maintenance_head.maint_headId AND member_maintenance_tbl.from_date='".$fromdate."' AND member_maintenance_tbl.to_date='".$todate."' ORDER BY member_maintenance_tbl.srid DESC LIMIT 1) AS from_date, (SELECT member_maintenance_tbl.to_date FROM ".$this->member_maintenance_tbl." WHERE member_maintenance_tbl.memberId='".$userId."' AND member_maintenance_tbl.maintenance_headId=maintenance_head.maint_headId AND member_maintenance_tbl.from_date='".$fromdate."' AND member_maintenance_tbl.to_date='".$todate."' ORDER BY member_maintenance_tbl.srid DESC LIMIT 1) AS to_date", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$result=$query['userdata'];

		$this->data['result']=$result;

		// $this->data['view']="maintenance_management/individual_maintenance/edit";
		$this->load->view("maintenance_management/individual_maintenance/edit", $this->data);
    }

    public function edit_maintenance()
    {
    	$this->db->trans_start();

		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$user_name=$this->input->post('user_name');
		unset($_POST['fromdate']);
	    unset($_POST['todate']);
	    unset($_POST['user_name']);
	    unset($_POST['editmaint']);

		foreach($this->input->post() as $key => $value)
		{
			if($key!="editmaint" && $key!="fromdate" && $key!="todate" && $key!="user_name")
			{ 
				$maintenance_headId=$key;
				$charges=$value;
				// $result=edit_maintenanceheadersusercharges($userName, $societyKey, $user_name, $fromdate, $todate, $maintenance_headId, $charges);

				// function edit_maintenanceheadersusercharges($username,$soc_key,$userId,$fromdate,$todate,$headId,$charges)

				$curr_date=date("Y-m-d");

				// $tnst_qry=mysqli_query($conn, "INSERT INTO `member_maintenance_tbl`(`memberId`, `maintenance_headId`, `charges`, `from_date`, `to_date`, `flag`) VALUES ('$user_name','$maintenance_headId','$charges','$curr_date','$todate','Yes')");

				$mbr_mnt_InsertArr=array();

				$mbr_mnt_InsertArr[]=array(
					'memberId'=>$user_name,
					'maintenance_headId'=>$maintenance_headId,
					'charges'=>$charges,
					'from_date'=>$curr_date,
					'to_date'=>$todate,
					'flag'=>'Yes',
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->member_maintenance_tbl, $mbr_mnt_InsertArr, $returnType="");

				$tnst_qry=$query['status'];

				if($tnst_qry==TRUE)
				{
					// $data_qry=mysqli_query($conn,"UPDATE `member_maintenance_tbl` SET `flag`='NO' WHERE `memberId`='$user_name' AND `maintenance_headId`='$maintenance_headId' AND `from_date`<'$fromdate' AND `to_date`='$todate'");

					$mbr_mnt_UpdateArr=array(
						'flag'=>'NO',
						'updatedBy'=>$this->user_name,
						'updatedDatetime'=>$this->curr_datetime
					);

					$mbr_mnt_updt_condtnArr['member_maintenance_tbl.memberId']=$user_name;
					$mbr_mnt_updt_condtnArr['member_maintenance_tbl.maintenance_headId']=$maintenance_headId;
					$mbr_mnt_updt_condtnArr['member_maintenance_tbl.from_date < ']=$fromdate;
					$mbr_mnt_updt_condtnArr['member_maintenance_tbl.to_date']=$todate;

					$query=$this->Mcommon->update($tableName=$this->member_maintenance_tbl, $mbr_mnt_UpdateArr, $mbr_mnt_updt_condtnArr, $likeCondtnArr=array());

					$mbr_mnt_Update_qry=$query['status'];

					if($mbr_mnt_Update_qry==TRUE)
					{
						$result="Sucessfully updated";
					}
				}
			}
		}
		if(empty($result))
		{
			$response='Charges not updated';
		}
		else
		{
			$response='Charges updated';
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Maintenance charges has not updated.');
			redirect(base_url()."maintenance_management/individual_maintenance/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Maintenance charges has been updated successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
			redirect(base_url()."maintenance_management/individual_maintenance/list", 'refresh');
		}     
    }
}

?>