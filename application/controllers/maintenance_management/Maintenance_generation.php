<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance_generation extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='MAINTENANCE_GENERATION';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->maintenance_head_tbl=$socTables['maintenance_head_tbl'];
		$this->maintenance_head_charges_tbl=$socTables['maintenance_head_charges_tbl'];
		$this->member_maintenance_tbl=$socTables['member_maintenance_tbl'];
		$this->maintenance_tbl=$socTables['maintenance_tbl'];
		$this->penalty_tbl=$socTables['penalty_tbl'];
		$this->society_master_tbl=$socTables['society_master_tbl'];
		$this->members_advance_log_tbl=$socTables['members_advance_log_tbl'];
		$this->accounting_tbl=$socTables['accounting_tbl'];
		$this->accounting_types_tbl=$socTables['accounting_types_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Maintenance Generation';
    }

    public function home()
    {
    	$breadcrumbArr[0]['name']="Maintenance Generation";
    	$breadcrumbArr[0]['link']=base_url()."maintenance_management/maintenance_generation/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Maintenance Generation List";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$condtnArr['s-r-user.s-r-approve']='approve';
    	$condtnArr['s-r-user.s-r-active']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname AS usrFname, s-r-user.s-r-lname AS usrLname, s-r-user.s-r-username AS usrId, s-r-user.s-r-mobile AS usrMobile, s-r-user.s-r-appartment AS usrAppartment, s-r-user.s-r-wing AS usrWing, s-r-user.s-r-flat AS usrFlat, s-r-user.s-r-email AS usrMail, s-r-user.active_reason, (SELECT `directory`.`d-body` FROM `".$this->society_db."`.`directory` WHERE `directory`.`important`=`s-r-user`.`s-r-email` AND `directory`.`directory_type`='type1' AND `directory`.`status`='Active' LIMIT 1) AS designation", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;
		
    	$this->data['view']="maintenance_management/maintenance_generation/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function print_maintenance()
    {
    	$fromdate=$this->input->get('allfromdate');
    	$todate=$this->input->get('alltodate');
    	// $all_bill_fetch=bill_print_all($_GET['allfromdate'], $_GET['alltodate'], $userName, $societyKey);
    	// function bill_print_all($fromdate, $todate, $userName, $soc_key)

		// include '../database_connection/s-conn.php';
		//$fromdate=date("Y-m-d");
		// $fetch_maint_user=mysqli_query($conn,"SELECT `user_name`, `end_day`,`tansaction_no` FROM `maintance` WHERE `start_day`='$fromdate' AND `end_day`='$todate'");

		$mnt_user_condtnArr['maintenance.start_day']=$fromdate;
    	$mnt_user_condtnArr['maintenance.end_day']=$todate;

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.user_name, maintenance.end_day, maintenance.tansaction_no", $mnt_user_condtnArr, $mnt_user_likeCondtnArr=array(), $mnt_user_joinArr=array(), $singleRow=FALSE, $mnt_user_orderByArr=array(), $mnt_user_groupByArr=array(), $mnt_user_whereInArray=array(), $mnt_user_customWhereArray=array(), $backTicks=TRUE, $mnt_user_customOrWhereArray=array(), $mnt_user_orNotLikeArray=array());

		$fetch_maint_user=$query['userdata'];

		$all_bill_fetch=array();
		$result=array();

		if(!empty($fetch_maint_user))
		{
			// while($maint_user=mysqli_fetch_assoc($fetch_maint_user))
			foreach($fetch_maint_user AS $maint_user)
			{
				$value3="Unpaid";
				$todate=$maint_user['end_day'];
				$transactionId=$maint_user['tansaction_no'];
				$userId=$maint_user['user_name'];

				// $result[]=Fetch_maintenanbillprint($end_day,$tansaction_no,$value3,$user_name,$userName,$soc_key);
				// function Fetch_maintenanbillprint($todate,$transactionId,$value3,$userId,$username,$soc_key)

				// include '../database_connection/s-conn.php';
				$fromdate=date("Y-m-d");

				$result = array(); 
				// $soc_bill_fetch=mysqli_query($conn,"SELECT `soc_name`, `soc_register_no`, `soc_address`, `bill_note` FROM `society_master`");

		    	$query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="society_master.soc_name, society_master.soc_register_no, society_master.soc_address, society_master.bill_note", $soc_bill_condtnArr=array(), $soc_bill_likeCondtnArr=array(), $soc_bill_joinArr=array(), $singleRow=TRUE, $soc_bill_orderByArr=array(), $soc_bill_groupByArr=array(), $soc_bill_whereInArray=array(), $soc_bill_customWhereArray=array(), $backTicks=TRUE, $soc_bill_customOrWhereArray=array(), $soc_bill_orNotLikeArray=array());

				$soc_bill_fetch=$query['userdata'];

				if(!empty($soc_bill_fetch))
				{
					// $result['society_details']=mysqli_fetch_assoc($soc_bill_fetch);
					$result['society_details']=$soc_bill_fetch;
				}

				// $select = "SELECT `status`,`s-r-fname`,`s-r-lname`,DATE_FORMAT(`due_date`,'%d %b %Y') AS duedate,`tansaction_no` AS tansaction_no1,`voucher_name` AS voucher_no1,
				// `amount` AS total,DATE_FORMAT(`end_day`,'%d %b %Y') AS `end_day`,DATE_FORMAT(`start_day`,'%d %b %Y') AS `start_day`,`due_amount`,CONCAT(`s-r-floor`,`s-r-flat`) AS flatNo,`start_day` AS biiEnd FROM `maintance` LEFT JOIN `s-r-user` 
				// ON `maintance`.`user_name`=`s-r-user`.`s-r-username` WHERE `maintance`.`tansaction_no`='$transactionId' AND  `maintance`.`user_name`='$userId'";

				// print_r($select);
				// die();
				$mnt_user_joinArr=array();

				$mnt_user_condtnArr['maintenance.tansaction_no']=$transactionId;
				$mnt_user_condtnArr['maintenance.user_name']=$userId;

				$mnt_user_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"maintenance.user_name=s-r-user.s-r-username", "type"=>"left");

				$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="s-r-user.status, s-r-user.s-r-fname, s-r-user.s-r-lname, DATE_FORMAT(maintenance.due_date,'%d %b %Y') AS duedate, maintenance.tansaction_no AS tansaction_no1, maintenance.voucher_name AS voucher_no1, maintenance.amount AS total, DATE_FORMAT(maintenance.end_day, '%d %b %Y') AS end_day, DATE_FORMAT(maintenance.start_day, '%d %b %Y') AS start_day, maintenance.due_amount, CONCAT(`s-r-user`.`s-r-floor`, `s-r-user`.`s-r-flat`) AS flatNo, maintenance.start_day AS biiEnd", $mnt_user_condtnArr, $likeCondtnArr=array(), $mnt_user_joinArr, $singleRow=TRUE, $mnt_user_orderByArr=array(), $mnt_user_groupByArr=array(), $mnt_user_whereInArray=array(), $mnt_user_customWhereArray=array(), $backTicks=TRUE);

				$get_value=$query['userdata'];

				if(!empty($get_value))
				{
					// $fire=mysqli_query($conn,$select);
					// $get_value=mysqli_fetch_assoc($fire);
					$result['flatNo']=$get_value['flatNo'];

					$result['status']=$get_value['status'];
					$result['s-r-fname']=$get_value['s-r-fname'];
					$result['s-r-lname']=$get_value['s-r-lname'];
					$result['bill_dt']=$get_value['start_day'];
					$result['duedate']=$get_value['duedate'];
					$result['total']=$get_value['total'];
					$result['principal_arrears']=$get_value['due_amount'];
					$result['tansaction_no1']=$get_value['tansaction_no1'];
					$result['voucher_no1']=$get_value['voucher_no1'];
					// $billDt=$get_value['date'];
					$billendDt=$get_value['biiEnd'];

					$result['intrestAmount']=0;

					// $data_qry=mysqli_query($conn,"SELECT `head_name` AS Particular, (SELECT `charges` FROM `member_maintenance_tbl` 
					// WHERE `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('$fromdate' between 
					// `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('$todate' between 
					// `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` 
					// DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS charges FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC ");

					$mbr_mnt_condtnArr['maintenance_head.head_status']='Active';
			    	$mbr_mnt_orderByArr['maintenance_head.srid']='ASC';

			    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.head_name AS Particular, (SELECT member_maintenance_tbl.charges FROM ".$this->member_maintenance_tbl." WHERE member_maintenance_tbl.memberId='".$userId."' AND member_maintenance_tbl.maintenance_headId=maintenance_head.maint_headId AND ('".$fromdate."' between member_maintenance_tbl.from_date and member_maintenance_tbl.to_date) AND ('".$todate."' between member_maintenance_tbl.from_date and member_maintenance_tbl.to_date) ORDER BY member_maintenance_tbl.from_date DESC, member_maintenance_tbl.srid DESC LIMIT 1) AS charges", $mbr_mnt_condtnArr, $mbr_mnt_likeCondtnArr=array(), $mbr_mnt_joinArr=array(), $singleRow=FALSE, $mbr_mnt_orderByArr, $mbr_mnt_groupByArr=array(), $mbr_mnt_whereInArray=array(), $mbr_mnt_customWhereArray=array(), $backTicks=TRUE, $mbr_mnt_customOrWhereArray=array(), $mbr_mnt_orNotLikeArray=array());

					$data_qry=$query['userdata'];

					if(!empty($data_qry))
					{
						// while($headerdata=mysqli_fetch_assoc($data_qry))
						foreach($data_qry AS $headerdata)
						{
							$maint_perticular[]=$headerdata;
						}
						$total=0;
						foreach($maint_perticular as $key => $value)
						{
							if($value['charges']!="0")
							{
								//   $total+=$value['charges'];
								if($value['Particular']!="INT")
								{
									$result['MaintenanceParticular'][]=array('Particular'=>$value['Particular'],'Charges'=>$value['charges']);
									$total+=$value['charges'];
								}
								else
								{
									$result['intrestAmount']=$value['charges'];
								}
							}
						}
						//   $result['MaintenanceTotal']=$total;
						// $fetch_arres=mysqli_query($conn,"SELECT `advance_amount` FROM `members_advance_log` WHERE `creation_date`<'$billendDt' AND `user_id`='$userId' ORDER BY `creation_date` DESC LIMIT 1");

						$adv_amt_condtnArr['members_advance_log.creation_date <']=$billendDt;
						$adv_amt_condtnArr['members_advance_log.user_id']=$userId;
				    	$adv_amt_orderByArr['members_advance_log.creation_date']='DESC';

				    	$query=$this->Mcommon->getRecords($tableName=$this->members_advance_log_tbl, $colNames="members_advance_log.advance_amount", $adv_amt_condtnArr, $adv_amt_likeCondtnArr=array(), $adv_amt_joinArr=array(), $singleRow=TRUE, $adv_amt_orderByArr, $adv_amt_groupByArr=array(), $adv_amt_whereInArray=array(), $adv_amt_customWhereArray=array(), $backTicks=TRUE, $adv_amt_customOrWhereArray=array(), $adv_amt_orNotLikeArray=array());

						$fetch_arres=$query['userdata'];

						if(!empty($fetch_arres))
						{
							// $arr_data=$fetch_arres;
							$result['MaintenanceAdvance']=$fetch_arres['advance_amount'];
						}
						else
						{
							$result['MaintenanceAdvance']=0;  
						}

						$result['MaintenanceSubTotal']=$total;
						// $maint_intrest_fetch=mysqli_query($conn, "SELECT `penalty_percentage` FROM `penalty` WHERE `flag`='Yes'");

						$pntly_condtnArr['penalty.flag']='Yes';

				    	$query=$this->Mcommon->getRecords($tableName=$this->penalty_tbl, $colNames="penalty.penalty_percentage", $pntly_condtnArr, $pntly_likeCondtnArr=array(), $pntly_joinArr=array(), $singleRow=TRUE, $pntly_orderByArr=array(), $pntly_groupByArr=array(), $pntly_whereInArray=array(), $pntly_customWhereArray=array(), $backTicks=TRUE, $pntly_customOrWhereArray=array(), $pntly_orNotLikeArray=array());

						$maint_intrest=$query['userdata'];

						if(!empty($maint_intrest))
						{

							// $maint_intrest=mysqli_fetch_assoc($maint_intrest_fetch);
							$result['intrest']=$maint_intrest['penalty_percentage'];
							//  $result['intrestAmount']=$intrest;//value came from db
							$result['totalArrears']=$result['principal_arrears']+$result['intrestAmount'];
							$result['MaintenanceGrandTotal']=  $result['MaintenanceSubTotal']+$result['totalArrears'];
							//$result['MaintenanceParticular'][]=array('Particular'=>NULL,'Charges'=>NULL);
							$result['start_day']=$get_value['start_day'];
							$result['end_day']=$get_value['end_day'];

							$result['inwords']=$this->Mfunctional->inWords($result['MaintenanceGrandTotal']);

							$result['receipt']=NULL;
							// $reciptdatasql=mysqli_query($conn,"SELECT `transaction_no`,DATE_FORMAT(`s-date`,'%d %b %Y')  AS Recipt_date,`accounting_charges`,`payment_mode`,`cheque_no`,
							// (SELECT `tansaction_no` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS Billnum,(SELECT `start_day` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS billperiod FROM `accounting` WHERE `vaucher_name`='Reciept Voucher' AND `member_id`='$userId' AND `s-date`<'$billendDt' AND `cr_dr`='Cr' ORDER BY `a_id` DESC LIMIT 1");

							$rcpt_condtnArr['accounting.vaucher_name']='Reciept Voucher';
							$rcpt_condtnArr['accounting.member_id']=$userId;
							$rcpt_condtnArr['accounting.s-date < ']=$billendDt;
							$rcpt_condtnArr['accounting.cr_dr']='Cr';
					    	$rcpt_orderByArr['accounting.a_id']='DESC';

					    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.transaction_no, DATE_FORMAT(`accounting`.`s-date`, '%d %b %Y') AS Recipt_date, accounting.accounting_charges, accounting.payment_mode, accounting.cheque_no, (SELECT maintenance.tansaction_no FROM ".$this->maintenance_tbl." WHERE maintenance.user_name='".$userId."' ORDER BY maintenance.start_day DESC LIMIT 1) AS Billnum, (SELECT maintenance.start_day FROM ".$this->maintenance_tbl." WHERE maintenance.user_name='".$userId."' ORDER BY maintenance.start_day DESC LIMIT 1) AS billperiod", $rcpt_condtnArr, $rcpt_likeCondtnArr=array(), $rcpt_joinArr=array(), $singleRow=TRUE, $rcpt_orderByArr, $rcpt_groupByArr=array(), $rcpt_whereInArray=array(), $rcpt_customWhereArray=array(), $backTicks=TRUE, $rcpt_customOrWhereArray=array(), $rcpt_orNotLikeArray=array());

							$recipt_fetch=$query['userdata'];

							if(!empty($recipt_fetch))
							{
								// $recipt_fetch=mysqli_fetch_assoc($reciptdatasql);
								//print_r($recipt_fetch);
								$result['receipt']=array(
									"transaction_no"=>$recipt_fetch['transaction_no'],
									"Recipt_date"=>$recipt_fetch['Recipt_date'],
									"Recipt_amount"=>$recipt_fetch['accounting_charges'],
									"Recipt_amountWords"=>$this->Mfunctional->inWords($recipt_fetch['accounting_charges']),
									"payment_mode"=>$recipt_fetch['payment_mode'],
									"cheque_no"=>$recipt_fetch['cheque_no'],
									"Billnum"=>$recipt_fetch['Billnum'],
									"billperiod"=>date("F",strtotime($recipt_fetch['billperiod'])),
									"Cheque_date"=>date("d-m-Y",strtotime($recipt_fetch['billperiod']))
								);
							}
						}
					}
				}

				$all_bill_fetch[]=$result;
			}  
		}

		$this->data['all_bill_fetch']=$all_bill_fetch;

		$this->data['view']="maintenance_management/maintenance_generation/print_maintenance";
		$this->load->view('layouts/layout/secondary_layout', $this->data);
    }

    public function fixed_generation()
    {
    	$this->db->trans_start();
    	$response_val="";
		$currmon= date("m");

		if($currmon>=4) 
		{
		   	$curryear= date("Y");
		}
		else
		{
		 	$curryear= date("Y")-1;
		}

		$ryear=$curryear;
		$date1=date("Y-m-d");
		$startday=strtotime($date1);
		$month=date("F",$startday);
		$year=date("Y",$startday);

		$previousmomth=date("m")-1;

		$current_date = date("Y-m-01");
		$cumomth=date("m");
		$cmonth = date("F",strtotime($current_date));

		$wmonth = date("M",strtotime($current_date));
		$wyear = date("Y",strtotime($current_date));
		// echo "SELECT * FROM `maintance` WHERE (start_day) = '$cumomth'";
		$enddate = date("m", strtotime("last day of previous month"));
		// echo "SELECT * FROM `maintance` WHERE MONTH(start_day) = '$cumomth'";

		// $check = mysqli_query($conn, "SELECT * FROM `maintance` WHERE start_day = '$current_date'");
		// $val = mysqli_num_rows($check);

		$chk_condtnArr['maintenance.start_day']=$current_date;

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.user_name", $chk_condtnArr, $chk_likeCondtnArr=array(), $chk_joinArr=array(), $singleRow=TRUE, $chk_orderByArr=array(), $chk_groupByArr=array(), $chk_whereInArray=array(), $chk_customWhereArray=array(), $backTicks=TRUE, $chk_customOrWhereArray=array(), $chk_orNotLikeArray=array());

		$val=$query['userdata'];

	    if(empty($val))
	    {  
			if(date("m")==4)
			{
				// finicial_year($soc_key,$userName);
				$this->Mfunctional->financial_year($this->society_key, $this->user_name);
			}

		  	// $data_qry=mysqli_query($conn, "SELECT  `s-r-username`, `s-r-flat-area`,(SELECT `accounting_id` FROM `accounting_types` WHERE `accounting_name`=`s-r-user`.`s-r-username` AND `accounting_type`='Assets') AS accId FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

		  	$user_condtnArr['s-r-user.s-r-approve']='approve';
			$user_orNotLikeArray['s-r-user.disabled_user']='disabled';

	    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, s-r-user.s-r-flat-area, (SELECT `accounting_id` FROM ".$this->accounting_types_tbl." WHERE `accounting_name`=`s-r-user`.`s-r-username` AND `accounting_type`='Assets') AS accId", $user_condtnArr, $user_likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr=array(), $user_groupByArr=array(), $user_whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $user_customOrWhereArray=array(), $user_orNotLikeArray);

			$data_qry=$query['userdata'];

        	// while($no_user=mysqli_fetch_assoc($data_qry))

        	if(!empty($data_qry))
        	{
		        foreach($data_qry AS $no_user)
		        {
			        $usrId=$no_user['s-r-username'];
			        $flat_area=$no_user['s-r-flat-area'];
			        $accId=$no_user['accId'];

					// $previous_maint_fetch=mysqli_query($conn, "SELECT `due_date`,`status`,`tansaction_no1`, `amount` FROM `maintance` WHERE `user_name`='$usrId' ORDER BY `start_day` DESC LIMIT 1");

					$prev_mnt_condtnArr['maintenance.user_name']=$usrId;
					$prev_mnt_orderByArr['maintenance.start_day']='DESC';

			    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.due_date, maintenance.status, maintenance.tansaction_no1, maintenance.amount", $prev_mnt_condtnArr, $prev_mnt_likeCondtnArr=array(), $prev_mnt_joinArr=array(), $singleRow=TRUE, $prev_mnt_orderByArr, $prev_mnt_groupByArr=array(), $prev_mnt_whereInArray=array(), $prev_mnt_customWhereArray=array(), $backTicks=TRUE, $prev_mnt_customOrWhereArray=array(), $prev_mnt_orNotLikeArray=array());

					$previous_maint=$query['userdata'];

					if(!empty($previous_maint))
					{
						// $previous_maint=mysqli_fetch_assoc($previous_maint_fetch);
						$previous_maint_status=$previous_maint['status'];
						$previous_maint_tansaction_no1=$previous_maint['tansaction_no1'];
						$previous_maint_due_date=$previous_maint['due_date'];
						$result['intrest_flag']="1";

						// $maint_intrest_fetch=mysqli_query($conn, "SELECT `penalty_percentage` FROM `penalty` WHERE `flag`='Yes'");
						// $maint_intrest=mysqli_fetch_assoc($maint_intrest_fetch);

						$mnt_inrst_condtnArr['penalty.flag']='Yes';

				    	$query=$this->Mcommon->getRecords($tableName=$this->penalty_tbl, $colNames="penalty.penalty_percentage", $mnt_inrst_condtnArr, $mnt_inrst_likeCondtnArr=array(), $mnt_inrst_joinArr=array(), $singleRow=TRUE, $mnt_inrst_orderByArr=array(), $mnt_inrst_groupByArr=array(), $mnt_inrst_whereInArray=array(), $mnt_inrst_customWhereArray=array(), $backTicks=TRUE, $mnt_inrst_customOrWhereArray=array(), $mnt_inrst_orNotLikeArray=array());

						$maint_intrest=$query['userdata'];

						if(!empty($maint_intrest))
						{
							$result['amount']=$previous_maint['amount']*(($maint_intrest['penalty_percentage']/12)/100);
							if($previous_maint_status=="paid")
							{
								// $previous_recipt_fetch=mysqli_query($conn, "SELECT `s-date` AS receiptDate FROM `accounting` WHERE `accouting_id`='$accId' AND `transaction_no`='$previous_maint_tansaction_no1'");
								// $previous_recipt_data=mysqli_fetch_assoc($previous_recipt_fetch);

								$prev_rcpt_condtnArr['accounting.accouting_id']=$accId;
								$prev_rcpt_condtnArr['accounting.transaction_no']=$previous_maint_tansaction_no1;

						    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.s-date AS receiptDate", $prev_rcpt_condtnArr, $prev_rcpt_likeCondtnArr=array(), $prev_rcpt_joinArr=array(), $singleRow=TRUE, $prev_rcpt_orderByArr=array(), $prev_rcpt_groupByArr=array(), $prev_rcpt_whereInArray=array(), $prev_rcpt_customWhereArray=array(), $backTicks=TRUE, $prev_rcpt_customOrWhereArray=array(), $prev_rcpt_orNotLikeArray=array());

								$previous_recipt_data=$query['userdata'];

								if(date("Y-m-d",strtotime($previous_recipt_data['receiptDate'])) < date("Y-m-d",strtotime($previous_maint_due_date)))
								{
									$result['intrest_flag']="0";
								}
							}
						}
					}
	        
					// $previous_maint_fetch=user_intrst_checker($soc_key,$userName,$usrId,$accId);
					$previous_maint_fetch=$result;

					//print_r($previous_maint_fetch);
					if(!empty($previous_maint_fetch))
					{
				        if($previous_maint_fetch['intrest_flag']=="1")
				        {
		            		$todate=$ryear."-03-31";
		            		// $usermaintheadsql=mysqli_query($conn, "SELECT `from_date`,`to_date` FROM `member_maintenance_tbl` WHERE `memberId`='$usrId' ORDER BY `srid` DESC LIMIT 1");
		            		// $usermaintheaddata=mysqli_fetch_assoc($usermaintheadsql);

		            		$user_mnt_hd_condtnArr['member_maintenance_tbl.memberId']='approve';
		            		$user_mnt_hd_orderByArr['member_maintenance_tbl.srid']='DESC';

					    	$query=$this->Mcommon->getRecords($tableName=$this->member_maintenance_tbl, $colNames="member_maintenance_tbl.from_date, member_maintenance_tbl.to_date", $user_mnt_hd_condtnArr, $user_mnt_hd_likeCondtnArr=array(), $user_mnt_hd_joinArr=array(), $singleRow=TRUE, $user_mnt_hd_orderByArr, $user_mnt_hd_groupByArr=array(), $user_mnt_hd_whereInArray=array(), $user_mnt_hd_customWhereArray=array(), $backTicks=TRUE, $user_mnt_hd_customOrWhereArray=array(), $user_mnt_hd_orNotLikeArray=array());

							$usermaintheaddata=$query['userdata'];

							if(!empty($usermaintheaddata))
							{

			            		// $intrest_head=Fetch_maintenanceheadersusercharges($userName,$soc_key,$usrId,date("Y-m-d",strtotime($usermaintheaddata['from_date'])),date("Y-m-d",strtotime($usermaintheaddata['to_date'])));

			            		// function Fetch_maintenanceheadersusercharges($username,$soc_key,$userId,$fromdate,$todate)

			            		$userId=$usrId;
			            		$fromdate=date("Y-m-d",strtotime($usermaintheaddata['from_date']));
			            		$todate=date("Y-m-d",strtotime($usermaintheaddata['to_date']));

			            		 // $data_qry=mysqli_query($conn,"SELECT `srid`,`maint_headId`, `head_name`, (SELECT `charges` FROM `member_maintenance_tbl` WHERE `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' AND `to_date`='$todate' ORDER BY `srid` DESC LIMIT 1) AS charges, (SELECT `from_date` FROM `member_maintenance_tbl` WHERE  `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' AND  `to_date`='$todate' ORDER BY `srid` DESC LIMIT 1) AS from_date, (SELECT `to_date` FROM `member_maintenance_tbl` WHERE  `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='$fromdate' AND `to_date`='$todate'  ORDER BY `srid` DESC LIMIT 1) AS to_date FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC ");

			            		$inrst_hd_condtnArr['maintenance_head.head_status']='Active';
			            		$inrst_hd_orderByArr['maintenance_head.srid']='ASC';

						    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.srid, maintenance_head.maint_headId, maintenance_head.head_name, (SELECT `charges` FROM ".$this->member_maintenance_tbl." WHERE `memberId`='".$userId."' AND `maintenance_headId` = `maintenance_head`.`maint_headId` AND `from_date`='".$fromdate."' AND `to_date`='".$todate."' ORDER BY `srid` DESC LIMIT 1) AS charges, (SELECT `from_date` FROM ".$this->member_maintenance_tbl." WHERE  `memberId`='".$userId."' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND `from_date`='".$fromdate."' AND  `to_date`='".$todate."' ORDER BY `srid` DESC LIMIT 1) AS from_date, (SELECT `to_date` FROM ".$this->member_maintenance_tbl." WHERE  `memberId`='".$userId."' AND `maintenance_headId` = `maintenance_head`.`maint_headId` AND `from_date`='".$fromdate."' AND `to_date`='".$todate."'  ORDER BY `srid` DESC LIMIT 1) AS to_date", $inrst_hd_condtnArr, $inrst_hd_likeCondtnArr=array(), $inrst_hd_joinArr=array(), $singleRow=FALSE, $inrst_hd_orderByArr, $inrst_hd_groupByArr=array(), $inrst_hd_whereInArray=array(), $inrst_hd_customWhereArray=array(), $backTicks=TRUE, $inrst_hd_customOrWhereArray=array(), $inrst_hd_orNotLikeArray=array());

								$intrest_head=$query['userdata'];


			                                    
			        			// $intrestheadIdsql=mysqli_query($conn, "SELECT `maint_headId` FROM `maintenance_head` WHERE `head_name`='INT' ");

			        			$inrst_hd_condtnArr['maintenance_head.head_name']='INT';

						    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId", $inrst_hd_condtnArr, $inrst_hd_likeCondtnArr=array(), $inrst_hd_joinArr=array(), $singleRow=TRUE, $inrst_hd_orderByArr=array(), $inrst_hd_groupByArr=array(), $inrst_hd_whereInArray=array(), $inrst_hd_customWhereArray=array(), $backTicks=TRUE, $inrst_hd_customOrWhereArray=array(), $inrst_hd_orNotLikeArray=array());

								$intrestheadId=$query['userdata'];

						        // if(mysqli_num_rows($intrestheadIdsql)>0)
						        if(count($intrestheadId)>0)
						        {
						        	// $intrestheadId=mysqli_fetch_assoc($intrestheadIdsql);

							        foreach($intrest_head as $inkey => $invalue)
							        {
							         	$maintenance_headId=$invalue['maint_headId'];
			        
								        if($maintenance_headId==$intrestheadId['maint_headId'])
								        {
								        	$charges=$previous_maint_fetch['amount'];
								        }
								        else
								        {
								        	$charges=$invalue['charges'];
								        }
			      
			        					// $penalty=edit_maintenanceheadersusercharges($userName,$soc_key,$usrId,date("Y-m-d"),date("Y-m-d",strtotime($usermaintheaddata['to_date'])),$maintenance_headId,$charges);

			        					// function edit_maintenanceheadersusercharges($username,$soc_key,$userId,$fromdate,$todate,$headId,$charges)
			        					$userId=$usrId;
			        					$fromdate=date("Y-m-d");
			        					$todate=date("Y-m-d",strtotime($usermaintheaddata['to_date']));
			        					$headId=$maintenance_headId;
			        					$curr_date=date("Y-m-d");
			        					$penalty="";

			        					// $tnst_qry=mysqli_query($conn, "INSERT INTO `member_maintenance_tbl`(`memberId`, `maintenance_headId`, `charges`, `from_date`, `to_date`, `flag`) VALUES ('$userId','$headId','$charges','$curr_date','$todate','Yes')");

			        					$tnst_InsertArr=array();

			        					$tnst_InsertArr[]=array(
											'memberId'=>$userId,
											'maintenance_headId'=>$headId,
											'charges'=>$charges,
											'from_date'=>$curr_date,
											'to_date'=>$todate,
											'flag'=>'Yes',
											'createdBy'=>$this->user_name,
											'createdDatetime'=>$this->curr_datetime
										);

										$query=$this->Mcommon->insert($tableName=$this->member_maintenance_tbl, $tnst_InsertArr, $returnType="");

										$tnst_qry=$query['status'];

			                            if($tnst_qry==TRUE)
			                            {
				                            // $data_qry=mysqli_query($conn,"UPDATE `member_maintenance_tbl` SET `flag`='NO' WHERE `memberId`='$userId' AND `maintenance_headId`='$headId' AND `from_date`<'$fromdate' AND `to_date`='$todate'");

				                            $member_mnt_UpdateArr=array(
												'flag'=>'NO',
												'updatedBy'=>$this->user_name,
												'updatedDatetime'=>$this->curr_datetime
											);

											$member_mnt_condtnArr['member_maintenance_tbl.memberId']=$userId;
											$member_mnt_condtnArr['member_maintenance_tbl.maintenance_headId']=$headId;
											$member_mnt_condtnArr['member_maintenance_tbl.from_date <']=$fromdate;
											$member_mnt_condtnArr['member_maintenance_tbl.to_date']=$todate;

											$query=$this->Mcommon->update($tableName=$this->member_maintenance_tbl, $member_mnt_UpdateArr, $member_mnt_condtnArr, $likeCondtnArr=array());

											$data_qry=$query['status'];

				                            if($data_qry==TRUE)
				                            {
				                            	$penalty="Sucessfully updated";
				                            }
			                            }
			        				}
			        			}
		        			}
		    			}
		    		}

	    			// $result1=mysqli_query($conn,"SELECT `maint_headId`, `head_name`, (SELECT `from_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS from_date,(SELECT `to_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS to_date FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

	        		$res_condtnArr['maintenance_head.head_status']='Active';
	        		$res_orderByArr['maintenance_head.srid']='ASC';

			    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name, (SELECT `from_date` FROM ".$this->maintenance_head_charges_tbl." WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS from_date, (SELECT `to_date` FROM ".$this->maintenance_head_charges_tbl." WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS to_date", $res_condtnArr, $res_likeCondtnArr=array(), $res_joinArr=array(), $singleRow=FALSE, $res_orderByArr, $res_groupByArr=array(), $res_whereInArray=array(), $res_customWhereArray=array(), $backTicks=TRUE, $res_customOrWhereArray=array(), $res_orNotLikeArray=array());

					$result1=$query['userdata'];

					if(!empty($result1))
					{
						// while($data1=mysqli_fetch_assoc($result1))
						foreach($result1 AS $data1)
						{
							$maint_head_name=$data1['head_name'];
							$maint_headId=$data1['maint_headId'];
							$from_date=date("Y-m-d");
							$to_date=$data1['to_date'];

		        			// $result11=mysqli_query($conn,"SELECT `Charges`,(SELECT `type` FROM `maintenance_head` WHERE `maint_headId`=`member_maintenance_tbl`.`maintenance_headId`) AS type FROM `member_maintenance_tbl` WHERE `memberId`='$usrId' AND `maintenance_headId`='$maint_headId' AND `from_date`<='$from_date' AND `to_date`='$to_date' ORDER BY`member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1");



		        			$res1_condtnArr['member_maintenance_tbl.memberId']=$usrId;
		        			$res1_condtnArr['member_maintenance_tbl.maintenance_headId']=$maint_headId;
		        			$res1_condtnArr['member_maintenance_tbl.from_date <=']=$from_date;
		        			$res1_condtnArr['member_maintenance_tbl.to_date']=$to_date;
			        		$res1_orderByArr['member_maintenance_tbl.from_date']='DESC';
			        		$res1_orderByArr['member_maintenance_tbl.srid']='DESC';

					    	$query=$this->Mcommon->getRecords($tableName=$this->member_maintenance_tbl, $colNames="member_maintenance_tbl.Charges, (SELECT `type` FROM ".$this->maintenance_head_tbl." WHERE `maint_headId`=`member_maintenance_tbl`.`maintenance_headId`) AS type", $res1_condtnArr, $res1_likeCondtnArr=array(), $res1_joinArr=array(), $singleRow=FALSE, $res1_orderByArr, $res1_groupByArr=array(), $res1_whereInArray=array(), $res1_customWhereArray=array(), $backTicks=TRUE, $res1_customOrWhereArray=array(), $res1_orNotLikeArray=array());

							$result11=$query['userdata'];

					        // while($maintdata=mysqli_fetch_assoc($result11))
					        foreach($result11 AS $maintdata)
					        {
					        	$output[$usrId][$maint_head_name]=$maintdata;
					        }    
		    			}
		    		}

	    			if(!empty($output))
	    			{
				        foreach(end($output) as $col=> $val)
				        {
		        			$particularList[$usrId][]=$val;
		        		}
		        	}
	        
			        $result[$usrId]=end($particularList);
			        $lusum=0;

					foreach($result[$usrId] as $usrKey => $usrvalue)
					{
				            //print_r($usrvalue);
				        if($usrvalue['type']=='Fixed')
				        {
				            $lusum += (int)$usrvalue['Charges'];
				        }
				        else
				        {
				            $lusum += (int)((int)$usrvalue['Charges']*(int)$flat_area);
				        }
	        		}

				    $dueday = date('Y-m-15');
				    $lastday = date('Y-m-t', strtotime($date1));
			           
			        $value2=$cumomth;
				    $value3=date("Y");
				    $value4=$value3-1;

			        if(($value2=="1") || ($value2=="2") || ($value2=="3"))
			        {
			       		// $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

			         //    $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='Journal Voucher' AND (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

			            $voc1_customWhereArray[]="(( YEAR(`s-date`)='".$value4."' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)<='".$value2."' ))";
			            $voc1_orderByArr['accounting.a_id']="DESC";

				    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.transaction_no", $voc1_condtnArr=array(), $voc1_likeCondtnArr=array(), $voc1_joinArr=array(), $singleRow=TRUE, $voc1_orderByArr, $voc1_groupByArr=array(), $voc1_whereInArray=array(), $voc1_customWhereArray, $backTicks=TRUE, $voc1_customOrWhereArray=array(), $voc1_orNotLikeArray=array());

						$acchead=$query['userdata'];

						$voc2_condtnArr['accounting.vaucher_name']="Journal Voucher";
						$voc2_customWhereArray[]="(( YEAR(`s-date`)='".$value4."' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)<='".$value2."' ))";
			            $voc2_orderByArr['accounting.a_id']="DESC";

				    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.vaucher_no", $voc2_condtnArr, $voc2_likeCondtnArr=array(), $voc2_joinArr=array(), $singleRow=TRUE, $voc2_orderByArr, $voc2_groupByArr=array(), $voc2_whereInArray=array(), $voc2_customWhereArray, $backTicks=TRUE, $voc2_customOrWhereArray=array(), $voc2_orNotLikeArray=array());

						$acchead_voc=$query['userdata'];
			        }
			        else
			        {
			            // $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

			            // $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='Journal Voucher' AND ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

			            $voc1_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";
			            $voc1_orderByArr['accounting.a_id']="DESC";

				    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.transaction_no", $voc1_condtnArr=array(), $voc1_likeCondtnArr=array(), $voc1_joinArr=array(), $singleRow=TRUE, $voc1_orderByArr, $voc1_groupByArr=array(), $voc1_whereInArray=array(), $voc1_customWhereArray, $backTicks=TRUE, $voc1_customOrWhereArray=array(), $voc1_orNotLikeArray=array());

						$acchead=$query['userdata'];

						$voc2_condtnArr['accounting.vaucher_name']="Journal Voucher";
						$voc2_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";
			            $voc2_orderByArr['accounting.a_id']="DESC";

				    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.vaucher_no", $voc2_condtnArr, $voc2_likeCondtnArr=array(), $voc2_joinArr=array(), $singleRow=TRUE, $voc2_orderByArr, $voc2_groupByArr=array(), $voc2_whereInArray=array(), $voc2_customWhereArray, $backTicks=TRUE, $voc2_customOrWhereArray=array(), $voc2_orNotLikeArray=array());

						$acchead_voc=$query['userdata'];
			        }
		            
			        // $acchead=mysqli_fetch_assoc($fetch);

					if($acchead['transaction_no']=="")
					{
						$acchead['transaction_no'];
						$iddd=1;
					}
					else
					{
						$vall=str_split($acchead['transaction_no'],10);
					 	$iddd=$vall[1]+1;
					}

			        $trannumber="trn_".$curryear."-0".$iddd;
			        // $acchead_voc=mysqli_fetch_assoc($fetch_voc);

					if($acchead_voc['vaucher_no']=="")
					{
						$acchead_voc['vaucher_no'];
						$iddd_voc=1;
					}
					else
					{
						$vall_voc=str_split($acchead_voc['vaucher_no'],9);
					 	$iddd_voc=$vall_voc[1]+1;
					}

					$trannumber="trn_".$curryear."-0".$iddd;
			 		$voucher="Journal Voucher";

			   		$voucherno="JV_".$curryear."-0".$iddd_voc;

			  		// $select_due = "SELECT `status`,`start_day` AS date,`tansaction_no` AS tansaction_no1,`voucher_name` AS voucher_no1,`amount` AS total,`end_day`,`start_day` FROM `maintance` WHERE (`maintance`.`start_day`)<='$current_date' AND  `maintance`.`user_name`='$usrId' AND `status`='unpaid' ORDER BY `status` ASC,`start_day` DESC ";
			       
			        // $fire_due=mysqli_query($conn,$select_due);
			        $total_due=0;

			        $due_condtnArr['maintenance.start_day <=']=$current_date;
			        $due_condtnArr['maintenance.user_name']=$usrId;
			        $due_condtnArr['maintenance.status']='unpaid';
		            $due_orderByArr['maintenance.status']="ASC";
		            $due_orderByArr['maintenance.start_day']="DESC";

			    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.status, maintenance.start_day AS date, maintenance.tansaction_no AS tansaction_no1, maintenance.voucher_name AS voucher_no1, maintenance.amount AS total, maintenance.end_day, maintenance.start_day", $due_condtnArr, $due_likeCondtnArr=array(), $due_joinArr=array(), $singleRow=FALSE, $due_orderByArr, $due_groupByArr=array(), $due_whereInArray=array(), $due_customWhereArray=array(), $backTicks=TRUE, $due_customOrWhereArray=array(), $due_orNotLikeArray=array());

					$fire_due=$query['userdata'];

			        // while($get_value_due=mysqli_fetch_assoc($fire_due))
			        foreach($fire_due AS $get_value_due)
			        {
			           	$intrest="0";
			          	$ammount_due=$get_value_due['total'];
			          	$total_due+=$ammount_due;
			        }

			        $ammount = $lusum;
			        // $insert = "INSERT INTO `maintance` (`user_name`, `start_day`, `end_day`, `status`, `amount`, `due_date`, `tansaction_no`, `voucher_no`, `voucher_name`, `due_amount`) VALUES ('$usrId','$current_date', '$lastday','unpaid', '$ammount','$dueday','$trannumber','$voucher','$voucherno','$total_due')";

			        // $change = mysqli_query($conn, $insert) or die("err" . mysqli_error($conn));

			        $mnt_InsertArr=array();

			        $mnt_InsertArr[]=array(
						'user_name'=>$usrId,
						'start_day'=>$current_date,
						'end_day'=>$lastday,
						'status'=>'unpaid',
						'amount'=>$ammount,
						'due_date'=>$dueday,
						'tansaction_no'=>$trannumber,
						'voucher_no'=>$voucher,
						'voucher_name'=>$voucherno,
						'due_amount'=>$total_due,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->maintenance_tbl, $mnt_InsertArr, $returnType="");

			 		$narration="";
			   		$narration .= "maintenance Chgs. for ".$wmonth."-".$wyear;

			   		// $insert=mysqli_query($conn,"INSERT INTO `accounting` (`accouting_id`, `accounting_charges`, `s-date`, `transaction_no`, `vaucher_name`, `vaucher_no`,`cr_dr`,`s-des`,`member_id`) VALUES ('$accId', '$ammount', '$current_date','$trannumber','$voucher','$voucherno','Dr','$narration','$usrId')")or die(mysqli_error($conn));

			   		$acc_InsertArr=array();

			        $acc_InsertArr[]=array(
						'accouting_id'=>$accId,
						'accounting_charges'=>$ammount,
						's-date'=>$current_date,
						'transaction_no'=>$trannumber,
						'vaucher_name'=>$voucher,
						'vaucher_no'=>$voucherno,
						'cr_dr'=>'Dr',
						's-des'=>$narration,
						'member_id'=>$usrId,
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->accounting_tbl, $acc_InsertArr, $returnType="");

					$push_type="Maintenance"; 
					$push_title="Maintenance bill";
					$push_body= $cmonth ." Maintenance bill has been genrated";

					$this->load->library('Push_notify');

		            $notifyArr['userType']='user app';
		            $notifyArr['userName']=$this->user_name;
		            $notifyArr['title']=$push_title;
		            $notifyArr['body']=$push_body;
		            $notifyArr['pushType']=$push_type;
		            $notifyArr['month']=$cmonth;
		            $notifyArr['priority']=false;

		            $notifyRes=$this->push_notify->notification($notifyArr);		
		    	}
	        	$response_val="Bills Genrated updated for ". $cmonth." ".$wyear;
		    }
	    }
	    else
	    {
	        $response_val="Bills already Genrated for ". $cmonth." ".$wyear;
	    }

	    $this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			// echo "<html><body><script>alert('Something went wrong...')</script></body></html>";
			$this->session->set_flashdata('error_msg', 'Something went wrong...');
			redirect(base_url()."maintenance_management/maintenance_generation/home", 'refresh');
		}
		else
		{
			// echo "<html><body><script>alert('".$response_val."')</script></body></html>";
			$this->session->set_flashdata('success_msg', $response_val);
			redirect(base_url()."maintenance_management/maintenance_generation/home", 'refresh');
		}     
    }

    public function passbook()
    {
		$this->data['member_id']=$member_id=$this->uri->segment('4');

		$breadcrumbArr[0]['name']="Maintenance Generation";
    	$breadcrumbArr[0]['link']=base_url()."maintenance_management/maintenance_generation/home";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Maintenance Generation List";
    	$breadcrumbArr[1]['link']=base_url()."maintenance_management/maintenance_generation/home";
    	$breadcrumbArr[1]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Passbook";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

		$fyear=( date('m') > 3) ? date('Y') + 1 : date('Y');
		$this->data['fyear']=$fyear=$fyear."-03-31";
		$this->data['month1']=$month1=$fyear;
		$this->data['year']=$year=str_split($fyear,4);
		$this->data['previous']=$previous=$year['0']-1;
		$this->data['lastyear']=$lastyear=$previous."-04-01";

		// $member_passbook=Fetch_maintenanceuser_passbook($userName,$societyKey,$member_id,$fyear,$lastyear);
		// function Fetch_maintenanceuser_passbook($username,$soc_key,$userId,$fyear,$lastyear)
	    $this->data['value2']=$value2=date('Y-m-01');

	    // $result=array();
	    // $sql = "SELECT * FROM `maintance` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username` = `maintance`.`user_name`  WHERE `user_name`='$userId' AND (`start_day` between '$lastyear' and '$fyear') AND (`end_day` between '$lastyear' and '$fyear') ORDER BY `m_id` ASC";

    	$customWhereArray[]="`user_name`='".$member_id."' AND (`start_day` between '".$lastyear."' and '".$fyear."') AND (`end_day` between '".$lastyear."' and '".$fyear."')";
    	$orderByArr['maintenance.m_id']='ASC';

    	$joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"`s-r-user`.`s-r-username` = `maintenance`.`user_name`", "type"=>"left");

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="*", $condtnArr=array(), $likeCondtnArr=array(), $joinArr, $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray, $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_passbook=$query['userdata'];

		$this->data['member_passbook']=$member_passbook;
		
    	$this->data['view']="maintenance_management/maintenance_generation/passbook";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function print_maintenance_bill()
    {
    	$todate=$this->input->get('prtodate');
    	$transactionId=$this->input->get('prtransactionId');
    	$value3=$this->input->get('prstatus');
    	$userId=$this->input->get('prusername');

		// $result[]=Fetch_maintenanbillprint($end_day,$tansaction_no,$value3,$user_name,$userName,$soc_key);
		// function Fetch_maintenanbillprint($todate,$transactionId,$value3,$userId,$username,$soc_key)

		// include '../database_connection/s-conn.php';
		$fromdate=date("Y-m-d");

		$result = array(); 
		// $soc_bill_fetch=mysqli_query($conn,"SELECT `soc_name`, `soc_register_no`, `soc_address`, `bill_note` FROM `society_master`");

    	$query=$this->Mcommon->getRecords($tableName=$this->society_master_tbl, $colNames="society_master.soc_name, society_master.soc_register_no, society_master.soc_address, society_master.bill_note", $soc_bill_condtnArr=array(), $soc_bill_likeCondtnArr=array(), $soc_bill_joinArr=array(), $singleRow=TRUE, $soc_bill_orderByArr=array(), $soc_bill_groupByArr=array(), $soc_bill_whereInArray=array(), $soc_bill_customWhereArray=array(), $backTicks=TRUE, $soc_bill_customOrWhereArray=array(), $soc_bill_orNotLikeArray=array());

		$soc_bill_fetch=$query['userdata'];

		if(!empty($soc_bill_fetch))
		{
			// $result['society_details']=mysqli_fetch_assoc($soc_bill_fetch);
			$result['society_details']=$soc_bill_fetch;
		}

		// $select = "SELECT `status`,`s-r-fname`,`s-r-lname`,DATE_FORMAT(`due_date`,'%d %b %Y') AS duedate,`tansaction_no` AS tansaction_no1,`voucher_name` AS voucher_no1,
		// `amount` AS total,DATE_FORMAT(`end_day`,'%d %b %Y') AS `end_day`,DATE_FORMAT(`start_day`,'%d %b %Y') AS `start_day`,`due_amount`,CONCAT(`s-r-floor`,`s-r-flat`) AS flatNo,`start_day` AS biiEnd FROM `maintance` LEFT JOIN `s-r-user` 
		// ON `maintance`.`user_name`=`s-r-user`.`s-r-username` WHERE `maintance`.`tansaction_no`='$transactionId' AND  `maintance`.`user_name`='$userId'";

		// print_r($select);
		// die();
		$mnt_user_joinArr=array();

		$mnt_user_condtnArr['maintenance.tansaction_no']=$transactionId;
		$mnt_user_condtnArr['maintenance.user_name']=$userId;

		$mnt_user_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"maintenance.user_name=s-r-user.s-r-username", "type"=>"left");

		$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="s-r-user.status, s-r-user.s-r-fname, s-r-user.s-r-lname, DATE_FORMAT(maintenance.due_date,'%d %b %Y') AS duedate, maintenance.tansaction_no AS tansaction_no1, maintenance.voucher_name AS voucher_no1, maintenance.amount AS total, DATE_FORMAT(maintenance.end_day, '%d %b %Y') AS end_day, DATE_FORMAT(maintenance.start_day, '%d %b %Y') AS start_day, maintenance.due_amount, CONCAT(`s-r-user`.`s-r-floor`, `s-r-user`.`s-r-flat`) AS flatNo, maintenance.start_day AS biiEnd", $mnt_user_condtnArr, $likeCondtnArr=array(), $mnt_user_joinArr, $singleRow=TRUE, $mnt_user_orderByArr=array(), $mnt_user_groupByArr=array(), $mnt_user_whereInArray=array(), $mnt_user_customWhereArray=array(), $backTicks=TRUE);

		$get_value=$query['userdata'];

		if(!empty($get_value))
		{
			// $fire=mysqli_query($conn,$select);
			// $get_value=mysqli_fetch_assoc($fire);
			$result['flatNo']=$get_value['flatNo'];

			$result['status']=$get_value['status'];
			$result['s-r-fname']=$get_value['s-r-fname'];
			$result['s-r-lname']=$get_value['s-r-lname'];
			$result['bill_dt']=$get_value['start_day'];
			$result['duedate']=$get_value['duedate'];
			$result['total']=$get_value['total'];
			$result['principal_arrears']=$get_value['due_amount'];
			$result['tansaction_no1']=$get_value['tansaction_no1'];
			$result['voucher_no1']=$get_value['voucher_no1'];
			// $billDt=$get_value['date'];
			$billendDt=$get_value['biiEnd'];

			$result['intrestAmount']=0;

			// $data_qry=mysqli_query($conn,"SELECT `head_name` AS Particular, (SELECT `charges` FROM `member_maintenance_tbl` 
			// WHERE `memberId`='$userId' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('$fromdate' between 
			// `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('$todate' between 
			// `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` 
			// DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS charges FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC ");

			$mbr_mnt_condtnArr['maintenance_head.head_status']='Active';
	    	$mbr_mnt_orderByArr['maintenance_head.srid']='ASC';

	    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.head_name AS Particular, (SELECT member_maintenance_tbl.charges FROM ".$this->member_maintenance_tbl." WHERE member_maintenance_tbl.memberId='".$userId."' AND member_maintenance_tbl.maintenance_headId=maintenance_head.maint_headId AND ('".$fromdate."' between member_maintenance_tbl.from_date and member_maintenance_tbl.to_date) AND ('".$todate."' between member_maintenance_tbl.from_date and member_maintenance_tbl.to_date) ORDER BY member_maintenance_tbl.from_date DESC, member_maintenance_tbl.srid DESC LIMIT 1) AS charges", $mbr_mnt_condtnArr, $mbr_mnt_likeCondtnArr=array(), $mbr_mnt_joinArr=array(), $singleRow=FALSE, $mbr_mnt_orderByArr, $mbr_mnt_groupByArr=array(), $mbr_mnt_whereInArray=array(), $mbr_mnt_customWhereArray=array(), $backTicks=TRUE, $mbr_mnt_customOrWhereArray=array(), $mbr_mnt_orNotLikeArray=array());

			$data_qry=$query['userdata'];

			if(!empty($data_qry))
			{
				// while($headerdata=mysqli_fetch_assoc($data_qry))
				foreach($data_qry AS $headerdata)
				{
					$maint_perticular[]=$headerdata;
				}
				$total=0;
				foreach($maint_perticular as $key => $value)
				{
					if($value['charges']!="0")
					{
						//   $total+=$value['charges'];
						if($value['Particular']!="INT")
						{
							$result['MaintenanceParticular'][]=array('Particular'=>$value['Particular'],'Charges'=>$value['charges']);
							$total+=$value['charges'];
						}
						else
						{
							$result['intrestAmount']=$value['charges'];
						}
					}
				}
				//   $result['MaintenanceTotal']=$total;
				// $fetch_arres=mysqli_query($conn,"SELECT `advance_amount` FROM `members_advance_log` WHERE `creation_date`<'$billendDt' AND `user_id`='$userId' ORDER BY `creation_date` DESC LIMIT 1");

				$adv_amt_condtnArr['members_advance_log.creation_date <']=$billendDt;
				$adv_amt_condtnArr['members_advance_log.user_id']=$userId;
		    	$adv_amt_orderByArr['members_advance_log.creation_date']='DESC';

		    	$query=$this->Mcommon->getRecords($tableName=$this->members_advance_log_tbl, $colNames="members_advance_log.advance_amount", $adv_amt_condtnArr, $adv_amt_likeCondtnArr=array(), $adv_amt_joinArr=array(), $singleRow=TRUE, $adv_amt_orderByArr, $adv_amt_groupByArr=array(), $adv_amt_whereInArray=array(), $adv_amt_customWhereArray=array(), $backTicks=TRUE, $adv_amt_customOrWhereArray=array(), $adv_amt_orNotLikeArray=array());

				$fetch_arres=$query['userdata'];

				if(!empty($fetch_arres))
				{
					// $arr_data=$fetch_arres;
					$result['MaintenanceAdvance']=$fetch_arres['advance_amount'];
				}
				else
				{
					$result['MaintenanceAdvance']=0;  
				}

				$result['MaintenanceSubTotal']=$total;
				// $maint_intrest_fetch=mysqli_query($conn, "SELECT `penalty_percentage` FROM `penalty` WHERE `flag`='Yes'");

				$pntly_condtnArr['penalty.flag']='Yes';

		    	$query=$this->Mcommon->getRecords($tableName=$this->penalty_tbl, $colNames="penalty.penalty_percentage", $pntly_condtnArr, $pntly_likeCondtnArr=array(), $pntly_joinArr=array(), $singleRow=TRUE, $pntly_orderByArr=array(), $pntly_groupByArr=array(), $pntly_whereInArray=array(), $pntly_customWhereArray=array(), $backTicks=TRUE, $pntly_customOrWhereArray=array(), $pntly_orNotLikeArray=array());

				$maint_intrest=$query['userdata'];

				if(!empty($maint_intrest))
				{

					// $maint_intrest=mysqli_fetch_assoc($maint_intrest_fetch);
					$result['intrest']=$maint_intrest['penalty_percentage'];
					//  $result['intrestAmount']=$intrest;//value came from db
					$result['totalArrears']=$result['principal_arrears']+$result['intrestAmount'];
					$result['MaintenanceGrandTotal']=  $result['MaintenanceSubTotal']+$result['totalArrears'];
					//$result['MaintenanceParticular'][]=array('Particular'=>NULL,'Charges'=>NULL);
					$result['start_day']=$get_value['start_day'];
					$result['end_day']=$get_value['end_day'];

					$result['inwords']=$this->Mfunctional->inWords($result['MaintenanceGrandTotal']);

					$result['receipt']=NULL;
					// $reciptdatasql=mysqli_query($conn,"SELECT `transaction_no`,DATE_FORMAT(`s-date`,'%d %b %Y')  AS Recipt_date,`accounting_charges`,`payment_mode`,`cheque_no`,
					// (SELECT `tansaction_no` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS Billnum,(SELECT `start_day` FROM `maintance` WHERE `user_name`='$userId' ORDER BY `start_day` DESC LIMIT 1) AS billperiod FROM `accounting` WHERE `vaucher_name`='Reciept Voucher' AND `member_id`='$userId' AND `s-date`<'$billendDt' AND `cr_dr`='Cr' ORDER BY `a_id` DESC LIMIT 1");

					$rcpt_condtnArr['accounting.vaucher_name']='Reciept Voucher';
					$rcpt_condtnArr['accounting.member_id']=$userId;
					$rcpt_condtnArr['accounting.s-date < ']=$billendDt;
					$rcpt_condtnArr['accounting.cr_dr']='Cr';
			    	$rcpt_orderByArr['accounting.a_id']='DESC';

			    	$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="accounting.transaction_no, DATE_FORMAT(`accounting`.`s-date`, '%d %b %Y') AS Recipt_date, accounting.accounting_charges, accounting.payment_mode, accounting.cheque_no, (SELECT maintenance.tansaction_no FROM ".$this->maintenance_tbl." WHERE maintenance.user_name='".$userId."' ORDER BY maintenance.start_day DESC LIMIT 1) AS Billnum, (SELECT maintenance.start_day FROM ".$this->maintenance_tbl." WHERE maintenance.user_name='".$userId."' ORDER BY maintenance.start_day DESC LIMIT 1) AS billperiod", $rcpt_condtnArr, $rcpt_likeCondtnArr=array(), $rcpt_joinArr=array(), $singleRow=TRUE, $rcpt_orderByArr, $rcpt_groupByArr=array(), $rcpt_whereInArray=array(), $rcpt_customWhereArray=array(), $backTicks=TRUE, $rcpt_customOrWhereArray=array(), $rcpt_orNotLikeArray=array());

					$recipt_fetch=$query['userdata'];

					if(!empty($recipt_fetch))
					{
						// $recipt_fetch=mysqli_fetch_assoc($reciptdatasql);
						//print_r($recipt_fetch);
						$result['receipt']=array(
							"transaction_no"=>$recipt_fetch['transaction_no'],
							"Recipt_date"=>$recipt_fetch['Recipt_date'],
							"Recipt_amount"=>$recipt_fetch['accounting_charges'],
							"Recipt_amountWords"=>$this->Mfunctional->inWords($recipt_fetch['accounting_charges']),
							"payment_mode"=>$recipt_fetch['payment_mode'],
							"cheque_no"=>$recipt_fetch['cheque_no'],
							"Billnum"=>$recipt_fetch['Billnum'],
							"billperiod"=>date("F",strtotime($recipt_fetch['billperiod'])),
							"Cheque_date"=>date("d-m-Y",strtotime($recipt_fetch['billperiod']))
						);
					}
				}
			}
		}

		$this->data['bill_fetch']=$bill_fetch=$result;
		
    	// $this->data['view']="maintenance_management/maintenance_generation/print_maintenance_bill";
		$this->load->view("maintenance_management/maintenance_generation/print_maintenance_bill", $this->data);
    }

    public function edit_user_passbook()
    {
    	$userId=$this->input->get('username');
    	$todate=$this->input->get('todate');
    	$transactionId=$this->input->get('transactionId');
    	$fromdate=date("Y-m-d");

    	$this->data['username']=$userId;
    	$this->data['transactionId']=$transactionId;

    	$condtnArr['maintenance_head.head_status']='Active';
    	$orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="srid, maint_headId, head_name, (SELECT `charges` FROM ".$this->member_maintenance_tbl." WHERE `memberId`='".$userId."' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('".$fromdate."' between  `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('".$todate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS charges, (SELECT `from_date` FROM ".$this->member_maintenance_tbl." WHERE `memberId`='".$userId."' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('".$fromdate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('".$todate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS from_date, (SELECT `to_date` FROM ".$this->member_maintenance_tbl." WHERE `memberId`='".$userId."' AND `maintenance_headId`=`maintenance_head`.`maint_headId` AND ('".$fromdate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) AND ('".$todate."' between `member_maintenance_tbl`.`from_date` and `member_maintenance_tbl`.`to_date`) ORDER BY `member_maintenance_tbl`.`from_date` DESC,`member_maintenance_tbl`.`srid` DESC LIMIT 1) AS to_date", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$user_maint_charges=$query['userdata'];

	    $this->data['user_maint_charges']=$user_maint_charges;

    	// $this->data['view']="maintenance_management/maintenance_generation/edit_user_passbook";
		$this->load->view('maintenance_management/maintenance_generation/edit_user_passbook', $this->data);
    }

    public function save_user_passbook()
    {
    	$this->db->trans_start();

		$fromdate=$this->input->post('fromdate');
		$todate=$this->input->post('todate');
		$user_name=$this->input->post('user_name');
		$transactionId=$this->input->post('transactionId');
		unset($_POST['fromdate']);
		unset($_POST['todate']);
		unset($_POST['user_name']);
		unset($_POST['editpassbook']);

		$bill_total=0;
		foreach($_POST as $key => $value)
		{
			if($key!="editpassbook" && $key!="fromdate" && $key!="todate" && $key!="user_name" && $key!="transactionId")
			{ 
				$maintenance_headId=$key;
				if($value!="")
				{
					if(is_numeric($value))
						$charges=$value;
					else
						$charges=0;
				}
				else
					$charges=0;

				$bill_total+=$charges;

				// $result=edit_maintenanceheadersusercharges_passbook($userName,$societyKey,$user_name,$fromdate,$todate,$maintenance_headId,$charges);

				// function edit_maintenanceheadersusercharges_passbook($username,$soc_key,$userID,$fromdate,$todate,$maintenance_headId,$charges)

				// $data_qry=mysqli_query($conn,"UPDATE `member_maintenance_tbl` SET `charges`='$charges' WHERE `memberId`='$user_name' AND `maintenance_headId`='$maintenance_headId' AND `from_date`='$fromdate' AND `to_date`='$todate'");

				$mbr_mnt_UpdateArr=array(
					'charges'=>$charges,
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$mbr_mnt_condtnArr['member_maintenance_tbl.memberId']=$user_name;
				$mbr_mnt_condtnArr['member_maintenance_tbl.maintenance_headId']=$maintenance_headId;
				$mbr_mnt_condtnArr['member_maintenance_tbl.from_date']=$fromdate;
				$mbr_mnt_condtnArr['member_maintenance_tbl.to_date']=$todate;

				$query=$this->Mcommon->update($tableName=$this->member_maintenance_tbl, $mbr_mnt_UpdateArr, $mbr_mnt_condtnArr, $likeCondtnArr=array());
			}
		}
		// $result=edit_maintenancebill_passbook($userName,$societyKey,$user_name,$bill_total,$transactionId);

		// function edit_maintenancebill_passbook($username,$soc_key,$userID,$bill_total,$transactionId)

		// $data_qry=mysqli_query($conn,"UPDATE `maintance` SET `amount`='$bill_total' WHERE `tansaction_no`='$transactionId' AND `user_name`='$user_name'");

		$mnt_UpdateArr=array(
			'amount'=>$bill_total,
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$mnt_condtnArr['maintenance.tansaction_no']=$transactionId;
		$mnt_condtnArr['maintenance.user_name']=$user_name;

		$query=$this->Mcommon->update($tableName=$this->maintenance_tbl, $mnt_UpdateArr, $mnt_condtnArr, $likeCondtnArr=array());

		$mnt_status=$query['status'];
                                    
        if($mnt_status==TRUE)
        {
			$result="charges updated";
			// $acc_qry=mysqli_query($conn,"UPDATE `accounting` SET `accounting_charges`='$bill_total' WHERE `transaction_no`='$transactionId'");

			$acc_UpdateArr=array(
				'accounting_charges'=>$bill_total,
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$acc_condtnArr['accounting.transaction_no']=$transactionId;

			$query=$this->Mcommon->update($tableName=$this->accounting_tbl, $acc_UpdateArr, $acc_condtnArr, $likeCondtnArr=array());
            
        }

		if(empty($result))
		{
			$response='Charges not updated';
		}
		else
		{
			$response='Charges updated';
		}

		//echo "<script>window.open('passbook?member_id=$user_name','_self')</script>";

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Charges has not updated.');
			redirect(base_url()."maintenance_management/maintenance_generation/passbook/".$user_name, 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Charges has been updated successfully');
			redirect(base_url()."maintenance_management/maintenance_generation/passbook/".$user_name, 'refresh');
		}
    }
}


?>