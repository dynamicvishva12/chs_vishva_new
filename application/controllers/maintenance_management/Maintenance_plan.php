<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance_plan extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='MAINTENANCE_PLAN';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->maintenance_head_tbl=$socTables['maintenance_head_tbl'];
		$this->maintenance_head_charges_tbl=$socTables['maintenance_head_charges_tbl'];
		$this->member_maintenance_tbl=$socTables['member_maintenance_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Maintenance Plan';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Maintenance Management";
    	$breadcrumbArr[0]['link']=base_url()."maintenance_management/maintenance_plan/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Maintenance Plan";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	// $data_qry=mysqli_query($conn,"SELECT  `maint_headId`, `head_name` FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

    	$condtnArr['maintenance_head.head_status']='Active';
    	$orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$headers_view=$query['userdata'];

		$this->data['headers_view']=$headers_view;

		// $result1=mysqli_query($conn,"SELECT `maint_headId`, `head_name`, (SELECT `from_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS from_date,(SELECT `to_date` FROM `maintenance_head_charges` WHERE `maintenance_headId`=`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS to_date FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

		$chrgs_condtnArr['maintenance_head.head_status']='Active';
    	$chrgs_orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name, (SELECT maintenance_head_charges.from_date FROM ".$this->maintenance_head_charges_tbl." WHERE maintenance_head_charges.maintenance_headId=maintenance_head.maint_headId ORDER BY maintenance_head_charges.srid DESC LIMIT 1) AS from_date, (SELECT maintenance_head_charges.to_date FROM ".$this->maintenance_head_charges_tbl." WHERE maintenance_head_charges.maintenance_headId=maintenance_head.maint_headId ORDER BY maintenance_head_charges.srid DESC LIMIT 1) AS to_date", $chrgs_condtnArr, $chrgs_likeCondtnArr=array(), $chrgs_joinArr=array(), $singleRow=FALSE, $chrgs_orderByArr, $chrgs_groupByArr=array(), $chrgs_whereInArray=array(), $chrgs_customWhereArray=array(), $backTicks=TRUE, $chrgs_customOrWhereArray=array(), $chrgs_orNotLikeArray=array());

		$charges=$query['userdata'];

		$result=array();

		if(!empty($charges))
		{
			// while($data1=mysqli_fetch_assoc($result1))
			foreach($charges AS $data1)
			{
				$maint_head_name=$data1['head_name'];
				$maint_headId=$data1['maint_headId'];

				// $data_qry=mysqli_query($conn,"SELECT `charges`, `from_date`, `to_date`,`flag` FROM `maintenance_head_charges` WHERE `maintenance_headId`='$maint_headId' ORDER BY `srid` ASC");

				$chrgs1_condtnArr['maintenance_head_charges.maintenance_headId']=$maint_headId;
		    	$chrgs1_orderByArr['maintenance_head_charges.srid']='ASC';

		    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_charges_tbl, $colNames="maintenance_head_charges.charges, maintenance_head_charges.from_date, maintenance_head_charges.to_date, maintenance_head_charges.flag", $chrgs1_condtnArr, $chrgs1_likeCondtnArr=array(), $chrgs1_joinArr=array(), $singleRow=FALSE, $chrgs1_orderByArr, $chrgs1_groupByArr=array(), $chrgs1_whereInArray=array(), $chrgs1_customWhereArray=array(), $backTicks=TRUE, $chrgs1_customOrWhereArray=array(), $chrgs1_orNotLikeArray=array());

				$charges1=$query['userdata'];

				if(!empty($charges1))
				{
					// while($headerchargesdata=mysqli_fetch_assoc($data_qry))
					foreach($charges1 AS $headerchargesdata)
					{
						$result_s[$maint_headId][]=$headerchargesdata;
					}
					foreach($result_s[$maint_headId] as $key => $values)
					{
						$result[$key][]=$values;
					}
				}
			}
		}

		$headers_charges=$result;

		$this->data['headers_charges']=$headers_charges;

		$this->data['view']="maintenance_management/maintenance_plan/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function fixed()
    {
    	// $data_qry=mysqli_query($conn,"SELECT  `maint_headId`, `head_name` FROM `maintenance_head` WHERE `head_status`='Active' ORDER BY `srid` ASC");

    	$condtnArr['maintenance_head.head_status']='Active';
    	$orderByArr['maintenance_head.srid']='ASC';

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, maintenance_head.head_name", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$headers_view=$query['userdata'];

		$this->data['headers_view']=$headers_view;

		$this->load->view("maintenance_management/maintenance_plan/fixed", $this->data, TRUE);
	}

	public function rate()
    {
		$this->load->view("maintenance_management/maintenance_plan/rate", '', TRUE);
	}

	public function save_maintenance()
	{
		$this->db->trans_start();

		$fyear=( date('m') > 3) ? date('Y') + 1 : date('Y');
		$fyear=$fyear."-03-31";
		$year=str_split($fyear,4);
		$previous=$year['0']-1;
		$lastyear=date("Y-m-d");

		$result="";

		foreach($this->input->post() as $key => $value)
		{
			if($key!="save_maint")
			{ 
				$maintenance_headId=$key;
				$charges=$value;
				// $result=add_maintenanceheaderscharges($userName, $societyKey, $maintenance_headId, $charges, $lastyear, $fyear);

				// function add_maintenanceheaderscharges($username,$soc_key,$maintenance_headId,$charges,$lastyear,$fyear)

				// $query=mysqli_query($conn,"INSERT INTO `maintenance_head_charges`(`maintenance_headId`, `charges`, `from_date`, `to_date`) VALUES ('$maintenance_headId','$charges','$lastyear','$fyear')")or die(mysqli_error($conn));

				$mnt_head_chrgs_InsertArr=array();

				$mnt_head_chrgs_InsertArr[]=array(
					'maintenance_headId'=>$maintenance_headId,
					'charges'=>$charges,
					'from_date'=>$lastyear,
					'to_date'=>$fyear,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->maintenance_head_charges_tbl, $mnt_head_chrgs_InsertArr, $returnType="");

				$mnt_head_chrgs_InsertStatus=$query['status'];

				if($mnt_head_chrgs_InsertStatus==TRUE) 
				{
					// $fetch1=mysqli_query($conn,"UPDATE `maintenance_head_charges` SET `flag`='No' WHERE `from_date`<'$lastyear'");

					$mnt_head_chrgs_UpdateArr=array(
						'flag'=>'No',
						'updatedBy'=>$this->user_name,
						'updatedDatetime'=>$this->curr_datetime
					);

					$mnt_head_chrgs_updt_condtnArr['maintenance_head_charges.from_date < ']=$lastyear;

					$query=$this->Mcommon->update($tableName=$this->maintenance_head_charges_tbl, $mnt_head_chrgs_UpdateArr, $mnt_head_chrgs_updt_condtnArr, $likeCondtnArr=array());

					// $usern = mysqli_query($conn, "SELECT * FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

					$user_orNotLikeArray['s-r-user.disabled_user']='disabled';
					$user_condtnArr['s-r-user.s-r-approve']='approve';

					$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr=array(), $user_groupByArr=array(), $user_whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $user_customOrWhereArray=array(), $user_orNotLikeArray);

					$user_info=$query['userdata'];

					if(!empty($user_info))
					{
						// while($row = mysqli_fetch_assoc($usern))
						foreach($user_info AS $row) 
						{
							$username = $row['s-r-username'];
							// $tnst_qry=mysqli_query($conn, "INSERT INTO `member_maintenance_tbl`(`memberId`, `maintenance_headId`, `charges`, `from_date`, `to_date`) VALUES ('$username','$maintenance_headId','$charges','$lastyear','$fyear')");

							$mbr_mnt_InsertArr=array();

							$mbr_mnt_InsertArr[]=array(
								'memberId'=>$username,
								'maintenance_headId'=>$maintenance_headId,
								'charges'=>$charges,
								'from_date'=>$lastyear,
								'to_date'=>$fyear,
								'createdBy'=>$this->user_name,
								'createdDatetime'=>$this->curr_datetime
							);

							$query=$this->Mcommon->insert($tableName=$this->member_maintenance_tbl, $mbr_mnt_InsertArr, $returnType="");

							// $fetch12=mysqli_query($conn,"UPDATE `member_maintenance_tbl` SET `flag`='No' WHERE `from_date`<'$lastyear'");

							$mbr_mnt_UpdateArr=array(
								'flag'=>'No',
								'updatedBy'=>$this->user_name,
								'updatedDatetime'=>$this->curr_datetime
							);

							$mbr_mnt_updt_condtnArr['member_maintenance_tbl.from_date < ']=$lastyear;

							$query=$this->Mcommon->update($tableName=$this->member_maintenance_tbl, $mbr_mnt_UpdateArr, $mbr_mnt_updt_condtnArr, $likeCondtnArr=array());

							$result="Charges enter sucessfully";
						}
					}
				}
				else
				{
					$this->session->set_flashdata('error_msg', 'Something went wrong!!!');
				}
			}   
		}

		if(empty($result))
		{
			$response='charges not added';
		}
		else
		{
			$response='Charges added';
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Fixed Maintenance has not added.');
			redirect(base_url()."maintenance_management/maintenance_plan/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Fixed Maintenance has been added successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription=$response, $userId=$this->user_name);
			redirect(base_url()."maintenance_management/maintenance_plan/list", 'refresh');
		}     
	}

	public function add_maintenance_head()
	{
		$this->form_validation->set_rules('Headname', 'Head Name', 'trim|required');
		$this->form_validation->set_rules('Headcharges', 'Head Charges', 'trim|required');
		$this->form_validation->set_rules('maint_type', 'Maintenance Type', 'trim|required');

		if($this->form_validation->run() == FALSE)
		{
			$this->list();
		}
		else
		{
			$this->db->trans_start();

			$log_date=date('Y-m-d');
		    $headname=$this->input->post('Headname');
		    $Headcharges=$this->input->post('Headcharges');
		    $headtype=$this->input->post('maint_type');

		    // $result=add_maintenancehead($userName,$societyKey,$Headname,$maint_type,$Headcharges);

		    // function add_maintenancehead($username,$soc_key,$headname,$headtype,$Headcharges)

			// include '../database_connection/s-conn.php';
			// $fetch=mysqli_query($conn,"SELECT `maint_headId` FROM `maintenance_head` ORDER BY `srid` DESC LIMIT 1 ");

			// $condtnArr['maintenance_head.head_status']='Active';
	    	$mnt_hd_orderByArr['maintenance_head.srid']='DESC';

	    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId", $mnt_hd_condtnArr=array(), $mnt_hd_likeCondtnArr=array(), $mnt_hd_joinArr=array(), $singleRow=TRUE, $mnt_hd_orderByArr, $mnt_hd_groupByArr=array(), $mnt_hd_whereInArray=array(), $mnt_hd_customWhereArray=array(), $backTicks=TRUE, $mnt_hd_customOrWhereArray=array(), $mnt_hd_orNotLikeArray=array());

			$headers_data=$query['userdata'];

			$id=1;

			if(!empty($headers_data))
			{
				$temp_id=explode("_", $headers_data['maint_headId']);
				$id=$temp_id[1]+1;
			}

			$headId="mh_0".$id;
			// $insert_qry=mysqli_query($conn,"INSERT INTO `maintenance_head`(`maint_headId`, `head_name`, `type`,`date_creation`) VALUES ('$headId','$headname','$headtype','$log_date')");

			$mnt_head_InsertArr=array();

			$mnt_head_InsertArr[]=array(
				'maint_headId'=>$headId,
				'head_name'=>$headname,
				'type'=>$headtype,
				'date_creation'=>$log_date,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->maintenance_head_tbl, $mnt_head_InsertArr, $returnType="");

			$mnt_head_InsertStatus=$query['status'];

			if($mnt_head_InsertStatus==TRUE)
			{
				$fyear=( date('m') > 3) ? date('Y') + 1 : date('Y');
				$fyear=$fyear."-03-31";

				// $head_dtfetch = mysqli_query($conn, "SELECT `from_date`, `flag`  FROM `maintenance_head_charges` GROUP BY `from_date`");

				$chrgs1_groupByArr=array('maintenance_head_charges.from_date');

		    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_charges_tbl, $colNames="maintenance_head_charges.from_date, maintenance_head_charges.flag", $chrgs1_condtnArr=array(), $chrgs1_likeCondtnArr=array(), $chrgs1_joinArr=array(), $singleRow=FALSE, $chrgs1_orderByArr=array(), $chrgs1_groupByArr, $chrgs1_whereInArray=array(), $chrgs1_customWhereArray=array(), $backTicks=TRUE, $chrgs1_customOrWhereArray=array(), $chrgs1_orNotLikeArray=array());

				$charges1=$query['userdata'];

				if(!empty($charges1))
				{
					// while($head_date = mysqli_fetch_assoc($head_dtfetch)) 
					foreach($charges1 AS $head_date) 
					{
						$lastyear=$head_date['from_date'];
						$flag=$head_date['flag'];
						// $insert_qry=mysqli_query($conn,"INSERT INTO `maintenance_head_charges`(`maintenance_headId`, `charges`, `from_date`, `to_date`, `flag`) VALUES ('$headId','$Headcharges','$lastyear','$fyear','$flag')");

						$mnt_head_chrgs_InsertArr=array();

						$mnt_head_chrgs_InsertArr[]=array(
							'maintenance_headId'=>$headId,
							'charges'=>$Headcharges,
							'from_date'=>$lastyear,
							'to_date'=>$fyear,
							'flag'=>$flag,
							'createdBy'=>$this->user_name,
							'createdDatetime'=>$this->curr_datetime
						);

						$query=$this->Mcommon->insert($tableName=$this->maintenance_head_charges_tbl, $mnt_head_chrgs_InsertArr, $returnType="");

						$result="Charges enter sucessfully";
					}
				}

				// $usern = mysqli_query($conn, "SELECT * FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

				$user_orNotLikeArray['s-r-user.disabled_user']='disabled';
				$user_condtnArr['s-r-user.s-r-approve']='approve';

				$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr=array(), $user_groupByArr=array(), $user_whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $user_customOrWhereArray=array(), $user_orNotLikeArray);

				$user_info=$query['userdata'];

				if(!empty($user_info))
				{
					// while($row = mysqli_fetch_assoc($usern))
					foreach($user_info AS $row) 
					{
						$username = $row['s-r-username'];

						// $member_head_fetch = mysqli_query($conn, "SELECT `from_date`,`flag` FROM `member_maintenance_tbl` WHERE `memberId`='$username'  GROUP BY `from_date` ORDER BY `from_date` ASC");

						$mbr_mnt_condtnArr['member_maintenance_tbl.memberId']=$username;
				    	$mbr_mnt_orderByArr['member_maintenance_tbl.from_date']='ASC';
				    	$mbr_mnt_groupByArr=array('member_maintenance_tbl.from_date');

				    	$query=$this->Mcommon->getRecords($tableName=$this->member_maintenance_tbl, $colNames="member_maintenance_tbl.from_date, member_maintenance_tbl.flag", $mbr_mnt_condtnArr, $mbr_mnt_likeCondtnArr=array(), $mbr_mnt_joinArr=array(), $singleRow=FALSE, $mbr_mnt_orderByArr, $mbr_mnt_groupByArr, $mbr_mnt_whereInArray=array(), $mbr_mnt_customWhereArray=array(), $backTicks=TRUE, $mbr_mnt_customOrWhereArray=array(), $mbr_mnt_orNotLikeArray=array());

						$mbr_mnt_data=$query['userdata'];

						if(!empty($mbr_mnt_data))
						{
							// while($member_head_date = mysqli_fetch_assoc($member_head_fetch)) 
							foreach($mbr_mnt_data AS $member_head_date) 
							{
								$lastyear=$member_head_date['from_date'];
								$flag=$member_head_date['flag'];
								// $tnst_qry=mysqli_query($conn, "INSERT INTO `member_maintenance_tbl`(`memberId`, `maintenance_headId`, `charges`, `from_date`, `to_date`, `flag`) VALUES ('$username','$headId','$Headcharges','$lastyear','$fyear','$flag')");

								$mbr_mnt_InsertArr=array();

								$mbr_mnt_InsertArr[]=array(
									'memberId'=>$username,
									'maintenance_headId'=>$headId,
									'charges'=>$Headcharges,
									'from_date'=>$lastyear,
									'to_date'=>$fyear,
									'flag'=>$flag,
									'createdBy'=>$this->user_name,
									'createdDatetime'=>$this->curr_datetime
								);

								$query=$this->Mcommon->insert($tableName=$this->member_maintenance_tbl, $mbr_mnt_InsertArr, $returnType="");

								$result="Charges enter sucessfully";
							}
						}
					}
				}
			}

			$this->db->trans_complete();

			if($this->db->trans_status() === FALSE)
			{
				$this->session->set_flashdata('error_msg', 'Something went wrong! Maintenance Head has not added.');
				redirect(base_url()."maintenance_management/maintenance_plan/list", 'refresh');
			}
			else
			{
				$this->session->set_flashdata('success_msg', 'Maintenance Head has been added successfully');
				$this->Mlogger->log($logModule=$this->logModule, $logDescription='Maintenance Head Added', $userId=$this->user_name);
				redirect(base_url()."maintenance_management/maintenance_plan/list", 'refresh');
			}   
		}  
	}
}

?>