<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penalty extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='PENALTY';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->sub_standard_tbl=$socTables['sub_standard_tbl'];
		$this->penalty_tbl=$socTables['penalty_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Penalty';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Penalty";
    	$breadcrumbArr[0]['link']=base_url()."maintenance_management/penalty/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Penalty List";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
  //   	$result=mysqli_query($conn,"SELECT `Sinking Fund`, `Parking Charges`, `Repairs and Maintenence Fees`, `Water Charges`, `Electricity Charges` FROM `sub-standard` LIMIT 1 ");
		// $data=mysqli_fetch_assoc($result);

    	$query=$this->Mcommon->getRecords($tableName=$this->sub_standard_tbl, $colNames="`sub-standard`.`Sinking Fund`, `sub-standard`.`Parking Charges`, `sub-standard`.`Repairs and Maintenence Fees`, `sub-standard`.`Water Charges`, `sub-standard`.`Electricity Charges`, `sub-standard`.`from_date`", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=FALSE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$data=$query['userdata'];

		$this->data['data']=$data;

		// $result=mysqli_query($conn,"SELECT `Sinking Fund`, `Parking Charges`, `Repairs and Maintenence Fees`, `Water Charges`, `Electricity Charges`,`from_date` FROM `penalty` ORDER BY `penalty`.`s-id` DESC");

		// $res_orderByArr['`penalty`.`s-id`']='DESC';

		// $query=$this->Mcommon->getRecords($tableName=$this->penalty_tbl, $colNames="`penalty`.`Sinking Fund`, `penalty`.`Parking Charges`, `penalty`.`Repairs and Maintenence Fees`, `penalty`.`Water Charges`, `penalty`.`Electricity Charges`, `penalty`.`from_date`", $res_condtnArr=array(), $res_likeCondtnArr=array(), $res_joinArr=array(), $singleRow=TRUE, $res_orderByArr, $res_groupByArr=array(), $res_whereInArray=array(), $res_customWhereArray=array(), $backTicks=FALSE, $res_customOrWhereArray=array(), $res_orNotLikeArray=array());

		// $result=$query['userdata'];

		$result=array();

		$this->data['result']=$result;

		$this->data['view']="maintenance_management/penalty/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function save()
    {
    	$this->db->trans_start();

    	$posted_data=$this->input->post();

		// $result=mysqli_query($conn,"SELECT `Sinking Fund`, `Parking Charges`, `Repairs and Maintenence Fees`, `Water Charges`, `Electricity Charges` FROM `sub-standard` LIMIT 1 ");
		// $data=mysqli_fetch_assoc($result);

    	$query=$this->Mcommon->getRecords($tableName=$this->sub_standard_tbl, $colNames="`sub-standard`.`Sinking Fund`, `sub-standard`.`Parking Charges`, `sub-standard`.`Repairs and Maintenence Fees`, `sub-standard`.`Water Charges`, `sub-standard`.`Electricity Charges`", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=FALSE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$data=$query['userdata'];
		
		if(!empty($data))
		{
			foreach($data as $key=> $name)
			{
				$key=str_replace(" ","_","$key");
				// if(isset($this->input->post($key))) 
				if(array_key_exists($key, $posted_data))
				{
					// $this->input->post($key)=$this->input->post('Penalty');
					$_POST[$key]=$_POST['Penalty'];
				}
			}
		}

		//print_r($_POST);
		// if(isset($_POST['Sinking_Fund'])) 
		if(array_key_exists('Sinking_Fund', $posted_data))
		{
			$Sinking_Fund=$this->input->post('Sinking_Fund');
		}
		else
		{
			$Sinking_Fund=0;
		}

		// if(isset($_POST['Parking_Charges'])) 
		if(array_key_exists('Parking_Charges', $posted_data))
		{
			$Parking_Charges=$this->input->post('Parking_Charges');
		}
		else
		{
			$Parking_Charges=0;
		}

		// if(isset($_POST['Repairs_and_Maintenence_Fees'])) 
		if(array_key_exists('Repairs_and_Maintenence_Fees', $posted_data))
		{
			$Repairs_and_Maintenence_Fees=$this->input->post('Repairs_and_Maintenence_Fees');
		}
		else
		{
			$Repairs_and_Maintenence_Fees=0;
		}

		// if(isset($_POST['Water_Charges'])) 
		if(array_key_exists('Water_Charges', $posted_data))
		{
			$Water_Charges=$this->input->post('Water_Charges');
		}
		else
		{
			$Water_Charges=0;
		}

		// if(isset($_POST['Electricity_Charges'])) 
		if(array_key_exists('Electricity_Charges', $posted_data))
		{
			$Electricity_Charges=$this->input->post('Electricity_Charges');
		}
		else
		{
			$Electricity_Charges=0;
		}

		$fyear=( date('m') > 3) ? date('Y') + 1 : date('Y');
		$fyear=$fyear."-03-31";
		$year=str_split($fyear,4);
		$previous=$year['0']-1;
		$lastyear=date("Y-m-d");

		$type=$this->input->post('type');

		// $insert_qry=mysqli_query($conn,"INSERT INTO `penalty`(`penalty_percentage`, `from_date`, `to_date`, `type`, `flag`) VALUES ('$Sinking_Fund','$lastyear','$fyear','$type','Yes')")or die(mysqli_error($conn));

		$pnlty_InsertArr[]=array(
			'penalty_percentage'=>$Sinking_Fund,
			'from_date'=>$lastyear,
			'to_date'=>$fyear,
			'type'=>$type,
			'flag'=>'Yes',
			'createdBy'=>$this->user_name,
			'createdDatetime'=>$this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->penalty_tbl, $pnlty_InsertArr, $returnType="");

		$pnlty_InsertStatus=$query['status'];

		// if ($insert_qry) 
		if($pnlty_InsertStatus==TRUE) 
		{
			// $fetch1=mysqli_query($conn,"UPDATE `penalty` SET `flag`='No' WHERE `from_date`<'$lastyear'");

			$pnlty_UpdateArr=array(
				'flag'=>'No',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$pnlty_condtnArr['penalty.from_date < ']=$lastyear;

			$query=$this->Mcommon->update($tableName=$this->penalty_tbl, $pnlty_UpdateArr, $pnlty_condtnArr, $likeCondtnArr=array());
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Penalty has not added.');
			redirect(base_url()."maintenance_management/penalty/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Penalty has been added successfully');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription='Penalty Added Successfully', $userId=$this->user_name);
			redirect(base_url()."maintenance_management/penalty/list", 'refresh');
		}     
    }
}

?>