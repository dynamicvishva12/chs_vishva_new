<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Active_member extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';			
		$this->data['navbar_menu']='layouts/include/navbar_menu';	
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='ACTIVE_MEMBER';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function list()
    {
    	$this->data['page_section']='Active Members';

    	$breadcrumbArr[0]['name']="Member Management";
    	$breadcrumbArr[0]['link']=base_url()."member_management/active_member/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Active Members";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	// $fetch = mysqli_query($conn,"SELECT s-r-fname AS usrFname, s-r-lname AS usrLname, s-r-username AS usrId, s-r-mobile AS usrMobile, s-r-appartment AS usrAppartment, s-r-wing AS usrWing, s-r-flat AS usrFlat,s-r-email AS usrMail,(SELECT d-body FROM directory WHERE important=s-r-user.s-r-email AND directory_type='type1' AND dir_status='Active' LIMIT 1) AS designation FROM s-r-user WHERE s-r-approve='approve' AND s-r-active='active'");

    	// $fetch=mysqli_query($conn,"SELECT * FROM `tents` ORDER BY `tents`.`id` DESC")or die(mysqli_error($conn));

    	$condtnArr['s-r-user.s-r-approve']='approve';
    	$condtnArr['s-r-user.s-r-active']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname AS usrFname, s-r-user.s-r-lname AS usrLname, s-r-user.s-r-username AS usrId, s-r-user.s-r-mobile AS usrMobile, s-r-user.s-r-appartment AS usrAppartment, s-r-user.s-r-wing AS usrWing, s-r-user.s-r-flat AS usrFlat, s-r-user.s-r-email AS usrMail, s-r-user.active_reason, (SELECT `directory`.`d-body` FROM `".$this->society_db."`.`directory` WHERE `directory`.`important`=`s-r-user`.`s-r-email` AND `directory`.`directory_type`='type1' AND `directory`.`status`='Active' LIMIT 1) AS designation", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		$this->data['view']="member_management/active_member/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function change_status()
    {
    	$this->db->trans_start();
		// print_r($_GET);
		$memId=$this->input->get('memId');
		$memstatus=$this->input->get('memstatus');
		$userMail=$this->input->get('usrMail');
		$callurl=$this->input->get('url');

		if(!empty($this->society_key) && !empty($this->user_name) && !empty($memId) && !empty($memstatus) && !empty($userMail))
		{
			$query=$this->Msociety->verify_society($this->society_key);

			$soc_check=$query['userdata'];
			
			if(!empty($soc_check))
			{
				$soc_key=$soc_check['society_key'];
				$soc_name=$soc_check['society_name'];

				$query=$this->Msociety->verify_society_user($this->user_name, $this->society_key);

	    		$uservalidate=$query['userdata'];

				if(empty($uservalidate))
				{
					$this->session->set_flashdata('warning_msg', 'User ID Invalid');
					redirect(base_url()."auth/logout", 'refresh');
				}
				else
				{
					// $result=Society_member_status($userName, $soc_key, $memstatus, $memId, $userMail);
					// function Society_member_status($username, $soc_key, $usr_status, $memId, $memMail)
					if($memstatus=="approve")
					{
					    // $fetch = mysqli_query($conn,"UPDATE `s-r-user` SET `s-r-approve`='approve' WHERE `s-r-username`='$memId'");
					    $memUpdateArr=array(
					    	's-r-approve'=>'approve',
					    	'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
					    );
					}
					elseif($memstatus=="reject")
					{
					    // $fetch = mysqli_query($conn,"UPDATE `s-r-user` SET `reject`='reject' WHERE `s-r-username`='$memId'");
					    $memUpdateArr=array(
					    	'reject'=>'reject',
					    	'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
					    );
					}
					elseif($memstatus=="inactive")
					{
					    // $fetch = mysqli_query($conn,"UPDATE `s-r-user` SET `s-r-active`='inactive' WHERE `s-r-username`='$memId'");
					    $memUpdateArr=array(
					    	's-r-active'=>'inactive',
					    	'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
					    );
					}
					elseif($memstatus=="disabled")
					{
					    // $fetch = mysqli_query($conn,"UPDATE `s-r-user` SET `s-r-active`='inactive', `disabled_user`='disabled' WHERE `s-r-username`='$memId'");
					    $memUpdateArr=array(
					    	's-r-active'=>'inactive',
					    	'disabled_user'=>'disabled',
					    	'updatedBy'=>$this->user_name,
							'updatedDatetime'=>$this->curr_datetime
					    );
					}

					$updt_condtnArr['s-r-user.s-r-username']=$memId;

					$query=$this->Mcommon->update($tableName=$this->s_r_user_tbl, $memUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

					$memUpdtStatus=$query['status'];

					$result="";

					if($memUpdtStatus==TRUE)
					{
						$result="membership status has been ".$memstatus;

						$subject="Membership Request ";
			            $message="Dear User, 


						Your Membership request is ".$memstatus.", 

						Best Regards,
						CHSVishva

						Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

			            $alt_message="This is the body in plain text for non-HTML mail clients";

			            $email="dynamicvishvateam@gmail.com";

			            $mailDataArr['to']=$email;
			            $mailDataArr['subject']=$subject;
			            $mailDataArr['alt_message']=$alt_message;
			            $mailDataArr['message']=$message;

			            $this->load->library('Email_sending');

			            $response=$this->email_sending->send($mailDataArr);

			            $responseStatus=$response['status'];
			            $responseMsg=$response['message'];

			            $this->load->library('Push_notify');

			            $notifyArr['userType']='user app';
			            $notifyArr['userName']='';
			            $notifyArr['title']='Membership status';
			            $notifyArr['body']='Your access to account has been denied. Please contact any of committee member for further details.';
			            $notifyArr['pushType']='logout';

			            $notifyRes=$this->push_notify->notification($notifyArr);

			            $this->Mlogger->log($logModule=$this->logModule, $logDescription=$result, $userId=$this->user_name);
					}

					if(empty($result))
					{
						$this->session->set_flashdata('error_msg', 'Something went wrong! Membership status not changed.');
					}
					else
					{
						$this->session->set_flashdata('success_msg', 'Membership status changed successfully.');
					}
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Society key invalid');
				redirect(base_url()."auth/logout", 'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."member_management/active_member/list", 'refresh');
		}
		else
		{
			redirect(base_url()."member_management/active_member/list", 'refresh');
		}      
    }
}

?>