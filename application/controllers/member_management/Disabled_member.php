<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disabled_member extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';																				
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='DISABLED_MEMBER';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->tents_tbl=$socTables['tents_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function list()
    {
    	$this->data['page_section']='Disabled Members';

    	$breadcrumbArr[0]['name']="Member Management";
    	$breadcrumbArr[0]['link']=base_url()."member_management/disabled_member/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Disabled Members";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	// $fetch=mysqli_query($conn,"SELECT * FROM `s-r-user` WHERE `s-r-approve`='notapprove'  ORDER BY `s-r-user`.`s-r-id` DESC");

    	$condtnArr['s-r-user.disabled_user']='disabled';

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-username, s-r-user.s-r-mobile, s-r-user.s-r-appartment, s-r-user.s-r-wing, s-r-user.s-r-floor, s-r-user.s-r-email, s-r-user.disabled_reason", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		$this->data['view']="member_management/disabled_member/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }
}

?>