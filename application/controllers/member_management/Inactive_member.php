<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inactive_member extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';																			
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='INACTIVE_MEMBER';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->booking_tbl=$socTables['booking_tbl'];
		$this->booking_slot_tbl=$socTables['booking_slot_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function list()
    {
    	$this->data['page_section']='Inactive Members';

    	$breadcrumbArr[0]['name']="Member Management";
    	$breadcrumbArr[0]['link']=base_url()."member_management/inactive_member/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Inactive Members";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;
    	// $fetch=mysqli_query($conn,"SELECT * FROM `s-r-user` WHERE `s-r-active`='inactive' ORDER BY `s-r-user`.`s-r-id` DESC");

    	// $condtnArr['s-r-user.s-r-approve']='approve';
    	$condtnArr['s-r-user.s-r-active']='inactive';
    	$orderByArr['s-r-user.s-r-id']='DESC';

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-username, s-r-user.s-r-username, s-r-user.s-r-mobile, s-r-user.s-r-appartment, s-r-user.s-r-wing, s-r-user.s-r-floor, s-r-user.s-r-email, s-r-user.inactive_reason", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		$this->data['view']="member_management/inactive_member/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

	public function approve_confirm()
	{
		$this->sec_data['id']=$id=$this->input->get('id');
		$this->sec_data['url']=$url=$this->input->get('url');
		$this->sec_data['status']=$status=$this->input->get('status');
		$this->sec_data['bookusr']=$bookusr=$this->input->get('bookusr');
		$this->load->view('member_management/inactive_member/approve_confirm', $this->sec_data);
	}

	public function change_status()
	{
		$this->db->trans_start();

		if(empty($this->input->post()))
			redirect(base_url()."member_management/inactive_member/list", 'refresh');

		$memId=$this->input->post('id');
		$reason=$this->input->post('reason');

		// $result=change_booking_status($userName, $societyKey, $status, $userId, $bookingId, $reason);
		// change_booking_status($username,$soc_key,$status,$userId,$bookingId,$reason)

		$reason=str_replace("'","\'",$reason);

		$mem_UpdateArr=array(
			's-r-active'=>'active',
			'active_reason'=>$reason,
			'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
		);

		$mem_updt_condtnArr['s-r-user.s-r-username']=$memId;

		$query=$this->Mcommon->update($tableName=$this->s_r_user_tbl, $mem_UpdateArr, $mem_updt_condtnArr, $likeCondtnArr=array());

		if($query['status']==TRUE)
		{
			$result="Status updated";

            $usr_condtnArr['`s-r-user`.`s-r-username`']=$memId;

            $query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-email", $usr_condtnArr, $usr_likeCondtnArr=array(), $usr_joinArr=array(), $singleRow=FALSE, $usr_orderByArr=array(), $usr_groupByArr=array(), $usr_whereInArray=array(), $usr_customWhereArray=array(), $backTicks=TRUE);

			$usr_data=$query['userdata'];

			if(!empty($usr_data))
			{
				foreach($usr_data AS $e_dt)
				{
					$subject="Online Website Membership ";
		            $message="Dear User, 


					Online Website Membership is Activated

					Best Regards,
					CHSVishva

					Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

		            $alt_message="This is the body in plain text for non-HTML mail clients";

		            $email=$e_dt['s-r-email'];

		            $mailDataArr['to']=$email;
		            $mailDataArr['subject']=$subject;
		            $mailDataArr['alt_message']=$alt_message;
		            $mailDataArr['message']=$message;

		            $this->load->library('Email_sending');

		            $response=$this->email_sending->send($mailDataArr);

		            $responseStatus=$response['status'];
		            $responseMsg=$response['message'];
				}
			}
		}

		if(empty($result))
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Status updated.');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Status has been updated successfully.');
			$this->Mlogger->log($logModule=$this->logModule, $logDescription="Member has been active successfully", $userId=$this->user_name);
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong!');
			redirect(base_url()."member_management/inactive_member/list", 'refresh');
		}
		else
		{
			redirect(base_url()."member_management/inactive_member/list", 'refresh');
		}   
	}
}

?>