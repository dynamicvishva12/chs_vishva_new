<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant_member extends Society_core
{
    public function __construct()
    {
		parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';																				
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='TENANT_MEMBER';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->tents_tbl=$socTables['tents_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function list()
    {
    	// $fetch=mysqli_query($conn,"SELECT * FROM `tents` ORDER BY `tents`.`id` DESC")or die(mysqli_error($conn));
    	$this->data['page_section']='Tenant Members';

    	$breadcrumbArr[0]['name']="Member Management";
    	$breadcrumbArr[0]['link']=base_url()."member_management/tenant_member/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Tenant Members";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$orderByArr['tents.id']='DESC';

    	$query=$this->Mcommon->getRecords($tableName=$this->tents_tbl, $colNames="tents.id, tents.name, tents.last_name, tents.username, tents.owner, tents.s_email, tents.contact, tents.file, tents.isActive, tents.reject_reason, tents.reject, tents.accept_reason", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

		$member_data=$query['userdata'];

		$this->data['member_data']=$member_data;

		$this->data['view']="member_management/tenant_member/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function approve_confirm()
	{
		$this->db->trans_start();

		$id=$this->input->post('id');
		$reason=$this->input->post('reason');

		// $Delete=mysqli_query($conn,"UPDATE `tents` SET `isActive`='Active',`accept_reason` = '$reason',`reject_reason` = '' WHERE `id`='$id'")or die(mysqli_error($conn));

		$updateArr=array(
	    	'isActive'=>'Active',
	    	'accept_reason'=>$reason,
	    	'reject_reason'=>'',
	    	'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
	    );

		$updt_condtnArr['tents.id']=$id;

		$query=$this->Mcommon->update($tableName=$this->tents_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$updtStatus=$query['status'];

		if($updtStatus==TRUE) 
		{
			// $bfetch=mysqli_query($conn,"SELECT * FROM `tents` WHERE `id`='$id'");
			// $buser= mysqli_fetch_assoc($bfetch);

			$condtnArr['tents.id']=$id;

	    	$query=$this->Mcommon->getRecords($tableName=$this->tents_tbl, $colNames="tents.owner", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

			$buser=$query['userdata'];

			if(!empty($buser))
			{
				// $ufetch=mysqli_query($conn,"SELECT `s-r-email` FROM `s-r-user` WHERE `s-r-username`='".$buser['owner']."' ")or die(mysqli_error($conn));

				$userCondtnArr['s-r-user.s-r-username']=$buser['owner'];

		    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-email", $usercondtnArr, $userlikeCondtnArr=array(), $userjoinArr=array(), $singleRow=TRUE, $userorderByArr=array(), $usergroupByArr=array(), $userwhereInArray=array(), $usercustomWhereArray=array(), $userbackTicks=TRUE, $usercustomOrWhereArray=array(), $userorNotLikeArray=array());

				$usermail=$query['userdata'];

				if(!empty($usermail))
				{
					$usermailid=$usermail['s-r-email'];

					$subject="Online Website tenants Membership ";
		            $message="Dear User, 


					Online Website tenants Membership is Accepted
					due to ".$reason." .

					Best Regards,
					CHSVishva

					Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

		            $alt_message="This is the body in plain text for non-HTML mail clients";

		            $email=$usermailid;

		            $mailDataArr['to']=$email;
		            $mailDataArr['subject']=$subject;
		            $mailDataArr['alt_message']=$alt_message;
		            $mailDataArr['message']=$message;

		            $this->load->library('Email_sending');

		            $response=$this->email_sending->send($mailDataArr);

		            $responseStatus=$response['status'];
		            $responseMsg=$response['message'];

					echo $this->Mfunctional->SALERT("sucess");
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."member_management/tenant_member/list", 'refresh');
		}
		else
		{
			redirect(base_url()."member_management/tenant_member/list", 'refresh');
		}      
	}

	public function reject()
	{
		$this->db->trans_start();
		
		$id=$this->input->post('id');
		$reason=$this->input->post('reason');

		// $Reject=mysqli_query($conn,"UPDATE `tents` SET `reject`='reject',`reject_reason` = '$reason',`accept_reason` = '' WHERE `id`='$Reject_id'")or die(mysqli_error($conn));

		$updateArr=array(
	    	'reject'=>'reject',
	    	'reject_reason'=>$reason,
	    	'accept_reason'=>'',
	    	'updatedBy'=>$this->user_name,
			'updatedDatetime'=>$this->curr_datetime
	    );

		$updt_condtnArr['tents.id']=$id;

		$query=$this->Mcommon->update($tableName=$this->tents_tbl, $updateArr, $updt_condtnArr, $likeCondtnArr=array());

		$updtStatus=$query['status'];

		if($updtStatus==TRUE) 
		{
			// $bfetch=mysqli_query($conn,"SELECT * FROM `tents` WHERE `id`='$Reject_id'");
			// $buser= mysqli_fetch_assoc($bfetch);

			$condtnArr['tents.id']=$id;

	    	$query=$this->Mcommon->getRecords($tableName=$this->tents_tbl, $colNames="tents.owner", $condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=TRUE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE, $customOrWhereArray=array(), $orNotLikeArray=array());

			$buser=$query['userdata'];

			if(!empty($buser))
			{
				// $fetch=mysqli_query($conn,"SELECT * FROM `email_account`")or die(mysqli_error($conn));
				// $email_data=mysqli_fetch_assoc($fetch);

				$userCondtnArr['s-r-user.s-r-username']=$buser['owner'];

		    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-email", $usercondtnArr, $userlikeCondtnArr=array(), $userjoinArr=array(), $singleRow=TRUE, $userorderByArr=array(), $usergroupByArr=array(), $userwhereInArray=array(), $usercustomWhereArray=array(), $userbackTicks=TRUE, $usercustomOrWhereArray=array(), $userorNotLikeArray=array());

				$usermail=$query['userdata'];

				if(!empty($usermail))
				{
					$usermailid=$usermail['s-r-email'];

					$subject="Online Website tenants Membership ";
		            $message="Dear User, 


					Online Website tenants Membership is Rejected
					due to ".$reason." .

					Best Regards,
					CHSVishva

					Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

		            $alt_message="This is the body in plain text for non-HTML mail clients";

		            $email=$usermailid;

		            $mailDataArr['to']=$email;
		            $mailDataArr['subject']=$subject;
		            $mailDataArr['alt_message']=$alt_message;
		            $mailDataArr['message']=$message;

		            $this->load->library('Email_sending');

		            $response=$this->email_sending->send($mailDataArr);

		            $responseStatus=$response['status'];
		            $responseMsg=$response['message'];

					echo $this->Mfunctional->SALERT("sucess");
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			redirect(base_url()."member_management/tenant_member/list", 'refresh');
		}
		else
		{
			redirect(base_url()."member_management/tenant_member/list", 'refresh');
		}      
	}
}

?>