<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
        $this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->accounting_tbl=$socTables['accounting_tbl'];
		$this->society_bank_master_tbl=$socTables['society_bank_master_tbl'];
		$this->maintenance_tbl=$socTables['maintenance_tbl'];
		$this->accounting_types_tbl=$socTables['accounting_types_tbl'];
		$this->accounting_groups_tbl=$socTables['accounting_groups_tbl'];
		$this->user_arrears_tbl=$socTables['user_arrears_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    function get_monthlist()
    {
		$this->data['usrId']=$usrId = $this->input->post("userid");
		$current_date = date('Y-m-d');

		$output = '';

		// $select_due = "SELECT `status`,`start_day` AS bill_date,`tansaction_no` AS tansaction_no1,`voucher_name` AS voucher_no1,`amount` AS total,`end_day`,`start_day` FROM `maintance` WHERE (`maintance`.`start_day`)<='$current_date' AND  `maintance`.`user_name`='$usrId' AND `status`='unpaid' ORDER BY `status` ASC,`start_day` ASC ";

		// $fire_due=mysqli_query($conn,$select_due);

		$trans_condtnArr['`maintenance`.`start_day` <=']=$current_date;
		$trans_condtnArr['`maintenance`.`user_name`']=$usrId;
		$trans_condtnArr['`maintenance`.`status`']="unpaid";
		$trans_orderByArr['`maintenance`.`status`']="ASC";
		$trans_orderByArr['`maintenance`.`start_day`']="ASC";

		$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.status, maintenance.start_day AS bill_date, maintenance.tansaction_no AS tansaction_no1, maintenance.voucher_name AS voucher_no1, maintenance.amount AS total, maintenance.end_day, maintenance.start_day", $trans_condtnArr, $likeCondtnArr=array(), $trans_joinArr=array(), $singleRow=FALSE, $trans_orderByArr, $trans_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$fire_due=$query['userdata'];

		$this->data['fire_due']=$fire_due;

		$output = "<div class='box-body monthlist' id='sel_month'>";
		if(!empty($fire_due)) 
		{
			$no=0;
			foreach ($fire_due as $row)
			{
				$output .= "<a class='btn btn-outline btn-danger mb-5 d-flex justify-content-between' href='javascript:void(0)'> <label id='month' class='".$usrId."' data-elem='".$no."' >".date("F",strtotime($row['bill_date']))."(".date("Y",strtotime($row['bill_date'])).") - ".$row['total']." </label></a>";

				$no++;

				// $output .= '<li id="month" class="'.$usrId.'">'.date("F",strtotime($row['bill_date'])).'('.date("Y",strtotime($row['bill_date'])).') - '.$row['total'].'</li>'; <span class="pull-right">'.$row['total'].'
			}
		}

		$output .= '</div>';

		// "<div class='box-body monthlist' id='sel_month'>
		// 	<a class='btn btn-outline btn-danger mb-5 d-flex justify-content-between' href='javascript:void(0)'> 
		// 		<span id='month' class='Gokul-A-3-10'>April(2020) - 0</span>
		// 	</a>
		// 	<a class='btn btn-outline btn-danger mb-5 d-flex justify-content-between' href='javascript:void(0)'> 
		// 		<span id='month' class='Gokul-A-3-10'>May(2020) - 0</span>
		// 	</a>
		// 	<a class='btn btn-outline btn-danger mb-5 d-flex justify-content-between' href='javascript:void(0)'> 
		// 		<span id='month' class='Gokul-A-3-10'>June(2020) - 8</span>
		// 	</a>
		// </div>";

		// $select_advance = "SELECT `Arrears_amount_pending` FROM `user_arrears` WHERE `user_id`='$usrId' ";

		// $fire_advance=mysqli_query($conn,$select_advance);

		$amt_condtnArr['`user_arrears`.`user_id`']=$usrId;

		$query=$this->Mcommon->getRecords($tableName=$this->user_arrears_tbl, $colNames="user_arrears.Arrears_amount_pending", $amt_condtnArr, $likeCondtnArr=array(), $amt_joinArr=array(), $singleRow=FALSE, $amt_orderByArr=array(), $amt_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$row_advance=$query['userdata'];

		$this->data['row_advance']=$row_advance;

		$advance_elem="";

		$advance_elem .= "<div class='form-group'>";
		$advance_elem .= "<label class='col-md-12'>Advance Amount</label>";
		$advance_elem .= "<div class='col-md-12'>";
		$advance_elem .= "<input type='text' class='form-control' name='advance' id='advance'";
		if(!empty($row_advance)) 
		{
			$advance_elem .= "value='".$row_advance['Arrears_amount_pending']."'";
		}

		$advance_elem .= " readonly>";
		$advance_elem .= "</div>";
		$advance_elem .= "</div>";

		$resultArr['monthArr']=$output;
		$resultArr['advanceElemArr']=$advance_elem;

		echo json_encode($resultArr);
		// $this->load->view('remote/accounting/get_monthlist', $this->data);
    }

    function acc_vno()
    {
    	$voucher_type=$this->input->post('query');

	    if($voucher_type=="Payment Voucher")
	    {
	        $voucher_initial="PV_";
	    }

	    if($voucher_type=="Reciept Voucher")
	    {
	        $voucher_initial="RV_";
	    }

	    if($voucher_type=="Contra Voucher")
	    {
	        $voucher_initial="CV_";
	    }

	    if($voucher_type=="Journal Voucher")
	    {
	        $voucher_initial="JV_";
	    }

	    $currmon= date("m");

		if($currmon>=4) 
		{
			$curryear= date("Y");
		}
		else
		{
			$curryear= date("Y")-1;
		}

        $currmon=date("m");
        
        if ($currmon>=4) 
        {
        	$curryear= date("Y");
        }
        else
        {
        	$curryear= date("Y")-1;
        }  
            
        $value2=date("m");
        $value3=date("Y");
        $value4=$value3-1;

        if(($value2=="1") || ($value2=="2") || ($value2=="3"))
        {
	        // $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr_customWhereArray[]="(( YEAR(`accounting`.`s-date`)='".$value4."' AND MONTH(`accounting`.`s-date`)>'4') OR (YEAR(`accounting`.`s-date`)='".$value3."' AND MONTH(`accounting`.`s-date`)<='".$value2."' ))";

	        $acc_tr_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`transaction_no`", $acc_tr_condtnArr=array(), $acc_tr_likeCondtnArr=array(), $acc_tr_joinArr=array(), $singleRow=TRUE, $acc_tr_orderByArr, $acc_tr_groupByArr=array(), $whereInArray=array(), $acc_tr_customWhereArray, $backTicks=TRUE);

			$acchead=$query['userdata'];

	        // $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='$voucher_type' AND (( YEAR(`s-date`)='$value4' AND MONTH(`s-date`)>'4') OR (YEAR(`s-date`)='$value3' AND MONTH(`s-date`)<='$value2' )) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr1_condtnArr["`accounting`.`vaucher_name`"]=$voucher_type;

	        $acc_tr1_customWhereArray[]="(( YEAR(`accounting`.`s-date`)='".$value4."' AND MONTH(`accounting`.`s-date`)>'4') OR (YEAR(`accounting`.`s-date`)='".$value3."' AND MONTH(`accounting`.`s-date`)<='".$value2."' ))";

	        $acc_tr1_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`vaucher_no`", $acc_tr1_condtnArr, $acc_tr1_likeCondtnArr=array(), $acc_tr1_joinArr=array(), $singleRow=TRUE, $acc_tr1_orderByArr, $acc_tr1_groupByArr=array(), $whereInArray=array(), $acc_tr1_customWhereArray, $backTicks=TRUE);

			$acchead_voc=$query['userdata'];
        }
        else
        {
	        // $fetch = mysqli_query($conn,"SELECT * FROM `accounting` WHERE ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";

	        $acc_tr_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`transaction_no`", $acc_tr_condtnArr=array(), $acc_tr_likeCondtnArr=array(), $acc_tr_joinArr=array(), $singleRow=TRUE, $acc_tr_orderByArr, $acc_tr_groupByArr=array(), $whereInArray=array(), $acc_tr_customWhereArray, $backTicks=TRUE);

			$acchead=$query['userdata'];

	        // $fetch_voc = mysqli_query($conn,"SELECT * FROM `accounting` WHERE  `vaucher_name`='$voucher_type' AND ((YEAR(`s-date`)='$value3' AND MONTH(`s-date`)>='4')) ORDER BY `a_id` DESC LIMIT 1");

	        $acc_tr1_condtnArr["`accounting`.`vaucher_name`"]=$voucher_type;

	        $acc_tr1_customWhereArray[]="((YEAR(`s-date`)='".$value3."' AND MONTH(`s-date`)>='4'))";

	        $acc_tr1_orderByArr['`accounting`.`a_id`']="DESC";

			$query=$this->Mcommon->getRecords($tableName=$this->accounting_tbl, $colNames="`accounting`.`vaucher_no`", $acc_tr1_condtnArr, $acc_tr1_likeCondtnArr=array(), $acc_tr1_joinArr=array(), $singleRow=TRUE, $acc_tr1_orderByArr, $acc_tr1_groupByArr=array(), $whereInArray=array(), $acc_tr1_customWhereArray, $backTicks=TRUE);

			$acchead_voc=$query['userdata'];
        }

        if($acchead['transaction_no']=="")
        {
	        $acchead['transaction_no'];
	        $iddd=1;
        }
        else
        {
	        $vall=str_split($acchead['transaction_no'],10);
	        $iddd=$vall[1]+1;
        }

        $vouc_data['trannumber']="trn_".$curryear."-0".$iddd;
        
        if($acchead_voc['vaucher_no']=="")
        {
	        $acchead_voc['vaucher_no'];
	        $iddd_voc=1;
        }
        else
        {
	        $vall_voc=str_split($acchead_voc['vaucher_no'],9);
	        $iddd_voc=$vall_voc[1]+1;
        }
        
        $voucher=$voucher_type;
        $vouc_data['voucherno']=$voucher_initial."".$curryear."-0".$iddd_voc;

		$trannumber =$vouc_data['trannumber'];
		$voucherno=$vouc_data['voucherno'];

        echo json_encode($vouc_data['voucherno']);
    }

}

?>