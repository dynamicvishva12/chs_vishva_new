<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poll_remote extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['test']='layouts/include/test';																					
		$this->data['footer']='layouts/include/footer';

		$this->data['society_userdata']=society_userdata();

		$this->society_db = $this->session->userdata('_society_database');
		$this->society_key=$this->session->userdata('_society_key');
		$this->user_name=$this->session->userdata('_user_name');

		$this->logModule='POLL';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->directory_tbl=$socTables['directory_tbl'];
		$this->poll_master_tbl=$socTables['poll_master_tbl'];
		$this->poll_result_tbl=$socTables['poll_result_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

	public function add_poll()
	{
		if($this->input->post('poll'))
		{
			$poll_data=$this->input->post('poll');
			$polluser ="A-A1-0-2";//$_SESSION['username'];

		    $p_res_condtnArr['poll_result.poll_status']=$polluser;
		    $p_res_condtnArr['poll_result.s-postid']=$poll_data;

			$query=$this->Mcommon->getRecords($tableName=$this->poll_result_tbl, $colNames="poll_result.s-postid", $p_res_condtnArr, $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $p_res_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

			$p_res_data=$query['userdata'];

			if(!empty($p_res_data))
			{
				DALERT('Your Poll Successfully Submited');
			}
			else
			{
				$pollInsertArr[]=array(
					'poll-username'=>$polluser,
					'poll-result'=>$poll_data,
					's-postid'=>$poll_data,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->poll_result_tbl, $pollInsertArr, $returnType="");
			}

			// redirect(base_url()."poll/home", 'refresh');
		}
	}
}
