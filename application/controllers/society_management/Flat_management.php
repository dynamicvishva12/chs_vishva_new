<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flat_management extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['test']='layouts/include/test';																					
		$this->data['footer']='layouts/include/footer';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='FLAT_MANAGEMENT';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->building_tbl=$socTables['building_tbl'];
		$this->wing_tbl=$socTables['wing_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function home()
    {
		$query=$this->Mcommon->getRecords($tableName=$this->building_tbl, $colNames="building.b-id, building.buildingname", $bldg_condtnArr=array(), $likeCondtnArr=array(), $bldg_joinArr=array(), $singleRow=FALSE, $bldg_orderByArr=array(), $bldg_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$bldg_data=$query['userdata'];

		$this->data['bldg_data']=$bldg_data;

		$query=$this->Mcommon->getRecords($tableName=$this->wing_tbl, $colNames="wing.wing-id, wing.wingname", $wing_condtnArr=array(), $likeCondtnArr=array(), $wing_joinArr=array(), $singleRow=FALSE, $wing_orderByArr=array(), $wing_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$wing_data=$query['userdata'];

		$this->data['wing_data']=$wing_data;


   		$this->data['view']="society_management/flat_management/home";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function delete_building()
    {
		$name = $this->input->post('delete-build');

		$delCondtnArr['building.b-id']=$name;

		$query=$this->Mcommon->delete($tableName=$this->building_tbl, $delCondtnArr, $likeCondtnArr=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==FALSE)
		{
			echo "<html><body><script>alert('Building not removed')</script></body></html>";
			redirect(base_url()."society_management/flat_management/home", 'refresh');
		}
		else
		{
			echo "<html><body><script>alert('Building removed sucessfully')</script></body></html>";
			redirect(base_url()."society_management/flat_management/home", 'refresh');
		}   
    }

    public function delete_wing()
    {
    	$name = $this->input->post('delete-wing');

		$delCondtnArr['wing.wing-id']=$name;

		$query=$this->Mcommon->delete($tableName=$this->wing_tbl, $delCondtnArr, $likeCondtnArr=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==FALSE)
		{
			echo "<html><body><script>alert('Wing not removed')</script></body></html>";
			redirect(base_url()."society_management/flat_management/home", 'refresh');
		}
		else
		{
			echo "<html><body><script>alert('Wing removed sucessfully')</script></body></html>";
			redirect(base_url()."society_management/flat_management/home", 'refresh');
		}   
    }
}

?>