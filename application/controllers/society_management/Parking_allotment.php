<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parking_allotment extends Society_core
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';		
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='PARKING_ALLOTMENT';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->parking_spot_name_tbl=$socTables['parking_spot_name_tbl'];
		$this->parking_spot_tbl=$socTables['parking_spot_tbl'];
		$this->vechical_tbl=$socTables['vechical_tbl'];
		$this->family_tbl=$socTables['family_tbl'];
		$this->staff_tbl=$socTables['staff_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Parking Allotment';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Parking Allotment";
    	$breadcrumbArr[0]['link']=base_url()."society_management/parking_allotment/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Parking Allotment List";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	// $sql1 = "SELECT * FROM `parking_spot_name` WHERE `status`='Not Alloted'";
    	$prkng_spot_condtnArr['parking_spot_name.status']='Not Alloted';

    	$query=$this->Mcommon->getRecords($tableName=$this->parking_spot_name_tbl, $colNames="parking_spot_name.ps-id, parking_spot_name.name", $prkng_spot_condtnArr, $likeCondtnArr=array(), $prkng_spot_joinArr=array(), $singleRow=FALSE, $prkng_spot_orderByArr=array(), $prkng_spot_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$prkng_spot_data=$query['userdata'];

		$this->data['prkng_spot_data']=$prkng_spot_data;

    	// $sql2 = "SELECT * FROM `vechical` LEFT JOIN `s-r-user` ON `s-r-user`.`s-r-username`=`vechical`.`user_name` WHERE `v-park` IS NULL";
    	$vechical_condtnArr['vechical.v-park']='';

    	$vechical_joinArr[]=array("tbl"=>$this->s_r_user_tbl, "condtn"=>"s-r-user.s-r-username=vechical.user_name", "type"=>"left");

    	$query=$this->Mcommon->getRecords($tableName=$this->vechical_tbl, $colNames="vechical.v_id, s-r-user.s-r-fname, s-r-user.s-r-lname, s-r-user.s-r-username", $vechical_condtnArr, $likeCondtnArr=array(), $vechical_joinArr, $singleRow=FALSE, $vechical_orderByArr=array(), $vechical_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$vechical_data=$query['userdata'];

		$this->data['vechical_data']=$vechical_data;

    	// $fetch=mysqli_query($conn,"SELECT * FROM ((`parking_spot` LEFT JOIN `parking_spot_name` on `parking_spot`.`parking_id`=`parking_spot_name`.`ps-id`)LEFT JOIN `vechical` on `parking_spot`.`user_name`=`vechical`.`v_id` )");

    	$pkgSpot_joinArr[]=array("tbl"=>$this->parking_spot_name_tbl, "condtn"=>"parking_spot.parking_id = parking_spot_name.ps-id", "type"=>"left");
    	$pkgSpot_joinArr[]=array("tbl"=>$this->vechical_tbl, "condtn"=>"parking_spot.user_name = vechical.v_id", "type"=>"left");

    	$query=$this->Mcommon->getRecords($tableName=$this->parking_spot_tbl, $colNames="parking_spot_name.name, parking_spot_name.building_name, parking_spot_name.wing_name, vechical.v_numplate, parking_spot.p-id, parking_spot.parking_id, parking_spot.user_name", $pkgSpot_condtnArr=array(), $likeCondtnArr=array(), $pkgSpot_joinArr, $singleRow=FALSE, $pkgSpot_orderByArr=array(), $pkgSpot_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$pkgSpot_data=$query['userdata'];

		$this->data['pkgSpot_data']=$pkgSpot_data;

		$this->data['view']="society_management/parking_allotment/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function allot_parking()
    {
    	$this->db->trans_start();

		$uname=$this->input->post('uname');
		$bname=$this->input->post('bname');

		// $update=mysqli_query($conn,"INSERT INTO `parking_spot`(`parking_id`, `user_name`) VALUES ('$uname','$bname')");

		$insertArr[]=array(
			'parking_id'=>$uname,
			'user_name'=>$bname,
			'createdBy'=>$this->user_name,
			'createdDatetime'=>$this->curr_datetime
		);

		$query=$this->Mcommon->insert($tableName=$this->parking_spot_tbl, $insertArr, $returnType="");

		$insertStatus=$query['status'];

		if($insertStatus==TRUE) 
		{
			// $change=mysqli_query($conn,"UPDATE `parking_spot_name` SET `status`='Alloted' WHERE `ps-id`='$uname'");

			$pkg_UpdateArr=array(
				'status'=>'Alloted',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$pkg_condtnArr['parking_spot_name.ps-id']=$uname;

			$query=$this->Mcommon->update($tableName=$this->parking_spot_name_tbl, $pkg_UpdateArr, $pkg_condtnArr, $likeCondtnArr=array());

			$pkg_updateStatus=$query['status'];

			if($pkg_updateStatus==TRUE) 
			{
				// $sql1 = "SELECT * FROM `parking_spot_name` WHERE `ps-id`='$uname'";

				$prkng_spot_condtnArr['parking_spot_name.ps-id']=$uname;

		    	$query=$this->Mcommon->getRecords($tableName=$this->parking_spot_name_tbl, $colNames="parking_spot_name.name", $prkng_spot_condtnArr, $likeCondtnArr=array(), $prkng_spot_joinArr=array(), $singleRow=TRUE, $prkng_spot_orderByArr=array(), $prkng_spot_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

				$prkng_spot_data=$query['userdata'];

				$prkng_spot_name=$prkng_spot_data['name'];

				// $park=mysqli_query($conn,"UPDATE `vechical` SET `v-park`='$uname',`spot_name`='".$res['name']."' WHERE `v_id`='$bname'");

				$vch_UpdateArr=array(
					'v-park'=>$uname,
					'spot_name'=>$prkng_spot_name,
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$vch_condtnArr['vechical.v_id']=$bname;

				$query=$this->Mcommon->update($tableName=$this->vechical_tbl, $vch_UpdateArr, $vch_condtnArr, $likeCondtnArr=array());

				$vch_updateStatus=$query['status'];

				if($vch_updateStatus==TRUE) 
				{
					$this->session->set_flashdata('success_msg', 'Parking has been alloted successfully');
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Parking has not alloted.');
			redirect(base_url()."society_management/parking_allotment/list", 'refresh');
		}
		else
		{
			redirect(base_url()."society_management/parking_allotment/list", 'refresh');
		}     
    }

    public function delete_parking_allotment()
    {
    	$this->db->trans_start();

		$id=$this->uri->segment('4');
		$parkid=$this->uri->segment('5');

		$delCondtnArr['parking_spot.p-id']=$id;

		// $delete=mysqli_query($conn,"DELETE FROM `parking_spot` WHERE `p-id`='$id'");
		$query=$this->Mcommon->delete($tableName=$this->parking_spot_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==TRUE)
		{
			// $change=mysqli_query($conn,"UPDATE `parking_spot_name` SET `status`='Not Alloted' WHERE `ps-id`='$parkid'");

			$pkg_UpdateArr=array(
				'status'=>'Not Alloted',
				'updatedBy'=>$this->user_name,
				'updatedDatetime'=>$this->curr_datetime
			);

			$pkg_condtnArr['parking_spot_name.ps-id']=$parkid;

			$query=$this->Mcommon->update($tableName=$this->parking_spot_name_tbl, $pkg_UpdateArr, $pkg_condtnArr, $likeCondtnArr=array());

			$pkg_updateStatus=$query['status'];

			if($pkg_updateStatus==TRUE) 
			{
				// $park=mysqli_query($conn,"UPDATE `vechical` SET `v-park`=NULL,`spot_name`=NULL WHERE `v-park`='$parkid'");

				$vch_UpdateArr=array(
					'v-park'=>'',
					'spot_name'=>'',
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$vch_condtnArr['vechical.v-park']=$parkid;

				$query=$this->Mcommon->update($tableName=$this->vechical_tbl, $vch_UpdateArr, $vch_condtnArr, $likeCondtnArr=array());

				$vch_updateStatus=$query['status'];

				if($vch_updateStatus==TRUE) 
				{
					$this->session->set_flashdata('success_msg', 'Parking allotment has been deleted successfully');
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Parking allotment has not deleted.');
			redirect(base_url()."society_management/parking_allotment/list", 'refresh');
		}
		else
		{
			// $this->session->set_flashdata('success_msg', 'Parking allotment has been deleted successfully');
			redirect(base_url()."society_management/parking_allotment/list", 'refresh');
		}
    }

    public function user_profile()
    {
    	$id=$this->uri->segment('4');

    	// $fetch=mysqli_query($conn,"SELECT * FROM `s-r-user` WHERE `s-r-username`='$id'");
    	$user_condtnArr['s-r-user.s-r-username']=$id;

    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-profile, s-r-user.s-r-fname, s-r-user.no-of-family-member, s-r-user.s-r-lname, s-r-user.s-r-email, s-r-user.s-r-mobile, s-r-user.s-r-appartment, s-r-user.s-r-wing, s-r-user.s-r-flat, s-r-user.s-r-bod, s-r-user.s-r-language, s-r-user.s-r-blood", $user_condtnArr, $likeCondtnArr=array(), $user_joinArr=array(), $singleRow=TRUE, $user_orderByArr=array(), $user_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userdetails=$query['userdata'];

		$this->data['userdetails']=$userdetails;

		// $runrows = mysqli_query($conn,"SELECT * FROM `family` WHERE `user_name` ='$id' AND `active`='active'")or die("error".mysqli_error($conn));

		$fmly_condtnArr['family.user_name']=$id;
		$fmly_condtnArr['family.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->family_tbl, $colNames="family.relation, family.name, family.relation, family.blood_type, family.dob, family.contact", $fmly_condtnArr, $likeCondtnArr=array(), $fmly_joinArr=array(), $singleRow=FALSE, $fmly_orderByArr=array(), $fmly_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userfamily=$query['userdata'];

		$this->data['userfamily']=$userfamily;

		// $runrows = mysqli_query($conn,"SELECT * FROM `staff` WHERE `user_name` ='$id' AND `active`='active'")or die("error".mysqli_error($conn));

		$staff_condtnArr['staff.user_name']=$id;
		$staff_condtnArr['staff.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->staff_tbl, $colNames="staff.name, staff.age, staff.relation, staff.address, staff.contact, staff.st_doc", $staff_condtnArr, $likeCondtnArr=array(), $staff_joinArr=array(), $singleRow=FALSE, $staff_orderByArr=array(), $staff_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$userstaff=$query['userdata'];

		$this->data['userstaff']=$userstaff;

		$vch_condtnArr['vechical.user_name']=$id;
		$vch_condtnArr['vechical.status']='active';

    	$query=$this->Mcommon->getRecords($tableName=$this->vechical_tbl, $colNames="vechical.v_id, vechical.v-type, vechical.make_model, vechical.v_numplate, vechical.v-park", $vch_condtnArr, $likeCondtnArr=array(), $vch_joinArr=array(), $singleRow=FALSE, $vch_orderByArr=array(), $vch_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$uservehical=$query['userdata'];

		$this->data['uservehical']=$uservehical;

		$this->data['view']="society_management/parking_allotment/user_profile";
		$this->load->view('layouts/layout/secondary_layout', $this->data);
    }
}

?>