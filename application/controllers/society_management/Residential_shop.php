<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Residential_shop extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';	
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='RESIDENTIAL_SHOPS';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->residential_shop_tbl=$socTables['residential_shop_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');

		$this->data['page_section']='Residential Shops';
    }

    public function list()
    {
    	$breadcrumbArr[0]['name']="Residential Shops";
    	$breadcrumbArr[0]['link']=base_url()."society_management/residential_shop/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Residential Shop List";
    	$breadcrumbArr[1]['link']="javascript:void(0);";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$query=$this->Mcommon->getRecords($tableName=$this->residential_shop_tbl, $colNames="residential_shop.id, residential_shop.shop_name, residential_shop.owner_name, residential_shop.wing, residential_shop.gala, residential_shop.contact", $res_shop_condtnArr=array(), $likeCondtnArr=array(), $res_shop_joinArr=array(), $singleRow=FALSE, $res_shop_orderByArr=array(), $res_shop_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$res_shop_data=$query['userdata'];

		$this->data['res_shop_data']=$res_shop_data;

		$this->data['view']="society_management/residential_shop/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function add_residential_shop()
    {
    	$this->form_validation->set_rules('Owner-name', 'Owner Name', 'trim|required');
    	$this->form_validation->set_rules('Owener-description', 'Owener description', 'trim|required');
    	$this->form_validation->set_rules('wing', 'Wing', 'trim|required');
    	$this->form_validation->set_rules('gala-No', 'Gala No', 'trim|required');
    	$this->form_validation->set_rules('Owner-mobile', 'Mobile mobile', 'trim|required|exact_length[10]|numeric');

    	if($this->form_validation->run() == FALSE)
		{
			$this->list();
		}
		else
		{
			$name=$this->Mfunctional->NORMAL($this->input->post('Owner-name'));
			$description=$this->Mfunctional->NORMAL($this->input->post('Owener-description'));
			$wing=$this->Mfunctional->NORMAL($this->input->post('wing'));
			$gala=$this->input->post('gala-No');
			$mobile=$this->Mfunctional->MOBILE($this->input->post('Owner-mobile'));

			if (!empty($name) && !empty($wing) && !empty($gala) && !empty($mobile)) 
			{
				// $insert=mysqli_query($conn,"INSERT INTO `residential_shop`(`shop_name`, `owner_name`, `contact`, `wing`, `gala`) VALUES ('$name','$description','$mobile','$wing','$gala')");

				$insertArr[]=array(
					'shop_name'=>$name,
					'owner_name'=>$description,
					'contact'=>$mobile,
					'wing'=>$wing,
					'gala'=>$gala,
					'createdBy'=>$this->user_name,
					'createdDatetime'=>$this->curr_datetime
				);

				$query=$this->Mcommon->insert($tableName=$this->residential_shop_tbl, $insertArr, $returnType="");

				$insertStatus=$query['status'];

				if($insertStatus==FALSE)
				{
					$this->session->set_flashdata('error_msg', 'Something went wrong! Residential shop has not added.');
					redirect(base_url()."society_management/residential_shop/list", 'refresh');
				}
				else
				{
					$this->session->set_flashdata('success_msg', 'Residential shop has been added successfully');
					redirect(base_url()."society_management/residential_shop/list", 'refresh');
				}
			}
			else
			{
				$this->session->set_flashdata('warning_msg', 'Check Out any field is missing');
				redirect(base_url()."society_management/residential_shop/list", 'refresh');
			}
		}
    }

    public function delete_residential_shop()
    {
		$id=$this->uri->segment('4');

		$delCondtnArr['residential_shop.id']=$id;

		// $Delete=mysqli_query($conn,"DELETE FROM `residential_shop` WHERE `id`='$id'")or die(mysqli_error($conn));
		$query=$this->Mcommon->delete($tableName=$this->residential_shop_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==FALSE)
		{
			$this->session->set_flashdata('error_msg', 'Something went wrong! Residential shop has not deleted.');
			redirect(base_url()."society_management/residential_shop/list", 'refresh');
		}
		else
		{
			$this->session->set_flashdata('success_msg', 'Residential shop has been deleted successfully');
			redirect(base_url()."society_management/residential_shop/list", 'refresh');
		}
    }
}

?>