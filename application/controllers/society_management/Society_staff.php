<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Society_staff extends Society_core
{
    public function __construct()
    {
         parent::__construct();

        $this->load->model('Mcommon', '', TRUE);
        $this->load->model('Msociety', '', TRUE);
		$this->load->model('Mfunctional', '', TRUE);
		$this->load->model('Mlogger', '', TRUE);

		$this->load->library('Society_lib');

        $this->data['base_url']=$this->base_url=$this->config->item('base_url');
		$this->data['assets_url']=$this->assets_url=$this->config->item('assets_url');

		$this->data['css']='layouts/include/css';
		$this->data['side_nav']='layouts/include/side_nav';																					
		$this->data['navbar_menu']='layouts/include/navbar_menu';
		$this->data['test']='layouts/include/test';																					
		$this->data['footer']='layouts/include/footer';
		$this->data['js']='layouts/include/js';

		$this->data['society_userdata']=society_userdata();

		$this->data['society_db']=$this->society_db=$this->session->userdata('_society_database');
		$this->data['society_key']=$this->society_key=$this->session->userdata('_society_key');
		$this->data['user_name']=$this->user_name=$this->session->userdata('_user_name');
		$this->data['sess_user_profile']=$this->session->userdata('_user_profile');

		$this->logModule='SOCIETY_STAFF';

		$socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->soc_staff_tbl=$socTables['soc_staff_tbl'];

		$this->curr_datetime=date('Y-m-d H:i:s');
    }

    public function list()
    {
    	$this->data['page_section']='Society Staff';

    	$breadcrumbArr[0]['name']="Society Management";
    	$breadcrumbArr[0]['link']=base_url()."society_management/society_staff/list";
    	$breadcrumbArr[0]['active']=FALSE;

    	$breadcrumbArr[1]['name']="Society Staff";
    	$breadcrumbArr[1]['link']="javascript:void(0)";
    	$breadcrumbArr[1]['active']=TRUE;

    	$this->data['breadcrumbArr']=$breadcrumbArr;

    	$soc_stf_orderByArr['soc_staff.id']='DESC';

    	$query=$this->Mcommon->getRecords($tableName=$this->soc_staff_tbl, $colNames="soc_staff.id, soc_staff.vendor_type, soc_staff.name, soc_staff.address, soc_staff.mobile_no, soc_staff.extra, soc_staff.extra1", $soc_stf_condtnArr=array(), $likeCondtnArr=array(), $soc_stf_joinArr=array(), $singleRow=FALSE, $soc_stf_orderByArr, $soc_stf_groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$soc_stf_data=$query['userdata'];

		$this->data['soc_stf_data']=$soc_stf_data;

		$this->data['view']="society_management/society_staff/list";
		$this->load->view('layouts/layout/main_layout', $this->data);
    }

    public function add_society_staff()
    {
    	$this->form_validation->set_rules('name', 'Name', 'trim|required');
    	$this->form_validation->set_rules('select-type', 'Type', 'trim|required');
    	$this->form_validation->set_rules('b-address', 'Address', 'trim|required');
    	$this->form_validation->set_rules('b-mobile', 'Mobile Number', 'trim|required|exact_length[10]|numeric');
    	$this->form_validation->set_rules('from', 'From Time', 'trim|required');
    	$this->form_validation->set_rules('to', 'To Time', 'trim|required');

    	if($this->form_validation->run() == FALSE)
		{
			$this->list();
		}
		else
		{
			$type=$this->input->post('select-type');
			$name=$this->Mfunctional->NORMAL($this->input->post('name'));
			$b_address=$this->Mfunctional->NORMAL($this->input->post('b-address'));
			$mobile=$this->input->post('b-mobile');
			$from=$this->input->post('from');
			$to=$this->input->post('to');

			// $insert=mysqli_query($conn,"INSERT INTO `soc_staff` (`vendor_type`, `name`, `address`,`mobile_no`,`extra`, `extra1`) VALUES ('$type', '$name', '$b_address', '$mobile', '$from','$to')");

			$insertArr[]=array(
				'vendor_type'=>$type,
				'name'=>$name,
				'address'=>$b_address,
				'mobile_no'=>$mobile,
				'extra'=>$from,
				'extra1'=>$to,
				'createdBy'=>$this->user_name,
				'createdDatetime'=>$this->curr_datetime
			);

			$query=$this->Mcommon->insert($tableName=$this->soc_staff_tbl, $insertArr, $returnType="");

			$insertStatus=$query['status'];

			if($insertStatus==FALSE)
			{
				// echo "<html><body><script>alert('Society staff not added')</script></body></html>";
				$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff not added.');
				redirect(base_url()."society_management/society_staff/list", 'refresh');
			}
			else
			{
				// $this->Mfunctional->SALERT("sucess");
				$this->session->set_flashdata('success_msg', 'Society staff has been added successfully.');
				redirect(base_url()."society_management/society_staff/list", 'refresh');
			}
		}
    }

    public function delete_society_staff()
    {
		$id=$this->uri->segment('4');

		$delCondtnArr['soc_staff.id']=$id;

		// $Delete=mysqli_query($conn,"DELETE FROM `soc_staff` WHERE `id`='$id'");
		$query=$this->Mcommon->delete($tableName=$this->soc_staff_tbl, $delCondtnArr, $likeCondtnArr=array(), $whereInArray=array());

		$nwDelStatus=$query['status'];

		if($nwDelStatus==FALSE)
		{
			// echo "<html><body><script>alert('Society staff not deleted')</script></body></html>";
			$this->session->set_flashdata('error_msg', 'Something went wrong! Society staff not deleted.');
			redirect(base_url()."society_management/society_staff/list", 'refresh');
		}
		else
		{
			// $this->Mfunctional->SALERT("sucess");
			$this->session->set_flashdata('success_msg', 'Society staff has been deleted successfully.');
			redirect(base_url()."society_management/society_staff/list", 'refresh');
		}
    }
}

?>