<?php

class Society_core extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $_society_key = $this->session->userdata('_society_key');
        $_society_database = $this->session->userdata('_society_database');
        $_society_status = $this->session->userdata('_society_status');
        $_user_name = $this->session->userdata('_user_name');
        $_user_type = $this->session->userdata('_user_type');
        $_designation_id = $this->session->userdata('_designation_id');

        if($_society_key=='' || $_society_status=='' || $_user_name=='' || $_user_type=='' || $_designation_id=='' || $_society_database=='')
            redirect(base_url().'auth/login', 'refresh');

        if($_SERVER['SERVER_NAME']=='localhost')
        {
            $admin_db="aadharch_society_parameter";
        }
        else
        {
            $admin_db="aadharch_society_parameter";
        }

        $this->society_list=$admin_db.".society_list";
        $this->society_table=$admin_db.".society_table";

        if($_society_database!='')
            $soc_db=$_society_database;
        else
            $soc_db="";

        if($soc_db!='')
        {
            $this->s_r_user_tbl=$soc_db.".s-r-user";
            $this->directory_tbl=$soc_db.".directory";
            $this->society_master_tbl=$soc_db.".society_master";

            $this->data['societyLogData']=society_log_helper();

            $this->data['_user_name']=$_user_name;
        }
    }
}

class Gatekeeper_core extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $_society_key = $this->session->userdata('_society_key');
        $_society_database = $this->session->userdata('_society_database');
        $_society_status = $this->session->userdata('_society_status');
        $_user_name = $this->session->userdata('_user_name');
        $_user_type = $this->session->userdata('_user_type');
        $_designation_id = $this->session->userdata('_designation_id');
        $_gatekeeper_key = $this->session->userdata('_gatekeeper_key');
        $_gatekeeper_database = $this->session->userdata('_gatekeeper_database');

        if($_gatekeeper_key=="")
        {
            redirect(base_url().'auth/connect_gatekeeper', 'refresh');
        }

        if($_society_key=='' || $_society_status=='' || $_user_name=='' || $_user_type=='' || $_designation_id=='' || $_society_database=='' || $_gatekeeper_key=='' || $_gatekeeper_database=='')
            redirect(base_url().'auth/login', 'refresh');

        if($_SERVER['SERVER_NAME']=='localhost')
        {
            $admin_db="aadharch_society_parameter";
        }
        else
        {
            $admin_db="aadharch_society_parameter";
        }

        $this->society_list=$admin_db.".society_list";
        $this->society_table=$admin_db.".society_table";

        if($_society_database!='')
            $soc_db=$_society_database;
        else
            $soc_db="";

        if($soc_db!='')
        {
            $this->s_r_user_tbl=$soc_db.".s-r-user";
            $this->directory_tbl=$soc_db.".directory";
            $this->society_master_tbl=$soc_db.".society_master";

            $this->data['societyLogData']=society_log_helper();

            $this->data['_user_name']=$_user_name;
        }
    }
}

?>