<?php

if (! defined('BASEPATH')) exit('No direct script access allowed');
 
if (! function_exists('society_userdata')) {

    function society_userdata()
    {
        // get main CodeIgniter object
        $CI=& get_instance();
       
        // Write your logic as per requirement
        $CI->load->model('Mcommon');
		$CI->soc_user_name = $CI->session->userdata('_user_name');
		$CI->society_dtbs = $CI->session->userdata('_society_database');

		$soc_condtnArr['s-r-user.s-r-username']=$CI->soc_user_name;

		$query=$CI->Mcommon->getRecords($tableName=$CI->society_dtbs.'.s-r-user', $colNames="s-r-user.*", $soc_condtnArr, $likeCondtnArr=array(), $soc_joinArr=array(), $singleRow=TRUE, $soc_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$socArr=$query['userdata'];

		return $socArr;
    }

    function society_log_helper()
    {
        // get main CodeIgniter object
        $CI=& get_instance();
       
        // Write your logic as per requirement
        $CI->load->model('Mcommon');
        $CI->soc_user_name = $CI->session->userdata('_user_name');
        $CI->society_dtbs = $CI->session->userdata('_society_database');

        $soc_orderByArr['society_log.sr_id']="DESC";

        $query=$CI->Mcommon->getRecords($tableName=$CI->society_dtbs.'.society_log', $colNames="society_log.logModule, society_log.logDescription, society_log.createdDatetime", $soc_condtnArr=array(), $likeCondtnArr=array(), $soc_joinArr=array(), $singleRow=FALSE, $soc_orderByArr, $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

        $socLogArr=$query['userdata'];

        return $socLogArr;
    }
}

?>