<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_operation
{
	public function forget_password($email, $society_key)
	{
		// get main CodeIgniter object
        $this->CI=& get_instance();

        $responseVal=FALSE;
        $currDateTime=date('Y-m-d H:i:s');
        $society_dtbs = $this->CI->session->userdata('_society_database');

        $random = str_shuffle('123456789');
        $oneTimePass=substr($random,0,6);

        $userUpdateArr=array(
            's-r-onetime'=>$oneTimePass,
            'updatedBy'=>'Admin',
            'updatedDatetime'=>$currDateTime
        );

        $updt_condtnArr['s-r-user.s-r-email']=$email;

        $query=$this->CI->Mcommon->update($tableName=$society_dtbs.".s-r-user", $userUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

        $updateRes=$query['status'];

        if($updateRes==TRUE)
        {
            $subject="One Time Password ";
            $message="Dear User, <br>
            <br><br>
            
            We have processed your request for password retrieval. Your account details are as mentioned below
            <br><br>
            
            User ID: ".$email."<br>
            One Time Password: ".$oneTimePass." <br>
            Change your password at your own and keep this safely. <br><br>
            
            
            Best Regards,<br>
            CHSVishva
            <br><br>
            Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

            $alt_message="This is the body in plain text for non-HTML mail clients";

            $mailDataArr['to']=$email;
            $mailDataArr['subject']=$subject;
            $mailDataArr['alt_message']=$alt_message;
            $mailDataArr['message']=$message;

            $this->CI->load->library('Email_sending');

            $response=$this->CI->email_sending->send($mailDataArr);

            $responseStatus=$response['status'];
            $responseMsg=$response['message'];

            if($responseStatus==TRUE)
                $responseVal=TRUE;
        }

        return $responseVal;
	}

    public function resend_otp($email, $society_key)
    {
        $this->CI=& get_instance();

        $responseVal=FALSE;
        $currDateTime=date('Y-m-d H:i:s');
        $society_dtbs = $this->CI->session->userdata('_society_database');

        $random = str_shuffle('123456789');
        $oneTimePass=substr($random,0,6);

        $userUpdateArr=array(
            's-r-onetime'=>$oneTimePass,
            'updatedBy'=>'Admin',
            'updatedDatetime'=>$currDateTime
        );

        $updt_condtnArr['s-r-user.s-r-email']=$email;

        $query=$this->CI->Mcommon->update($tableName=$society_dtbs.".s-r-user", $userUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

        $updateRes=$query['status'];

        if($updateRes==TRUE)
        {
            $subject="One Time Password ";
            $message="Dear User, <br>
            <br><br>
            
            We have processed your request for password retrieval. Your account details are as mentioned below
            <br><br>
            
            User ID: ".$email."<br>
            One Time Password: ".$oneTimePass." <br>
            Change your password at your own and keep this safely. <br><br>
            
            
            Best Regards,<br>
            CHSVishva
            <br><br>
            Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

            $alt_message="This is the body in plain text for non-HTML mail clients";

            $mailDataArr['to']=$email;
            $mailDataArr['subject']=$subject;
            $mailDataArr['alt_message']=$alt_message;
            $mailDataArr['message']=$message;

            $this->CI->load->library('Email_sending');

            $response=$this->CI->email_sending->send($mailDataArr);

            $responseStatus=$response['status'];
            $responseMsg=$response['message'];

            if($responseStatus==TRUE)
                $responseVal=TRUE;
        }

        return $responseVal;
    }

    public function change_password($email, $password, $society_key)
    {
        $this->CI=& get_instance();

        $responseVal=FALSE;
        $currDateTime=date('Y-m-d H:i:s');
        $society_dtbs = $this->CI->session->userdata('_society_database');

        $userUpdateArr=array(
            's-r-password'=>$password,
            'updatedBy'=>'Admin',
            'updatedDatetime'=>$currDateTime
        );

        $updt_condtnArr['s-r-user.s-r-email']=$email;

        $query=$this->CI->Mcommon->update($tableName=$society_dtbs.".s-r-user", $userUpdateArr, $updt_condtnArr, $likeCondtnArr=array());

        $updateRes=$query['status'];

        if($updateRes==TRUE)
        {
            $subject="One Time Password ";
            $message="Dear User, <br>
            <br><br>

            We have processed your request for password change. your new password is set for following Id
            <br><br>

            ".$email."<br>

            do not share password with anybody and keep this safely. 
            <br><br>


            Best Regards,<br>
            CHSVishva
            <br><br>
            Note : Please do not reply back to this mail. This is sent from an unattended mail box. Please mark all your queries / responses to info@chsvishva.com ";

            $alt_message="This is the body in plain text for non-HTML mail clients";

            $mailDataArr['to']=$email;
            $mailDataArr['subject']=$subject;
            $mailDataArr['alt_message']=$alt_message;
            $mailDataArr['message']=$message;

            $this->CI->load->library('Email_sending');

            $response=$this->CI->email_sending->send($mailDataArr);

            $responseStatus=$response['status'];
            $responseMsg=$response['message'];

            if($responseStatus==TRUE)
                $responseVal=TRUE;
        }

        return $responseVal;
    }
}


?>