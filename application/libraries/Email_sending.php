<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_sending
{
	public function send($mailDataArr)
	{
		// get main CodeIgniter object
        $this->CI=& get_instance();

        $to=$mailDataArr['to'];
        $subject=$mailDataArr['subject'];
        $alt_message=$mailDataArr['alt_message'];
        $message=$mailDataArr['message'];

        $this->CI->load->model('Mcommon');
		// $this->CI->soc_user_name = $this->CI->session->userdata('_user_name');
		$society_dtbs = $this->CI->session->userdata('_society_database');

		$query=$this->CI->Mcommon->getRecords($tableName=$society_dtbs.'.email_account', $colNames="email_account.host, email_account.email_id, email_account.password", $condtnArr=array(), $likeCondtnArr=array(), $soc_joinArr=array(), $singleRow=TRUE, $soc_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$emailData=$query['userdata'];

		$host=$emailData['host'];
        $from=$emailData['email_id'];
        $password=$emailData['password'];

		// $config = array(
		// 	'protocol' => 'smtp',
		// 	'smtp_host' => 'mail.gmx.com',
		// 	'smtp_port' => 587, //465,
		// 	'smtp_user' => 'myself@gmx.com',
		// 	'smtp_pass' => 'PASSWORD',
		// 	'smtp_crypto' => 'tls',
		// 	'smtp_timeout' => '20',
		// 	'mailtype'  => 'html', 
		// 	'charset'   => 'iso-8859-1'
		// );

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => $host,
			'smtp_port' => 587, //465,
			'smtp_user' => $from,
			'smtp_pass' => $password,
			'smtp_crypto' => 'tls',
			'mailtype'  => 'html', 
			'charset'   => 'utf-8'
		);

		// $config['newline'] = "\r\n";
		// $config['crlf'] = "\r\n";

		// $this->email->initialize($config);

		$this->CI->load->library('email', $config);

		$this->CI->email->clear();

		$this->CI->email->from($from, 'CHSVishva');
		$this->CI->email->to($to);
		$this->CI->email->subject($subject);
		$this->CI->email->set_alt_message($alt_message);
		$this->CI->email->message($message);

		if(!$this->CI->email->send()) 
		{
            $response_err="";
            $response_err.="Message could not be sent.";
            $response_err.="Mailer Error: ";
            $response_err.=$this->CI->email->print_debugger();

			$response['status']=FALSE;
			$response['message']=$response_err;
		}
		else
		{
			$response['status']=TRUE;
			$response['message']="";
		}

		return $response;
	}
}


?>