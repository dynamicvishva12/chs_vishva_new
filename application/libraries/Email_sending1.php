<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// include FCPATH.'vendor/autoload.php';

// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\SMTP;
// use PHPMailer\PHPMailer\Exception;

class Email_sending
{
	// public function send1($mailDataArr)
	// {
	// 	$this->CI=& get_instance();

	// 	$this->CI->load->model('Mcommon');
	// 	// $this->CI->soc_user_name = $this->CI->session->userdata('_user_name');
	// 	$society_dtbs = $this->CI->session->userdata('_society_database');

	// 	$query=$this->CI->Mcommon->getRecords($tableName=$society_dtbs.'.email_account', $colNames="email_account.host, email_account.email_id, email_account.password", $condtnArr=array(), $likeCondtnArr=array(), $soc_joinArr=array(), $singleRow=TRUE, $soc_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

	// 	$emailData=$query['userdata'];

	// 	$host=$emailData['host'];
 //        $from=$emailData['email_id'];
 //        $password=$emailData['password'];

	// 	$to=$mailDataArr['to'];
 //        $subject=$mailDataArr['subject'];
 //        $alt_message=$mailDataArr['alt_message'];
 //        $message=$mailDataArr['message'];

	// 	// Instantiation and passing `true` enables exceptions
	// 	// $mail = new PHPMailer(true);
	// 	$mail = new PHPMailer();

	// 	// try {
	// 	    //Server settings
	// 	    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
	// 	    $mail->isSMTP();                                            // Send using SMTP
	// 	    $mail->Host       = $host;                    // Set the SMTP server to send through
	// 	    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	// 	    $mail->Username   = $from;                     // SMTP username
	// 	    $mail->Password   = $password;                               // SMTP password
	// 	    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	// 	    // $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

	// 	    //Recipients
	// 	    $mail->setFrom($from, 'CHSVishva');
	// 	    $mail->addAddress('dynamicvishvateam@gmail.com');     // Add a recipient
	// 	    // $mail->addAddress('pratikshinde354@gmail.com');     // Add a recipient
	// 	    // $mail->addAddress('ellen@example.com');               // Name is optional
	// 	    // $mail->addReplyTo('info@example.com', 'Information');
	// 	    // $mail->addCC('cc@example.com');
	// 	    // $mail->addBCC('bcc@example.com');

	// 	    // Attachments
	// 	    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	// 	    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	// 	    // Content
	// 	    $mail->isHTML(true);                                  // Set email format to HTML
	// 	    $mail->WordWrap = 50;                       // set word wrap to 50 characters
	// 	    $mail->Subject = $subject;
	// 	    $mail->Body    = $message;
	// 	    $mail->AltBody = $alt_message;

	// 	    if(!$mail->send())
	// 	    {
	// 	    	$response['status']=FALSE;
	// 			$response['message']="Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	// 	    }
	// 	    else
	// 	    {
	// 			$response['status']=TRUE;
	// 			$response['message']='Message has been sent';
	// 	    }
	// 	    // echo 'Message has been sent';
		    
	// 	// } catch (Exception $e) {
	// 	//     // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

	// 	//     $response['status']=FALSE;
	// 	// 	$response['message']="Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	// 	// }

	// 	return $response;
	// }

	public function send($mailDataArr)
	{
		// get main CodeIgniter object
        $this->CI=& get_instance();

        $to=$mailDataArr['to'];
        $subject=$mailDataArr['subject'];
        $alt_message=$mailDataArr['alt_message'];
        $message=$mailDataArr['message'];

        $this->CI->load->model('Mcommon');
		// $this->CI->soc_user_name = $this->CI->session->userdata('_user_name');
		$society_dtbs = $this->CI->session->userdata('_society_database');

		$query=$this->CI->Mcommon->getRecords($tableName=$society_dtbs.'.email_account', $colNames="email_account.host, email_account.email_id, email_account.password", $condtnArr=array(), $likeCondtnArr=array(), $soc_joinArr=array(), $singleRow=TRUE, $soc_orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=TRUE);

		$emailData=$query['userdata'];

		$host=$emailData['host'];
        $from=$emailData['email_id'];
        $password=$emailData['password'];


		// $config = Array(
		// 	'protocol' => 'smtp',
		// 	'smtp_host' => 'mail.gmx.com',
		// 	'smtp_port' => 587, //465,
		// 	'smtp_user' => 'myself@gmx.com',
		// 	'smtp_pass' => 'PASSWORD',
		// 	'smtp_crypto' => 'tls',
		// 	'smtp_timeout' => '20',
		// 	'mailtype'  => 'html', 
		// 	'charset'   => 'iso-8859-1'
		// );

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.chsvishva.com',
			'smtp_port' => 587, //465,
			'smtp_user' => 'noreply@chsvishva.com',
			'smtp_pass' => 'shree@9969',
			'smtp_crypto' => 'tls',
			'mailtype'  => 'html', 
			'charset'   => 'utf-8'
		);

		// $config['newline'] = "\r\n";
		// $config['crlf'] = "\r\n";

		// $this->email->initialize($config);

		$this->CI->load->library('email', $config);

		$this->CI->email->clear();

		$this->CI->email->from($from, 'CHSVishva');
		$this->CI->email->to($to);
		$this->CI->email->subject($subject);
		$this->CI->email->set_alt_message($alt_message);
		$this->CI->email->message($message);

		//$this->email->send();
		if(!$this->CI->email->send()) 
		{
            $response_err="";
            $response_err.="Message could not be sent.";
            $response_err.="Mailer Error: ";
            $response_err.=$this->CI->email->print_debugger();

			$response['status']=FALSE;
			$response['message']=$response_err;
		}
		else
		{
			$response['status']=TRUE;
			$response['message']="";
		}

		return $response;
	}
}


?>