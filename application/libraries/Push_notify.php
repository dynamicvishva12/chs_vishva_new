<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_notify
{
	public function notification($notifyArr)
	{
		$this->CI=& get_instance();

		$this->CI->load->model('Msociety');
		// $this->CI->soc_user_name = $this->CI->session->userdata('_user_name');

		$output = "";
		$userType=$notifyArr['userType'];
        $userName=$notifyArr['userName'];
        $ntfyTitle=$notifyArr['title'];
        $ntfyBody=$notifyArr['body'];
        $ntfyPushType=$notifyArr['pushType'];

        if(isset($notifyArr['month']))
        	$month=$notifyArr['month'];
        else
        	$month="";

        if(isset($notifyArr['priority']))
        	$priority=$notifyArr['priority'];
        else
        	$priority=true;

        if(isset($notifyArr['complaintId']))
        	$complaintId=$notifyArr['complaintId'];
        else
        	$complaintId="";

		$query=$this->CI->Msociety->get_api_key($app_type=$userType);

		$app_key=$query['api_key'];

		if($userType=='user app')
		{
			if(!defined('API_ACCESS_KEY'))
				define( 'API_ACCESS_KEY', $app_key);
		}
		elseif($userType=='admin app')
		{
			if(!defined('API_ACCESS_KEY_ADMIN'))
				define( 'API_ACCESS_KEY_ADMIN', $app_key);
		}

		$fetch_user=$this->CI->Msociety->get_fms_tockens($userName);

		if(!empty($fetch_user))
		{
			foreach($fetch_user AS $user_dt)
			{         
				if($month!="")
					$typedata['month']=$month;

				if($complaintId!="")
					$typedata['$complaintId']=$complaintId;
				
				$typedata=array 
				(
					'body' 	=> $ntfyBody,
					'title'	=> $ntfyTitle,
					'type' => $ntfyPushType,
					'priority' => $priority
				);
				$value=$user_dt['tocken'];
				$fields = array
				(
					'to' => $value,
					'data' => $typedata
				);

				if($userType=='user app')
				{
					$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);
				}
				elseif($userType=='admin app')
				{
					$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY_ADMIN,
						'Content-Type: application/json'
					);
				}
				#Send Repo/nse To FireBase Server	
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$output = curl_exec($ch);
				//print_r($output);
				curl_close( $ch );
			}
		}
		return $output;
	}
}


?>