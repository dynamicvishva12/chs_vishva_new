<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Society_lib
{
	public function socTables()
	{
		// get main CodeIgniter object
        $CI=& get_instance();

        $sess_society_db = $CI->session->userdata('_society_database');
        $sess_gatekeeper_db = $CI->session->userdata('_gatekeeper_database');
       
        if($_SERVER['SERVER_NAME']=='localhost')
        {
            $admin_db="`aadharch_society_parameter`";
        }
        else
        {
            $admin_db="`aadharch_society_parameter`";
        }

        $socTable=array();

        $socTable['society_list']=$admin_db.".`society_list`";
        $socTable['society_table']=$admin_db.".`society_table`";

        $socTable['s_r_user_tbl']=$sess_society_db.".`s-r-user`";
        $socTable['directory_tbl']=$sess_society_db.".directory";
        $socTable['fsm_server_key_tbl']=$sess_society_db.".fsm_server_key";
        $socTable['fms_tocken_tbl']=$sess_society_db.".fms_tocken";
        $socTable['fms_tocken_admin_tbl']=$sess_society_db.".fms_tocken_admin";
        $socTable['class_member_register_tbl']=$sess_society_db.".class_member_register";
        $socTable['email_account_tbl']=$sess_society_db.".email_account";
        $socTable['poll_master_tbl']=$sess_society_db.".poll_master";
        $socTable['poll_result_tbl']=$sess_society_db.".poll_result";
        $socTable['soc_vendor_tbl']=$sess_society_db.".soc_vendor";
        $socTable['society_vendor_list_tbl']=$sess_society_db.".society_vendor_list";
        $socTable['vendor_tbl']=$sess_society_db.".vendor";
        $socTable['booking_tbl']=$sess_society_db.".booking";
        $socTable['booking_slot_tbl']=$sess_society_db.".booking_slot";
        $socTable['s_r_event_tbl']=$sess_society_db.".s-r-event";
        $socTable['album_tbl']=$sess_society_db.".album";
        $socTable['files_tbl']=$sess_society_db.".files";
        $socTable['useradmin2_tbl']=$sess_society_db.".useradmin2";
        $socTable['notices_tbl']=$sess_society_db.".notices";
        $socTable['schedular_tbl']=$sess_society_db.".schedular";
        $socTable['family_tbl']=$sess_society_db.".family";
        $socTable['news_letter_tbl']=$sess_society_db.".news_letter";
        $socTable['building_tbl']=$sess_society_db.".building";
        $socTable['wing_tbl']=$sess_society_db.".wing";
        $socTable['soc_staff_tbl']=$sess_society_db.".soc_staff";
        $socTable['residential_shop_tbl']=$sess_society_db.".residential_shop";
        $socTable['parking_spot_name_tbl']=$sess_society_db.".parking_spot_name";
        $socTable['parking_spot_tbl']=$sess_society_db.".parking_spot";
        $socTable['vechical_tbl']=$sess_society_db.".vechical";
        $socTable['staff_tbl']=$sess_society_db.".staff";
        $socTable['tents_tbl']=$sess_society_db.".tents";
        $socTable['j_form_tbl']=$sess_society_db.".j_form";
        $socTable['maintenance_head_tbl']=$sess_society_db.".maintenance_head";
        $socTable['maintenance_head_charges_tbl']=$sess_society_db.".maintenance_head_charges";
        $socTable['member_maintenance_tbl']=$sess_society_db.".member_maintenance_tbl";
        $socTable['sub_standard_tbl']=$sess_society_db.".sub-standard";
        $socTable['penalty_tbl']=$sess_society_db.".penalty";
        $socTable['maintenance_tbl']=$sess_society_db.".maintenance";
        $socTable['society_master_tbl']=$sess_society_db.".society_master";
        $socTable['members_advance_log_tbl']=$sess_society_db.".members_advance_log";
        $socTable['accounting_tbl']=$sess_society_db.".accounting";
        $socTable['accounting_types_tbl']=$sess_society_db.".accounting_types";
        $socTable['s_r_complain_tbl']=$sess_society_db.".`s-r-complain`";
        $socTable['complain_chat_tbl']=$sess_society_db.".complain_chat";
        $socTable['meetid_tbl']=$sess_society_db.".meetid";
        $socTable['meet_poll_tbl']=$sess_society_db.".meet-poll";
        $socTable['society_bank_master_tbl']=$sess_society_db.".society_bank_master";
        $socTable['accounting_groups_tbl']=$sess_society_db.".accounting_groups";
        $socTable['user_arrears_tbl']=$sess_society_db.".user_arrears";
        $socTable['accounting_opening_balance_tbl']=$sess_society_db.".accounting_opening_balance";
        $socTable['pnl_acc_tbl']=$sess_society_db.".pnl_acc_tbl";
        $socTable['staff_tbl']=$sess_society_db.".`staff`";
        $socTable['family_tbl']=$sess_society_db.".`family`";
        $socTable['visitors_tbl']=$sess_society_db.".`visitors`";
        $socTable['share_register_tbl']=$sess_society_db.".`share_register_tbl`";
        $socTable['transfer_share_tbl']=$sess_society_db.".`transfer_share_tbl`";
        $socTable['nominee_register_tbl']=$sess_society_db.".`nominee_register_tbl`";
        $socTable['audit_tbl']=$sess_society_db.".`audit`";
        $socTable['document_tbl']=$sess_society_db.".`document`";
        $socTable['j_form_tbl']=$sess_society_db.".`j_form`";
        $socTable['lien_tbl']=$sess_society_db.".`lien`";
        $socTable['legal_tbl']=$sess_society_db.".`legal`";

        if($sess_gatekeeper_db!="")
        {
            $socTable['visitor_tbl']=$sess_gatekeeper_db.".`visitor_tbl`";
            $socTable['late_mark_visitor_tbl']=$sess_gatekeeper_db.".`late_mark_visitor_tbl`";
            $socTable['visitor_type_tbl']=$sess_gatekeeper_db.".`visitor_type`";
            $socTable['visitor_entry_tbl']=$sess_gatekeeper_db.".`visitor_entry_tbl`";
            $socTable['expectedvisitors_tbl']=$sess_gatekeeper_db.".`expectedvisitors`";
            $socTable['society_staff_tbl']=$sess_gatekeeper_db.".`soc_staff`";
            $socTable['staff_entry_tbl']=$sess_gatekeeper_db.".`staff_entry_tbl`";
            $socTable['admin_login_tbl']=$sess_gatekeeper_db.".`admin_login`";
            $socTable['fsm_server_key_tbl']=$sess_gatekeeper_db.".`fsm_server_key`";
            $socTable['gatekeeper_attendance_tbl']=$sess_gatekeeper_db.".`gatekeeper_attendance_tbl`";
            $socTable['login_tbl']=$sess_gatekeeper_db.".`login_tbl`";
            $socTable['member_directory_tbl']=$sess_gatekeeper_db.".`member_directory`";
            $socTable['staff_type_tbl']=$sess_gatekeeper_db.".`staff_type_tbl`";
        }

		return $socTable;
	}
}
