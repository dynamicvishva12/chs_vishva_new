<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcommon extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function insert($tableName="", $insertArr=array(), $returnType="")
	{
		$query = $this->db->insert_batch($tableName, $insertArr);

		$result['query']=$this->db->last_query();
		if($query)
		{
			if($returnType=='lastInsertId')
			{
				$userdata['insertedId']=$this->db->insert_id();
			}
			else
			{
				$userdata=array();
			}
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']=$userdata;
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}

	public function updateBatch($tableName="", $updateArr=array(), $updateKey="", $condtnArr=array(), $likeCondtnArr=array())
	{	
		if(!empty($condtnArr))
		{
			foreach($condtnArr AS $w_col=>$w_val)
			{
				$this->db->where($w_col, $w_val);
			}
		}

		if(!empty($likeCondtnArr))
		{
			foreach($likeCondtnArr AS $l_col=>$l_val)
			{
				$this->db->like($l_col, $l_val);
			}
		}

		$query = $this->db->update_batch($tableName, $updateArr, $updateKey);

		$result['query']=$this->db->last_query();
		if($query)
		{
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']="";
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}

	public function update($tableName="", $updateArr=array(), $condtnArr=array(), $likeCondtnArr=array(), $customWhereArray=array())
	{
		if(!empty($condtnArr))
		{
			foreach($condtnArr AS $w_col=>$w_val)
			{
				$this->db->where($w_col, $w_val);
			}
		}

		if(!empty($likeCondtnArr))
		{
			foreach($likeCondtnArr AS $l_col=>$l_val)
			{
				$this->db->like($l_col, $l_val);
			}
		}

		if(!empty($customWhereArray))
		{
			foreach($customWhereArray AS $cust_wh_val)
			{
				$this->db->where($cust_wh_val);
			}
		}

		$query = $this->db->update($tableName, $updateArr);

		$result['query']=$this->db->last_query();
		if($query)
		{
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']="";
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}

	public function setUpdate($tableName="", $setUpdateArr=array(), $condtnArr=array(), $likeCondtnArr=array())
	{
		if(!empty($setUpdateArr))
		{
			foreach($setUpdateArr AS $st_col=>$st_val)
			{
				$this->db->set($st_col, $st_val, FALSE);
			}
		}

		if(!empty($condtnArr))
		{
			foreach($condtnArr AS $w_col=>$w_val)
			{
				$this->db->where($w_col, $w_val);
			}
		}

		if(!empty($likeCondtnArr))
		{
			foreach($likeCondtnArr AS $l_col=>$l_val)
			{
				$this->db->like($l_col, $l_val);
			}
		}

		$query = $this->db->update($tableName);

		$result['query']=$this->db->last_query();
		if($query)
		{
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']="";
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}

	public function getRecords($tableName="", $colNames="", $condtnArr=array(), $likeCondtnArr=array(), $joinArr=array(), $singleRow=FALSE, $orderByArr=array(), $groupByArr=array(), $whereInArray=array(), $customWhereArray=array(), $backTicks=FALSE, $customOrWhereArray=array(), $orNotLikeArray=array(), $limit="")
	{	
		$this->db->select($colNames, $backTicks);
		$this->db->from($tableName);

		if(!empty($joinArr))
		{
			foreach($joinArr AS $eachJoin)
			{
				$joinTable=$eachJoin['tbl'];
				$joinCondtn=$eachJoin['condtn'];
				$joinType=$eachJoin['type'];

				$this->db->join($joinTable, $joinCondtn, $joinType);
			}
		}

		if(!empty($condtnArr))
		{
			foreach($condtnArr AS $w_col=>$w_val)
			{
				$this->db->where($w_col, $w_val);
			}
		}

		if(!empty($likeCondtnArr))
		{
			foreach($likeCondtnArr AS $l_col=>$l_val)
			{
				$this->db->like($l_col, $l_val);
			}
		}

		if(!empty($whereInArray))
		{
			foreach($whereInArray AS $wh_col=>$wh_val)
			{
				$this->db->where_in($wh_col, $wh_val);
			}
		}

		if(!empty($customWhereArray))
		{
			foreach($customWhereArray AS $cust_wh_val)
			{
				$this->db->where($cust_wh_val);
			}
		}

		if(!empty($customOrWhereArray))
		{
			foreach($customOrWhereArray AS $cust_or_wh_val)
			{
				$this->db->or_where($cust_or_wh_val);
			}
		}

		if(!empty($orNotLikeArray))
		{
			foreach($orNotLikeArray AS $not_like_col=>$not_like_col_val)
			{
				$this->db->not_like($not_like_col, $not_like_col_val);
			}
		}

		if(!empty($orderByArr))
		{
			foreach($orderByArr AS $orderByCol=>$orderByType)
			{
				$this->db->order_by($orderByCol, $orderByType);
			}
		}

		if(!empty($groupByArr))
		{
			$this->db->group_by($groupByArr);
		}

		if($limit!="")
			$this->db->limit($limit);

		$query = $this->db->get();
		$error = $this->db->error();

		$result['query']=$this->db->last_query();

		if(empty($error['code']))
		{
			if($singleRow)
				$responseArr=$query->row_array();
			else
				$responseArr=$query->result_array();

			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']=$responseArr;
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$error;
			$result['userdata']="";
		}

		return $result;
	}

	public function delete($tableName="", $condtnArr=array(), $likeCondtnArr=array(), $whereInArray=array())
	{
		if(!empty($condtnArr))
		{
			foreach($condtnArr AS $w_col=>$w_val)
			{
				$this->db->where($w_col, $w_val);
			}
		}

		if(!empty($likeCondtnArr))
		{
			foreach($likeCondtnArr AS $l_col=>$l_val)
			{
				$this->db->like($l_col, $l_val);
			}
		}

		if(!empty($whereInArray))
		{
			foreach($whereInArray AS $wh_col=>$wh_val)
			{
				$this->db->where_in($wh_col, $wh_val);
			}
		}

		$query=$this->db->delete($tableName);
		$error = $this->db->error();

		$result['query']=$this->db->last_query();
		if(empty($error['code']))
		{
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']="";
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}

	public function plainQuery($sql_query="")
	{
		$query = $this->db->query($sql_query);

		$error = $this->db->error();

		$result['query']=$this->db->last_query();

		if(empty($error['code']))
		{
			$responseArr=$query->result_array();

			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']=$responseArr;
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$error;
			$result['userdata']="";
		}

		return $result;
	}
}


?>