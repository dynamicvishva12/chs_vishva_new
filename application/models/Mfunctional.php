<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfunctional extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->society_db=$this->session->userdata('_society_database');
		$this->society_key=$this->session->userdata('_society_key');
		$this->user_name=$this->session->userdata('_user_name');
		$this->session->userdata('_user_profile');

        $this->load->library('Society_lib');

        $socTables=$this->society_lib->socTables();

		$this->s_r_user_tbl=$socTables['s_r_user_tbl'];
		$this->maintenance_head_tbl=$socTables['maintenance_head_tbl'];
		$this->maintenance_head_charges_tbl=$socTables['maintenance_head_charges_tbl'];
		$this->member_maintenance_tbl=$socTables['member_maintenance_tbl'];
		$this->maintenance_tbl=$socTables['maintenance_tbl'];
		$this->penalty_tbl=$socTables['penalty_tbl'];
		$this->society_master_tbl=$socTables['society_master_tbl'];
		$this->members_advance_log_tbl=$socTables['members_advance_log_tbl'];
		$this->accounting_tbl=$socTables['accounting_tbl'];
    }

	function NAME($val)
	{
		if (!empty($val))
		{
			$val = $this->test_input($val);
		// check if name only contains letters and whitespace
			if (preg_match("/^[a-zA-Z ]*$/",$val))
			{
				return $val;
			}
			else
			{
				return '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Value cannot contain special Character like #, %, *, etc</strong>
				</div>';
			}
		}
		else
		{
			return '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Name cannot be empty</strong>
			</div>';
		}
	}

	function NORMAL($val)
	{
	    // check if name only contains letters and whitespace
	    if (preg_match("/^[a-zA-Z0-9\s,-]*$/",$val))
	    {
	    	$this->test_input($val);
	      	return $val;
	    }
	}

	function NUMNORMAL($val)
	{
	    // check if name only contains letters and whitespace
	    if (preg_match("/^[0-9]*$/",$val))
	    {
	    	$this->test_input($val);
	      	return $val;
	    }
	}


	function NUMNORMALFLOAT($val)
	{
	    // check if name only contains letters and whitespace
	    if (preg_match("/^[0-9].*$/",$val))
	    {	
	    	$this->test_input($val);
	      	return $val;
	    }
	}
	  
	  // Email validation
	function EMAIL($val)
	{
		if (empty($val)) 
		{
			return '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Email is required</strong>
			</div>';
		}
		elseif (!filter_var($val, FILTER_VALIDATE_EMAIL))
		{
			$this->IALERT('Email Should be Valid');
		}
		else
		{
			$val = test_input($val);
			return $val;
		}
	}

	function URL($val)
	{
		if (empty($val))
		{
			$this->WALERT('URL cannot be empty');
		}
		else
		{
			$val = test_input($val);
			// check if URL address syntax is valid (this regular expression also allows dashes in the URL)
			if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$val))
			{
				$this->DALERT("URL cannot be empty");
			}
			else
			{
				return $val;
			}
		}
	}
	  
	function PINCODE($val)
	{
		if(!preg_match('/^\d{6}$/', $val))
		{
			return '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>PINCODE do not contain letter / sign and Of 6 digit</strong>
				  </div>';
	    }
		else
		{
			return $val;
		}
	}

	function MOBILE($val)
	{
		if(!preg_match('/^\d{10}$/', $val))
		{
			return '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Mobile Number do not contain +91/0 and of 10 digit</strong>
				</div>';
		}
		else
		{
			return $val;
		}
	}

	function LANDLINE($val)
	{
		if(!preg_match('/^\d{11}$/', $val))
		{
			return '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>LANDLINE Number do not contain +91 and of 11 digit Including 0(zero)</strong>
					</div>';
		}
		else
		{
			return $val;
		}
	}
		
		
	function GENDER($val)
	{
		if(!$val == "male" || !$val == "female")
		{
			$this->DALERT("Gender Option should be CHECKED");
		}
		else
		{
			return $val;
		}
	}

	function PASSWORD($val)
	{
		if(strlen($val) > 7)
		{
			return $val;
		}
		else
		{
			$this->DALERT('Password minimum length 8');
		}
	}

		
	function date_validate($val)
	{
		if(strtotime($val) > strtotime(date("d-m-Y")))
		{
			return $val;
		}
		else
		{
			$this->WALERT('Date should be greater then current Date');
		}
	}

	function test_input($data)
	{
		$data = trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}  
		
	function DALERT($val)
	{
		return '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>'.$val.'</strong>
				  </div>';
	}

	function IALERT($val)
	{
		return '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>'.$val.'</strong>
				  </div>';
	}

	function WALERT($val)
	{
		return '<div class="alert alert-warning alert-dismissible" style="z-index: 100000000;">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>'.$val.'</strong>
				  </div>';
	}

	function SALERT($val)
	{
		return '<div class="alert alert-success alert-dismissible" style="z-index: 100000000;">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>'.$val.'</strong>
				  </div>';
	}

	function inWords($number)
	{
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);

		$i = 0;
		$str = array();
		$words = array(
			0 => '', 
			1 => 'One', 
			2 => 'Two',
			3 => 'Three', 
			4 => 'Four', 
			5 => 'Five', 
			6 => 'Six',
			7 => 'Seven', 
			8 => 'Eight', 
			9 => 'Nine',
			10 => 'Ten', 
			11 => 'Eleven', 
			12 => 'Twelve',
			13 => 'Thirteen', 
			14 => 'Fourteen', 
			15 => 'Fifteen',
			16 => 'Sixteen', 
			17 => 'Seventeen', 
			18 => 'Eighteen',
			19 => 'Nineteen', 
			20 => 'Twenty', 
			30 => 'Thirty',
			40 => 'Forty', 
			50 => 'Fifty', 
			60 => 'Sixty',
			70 => 'Seventy', 
			80 => 'Eighty', 
			90 => 'Ninety'
		);

		$digits = array('', 'Hundred','Thousand','Lakh', 'Crore');

		while( $i < $digits_length )
		{
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) 
			{
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} 
			else 
				$str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal) ? "." . ($words[$decimal / 10] . "" . $words[$decimal % 10]) . ' Paise' : '';
		$inwords= ($Rupees ? $Rupees . 'Rupees ' : '') . $paise."Only";

		return $inwords;
	}

	function financial_year($soc_key,$userName)
	{
		$currmon= date("m");

		if($currmon>=4) 
		{
			$curryear= date("Y");
		}
		else
		{
			$curryear= date("Y")-1;
		}

		$ryear=$curryear;
		$fyear=( date('m') > 3) ? date('Y') + 1 : date('Y');
		$fyear=$fyear."-03-31";
		$date_to=date("Y-m-d");

		// $checkersql=mysqli_query($conn, "SELECT * FROM `maintenance_head_charges` WHERE `to_date`='$fyear'");

		$chk_condtnArr['maintenance.to_date']=$fyear;

    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_tbl, $colNames="maintenance.user_name", $chk_condtnArr, $chk_likeCondtnArr=array(), $chk_joinArr=array(), $singleRow=TRUE, $chk_orderByArr=array(), $chk_groupByArr=array(), $chk_whereInArray=array(), $chk_customWhereArray=array(), $backTicks=TRUE, $chk_customOrWhereArray=array(), $chk_orNotLikeArray=array());

		$checkersql=$query['userdata'];

		if(empty($checkersql))
		{
			// $headIdsql=mysqli_query($conn, "SELECT `maint_headId`,(SELECT `charges` FROM `maintenance_head_charges` WHERE `maintenance_head_charges`.`maintenance_headId`=`maintenance_head`.`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS charges FROM `maintenance_head` ORDER BY `srid` ASC ");

			$headId_orderByArr['maintenance_head.srid']='ASC';

	    	$query=$this->Mcommon->getRecords($tableName=$this->maintenance_head_tbl, $colNames="maintenance_head.maint_headId, (SELECT `maintenance_head_charges`.`charges` FROM ".$this->maintenance_head_charges_tbl." WHERE `maintenance_head_charges`.`maintenance_headId` = `maintenance_head`.`maint_headId` ORDER BY `srid` DESC LIMIT 1) AS charges", $headId_condtnArr=array(), $headId_likeCondtnArr=array(), $headId_joinArr=array(), $singleRow=FALSE, $headId_orderByArr, $headId_groupByArr=array(), $headId_whereInArray=array(), $headId_customWhereArray=array(), $backTicks=TRUE, $headId_customOrWhereArray=array(), $headId_orNotLikeArray=array());

			$headIdsql=$query['userdata'];

			// if(mysqli_num_rows($headIdsql)>0)
			if(count($headIdsql)>0)
			{
				// while($headId=mysqli_fetch_assoc($headIdsql))
				foreach($headIdsql AS $headId)
				{
					$maintenance_headId=$headId['maint_headId'];
					$charges=$headId['charges'];

					// $headsql=mysqli_query($conn, "INSERT INTO `maintenance_head_charges`(`maintenance_headId`, `charges`, `from_date`, `to_date`, `flag`) VALUES ('$maintenance_headId','$charges','$date_to','$fyear','Yes')");

					$mnt_hd_chrgs_InsertArr=array();

					$mnt_hd_chrgs_InsertArr[]=array(
						'maintenance_headId'=>$maintenance_headId,
						'charges'=>$charges,
						'from_date'=>$date_to,
						'to_date'=>$fyear,
						'flag'=>'Yes',
						'createdBy'=>$this->user_name,
						'createdDatetime'=>$this->curr_datetime
					);

					$query=$this->Mcommon->insert($tableName=$this->maintenance_head_charges_tbl, $mnt_hd_chrgs_InsertArr, $returnType="");

					// $usern = mysqli_query($conn, "SELECT `s-r-username`,(SELECT `charges` FROM `member_maintenance_tbl` WHERE `memberId`=`s-r-username` AND `maintenance_headId`='$maintenance_headId' ORDER BY `srid` DESC LIMIT 1) userCharges FROM `s-r-user` WHERE `disabled_user` NOT LIKE 'disabled' AND `s-r-approve`='approve'");

					$user_condtnArr['s-r-user.s-r-approve']='approve';
					$user_orNotLikeArray['s-r-user.disabled_user']='disabled';

			    	$query=$this->Mcommon->getRecords($tableName=$this->s_r_user_tbl, $colNames="s-r-user.s-r-username, (SELECT `charges` FROM ".$this->member_maintenance_tbl." WHERE `memberId`=`s-r-username` AND `maintenance_headId`='$maintenance_headId' ORDER BY `srid` DESC LIMIT 1) AS userCharges", $user_condtnArr, $user_likeCondtnArr=array(), $user_joinArr=array(), $singleRow=FALSE, $user_orderByArr=array(), $user_groupByArr=array(), $user_whereInArray=array(), $user_customWhereArray=array(), $backTicks=TRUE, $user_customOrWhereArray=array(), $user_orNotLikeArray);

					$usern=$query['userdata'];

					if(!empty($usern))
					{
						foreach($usern AS $row) 
						{
							$username = $row['s-r-username'];
							$userCharges = $row['userCharges'];
							// $tnst_qry=mysqli_query($conn, "INSERT INTO `member_maintenance_tbl`(`memberId`, `maintenance_headId`, `charges`, `from_date`, `to_date`) VALUES ('$username','$maintenance_headId','$userCharges','$date_to','$fyear')");

							$tnst_InsertArr=array();

							$tnst_InsertArr[]=array(
								'memberId'=>$username,
								'maintenance_headId'=>$maintenance_headId,
								'charges'=>$userCharges,
								'from_date'=>$date_to,
								'to_date'=>$fyear,
								'createdBy'=>$this->user_name,
								'createdDatetime'=>$this->curr_datetime
							);

							$query=$this->Mcommon->insert($tableName=$this->member_maintenance_tbl, $tnst_InsertArr, $returnType="");
						}
					}
				}

				// $fetch12=mysqli_query($conn,"UPDATE `member_maintenance_tbl` SET `flag`='No' WHERE `from_date`<'$date_to'");
				// $fetch1=mysqli_query($conn,"UPDATE `maintenance_head_charges` SET `flag`='No' WHERE `from_date`<'$date_to'");

				$mbr_mnt_UpdateArr=array(
					'flag'=>'No',
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$mbr_mnt_condtnArr['member_maintenance_tbl.from_date <']=$date_to;

				$query=$this->Mcommon->update($tableName=$this->member_maintenance_tbl, $mbr_mnt_UpdateArr, $mbr_mnt_condtnArr, $likeCondtnArr=array());

				$mnt_hd_UpdateArr=array(
					'flag'=>'No',
					'updatedBy'=>$this->user_name,
					'updatedDatetime'=>$this->curr_datetime
				);

				$mnt_hd_condtnArr['maintenance_head_charges.from_date <']=$date_to;

				$query=$this->Mcommon->update($tableName=$this->maintenance_head_charges_tbl, $mnt_hd_UpdateArr, $mnt_hd_condtnArr, $likeCondtnArr=array());
			}
		} 
	}

	function timeAgo($time_ago)
	{
	    $time_ago = strtotime($time_ago);
	    $cur_time   = time();
	    $time_elapsed   = $cur_time - $time_ago;
	    $seconds    = $time_elapsed ;
	    $minutes    = round($time_elapsed / 60 );
	    $hours      = round($time_elapsed / 3600);
	    $days       = round($time_elapsed / 86400 );
	    $weeks      = round($time_elapsed / 604800);
	    $months     = round($time_elapsed / 2600640 );
	    $years      = round($time_elapsed / 31207680 );
	    // Seconds
	    if($seconds <= 60){
	        return "just now";
	    }
	    //Minutes
	    else if($minutes <=60){
	        if($minutes==1){
	            return "one minute ago";
	        }
	        else{
	            return "$minutes minutes ago";
	        }
	    }
	    //Hours
	    else if($hours <=24){
	        if($hours==1){
	            return "an hour ago";
	        }else{
	            return "$hours hrs ago";
	        }
	    }
	    //Days
	    else if($days <= 7){
	        if($days==1){
	            return "yesterday";
	        }else{
	            return "$days days ago";
	        }
	    }
	    //Weeks
	    else if($weeks <= 4.3){
	        if($weeks==1){
	            return "a week ago";
	        }else{
	            return "$weeks weeks ago";
	        }
	    }
	    //Months
	    else if($months <=12){
	        if($months==1){
	            return "a month ago";
	        }else{
	            return "$months months ago";
	        }
	    }
	    //Years
	    else{
	        if($years==1){
	            return "one year ago";
	        }else{
	            return "$years years ago";
	        }
	    }
	}
}
?>