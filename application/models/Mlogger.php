<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogger extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

	public function log($logModule="", $logDescription="", $userId="")
	{
		$soc_db=$this->session->userdata('_society_database');

		$log_tbl=$soc_db.'.society_log';

		$insertArr=array(
			'logModule' => $logModule,
			'logDescription' => $logDescription,
			'logTime' => date('H:i:s'),
			'createdBy' => $userId,
			'createdDatetime' => date('Y-m-d H:i:s'),
		);
		$query = $this->db->insert($log_tbl, $insertArr);

		$result['query']=$this->db->last_query();
		if($query)
		{
			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']="";
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$this->db->error();
			$result['userdata']="";
		}

		return $result;
	}
}


?>