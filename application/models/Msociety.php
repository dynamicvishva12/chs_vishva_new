<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msociety extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        if($_SERVER['SERVER_NAME']=='localhost')
        {
            $this->admin_db="aadharch_society_parameter";
        }
        else
        {
            $this->admin_db="aadharch_society_parameter";
        }

        $this->society_list=$this->admin_db.".society_list";
		$this->society_table=$this->admin_db.".society_table";

        $this->society_db = $this->session->userdata('_society_database');
    }

    public function verify_society($societyKey)
    {
    	$this->db->select('society_table.society_key, society_table.society_name');
		$this->db->from($this->admin_db.'.society_table');

		$this->db->where('society_table.society_key', $societyKey);

		$query = $this->db->get();
		$error = $this->db->error();

		$result['query']=$this->db->last_query();

		if(empty($error['code']))
		{
			$responseArr=$query->row_array();

			$result['status']=TRUE;
			$result['message']="";
			$result['userdata']=$responseArr;
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$error;
			$result['userdata']="";
		}

		return $result;
    }

    public function verify_society_user($userName, $societyKey)
    {
	    $this->db->select('s-r-user.s-r-email', TRUE);
		$this->db->from($this->society_db.'.s-r-user');

		$this->db->where('s-r-user.s-r-username', $userName);

		$query = $this->db->get();
		$error = $this->db->error();

		$result['query']=$this->db->last_query();

		if(empty($error['code']))
		{
			$responseArr=$query->row_array();

			$email= $responseArr['s-r-email'];

			$this->db->select('directory.d-body', TRUE);
			$this->db->from($this->society_db.'.directory');

			$this->db->where('directory.important', $email);
			$this->db->where('directory.directory_type', 'type1');
			$this->db->where('directory.status', 'Active');
			$this->db->where("(directory.d-body='treasurer' OR directory.d-body='chairman' OR directory.d-body='secretory' OR directory.d-body='co-secretory')");

			$query = $this->db->get();
			$error = $this->db->error();

			$result['query']=$this->db->last_query();

			if(empty($error['code']))
			{
				$responseDir=$query->row_array();

				$result['status']=TRUE;
				$result['message']="";
				$result['userdata']=$responseDir;
			}
			else
			{
				$result['status']=FALSE;
				$result['message']=$error;
				$result['userdata']="";
			}
		}
		else
		{
			$result['status']=FALSE;
			$result['message']=$error;
			$result['userdata']="";
		}

		return $result;
    }

    public function get_api_key($app_type)
    {
    	$response=array();
    	$this->db->select('fsm_server_key.api_key');
		$this->db->from($this->society_db.'.fsm_server_key');

		if($app_type!="")
			$this->db->where('fsm_server_key.app_type', $app_type);
		else
			return $response;

		$query = $this->db->get();
		$error = $this->db->error();

		if(empty($error['code']))
		{
			$response=$query->row_array();
		}
		return $response;
    }

    public function get_fms_tockens($userName)
    {
		$response=array();
    	$this->db->select('fms_tocken.tocken, fms_tocken.username');
		$this->db->from($this->society_db.'.fms_tocken');

		$this->db->where('fms_tocken.username', $userName);

		$query = $this->db->get();
		$error = $this->db->error();

		if(empty($error['code']))
		{
			$response=$query->result_array();
		}
		return $response;
    }

    public function set_society_session($society_key)
   	{
   		$response=FALSE;
    	$this->db->select('society_table.society_key AS _society_key, society_table.society_database AS _society_database, society_table.status AS _society_status');

		$this->db->from($this->society_table);

		if($society_key!="")
			$this->db->where('society_table.society_key', $society_key);
		else
			return $response;

		$query = $this->db->get();
		$error = $this->db->error();

		if(empty($error['code']))
		{
			$responseArr=$query->row_array();

			if(!empty($responseArr))
			{
				$response=TRUE;

				$this->session->set_userdata($responseArr);
				$this->load->database();
			}
		}
		return $response;
   	}
}


?>