<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
        <h4 class="title">Ledger</h4>
      </div>
      <div class="card-content">

        <?php 
          if(isset($_GET['updateAccId']))
          { 
        ?>
        <form action="<?php echo base_url()."accounting/update_acchead"; ?>" method="post">

          <input type="hidden" name="headId" value="<?php echo $fetch_tbl_accId_head['accounting_id']; ?>">
          <div class="col-md-12">
            <label>Name</label>
            <input type="text" name="head_name" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_accId_head['accounting_name']; ?>">
          </div>
          <div class="col-md-12">
            <label>Groups</label>
            <select name="account_group" class="" style="width: 100%; height: 40px;">
            <option value="">select group</option></option>
            <?php 
              if(!empty($accounting_group_data))
              {
                foreach($accounting_group_data as $key => $value)
                { 
                  if($fetch_tbl_accId_head['Account_group']==$value['id'])
                  {
            ?>
                <option value="<?php echo $value['id']; ?>" <?php if($fetch_tbl_accId_head['Account_group']==$value['id']){ echo  "Selected"; } ?> ><?php echo $value['name']; ?></option>
            <?php 
                  } 
                } 
              } 
            ?>
            </select>
          </div>

          <div class="col-md-12">
            <label>Amount</label>
            <input type="text" name="amount" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_accId_head['opening_balance'] ?>">
          </div>

          <div class="col-md-12">
            <label for="cr"> Cr  <input type="radio" name="cr_dr" id="cr" value="Cr" <?php if($fetch_tbl_accId_head['type_ledger']=="Cr"){ echo  "checked"; } ?>></label>
            <label for="dr"> Dr  <input type="radio" name="cr_dr" id="dr" value="Dr" <?php if($fetch_tbl_accId_head['type_ledger']=="Dr"){ echo  "checked"; } ?>></label>
          </div>

          <button type="submit" name="update_account_head" class="btn btn-primary" style="background: #212121; color: #fff;">Submit</button>
        </form>
        <?php 
          }
          
          if(isset($_GET['updateGroupId']))
          { 
        ?>
        <form action="<?php echo base_url()."accounting/update_group"; ?>" method="post" enctype="multipart/form-data">

          <input type="hidden" name="groupId" value="<?php echo $accounting_groupId_data['id']; ?>">

          <div class="col-md-12">
            <label>Name</label>
            <input type="text" name="group_name" style="height: 40px; width: 100%" value="<?php echo $accounting_groupId_data['name']; ?>" required>
          </div>

          <div class="col-md-12">
            <label>Type of Account</label>
            <select name="group_type" class="" style="width: 100%; height: 40px;" required>
              <option value="Assets" <?php if($accounting_groupId_data['account_type']=="Assets"){ echo  "Selected";} ?>>Assets</option>
              <option value="Liability" <?php if($accounting_groupId_data['account_type']=="Liability"){ echo  "Selected";} ?>>Liability</option>
              <option value="income" <?php if($accounting_groupId_data['account_type']=="Income"){ echo  "Selected";} ?>>Income</option>
              <option value="expenses" <?php if($accounting_groupId_data['account_type']=="Expenses"){ echo  "Selected";} ?>>Expenses</option>
              <option value="Suspnse" <?php if($accounting_groupId_data['account_type']=="Suspnse"){ echo  "Selected";} ?>>Suspnse</option>
            </select>
          </div>

          <button type="submit" name="update_group" class="btn btn-primary" style="background: #212121; color: #fff;">Create Group</button>
        </form> 
        <?php 
          }

          if(isset($_GET['updateBankId']))
          { 
        ?>
          <form action="<?php echo base_url()."accounting/update_bank"; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="bankId" value="<?php echo $fetch_tbl_details_bankId['bank_id']; ?>">

            <div class="col-md-12">
              <label>Bank Name</label>
              <input type="text" name="bank_name" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_details_bankId['bank_name']; ?>">
            </div>

            <div class="col-md-12">
              <label>Bank Branch</label>
              <input type="text" name="bank_br" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_details_bankId['bank_branch']; ?>">
            </div>

            <div class="col-md-12">
              <label>Account Number</label>
              <input type="text" name="bk_acc_no" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_details_bankId['bank_account_number']; ?>">
            </div>

            <div class="col-md-12">
              <label>IFSC Code</label>
              <input type="text" name="bk_ifsc" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_details_bankId['bank_ifsc']; ?>">
            </div>

            <div class="col-md-12">
              <label>Micr</label>
              <input type="text" name="bk_micr" style="height: 40px; width: 100%" value="<?php echo $fetch_tbl_details_bankId['bank_micr']; ?>">
            </div>

            <button type="submit" name="update_bank" class="btn btn-primary" style="background: #212121; color: #fff;">Add Bank</button>
          </form>                         
          <?php 
            }
          ?>
          <form action="<?php echo base_url()."accounting/add_last_pnl"; ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
              <label>Last PNL</label>
              <input type="text" name="pnl" style="height: 40px; width: 100%">
            </div>
            <div class="col-md-12">
              <label for="cr"> Cr  <input type="radio" name="cr_dr" id="cr" value="Cr" checked></label>
              <label for="dr"> Dr  <input type="radio" name="cr_dr" id="dr" value="Dr"></label>
            </div>
            <button type="submit" name="add_pnl" class="btn btn-primary" style="background: #212121; color: #fff;">Add pnl</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
