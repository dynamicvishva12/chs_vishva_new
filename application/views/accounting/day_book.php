<style>
	.class44
	{
		background: #212121;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
	input
	{
		height: 40px;
	}
</style>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
				<h4 class="title">
					Ledger (
					<?php 
						if(isset($_GET['AccId']))
						{ 
							echo 'Account head'; 
						}
						elseif(isset($_GET['GroupId']))
						{ 
							echo 'Group';	
						}
						else
						{ 
							echo 'Day Book'; 
						}  
					?>
					)
				</h4>
			</div>
			<div class="card-content table-responsive">
				<form action="<?php echo base_url()."accounting/day_book"; ?>" method="post">
					<div class="row date">
						<div class="col-lg-4">
							<div class="wrap">
								<label>From</label>
								<input type="date" name="from_date" id="from_date" class="input-style " value="<?php if(isset($_GET['from_date'])){ echo date("Y-m-d",strtotime($_GET['from_date'])); } ?>"  required onchange="datecompare()">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="wrap">
								<label>TO</label>
								<input type="date" name="to_date" id="to_date" class="input-style " value="<?php if(isset($_GET['to_date'])) { echo date("Y-m-d",strtotime($_GET['to_date'])); } ?>" placeholder="Enter Date*" required onchange="datecompare()">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="wrap">
								<div class="">
									<button type="submit" name="filter" class="submit" style="margin-top: 23px;">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<table id="table_id" class="display">
					<thead class="text-primary">
						<th>Sr no</th>
						<th>Date</th>
						<th>Head name</th>
						<th>Amount</th>
						<th>Vaucher Number</th>
						<th>Vaucher Nname</th>
						<th>Cr/Dr</th>
						<th>Action</th>
					</thead>
					<tbody>
					<?php  
						$x=1;
						if(!empty($ladger_data)):
							foreach($ladger_data as $key => $value):
					?>
						<tr class="odd gradeX">
							<td><?php echo $x++; ?></td>
							<td><?php echo date("d-m-Y",strtotime($value['s-date'])); ?></td>
							<td><?php echo $value['headName']; ?></td>
							<td><?php echo number_format($value['accounting_charges']); ?></td>
							<td><?php echo $value['vaucher_no']; ?></td>
							<td><?php echo $value['vaucher_name']; ?></td>
							<td><?php echo $value['cr_dr']." ".$value['vaucher_no']; ?></td>
							<td>  
								<a href="#id04<?php echo $value['a_id']; ?>">View</a>
								<a href="update_ladger_account?transId=<?php echo $value['transaction_no']; ?>">Update</a>
							</td>
						</tr>
						<div id="id04<?php echo $value['a_id'];  ?>" class="modal-rosh">
							<div class="modal-dialog-rosh" >
								<div class="modal-content-rosh" >
									<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
										<a href="#" class="closebtn">×</a>
										<center><h2 style="margin-top: 5px;">Ledger</h2></center>
									</header>
									<div class="container-fluid rosh" style="padding-top: 10px;">
										<div class="row">
											<div class="col-lg-2"><b>Date</b></div>
											<div class="col-lg-2"><b>Head name</b></div>
											<div class="col-lg-2"><b>Amount</b></div>
											<div class="col-lg-2"><b>Vaucher Number</b></div>
											<div class="col-lg-2"><b>Vaucher Nname</b></div>
											<div class="col-lg-2"><b>Cr/Dr</b></div>
											<?php
												// $particular_voc=fetch_ladger_voucherId_data($soc_key, $value['vaucher_no']);
												if(isset($voucherArr[$value['vaucher_no']])):
													$voucherData=$voucherArr[$value['vaucher_no']];
													if(!empty($voucherData)):
											?>
													<div class="col-lg-2"><?php echo date("d-m-Y",strtotime($voucherData['s-date'])); ?></div>
													<div class="col-lg-2"><?php echo $voucherData['headName']; ?></div>
													<div class="col-lg-2"><?php echo number_format($voucherData['accounting_charges']); ?></div>
													<div class="col-lg-2"><?php echo $voucherData['vaucher_no']; ?></div>
													<div class="col-lg-2"><?php echo $voucherData['vaucher_name']; ?></div>
													<div class="col-lg-2"><?php echo $voucherData['cr_dr']; ?></div>
												<?php endif; ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script>
							$(document).ready(function(){
								$('.openPopup').on('click',function(){
									var dataURL = $(this).attr('data-href');
									console.log(dataURL);
									$('.roshan').load(dataURL,function(){
										$('#id04<?php echo $value['a_id'];  ?>').modal({show:true});
									});
								});
							});
						</script>   
						<?php endforeach; ?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	function goBack() 
	{
	    location.reload();
	}
</script>

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
  	$(document).ready( function () {
    	$('#table_id').DataTable();
	});
 	$(document).ready( function () {
    	$('#myTable13').DataTable();
	});
	$(document).ready( function () {
    	$('#myTable14').DataTable();
	});
</script>

<script>
	function datecompare()
	{
		var from_date  = $("#from_date").val(); //2013-09-5
		var to_date    = $("#to_date").val(); //2013-09-10
		document.getElementById("to_date").setAttribute("min", from_date);
	}
</script>

<script>
  	$(document).ready(function() {
    	$('#table_id').DataTable();
	});
</script>

<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++)
    {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>