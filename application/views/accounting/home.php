		<!-- tabs -->

		<div class="row">

			<div class="col-12">
				<div class="box box-default">
					<!-- /.box-header -->
					<div class="box-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-fill" role="tablist">
							<li class="nav-item"> 
								<a class="nav-link acc_tab active" data-toggle="tab" href="#accounting" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Accounting</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link maint_acc_tab" data-toggle="tab" href="#maintenance" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Maintenance Accounting</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link daybook_tab" data-toggle="tab" href="#daybook" role="tab">
									<span></span> <span class="hidden-xs-down ml-15">Day Book</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link op_bal_tab" data-toggle="tab" href="#balance" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Opening Balance</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link grp_tab" data-toggle="tab" href="#group" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Groups</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link bank_tab" data-toggle="tab" href="#bank" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Bank Details</span>
								</a> 
							</li>
							<li class="nav-item"> 
								<a class="nav-link last_pnl_tab" data-toggle="tab" href="#last_pnl" role="tab">
									<span></span> 
									<span class="hidden-xs-down ml-15">Add Last PnL</span>
								</a> 
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane active" id="accounting" role="tabpanel">
								<div class="p-15">
									<form class="form-horizontal" action="<?php echo base_url()."accounting/account_entry"; ?>" method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Transaction No.</label>
													<div class="col-md-12">
													<input type="text" name="acc_trannumber" class="form-control" placeholder="Transaction No." value="<?php echo $trannumber; ?>" readonly>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Voucher Type</label>
													<select name="acc_voucher_type" id="voucher_type" class="form-control select2" style="width: 100%;" onchange="toggleForm()">
													  	<option value="">Select Voucher type</option>
														<option value="Payment Voucher">Payment Voucher</option>
														<option value="Reciept Voucher">Reciept Voucher</option>
														<option value="Contra Voucher">Contra Voucher</option>
														<option value="Journal Voucher">Journal Voucher</option>
													</select>
											  	</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Voucher No.</label>
													<div class="col-md-12">
													<input type="text" name="acc_vno" id="acc_vno" class="form-control" placeholder="Voucher No." readonly>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Transaction type</label>
													<div class="col-md-12">
													<input type="text" name="acc_dr" id="dr" class="form-control" placeholder="Transaction No." value="Dr" readonly>
													</div>
												</div>
											</div>
											<div class="col-md-6" id="acc_head_one" style="display:block">
												<div class="form-group">
													<label>Account Head</label>
													<select name="account_head_dr" class="form-control select2" style="width: 100%;">
													  	<option value="">Select Account head</option>
														<?php 
															if(!empty($accounting_head_data))
															{
																foreach($accounting_head_data as $acc_key => $acc_value)
																{ 
														?>
															<option value="<?php echo $acc_value['accounting_id']; ?>"><?php echo $acc_value['accounting_name']; ?></option>

														<?php 	
																} 
														 	} 
														?>
													</select>
											  	</div>
											</div>
											<div class="col-md-6" id="bank_head_one" style="display:block" >
												<div class="form-group">
													<label>Bank Head</label>
													<select name="acc_bank_head_dr" class="form-control select2" style="width: 100%;">
													  	<option value="">Select Bank/Cash</option>
														<?php 
															if(!empty($banking_account_head))
															{
																foreach($banking_account_head as $bk_key => $bk_value)
																{ 
														?>
															<option value="<?php echo $bk_value['accounting_id']; ?>">
																<?php echo $bk_value['accounting_name']; ?>
															</option>
														<?php 
																} 
														 	} 
														?>
													</select>
											  	</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Transaction type</label>
													<div class="col-md-12">
													<input type="text" name="acc_cr" id="dr" class="form-control" placeholder="Transaction No." value="Cr" readonly>
													</div>
												</div>
											</div>
											<div class="col-md-6" id="acc_head_two" style="display:block" >
												<div class="form-group">
													<label>Account Head</label>
													<select name="account_head_cr" class="form-control select2" style="width: 100%;">
													  	<option value="">Select Account head</option>
														<?php 
															if(!empty($accounting_head_data))
															{
																foreach($accounting_head_data as $acc_key => $acc_value)
																{ 
														?>
															<option value="<?php echo $acc_value['accounting_id']; ?>">
																<?php echo $acc_value['accounting_name']; ?>
															</option>
														<?php 
																} 
															} 
														?>
													</select>
											  	</div>
											</div>
											<div class="col-md-6" id="bank_head_two" style="display:block" >
												<div class="form-group">
													<label>Bank Head</label>
													<select name="bank_head_cr" class="form-control select2" style="width: 100%;">
													  	<option value="">Select Bank/Cash</option>
														<?php 
															if(!empty($banking_account_head))
															{
																foreach($banking_account_head as $bk_key => $bk_value)
																{ 
														?>
															<option value="<?php echo $bk_value['accounting_id']; ?>">
																<?php echo $bk_value['accounting_name']; ?>
															</option>
														<?php 
																} 
														 	} 
														?>
													</select>
											  	</div>
											</div>
											<div class="col-md-6" id="rv_disp" style="display:none" >
												<div class="form-group">
													<label>Member Name</label>
													<select  name="maint_member12" class="" id="member12" class="form-control select2" style="width: 100%;" onchange="getMember()">
													  	<option>Select Member</option>
														<option id="Gokul-A-3-10" value="Gokul-A-3-10">Rahul Waghmare (Gokul-A-3-10)</option>
														<option id="Gokul-A-0-2" value="Gokul-A-0-2">jatin todi (Gokul-A-0-2)</option>
														<option id="Gokul-A-0-3" value="Gokul-A-0-3">sreeraj nair (Gokul-A-0-3)</option>
													</select>
											  	</div>
											</div>
											<div class="col-md-6" id="month_disp" style="display:none" >
												<div class="form-group">
													<label>Select Month</label>
													<ul name="" id="sel_month" class="monthlist">
														<li id="month">April - 4200</li>
														<li id="month">May - 3575</li>
														<li id="month">June - 3600</li>
														<li id="month">July - 4100</li>
														<li id="month">August - 2500</li>
														<li id="month">September - 3200</li>
														<li id="month">October - 4135</li>
													</ul>
											  	</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-12">Narration</label>
											<div class="col-md-12">
											<textarea type="text" name="acc_narratn" maxlength="60" class="form-control" placeholder=""></textarea>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label>Type of Payment</label>
													<select name="acc_Payment" id="p_mode" class="form-control select2" style="width: 100%;" onchange="toggleCheque()">
													  	<option value="no_pay">Cash</option>
														<option value="cheque_yes">Cheque</option>
													</select>
											  	</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Amount</label>
													<div class="col-md-12">
													<input type="text" name="acc_amount" class="form-control" placeholder="Amount">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Receipt Date</label>
													<div class="col-md-12">
													<input type="date" name="acc_s_date" class="form-control" placeholder="Select Date">
													</div>
												</div>
											</div>
										</div>
										<div class="row pay" id="acc_bank" style="display:none">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4">
														<div class="form-group">
															<label>Cheque No</label>
															<input type="text" name="acc_chkno" class="form-control" maxlength="10" placeholder="Cheque No">
													  	</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>Bank Name</label>
															<input type="text" name="acc_bank_name" class="form-control" placeholder="Bank Name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>IFSC code</label>
															<input type="text" name="acc_ifsccode" class="form-control" placeholder="IFSC code">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>MICR code</label>
															<input type="text" name="acc_Micr" class="form-control" placeholder="MICR code">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-12">Upload Document</label>
											<div class="col-md-12">
											<input type="file" name="acc_myfile" class="form-control">
											</div>
										</div>
										<input type="hidden" name="submit_type" id="acc_tab_input" value="acc_tab" >
										<button type="submit" class="btn btn-success text-center">Submit</button>
										<button type="submit" id="acc_tab_next_sub_btn" class="btn btn-success text-center">Submit & Next</button>
										<script>
											$(document).ready(function(){

												$('#acc_tab_next_sub_btn').on('click', function(){

													$('#acc_tab_input').val('maint_acc_tab');

												});


											});
										</script>
									</form>
								</div>
							</div>
							<div class="tab-pane" id="maintenance" role="tabpanel">
								<div class="p-15">
									<form class="form-horizontal" action="<?php echo base_url()."accounting/maintainence_accounting"; ?>" method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Transaction No.</label>
													<div class="col-md-12">
													<input type="text" name="trannumber" class="form-control" placeholder="Transaction No." value="<?php echo $trannumber; ?>" readonly>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label>Voucher Type</label>
													<div class="col-md-12">
													<input type="text" name="rv_name" class="form-control" value="Reciept Voucher" readonly>
													</div>
											  	</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">Voucher No.</label>
													<div class="col-md-12">
													<input type="text" name="vc_number" class="form-control" value="<?php echo $voucherno; ?>" readonly>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Transaction type</label>
													<div class="col-md-12">
													<input type="text" name="Dr" id="dr" class="form-control" placeholder="Transaction No." value="Dr" readonly>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Bank Head</label>
													<select name="bank_head" class="form-control select2" style="width: 100%;">
														<option value="">Select Bank</option>
														<?php
															if(!empty($bank_data))
															{
																foreach ($bank_data as $bkey => $bvalue) 
																{ 
														?>
															<option value="<?php echo $bvalue['bank_id']; ?>"><?php echo $bvalue['bank_name']; ?></option>
														<?php 
																}
															}
														?>
													</select>
											  	</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Transaction type</label>
													<div class="col-md-12">
														<input type="text" name="Cr" id="cr" class="form-control" placeholder="Transaction No." value="Cr" readonly>
														<input type="hidden" id="lentharr" name="lentharr">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Member Name</label>
													<select name="maint_member" id="member" class="form-control select2" style="width: 100%;" onchange="getMember()">
														<option value="">Select Member</option>
														<?php
															if(!empty($member_data))
															{
																foreach ($member_data as $key => $value) 
																{ 
														?>
															<option id="<?php echo $value['s-r-username']; ?>" value="<?php echo $value['s-r-username']; ?>">
																<?php echo ucfirst($value['usrFname']." ".$value['usrLname']." (".$value['s-r-username'].")"); ?>
															</option>
														<?php 
																}
															}
														?>
													</select>
											  	</div>
											</div>
										</div>
										<hr>
										<div class="row hide" id="selecting_months">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Select Month</label>
													<div class="col-md-12">
														<div class="box no-shadow" id="month_list">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Months Selected</label>
													<div class="col-md-12">
														<div class="box no-shadow">
															<div class="box-body" id="months_sel">
														  	</div>
														</div>
													</div>
											  	</div>
											</div>
										</div>
										<script>
											function getMember() 
											{
												$('#months_sel').empty();
												$('#total_amt').empty();
												var username = $("#member").val();
												if (username != '')
												{
													console.log(username);
													$.ajax({
														url:"<?php echo base_url()."remote/accounting/get_monthlist"; ?>",
														method: "POST",
														data: {userid:username},
														dataType: 'json',
														success:function(data)
														{

															// console.log(data);

															// JSON.stringify(response);
															// alert('here cvc');
															var monthdata=data['monthArr'];
															var adv_data=data['advanceElemArr'];

															$('#month_list').html(monthdata);
															$('#advance_div').html(adv_data);

															if(monthdata!="")
															{
																$('#selecting_months').removeClass('row hide');
																$('#selecting_months').addClass('row show');
															}
															else
															{
																$('#selecting_months').removeClass('row show');
																$('#selecting_months').addClass('row hide');
															}
														}
													});
												}
											}
										</script>

										<script>
											//DO NOT CHANGE THE CODE BELOW

											$(document).ready(function(){
											// var length = document.getElementById("sel_month").getElementsByTagName("li").length;
												var month_arr = [];
												var amt_arr = [];
												var user_arr = [];
												var tot_amt = 0;

												$(document).on('click', '#month', function(){

													var selected = $(this).text(); //Selects The month user clicked on
													//var sel_index = $(this).index(); //Index number of the clicked month
													var sel_index = $(this).data('elem'); //Index number of the clicked month
													var sel_user = $(this).attr('class'); //Selected Username
													tot_amt = 0;

													if(user_arr[0] != '')
													{
														user_arr.push(sel_user);
														if(sel_user == user_arr[0])
														{
															// $("#month_list ul#sel_month #month").each(function(index, element){
															$("#month_list div#sel_month #month").each(function(index, element){

																if(index > sel_index) 
																{
																	return false;
																}
																else
																{
																	var temp_var = $(element).text();
																	var make_array = temp_var.split(" - ");
																	month_arr.push(make_array[0]);
																	amt_arr.push(make_array[1]);
																	// $(this).remove();
																	$(this).parents('a').remove();
																	$('#months_sel').empty();
																	$('#total_amt').empty();
																}

															});

															//console.log(month_arr); //shows month
															//console.log(amt_arr); //shows the corresponding amount
															var advance = parseInt($('#advance').val());

															for (var i = 0; i <= month_arr.length-1; i++) 
															{
																tot_amt += parseInt(amt_arr[i]);

																// '<a class="btn btn-success mb-5 d-flex justify-content-between" href="javascript:void(0)">January <span class="pull-right">1800</span></a>';
											
																// '<a class="btn btn-outline btn-success mb-5 d-flex justify-content-between" href="javascript:void(0)">January <span class="pull-right">1800 <span>×</span></span></a>';

																if (i == month_arr.length-1) 
																{
																	// $('#months_sel').append('<div class="tag tag-active" id="'+sel_user+'">'+month_arr[i]+'&nbsp;&nbsp;<i class="fa fa-close" id="close_btn"></i></div>'); 
																	$('#months_sel').append('<a class="btn btn-inact btn-success mb-5 d-flex justify-content-between" href="javascript:void(0)" id="'+sel_user+'">'+month_arr[i]+' <span id="close_btn">×</span></a>'); 
																}
																else
																{
																	// $('#months_sel').append('<div class="tag tag-inactive" id="'+sel_user+'">'+month_arr[i]+'</div>'); 
																	$('#months_sel').append('<a class="btn btn-outline btn-success mb-5 d-flex justify-content-between" href="javascript:void(0)" id="'+sel_user+'">'+month_arr[i]+'</a>'); 
																}
															}

															if(tot_amt>=advance)
															{
																tot_amt -= parseInt(advance);
															}

															if(tot_amt<advance)
															{
																var afteradvance=parseInt(advance)- parseInt(tot_amt);
																$('#afteradvance').val(afteradvance);
																tot_amt = parseInt(0);
															}
															$('#total_amt').val(tot_amt);
															$('#lentharr').val(month_arr.length);
														} //End If  
														else
														{
															month_arr = [];
															amt_arr = [];
															user_arr = [];
														}
													} 

													// $('#month_list a:eq('+sel_index+')').remove();

													// alert(month_arr.toString());
												}); //End each Onclick function

												$(document).on('click', '#close_btn', function(){

													$('#total_amt').empty();
													var user = $('#close_btn').parent().attr('id');
													// var store_index = $(".tag-active").index();
													var store_index = $(".btn-inact").index();
													var new_last_element = store_index - 1;
													var last_amt = amt_arr[amt_arr.length-1];

													tot_amt -= parseInt(amt_arr[store_index]);
													month_arr.pop();
													amt_arr.pop();

													if(tot_amt<0)
													{
														tot_amt = parseInt(0);
													}

													$('#total_amt').val(tot_amt);

													// var active = $(".tag-active").text().replace(/\s/g, '');
													var active_text = $(".btn-inact").text();
													var active_month = active_text.split(" ");
													var active = active_month[0].replace('×', ' ');

													// alert(active);
													// $("#sel_month").prepend('<li id="month" class="'+user+'">'+active+' - '+last_amt+'</li>');
													$(".monthlist").prepend("<a class='btn btn-outline btn-danger mb-5 d-flex justify-content-between' href='javascript:void(0)'> <label id='month' class='"+user+"' data-elem='"+store_index+"' >"+active+" - "+last_amt+" </label></a>");
													// $(".tag-active").remove();
													$(".btn-inact").remove();
													active_text = null;
													active_month = null;
													active = null;
													// $("a."+user).remove();

													for (var i = 0; i <= month_arr.length-1; i++) 
													{
														if (i == month_arr.length-1) 
														{
															// alert(month_arr.length);
															// $('#months_sel div:nth-child('+month_arr.length+')').removeClass("tag-inactive");
															// $('#months_sel div:nth-child('+month_arr.length+')').addClass("tag-active");
															// $('#months_sel div:nth-child('+month_arr.length+')').html(month_arr[new_last_element]+'&nbsp;&nbsp;<i class="fa fa-close" id="close_btn"></i>');
															$('#months_sel a:nth-child('+month_arr.length+')').removeClass("btn-outline");
															$('#months_sel a:nth-child('+month_arr.length+')').addClass("btn-inact");
															$('#months_sel a:nth-child('+month_arr.length+')').html("");
															$('#months_sel a:nth-child('+month_arr.length+')').html(month_arr[new_last_element]+'<span id="close_btn">×</span>');
														}
													}
													$('#lentharr').val(month_arr.length);
												});
											});
											//DO NOT CHANGE THE CODE ABOVE
										</script>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12">Total Amount</label>
													<div class="col-md-12">
													<input type="text"  id="total_amt" name="total_amt" class="form-control" readonly="">
													</div>
												</div>
											</div>
											<div class="col-md-6" id="advance_div"></div>
										</div>
										<hr>
										<div class="form-group">
											<label class="col-md-12">Narration</label>
											<div class="col-md-12">
											<textarea type="text" name="narratn" maxlength="60" class="form-control" placeholder=""></textarea>
											</div>
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label>Type of Payment</label>
													<select name="Payment" id="payment_mode" class="form-control select2" style="width: 100%;">
													  	<option value="Cash">Select Type Of Payment</option>
														<option value="Cheque">Cheque</option>
													</select>
													<input type="hidden" name="afteradvance" id="afteradvance" value="0">
											  	</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-12">Amount</label>
													<div class="col-md-12">
														<input type="number" name="paying_amt" id="paying_amt" class="form-control" placeholder="Amount"  onkeyup="generateBal()" required>
													</div>
													<div class="amt_alert" id="amt_alert" style="display: none;">
														<p>Please enter amount greater than or equal to total amount</p>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-12">Balance Amount</label>
													<div class="col-md-12">
													<input type="text" name="bal_amt" id="bal_amt" class="form-control" readonly="">
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="col-md-12">Receipt Date</label>
													<div class="col-md-12">
													<input type="date" name="s-date" class="form-control" placeholder="Select Date">
													</div>
												</div>
											</div>
										</div>
										<div class="row pay" id="Cheque" style="display:none">
											<div class="col-md-12">
												<div class="form-group row">
													<div class="col-md-4">
														<div class="form-group">
															<label>Cheque No</label>
															<input type="text" name="chkno" class="form-control" maxlength="10" placeholder="Cheque No">
													  	</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>Bank Name</label>
															<input type="text" name="bank_name" class="form-control" placeholder="Bank Name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>IFSC code</label>
															<input type="text" name="ifsccode" class="form-control" placeholder="IFSC code">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<label>MICR code</label>
															<input type="text" name="Micr" class="form-control" placeholder="MICR code">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-12">Upload Document</label>
											<div class="col-md-12">
											<input type="file" name="myfile" class="form-control">
											</div>
										</div>
										<input type="hidden" name="submit_type" id="maint_acc_tab_input" value="maint_acc_tab" >
										<button type="submit" class="btn btn-success text-center">Submit</button>
										<button type="submit" id="maint_acc_tab_next_sub_btn" class="btn btn-success text-center">Submit & Next</button>
									</form>
									<script>
										$(document).ready(function(){

											$('#maint_acc_tab_next_sub_btn').on('click', function(){

												$('#maint_acc_tab_input').val('daybook_tab');

											});


										});
									</script>
								</div>
							</div>
							<div class="tab-pane" id="daybook" role="tabpanel">
								<div class="p-15">
									<form action="<?php echo base_url()."accounting/home"; ?>" method="post">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">From</label>
													<div class="col-md-12">
													<input type="date" name="from_date" id="from_date" class="form-control" placeholder="Select Date" value="<?php echo $from_date_day_bk; ?>" required onchange="datecompare()">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12">To</label>
													<div class="col-md-12">
													<input type="date" name="to_date" id="to_date" class="form-control" placeholder="Select Date" value="<?php echo $to_date_day_bk; ?>" required onchange="datecompare()">
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-12"></label>
													<div class="col-md-12">
														<input type="hidden" name="submit_type" value="daybook_tab" >
														<button type="submit" class="btn btn-info">Search</button>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="row">
										<div class="table-responsive">
									  		<table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
									  				
												<thead>
													<tr>
														<th>Sr no</th>
														<th>Action</th>
														<th>Date</th>
														<th>Head name</th>
														<th>Amount</th>
														<th>Vaucher Number</th>
														<th>Vaucher Nname</th>
														<th>Cr/Dr</th>
													</tr>
												</thead>
												<tbody>
													<?php  
														$x=1;
														if(!empty($ladger_data)):
															foreach($ladger_data as $key => $value):
													?>
													<tr>
														<td><?php echo $x++; ?></td>
														<td>  
															<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="modal" data-target="#day_book<?php echo $value['a_id']; ?>">
																<i class="fa fa-eye"></i>
															</button>

															<a href="update_ladger_account?transId=<?php echo $value['transaction_no']; ?>">
																<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit">
																	<i class="fa fa-edit"></i>
																</button>
															</a>

															<div class="modal fade bs-example-modal-lg" id="day_book<?php echo $value['a_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
																<div class="modal-dialog modal-lg">
																	<div class="modal-content">
																		<div class="modal-header">
																			<h4 class="modal-title" id="myLargeModalLabel">Ledger</h4>
																			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																		</div>
																		<div class="modal-body">
																			<div class="table-responsive">
																				<table class="table b-1 border-primary">
																					<thead class="bg-primary">
																						<tr>
																							<th>Date</th>
																							<th>Head name</th>
																							<th>Amount</th>
																							<th>Vaucher Number</th>
																							<th>Vaucher Name</th>
																							<th>Cr/Dr</th>
																						</tr>
																					</thead>
																					<tbody>
																						<?php
																							// $particular_voc=fetch_ladger_voucherId_data($soc_key, $value['vaucher_no']);
																							if(isset($voucherArr[$value['vaucher_no']])):
																								$voucherData=$voucherArr[$value['vaucher_no']];
																								if(!empty($voucherData)):
																						?>
																							<tr>
																								<td>
																									<?php echo date("d-m-Y",strtotime($voucherData['s-date'])); ?>
																								</td>
																								<td>
																									<?php echo $voucherData['headName']; ?>
																								</td>
																								<td>
																									<?php echo number_format($voucherData['accounting_charges']); ?>
																								</td>
																								<td>
																									<?php echo $voucherData['vaucher_no']; ?>
																								</td>
																								<td>
																									<?php echo $voucherData['vaucher_name']; ?>
																								</td>
																								<td>
																									<?php echo $voucherData['cr_dr']; ?>
																								</td>
																							</tr>
																							<?php endif; ?>
																						<?php endif; ?>
																					</tbody>
																				</table>
																			</div>
																		</div>
																		<div class="modal-footer text-right">
																			<button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
																		</div>
																	</div>
																	<!-- /.modal-content -->
																</div>
																<!-- /.modal-dialog -->
															</div>
														  	<!-- /.modal -->
															<script>
																$(document).ready(function(){
																	$('.openPopup').on('click',function(){
																		var dataURL = $(this).attr('data-href');
																		console.log(dataURL);
																		$('.roshan').load(dataURL,function(){
																			$('#id04<?php echo $value['a_id'];  ?>').modal({show:true});
																		});
																	});
																});
															</script>   
														</td>
														<td><?php echo date("d-m-Y",strtotime($value['s-date'])); ?></td>
														<td><?php echo $value['headName']; ?></td>
														<td><?php echo number_format($value['accounting_charges']); ?></td>
														<td><?php echo $value['vaucher_no']; ?></td>
														<td><?php echo $value['vaucher_name']; ?></td>
														<td><?php echo $value['cr_dr']." ".$value['vaucher_no']; ?></td>
													</tr>
													<?php endforeach; ?>
												<?php endif; ?>
												</tbody>				  
												<tfoot>
													<tr>
														<th>Sr no</th>
														<th>Action</th>
														<th>Date</th>
														<th>Head name</th>
														<th>Amount</th>
														<th>Vaucher Number</th>
														<th>Vaucher Nname</th>
														<th>Cr/Dr</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="balance" role="tabpanel">
								<div class="p-15">
									<div class="row mb-10">
										<div class="col-9">
								  			Opening Balance
								  		</div>
								  		<div class="col-3 text-right">
								  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addBalance">Add Opening Balance</button>
								  		</div>
									</div>
									<div class="row">
										<div class="table-responsive">
									  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
									  				
												<thead>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Account Head</th>
														<th>Account Group</th>
														<th>Opening Balance</th>
														<th>Account Type</th>
													</tr>
												</thead>
												<tbody>
													<?php 
														$x="1";
														if(!empty($fetch_tbl_acc_head)):
															foreach($fetch_tbl_acc_head as $key => $vendor):
													?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td>
																<center>
																	<!-- <a href="<?php //echo htmlspecialchars('account_entry');?>?AccId=<?php //echo $vendor['accounting_id'];?>"> -->
																	<a href="<?php echo base_url()."accounting/view_data"; ?>?AccId=<?php echo $vendor['accounting_id'];?>">
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View">
																			<i class="fa fa-eye"></i>
																		</button>
																	</a>

																	<a href="<?php echo base_url()."accounting/update_acc"; ?>/updateAccId/<?php echo $vendor['accounting_id'];?>">
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</button>
																	</a>
																</center>
															</td>
															<td><?php echo $vendor['accounting_name']; ?></td>
															<td>
																<?php
																	if(empty($vendor['Groupname']))
																		echo $vendor['Account_group'];
																	else
																		echo $vendor['Groupname'];
																?>
															</td>
															<td><?php echo $vendor['opening_balance']; ?></td>
															<td>
																<?php 
																	if(!empty($vendor['type_ledger']))
																		echo $vendor['type_ledger'];
																	else
																		echo $vendor['Groupnature'];
																?>
															</td>
														</tr>
													<?php 
															endforeach;
														endif;
													?>
												</tbody>				  
												<tfoot>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Account Head</th>
														<th>Account Group</th>
														<th>Opening Balance</th>
														<th>Account Type</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="group" role="tabpanel">
								<div class="p-15">
									<div class="row mb-10">
										<div class="col-9">
								  			Create Group
								  		</div>
								  		<div class="col-3 text-right">
								  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addGroup">New Group</button>
								  		</div>
									</div>
									<div class="row">
										<div class="table-responsive">
									  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
												<thead>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Group Name</th>
														<th>Group Type</th>
														<th>Nature</th>
													</tr>
												</thead>
												<tbody>
													<?php 
														$x="1";
														$vendor=array();
														if(!empty($accounting_group_data)):
															foreach($accounting_group_data as $key => $vendor):
													?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td>
																<center>

																	<!-- <a href="<?php //echo htmlspecialchars('account_entry'); ?>?GroupId=<?php //echo $vendor['id']; ?>"> -->
																	<a href="<?php echo base_url()."accounting/view_data"; ?>?GroupId=<?php echo $vendor['id']; ?>">
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View">
																			<i class="fa fa-eye"></i>
																		</button>
																	</a>

																	<a href="<?php echo base_url()."accounting/update_acc"; ?>/updateGroupId/<?php echo $vendor['id']; ?>" >
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</button>
																	</a>
																	
																</center>
															</td>
															<td><?php echo $vendor['name']; ?></td>
															<td><?php echo $vendor['account_type']; ?></td>
															<td><?php echo $vendor['extra']; ?></td>
														</tr>
													<?php 
															endforeach;
														endif;
													?>
												</tbody>				  
												<tfoot>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Group Name</th>
														<th>Group Type</th>
														<th>Nature</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="bank" role="tabpanel">
								<div class="p-15">
									<div class="row mb-10">
										<div class="col-9">
								  		</div>
								  		<div class="col-3 text-right">
								  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addBank">Add Bank</button>
								  		</div>
									</div>
									<div class="row">
										<div class="table-responsive">
									  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
												<thead>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Account Number</th>
														<th>Bank Name</th>
														<th>Bank Branch</th>
														<th>IFSC Code</th>
														<th>MICR</th>
													</tr>
												</thead>
												<tbody>
													<?php 
														$x="1";
														$vendor=array();
														if(!empty($fetch_tbl_details_bank)):
															foreach($fetch_tbl_details_bank as $key => $vendor):
													?>
														<tr>
															<td><?php echo $x++;?></td>
															<td>
																<center>
																	<!-- <a href="<?php //echo htmlspecialchars('account_entry'); ?>?GroupId=<?php //echo $vendor['bank_id']; ?>" > -->
																	<a href="<?php echo base_url()."accounting/view_data"; ?>?GroupId=<?php echo $vendor['bank_id']; ?>" >
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View">
																			<i class="fa fa-eye"></i>
																		</button>
																	</a>

																	<a href="<?php echo base_url()."accounting/update_acc"; ?>/updateBankId/<?php echo $vendor['bank_id']; ?>">
																		<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</button>
																	</a>
																</center>
															</td>
															<td><?php echo $vendor['bank_account_number'];?></td>
															<td><?php echo $vendor['bank_name']; ?></td>
															<td><?php echo $vendor['bank_branch']; ?></td>
															<td><?php echo $vendor['bank_ifsc']; ?></td>
															<td><?php echo $vendor['bank_micr']; ?></td>
														</tr>
													<?php 
															endforeach;
														endif;
													?>    
												</tbody>				  
												<tfoot>
													<tr>
														<th>Sr No</th>
														<th>Action</th>
														<th>Account Number</th>
														<th>Bank Name</th>
														<th>Bank Branch</th>
														<th>IFSC Code</th>
														<th>MICR</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="last_pnl" role="tabpanel">
								<div class="p-15">
									<div class="row mb-10">
										<div class="col-9">
								  			Add Last PnL
								  		</div>
								  		<div class="col-3 text-right"></div>
									</div>
									<form class="form-horizontal" action="<?php echo base_url()."accounting/add_last_pnl"; ?>" method="post" >
										<div class="row">
											<div class="col-12">
									  			<div class="form-group row">
													<label class="col-md-12">Last PNL</label>
													<div class="col-md-12">
														<input type="text" name="pnl" class="form-control" placeholder="Last PnL" required>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-md-6">
														<input name="cr_dr" type="radio" id="cr1" value="Cr" checked />
														<label for="cr1">Cr</label>
													</div>
													<div class="col-md-6">
														<input name="cr_dr" type="radio" id="dr1" value="Dr" />
														<label for="dr1">Dr</label>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-md-6"></div>
													<div class="col-md-6 text-right">
														<input type="hidden" name="submit_type" value="last_pnl_tab" >
														<button type="submit" class="btn btn-success float-right">Add</button>
													</div>
												</div>
									  		</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</div>
		<!-- /.row -->
		<!-- END tabs -->

<div id="addBalance" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."accounting/add_account_head"; ?>" method="post" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Opening Balance</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-md-12">Account Head</label>
						<div class="col-md-12">
							<input type="text" name="head_name" class="form-control" placeholder="Account Head" required>
						</div>
					</div>
					<div class="form-group row">
						<label>Select Group</label>
						<select name="account_group" class="form-control select2" style="width: 100%;" required>
							<option value="">Select group</option></option>
							<?php 
								if(!empty($accounting_group_data))
								{
									foreach($accounting_group_data as $key => $value)
									{ 
							?>
								<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
							<?php  
									} 
								} 
							?>
						</select>
				  	</div>
				  	<div class="form-group row">
						<label class="col-md-12">Amount</label>
						<div class="col-md-12">
							<input type="text" name="amount" class="form-control" placeholder="Amount" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<input name="cr_dr" type="radio" id="cr2" value="Cr" checked />
							<label for="cr2">Cr</label>
						</div>
						<div class="col-md-6">
							<input name="cr_dr" type="radio" id="dr2" value="Dr" />
							<label for="dr2">Dr</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="submit_type" value="op_bal_tab" >
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="addGroup" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."accounting/create_group"; ?>" method="post" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Create Group</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Name</label>
						<div class="col-md-12">
							<input type="text" name="group_name" class="form-control" placeholder="Name">
						</div>
					</div>
					<div class="form-group">
						<label>Select Type</label>
						<select class="form-control select2" name="group_type" style="width: 100%;">
							<option selected="selected">Assets</option>
							<option>Liability</option>
							<option>Income</option>
							<option>Expenses</option>
						</select>
				  	</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="submit_type" value="grp_tab" >
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="addBank" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."accounting/add_bank"; ?>" method="post" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Bank Details</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Account head Name</label>
						<select name="head_bk_name" class="form-control select2" style="width: 100%;" required>
							<option value="">Select Account head</option></option>
							<?php
								if(!empty($fetch_tbl_acc_bank))
								{
									foreach($fetch_tbl_acc_bank as $key => $bank_values)
									{
							?>
								<option value="<?php echo $bank_values['accounting_id'];  ?>">
								<?php echo $bank_values['accounting_name']; ?>
								</option>
							<?php 
									}
								}
							?>
						</select>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Bank Name</label>
								<div class="col-md-12">
								<input type="text" name="bank_name" class="form-control" placeholder="Bank Name">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Bank Branch</label>
								<div class="col-md-12">
								<input type="text" name="bank_br" class="form-control" placeholder="Bank Branch">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Account Number</label>
						<div class="col-md-12">
							<input type="text" name="bk_acc_no" class="form-control" placeholder="Account Number">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">IFSC Code</label>
								<div class="col-md-12">
									<input type="text" name="bk_ifsc" class="form-control" placeholder="IFSC Code">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Micr.</label>
								<div class="col-md-12">
									<input type="text" name="bk_micr" class="form-control" placeholder="Micr.">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="submit_type" value="bank_tab" >
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>


<script>

	function toggleCheque()
	{ 
		var mode = $("#p_mode").val();
		var bank_div = document.getElementById("acc_bank");

		if(mode == "cheque_yes")
		{
			bank_div.style.display = "block";
		}
		else
		{
			bank_div.style.display = "none";
		}
	}

	function toggleForm() 
	{
		var type = $('#voucher_type').val();
		//console.log(type);
		var acc_one = document.getElementById("acc_head_one");
		var acc_two = document.getElementById("acc_head_two");

		var bank_one = document.getElementById("bank_head_one");
		var bank_two = document.getElementById("bank_head_two");

		var rv_display = document.getElementById("rv_disp");
		var month_disp = document.getElementById("month_disp");

		if(type == "Payment Voucher")
		{
			//Non visible
			acc_two.style.display = "none";
			bank_one.style.display = "none";
			rv_display.style.display = "none";
			month_disp.style.display = "none";

			//Visible
			acc_one.style.display = "block";
			bank_two.style.display = "block";
		}
		else if(type == "Reciept Voucher")
		{
			//Non visible
			acc_one.style.display = "none";
			bank_two.style.display = "none";
			rv_display.style.display = "none";
			month_disp.style.display = "none";

			//Visible
			bank_one.style.display = "block";
			acc_two.style.display = "block";
		}
		else if(type == "Contra Voucher")
		{
			//Non visible
			rv_display.style.display = "none";
			month_disp.style.display = "none";
			acc_one.style.display = "none";
			acc_two.style.display = "none";

			//Visible
			bank_one.style.display = "block";
			bank_two.style.display = "block";
		}

		else if(type == "Journal Voucher")
		{
			//Non visible
			bank_one.style.display = "none";
			bank_two.style.display = "none";
			rv_display.style.display = "none";
			month_disp.style.display = "none";

			//Visible
			acc_one.style.display = "block";
			acc_two.style.display = "block";
		}
		else
		{
			//Non visible
			bank_one.style.display = "none";
			bank_two.style.display = "none";
			rv_display.style.display = "none";
			month_disp.style.display = "none";

			//Visible
			acc_one.style.display = "block";
			acc_two.style.display = "block";
		}
	}
</script>


<script>
//category AJAX Call
$(document).ready(function(){

	toggleForm();
    
  	$("#voucher_type").on('change',function(){

        var id = $(this).val();
        //console.log(id);
        if (id != '')
        {
            $.ajax({
                url:"<?php echo base_url()."remote/accounting/acc_vno"; ?>",
                method: "POST",
                data: {query:id},
                dataType: 'json',
                success:function(data)
                {
//                    alert('dfds');
                  // console.log(data);
                    $('#acc_vno').val(data);
                    
                },
            });
        }
        else
        {
        	$('#acc_vno').val("");
        }
    });
    
    $("#voucher").change(function(){
	    var voucher = $('#voucher').val();
	    console.log(voucher);
	    $.post("20mar_account_add",
	    {
	     	voucher : voucher
	    },
	    function(data){
	        $('#variable').html(data);
	    });
    });
});

$(document).ready(function(){
    $("#user").change(function(){
	    var voucher = $('#voucher').val();
	    var user = $('#user').val();
	    var bank_acc = $('#bank_acc').val();
	    var vno = $('#vno').val();
	    console.log(user);
	    $.post("20mar_account_add",
	    {
			bank_acc : bank_acc,
			voucher : voucher,
			vno : vno,
			user : user
	    },
	    function(data){
	        $('#variable').html(data);
	    });
    });
});
</script>

<script>
  $(function() {
        $('#payment_mode').change(function(){

            $('.pay').hide();
            $('#' + $(this).val()).show();

        });
    });
</script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function (){
		$('#myTable12').DataTable();
	});

	$(document).ready(function(){
		$('#myTable13').DataTable();
	});

	$(document).ready(function(){
		$('#myTable14').DataTable();
	});

	function myFunctionpnl() 
	{
		var x = document.getElementById("pnl");
		if (x.style.display === "none") 
		{
			x.style.display = "block";
		} 
		else 
		{
			x.style.display = "none";
		}
	}
</script>


<script>
	var divs = ["default", "open_bal", "create_grp", "bank_details","maintance_acc"];
	var visibleDivId = null;

	function toggleVisibility(divId) 
	{
		if(visibleDivId === divId) 
		{
			//visibleDivId = null;
		} 
		else 
		{
			visibleDivId = divId;
		}
		hideNonVisibleDivs();
	}

	function hideNonVisibleDivs() 
	{
		var i, divId, div;
		for(i = 0; i < divs.length; i++) 
		{
			divId = divs[i];
			div = document.getElementById(divId);

			if(visibleDivId === divId) 
			{
				div.style.display = "block";
			} 
			else 
			{
				div.style.display = "none";
			}
		}
	}
</script>


<script>
	function generateBal()
	{
		// alert('in function');
		var amt_max = $('#total_amt').val();
		if(parseInt(amt_max)>0)
		{
			$('#paying_amt').attr("min",amt_max);
			var pay = $('#paying_amt').val();

			if (parseInt(pay) >= parseInt(amt_max))
			{
				// alert('advance');
				var bal = parseInt(pay)-parseInt(amt_max);
				$('#bal_amt').val(bal);
				$('#amt_alert').hide();
			}
			else
			{
				// alert('zero');
				$('#amt_alert').show();
				$('#bal_amt').val('0');
			}
		}
		else
		{
			var bal= $('#afteradvance').val();
			$('#bal_amt').val(bal);
		}
	}
</script>

<script type="text/javascript">

	$(document).ready(function (){
	
		var submitType = "<?php echo get_cookie('submit_type_cookie'); ?>";

		if(submitType!="" && submitType!=null && submitType!=undefined)
		{
			$('.'+submitType).click();
		}
		else
		{
			$('.acc_tab').click();
		}

	});

</script>

<script>
	function datecompare()
	{
		var from_date  = $("#from_date").val(); //2013-09-5
		var to_date    = $("#to_date").val(); //2013-09-10
		document.getElementById("to_date").setAttribute("min", from_date);
	}
</script>