<link rel="stylesheet" type="text/css" href="<?php echo $assets_url; ?>css/rightnav.css">

<style>
	.modal-dialog 
	{
		right: auto;
		left: 0%;
		width: 600px;
		padding-top: 30px;
		padding-bottom: 30px;
	}

	.class44
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>

<!-- Right Navigation Accounting Tab -->
<script type="text/javascript" src="<?php echo $assets_url; ?>js/rightnav.js"></script>

<div class="right_nav_wrapper" id="wrapper" style="right: -270px;">
	<div class="right_nav_expand" onclick="expandTab();"> 
		<i class="fa fa-chevron-left" id="icon"></i>
	</div>
	<div class="right_nav_body"> 
		<ul>
			<li onclick="toggleVisibility('open_bal');" >Open Balance</li>
			<li onclick="toggleVisibility('create_grp');" >Create Group</li>
			<!--<li onclick="toggleVisibility('account_head');">Account Head</li>-->
			<li onclick="toggleVisibility('bank_details');" >Add Bank Details</li>
			<hr>
			<li onclick="toggleVisibility('default');" >Accounting Tab</li>
			<li onclick="toggleVisibility('maintance_acc');" >Maintainence Accounting</li>
			<li><a href="<?php echo base_url()."accounting/day_book"; ?>">Day book</a></li>
			<li><a href="<?php echo base_url()."accounting/add_last_pnl"; ?>" >Add last pnl</a></li>
		</ul>  
	</div>
</div>
<!-- Right Navigation Accounting Tab -->

<div class="content" id="default">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
						<h4 class="title">Accounting Tab</h4>
					</div>
					<div class="card-content">

						<form action="<?php echo base_url()."accounting/account_entry"; ?>" method="post" enctype="multipart/form-data">

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Transaction Number</strong>
										</label>
										<input type="text" name="acc_trannumber" value="<?php echo $trannumber; ?>" style=" width: 100%; height: 40px;" readonly>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Voucher Type</strong>
										</label>
										<select name="acc_voucher_type" id="voucher_type" style="height: 40px; width: 100%" onchange="toggleForm()">
											<option value="">Select Voucher type</option>
											<option value="Payment Voucher">Payment Voucher</option>
											<option value="Reciept Voucher">Reciept Voucher</option>
											<option value="Contra Voucher">Contra Voucher</option>
											<option value="Journal Voucher">Journal Voucher</option>
										</select>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Voucher Number</strong>
										</label>
										<input type="text" name="acc_vno" id="acc_vno" style="height: 40px; width: 100%" readonly>
									</div>
								</div>
							</div>   

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="">
											<strong style="font-size: 14px;">Transaction Type</strong>
										</label>
										<input type="text" name="acc_dr" id="dr" value="Dr" style="height: 40px; width: 100%" readonly>
									</div>
								</div>
								<div class="col-md-3" id="acc_head_one" style="display:block">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Account Head</strong>
										</label>
										<select name="account_head_dr" class="" style="width: 100%; height: 40px;">
											<option value="">Select Account head</option>
											<?php 
												if(!empty($accounting_head_data))
												{
													foreach($accounting_head_data as $acc_key => $acc_value)
													{ 
											?>
												<option value="<?php echo $acc_value['accounting_id']; ?>"><?php echo $acc_value['accounting_name']; ?></option>

											<?php 	
													} 
											 	} 
											?>
										</select>
									</div>
								</div>

								<div class="col-md-3" id="bank_head_one" style="display:none">
									<div class="form-group">
										<label><strong style="font-size: 14px;">Bank Head</strong></label>
										<select name="acc_bank_head_dr" class="" style="width: 100%; height: 40px;">
											<option value="">Select Bank/cash</option>
											<?php 
												if(!empty($banking_account_head))
												{
													foreach($banking_account_head as $bk_key => $bk_value)
													{ 
											?>
												<option value="<?php echo $bk_value['accounting_id']; ?>">
													<?php echo $bk_value['accounting_name']; ?>
												</option>
											<?php 
													} 
											 	} 
											?>
										</select>
									</div>
								</div>
							</div><!--Row ends here  -->

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="">
											<strong style="font-size: 14px;" >Transaction Type</strong>
										</label>
										<input type="text" name="acc_cr" id="dr" value="Cr" style="height: 40px; width: 100%" readonly>
									</div>
								</div>
								<div class="col-md-3" id="acc_head_two" style="display:block">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Account Head</strong>
										</label>
										<select name="account_head_cr" class="" style="width: 100%; height: 40px;">
											<option value="">Select Account head</option>
											<?php 
												if(!empty($accounting_head_data))
												{
													foreach($accounting_head_data as $acc_key => $acc_value)
													{ 
											?>
												<option value="<?php echo $acc_value['accounting_id']; ?>">
													<?php echo $acc_value['accounting_name']; ?>
												</option>
											<?php 
													} 
												} 
											?>
										</select>
									</div>
								</div>
								<div class="col-md-3" id="bank_head_two" style="display:none">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Bank Head</strong>
										</label>
										<select name="bank_head_cr" class="" style="width: 100%; height: 40px;">
											<option value="">Select Bank/cash</option>
											<?php 
												if(!empty($banking_account_head))
												{
													foreach($banking_account_head as $bk_key => $bk_value)
													{ 
											?>
												<option value="<?php echo $bk_value['accounting_id']; ?>"><?php echo $bk_value['accounting_name']; ?></option>
											<?php 
													} 
												} 
											?>
										</select>
									</div>
								</div>
								<div class="col-md-3" id="rv_disp" style="display:none">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Member Name</strong>
										</label>
										<select name="maint_member12" class="" id="member12" style="width: 100%; height: 40px;" onchange="getMember()">
											<option>Select Member</option>
											<option id="Gokul-A-3-10" value="Gokul-A-3-10">Rahul Waghmare (Gokul-A-3-10)</option>
											<option id="Gokul-A-0-2" value="Gokul-A-0-2">jatin todi (Gokul-A-0-2)</option>
											<option id="Gokul-A-0-3" value="Gokul-A-0-3">sreeraj nair (Gokul-A-0-3)</option>
										</select>
									</div>
								</div>
								<div class="col-md-3" id="month_disp" style="display:none">
									<div class="form-group">
										<label>
											<strong style="font-size: 14px;">Select Month</strong>
										</label>
										<ul name="" id="sel_month" class="monthlist">
											<li id="month">April - 4200</li>
											<li id="month">May - 3575</li>
											<li id="month">June - 3600</li>
											<li id="month">July - 4100</li>
											<li id="month">August - 2500</li>
											<li id="month">September - 3200</li>
											<li id="month">October - 4135</li>
										</ul>
									</div>
								</div>
							</div>
							<!--Row ends here-->

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>
											<strong>Narration</strong>
										</label>
										<textarea type="text" name="acc_narratn" maxlength="60" style=" width: 100%"></textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label><strong>Type Of Payment</strong></label>
										<select name="acc_Payment" id="p_mode" style="height: 40px; width: 100%" onchange="toggleCheque()">
											<option value="no_pay">Cash</option>
											<option value="cheque_yes">Cheque</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>
											<strong>Amount</strong>
										</label>
										<input type="text" name="acc_amount" style="height: 40px; width: 100%">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group" >
										<label>
											<strong>Recipt Date</strong>
										</label>
										<input type="date" name="acc_s_date" placeholder="DD-MM-YY" style="height: 40px; width: 100%">
									</div>
								</div>
							</div>

							<div class="pay" id="acc_bank" style="display:none">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label><strong>Cheque No</strong></label>
											<input type="text" name="acc_chkno" style="height: 40px; width: 100%" maxlength="10">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label><strong>Bank Name</strong></label>
											<input type="text" name="acc_bank_name" style="height: 40px; width: 100%">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label><strong>IFSC code</strong></label>
											<input type="text" name="acc_ifsccode" style="height: 40px; width: 100%">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label><strong> MICR Code</strong></label>
											<input type="text" name="acc_Micr" style="height: 40px; width: 100%">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<center> 
											<label class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="padding-top: 10px;background: #212121; color: #fff;">
												<strong> Upload Doc's</strong>
											</label>
											<input type="file" name="acc_myfile" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
										</center>
									</div>
								</div>
							</div>
							<br>
							<center>
								<button type="submit" name="account_entry" class="btn btn-primary" style="background: #212121; color: #fff;">Save</button>
							</center>
							<div class="clearfix"></div>
						</form>

						<script>

							function toggleCheque()
							{ 
								var mode = $("#p_mode").val();
								var bank_div = document.getElementById("acc_bank");

								if(mode == "cheque_yes")
								{
									bank_div.style.display = "block";
								}
								else
								{
									bank_div.style.display = "none";
								}
							}

							function toggleForm() 
							{
								var type = $('#voucher_type').val();
								//console.log(type);
								var acc_one = document.getElementById("acc_head_one");
								var acc_two = document.getElementById("acc_head_two");

								var bank_one = document.getElementById("bank_head_one");
								var bank_two = document.getElementById("bank_head_two");

								var rv_display = document.getElementById("rv_disp");
								var month_disp = document.getElementById("month_disp");

								if(type == "Payment Voucher")
								{
									//Non visible
									acc_two.style.display = "none";
									bank_one.style.display = "none";
									rv_display.style.display = "none";
									month_disp.style.display = "none";

									//Visible
									acc_one.style.display = "block";
									bank_two.style.display = "block";
								}
								else if(type == "Reciept Voucher")
								{
									//Non visible
									acc_one.style.display = "none";
									bank_two.style.display = "none";
									rv_display.style.display = "none";
									month_disp.style.display = "none";

									//Visible
									bank_one.style.display = "block";
									acc_two.style.display = "block";
								}
								else if(type == "Contra Voucher")
								{
									//Non visible
									rv_display.style.display = "none";
									month_disp.style.display = "none";
									acc_one.style.display = "none";
									acc_two.style.display = "none";

									//Visible
									bank_one.style.display = "block";
									bank_two.style.display = "block";
								}

								else if(type == "Journal Voucher")
								{
									//Non visible
									bank_one.style.display = "none";
									bank_two.style.display = "none";
									rv_display.style.display = "none";
									month_disp.style.display = "none";

									//Visible
									acc_one.style.display = "block";
									acc_two.style.display = "block";
								}
								else
								{
									//Non visible
									bank_one.style.display = "none";
									bank_two.style.display = "none";
									rv_display.style.display = "none";
									month_disp.style.display = "none";

									//Visible
									acc_one.style.display = "block";
									acc_two.style.display = "block";
								}
							}
						</script>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Toggling Forms -->
<div class="content"> 
	<div class="container-fluid">
		<!-- Open Balance -->
		<div class="open_bal" id="open_bal" style="display: none;"> 
			<form action="<?php echo base_url()."accounting/add_account_head"; ?>" method="post" enctype="multipart/form-data">

				<div class="row">
					<div class="col-md-12">
						<label>Name</label>
						<input type="text" name="head_name" style="height: 40px; width: 100%">
					</div>

					<div class="col-md-12">
						<label>Groups</label>
						<select name="account_group" class="" style="width: 100%; height: 40px;">
							<option value="">select group</option></option>
							<?php 
								if(!empty($accounting_group_data))
								{
									foreach($accounting_group_data as $key => $value)
									{ 
							?>
								<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
							<?php  
									} 
								} 
							?>
						</select>
					</div>

					<div class="col-md-12">
						<label>Amount</label>
						<input type="text" name="amount" style="height: 40px; width: 100%">
					</div>

					<div class="col-md-12">
						<label for="cr"> Cr  <input type="radio" name="cr_dr" id="cr" value="Cr" checked></label>
						<label for="dr"> Dr  <input type="radio" name="cr_dr" id="dr" value="Dr"></label>
					</div>

					<div class="col-md-12">
						<button type="submit" name="add_account_head" class="btn btn-primary" style="background: #212121; color: #fff;">Submit</button>
					</div>
				</div>
			</form>

			<div class="card-content table-responsive">
				<table id="myTable12" class="display">
					<thead>
						<th>sr no</th>
						<th>Account head</th>
						<th>Account group</th>
						<th>Opening balance</th>
						<th>Account type</th>
						<th>Action</th>
					</thead>
					<tbody>
					<?php 
						$x="1";
						if(!empty($fetch_tbl_acc_head))
						{
							foreach($fetch_tbl_acc_head as $key => $vendor) 
							{
					?>
						<tr class="odd gradeX">
							<td><?php echo $x++; ?></td>
							<td><?php echo $vendor['accounting_name']; ?></td>
							<td>
								<?php
									if(empty($vendor['Groupname']))
									{
										echo $vendor['Account_group'];
									}
									else
									{
										echo $vendor['Groupname'];
									}
								?>
							</td>
							<td><?php echo $vendor['opening_balance']; ?></td>
							<td>
								<?php 
									if(!empty($vendor['type_ledger']))
									{
										echo $vendor['type_ledger'];
									}
									else
									{
										echo $vendor['Groupnature'];
									}
								?>
							</td>
							<td>
								<center>
									<a href="<?php echo htmlspecialchars('account_entry');?>?AccId=<?php echo $vendor['accounting_id'];?>" class="confirmation">View</a>                                 
									<a href="<?php echo base_url()."accounting/add_last_pnl"; ?>?updateAccId=<?php echo $vendor['accounting_id'];?>" class="confirmation">Update</a>
								</center>
							</td>
						</tr>
					<?php 
							}
						}
					?>                 
					</tbody>
				</table>
			</div>
		</div>
		<!-- Open Balance -->

		<!-- Create Group -->
		<div class="create_grp" id="create_grp" style="display: none;"> 
			<form action="<?php echo base_url()."accounting/create_group"; ?>" method="post" enctype="multipart/form-data">
				<div class="col-md-12">
					<label>Name</label>
					<input type="text" name="group_name" style="height: 40px; width: 100%" required>
				</div>
				<div class="col-md-12">
					<label>Type of Account</label>
					<select name="group_type" class="" style="width: 100%; height: 40px;" required>
						<option value="Assets">Assets</option>
						<option value="Liability">Liability</option>
						<option value="income">Income</option>
						<option value="expenses">Expenses</option>
						<option value="Suspnse">Suspnse</option>
					</select>
				</div>
				<button type="submit" name="create_group" class="btn btn-primary" style="background: #212121; color: #fff;">Create Group</button>
			</form>
			<div class="card-content table-responsive">
				<table id="myTable13" class="display">
					<thead>
						<th>sr no</th>
						<th>Group Name</th>
						<th>Group Type</th>
						<th>Group Nature</th>
						<th>Action</th>
					</thead>
					<tbody>
					<?php 
						$x="1";
						$vendor=array();
						if(!empty($accounting_group_data))
						{
							foreach($accounting_group_data as $key => $vendor) 
							{
					?>
						<tr class="odd gradeX">
							<td><?php echo $x++; ?></td>
							<td><?php echo $vendor['name']; ?></td>
							<td>
								<?php
									echo $vendor['account_type'];
								?>
							</td>
							<td><?php echo $vendor['extra']; ?></td>
							<td>
								<center>
									<a href="<?php echo htmlspecialchars('account_entry'); ?>?GroupId=<?php echo $vendor['id']; ?>" class="confirmation">View</a>
									<a href="<?php echo base_url()."accounting/add_last_pnl"; ?>?updateGroupId=<?php echo $vendor['id']; ?>" class="confirmation">Update</a>
								</center>
							</td>
						</tr>
					<?php 
							}
						}
					?>                 
					</tbody>
				</table>
			</div>
		</div>
		<!-- Create Group -->
		<!-- Bank Details -->
		<div class="bank_details" id="bank_details" style="display: none;"> 
			<form action="<?php echo base_url()."accounting/add_bank"; ?>" method="post" enctype="multipart/form-data">
				<div class="col-md-12">
					<label>Account head Name</label>
					<select name="head_bk_name" style="height: 40px; width: 100%" required>
						<?php
							if(!empty($fetch_tbl_acc_bank))
							{
								foreach($fetch_tbl_acc_bank as $key => $bank_values)
								{
						?>
							<option value="<?php echo $bank_values['accounting_id'];  ?>">
							<?php echo $bank_values['accounting_name']; ?>
							</option>
						<?php 
								}
							}
						?>
					</select>
				</div>

				<div class="col-md-12">
					<label>Bank Name</label>
					<input type="text" name="bank_name" style="height: 40px; width: 100%">
				</div>

				<div class="col-md-12">
					<label>Bank Branch</label>
					<input type="text" name="bank_br" style="height: 40px; width: 100%">
				</div>

				<div class="col-md-12">
					<label>Account Number</label>
					<input type="text" name="bk_acc_no" style="height: 40px; width: 100%">
				</div>

				<div class="col-md-12">
					<label>IFSC Code</label>
					<input type="text" name="bk_ifsc" style="height: 40px; width: 100%">
				</div>

				<div class="col-md-12">
					<label>Micr</label>
					<input type="text" name="bk_micr" style="height: 40px; width: 100%">
				</div>

				<button type="submit" name="add_bank" class="btn btn-primary" style="background: #212121; color: #fff;">Add Bank</button>
			</form>

			<div class="card-content table-responsive">
				<table id="myTable14" class="display">
					<thead>
						<th>sr no</th>
						<th>Account Number</th>
						<th>bank_name</th>
						<th>Bank Branch</th>
						<th>IFSC Code</th>
						<th>Micr</th>
						<th>Action</th>
					</thead>
					<tbody>
					<?php 
						$x="1";
						$vendor=array();
						if(!empty($fetch_tbl_details_bank))
						{
							foreach($fetch_tbl_details_bank as $key => $vendor) 
							{
					?>
						<tr class="odd gradeX">
							<td><?php echo $x++;?></td>
							<td><?php echo $vendor['bank_account_number'];?></td>
							<td><?php echo $vendor['bank_name']; ?></td>
							<td><?php echo $vendor['bank_branch']; ?></td>
							<td><?php echo $vendor['bank_ifsc']; ?></td>
							<td><?php echo $vendor['bank_micr']; ?></td>
							<td>
								<center>
									<a href="<?php echo htmlspecialchars('account_entry'); ?>?GroupId=<?php echo $vendor['bank_id']; ?>" class="confirmation">View</a>
									<a href="<?php echo base_url()."accounting/add_last_pnl"; ?>?updateBankId=<?php echo $vendor['bank_id']; ?>" class="confirmation">Update</a>
								</center>
							</td>
						</tr>
					<?php 
							}
						}
					?>    
					</tbody>
				</table>
			</div>
		</div>
		<!-- Bank Details -->

		<!-- Maintainence Accounting -->
		<div class="maintance_acc" id="maintance_acc" style="display: none;"> 
			<form action="<?php echo base_url()."accounting/maintainence_accounting"; ?>" method="post" enctype="multipart/form-data"?>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Transaction Number</strong>
							</label>
							<input type="text" name="trannumber" value="<?php echo $trannumber; ?>" style=" width: 100%; height: 40px;" readonly>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Voucher Type</strong>
							</label>
							<input type="text" name="rv_name" value="Reciept Voucher" style=" width: 100%; height: 40px;" readonly>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Voucher Number</strong>
							</label>
							<input type="text" name="vc_number" value="<?php echo $voucherno; ?>" style="height: 40px; width: 100%" readonly>
						</div>
					</div>
				</div>   

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label class="">
								<strong style="font-size: 14px;">Transaction Type</strong>
							</label>
							<input type="text" name="Dr" id="dr" value="Dr" style="height: 40px; width: 100%" readonly>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Bank Head</strong>
							</label>
							<select name="bank_head" class="" style="width: 100%; height: 40px;">
								<option value="">Select Bank</option>
								<?php
									if(!empty($bank_data))
									{
										foreach ($bank_data as $bkey => $bvalue) 
										{ 
								?>
									<option value="<?php echo $bvalue['bank_id']; ?>"><?php echo $bvalue['bank_name']; ?></option>
								<?php 
										}
									}
								?>
							</select>
						</div>
					</div>
				</div><!--Row ends here  -->

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Transaction Type</strong>
							</label>
							<input type="text" name="Cr" id="cr" value="Cr" style="height: 40px; width: 100%" readonly>
							<input type="hidden" id="lentharr" name="lentharr">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Member Name</strong>
							</label>
							<select name="maint_member" class="" id="member" style="width: 100%; height: 40px;" onchange="getMember()">
								<option value="">Select Member</option>
								<?php
									if(!empty($member_data))
									{
										foreach ($member_data as $key => $value) 
										{ 
								?>
									<option id="<?php echo $value['s-r-username']; ?>" value="<?php echo $value['s-r-username']; ?>">
										<?php echo $value['usrFname']." ".$value['usrLname']." (".$value['s-r-username'].")"; ?>
									</option>
								<?php 
										}
									}
								?>
							</select>
						</div>
					</div>

					<script>
						function getMember() 
						{
							$('#months_sel').empty();
							$('#total_amt').empty();
							var username = $("#member").val();
							if (username != '')
							{
								console.log(username);
								$.ajax({
									url:"<?php echo base_url()."remote/accounting/get_monthlist"; ?>",
									method: "POST",
									data: {userid:username},
									success:function(data)
									{
										console.log(data);
										// alert('here cvc');
										$('#month_list').html(data);
									}
								});
							}
						}
					</script>

					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong style="font-size: 14px;">Select Month</strong>
							</label>
							<div id="month_list"></div>
						</div>
					</div>

					<script>
						//DO NOT CHANGE THE CODE BELOW

						$(document).ready(function(){
						// var length = document.getElementById("sel_month").getElementsByTagName("li").length;
							var month_arr = [];
							var amt_arr = [];
							var user_arr = [];
							var tot_amt = 0;

							$(document).on('click', '#month', function(){

								var selected = $(this).text(); //Selects The month user clicked on
								var sel_index = $(this).index(); //Index number of the clicked month
								var sel_user = $(this).attr('class'); //Selected Username
								tot_amt = 0;

								if(user_arr[0] != '')
								{
									user_arr.push(sel_user);
									if(sel_user == user_arr[0])
									{
										$("#month_list ul#sel_month #month").each(function(index, element){

											if (index > sel_index) 
											{
												return false;
											}
											else
											{
												var temp_var = $(element).text();
												var make_array = temp_var.split(" - ");
												month_arr.push(make_array[0]);
												amt_arr.push(make_array[1]);
												$(this).remove();
												$('#months_sel').empty();
												$('#total_amt').empty();
											}

										});

										//console.log(month_arr); //shows month
										//console.log(amt_arr); //shows the corresponding amount
										var advance = parseInt($('#advance').val());

										for (var i = 0; i <= month_arr.length-1; i++) 
										{
											tot_amt += parseInt(amt_arr[i]);

											if (i == month_arr.length-1) 
											{
												$('#months_sel').append('<div class="tag tag-active" id="'+sel_user+'">'+month_arr[i]+'&nbsp;&nbsp;<i class="fa fa-close" id="close_btn"></i></div>'); 
											}
											else
											{
												$('#months_sel').append('<div class="tag tag-inactive" id="'+sel_user+'">'+month_arr[i]+'</div>'); 
											}
										}

										if(tot_amt>=advance)
										{
											tot_amt -= parseInt(advance);
										}

										if(tot_amt<advance)
										{
											var afteradvance=parseInt(advance)- parseInt(tot_amt);
											$('#afteradvance').val(afteradvance);
											tot_amt = parseInt(0);
										}
										$('#total_amt').val(tot_amt);
										$('#lentharr').val(month_arr.length);
									} //End If  
									else
									{
										month_arr = [];
										amt_arr = [];
										user_arr = [];
									}
								}
							}); //End each Onclick function

							$(document).on('click', '#close_btn', function(){
								
								$('#total_amt').empty();
								var user = $('#close_btn').parent().attr('id');
								var store_index = $(".tag-active").index();
								var new_last_element = store_index - 1;
								var last_amt = amt_arr[amt_arr.length-1];

								tot_amt -= parseInt(amt_arr[store_index]);
								month_arr.pop();
								amt_arr.pop();

								if(tot_amt<0)
								{
									tot_amt = parseInt(0);
								}

								$('#total_amt').val(tot_amt);

								var active = $(".tag-active").text().replace(/\s/g, '');
								$("#sel_month").prepend('<li id="month" class="'+user+'">'+active+' - '+last_amt+'</li>');
								$(".tag-active").remove();

								for (var i = 0; i <= month_arr.length-1; i++) 
								{
									if (i == month_arr.length-1) 
									{
										$('#months_sel div:nth-child('+month_arr.length+')').removeClass("tag-inactive");
										$('#months_sel div:nth-child('+month_arr.length+')').addClass("tag-active");
										$('#months_sel div:nth-child('+month_arr.length+')').html(month_arr[new_last_element]+'&nbsp;&nbsp;<i class="fa fa-close" id="close_btn"></i>');
									}
								}
								$('#lentharr').val(month_arr.length);
							});
						});
						//DO NOT CHANGE THE CODE ABOVE
					</script>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label><strong>Months Selected</strong></label>
							<div type="text" class="textarea_style" id="months_sel" name="months_sel" style="width:auto;"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label><strong>Total Amount</strong></label>
							<textarea type="text" id="total_amt" name="total_amt" style=" width: 100%"></textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label><strong>Narration</strong></label>
							<textarea type="text" name="narratn" maxlength="60" style=" width: 100%"></textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong>Type Of Payment</strong>
							</label>
							<select name="Payment" id="payment_mode" style="height: 40px; width: 100%">
								<option value="Cash">Select Type Of Payment</option>
								<option value="Cheque">Cheque</option>
							</select>
							<input type="hidden" name="afteradvance" id="afteradvance" value="0">
						</div>
					</div>
					
					<div class="col-md-3">
						<div class="form-group">
							<label>
								<strong>Amount</strong>
							</label>
							<input type="number" name="paying_amt" id="paying_amt" style="height: 40px; width: 100%" onkeyup="generateBal()" required>
						</div>
					<div class="amt_alert" id="amt_alert" style="display: none;">
						<p>Please enter amount greater than or equal to total amount</p>
					</div>
				</div>

				<script>
					function generateBal()
					{
						// alert('in function');
						var amt_max = $('#total_amt').val();
						if(parseInt(amt_max)>0)
						{
							$('#paying_amt').attr("min",amt_max);
							var pay = $('#paying_amt').val();

							if (parseInt(pay) >= parseInt(amt_max))
							{
								// alert('advance');
								var bal = parseInt(pay)-parseInt(amt_max);
								$('#bal_amt').val(bal);
								$('#amt_alert').hide();
							}
							else
							{
								// alert('zero');
								$('#amt_alert').show();
								$('#bal_amt').val('0');
							}
						}
						else
						{
							var bal= $('#afteradvance').val();
							$('#bal_amt').val(bal);
						}
					}
				</script>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label><strong>Balance Amount</strong></label>
							<input type="text" name="bal_amt" id="bal_amt" style="height: 40px; width: 100%" readonly>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group" >
							<label><strong>Recipt Date</strong></label>
							<input type="date" name="s-date" placeholder="DD-MM-YY" style="height: 40px; width: 100%">
						</div>
					</div>
				</div>

				<div class="pay" id="Cheque" style="display:none">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label><strong>Cheque No</strong></label>
								<input type="text" name="chkno" style="height: 40px; width: 100%" maxlength="10">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><strong>Bank Name</strong></label>
								<input type="text" name="bank_name" style="height: 40px; width: 100%">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><strong>IFSC code</strong></label>
								<input type="text" name="ifsccode" style="height: 40px; width: 100%">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label><strong> MICR Code</strong></label>
								<input type="text" name="Micr" style="height: 40px; width: 100%">
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<center> 
								<label class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="padding-top: 10px;background: #212121; color: #fff;">
									<strong> Upload Doc's</strong>
								</label>
								<input type="file" name="myfile" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
							</center>
						</div>
					</div>
				</div><br>
				<center>
					<button type="submit" name="maint_account" class="btn btn-primary" style="background: #212121; color: #fff;">Save</button>
				</center>
				<div class="clearfix"></div>
			</form>
			<!-- Maintainence Accounting -->
		</div>
	</div>
	<!-- Toggling Forms -->
	<!-- </div> -->
</div>

<script>
//category AJAX Call
$(document).ready(function(){
    
  	$("#voucher_type").on('change',function(){

        var id = $(this).val();
        //console.log(id);
        if (id != '')
        {
            $.ajax({
                url:"<?php echo base_url()."remote/accounting/acc_vno"; ?>",
                method: "POST",
                data: {query:id},
                dataType: 'json',
                success:function(data)
                {
//                    alert('dfds');
                  // console.log(data);
                    $('#acc_vno').val(data);
                    
                },
            });
        }
    });
    
    $("#voucher").change(function(){
	    var voucher = $('#voucher').val();
	    console.log(voucher);
	    $.post("20mar_account_add",
	    {
	     	voucher : voucher
	    },
	    function(data){
	        $('#variable').html(data);
	    });
    });
});

$(document).ready(function(){
    $("#user").change(function(){
	    var voucher = $('#voucher').val();
	    var user = $('#user').val();
	    var bank_acc = $('#bank_acc').val();
	    var vno = $('#vno').val();
	    console.log(user);
	    $.post("20mar_account_add",
	    {
			bank_acc : bank_acc,
			voucher : voucher,
			vno : vno,
			user : user
	    },
	    function(data){
	        $('#variable').html(data);
	    });
    });
});
</script>

<script>
  $(function() {
        $('#payment_mode').change(function(){

            $('.pay').hide();
            $('#' + $(this).val()).show();

        });
    });
</script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function (){
		$('#myTable12').DataTable();
	});

	$(document).ready(function(){
		$('#myTable13').DataTable();
	});

	$(document).ready(function(){
		$('#myTable14').DataTable();
	});

	function myFunctionpnl() 
	{
		var x = document.getElementById("pnl");
		if (x.style.display === "none") 
		{
			x.style.display = "block";
		} 
		else 
		{
			x.style.display = "none";
		}
	}
</script>


<script>
	var divs = ["default", "open_bal", "create_grp", "bank_details","maintance_acc"];
	var visibleDivId = null;

	function toggleVisibility(divId) 
	{
		if(visibleDivId === divId) 
		{
			//visibleDivId = null;
		} 
		else 
		{
			visibleDivId = divId;
		}
		hideNonVisibleDivs();
	}

	function hideNonVisibleDivs() 
	{
		var i, divId, div;
		for(i = 0; i < divs.length; i++) 
		{
			divId = divs[i];
			div = document.getElementById(divId);

			if(visibleDivId === divId) 
			{
				div.style.display = "block";
			} 
			else 
			{
				div.style.display = "none";
			}
		}
	}
</script>