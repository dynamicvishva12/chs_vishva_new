<div class="row">

	<div class="col-12">
		<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-body">

			<?php 
		      if($updateType=='updateAccId')
		      { 
		    ?>
		    <form class="form-horizontal" action="<?php echo base_url()."accounting/update_acchead"; ?>" method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">Name</label>
							<div class="col-md-12">
							<input type="text" name="head_name" class="form-control" placeholder="Name" value="<?php echo $fetch_tbl_accId_head['accounting_name']; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Groups</label>
							<select name="account_group" class="form-control select2" style="width: 100%;" onchange="toggleForm()">
							  	<option value="">Select group</option></option>
					            <?php 
					              if(!empty($accounting_group_data))
					              {
					                foreach($accounting_group_data as $key => $value)
					                { 
					                  if($fetch_tbl_accId_head['Account_group']==$value['id'])
					                  {
					            ?>
					                <option value="<?php echo $value['id']; ?>" <?php if($fetch_tbl_accId_head['Account_group']==$value['id']){ echo  "Selected"; } ?> ><?php echo $value['name']; ?></option>
					            <?php 
					                  } 
					                } 
					              } 
					            ?>
							</select>
					  	</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">Amount</label>
							<div class="col-md-12">
							<input type="text" name="amount" class="form-control" placeholder="Amount" value="<?php echo $fetch_tbl_accId_head['opening_balance'] ?>">
							</div>
						</div>
					</div>				
					<div class="col-md-6">
						<div class="form-group row">
							<label class="col-md-12">Type</label>
							<div class="col-md-2">
								<input name="cr_dr" type="radio" id="cr2" value="Cr" <?php if($fetch_tbl_accId_head['type_ledger']=="Cr"){ echo  "checked"; } ?> />
								<label for="cr2">Cr</label>
							</div>
							<div class="col-md-2">
								<input name="cr_dr" type="radio" id="dr2" value="Dr" <?php if($fetch_tbl_accId_head['type_ledger']=="Dr"){ echo  "checked"; } ?>/>
								<label for="dr2">Dr</label>
							</div>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<input type="hidden" name="submit_type" value="op_bal_tab" >
						<input type="hidden" name="headId" value="<?php echo $fetch_tbl_accId_head['accounting_id']; ?>">
						<button type="submit" name="update_account_head" class="btn btn-success text-center">Submit</button>
					</div>
				</div>
			</form>
		    <?php 
				}

				if($updateType=='updateGroupId')
				{ 
		    ?>
		    <form class="form-horizontal" action="<?php echo base_url()."accounting/update_group"; ?>" method="post">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-12">Name</label>
							<div class="col-md-12">
							<input type="text" name="group_name" class="form-control" placeholder="Name" value="<?php echo $accounting_groupId_data['name']; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Type of Account</label>
							<select name="group_type" class="form-control select2" style="width: 100%;" onchange="toggleForm()">
							  	<option value="Assets" <?php if($accounting_groupId_data['account_type']=="Assets"){ echo  "Selected";} ?>>Assets</option>
								<option value="Liability" <?php if($accounting_groupId_data['account_type']=="Liability"){ echo  "Selected";} ?>>Liability</option>
								<option value="income" <?php if($accounting_groupId_data['account_type']=="Income"){ echo  "Selected";} ?>>Income</option>
								<option value="expenses" <?php if($accounting_groupId_data['account_type']=="Expenses"){ echo  "Selected";} ?>>Expenses</option>
								<option value="Suspnse" <?php if($accounting_groupId_data['account_type']=="Suspnse"){ echo  "Selected";} ?>>Suspnse</option>
							</select>
					  	</div>
					</div>	
					<div class="col-md-12 text-right">
						<input type="hidden" name="submit_type" value="grp_tab" >
						<input type="hidden" name="groupId" value="<?php echo $accounting_groupId_data['id']; ?>">
						<button type="submit" name="update_group" class="btn btn-success text-center">Submit</button>	
					</div>		
				</div>
			</form>

		    <?php 
				}

				if($updateType=='updateBankId')
				{ 
		    ?>
		    <form class="form-horizontal" action="<?php echo base_url()."accounting/update_bank"; ?>" method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">Bank Name</label>
							<div class="col-md-12">
							<input type="text" name="bank_name" class="form-control" placeholder="Bank Name" value="<?php echo $fetch_tbl_details_bankId['bank_name']; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">Bank Branch</label>
							<div class="col-md-12">
							<input type="text" name="bank_br" class="form-control" placeholder="Bank Branch" value="<?php echo $fetch_tbl_details_bankId['bank_branch']; ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-12">Account Number</label>
					<div class="col-md-12">
						<input type="text" name="bk_acc_no" class="form-control" placeholder="Account Number" value="<?php echo $fetch_tbl_details_bankId['bank_account_number']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">IFSC Code</label>
							<div class="col-md-12">
								<input type="text" name="bk_ifsc" class="form-control" placeholder="IFSC Code" value="<?php echo $fetch_tbl_details_bankId['bank_ifsc']; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-12">Micr.</label>
							<div class="col-md-12">
								<input type="text" name="bk_micr" class="form-control" placeholder="Micr." value="<?php echo $fetch_tbl_details_bankId['bank_micr']; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-12 text-right">
						<input type="hidden" name="submit_type" value="bank_tab" >
						<input type="hidden" name="bankId" value="<?php echo $fetch_tbl_details_bankId['bank_id']; ?>">
						<button type="submit" name="update_bank" class="btn btn-success text-center">Submit</button>
					</div>
				</div>
				
			</form>
		    <?php 
				}
			?>
			</div>
		</div>
	</div>
</div>