<div class="row">

	<div class="col-12">
		<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-header with-border">
		 		<div class="row">
					<div class="col-10">
			  			<h3 class="box-title">
			  				Ledger (
							<?php 
								if(isset($_GET['AccId']))
								{ 
									echo 'Account head'; 
								}
								elseif(isset($_GET['GroupId']))
								{ 
									echo 'Group';	
								}
								else
								{ 
									echo 'Day Book'; 
								}  
							?>
							)
			  			</h3>
			  		</div>
			  		<div class="col-2 text-right"></div>
				</div>
		 	</div>
			<div class="box-body">
				<form action="<?php echo base_url()."accounting/view_data".$getStringParams; ?>" method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-12">From</label>
								<div class="col-md-12">
								<input type="date" name="from_date" id="from_date" class="form-control" placeholder="Select Date" value="<?php echo set_value('from_date'); ?>" required onchange="datecompare()">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-12">To</label>
								<div class="col-md-12">
								<input type="date" name="to_date" id="to_date" class="form-control" placeholder="Select Date" value="<?php echo set_value('to_date'); ?>" required onchange="datecompare()">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-12"></label>
								<div class="col-md-12">
									<button type="submit" class="btn btn-info">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="row">
					<div class="table-responsive">
				  		<table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				  				
							<thead>
								<tr>
									<th>Sr no</th>
									<th>Action</th>
									<th>Date</th>
									<th>Head name</th>
									<th>Amount</th>
									<th>Vaucher Number</th>
									<th>Vaucher Nname</th>
									<th>Cr/Dr</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									$x=1;
									if(!empty($ladger_data)):
										foreach($ladger_data as $key => $value):
								?>
								<tr>
									<td><?php echo $x++; ?></td>
									<td>  
										<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="modal" data-target="#day_book<?php echo $value['a_id']; ?>">
											<i class="fa fa-eye"></i>
										</button>

										<a href="update_ladger_account?transId=<?php echo $value['transaction_no']; ?>">
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit">
												<i class="fa fa-edit"></i>
											</button>
										</a>

										<div class="modal fade bs-example-modal-lg" id="day_book<?php echo $value['a_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myLargeModalLabel">Ledger</h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													</div>
													<div class="modal-body">
														<div class="table-responsive">
															<table class="table b-1 border-primary">
																<thead class="bg-primary">
																	<tr>
																		<th>Date</th>
																		<th>Head name</th>
																		<th>Amount</th>
																		<th>Vaucher Number</th>
																		<th>Vaucher Name</th>
																		<th>Cr/Dr</th>
																	</tr>
																</thead>
																<tbody>
																	<?php
																		// $particular_voc=fetch_ladger_voucherId_data($soc_key, $value['vaucher_no']);
																		if(isset($voucherArr[$value['vaucher_no']])):
																			$voucherData=$voucherArr[$value['vaucher_no']];
																			if(!empty($voucherData)):
																	?>
																		<tr>
																			<td>
																				<?php echo date("d-m-Y",strtotime($voucherData['s-date'])); ?>
																			</td>
																			<td>
																				<?php echo $voucherData['headName']; ?>
																			</td>
																			<td>
																				<?php echo number_format($voucherData['accounting_charges']); ?>
																			</td>
																			<td>
																				<?php echo $voucherData['vaucher_no']; ?>
																			</td>
																			<td>
																				<?php echo $voucherData['vaucher_name']; ?>
																			</td>
																			<td>
																				<?php echo $voucherData['cr_dr']; ?>
																			</td>
																		</tr>
																		<?php endif; ?>
																	<?php endif; ?>
																</tbody>
															</table>
														</div>
													</div>
													<div class="modal-footer text-right">
														<button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
													</div>
												</div>
												<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										</div>
									  	<!-- /.modal -->
										<script>
											$(document).ready(function(){
												$('.openPopup').on('click',function(){
													var dataURL = $(this).attr('data-href');
													console.log(dataURL);
													$('.roshan').load(dataURL,function(){
														$('#id04<?php echo $value['a_id'];  ?>').modal({show:true});
													});
												});
											});
										</script>   
									</td>
									<td><?php echo date("d-m-Y",strtotime($value['s-date'])); ?></td>
									<td><?php echo $value['headName']; ?></td>
									<td><?php echo number_format($value['accounting_charges']); ?></td>
									<td><?php echo $value['vaucher_no']; ?></td>
									<td><?php echo $value['vaucher_name']; ?></td>
									<td><?php echo $value['cr_dr']." ".$value['vaucher_no']; ?></td>
								</tr>
								<?php endforeach; ?>
							<?php endif; ?>
							</tbody>				  
							<tfoot>
								<tr>
									<th>Sr no</th>
									<th>Action</th>
									<th>Date</th>
									<th>Head name</th>
									<th>Amount</th>
									<th>Vaucher Number</th>
									<th>Vaucher Nname</th>
									<th>Cr/Dr</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
