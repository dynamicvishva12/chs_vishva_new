		<!-- START Card With Image -->
		  <div class="row">
		  	<?php if(!empty($gallery_data)): ?>
                <?php foreach($gallery_data as $key => $row): ?>
					<div class="col-md-12 col-lg-4">
					  <div class="card">
						  <img class="card-img-top img-responsive" src="<?php echo $row['albumnFilename']; ?>" width="350px" height="230px" alt="Card image cap">
						<div class="card-body">  
							<a href="<?php echo $row['albumnFilename']; ?>" target="_blank" >         	
								<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success">
									<i class="fa fa-eye"></i>
								</button>
							</a>
							<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."albums/delete_photo/".$album_name."/".$row['f-id']; ?>"<?php echo base_url()."albums/delete_photo/".$album_name."/".$row['f-id']; ?>>
								<i class="fa fa-trash-o"></i>
							</button>
							<a href="<?php echo $row['albumnFilename']; ?>" download="" >
								<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-primary">
									<i class="fa fa-download"></i>
								</button>
							</a>
						</div>
					  </div>
					</div>
				<?php endforeach; ?>
            <?php endif; ?>
		  </div>