<style>
    .class7
    {
        background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    } 
    .cd-case 
    {
        height: 206px;
        width: 230px;
        margin:20px 0px 15px 35px;
        display: inline-block;
        border-radius: 3px;
        background: rgba(0,0,0,0);
        box-shadow: 0px 2px 10px 1px rgba(0,0,0,0.6), inset 0px 0px 20px 2px rgba(0,0,0,0.4), inset 0px 0px 5px 1px rgba(255,255,255,0.6), inset 0px 0px 0px 1px rgba(255,255,255,0.2);
    }
    .album-art 
    {
        height: 200px;
        width: 200px;
        border-radius: 3px;
        background: rgba(0,0,0,0);
        box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.5),inset 0px 0px 16px 2px rgba(0,0,0,0.1), inset 0px 0px 0px 1px rgba(255,255,255,0.3);
        position: relative;
        top: 3px;
        left: 25px;
        z-index: -2;
    }
    #case1 .album-art 
    {
        background:  linear-gradient(rgba(255,255,255,0.15),rgba(255,255,255,0)), url(http://upload.wikimedia.org/wikipedia/en/2/22/Led_Zeppelin_-_Led_Zeppelin_III_%281970%29_front_cover.png) center center no-repeat;
        background-size: cover;
    }
    #case2 .album-art 
    {
        background:  linear-gradient(rgba(255,255,255,0.15),rgba(255,255,255,0)), url(http://upload.wikimedia.org/wikipedia/en/5/5c/Floyd_PULSE_Cover.jpg) center center no-repeat;
        background-size: 100% 100%;
    }
    #case3 .album-art 
    {
        background: linear-gradient(rgba(255,255,255,0.15),rgba(255,255,255,0)), url(http://upload.wikimedia.org/wikipedia/en/4/49/Hotelcalifornia.jpg) center center no-repeat;
        background-size: 100% 100%;
    }
    #case4 .album-art 
    {
        background: linear-gradient(rgba(255,255,255,0.15),rgba(255,255,255,0)), url(http://upload.wikimedia.org/wikipedia/en/0/0d/Acdcironman.jpg) center center no-repeat;
        background-size: 100% 100%;
    }
    #case5 .album-art 
    {
        background: linear-gradient(rgba(255,255,255,0.15),rgba(255,255,255,0)), url(http://upload.wikimedia.org/wikipedia/en/1/1a/Scorpions_-_Sting_in_the_Tail.jpg) center center no-repeat;
        background-size: 100% 100%;
    }
    .sup 
    {
        height: 10px;
        width: 20px;
        background: rgba(255,255,255,0.15);
        box-shadow: inset 0px 0px 6px 1px rgba(0,0,0,0.22), inset 0px 0px 1px 1px rgba(255,255,255,0.17);
    }
    .pos-tl 
    {
        position: relative;
        top: 0;
        left: 20px;
        border-radius: 0 0 5px 5px;
    }
    .pos-tr 
    {
        position: relative;
        top: -10px;
        left: 160px;
        border-radius: 0 0 5px 5px;
    }
    .pos-bl 
    {
        position: relative;
        top: 169px;
        left: 20px;
        border-radius: 5px 5px 0 0;
    }
    .pos-br 
    {
        position: relative;
        top: 159px;
        left: 160px;
        border-radius: 5px 5px 0 0;
    }
    .spine 
    {
        height: 120px;
        width: 15px;
        background: -webkit-linear-gradient(rgba(255,255,255,0.2) 0%, rgba(255,255,255,0.07) 100%);
        box-shadow: 3px 0px 5px 0px rgba(0,0,0,0.23), inset 0px 0px 7px 0px rgba(0,0,0,0.22), inset 0px 0px 0px 1px rgba(255,255,255,0.22);
        position: relative;
        top: -160px;
        left: 0;
        border-radius: 0 50% 50% 0;
        z-index: -1;
    }
</style>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 


    <div class="row">
        <div class=" col-md-12" >

            <?php if(!empty($gallery_data)): ?>
                <?php foreach($gallery_data as $key => $row): ?>

                    <div class="col-md-4">
                        <div class="demo-card-square mdl-card mdl-shadow--2dp">
                            <div class="mdl-card__title mdl-card--expand" style="background-image: url(<?php echo $row['albumnFilename']; ?>);"></div>
                            <center>
                                <div class="mdl-card__actions mdl-card--border">
                                    
                                    <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect confirmation" href="<?php echo base_url()."albums/delete_photo/".$album_name."/".$row['f-id']; ?>" style="background: linear-gradient(60deg, #004D40, #33691E); color: #fff;">
                                        Delete
                                    </a>
                                    <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" href="<?php echo $row['albumnFilename']; ?>" target="_blank" style="background: linear-gradient(60deg, #004D40, #33691E); color: #fff;">
                                        View
                                    </a>
                                </div>
                            </center>
                        </div><br>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>


    <a href="<?php echo base_url()."albums/list"; ?>">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored confirmation" style="position: fixed;top: 85%; right: 3%; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
            <i class="material-icons">undo</i>
        </button>
    </a>

    <script type="text/javascript">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure?')) e.preventDefault();
            };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>