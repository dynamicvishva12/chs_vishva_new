<!-- tabs -->

		  <div class="row">

			<div class="col-12">
			  <div class="box box-default">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h3 class="box-title">Album</h3>
				  		</div>
				  		<div class="col-2 text-right">
				  			
				  		</div>
					</div>
			 	</div>
				<div class="box-body">
					<div class="row">
						<?php if(!empty($album_data)): ?>
                    		<?php foreach($album_data as $key => $now): ?>
                    		<?php 
                    			$albumnName = $now['albumnName']; 
                    			$albName=str_replace(" ", "_", $albumnName);
                    			if($now['albumnFilename'] != NULL)
                                {
                                    $albImg=$now['albumnFilename'];
                                }
                                else
                                {
                                    $albImg=$assets_url."IMAGES/thephotoalbum1.jpg";
                                }
                    		?>
							<div class="col-md-12 col-lg-4">
								<div class="card">
								  <img class="card-img-top img-responsive" src="<?php echo $albImg; ?>" width="350px" height="230px" alt="Card image cap">
								<div class="card-body">            	
									<h4 class="card-title"><?php echo $albumnName; ?></h4>
									<a href="<?php echo base_url()."albums/gallery/".$albName; ?>" class="btn btn-rounded btn-primary">View</a>
								</div>
							  </div>
							</div>
							<?php endforeach; ?>
                		<?php endif; ?>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->