<style>
    .class7
    {
        background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    .class222
    {
        background:#212121; color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    .mdl-button:hover
    {
        background-color: #212121;
        color: #fff;
    }
</style>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 

    <div class="row" style="padding-left: 10px;">
        <div class="col-md-6">
            <a href="<?php echo base_url()."events/list"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class111" style="width: 100%;">
                    Events
                </button>
            </a>
        </div>
        <div class="col-md-6">
            <a href="<?php echo base_url()."albums/list"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class222" style="width: 100%;">
                    Society Album
                </button>
            </a>
        </div>
    </div><br>

    <div class="container-fluid">
        <div class="row">
            <form method="post" action="">
                <?php if(!empty($album_data)): ?>
                    <?php foreach($album_data as $key => $now): ?>
                        <div class="col-lg-3">
                            <div class="demo-card-square mdl-card mdl-shadow--2dp" style=" width: 230px; height: 250px; margin-bottom: 20px;">
                                <div class="mdl-card__title mdl-card--expand" style="background: url(<?php
                                $filename = $now['albumnName'];
                                if($now['albumnFilename'] != NULL)
                                {
                                    echo $now['albumnFilename'];
                                }
                                else
                                {
                                    echo "IMAGES/thephotoalbum1.jpg";
                                }
                                ?>); background-size: 100% 100%;">
                                    <h2 class="mdl-card__title-text"><?php echo $now['albumnName']; ?></h2>
                                </div>
                                <div class="mdl-card__supporting-text">
                                    <?php echo $now['albumnDesc']; ?>
                                </div>
                                <div class="mdl-card__actions mdl-card--border">
                                    <center> 
                                        <a href="<?php echo base_url()."albums/gallery/".$now['albumnName']; ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
                                            View
                                        </a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </form>

        </div>
    </div>