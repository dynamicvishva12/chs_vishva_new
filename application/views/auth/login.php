	<div class="row align-items-center justify-content-md-center h-p100">	
			
		<div class="col-12">
			<div class="row justify-content-center no-gutters">
				<div class="col-lg-5 col-md-5 col-12">
					<div class="bg-white rounded30 shadow-lg">
						<div class="content-top-agile p-20 pb-0">
							<h2 class="text-primary">Let's Get Started</h2>
							<p class="mb-0">Sign in</p>							
						</div>
						<div class="p-40">
							<form action="" method="post">
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
										</div>
										<input type="text" name="email" class="form-control pl-15 bg-transparent" placeholder="Username">
										<?php if(!empty(form_error('email'))): ?>
						                    <small><?php echo form_error('email'); ?></small>
						                <?php endif; ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
										</div>
										<input type="password" name="password" class="form-control pl-15 bg-transparent" placeholder="Password">
										<?php if(!empty(form_error('password'))): ?>
						                    <small><?php echo form_error('password'); ?></small>
						                <?php endif; ?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
										</div>
										<input type="text" name="societykey" class="form-control pl-15 bg-transparent" placeholder="Society Key">
										<?php if(!empty(form_error('societykey'))): ?>
						                    <small><?php echo form_error('societykey'); ?></small>
						                <?php endif; ?>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="checkbox">
											<input type="checkbox" id="basic_checkbox_1" >
											<label for="basic_checkbox_1">Remember Me</label>
										</div>
									</div>
									<!-- /.col -->
									<div class="col-6">
										<div class="fog-pwd text-right">
											<a href="<?php echo base_url().'auth/forgot_password'; ?>" class="hover-warning"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
										</div>
									</div>
									<!-- /.col -->
									<div class="col-12 text-center">
										<button type="submit" class="btn btn-danger mt-10">SIGN IN</button>
									</div>
								<!-- /.col -->
								</div>
							</form>	

						</div>						
					</div>

				</div>
			</div>
		</div>
	</div>