<?php
    // ob_start();
    // session_start();
    // include '../CHS_Admin_controllers/society_data.php';
    // include '../CHS_Admin_controllers/login_data.php';
    // include'../CHS_Admin_modules/login_data.php';
?>

<style>

.right-side small p{
    padding: 0px;
    color: red;
}

</style>

<div class="col-sm-6 left-side">
    <center>  
        <img src="<?php echo $assets_url; ?>img/logo/CHS Vishva final logo.png" width="350px"> 
    </center>
    <br>
    <br>
    <br>
    <center>
        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background: linear-gradient(60deg, #004D40, #33691E); color: #fff;  margin-right: 10px; width: 160px; font-size: 12px; border-radius: 20px; margin-left:20px;" href="<?php echo base_url().'auth/forgot_password'; ?>">Forgot Password</a>
    </center>
    <br>
    <br>
    <br>
    <h4>
        <i class="material-icons" style="font-size: 17px;">copyright</i> 2018. All Rights Reserved.<br>
        <strong>Design & Developed By<br> 
        <u> Dynamic Vishva Softwares . . .</u></strong><br>
        <strong>
            E-mail Id :<a href="mailto:info@dynamicvishva.in" target="_blank"><strong style="color: linear-gradient(60deg, #004D40, #33691E);  "> info@dynamicvishva.in</strong></a><br>
            Contact us: 02249740618
        </strong>
    </h4>
    </div>
    <div class="col-sm-6 right-side">
    <h1>Admin Login</h1>
    <!--Form with header-->
    <div class="form">
        <form role="form" action="" method="post">
            <div class="form-group">
                <label for="form2"><strong style="font-size: 14px;">E-mail</strong></label>
                <strong><input type="text" id="form2" name="email" class="form-control" style="font-size: 12px;" value="<?php echo set_value('email'); ?>"></strong>
                <?php if(!empty(form_error('email'))): ?>
                    <small><?php echo form_error('email'); ?></small>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="form4"><strong style="font-size: 14px;">Password</strong></label>
                <strong><input type="password" name="password" id="form4" class="form-control" value="<?php echo set_value('password'); ?>"></strong>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck" onclick="myFunction()">
                    <label class="custom-control-label" for="customCheck">Show Password</label>
                </div>
                <?php if(!empty(form_error('password'))): ?>
                    <small><?php echo form_error('password'); ?></small>
                <?php endif; ?>
            </div>
            <div class="form-group">
                <label for="form4"><strong style="font-size: 14px;">Society Key</strong></label>
                <strong><input type="text" name="societykey" id="form5" class="form-control" value="<?php echo set_value('societykey'); ?>"></strong>
                <?php if(!empty(form_error('societykey'))): ?>
                    <small><?php echo form_error('societykey'); ?></small>
                <?php endif; ?>
            </div>
            <div class="text-xs-center">
                <center>
                    <button type="submit" name="login" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background: linear-gradient(60deg, #004D40, #33691E); color: #fff;  margin-right: 10px; width: 160px; font-size: 12px; border-radius: 20px;margin-left:20px;" >Login</button>
                    <?php if(!empty($this->session->flashdata('msg'))): ?>
                        <h5><?php echo $this->session->flashdata('msg'); ?></h5>
                    <?php endif; ?>
                </center>
            </div>
        </form>
    </div>  
</div>  
<!--/Form with header-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script>
    function myFunction() 
    {
        var x = document.getElementById("form4");

        if (x.type === "password") 
        {
            x.type = "text";
        } 
        else 
        {
            x.type = "password";
        }
    }
</script>