<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Society</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="logcs.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        .form-gap {
            padding-top: 70px;
        }
    </style>
</head>
<body style="background-image: url($assets_url.'img/login.jpg'); background-size:cover;">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="form-gap"></div>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                        <h2 class="text-center">Enter OTP</h2>
                        <p>You can reset your password here.</p>
                        <div class="panel-body">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                            <form id="register-form" role="form" autocomplete="off" class="form" action="" method="post">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="Otp" name="Otp" type="text" value="" class="form-control input-md" required>
                                    </div>
                                    <font color="red">Time remaining  <div id="worked">03:00</div></font>
                                </div>
                                <div class="form-group">
                                    <input name="onetime_pass" class="btn btn-lg btn-primary btn-block" value="Send" type="submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function (e) {
    var $worked = $("#worked");

    function update() {
        var myTime = $worked.html();
        var ss = myTime.split(":");
        var dt = new Date();
        dt.setHours(0);
        dt.setMinutes(ss[0]);
        dt.setSeconds(ss[1]);

        var dt2 = new Date(dt.valueOf() - 1000);
       
        var temp = dt2.toTimeString().split(" ");
        var ts = temp[0].split(":");
         if(ts[1]==59){
            console.log("show");
            $worked.html("00:00 <br><a href='<?php echo base_url()."auth/resend_otp"; ?>'>Resend OTP<a/> ");
        }
        if(ts[1]>=0 && ts[1]!=59){
        $worked.html(ts[1]+":"+ts[2]);
        }
       
        setTimeout(update, 1000);
      
    }

    setTimeout(update, 1000);
});

</script>
