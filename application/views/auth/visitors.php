<style type="text/css">
    /*.class9{
    background: linear-gradient(60deg, #4DB6AC, #8BC34A); color: #fff;
    box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }*/
    .class666
    {
        background: linear-gradient(60deg, #004D40, #33691E); color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    th,thead,tr,td,tbody
    {
        width: 10%;
        border: 1px solid;
    }
</style>

<div class="row" style="padding-left: 20px;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
                <h4 class="title">Visitors</h4>
            </div>
            <div class="card-content table-responsive">
                <table class="table table-bordered" id="table_id">
                    <thead>
                        <th>sr no.</th>
                        <th>MEmber Id</th>
                        <th>Member Name</th>
                        <th>visitor name</th>
                        <th>no.perons </th>
                        <th>Date</th>
                        <th>time</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                    <?php 
                        $sr_no=1;
                        if(!empty($visitorsArr))
                        {
                            foreach($visitorsArr AS $e_vstr)
                            {
                    ?>
                        <tr>
                            <td><?php echo $sr_no++; ?></td>
                            <td><?php echo $e_vstr['member_id']; ?></td>
                            <td><?php echo $e_vstr['s-r-fname']." ".$e_vstr['s-r-lname']; ?></td>
                            <td><?php echo $e_vstr['name']; ?></td>
                            <td><?php echo $e_vstr['no_person']; ?></td>
                            <td><?php echo date("d-m-Y",strtotime($e_vstr['date_visit'])); ?></td>
                            <td><?php echo date("h:i:s a",strtotime($e_vstr['from_time'])); ?></td>
                            <td><?php echo $e_vstr['reason']; ?></td>
                            <td><?php echo $e_vstr['status']; ?></td>
                        </tr>
                    <?php 
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>