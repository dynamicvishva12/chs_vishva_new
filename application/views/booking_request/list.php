		<div class="row">

			<div class="col-12">

				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
								<thead>
									<tr>
										<th>Sr. No</th>
										<th>Action</th>
										<th>Name</th>
										<th>Booking Place</th>
										<th>From Date</th>
										<th>To Date</th>
										<th>Booking Time</th>
										<th>Reject Request</th>
										<th>Cancellation Reason</th>
									</tr>
								</thead>
								<tbody>
									<?php
										if(!empty($booking_data)):
										$srno=1;
										foreach ($booking_data as $key => $booking):
									?>
									<tr>
										<td><?php echo $srno; ?></td>
										<td>
											<?php if($booking['bookStatus']=="Not Approved"): ?>

												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success confirm_it" data-toggle="tooltip" data-original-title="Approve" data-link="<?php echo base_url()."booking_request/change_booking_status"; ?>?id=<?php echo $booking['bookingId']; ?>&url=<?php echo base_url()."booking_request/change_booking_status"; ?>&status=Approved&bookusr=<?php echo $booking['bookingusr']; ?>" data-conf="Do you want to approve ?" data-accept="Approved" data-btn_text="Yes, approve it" data-accept_res="Your request will be approve shortly..." data-cancel_res="Your request has been cancelled...">
													<i class="fa fa-check"></i>
												</button>

											<?php elseif($booking['bookStatus']=="Pending Cancel"): ?>

												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success confirm_it" data-toggle="tooltip" data-original-title="Cancel" data-link="<?php base_url()."booking_request/change_booking_status"; ?>?id=<?php echo $booking['bookingId']; ?>&url=<?php echo base_url()."booking_request/change_booking_status"; ?>&status=Cancel&bookusr=<?php echo $booking['bookingusr']; ?>" data-conf="Do you want to cancel ?" data-accept="Cancelled" data-btn_text="Yes, cancel it" data-accept_res="Your request will be cancel shortly..." data-cancel_res="Your request has been cancelled...">
													<i class="fa fa-check"></i>
												</button>

											<?php else: ?>

												<?php echo $booking['bookStatus']; ?>

											<?php endif; ?>

											<?php if($booking['bookRejectReason']=="" && $booking['bookStatus']=="Not Approved"): ?>

												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger" data-toggle="modal" data-target="#userModal_<?php echo $srno; ?>">
													<i class="fa fa-close"></i>
												</button>

												<div id="userModal_<?php echo $srno; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<form class="form-horizontal" method="post" action="<?php echo base_url()."booking_request/reject_booking"; ?>">
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title" id="myModalLabel">Change Status</h4>
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																</div>
																<div class="modal-body">
																	<div class="form-group">
																		<label class="col-md-12">Reason</label>
																		<div class="col-md-12">
																			<textarea class="form-control" rows="4" id="textarea" name="reason" placeholder=""></textarea>
																		</div>
																	</div>
																</div>
																<div class="modal-footer text-right">
																	<input type="hidden" value="<?php echo $booking['bookingId']; ?>" name="id">
																	<input type="hidden" value="Rejected" name="status">
																	<input type="hidden" value="<?php echo $booking['bookingusr']; ?>" name="bookusr">
																	<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
																	<button type="submit" class="btn btn-info">Yes</button>
																</div>
															</div>
															<!-- /.modal-content -->
														</form>
													</div>
													<!-- /.modal-dialog -->
												</div>
											  <!-- /.modal -->

											<?php endif; ?>
								 		</td>
										<td><?php echo ucwords($booking['fullName']); ?></td>
										<td><?php echo $booking['booking_name']; ?></td>
										<td><?php echo $booking['bookFromDate']; ?></td>
										<td><?php echo $booking['bookToDate']; ?></td>
										<td><?php echo $booking['bookFromtime']." TO ".$booking['bookTotime']; ?></td>
										<td>
											<?php
												if($booking['bookStatus']=="Rejected")
												{
													echo $booking['bookRejectReason'];
												}
											?>
										</td>
										<td><?php echo $booking['cancel_reqest']; ?></td>
									</tr>
									<?php 
										$srno++; 
										endforeach;
									?>
									<?php else: ?>
		                                 <tr>
		                                    <td colspan=9><center>No Records</center></td>
		                                </tr>
		                            <?php endif; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>Sr. No</th>
										<th>Action</th>
										<th>Name</th>
										<th>Booking Place</th>
										<th>From Date</th>
										<th>To Date</th>
										<th>Booking Time</th>
										<th>Reject Request</th>
										<th>Cancellation Reason</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->      
			</div>
			<!-- /.col -->
		</div>