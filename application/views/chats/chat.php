		  <div class="box-header px-0">
			<div class="media align-items-center p-0">
			  <a class="avatar avatar-lg status-success mx-0" href="#">
				<!-- <img src="<?php //echo $assets_url; ?>tmp_images/avatar/2.jpg" class="rounded-circle" alt="..."> -->
				<?php if($profile==""): ?>
					<img alt="Profile" src="<?php echo $assets_url; ?>tmp_images/avatar/1.jpg" class="rounded-circle" alt="...">
				<?php else: ?>
					<img alt="Profile" src="<?php echo $assets_url; ?>IMAGESDRIVE/<?php echo $profile; ?>" class="rounded-circle" alt="">
				<?php endif; ?>
			  </a>
			  <div class="media-body">
				<p class="font-size-16">
				  <a class="hover-primary" href="#"><strong><?php echo ucwords($chatuname); ?></strong></a>
				</p>
				  <?php
				  	$_user_fname = $this->session->userdata('_user_fname');
                    $_user_lname = $this->session->userdata('_user_lname');
                    $_user_profile = $this->session->userdata('_user_profile');
				  	$getUserActiveTime="N/A";
				  	if(!empty($userData))
				  	{
				  		$lastMsgTimeArr=$userData['createdDatetime'];
					  	$lastMsgTime=$lastMsgTimeArr;

						$getUserActiveTime=$this->Mfunctional->timeAgo($lastMsgTime);
				  	}
				  	
				  ?>
				  <span><?php echo $getUserActiveTime; ?></span>
			  </div>
			</div>             
		  </div>

		  <div class="box-body px-0">
			  <div class="chat-box-one">
			  	<?php
			  		$lastChatDate="";
                    if(!empty($chat_arr)):
                        foreach($chat_arr AS $row):

                        	$currChatDate=strtotime($row['createdDatetime']);
                ?>
                	<div class="clearfix"></div>
                	<?php if($currChatDate!=$lastChatDate): ?>
						<p class="mb-0 text-semi-muted text-center">
							<?php 	
								if($lastChatDate!="")
									echo date('d-M-Y', strtotime($row['createdDatetime'])); 
								else
									echo "N/A";
							?>
						</p>
				<?php endif; ?>
                <?php if($row['s-admin'] == $user_name): ?>
				  	
					<div class="card d-inline-block mb-3 float-right mr-2 bg-primary">
						<div class="position-absolute pt-1 pr-2 r-0">
							<span class="text-extra-small"><?php echo date('h:i A', strtotime($row['createdDatetime'])); ?></span>
						</div>
						<div class="card-body">
							<div class="d-flex flex-row pb-2">
								<a class="d-flex" href="#">
									<img alt="Profile" src="<?php echo $_user_profile; ?>" class="avatar mr-10">
								</a>
								<div class="d-flex flex-grow-1 min-width-zero">
									<div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between">
										<div class="min-width-zero">
											<p class="mb-0 font-size-16"><?php echo ucwords($_user_fname." ".$_user_lname); ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="chat-text-left pl-55">
								<p class="mb-0 text-semi-muted"><?php echo $row['user-message']; ?></p>
							</div>
						</div>
					</div>

				  	<?php else: ?>

					<div class="card d-inline-block mb-3 float-left mr-2">
						<div class="position-absolute pt-1 pr-2 r-0">
							<span class="text-extra-small text-muted"><?php echo date('h:i A', strtotime($row['createdDatetime'])); ?></span>
						</div>
						<div class="card-body">
							<div class="d-flex flex-row pb-2">
								<a class="d-flex" href="#">
									<?php if($row['s-r-profile']==""): ?>
										<img alt="Profile" src="<?php echo $assets_url; ?>tmp_images/avatar/1.jpg" class="avatar mr-10">
									<?php else: ?>
										<img alt="Profile" src="<?php echo $assets_url; ?>IMAGESDRIVE/<?php echo $row['s-r-profile']; ?>" class="avatar mr-10">
									<?php endif; ?>
								</a>
								<div class="d-flex flex-grow-1 min-width-zero">
									<div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between">
										<div class="min-width-zero">
											<p class="mb-0 font-size-16 text-dark"><?php echo ucwords($chatuname); ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="chat-text-left pl-55">
								<p class="mb-0 text-semi-muted"><?php echo $row['user-message']; ?></p>
							</div>
						</div>
					</div>
				<?php 
							$lastChatDate=$currChatDate;
							endif; 
		                 endforeach; 
		        ?>
		        <?php else: ?>
		        	<div class="clearfix"></div>
					<div class="card d-inline-block mb-3 float-left mr-2">
						<p class="mb-0 text-semi-muted text-center">No Chats</p>
					</div>
		        <?php endif; ?>
				 
			  </div>
		  </div>
		</div>
		<div class="box box-body">
			<div class="d-flex justify-content-between align-items-center">
				<input class="form-control b-0 py-10" type="text" id="send-message" placeholder="Say something...">
				<div class="d-flex justify-content-between align-items-center">
					
					<button type="button" class="waves-effect waves-circle btn btn-circle btn-primary" id="show1">
						<i class="mdi mdi-send"></i>
					</button>
				</div>
			</div>
		</div>

		<script>
	        $("#show1").click(function(event){

	            event.preventDefault();
	            console.log('working');
	            var base_url = "<?php echo base_url(); ?>";
	            var showuser = $('#send-message').val();
	            var id = <?php echo $chatid; ?>;
	            var admin ='<?php echo $user_name; ?>';
	            var chatuname ='<?php echo $chatuname; ?>';
	            console.log(admin);

	            // $.post("admin-user3",
	            $.post(base_url+"chats/send_message",
	            {
	                show : showuser,
	                admin : admin,
	                id : id
	            },
	            function(data){
	                //$('#result-show').html(data);
	                window.location.href=base_url+"chats/home";
	            });

	        });
	    </script>