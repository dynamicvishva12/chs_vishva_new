<div class="row">
	<div class="col-lg-9 col-md-7 col-12">
		<div class="box box-transparent no-border chats_div" style="height: 75vh; overflow-y: scroll; overflow-x: hidden;">
		  	<div class="clearfix"></div>
			<div class="card d-inline-block mb-3 float-left mr-2">
				<p class="mb-0 text-semi-muted text-center">No Chats</p>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-5 col-12">
		<div class="box">
			<div class="box-body">
				<div class="lookup lookup-sm lookup-right d-none d-lg-block my-20">
					<input type="text" name="s" placeholder="Search" class="w-p100">
			    </div>
				<div class="chat-box-one-side">
					<div class="media-list media-list-hover">
						<?php 
							$x=1;
							if(!empty($chat_arr)):
								foreach($chat_arr AS $chatnot):
						?>
						<div class="media py-10 px-0 chat_list" data-user_id="<?php echo $chatnot['user-keyvalue']; ?>" data-user_name="<?php echo $chatnot['s-r-fname']." ".$chatnot['s-r-lname']; ?>" data-profile="<?php echo $chatnot['s-r-profile']; ?>" data-user="<?php echo $chatnot['s-r-username']; ?>">
						  	<a class="avatar avatar-lg status-success" href="#">
								<img src="<?php echo $assets_url; ?>IMAGESDRIVE/<?php echo $chatnot['s-r-profile']; ?>" alt="...">
						  	</a>
						  	<div class="media-body">
								<p class="font-size-16">
								  <a class="hover-primary" href="#">
								  	<strong><?php echo ucwords($chatnot['s-r-fname']." ".$chatnot['s-r-lname']); ?></strong>
								  	<small>(<?php echo $chatnot['s-admin']; ?>)</small>
								  </a>
								</p>
								<p><?php echo $chatnot['user-message']; ?></p>
								<?php
									$lastMsgTime=$chatnot['createdDatetime'];

									$getUserActiveTime=$this->Mfunctional->timeAgo($lastMsgTime);
								?>
								<span><?php echo $getUserActiveTime; ?></span>
						  	</div>
						</div>
						<?php 
								$x++;
								endforeach;
							else:
						?>   
						<div class="media py-10 px-0">
							<center>No chats</center>
						</div>
						<?php endif; ?>  
					  </div>
				</div>
			</div>
			<!-- /.box-body -->
		  </div>
	</div>
</div>

<script type="text/javascript">

	var base_url = "<?php echo $base_url; ?>";

	$(document).ready(function(){

		$('body').on('click', '.chat_list', function(){

			var user_Id = $(this).data('user_id');
			var user_name = $(this).data('user_name');
			var profile = $(this).data('profile');
			var user = $(this).data('user');

			$.ajax({
		        url : base_url+'chats/chat',
		        type : 'POST',
		        data : { 'user_Id' : user_Id, 'user_name' : user_name, 'profile' : profile, 'user':user },
		        dataType: 'html',
		        success : function(data) {     

		          $('.chats_div').html(data);

		        },
		        error : function(request, error)
		        {
		            alert("Request: "+JSON.stringify(request));
		        }
		    });

		});

	});

</script>