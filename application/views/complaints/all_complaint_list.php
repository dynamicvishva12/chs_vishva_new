<div class="row">
				
				<!-- /.col -->
				<div class="col-xl-6 col-lg-8 col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">All Complaints</h4>
						<div class="box-controls pull-right">
						<div class="box-header-actions">
						  <div class="lookup lookup-sm lookup-right d-none d-lg-block">
							<input type="text" name="s" placeholder="Search">
						  </div>
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <div class="mailbox-controls">
						<!-- Check all button -->
						 
						<!-- /.btn-group -->
						<button type="button" class="btn btn-primary btn-sm self_refresh"><i class="fa fa-refresh"></i></button>
						<div class="pull-right">
						  <div class="btn-group">
							<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-left"></i></button>
							<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-right"></i></button>
						  </div>
						  <!-- /.btn-group -->
						</div>
						<!-- /.pull-right -->
					  </div>
					  <div class="mailbox-messages inbox-bx">
						  <div class="table-responsive">
							<table class="table table-hover table-striped">
								<thead>
									<th></th>
									<th>Action</th>
									<th>Subject</th>
									<!-- <th>Details</th> -->
									<th>Status</th>
									<th>Date</th>
								</thead>
								<tbody>
									<?php 
										if(!empty($complain_data)):
											foreach ($complain_data as $key => $compain):
									?>
									<tr>
										<td class="mailbox-star">
											<a href="#"><i class="fa fa-star-o text-yellow"></i></a>
										</td>
										<td>
											<a href="<?php echo base_url()."complaints/all_complaint_list/".$usrId; ?>?c_id=<?php echo $compain['compId']; ?>&&c_name=<?php echo $compain['compSubjet']; ?>">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
											</a>
										</td>
										<td>
											<p class="mailbox-name mb-0 font-size-16 font-weight-600">
												<?php echo $compain['compSubjet']; ?>
											</p>
										</td>
										<!-- <td>
											<a class="mailbox-subject" href="#">
												<?php //echo $compain['compDscr']; ?>
											</a>
										</td> -->
										<td><?php echo ucfirst($compain['compStatus'])	; ?></td>
										<td class="mailbox-date"><?php echo date('d M Y H:i:s A', strtotime($compain['compTime'])); ?></td>
									</tr>
										<?php endforeach; ?>
									<?php endif; ?>
								</tbody>
							</table>
						  </div>                
						<!-- /.table -->
					  </div>
					  <!-- /.mail-box-messages -->
					</div>
					<!-- /.box-body -->
				  </div>
				  <!-- /. box -->
				</div>
				<!-- /.col -->
				<div class="col-xl-6 col-lg-8 col-12">
				  <div class="box">
					<div class="box-body pt-10">
					  <div class="mailbox-read-info">
						<h4>Complaint Subject - <?php echo $this->session->userdata('complainname'); ?></h4>
					  </div>
					  <div class="mailbox-read-info clearfix mb-20">
						<div class="float-left mr-10"><a href="#"><img src="../images/1.jpg" alt="user" width="40" class="rounded-circle"></a></div>
						<h5 class="no-margin">
							<?php
								if(isset($complain_data[0]['fullName']))
									echo $complain_data[0]['fullName'];
								else
									"N/A";
							?>
							<br>
							 <small>From: 
							 	<?php
									if(isset($complain_data[0]['email_id']))
										echo $complain_data[0]['email_id'];
									else
										"N/A";
								?>
							 </small>
						  <span class="mailbox-read-time pull-right">22 JUL. 2019 08:03 PM</span></h5>
					  </div>
					  <!-- /.mailbox-read-info -->
					  
					  <!-- /.mailbox-controls -->
					  <div class="mailbox-read-message read-mail-bx">
					  	<?php
		                    if(!empty($complain_chat_data)):
		                        foreach($complain_chat_data as $key => $cchat):
		                            if($cchat['CompUserId']==$this->session->userdata('_user_name')):
		                ?>
		                		<p class="text-right text-success"><?php echo $cchat['compDscr']; ?> <br><small class="text-right"><?php echo $cchat['compTime']; ?></small></p>

		                        <?php else: ?>

		                        <p class="text-left text-secondary"><?php echo $cchat['compDscr']; ?> <br><small class="text-right"><?php echo $cchat['compTime']; ?></small></p>
		                <?php 
		                            endif;
		                        endforeach;
		                    endif;
		                ?>
					  </div>
					  <!-- /.mailbox-read-message -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
					<?php if(!empty($complain_chat_data)): ?>
						<?php if($comp_data==1): ?>
							<form action="<?php echo base_url()."complaints/send_complaint/".$usrId; ?>" method="post">
								<div class="row">
								 	<div class="col-md-9">
								 		<textarea type="text" name="chatter_message" class="form-control" placeholder="Type message to reply..."></textarea>
								 	</div>
								 	<div class="col-md-3">
								 		<button type="submit" class="btn btn-success"><i class="fa fa-reply"></i> Reply</button>
								 	</div>
								</div>
							</form>
						<?php endif; ?>
					<?php endif; ?>
					</div>
					<!-- /.box-footer -->
				  </div>
				  <!-- /. box -->
				</div>
				<!-- /.col -->
			  </div>