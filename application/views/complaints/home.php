			<div class="row">
				<!-- /.col -->
				<div class="col-xl-12 col-lg-12 col-12">
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">Complaints</h4>
						<div class="box-controls pull-right">
						<div class="box-header-actions">
						  <div class="lookup lookup-sm lookup-right d-none d-lg-block">
							<input type="text" name="s" placeholder="Search">
						  </div>
						</div>
					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <div class="mailbox-controls">
						<!-- Check all button -->
						 
						<!-- /.btn-group -->
						<button type="button" class="btn btn-primary btn-sm self_refresh"><i class="fa fa-refresh"></i></button>
						<div class="pull-right">
						  <div class="btn-group">
							<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-left"></i></button>
							<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-chevron-right"></i></button>
						  </div>
						  <!-- /.btn-group -->
						</div>
						<!-- /.pull-right -->
					  </div>
					  <div class="mailbox-messages inbox-bx">
						  <div class="table-responsive">
							<table class="table table-hover table-striped">
							  <tbody>
							  	<?php 
									$srno=1;
									if(!empty($complain_data)):
										foreach($complain_data as $key => $cuser):
								?>
								 <tr>
									<td><input type="checkbox"></td>
									<td class="mailbox-star">
										<a href="#"><i class="fa fa-star text-yellow"></i></a>
									</td>
									<td>
										<p class="mailbox-name mb-0 font-size-16 font-weight-600">
											<?php echo ucfirst($cuser['fullName']); ?> (<?php echo $cuser['compUsr']; ?>)
										</p>
										<a class="mailbox-subject" href="#">
											<b>No.of complaints: </b><?php echo $cuser['noComp'];  ?>
										</a>
									</td>
									<td class="mailbox-attachment"></td>
									<td class="mailbox-date">
										<a href="<?php echo base_url()."complaints/all_complaint_list/".$cuser['compUsr']; ?>">
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
										</a>										
									</td>
								</tr>
									<?php endforeach; ?>
								<?php endif; ?>
							  </tbody>
							</table>
						  </div>                
						<!-- /.table -->
					  </div>
					  <!-- /.mail-box-messages -->
					</div>
					<!-- /.box-body -->
				  </div>
				  <!-- /. box -->
				</div>
				<!-- /.col -->
			  </div>