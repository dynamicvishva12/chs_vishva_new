<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-2 control-label">
											<strong >Event Title</strong>
										</label>
										<div class="col-md-10">                     
											<input type="text" name="name" class="form-control" placeholder="Event Title" value="<?php echo $event_data['eventTitle']; ?>">
										</div>
									</div>
									<!-- Text input-->
									<div class="form-group row">
										<label class="col-md-2 control-label">
											<strong >Event Description</strong>
										</label>  
										<div class="col-md-10">
											<textarea type="text" name="ev_Description"  rows="8" class="form-control" placeholder=""><?php echo $event_data['eventDesc']; ?></textarea>
										</div>
									</div>
									<!-- Textarea -->
									<div class="form-group row">
										<label class="col-md-2 control-label">
											<strong>Event Date:</strong>
										</label>
										<div class="col-md-10">
											<input type="date" name="ev_Date" class="form-control" placeholder="Event Date" value="<?php echo $event_data['eventDate']; ?>" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-2"></div>
										<div class="col-md-5 callout callout-warning">
											<h4><i class="icon fa fa-info"></i> Note:</h4>
											<ul>
												<li>File size must be 5MB or less.</li>
												<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
											</ul>
										</div>  
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textarea">
											<strong>Upload Document:</strong>
										</label>
										<div class="col-md-5"> 
											<input id="oldfile" name="myfile"  class="input-file" type="file" onclick="switchVisible();">
											<?php 
												// echo $notice_data['noticeFilename']; 
												$temp = explode('/', $event_data['eventFilename']);
												$getId = end($temp);
											?>
											<a id="nameold" href="<?php echo $event_data['eventFilename']; ?>" target="_blank"><?php echo $getId; ?></a>
											<a href="#" id="removebtn" class="btn btn-danger confirmations" onclick="switchVisible1();">
												<i class="material-icons">Clear</i>
											</a>
					                        <input id="nameold" name="nameold" class=" input-file" value="<?php echo $getId; ?>" type="hidden">
					                        <input id="image_status" name="image_status" class=" input-file" value="<?php echo "unchanged"; ?>" type="hidden">
					                        <input id="nofiletype" name="nofiletype" class=" input-file" value="<?php echo $event_data['eventFileType']; ?>" type="hidden">

										<!-- Button -->
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 text-right">
											<input type="hidden" name="eventId" id="meetId" value="<?php echo $event_data['eventId']; ?>" required>
											<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                            				<input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
											<button type="submit" class="btn btn-success text-center" >Submit</button>
										</div>
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>


    <script>

        function switchVisible() 
        {
            if (document.getElementById('oldfile')) 
            {
                document.getElementById("image_status").value="changed";
                document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
            }
        }

        function switchVisible1() 
        {
            if (document.getElementById('removebtn')) 
            {
                document.getElementById("image_status").value="changed";
                document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
            }
        }

    </script>
