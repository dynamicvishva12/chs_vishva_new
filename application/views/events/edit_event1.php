<style type="text/css">
  .class7
  {
    background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
    box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
  }
  .class111
  {
    background:#212121; color: #fff;
    box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
  }
  .mdl-button:hover
  {
    background-color: #212121;
    color: #fff;
  }
</style>


    <div class="row">
        <div class="col-md-12">
            <div id="meet-result">Edit Event</div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="eventId" id="meetId" value="<?php echo $event_data['eventId']; ?>" required>

                    <input type="text" style="width: 100%; height: 40px;" name="name" placeholder="Event Title" value="<?php echo $event_data['eventTitle']; ?>" required><br><br>

                    <input type="text" style="width: 100%; height: 40px;" placeholder="Event Description" name="ev_Description" value="<?php echo $event_data['eventDesc']; ?>" required><br><br>

                    <input type="date" style="width: 100%; height: 40px;" name="ev_Date" placeholder="Event Date" value="<?php echo $event_data['eventDate']; ?>" required><br><br>

                    <center>  
                        <input id="oldfile" name="myfile"  class="input-file" type="file" onclick="switchVisible();">
                        <?php 
                            $temp = explode('/', $event_data['eventFilename']);
                            $getId = end($temp);
                        ?>
                        <a id="nameold" href="<?php echo $event_data['eventFilename']; ?>"><?php echo $getId; ?></a>
                        <a href="#" id="removebtn" class="btn btn-danger confirmation" style=" width: 30px;
                        height: 30px; text-align: center; padding: 6px 0; font-size: 12px; line-height: 1.42; border-radius: 15px;margin-bottom: 17px;" onclick="switchVisible1();">
                            <i class="material-icons">clear</i>
                        </a>
                        <input id="nameold" name="nameold" class=" input-file" value="<?php echo $getId; ?>" type="hidden">
                        <input id="image_status" name="image_status" class=" input-file" value="<?php echo "unchanged"; ?>" type="hidden">
                        <input id="nofiletype" name="nofiletype" class=" input-file" value="<?php echo $event_data['eventFileType']; ?>" type="hidden">
                    </center>
                    <center>
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="singlebutton"></label>
                            <div class="col-md-12">
                                <input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                                <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
                                <button type="submit" id="singlebutton" name="editevent" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  style="background: #212121; color: #fff; ">post</button>
                            </div>
                        </div>
                    </center>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <a href="<?php echo base_url()."events/list"; ?>">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored confirmation" style="position: fixed;top: 85%; right: 5%; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
                <i class="material-icons">undo</i>
            </button>
        </a>
    </div>


    <script type="text/javascript">
        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {

            if (!confirm('Are you sure?')) 
                e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {

            elems[i].addEventListener('click', confirmIt, false);
        }
    </script>
    <script>

        function switchVisible() 
        {
            if (document.getElementById('oldfile')) 
            {
                document.getElementById("image_status").value="changed";
                document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
            }
        }

        function switchVisible1() 
        {
            if (document.getElementById('removebtn')) 
            {
                document.getElementById("image_status").value="changed";
                document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
            }
        }

    </script>