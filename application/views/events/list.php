<!-- tabs -->

		  <div class="row">

			<div class="col-12">

				 <div class="box">
					<div class="box-header with-border">
				 		<div class="row">
							<div class="col-10">
					  			<h3 class="box-title">Events</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addEvents">Add Event</button>
					  		</div>
						</div>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
						  	<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
								<thead>
									<tr>
										<th>Sr. No.</th>
										<th>Action</th>
										<th>Event Title</th>
										<th>Event Description</th>
										<th>Event Date</th>
										<th>Event Image</th>
									</tr>
								</thead>
								<tbody>
									<?php 
		                                if(!empty($event_data)):
		                                    $srno=1;
		                                    foreach($event_data as $key => $get_value):
		                            ?>
									<tr>
										<td><?php echo $srno++; ?></td>
	                                    <td>
	                                    	<center>
		                                        <a href="<?php echo base_url().'events/edit_event/'.$get_value['eventId']; ?>">
		                                        	<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
		                                        </a>

												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it"  data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url().'events/delete_event/'.$get_value['eventId']; ?>">
													<i class="fa fa-trash-o"></i>
												</button>
											</center>
	                                    </td>
	                                    <td><?php echo $get_value['eventTitle']; ?></td>
	                                    <td><?php echo $get_value['eventDesc']; ?></td>
	                                    <td><?php echo date("d-m-Y",strtotime($get_value['eventDate'])); ?></td>
	                                    <td class="center">
											<?php if(!empty($get_value['eventFilename'])): ?>
												<?php 
													$explodedArr=explode('/', $get_value['eventFilename']);
													$fileAtmntName=end($explodedArr);
												?>
												<a href="<?php echo $get_value['eventFilename']; ?>" target="_blank"><?php echo $fileAtmntName; ?></a>
											<?php else: ?>
												N/A
											<?php endif; ?>
										</td>
									</tr>
									    <?php endforeach; ?>
		                            <?php else: ?>
		                                 <tr>
		                                    <td colspan=6><center>No Records</center></td>
		                                </tr>
		                            <?php endif; ?>
								</tbody>
								<tfoot>
									<tr>
										<th>Sr. No.</th>
										<th>Action</th>
										<th>Event Title</th>
										<th>Event Description</th>
										<th>Event Date</th>
										<th>Event Image</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
		<!-- END tabs -->


<div id="addEvents" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url().'events/add_event'; ?>" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Events</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label class="col-md-12">Event Name</label>
						<div class="col-md-12">
							<input type="text" name="name" class="form-control" placeholder="Event Name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Event Description</label>
						<div class="col-md-12">
							<textarea type="text" name="ev_Description"  rows="8" class="form-control" placeholder=""></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Event Date</label>
						<div class="col-md-12">
							<input type="date" name="ev_Date" class="form-control" placeholder="Event Date">
						</div>
					</div>
					<div class="callout callout-warning">
						<h4><i class="icon fa fa-info"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
				  	<div class="form-group">
				  		<label class="col-md-12">File</label>
						<div class="col-md-12">
							<input type="file" name="myfile" id="file">
						</div>
				  	</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                    <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>