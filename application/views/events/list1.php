<style>
    .class7
    {
        background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    .class111
    {
        background:#212121; color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    .mdl-button:hover
    {
        background-color: #212121;
        color: #fff;
    }
</style>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 


    <div id="id05" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="width: 500px;">
                <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
                    <a href="#" class="closebtn">×</a>
                    <center><h2 style="margin-top: 5px;">Events</h2></center>
                </header>
                <div class="container-fluid rosh" style="padding-top: 20px;">
                    <form method="post" action="<?php echo base_url().'events/add_event'; ?>" enctype="multipart/form-data">

                        <input type="text" style="width: 100%; height: 40px;" name="name" placeholder="Event Title" required><br><br>
                        <input type="text" style="width: 100%; height: 40px;" placeholder="Event Description" name="ev_Description" required><br>
                        <br>
                        <input type="date" style="width: 100%; height: 40px;" name="ev_Date" placeholder="Event Date" required><br><br>
                        <input type="file" style="width: 100%; height: 40px;" name="myfile"><br><br>

                        <center>
                            <input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                            <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
                            <input type="submit" class=" mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " style="width: 200px; background:#212121; color: #fff;" name="eventadd" value="SUBMIT">
                        </center>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <center>
        <div id="id30" class="modal-rosh">
            <div class="modal-dialog-rosh" style="width: 500px;">
                <div class="modal-content-rosh" style="margin-top: 100px;">
                    <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px; padding-top: 0px; padding-bottom: 10px;"> 
                        <button  style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>        
                        <center>
                            <h2 style="margin-top: 0px; font-size: 20px;">Confirm Delete</h2>
                        </center>
                    </header>
                    <div class="container-fluid rosh roshan" style="padding-top: 20px; padding-bottom: 20px;"></div>
                </div>
            </div>
        </div>
    </center>


    <div class="row" style="padding-left: 10px;">
        <div class="col-md-6">
            <a href="<?php echo base_url()."events/list"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class111" style="width: 100%;">
                    Events
                </button>
            </a>
        </div>
        <div class="col-md-6">
            <a href="<?php echo base_url()."albums/list"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect " style="width: 100%;">
                    Society Album
                </button>
            </a>
        </div>
    </div><br>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
                        <h4 class="title">Events</h4>
                    </div>
                    <div class="card-content table-responsive">
                        <table id="table_id" class="display">
                            <thead class="text-primary">
                                <th>Sr.No</th>
                                <th>Event Title</th>
                                <th>Event Description</th>
                                <th>Event Date</th>
                                <th>Event Image</th>
                                <th colspan="2">Action</th>
                            </thead>
                            <tbody>
                            <?php 
                                if(!empty($event_data)):
                                    $srno=1;
                                    foreach($event_data as $key => $get_value):
                            ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $srno++; ?></td>
                                    <td><?php echo $get_value['eventTitle']; ?></td>
                                    <td><?php echo $get_value['eventDesc']; ?></td>
                                    <td><?php echo date("d-m-Y",strtotime($get_value['eventDate'])); ?></td>
                                    <td class="center">

                                        <?php if($get_value['eventFileType']=="pdf"): ?>

                                            <iframe class="mdl-card__title mdl-card--expand" src="<?php echo $get_value['eventFilename']; ?>" style="width: 100%;height: 100%;border: none;" download=""></iframe> 

                                        <?php  elseif($get_value['eventFileType']=="image"): ?>

                                            <img class="mdl-card__title mdl-card--expand" src="<?php echo $get_value['eventFilename']; ?>" style="height: 67%; download=">

                                        <?php else: ?>

                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url().'events/edit_event/'.$get_value['eventId']; ?>" style="background: #212121; color: #fff;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent confirmation">
                                            Edit
                                        </a>
                                        <a href="<?php echo base_url().'events/delete_event/'.$get_value['eventId']; ?>" style="background: #212121; color: #fff;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent confirmation">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                 <tr>
                                    <td colspan=7><center>No Records</center></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a href="#id05">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored"  style="position: fixed;top: 85%; right: 5%;  background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
            <i class="material-icons">add</i>
        </button>
    </a>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">Add Event</h4></center>
                </div>
                <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data">

                        <input type="text" style="width: 100%;" name="name" placeholder="Event Title" pattern="[a-zA-Z\s]+" required><br><br>
                        <input type="text" style="width: 100%;" placeholder="Event Description" name="b-address" pattern="[a-zA-Z0-9,\s]+"><br>
                        <br>
                        <input type="date" style="width: 100%;" name="Date" placeholder="Event Date"><br><br>
                        <input type="time" style="width: 100%;" name="from" required><br><br>
                        <input type="file" style="width: 100%;" name="myfile"><br><br>

                        <center>
                            <input type="submit" class=" mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " style="width: 200px;" name="submit-b" value="SUBMIT">
                        </center>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">Delete Confirmation</h4></center>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>


    <script>

        $(document).ready(function(){
            $('.openPopup1').on('click',function(){

                var dataURL = $(this).attr('data-href');
                console.log(dataURL);
                $('.roshan').load(dataURL,function(){
                    $('#id30').modal({show:true});
                });

            });
        });

    </script>

    <script>
        function goBack() 
        {
            location.reload();
        }
    </script>

    <script type="text/javascript">

        var elems = document.getElementsByClassName('confirmation');
        var confirmIt = function (e) {
            if (!confirm('Are you sure?')) e.preventDefault();
        };
        for (var i = 0, l = elems.length; i < l; i++) {
            elems[i].addEventListener('click', confirmIt, false);
        }

    </script>