<?php include 'header.php'; ?>

<body class="hold-transition light-skin sidebar-mini theme-primary">
<div class="spinner-wrapper">
<div class="spinner"></div>
</div>
<div class="wrapper">

<?php include 'navbar.php'; ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'sidebar.php'; ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Expected Visitors</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Informative</li>
								<li class="breadcrumb-item" aria-current="page">Gatekeeper</li>
								<li class="breadcrumb-item active" aria-current="page">Expected Visitors</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div >

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			
				  		</div>
				  		<div class="col-3 text-right">
				  			<!-- <button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button> -->
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Gender</th>
								<th>Locality</th>
								<th>Total Person</th>
								<th>Contact No.</th>
								<th>Email</th>
								<th>Visit Type</th>
								<th>Purpose</th>
								<th>Vehicle No</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Gender</th>
								<th>Locality</th>
								<th>Total Person</th>
								<th>Contact No.</th>
								<th>Email</th>
								<th>Visit Type</th>
								<th>Purpose</th>
								<th>Vehicle No</th>
							</tr>
						</tfoot>
						<tbody>
							
							
						</tbody>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

   <!-- /.content-wrapper -->
  <?php include 'footer.php'; ?>
  
</div>
<!-- ./wrapper -->	 
	
<?php include 'scripts.php';?>
	
</body>
</html>