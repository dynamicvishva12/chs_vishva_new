<?php include 'header.php'; ?>

<body class="hold-transition light-skin sidebar-mini theme-primary">
<div class="spinner-wrapper">
<div class="spinner"></div>
</div>
<div class="wrapper">

<?php include 'navbar.php'; ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include 'sidebar.php'; ?>

	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Society Staff</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Informative</li>
								<li class="breadcrumb-item" aria-current="page">Gatekeeper</li>
								<li class="breadcrumb-item active" aria-current="page">Society Staff</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div >

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			
				  		</div>
				  		<div class="col-3 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Staff Type</th>
								<th>Contact No.</th>
								<th>Emergency Contact No.</th>
								<th>Image</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Staff Type</th>
								<th>Contact No.</th>
								<th>Emergency Contact No.</th>
								<th>Image</th>
							</tr>
						</tfoot>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									<center>
										<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
										<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger"  data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o" id="sa-params"></i></button>
									</center>
								</td>
								<td>Name</td>
								<td>Staff Type</td>
								<td>Contact No.</td>
								<td>Emergency Contact No.</td>
								<td>Image</td>
							</tr>
							
						</tbody>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

<!-- Popup Model Plase Here -->
<div id="addStaff" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Add Staff</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-md-12">Name</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Contact No.</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Contact No.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Emergency Contact No.</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Emergency Contact No.">
						</div>
					</div>
					<div class="form-group">
						<label>Select Type</label>
						<select class="form-control" style="width: 100%;">
						  <option selected="selected">abc</option>
						  <option>xyz</option>
						  <option>pqr</option>
						</select>
				  	</div>
					<div class="form-group">
						<label class="col-md-12">Staff Image</label>
						<div class="col-md-12">
							<input type="file" class="form-control" placeholder="">
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success float-right" id="sa-success" data-dismiss="modal">Add</button>
				<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

   <!-- /.content-wrapper -->
  <?php include 'footer.php'; ?>
  
</div>
<!-- ./wrapper -->	 
	
<?php include 'scripts.php';?>
	
</body>
</html>