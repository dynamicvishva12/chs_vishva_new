	<div class="row">
		<div class="col-12">
		  	<div class="box box-default">
		  		<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			<h3 class="box-title">Edit Society Staff</h3>
				  		</div>
				  		<div class="col-3 text-right">
				  			<!-- <button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button> -->
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="row">
						<div class="col-md-12">
							<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group row">
											<label class="col-md-2">Name</label>
											<div class="col-md-10">
												<input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $soc_staff_data['name']; ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-2">Contact No.</label>
											<div class="col-md-10">
												<input type="text" name="contact_no" class="form-control" placeholder="Contact No." value="<?php echo $soc_staff_data['mobile_no']; ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-2">Emergency Contact No.</label>
											<div class="col-md-10">
												<input type="text" name="emergency_contact_no" class="form-control" placeholder="Emergency Contact No." value="<?php echo $soc_staff_data['Emergency_contact']; ?>">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-md-2">Select Type</label>
											<div class="col-md-10">
												<select class="form-control" name="staff_type" style="width: 100%;">
													<option value="" >Select</option>
													<?php if(!empty($soc_staff_type_data)): ?>
														<?php foreach($soc_staff_type_data AS $e_s_typ): ?>
															<option value="<?php echo $e_s_typ['id']; ?>" <?php if($soc_staff_data['vendor_type']==$e_s_typ['id']): ?>selected<?php endif; ?> ><?php echo $e_s_typ['staff_type_name']; ?></option>
														<?php endforeach; ?>
													<?php endif; ?>
												</select>
											</div>
									  	</div>
									  	<div class="form-group row">
											<label class="col-md-2">Address</label>
											<div class="col-md-10">
												<textarea class="form-control" rows="3" id="textarea" name="address" placeholder="Address"><?php echo $soc_staff_data['address']; ?></textarea>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-md-2"></div>
											<div class="col-md-5 callout callout-warning">
												<h4><i class="icon fa fa-info"></i> Note:</h4>
												<ul>
													<li>File size must be 5MB or less.</li>
													<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
												</ul>
											</div>  
										</div>
										<div class="form-group row">
											<label class="col-md-2 control-label" for="textarea">
												<strong>Upload Document:</strong>
											</label>
											<div class="col-md-10"> 
												<input id="oldfile" name="staff_img"  class="input-file" type="file" onclick="switchVisible();">
												<?php 
													// echo $notice_data['noticeFilename']; 
													$temp = explode('/', $soc_staff_data['staff_pic']);
													$getId = end($temp);
												?>
												<a id="nameold" href="<?php echo $soc_staff_data['staff_pic']; ?>" target="_blank"><?php echo $getId; ?></a>
												<a href="#" id="removebtn" class="btn btn-danger" onclick="switchVisible1();">
													<i class="material-icons">Clear</i>
												</a>
						                        <input id="nameold" name="nameold" class=" input-file" value="<?php echo $getId; ?>" type="hidden">
						                        <input id="image_status" name="image_status" class=" input-file" value="unchanged" type="hidden">

											<!-- Button -->
											</div>
										</div>
										<div class="form-group row">
											<div class="col-md-12 text-right">
												<input type="hidden" name="staffId" value="<?php echo $staffId; ?>" >
												<button type="submit" class="btn btn-success text-center" >Submit</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>

	<script>

	    function switchVisible() 
	    {
	        if (document.getElementById('oldfile')) 
	        {
	            document.getElementById("image_status").value="changed";
	            document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
	        }
	    }

	    function switchVisible1() 
	    {
	        if (document.getElementById('removebtn')) 
	        {
	            document.getElementById("image_status").value="changed";
	            document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
	        }
	    }

	</script>