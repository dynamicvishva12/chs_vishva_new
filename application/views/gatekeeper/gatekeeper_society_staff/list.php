	<div class="row">
		<div class="col-12">
			<div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			<h3 class="box-title">Society Staff</h3>
				  		</div>
				  		<div class="col-3 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  	<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr. No</th>
									<th>Action</th>
									<th>Name</th>
									<th>Staff Type</th>
									<th>Contact No.</th>
									<th>Emergency Contact No.</th>
									<th>Image</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Sr. No</th>
									<th>Action</th>
									<th>Name</th>
									<th>Staff Type</th>
									<th>Contact No.</th>
									<th>Emergency Contact No.</th>
									<th>Image</th>
								</tr>
							</tfoot>
							<tbody>
								<?php $i=1; ?>
								<?php if(!empty($soc_staff_data)): ?>
									<?php foreach($soc_staff_data AS $e_stf): ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td>
											<center>
												<a href="<?php echo base_url()."gatekeeper/gatekeeper_society_staff/edit_staff/".$e_stf['staff_id']; ?>">
													<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit">
														<i class="fa fa-edit"></i>
													</button>
												</a>
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."gatekeeper/gatekeeper_society_staff/delete_staff/".$e_stf['staff_id']; ?>" data-toggle="tooltip" data-original-title="Delete">
													<i class="fa fa-trash-o" ></i>
												</button>
											</center>
										</td>
										<td><?php echo $e_stf['name']; ?></td>
										<td><?php echo $e_stf['staff_type_name']; ?></td>
										<td><?php echo $e_stf['mobile_no']; ?></td>
										<td><?php echo $e_stf['Emergency_contact']; ?></td>
										<td>
											<a href="<?php echo $e_stf['staff_pic']; ?>" target="_blank">
											<?php 
												$temp = explode('/', $e_stf['staff_pic']);
												$pic = end($temp);
											?>
												<?php echo $pic; ?>
											</a>
										</td>
									</tr>
									<?php $i++; ?>
								<?php endforeach; ?>
							<?php endif; ?>		
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->      
		</div>
		<!-- /.col -->
	</div>


<!-- Popup Model Plase Here -->
<div id="addStaff" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."gatekeeper/gatekeeper_society_staff/add_staff"; ?>" method="POST" enctype="multipart/form-data" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Staff</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Name</label>
						<div class="col-md-12">
							<input type="text" name="name" class="form-control" placeholder="Name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Contact No.</label>
						<div class="col-md-12">
							<input type="text" name="contact_no" class="form-control" placeholder="Contact No.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Emergency Contact No.</label>
						<div class="col-md-12">
							<input type="text" name="emergency_contact_no" class="form-control" placeholder="Emergency Contact No.">
						</div>
					</div>
					<div class="form-group">
						<label>Select Type</label>
						<select class="form-control" name="staff_type" style="width: 100%;">
							<option value="" >Select</option>
							<?php if(!empty($soc_staff_type_data)): ?>
								<?php foreach($soc_staff_type_data AS $e_s_typ): ?>
									<option value="<?php echo $e_s_typ['id']; ?>"><?php echo $e_s_typ['staff_type_name']; ?></option>
								<?php endforeach; ?>
							<?php endif; ?>
						</select>
				  	</div>
				  	<div class="form-group">
						<label class="col-md-12">Address</label>
						<div class="col-md-12">
							<textarea class="form-control" rows="3" id="textarea" name="address" placeholder="Address"></textarea>
						</div>
					</div>
					<div class="form-group callout callout-warning">
						<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
					<div class="form-group">
						<label class="col-md-12">Staff Image</label>
						<div class="col-md-12">
							<input type="file" class="form-control" name="staff_img" placeholder="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>