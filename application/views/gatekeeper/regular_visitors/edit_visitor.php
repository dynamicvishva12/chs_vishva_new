<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
	  		<div class="box-header with-border">
		 		<div class="row">
					<div class="col-9">
			  			<h3 class="box-title">Edit Regular Visitor</h3>
			  		</div>
			  		<div class="col-3 text-right">
			  			<!-- <button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button> -->
			  		</div>
				</div>
		 	</div>
			<!-- /.box-header -->
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Name</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $visitors_data['visitor_name']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Location</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="location" class="form-control" placeholder="Location" value="<?php echo $visitors_data['visitor_location']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Unit Number</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="unit_number" class="form-control" placeholder="Unit Number" value="<?php echo $visitors_data['visitor_unit_no']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Total Person</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="total_person" class="form-control" placeholder="Total Person" value="<?php echo $visitors_data['visitor_total_person']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Contact No.</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="contact_no" class="form-control" placeholder="Contact No." value="<?php echo $visitors_data['visitor_contact']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Visit Type</strong>
										</label>
										<div class="col-md-8">                     
											<select class="form-control select2" name="visit_type" style="width: 100%;">
											  	<option value="">Select</option>
											  	<?php if(!empty($visitorTypeArr)): ?>
											  		<?php foreach($visitorTypeArr AS $e_vstr): ?>
											  			<option value="<?php echo $e_vstr['type_id']; ?>" <?php if($visitors_data['visitor_type']==$e_vstr['type_id']): ?> selected <?php endif; ?> >
											  				<?php echo $e_vstr['vistor_type']; ?>
											  			</option>
											  		<?php endforeach; ?>
											  	<?php endif; ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-4 control-label">
											<strong >Vehicle</strong>
										</label>
										<div class="col-md-8">                     
											<input type="text" name="vehicle" class="form-control" placeholder="Vehicle" value="<?php echo $visitors_data['visitor_vehical']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 text-right">
											<input type="hidden" name="visitorId" value="<?php echo $visitorId; ?>" >
											<button type="submit" class="btn btn-success text-center" >Submit</button>
										</div>
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>