		<div class="row">
			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			<h3 class="box-title">Regular Visitors</h3>
				  		</div>
				  		<div class="col-3 text-right">
				  			<!-- <button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button> -->
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  	<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr. No</th>
									<th>Action</th>
									<th>Name</th>
									<th>Location</th>
									<th>Unit Number</th>
									<th>Total Person</th>
									<th>Contact No.</th>
									<th>Visit Type</th>
									<th>Vehicle</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Sr. No</th>
									<th>Action</th>
									<th>Name</th>
									<th>Location</th>
									<th>Unit Number</th>
									<th>Total Person</th>
									<th>Contact No.</th>
									<th>Visit Type</th>
									<th>Vehicle</th>
								</tr>
							</tfoot>
							<tbody>
							<?php $i=1; ?>
							<?php if(!empty($visitors_data)): ?>
								<?php foreach($visitors_data AS $e_vstr): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
										<center>
											<a href="<?php echo base_url()."gatekeeper/regular_visitors/edit_visitor/".$e_vstr['visitor_id']; ?>">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit">
													<i class="fa fa-edit"></i>
												</button>
											</a>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."gatekeeper/regular_visitors/delete_visitor/".$e_vstr['visitor_id']; ?>" data-toggle="tooltip" data-original-title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>
										</center>
									</td>
									<td><?php echo $e_vstr['visitor_name']; ?></td>
									<td><?php echo $e_vstr['visitor_location']; ?></td>
									<td><?php echo $e_vstr['visitor_unit_no']; ?></td>
									<td><?php echo $e_vstr['visitor_total_person']; ?></td>
									<td><?php echo $e_vstr['visitor_contact']; ?></td>
									<td><?php echo $e_vstr['vistor_type']; ?></td>
									<td><?php echo $e_vstr['visitor_vehical']; ?></td>
								</tr>
								<?php $i++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>