		<div class="row">
			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			
				  		</div>
				  		<div class="col-3 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addBody">Add Government Body</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Designation</th>
								<th>Contact No.</th>
								<th>Email Id</th>
							</tr>
						</thead>
						<tbody>
							<?php  
                                if(!empty($govBodyArr)):
                                    $x=1;
                                    foreach($govBodyArr AS $e_gov):
                            ?>
							<tr>
								<td><?php echo $x++; ?></td>
                                <td>
                                	<center>
	                                    <button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it"  data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."government_body/delete_gov_body/".$e_gov['directoryId']; ?>">
	                                    	<i class="fa fa-trash-o" id="sa-params"></i>
	                                    </button>
                                   </center>
                                </td>
                                <td><?php echo ucwords($e_gov['directoryName']); ?></td>
                                <td><?php echo $e_gov['directoryDesignation']; ?></td>
                                <td><?php echo $e_gov['directoryMobile']; ?></td>
                                <td><?php echo $e_gov['directoryMail']; ?></td>
							</tr>
							    <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan=6>
                                        <center>No Records</center>
                                    </td>
                                </tr>
                            <?php endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Name</th>
								<th>Designation</th>
								<th>Contact No.</th>
								<th>Email Id</th>
							</tr>
						</tfoot>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>


<!-- Popup Model Plase Here -->
<div id="addBody" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."government_body/add_gov_body"; ?>" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Government Body</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Name</label>
						<div class="col-md-12">
							<input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo set_value('name'); ?>">
							<label class="text-danger" ><?php echo form_error('name'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Designation</label>
						<div class="col-md-12">
							<input type="text" name="post" class="form-control" placeholder="Designation" value="<?php echo set_value('post'); ?>">
							<label class="text-danger"><?php echo form_error('post'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="text" name="E-mail" class="form-control" placeholder="Email" value="<?php echo set_value('E-mail'); ?>">
							<label class="text-danger"><?php echo form_error('E-mail'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Mobile Number</label>
						<div class="col-md-12">
							<input type="text" name="Owner-mobile" class="form-control" placeholder="Mobile Number" value="<?php echo set_value('Owner-mobile'); ?>">
							<label class="text-danger"><?php echo form_error('Owner-mobile'); ?></label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="society_key" value="<?php echo $society_key; ?>">
	                <input type="hidden" name="user_name" value="<?php echo $user_name; ?>">
					<button type="submit" class="btn btn-success float-right" >Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#addBody').modal('show');

		<?php endif; ?>
	});

</script>
