
<?php $uri_one=$this->uri->segment('1'); ?>
<!-- Vendor JS -->
<script src="<?php echo $assets_url; ?>js/vendors.min.js"></script>
<script src="<?php echo $assets_url; ?>icons/feather-icons/feather.min.js"></script>  

<?php if($uri_one=="dashboard"): ?>
<script src="<?php echo $assets_url; ?>vendor_components/apexcharts-bundle/dist/apexcharts.js"></script>
<?php endif; ?>

<script src="<?php echo $assets_url; ?>vendor_components/progressbar.js-master/dist/progressbar.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/datatable/datatables.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<?php if($uri_one=="dashboard"): ?>
<script src="<?php echo $assets_url; ?>vendor_components/ckeditor/ckeditor.js"></script>
<?php endif; ?>
<script src="<?php echo $assets_url; ?>vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>

<script src="<?php echo $assets_url; ?>vendor_components/perfect-scrollbar-master/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-select/dist/js/bootstrap-select.js"></script>

<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/select2/dist/js/select2.full.js"></script>
<script src="<?php echo $assets_url; ?>vendor_plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo $assets_url; ?>vendor_plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo $assets_url; ?>vendor_plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/moment/min/moment.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo $assets_url; ?>vendor_plugins/iCheck/icheck.min.js"></script>

<!-- Florence Admin App -->
<script src="<?php echo $assets_url; ?>js/template.js"></script>
<?php if($uri_one=="dashboard"): ?>
<script src="<?php echo $assets_url; ?>js/pages/dashboard.js"></script>
<?php endif; ?>

<script src="<?php echo $assets_url; ?>js/demo.js"></script>
<script src="<?php echo $assets_url; ?>js/pages/data-table.js"></script>
<script src="<?php echo $assets_url; ?>js/pages/advanced-form-element.js"></script>

<?php if($uri_one=="dashboard"): ?>
<script src="<?php echo $assets_url; ?>js/pages/editor.js"></script>
<?php endif; ?>

<script>
  $(document).ready(function() {
  //Preloader
    preloaderFadeOutTime = 500;
    function hidePreloader() {
      var preloader = $('.spinner-wrapper');
      preloader.fadeOut(preloaderFadeOutTime);
    }
    hidePreloader();
  });
</script>
