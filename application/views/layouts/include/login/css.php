        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <!-- Website CSS style -->
        <link href="<?php echo $assets_url; ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Pacifico&subset=latin-ext,vietnamese" rel="stylesheet">
        <!-- Website Font style -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $assets_url; ?>form1.css">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="icon" href="<?php echo $assets_url; ?>img/logo/chs.ico" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $assets_url; ?>img/logo/chs.ico" />
        <title>CHS Vishva Admin</title>
        <style type="text/css">

            @import url('https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&subset=latin-ext');

            #playground-container {
                height: 500px;
                overflow: hidden !important;
            }
            .main{
                margin-top:70px;
                -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                padding:0px;
            }
            .fb:focus, .fb:hover{color:FFF !important;}
            body{
                background-image:url(img/login.jpg);
                background-size: 100% 100%;
                background-repeat: no-repeat;
                font-family: 'Raleway', sans-serif;
            }
            .left-side{
                padding:0px 0px 100px;
                background: linear-gradient(0deg, #4DB6AC, #8BC34A);
                background-size:cover;
                min-height:514px;
                max-height: 700px;
                height: 514px;
            }
            .left-side h1{
                font-size: 70px;
                font-weight: 900;
                color:#FFF;
                padding: 50px 10px 00px 26px;
            }
            .left-side p{
                font-weight:15px;
                color:#FFF;
                padding:10px 10px 10px 26px;
            }
            .left-side h4{
                color:#FFF;
                padding:10px 10px 10px 26px;
            }
            .mdl-button{
                margin-right: 15px;
                margin-left:26px; 
            }
            .fb{
                background: #2d6bb7;
                width: 200px; max-width: 50px;
                color: #FFF;
                padding: 10px 15px;
                border-radius: 18px;
                font-size: 12px;
                font-weight: 600;
                margin-right: 15px;
                margin-left:26px;-webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);}
            .tw{
                background: #20c1ed;
                color: #FFF;
                padding: 10px 15px;
                border-radius: 18px;
                font-size: 12px;
                font-weight: 600;
                margin-right: 15px;-webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
            }
            .right-side{
                padding:30px 0px 100px;
                 background: #FFF;
                background-size:cover;
                min-height:514px;
                height: 514px;
            }
            .right-side h1{
                font-size: 30px;
                font-weight: 700;
                color:#000;
                padding: 50px 10px 00px 50px;
            }
            .right-side p{
                font-weight:600;
                color:#000;
                padding:10px 50px 10px 50px;
            }
            .form{
                padding:10px 50px 10px 50px;
            }
            .form-control{
                box-shadow: none !important;
                border-radius: 0px !important;
                border-bottom: 1px solid #9100ff !important;
                border-top: none !important;
                border-left: none !important;
                border-right: none !important;
            }
            .btn-deep-purple {
                background: #84d14e;
                border-radius: 18px;
                padding: 5px 19px;
                color: #FFF;
                font-weight: 600;
                float: right;
                -webkit-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                -moz-box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
                box-shadow: 0px 0px 14px 0px rgba(0,0,0,0.24);
            }
            .mdl-button--raised {
                background: rgba(158,158,158,.2);
                box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
            }
            .mdl-button {
                background: 0 0;
                border: none;
                border-radius: 2px;
                color: #000;
                position: relative;
                height: 36px;
                margin: 0;
                min-width: 64px;
                padding: 0 16px;
                display: inline-block;
                font-family: "Roboto","Helvetica","Arial",sans-serif;
                font-size: 14px;
                font-weight: 500;
                text-transform: uppercase;
                letter-spacing: 0;
                overflow: hidden;
                will-change: box-shadow;
                transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);
                outline: none;
                cursor: pointer;
                text-decoration: none;
                text-align: center;
                line-height: 36px;
                vertical-align: middle;
            }
            .mdl-button, {
                -webkit-tap-highlight-color: transparent;
                -webkit-tap-highlight-color: rgba(255,255,255,0);
            }
            user agent stylesheet
            input[type="button" i], input[type="submit" i], input[type="reset" i], input[type="file" i]::-webkit-file-upload-button, button {
                padding: 1px 6px;
            }
            user agent stylesheet
            input[type="button" i], input[type="submit" i], input[type="reset" i], input[type="file" i]::-webkit-file-upload-button, button {
                align-items: flex-start;
                text-align: center;
                cursor: default;
                color: buttontext;
                background-color: buttonface;
                box-sizing: border-box;
                padding: 2px 6px 3px;
                border-width: 2px;
                border-style: outset;
                border-color: buttonface;
                border-image: initial;
            }
            user agent stylesheet
            button {
                text-rendering: auto;
                color: initial;
                letter-spacing: normal;
                word-spacing: normal;
                text-transform: none;
                text-indent: 0px;
                text-shadow: none;
                display: inline-block;
                text-align: start;
                margin: 0em;
                font: 400 13.3333px Arial;
            }
            user agent stylesheet
            button,{
                -webkit-writing-mode: horizontal-tb !important;
            }
            user agent stylesheet
            button {
                -webkit-appearance: button;
            }
        </style>