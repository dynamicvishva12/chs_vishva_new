  <header class="main-header">
	<div class="d-flex align-items-center logo-box justify-content-between">
		<a href="#" class="waves-effect waves-light nav-link rounded d-none d-md-inline-block mx-10 push-btn" data-toggle="push-menu" role="button">
			<i class="ti-menu"></i>
		</a>	
		<!-- Logo -->
		<a href="index.php" class="logo">
		  <!-- logo-->
		  <div class="logo-lg">
			  <span class="light-logo"><img src="<?php echo $assets_url; ?>IMAGES/admin_logo.png" alt="logo"></span>
			  <!-- <span class="dark-logo"><img src="<?php //echo $assets_url; ?>tmp_images/logo-light-text.png" alt="logo"></span> -->
		  </div>
		</a>	
	</div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top pl-10">
      <!-- Sidebar toggle button-->
	  <div class="app-menu">
		<ul class="header-megamenu nav">
			<li class="btn-group nav-item d-md-none">
				<a href="#" class="waves-effect waves-light nav-link rounded push-btn" data-toggle="push-menu" role="button">
					<i class="ti-menu"></i>
			    </a>
			</li>
			<li class="btn-group nav-item d-none d-xl-inline-block">
				<a href="<?php echo base_url()."accounting/home"; ?>" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="tooltip" data-original-title="Accounting" title="">
					<i class="ti-comments"></i>
			    </a>
			</li>
			<li class="btn-group nav-item d-none d-xl-inline-block">
				<a href="<?php echo base_url()."report/balance_sheet"; ?>" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="tooltip" data-original-title="Balance Sheet" title="">
					<i class="ti-email"></i>
			    </a>
			</li>
			<li class="btn-group nav-item d-none d-xl-inline-block">
				<a href="<?php echo base_url()."report/trail_balance"; ?>" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="tooltip" data-original-title="Trial Balance" title="">
					<i class="ti-check-box"></i>
			    </a>
			</li>
			<li class="btn-group nav-item d-none d-xl-inline-block">
				<a href="<?php echo base_url()."report/profit_loss"; ?>" class="waves-effect waves-light nav-link rounded svg-bt-icon" data-toggle="tooltip" data-original-title="Profit and Loss" title="">
					<i class="ti-calendar"></i>
			    </a>
			</li>
		</ul> 
	  </div>
		
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav">	
			<li class="btn-group nav-item d-lg-inline-flex d-none">
				<a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded full-screen" title="Full Screen">
					<i class="ti-fullscreen"></i>
			    </a>
			</li>	  
			<li class="btn-group d-lg-inline-flex d-none">
				<div class="app-menu">
					<div class="search-bx mx-5">
						<form>
							<div class="input-group">
							  <input type="search" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
							  <div class="input-group-append">
								<button class="btn" type="submit" id="button-addon3"><i class="ti-search"></i></button>
							  </div>
							</div>
						</form>
					</div>
				</div>
			</li>
		  <!-- Notifications -->
		  <li class="dropdown notifications-menu">
			<a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="Notifications">
			  <i class="ti-bell"></i>
			</a>
			<ul class="dropdown-menu animated bounceIn">

			  	<li class="header">
					<div class="p-20">
						<div class="flexbox">
							<div>
								<h4 class="mb-0 mt-0">Notifications</h4>
							</div>
							<div>
								<a href="#" class="text-danger">Clear All</a>
							</div>
						</div>
					</div>
			  	</li>

			  <li>
				<!-- inner menu: contains the actual data -->
				<ul class="menu sm-scrol">
				<?php if(!empty($societyLogData)): ?>
					<?php foreach($societyLogData AS $eLog): ?>
					<li>
						<a href="#">
						  <i class="fa fa-user text-success"></i> 
						  <span class="font-weight-bold"><?php echo $eLog['logModule']; ?></span>: 
						  <span class="font-italic"><?php echo $eLog['logDescription']; ?></span>
						</a>
					</li>
					<?php endforeach; ?>
				<?php endif; ?>
				</ul>
			  </li>
			  <li class="footer">
				  <a href="#">View all</a>
			  </li>
			</ul>
		  </li>	
		  
	      <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="User">
				<i class="ti-user"></i>
            </a>
            <ul class="dropdown-menu animated flipInX">
              <li class="user-body">
				 <a class="dropdown-item" href="<?php echo base_url()."user/user_profile/".$_user_name; ?>">
				 	<i class="ti-user text-muted mr-2"></i> Profile
				 </a>
				 <a class="dropdown-item" href="#"><i class="ti-settings text-muted mr-2"></i> Settings</a>
				 <div class="dropdown-divider"></div>
				 <a class="dropdown-item confirm_logout" href="javascript:void(0)"><i class="ti-lock text-muted mr-2"></i> Logout</a>
              </li>
            </ul>
          </li>	
			
        </ul>
      </div>
    </nav>
  </header>

<script type="text/javascript">
	
	$(document).ready(function(){

		$('.confirm_logout').on('click', function(){

    		var ref_url = $(this).data('link');
    		
    		 swal({   
	            title: "Are you sure?",   
	            text: "Do you really want to log off ?",   
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Yes, logout!",   
	            cancelButtonText: "No, cancel please!",   
	            closeOnConfirm: false,   
	            closeOnCancel: false 
	        }, function(isConfirm){   
	            if (isConfirm) {    
	            	
	            	swal("Logging Out", "You will be logout shortly", "success");   
	            	
    				setTimeout(function(){ 
    					window.location.href="<?php echo base_url()."auth/logout"; ?>";
    				}, 1500);
	                
	            } else {     
	                swal("Cancelled", "You cancelled :)", "error");   
	            } 
	        });
    	});

	});

</script>