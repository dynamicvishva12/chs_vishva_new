
<?php $uri_one=$this->uri->segment('1'); ?>
<?php $uri_two=$this->uri->segment('2'); ?>
<aside class="main-sidebar">
   <!-- sidebar-->
   <section class="sidebar">
      <div class="user-profile px-10 py-15">
         <div class="d-flex align-items-center">
            <div class="image">
               <?php $user_profile=$this->session->userdata('_user_profile'); ?>
               <?php if($user_profile!=""): ?>
               <img src="<?php echo $assets_url.'IMAGESDRIVE/'.$user_profile; ?>" class="avatar avatar-lg" alt="User Image">
               <?php else: ?>
               <img src="<?php echo $assets_url; ?>tmp_images/avatar/1.jpg" class="avatar avatar-lg" alt="User Image">
            <?php endif; ?>
            </div>
            <div class="info ml-10">
               <p class="mb-0">Welcome</p>
               <?php
                  $fname=$this->session->userdata('_user_fname');
                  $lname=$this->session->userdata('_user_lname');

                  $sess_user_name=$fname." ".$lname;
               ?>
               <h5 class="mb-0"><?php echo $sess_user_name; ?></h5>
            </div>
         </div>
      </div>
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
         <li class="<?php if($uri_one=="dashboard"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."dashboard/" ?>">
            <i class="ti-dashboard"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($uri_one=="notices"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-email"></i>
            <span>Notices & Meetings</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_one=="notices"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."notices/list" ?>"><i class="ti-more"></i>Notices</a>
               </li>
               <!-- <li><a href="meetings.php"><i class="ti-more"></i>Meetings</a></li> -->
                <li class="<?php if($uri_one=="meetings"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."meetings/home" ?>"><i class="ti-more"></i>Meetings</a>
               </li>
            </ul>
         </li>
         <li class="<?php if($uri_one=="complaints"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."complaints/home"; ?>">
            <i class="ti-email"></i>
            <span>Complaints</span>
            </a>
         </li>
         <li class="treeview <?php if($uri_one=="maintenance_management"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-layout-grid2"></i>
            <span>Maintenance</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="maintenance_plan"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."maintenance_management/maintenance_plan/list" ?>">
                     <i class="ti-more"></i>Maintenance Plan
                  </a>
               </li>
               <li class="<?php if($uri_two=="individual_maintenance"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."maintenance_management/individual_maintenance/list"; ?>">
                     <i class="ti-more"></i>Individual Maintenance
                  </a>
               </li>
               <li class="<?php if($uri_two=="penalty"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."maintenance_management/penalty/list"; ?>">
                     <i class="ti-more"></i>Penalty
                  </a>
               </li>
               <li class="<?php if($uri_two=="maintenance_generation"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."maintenance_management/maintenance_generation/home"; ?>">
                     <i class="ti-more"></i>Maintenance Generation
                  </a>
               </li>
            </ul>
         </li>
         <li class="header">Communication</li>
         <li class="<?php if($uri_one=="poll"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."poll/home"; ?>">
            <i class="ti-alert"></i>
            <span>Poll</span>
            </a>
         </li>
         <li class="<?php if($uri_one=="booking_request"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."booking_request/list" ?>">
            <i class="ti-unlock"></i>
            <span>Booking Request</span>
            </a>
         </li>
         <li class="<?php if($uri_one=="chats"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."chats/home"; ?>">
            <i class="ti-unlock"></i>
            <span>Chats</span>
            </a>
         </li>
         <li class="<?php if($uri_one=="schedular"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."schedular/home"; ?>">
            <i class="ti-unlock"></i>
            <span>Schedular</span>
            </a>
         </li>
         <li class="<?php if($uri_one=="news_letter"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."news_letter/home"; ?>">
            <i class="ti-unlock"></i>
            <span>Newsletters</span>
            </a>
         </li>
         <li class="header">Informative</li>
         <li class="treeview <?php if($uri_one=="society_management"): ?>active menu-open<?php endif; ?>" >
            <a href="#">
            <i class="ti-pencil-alt"></i>
            <span>Society Management</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="society_staff"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."society_management/society_staff/list" ?>">
                     <i class="ti-more"></i>Society Staff
                  </a>
               </li>
               <li class="<?php if($uri_two=="residential_shop"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."society_management/residential_shop/list"; ?>">
                     <i class="ti-more"></i>Residential Shops
                  </a>
               </li>
               <li class="<?php if($uri_two=="parking_allotment"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."society_management/parking_allotment/list"; ?>">
                     <i class="ti-more"></i>Parking Allotment
                  </a>
               </li>
            </ul>
         </li>
         <li class="treeview <?php if($uri_one=="member_management"): ?>active menu-open<?php endif; ?>" >
            <a href="#">
            <i class="ti-cup"></i>
            <span>Member Management</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="pending_approval"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."member_management/pending_approval/list" ?>">
                     <i class="ti-more"></i>Pending Approval
                  </a>
               </li>
               <li class="<?php if($uri_two=="active_member"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."member_management/active_member/list" ?>">
                     <i class="ti-more"></i>Active Members
                  </a>
               </li>
               <li class="<?php if($uri_two=="inactive_member"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."member_management/inactive_member/list" ?>">
                     <i class="ti-more"></i>Inactive Members
                  </a>
               </li>
               <li class="<?php if($uri_two=="tenant_member"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."member_management/tenant_member/list" ?>">
                     <i class="ti-more"></i>Tenants Members
                  </a>
               </li>
               <li class="<?php if($uri_two=="disabled_member"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."member_management/disabled_member/list" ?>">
                     <i class="ti-more"></i>Disabled Members
                  </a>
               </li>
            </ul>
         </li>
         <li class="<?php if($uri_one=="managing_commitee"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."managing_commitee/list"; ?>">
            <i class="ti-file"></i>
            <span>Managing Committe</span>
            </a>
         </li>
         <li class="<?php if($uri_one=="government_body"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."government_body/list"; ?>">
            <i class="ti-layout-grid3"></i>
            <span>Goverment Body</span>
            </a>
         </li>
         <li class="treeview <?php if($uri_one=="reg_docs"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-pie-chart"></i>
            <span>Register & Documents</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="<?php if($uri_two=="jform"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."reg_docs/jform"; ?>">
                     <i class="ti-more"></i>Form J
                  </a>
               </li>
               <li class="<?php if($uri_two=="audit_formo"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."reg_docs/audit_formo"; ?>">
                     <i class="ti-more"></i>Audit Reports & Form O
                  </a>
               </li>
               <li class="<?php if($uri_two=="share_register"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."reg_docs/share_register"; ?>">
                     <i class="ti-more"></i>Share Register
                  </a>
               </li>
               <li class="<?php if($uri_two=="nomi_register"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."reg_docs/nomi_register"; ?>">
                     <i class="ti-more"></i>Nomination Register
                  </a>
               </li>
               <li class="<?php if($uri_two=="other_docs"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."reg_docs/other_docs"; ?>">
                     <i class="ti-more"></i>Other Documents
                  </a>
               </li>
            </ul>
         </li>
         <li class="treeview <?php if($uri_one=="lien_legal"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-map"></i>
            <span>Lien & Legal</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="lien"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."lien_legal/lien/list"; ?>">
                     <i class="ti-more"></i>Lien
                  </a>
               </li>
               <li class="<?php if($uri_two=="legal"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."lien_legal/legal/list"; ?>">
                     <i class="ti-more"></i>Legal
                  </a>
               </li>
            </ul>
         </li>
         <li class="treeview <?php if($uri_one=="events" || $uri_one=="albums"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-map"></i>
            <span>Events & Albums</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_one=="events"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."events/list"; ?>">
                     <i class="ti-more"></i>Events
                  </a>
               </li>
               <li class="<?php if($uri_one=="albums"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."albums/list"; ?>">
                     <i class="ti-more"></i>Albums
                  </a>
               </li>
            </ul>
         </li>
         <li class="treeview <?php if($uri_one=="vendor"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-map"></i>
            <span>Vendors</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="society_vendor"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."vendor/society_vendor"; ?>">
                     <i class="ti-more"></i>Society Vendors
                  </a>
               </li>
               <li class="<?php if($uri_two=="residential_vendor"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."vendor/residential_vendor"; ?>">
                     <i class="ti-more"></i>Residential Vendors
                  </a>
               </li>
            </ul>
         </li>
         <li class="treeview <?php if($uri_one=="gatekeeper"): ?>active menu-open<?php endif; ?>">
            <a href="#">
            <i class="ti-pencil-alt"></i>
            <span>Gatekeeper</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li class="<?php if($uri_two=="visitors"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."gatekeeper/visitors/list"; ?>">
                     <i class="ti-more"></i>Visitors
                  </a>
               </li>
               <li class="<?php if($uri_two=="expected_visitors"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."gatekeeper/expected_visitors/list"; ?>">
                     <i class="ti-more"></i>Expected Visitors
                  </a>
               </li>
               <li class="<?php if($uri_two=="gatekeeper_society_staff"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."gatekeeper/gatekeeper_society_staff/list"; ?>">
                     <i class="ti-more"></i>Society Staff
                  </a>
               </li>
               <li class="<?php if($uri_two=="regular_visitors"): ?>active<?php endif; ?>">
                  <a href="<?php echo base_url()."gatekeeper/regular_visitors/list"; ?>">
                     <i class="ti-more"></i>Regular Visitors
                  </a>
               </li>
            </ul>
         </li>
         <li class="header">Accounting</li>
         <li class="<?php if($uri_one=="accounting"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."accounting/home"; ?>">
               <i class="ti-files"></i>
               <span>Accounting</span>
            </a>
         </li>
         <li class="header">Reports </li>
         <li class="<?php if($uri_two=="blood_group"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/blood_group"; ?>">
            <i class="ti-file"></i>
            <span>Blood Groups</span>
            </a>
         </li>
         <li class="<?php if($uri_two=="blood_group1"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/blood_group"; ?>">
            <i class="ti-layout-cta-center"></i>
            <span>Ledger</span>
            </a>
         </li>
         <li class="<?php if($uri_two=="trail_balance"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/trail_balance"; ?>">
            <i class="ti-shopping-cart-full"></i>
            <span>Trail Balance</span>
            </a>
         </li>
         <li class="<?php if($uri_two=="profit_loss"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/profit_loss"; ?>">
            <i class="ti-envelope"></i>
            <span>Profit & Loss</span>
            </a>
         </li>
         <li class="<?php if($uri_two=="balance_sheet"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/balance_sheet"; ?>">
            <i class="ti-envelope"></i>
            <span>Balance Sheet</span>
            </a>
         </li>
         <li class="<?php if($uri_two=="collection_register"): ?>active<?php endif; ?>">
            <a href="<?php echo base_url()."report/collection_register"; ?>">
            <i class="ti-envelope"></i>
            <span>Monthly Collection Book</span>
            </a>
         </li>
      </ul>
   </section>
   <div class="sidebar-footer">
      <!-- item-->
      <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Instructions"><i class="ti-info"></i></a>
      <!-- item-->
      <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Helpdesk"><i class="ti-help"></i></a>
      <!-- item-->
      <a href="auth_lockscreen.php" class="link" data-toggle="tooltip" title="" data-original-title="Lockscreen"><i class="ti-power-off"></i></a>
   </div>
</aside>