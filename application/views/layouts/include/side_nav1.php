
<?php //$uri1=$this->uri->segment(1); ?>
<aside class="main-sidebar">
   <!-- sidebar-->
   <section class="sidebar">
      <div class="user-profile px-10 py-15">
         <div class="d-flex align-items-center">
            <div class="image">
               <img src="<?php echo $assets_url; ?>tmp_images/avatar/1.jpg" class="avatar avatar-lg" alt="User Image">
            </div>
            <div class="info ml-10">
               <p class="mb-0">Welcome</p>
               <h5 class="mb-0">Sourabh Jadhav</h5>
            </div>
         </div>
      </div>
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
         <li>
            <a href="index.php">
                <i class="ti-dashboard"></i>
                <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview">
            <a href="#">
                <i class="ti-email"></i>
                <span>Notices & Meetings</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="notices.php"><i class="ti-more"></i>Notices</a></li>
               <li><a href="meetings.php"><i class="ti-more"></i>Meetings</a></li>
            </ul>
         </li>
         <li>
            <a href="complaints.php">
            <i class="ti-email"></i>
            <span>Complaints</span>
            </a>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-layout-grid2"></i>
            <span>Maintainance</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="maintenance-plan.php"><i class="ti-more"></i>Maintainance Plan</a></li>
               <li><a href="individual-maintenance.php"><i class="ti-more"></i>Individual Maintainance</a></li>
               <li><a href="penalty.php"><i class="ti-more"></i>Penalty</a></li>
               <li><a href="maintenance-generation.php"><i class="ti-more"></i>Maintainance Generation</a></li>
            </ul>
         </li>
         <li class="header">Communication</li>
         <li>
            <a href="polls.php">
            <i class="ti-alert"></i>
            <span>Poll</span>
            </a>
         </li>
         <li>
            <a href="booking-requests.php">
            <i class="ti-unlock"></i>
            <span>Booking Request</span>
            </a>
         </li>
         <li>
            <a href="chats.php">
            <i class="ti-unlock"></i>
            <span>Chats</span>
            </a>
         </li>
         <li>
            <a href="schedular.php">
            <i class="ti-unlock"></i>
            <span>Schedular</span>
            </a>
         </li>
         <li>
            <a href="newsletter.php">
            <i class="ti-unlock"></i>
            <span>Newsletters</span>
            </a>
         </li>
         <li class="header">Informative</li>
         <li class="treeview">
            <a href="#">
            <i class="ti-pencil-alt"></i>
            <span>Society Management</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="society-staffs.php"><i class="ti-more"></i>Society Staff</a></li>
               <li><a href="residential-shops.php"><i class="ti-more"></i>Residential Shops</a></li>
               <li><a href="parking-allotment.php"><i class="ti-more"></i>Parking Allotment</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-cup"></i>
            <span>Member Management</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="pending-approval.php"><i class="ti-more"></i>Pending Approval</a></li>
               <li><a href="active-members.php"><i class="ti-more"></i>Active Members</a></li>
               <li><a href="inactive-members.php"><i class="ti-more"></i>Inactive Members</a></li>
               <li><a href="tenants-members.php"><i class="ti-more"></i>Tenants Members</a></li>
               <li><a href="disabled-members.php"><i class="ti-more"></i>Disabled Members</a></li>
            </ul>
         </li>
         <li>
            <a href="managing-committee.php">
            <i class="ti-file"></i>
            <span>Managing Committe</span>
            </a>
         </li>
         <li>
            <a href="government-body.php">
            <i class="ti-layout-grid3"></i>
            <span>Goverment Body</span>
            </a>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-pie-chart"></i>
            <span>Register & Documents</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="form-j.php"><i class="ti-more"></i>Form J</a></li>
               <li><a href="audit-reports-and-form-o.php"><i class="ti-more"></i>Audit Reports & Form O</a></li>
               <li><a href="share-register.php"><i class="ti-more"></i>Share Register</a></li>
               <li><a href="nomination-register.php"><i class="ti-more"></i>Nomination Register</a></li>
               <li><a href="other-documents.php"><i class="ti-more"></i>Other Documents</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-map"></i>
            <span>Lien & Legal</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="lien.php"><i class="ti-more"></i>Lien</a></li>
               <li><a href="legal.php"><i class="ti-more"></i>Legal</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-map"></i>
            <span>Events & Albums</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="events.php"><i class="ti-more"></i>Events</a></li>
               <li><a href="albums.php"><i class="ti-more"></i>Albums</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-map"></i>
            <span>Vendors</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="society-vendors.php"><i class="ti-more"></i>Society Vendors</a></li>
               <li><a href="residential-vendors.php"><i class="ti-more"></i>Residential Vendors</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-pencil-alt"></i>
            <span>Gatekeeper</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
               <li><a href=""><i class="ti-more"></i>Visitors</a></li>
               <li><a href=""><i class="ti-more"></i>Expected Visitors</a></li>
               <li><a href=""><i class="ti-more"></i>Society Staff</a></li>
               <li><a href=""><i class="ti-more"></i>Regular Visitors</a></li>
            </ul>
         </li>
         <li class="header">Accounting</li>
         <li>
            <a href="accounting.php">
            <i class="ti-files"></i>
            <span>Accounting</span>
            </a>
         </li>
         <li class="header">Reports </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-widget-alt"></i>
            <span>Blood Groups</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-layout-cta-center"></i>
            <span>Ledger</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="ti-shopping-cart-full"></i>
            <span>Trail Balance</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
            </span>
            </a>
         </li>
         <li>
            <a href="email_index.php">
            <i class="ti-envelope"></i>
            <span>Profit & Loss</span>
            </a>
         </li>
         <li>
            <a href="email_index.php">
            <i class="ti-envelope"></i>
            <span>Balance Sheet</span>
            </a>
         </li>
         <li>
            <a href="email_index.php">
            <i class="ti-envelope"></i>
            <span>Monthly Collection Book</span>
            </a>
         </li>
      </ul>
   </section>
   <div class="sidebar-footer">
      <!-- item-->
      <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
      <!-- item-->
      <a href="mailbox_inbox.php" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="ti-email"></i></a>
      <!-- item-->
      <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="ti-lock"></i></a>
   </div>
</aside>