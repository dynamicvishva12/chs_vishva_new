<style type="text/css">
	.hero{
		z-index: 100000000;
	}
</style>
<?php
ob_start();
// only whitespace and letter accepted
function NAME($val)
{
  if (!empty($val))
  {
    $val = test_input($val);
    // check if name only contains letters and whitespace
    if (preg_match("/^[a-zA-Z ]*$/",$val))
    {
      return $val;
    }
    else
    {
      echo '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Value cannot contain special Character like #, %, *, etc</strong>
		  </div>';
    }
  }
  else
  {
    echo '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Name cannot be empty</strong>
		  </div>';
  }
}

function NORMAL($val)
{
    // check if name only contains letters and whitespace
    if (preg_match("/^[a-zA-Z0-9\s,-]*$/",$val))
    {
    	test_input($val);
      return $val;
    }
}

function NUMNORMAL($val)
{
    // check if name only contains letters and whitespace
    if (preg_match("/^[0-9]*$/",$val))
    {
    	test_input($val);
      return $val;
    }
}


function NUMNORMALFLOAT($val)
{
    // check if name only contains letters and whitespace
    if (preg_match("/^[0-9].*$/",$val))
    {	
    	test_input($val);
      return $val;
    }
}
  
  // Email validation
function EMAIL($val)
{
  if (empty($val)) {
    echo '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Email is required</strong>
		  </div>';
  }
  elseif (!filter_var($val, FILTER_VALIDATE_EMAIL))
    {
      IALERT('Email Should be Valid');
    }
    else{
			$val = test_input($val);
      return $val;
    }
}

function URL($val)
{
  if (empty($val))
  {
    WALERT('URL cannot be empty');
  }
  else
  {
    $val = test_input($val);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$val))
    {
      DALERT("URL cannot be empty");
    }
    else
    {
      return $val;
    }
  }
}
  
function PINCODE($val)
{
	if(!preg_match('/^\d{6}$/', $val)){
		echo '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>PINCODE do not contain letter / sign and Of 6 digit</strong>
			  </div>';
    }
	else{
		return $val;
	}
}

function MOBILE($val)
{
	if(!preg_match('/^\d{10}$/', $val)){
			echo '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Mobile Number do not contain +91/0 and of 10 digit</strong>
					</div>';
			}
	else
	{
		return $val;
	}
}

function LANDLINE($val)
{
	if(!preg_match('/^\d{11}$/', $val)){
			echo '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>LANDLINE Number do not contain +91 and of 11 digit Including 0(zero)</strong>
					</div>';
			}
	else
	{
		return $val;
	}
}
	
	
function GENDER($val)
{
	if(!$val == "male" || !$val == "female"){
		DALERT("Gender Option should be CHECKED");
	}else{
		return $val;
	}
}

function PASSWORD($val){
	if(strlen($val) > 7)
	{
		return $val;
	}
	else{
					DALERT('Password minimum length 8');
		}
	}

	
function date_validate($val)
{
		if(strtotime($val) > strtotime(date("d-m-Y"))){
			return $val;
		}
	else
		{
			WALERT('Date should be greater then current Date');
		}
}

function test_input($data){
 $data = trim($data);
 $data = stripcslashes($data);
 $data = htmlspecialchars($data);
 return $data;
}  
	
function DALERT($val){
	echo '<div class="alert alert-danger alert-dismissible" style="z-index: 100000000;">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>'.$val.'</strong>
			  </div>';
}

function IALERT($val){
	echo '<div class="alert alert-info alert-dismissible" style="z-index: 100000000;">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>'.$val.'</strong>
			  </div>';
}

function WALERT($val){
	echo '<div class="alert alert-warning alert-dismissible" style="z-index: 100000000;">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>'.$val.'</strong>
			  </div>';
}

function SALERT($val){
	echo '<div class="alert alert-success alert-dismissible" style="z-index: 100000000;">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>'.$val.'</strong>
			  </div>';
}
?>