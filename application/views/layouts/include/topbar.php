  <?php $user_cmpy_arr=user_companies(); ?>
  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Brand -->
        <!-- 
        <a class="navbar-brand waves-effect" href="https://mdbootstrap.com/docs/jquery/" target="_blank">
          <strong class="blue-text">MDB</strong>
        </a> 
        -->
        <a class="navbar-brand waves-effect" href="<?php echo $base_url; ?>">
          <img class="company_logo" src="<?php echo $assets_url; ?>images/logo.png" >
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <?php $uri1=$this->uri->segment(1); ?>
          <?php $uri2=$this->uri->segment(2); ?>
          <!-- Left -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item <?php if($uri1=='scoreboard' || $uri1=='dashboard_pages'): ?>active<?php endif; ?>">
              <a class="nav-link waves-effect" href="<?php echo $base_url; ?>scoreboard/">Dashboard</a>
            </li>
            <li class="nav-item <?php if($uri1=='budget'): ?>active<?php endif; ?>">
              <a class="nav-link waves-effect" href="<?php echo $base_url; ?>budget/month_budget" >Budget</a>
            </li>
            <li class="nav-item <?php if($uri1=='applicable'): ?>active<?php endif; ?>">
              <a class="nav-link waves-effect" href="<?php echo $base_url; ?>applicable/applicable_list" >Applicable</a>
            </li>
            <li class="nav-item dropdown <?php if($uri1=='payment'): ?>active<?php endif; ?>">
              <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span>Payment In-Out</span>
              </a>
              <div class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <a class="dropdown-item <?php if($uri2=='bank'): ?>active<?php endif; ?>" href="<?php echo $base_url; ?>payment/bank/bank_trans_list">Bank</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item <?php if($uri2=='cash'): ?>active<?php endif; ?>" href="<?php echo $base_url; ?>payment/cash/cash_trans_list">Cash</a>
              </div>
            </li>
            <li class="nav-item <?php if($uri1=='bankcashcontra'): ?>active<?php endif; ?>">
              <a class="nav-link waves-effect" href="<?php echo $base_url; ?>bankcashcontra/list_trans" >Bank-Cash Contra</a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="javascript:void(0)" >Report</a>
            </li>
            <li class="nav-item <?php if($uri1=='system_setup'): ?>active<?php endif; ?>">
              <a class="nav-link waves-effect" href="<?php echo $base_url; ?>system_setup/account_head_ledger_list" >System Setup</a>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <!-- <li class="nav-item">
              <a href="javascript:void(0)" class="nav-link waves-effect">
                <i class="fas fa-user-circle" data-toggle="modal" data-target="#logout_modal"></i>
              </a>
            </li> -->
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pagesDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle"></i>
              </a>
              <div class="dropdown-menu" aria-labelledby="pagesDropdown3" style="margin-left: -115px;">
                <?php if(count($user_cmpy_arr)>"1"): ?>
                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#switch_cmpy_modal">Switch Company</a>
                <div class="dropdown-divider"></div>
                <?php endif; ?>
                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#logout_modal">Log Out</a>
              </div>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <!-- <div class="sidebar-fixed position-fixed">

      <a class="logo-wrapper waves-effect">
        <img src="https://mdbootstrap.com/img/logo/mdb-email.png" class="img-fluid" alt="">
      </a>

      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item active waves-effect">
          <i class="fas fa-chart-pie mr-3"></i>Dashboard
        </a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-user mr-3"></i>Profile</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-table mr-3"></i>Tables</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-map mr-3"></i>Maps</a>
        <a href="#" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-money-bill-alt mr-3"></i>Orders</a>
      </div>

    </div> -->
    <!-- Sidebar -->

  </header>
  <!--Main Navigation-->

    <div class="modal fade" id="logout_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="row wow fadeIn text-center">
              <div class="col-md-12 mb-2">
                Are you sure want to logout ?
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <a href="<?php echo $base_url; ?>admin/logout/<?php echo $this->userId; ?>">
              <button type="button" class="btn btn-sm proj_deepblue">Logout</button>
            </a>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="switch_cmpy_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
      <div class="modal-dialog modal-sm" role="document">
        <form action="<?php echo base_url(); ?>scoreboard/switch_company" method="POST">
          <div class="modal-content">
            <div class="modal-header" style="padding-left: 95px;">
              <h5 class="modal-title" id="exampleModalLabel" style="font-size: 15px;">Switch Company</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <div class="row wow fadeIn">
                <div class="col-lg-12">
                  <select class="browser-default custom-select" name="companyId" id="companyId">
                    
                    <?php if(!empty($user_cmpy_arr)): ?>
                      <?php foreach($user_cmpy_arr AS $e_dt): ?>
                      <option value="<?php echo $e_dt['companyId']; ?>" <?php if($this->companyId==$e_dt['companyId']): ?> selected <?php endif; ?> ><?php echo $e_dt['companyName']; ?> <?php if($this->companyId==$e_dt['companyId']): ?> (Current) <?php endif; ?></option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>

            </div>
            <div class="modal-footer" style="padding-right: 95px;">
              <input type="hidden" name="curr_url" value="<?php echo base_url(uri_string()); ?>" >
              <button type="submit" class="btn btn-sm proj_deepblue">Switch</button>
            </div>
          </div>
        </form>
      </div>
    </div>