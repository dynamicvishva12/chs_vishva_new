<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo $assets_url; ?>tmp_images/favicon.ico">

        <title>CHS Vishva Admin</title>
        <?php $this->load->view($css); ?>
    </head>
    <body class="hold-transition theme-primary bg-img" style="background-image: url(<?php echo $assets_url; ?>tmp_images/Background.jpg)" data-overlay="5">
        <div class="container h-p100">

            <?php $this->load->view($view); ?>

        </div><!--container-->
        
        <script src="<?php echo $assets_url; ?>js/vendors.min.js"></script>
        <script src="<?php echo $assets_url; ?>icons/feather-icons/feather.min.js"></script>

        <?php
            $success_msg=$this->session->flashdata('success_msg');
            $error_msg=$this->session->flashdata('error_msg');
            $warning_msg=$this->session->flashdata('warning_msg');
        ?>

        <script type="text/javascript">

            $(document).ready(function(){

                <?php if(!empty($success_msg)): ?>
                    swal("Good job!", "<?php echo $success_msg; ?>", "success");
                <?php endif; ?>

                <?php if(!empty($error_msg)): ?>
                    swal("Error!", "<?php echo $error_msg; ?>", "error"); 
                <?php endif; ?>

                <?php if(!empty($warning_msg)): ?>
                    swal('Warning', "<?php echo $warning_msg; ?>", 'warning');
                <?php endif; ?>

            });
            
        </script>

    </body>
</html>