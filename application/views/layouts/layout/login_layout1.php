<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html lang="en">
    <head> 
        <?php $this->load->view($css); ?>
    </head>
    <body>
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2 main">

                <?php $this->load->view($view); ?>

            </div><!--col-sm-8-->
        </div><!--container-->
        
        <?php $this->load->view($js); ?>
    </body>
</html>