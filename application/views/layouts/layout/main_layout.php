<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="<?php echo $assets_url; ?>tmp_images/favicon.ico">

		<title>CHS Vishva Admin <?php if(isset($page_section)): echo "- ".$page_section; endif; ?></title>

		<?php $this->load->view($css); ?>
	</head>
	<body class="hold-transition light-skin sidebar-mini theme-primary">
		<div class="spinner-wrapper">
			<div class="spinner"></div>
		</div>
		<div class="wrapper">

			<?php $this->load->view($navbar_menu); ?>

			<?php $this->load->view($side_nav); ?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="container-full">

					<!-- Content Header (Page header) -->
					<div class="content-header">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="page-title">
									<?php if(isset($page_section)): echo $page_section!='' ?  $page_section : "N/A"; endif; ?>
								</h3>
								<div class="d-inline-block align-items-center">
									<nav>
										<ol class="breadcrumb">
											<li class="breadcrumb-item">
												<a href="<?php echo base_url(); ?>">
													<i class="mdi mdi-home-outline"></i>
												</a>
											</li>
											<?php if(!empty($breadcrumbArr)): ?>
												<?php foreach($breadcrumbArr AS $e_brcmb): ?>
													<li class="breadcrumb-item <?php if($e_brcmb['active']!=""): ?>active<?php endif; ?>" aria-current="page">
														<a href="<?php echo $e_brcmb['link']; ?>">
															<?php
																if($e_brcmb['name']!="")
																	echo $e_brcmb['name'];
																else
																	echo "N/A"; 
															?>
														</a>
													</li>
												<?php endforeach; ?>
											<?php endif; ?>
										</ol>
									</nav>
								</div>
							</div>
							
						</div>
					</div>

					<!-- Main content - Start -->
					<section class="content">
						<?php $this->load->view($view); ?>
					</section>
					<!-- Main content - End -->

				</div>
			</div>

			<?php $this->load->view($footer); ?>
		</div>

		<?php $this->load->view($js); ?>

		<?php
			$success_msg=$this->session->flashdata('success_msg');
			$error_msg=$this->session->flashdata('error_msg');
			$warning_msg=$this->session->flashdata('warning_msg');
		?>

		<script type="text/javascript">

			$(document).ready(function(){

				<?php if(!empty($success_msg)): ?>
	        		swal("", "<?php echo $success_msg; ?>", "success");
	        	<?php endif; ?>

	        	<?php if(!empty($error_msg)): ?>
	        		swal("Error!", "<?php echo $error_msg; ?>", "error"); 
	        	<?php endif; ?>

			    <?php if(!empty($warning_msg)): ?>
	        		swal('Warning', "<?php echo $warning_msg; ?>", 'warning');
	        	<?php endif; ?>

	        	$('.remove_it').on('click', function(){

	        		var ref_url = $(this).data('link');
	        		
	        		 swal({   
			            title: "Are you sure?",   
			            text: "You will not be able to recover this data again!",   
			            type: "warning",   
			            showCancelButton: true,   
			            confirmButtonColor: "#DD6B55",   
			            confirmButtonText: "Yes, delete it!",   
			            cancelButtonText: "No, cancel please!",   
			            closeOnConfirm: false,   
			            closeOnCancel: false 
			        }, function(isConfirm){   
			            if (isConfirm) {    
			            	
			            	swal("Deleted!", "Your data will be deleted shortly.", "success");   
			            	
	        				setTimeout(function(){ 
	        					window.location.href=ref_url;
	        				}, 1500);
			                
			            } else {     
			                swal("Cancelled", "Your data is safe :)", "error");   
			            } 
			        });
	        	});

	        	$('.confirm_it').on('click', function(){

	        		var ref_url = $(this).data('link');

	        		var conf_msg = "";
	        		var accept = "";
	        		var btn_text = "";
	        		var accept_res = "";
	        		var cancel_res = "";

	        		if($(this).data('conf').length!="")
	        			conf_msg = $(this).data('conf');

	        		if($(this).data('accept').length!="")
	        			accept = $(this).data('accept');	

	        		if($(this).data('btn_text').length!="")
	        			btn_text = $(this).data('btn_text');	        		

	        		if($(this).data('accept_res').length!="")
	        			accept_res = $(this).data('accept_res');	        		

	        		if($(this).data('cancel_res').length!="")
	        			cancel_res = $(this).data('cancel_res');

	        		if(conf_msg!="")
	        			confirmation_msg=conf_msg;
	        		else
	        			confirmation_msg="You will not be able to recover this data again!";
	        		
	        		 swal({   
			            title: "Are you sure?",   
			            text: confirmation_msg,   
			            type: "warning",   
			            showCancelButton: true,   
			            confirmButtonColor: "#DD6B55",   
			            confirmButtonText: btn_text,   
			            cancelButtonText: "No, cancel please!",   
			            closeOnConfirm: false,   
			            closeOnCancel: false 
			        }, function(isConfirm){   
			            if (isConfirm) {    
			            	
			            	swal(accept+"!", accept_res, "success");   
			            	
	        				setTimeout(function(){ 
	        					window.location.href=ref_url;
	        				}, 1500);
			                
			            } else {     
			                swal("Cancelled", cancel_res, "error");   
			            } 
			        });
	        	});

	        	$('.self_refresh').on('click', function(){
	        		location.reload();
	        	});

	        	$('.get_back').on('click', function(){
	        		window.history.back();
	        	});
	    	});

		</script>
	</body>
</html>