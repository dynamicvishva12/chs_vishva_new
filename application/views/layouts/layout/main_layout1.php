<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Winning Scoreboard</title>

  <?php $uri1=$this->uri->segment(1); ?>
  <?php $uri2=$this->uri->segment(2); ?>

  <?php $this->load->view($css); ?>

  <?php if($uri1=='system_setup'): ?>

  <link href="<?php echo $assets_url; ?>css/system_setup.css" rel="stylesheet">

  <?php endif; ?>

  <style>

    .navbar.scrolling-navbar 
    {
      padding-top: 0px;
      padding-bottom: 0px;
      transition: background 0.5s ease-in-out, padding 0.5s ease-in-out; 
    }

    label, p, a, button, select, div, td, th, option 
    {
      font-size: 13px;
    }

    label
    {
      margin-top: 11px;
    }

    .custom-select 
    {
      display: inline-block;
      width: 100%;
      height: 30px;
      padding: 0.375rem 1.75rem 0.375rem 0.75rem;
      font-size: 12px;
      font-weight: 400;
      line-height: 1.5;
      color: #495057;
      vertical-align: middle;
      background: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3e%3cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3e%3c/svg%3e") no-repeat right 0.75rem center/8px 10px;
      background-color: rgba(0, 0, 0, 0);
      background-color: #fff;
      border: 1px solid #ced4da;
      border-radius: 0.25rem;
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin-top: 6px;
    }

    .form-control 
    {
      display: block;
      width: 100%;
      height: 30px;
      padding: 0.375rem 0.75rem;
      font-size: 11px;
      font-weight: 400;
      line-height: 1.5;#1C1C1C;
      background-color: #fff;
      background-clip: padding-box;
      border: 1px solid #ced4da;
      border-radius: 0.25rem;
      transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      margin-top: 6px;
    }

    .bootstrap-select .dropdown-toggle .filter-option-inner-inner 
    {
      overflow: hidden;
      font-size: 12px;
      margin-left: 10px;
      text-transform: none;
    }

    .sel_btn:hover
    {
      border: 1px solid darkgray;
      height: 38px;
      border-radius: 5px;
      padding: 9px;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover 
    {
      color: #999;
      border: 1px solid darkgray;
      height: 38px;
      border-radius: 5px;
      padding: 9px;
    }

    .btn-light 
    {
      color: #000;
      background-color: #fff !important;
    }

    .btn-light:not([disabled]):not(.disabled).active,
    .btn-light:not([disabled]):not(.disabled):active,
    .show>.btn-light.dropdown-toggle 
    {
      background-color:#fff !important
    }

    .bootstrap-select > .dropdown-toggle 
    {
      position: relative;
      width: 100%;
      text-align: right;
      white-space: nowrap;
      display: -webkit-inline-box;
      display: -webkit-inline-flex;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -webkit-box-align: center;
      -webkit-align-items: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
      justify-content: space-between;
      border: 1px solid darkgray;
      height: 38px;
      border-radius: 5px;
      padding: 9px;
    }

    .btn-light.dropdown-toggle 
    {
      background-color: #fff !important;
    }

    .bootstrap-select .dropdown-toggle:focus, .bootstrap-select > select.mobile-device:focus + .dropdown-toggle
    {
      outline: lightgray !important;
      outline: 5px auto -webkit-focus-ring-color !important;
      outline-offset: -2px;
    }

    .text-danger p
    {
      color: #ff3547 !important;
      margin-bottom: 0 !important; 
    }

  </style>

</head>

<body class="grey lighten-3">

  <?php $this->load->view($topbar); ?>

  <!--Main layout-->
  <main class="pt_nav-4 mx-lg-5">
    <div class="container-fluid">

      <?php $this->load->view($view); ?>

    </div>
  </main>
  <!--Main layout-->

  <?php $this->load->view($footer); ?>

  <?php $this->load->view($js); ?>

</body>

</html>
