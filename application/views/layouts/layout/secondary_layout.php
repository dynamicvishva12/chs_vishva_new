<!DOCTYPE html>
<html style="padding-top: 0px; margin-top: 0px;">
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta charset="utf-8">
	   
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <title>CHS Vishva (Admin)</title>
	    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	    <meta name="viewport" content="width=device-width" />

	    <?php $this->load->view($css); ?>

	</head>
	<body style="margin-top: 0px;">

		<div class="wrapper">


			<div class="content">
            	<div class="container-fluid">

            		<?php $this->load->view($view); ?>

            	</div>
            </div>


		</div>

		<?php $this->load->view($footer); ?>

	</body>
</html>