		<div class="row">

			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h3 class="box-title">Member's Legal Documents</h3>
				  		</div>
				  		<div class="col-2 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addLegal">Add Documents</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Date</th>
								<th>Case</th>
								<th>Member Name</th>
								<th>Member ID</th>
								<th>Documents</th>
								<th>Description</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Date</th>
								<th>Case</th>
								<th>Member Name</th>
								<th>Member ID</th>
								<th>Documents</th>
								<th>Description</th>
							</tr>
						</tfoot>
						<tbody>
							<?php $i=1; ?>
							<?php if(!empty($legalData)): ?>
								<?php foreach($legalData AS $eLeg): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
										<center>
											<a href="<?php echo base_url()."lien_legal/legal/edit_legal/".$eLeg['sr_id']; ?>">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
											</a>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."lien_legal/legal/delete_legal/".$eLeg['sr_id']; ?>" data-toggle="tooltip" data-original-title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>
										</center>
									</td>
									<td><?php echo date("d-m-Y",strtotime($eLeg['date'])); ?></td>
									<td><?php echo $eLeg['case_no']; ?></td>
									<td><?php echo $eLeg['fullName']; ?></td>
									<td><?php echo $eLeg['member_id']; ?></td>
									<td>
										<a href="<?php echo $eLeg['leg_doc']; ?>">
											<?php 
												$doc_lin=explode("/", $eLeg['leg_doc']);
												echo end($doc_lin);
											?>
										</a>
									</td>
									<td><?php echo $eLeg['details']; ?></td>
								</tr>
								<?php $i++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>


<!-- Popup Model Plase Here -->
<div id="addLegal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url()."lien_legal/legal/add_legal"; ?>" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Legal Documents</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Select Member</label>
						<select class="form-control select2" name="member" style="width: 100%;">
						<?php if(!empty($memberArr)): ?>
					  		<?php foreach($memberArr AS $e_mbr): ?>
					  			<option value="<?php echo $e_mbr['usrId']; ?>">
					  				<?php echo ucfirst($e_mbr['usrFname']." ".$e_mbr['usrLname']." (".$e_mbr['usrId'].")"); ?>
					  			</option>
					  		<?php endforeach; ?>
					  	<?php endif; ?>
						</select>
				  	</div>
				  	<div class="form-group">
						<label class="col-md-12">Case No.</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="legal_caseno" placeholder="Enter Case No.">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Date</label>
						<div class="col-md-12">
							<input type="date" class="form-control" name="legal_date" placeholder="Date">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Details</label>
						<div class="col-md-12">
							<textarea type="text" name="legal_detail" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group callout callout-warning">
						<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
					<div class="form-group">
					  	<label>File</label>
					  	<label class="file">
							<input type="file" id="file" name="legal_myfile">
					  	</label>
				  	</div>	
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>