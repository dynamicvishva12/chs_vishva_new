		<div class="row">
			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-9">
				  			<h3 class="box-title">Member's Lien</h3>
				  		</div>
				  		<div class="col-3 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addLien">Add Member Lien</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Date</th>
								<th>Details</th>
								<th>Authority</th>
								<th>Documents</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Date</th>
								<th>Details</th>
								<th>Authority</th>
								<th>Documents</th>
							</tr>
						</thead>
						</tfoot>
						<tbody>
							<?php $i=1; ?>
							<?php if(!empty($lienData)): ?>
								<?php foreach($lienData AS $eLien): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
										<center>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."lien_legal/lien/delete_lien/".$eLien['sr_id']; ?>" data-toggle="tooltip" data-original-title="Delete">
												<i class="fa fa-trash-o"></i>
											</button>
										</center>
									</td>
									<td><?php echo $eLien['fullName'];?></td>
                            		<td><?php echo date("d-m-Y",strtotime($eLien['l_m_date']));?></td>
                            		<td><?php echo $eLien['deatails'];?></td>
                            		<td><?php echo $eLien['l_m_authority'];?></td>
                            		<td>
                            			<a href="<?php echo $eLien['l_doc']; ?>">
                            			<?php 
			                                $doc_lin=explode("/", $eLien['l_doc']);
			                                echo end($doc_lin);
			                            ?>
			                            </a>
			                        </td>
								</tr>
							<?php $i++; ?>
							<?php endforeach; ?>
						<?php endif; ?>
						</tbody>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>


<!-- Popup Model Plase Here -->
<div id="addLien" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url()."lien_legal/lien/add_lien"; ?>" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Member Lien</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Select Member</label>
						<select class="form-control select2" name="member" style="width: 100%;">
						  	<option value="">Select</option>
						  	<?php if(!empty($memberArr)): ?>
						  		<?php foreach($memberArr AS $e_mbr): ?>
						  			<option value="<?php echo $e_mbr['usrId']; ?>">
						  				<?php echo ucfirst($e_mbr['usrFname']." ".$e_mbr['usrLname']." (".$e_mbr['usrId'].")"); ?>
						  			</option>
						  		<?php endforeach; ?>
						  	<?php endif; ?>
						</select>
				  	</div>
					<div class="form-group">
						<label class="col-md-12">Date</label>
						<div class="col-md-12">
							<input type="date" class="form-control" name="lin_date" placeholder="Date">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Lien Details</label>
						<div class="col-md-12">
							<textarea type="text" name="lin_detail" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Authority</label>
						<div class="col-md-12">
							<input type="text" name="lin_Authourity" class="form-control" placeholder="Enter Authority">
						</div>
					</div>
					<div class="form-group">
					  	<label>File</label>
					  	<label class="file">
							<input type="file" id="file" name="lin_myfile">
					  	</label>
				  	</div>	
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>