<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<!-- Multiple Radios -->
									<div class="form-group row">
										<label class="col-md-2">Select Member</label>
										<div class="col-md-10">
											<select class="form-control select2" name="member" style="width: 100%;">
											  	<option value="">Select</option>
											  	<?php if(!empty($memberArr)): ?>
											  		<?php foreach($memberArr AS $e_mbr): ?>
											  			<option value="<?php echo $e_mbr['usrId']; ?>" <?php if($lienData['member_id']==$e_mbr['usrId']): ?> selected <?php endif; ?> >
											  				<?php echo ucfirst($e_mbr['usrFname']." ".$e_mbr['usrLname']." (".$e_mbr['usrId'].")"); ?>
											  			</option>
											  		<?php endforeach; ?>
											  	<?php endif; ?>
											</select>
										</div>
								  	</div>
									<div class="form-group row">
										<label class="col-md-2">Date</label>
										<div class="col-md-10">
											<input type="date" class="form-control" name="lin_date" placeholder="Date" value="<?php echo $lienData['l_m_date']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2">Lien Details</label>
										<div class="col-md-10">
											<textarea type="text" name="lin_detail" class="form-control"><?php echo $lienData['deatails']; ?></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2">Authority</label>
										<div class="col-md-10">
											<input type="text" name="lin_Authourity" class="form-control" placeholder="Enter Authority" value="<?php echo $lienData['l_m_authority']; ?>">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2"></label>
										<div class="col-md-5 callout callout-warning">
											<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
											<ul>
												<li>File size must be 5MB or less.</li>
												<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
											</ul>
										</div>  
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textarea">
											<strong>Upload Document:</strong>
										</label>
										<div class="col-md-10"> 
											<input id="oldfile" name="lin_myfile"  class="input-file" type="file" onclick="switchVisible();">
											<?php 
												// echo $notice_data['noticeFilename']; 
												$temp = explode('/', $lienData['l_doc']);
												$getId = end($temp);
											?>
											<a id="nameold" href="<?php echo $lienData['l_doc']; ?>" target="_blank"><?php echo $getId; ?></a>
											<a href="#" id="removebtn" class="btn btn-danger confirmations" onclick="switchVisible1();">
												<i class="material-icons">Clear</i>
											</a>
											<input id="image_status" name="image_status" class=" input-file" value="unchanged" type="hidden">
											<input type="hidden" name="leinId" id="leinId" value="<?php echo $lienData['sr_id']; ?>" required>
										<!-- Button -->
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 text-right">
											<!-- <button type="submit" id="singlebutton" name="editnotice" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  style="background: #212121; color: #fff; ">post</button> -->
											<button type="submit" class="btn btn-success text-center" >Submit</button>

										</div>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>


<script>
    function switchVisible() 
    {
        if(document.getElementById('oldfile')) 
        {
            document.getElementById("image_status").value="changed";
           	document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
        }
	}

	function switchVisible1() 
	{
        if(document.getElementById('removebtn')) 
        {
            document.getElementById("image_status").value="changed";
           	document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
        }
	}
</script>
