<form class="form-horizontal" action="<?php echo base_url()."maintenance_management/individual_maintenance/edit_maintenance"; ?>" method="post">
    <div class="modal-content ">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Maintenance</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php 
                    if(!empty($result)):
                        foreach($result as $key => $name):
                ?>
                <div class="col-md-4">
                    <div class="form-group row">
                        <label class="col-md-12"><?php echo $name['head_name']; ?></label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="<?php echo $name['maint_headId']; ?>" value="<?php echo $name['charges']; ?>" >
                        </div>
                    </div>
                </div>
                <?php 
                       endforeach;
                    endif;
                ?>
                <input type="hidden" name="user_name" value="<?php echo $userId; ?>">
                <input type="hidden" name="fromdate" value="<?php echo $fromdate; ?>">
                <input type="hidden" name="todate" value="<?php echo $todate; ?>">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" name="editmaint" class="btn btn-success float-right" >Save</button>
            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</form>