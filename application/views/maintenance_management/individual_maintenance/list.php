		<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h4 class="box-title">Individual Maintenance</h4>
				  		</div>
				  		<div class="col-2 text-right">
				  			
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th> Sr No </th>
				                <th> Action </th>
				                <th> Member Id</th>
				                <th> Member Name</th>
				                <?php 
				                    if(!empty($headers_view)):
				                        foreach($headers_view as $key=> $name):
				                ?>
				                <th><?php echo $name['head_name']; ?> </th>
				                <?php  
				                        endforeach;
				                    endif;
				                ?>
				                <th>From Date </th>
				                <th>To date</th>
							</tr>
						</thead>
						<tbody>
							<?php 
			                    $x=1;
			                    if(!empty($list_view)):
			                        foreach($list_view as $list => $view):
			                ?>
			                <tr>
			                    <td><?php echo $x++; ?></td>
			                    <td>
			                        <button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning openPopup_2" data-href="<?php echo base_url(); ?>maintenance_management/individual_maintenance/edit?username=<?php echo $view['usrId']; ?>&&fromdate=<?php echo $view['from_date']; ?>&&todate=<?php echo $view['to_date']; ?>">
			                        	<i class="fa fa-edit"></i>
			                        </button>
			                    </td>
			                    <td><?php echo $usrId=$view['usrId']; ?></td>
			                    <td><?php echo $view['userName']; ?></td>
			                    <?php 
			                        if(!empty($view[$usrId])):
			                            foreach($view[$usrId] as $list1 => $view1):
			                    ?>
			                    <td><?php echo $view1; ?></td>
			                    <?php 
			                           endforeach;
			                        endif;
			                    ?>
			                    <td><?php echo $view['from_date']; ?></td>
			                    <td><?php echo $view['to_date']; ?></td>
			                </tr>
			                <?php 
			                       endforeach;
			                    endif;
			                ?>
						</tbody>
						<tfoot>
							<tr>
								<th> Sr No </th>
				                <th> Action </th>
				                <th> Member Id</th>
				                <th> Member Name</th>
				                <?php 
				                    if(!empty($headers_view)):
				                        foreach($headers_view as $key=> $name):
				                ?>
				                <th><?php echo $name['head_name']; ?> </th>
				                <?php  
				                        endforeach;
				                    endif;
				                ?>
				                <th>From Date </th>
				                <th>To date</th>
							</tr>
						</tfoot>
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->

<div id="indMain" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog roshan" style="max-width: 800px">
		
	</div>
</div>

<script>

    $(document).ready(function() {

        $('.openPopup_2').on('click',function(){

            var dataURL = $(this).attr('data-href');

            $('.roshan').load(dataURL,function(){
                $('#indMain').modal({show:true});
            });

        });
    });

</script>