<form action="<?php echo base_url()."maintenance_management/maintenance_generation/save_user_passbook"; ?>" method="post">
    <div class="modal-content ">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit Maintenance passbook</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="row" style="padding-left: 20px;" >
                <div class="col-md-12">
                    <table>
                        <?php 
                            foreach($user_maint_charges as $key=> $name)
                            {
                                $from_date=$name['from_date'];
                                $to_date=$name['to_date'];
                        ?>
                        <tr>
                            <td>
                                <label><?php echo $name['head_name']; ?> :</label>
                            </td>
                            <td style="padding-left: 10%;">
                                <input type="text"  class="form-control" value="<?php echo $name['charges']; ?>" name="<?php echo $name['maint_headId']; ?>" style="width: 160%; height: 100%;">
                            </td>
                        </tr>
                        <?php 
                            }
                        ?>
                    </table>
                    <input type="hidden" name="user_name" value="<?php echo $username; ?>">
                    <input type="hidden" name="fromdate" value="<?php echo $from_date; ?>">
                    <input type="hidden" name="todate" value="<?php echo $to_date; ?>">
                    <input type="hidden" name="transactionId" value="<?php echo $transactionId; ?>">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" name="editpassbook" class="btn btn-success float-right" >Submit</button>
            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</form>