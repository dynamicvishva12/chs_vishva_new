<div class="row">

	<div class="col-12">
		<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-header with-border">
				<div class="row">
					<div class="col-4">
						<h3 class="box-title">Maintenance Generation</h3>
					</div>
					<div class="col-8 text-right">
						<button type="button" class="waves-effect waves-light btn btn-primary dropdown-toggle" data-toggle="dropdown">Generate Maintenance Bill</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="<?php echo base_url()."maintenance_management/maintenance_generation/fixed_generation"; ?>">
								Fixed Maintenance
							</a>
						</div>
						<a href="<?php echo base_url()."maintenance_management/maintenance_generation/print_maintenance"; ?>?allfromdate=<?php echo date("Y-m-01"); ?>&&alltodate=<?php echo date("Y-m-t"); ?>">
							<button type="button" class="waves-effect waves-light btn btn-info">
								Print Bill of <?php echo date("F"); ?>
							</button>
						</a>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>View Passbook</th>
								<th>User Id</th>
								<th>Owner Name</th>
								<th>Apartment</th>
								<th>Wing</th>
								<th>Flat No.</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$x=1;
								if(!empty($member_data)):
									foreach($member_data as $key => $user):
							?>
							<tr>
								<td><?php echo $x++; ?></td>
								<td>
									<center>
										<a href="<?php echo base_url()."maintenance_management/maintenance_generation/passbook/".$user['usrId']; ?>">
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i></button>
										</a>
									</center>
								</td>
								<td><?php echo $user['usrId']; ?></td>
								<td><?php echo $user['usrFname']." ".$user['usrLname']; ?></td>
								<td><?php echo $user['usrAppartment']; ?></td>
								<td><?php echo $user['usrWing']; ?></td>
								<td><?php echo $user['usrFlat']; ?></td>
							</tr>
							<?php 
									endforeach; 
								else: 
							?>
		                        <tr>
		                            <td colspan=7><center>No Records</center></td>
		                        </tr>
		                    <?php endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr. No.</th>
								<th>View Passbook</th>
								<th>User Id</th>
								<th>Owner Name</th>
								<th>Apartment</th>
								<th>Wing</th>
								<th>Flat No.</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->

</div>
<!-- /.row -->
<!-- END tabs -->