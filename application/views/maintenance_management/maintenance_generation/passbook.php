	<div class="row">

		<div class="col-12">
			<div class="box box-default">
				<!-- /.box-header -->
				<div class="box-header with-border">
					<div class="row">
						<div class="col-8">
							<h4 class="box-title">
								Passbook (Financial year <?php echo date("d-m-Y",strtotime($lastyear))." To ".date("d-m-Y",strtotime($month1));  ?> )
							</h4>
						</div>
						<div class="col-4">
							<div class="form-group row">
								<label class="col-4 text-right"><strong>Financial year:</strong></label>
								<div class="col-8">
									<select name="fyear" class="form-control select2" style="width: 100%;">
										<option value="">Select Financial year</option>
										<?php //echo "string";
											$year = ( date('m') > 3) ? date('Y') + 1 : date('Y');
										// echo ($year-1)."-".($year);
										?>
										<option value="<?php echo $year; ?>-03-31"><?php  echo ($year-1)."-".($year); ?></option>
										<?php  
											$i=1;
											while($i!=11)
											{
												$value=$year-$i;
												$i++;
										?>
										<option value="<?php echo $value; ?>-03-31"><?php  echo ($year-$i)."-".($value); ?></option>
										<?php } ?>
									</select>
									<input type="hidden" id="username" name="username" value="<?php echo $member_id; ?>"> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr. No.</th>
									<th>Action</th>
									<th>Member Id</th>
									<th>Owner Name</th>
									<th>Transaction No.</th>
									<th>Voucher No.</th>
									<th>Month</th>
									<th>Status</th>
									<th>Amt. in Rupees</th>
									<th>Payment<br>Recd. Month</th>
									<th>Transaction No.</th>
									<th>Voucher No.</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(!empty($member_passbook)):
										$x=1;
										foreach($member_passbook as $key => $maintance):
								?>
								<tr>
									<td><?php echo $x++; ?></td>
									<td>
										<?php if ($maintance['status']=="paid"): ?>

											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-info print_modal_btn" data-toggle="tooltip" data-original-title="Print Bill" data-href="<?php echo base_url()."maintenance_management/maintenance_generation/print_maintenance_bill"; ?>?prusername=<?php echo $maintance['user_name']; ?>&&prfromdate=<?php echo $maintance['start_day']; ?>&&prtodate=<?php echo $maintance['end_day']; ?>&&prtransactionId=<?php echo $maintance['tansaction_no']; ?>&&prstatus=<?php echo $maintance['status']; ?>">
												<i class="fa fa-print"></i>
											</button>

										<?php else: ?>

											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-info print_modal_btn" data-toggle="tooltip" data-original-title="Print Bill" data-href="<?php echo base_url()."maintenance_management/maintenance_generation/print_maintenance_bill"; ?>?prusername=<?php echo $maintance['user_name']; ?>&&prfromdate=<?php echo $maintance['start_day']; ?>&&prtodate=<?php echo $maintance['end_day']; ?>&&prtransactionId=<?php echo $maintance['tansaction_no']; ?>&&prstatus=<?php echo $maintance['status']; ?>">
												<i class="fa fa-print"></i>
											</button>

											<?php if($maintance['start_day']==date("Y-m-01")):  ?>

												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning edit_maint_btn" data-href="<?php echo base_url()."maintenance_management/maintenance_generation/edit_user_passbook"; ?>?username=<?php echo $maintance['user_name']; ?>&&fromdate=<?php echo $maintance['start_day']; ?>&&todate=<?php echo $maintance['end_day']; ?>&&transactionId=<?php echo $maintance['tansaction_no']; ?>" data-toggle="tooltip" data-original-title="Edit">
													<i class="fa fa-edit"></i>
												</button>

											<?php endif; ?>

										<?php endif; ?>
									</td>
									<td><?php echo $maintance['user_name']; ?></td>
									<td><?php echo $maintance['s-r-fname']." ".$maintance['s-r-lname']; ?></td>
									<td><?php echo $maintance['tansaction_no']; ?></td>
									<td><?php echo $maintance['voucher_name']; ?></td>
									<td><?php echo date("F",strtotime($maintance['start_day'])); ?></td>
									<td><?php echo $maintance['status']; ?></td>
									<td><?php echo $maintance['amount']; ?></td>
									<td><?php echo $maintance['Pay_month']; ?></td>
									<td><?php echo $maintance['tansaction_no1']; ?></td>
									<td><?php echo $maintance['voucher_no1']; ?></td>
								</tr>
								<?php
										endforeach;
									endif;
								?>
							</tbody>
							<tfoot>
								<tr>
									<th>Sr. No.</th>
									<th>Action</th>
									<th>Member Id</th>
									<th>Owner Name</th>
									<th>Transaction No.</th>
									<th>Voucher No.</th>
									<th>Month</th>
									<th>Status</th>
									<th>Amt. in Rupees</th>
									<th>Payment<br>Recd. Month</th>
									<th>Transaction No.</th>
									<th>Voucher No.</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

	</div>
	<!-- /.row -->
	<!-- END tabs -->

	<div id="passbook_modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog passbook_dialog" style="max-width:1000px;">
			
		</div>
	</div>

	<div id="edit_maint_modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog edit_maint_dialog" style="max-width:1000px;">
			
		</div>
	</div>

	<script>

		$(document).ready(function(){
			$('.print_modal_btn').on('click',function(){

				var printMaintdataURL = $(this).attr('data-href');

				 $.ajax({
		            type: 'get',
		            url: printMaintdataURL,
		            data: '',
		            dataType:'html', 
		            success: function (data) {
		              $('#passbook_modal').modal({show:true});
		              $('#passbook_modal .passbook_dialog').html(data);
		            }
		          });

				// $('.roshan').load(dataURL,function(){
				// 	$('#passbook_modal').modal({show:true});
				// });
			});

			$('.edit_maint_btn').on('click',function(){

				var editManitdataURL = $(this).attr('data-href');

				 $.ajax({
		            type: 'get',
		            url: editManitdataURL,
		            data: '',
		            dataType:'html', 
		            success: function (data) {
		              $('#edit_maint_modal').modal({show:true});
		              $('#edit_maint_modal .edit_maint_dialog').html(data);
		            }
		          });

				// $('.roshan').load(dataURL,function(){
				// 	$('#passbook_modal').modal({show:true});
				// });
			});
		});



	</script>