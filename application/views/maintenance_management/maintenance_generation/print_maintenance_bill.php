<div class="modal-content ">
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel" style="margin-left: 425px;">Maintenance Bill</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
		<div class="row" >
			<div class="col-md-12" id="div1" >
				<div class="row" >
					<div class="col-md-12" >
						<center style="font-family:Helvetica;">
							<font ><b><?php echo $bill_fetch['society_details']['soc_name']; ?></b></font><br>
							<font style="font-size: 12px;"><?php echo $bill_fetch['society_details']['soc_address']; ?>,</font><br>
							<font style="font-size: 12px;"><?php echo $bill_fetch['society_details']['soc_register_no']; ?></font>
						</center>
						<br>
					</div>

					<table width="100%" class="table table-striped table-bordered table-hover nwetbl" id="dataTables-example">
						<thead>
							<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;"><b>Bill No</b></td>
							<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;"><b>Bill Date</b></td>
							<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;"><b>Flat No</b></td>
							<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;"><b>Due Date</b></td>
						</thead>
						<tbody>
							<tr>
								<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['tansaction_no1'];?>
								</td>
								<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['bill_dt']; ?>
								</td>
								<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['flatNo'];?>
								</td>
								<td align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['duedate']; //echo date('d-m-Y', mktime(0,0,0,date('m'),date('t')-15,date('Y')))?>
								</td>
							</tr>
						</tbody>
					</table>

					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<tbody>
							<tr>
								<td colspan="10" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;border-top-style: none;" >
									Period : <?php echo $bill_fetch['start_day']; ?> To <?php echo $bill_fetch['end_day']; ?> 
								</td>
							</tr>
							<tr>
								<td colspan="10" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									Name : Mr/Mrs <?php echo $bill_fetch['s-r-fname'];?>  <?php echo $bill_fetch['s-r-lname'];?>
								</td>
							</tr>
						</tbody>
					</table>

					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<tbody>
							<tr class="vendorListHeading">
								<td colspan="7" style="background: #2196F3; color: #000;font-size:12px;border-top-style: none;" class="vendorListHeading1" align="center" >
									Details/Particular
								</td>
								<td colspan="3" style="background: #2196F3; color: #000;font-size:12px;border-top-style: none;"  class="vendorListHeading1" align="center">
									Amount In Rupees
								</td>
							</tr>
							<?php 
								$marry_me=$bill_fetch['MaintenanceParticular'];
								$i=1;
								foreach($marry_me as $key => $value) 
								{
									$i++;
							?>
							<tr class="normcell">
								<td colspan="7" class="normcell" style="font-size:12px;padding: 5 5px 5 5px;" >
									<?php echo $value['Particular'];?>
								</td>
								<td colspan="3" class="normcell" align="right" style="font-size:12px;padding: 5 5px 5 5px;">
									<?php echo  $value['Charges']; ?>
								</td>
							</tr>
							<?php 
								}
							?>
							<tr class="normcell">
								<td colspan="7" align="right" style="font-family:Helvetica;height: 25px !important;background-color: #FFF;padding: 5 5px 5 5px;" class="normcell"> 
								</td>
								<td colspan="3" align="right" style="font-family:Helvetica;height: 25px !important;background-color: #FFF;padding: 5 5px 5 5px;" class="normcell">  
								</td>
							</tr>
							<tr class="normcell">
								<td colspan="7" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">Sub Total</td>
								<td colspan="3" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['MaintenanceSubTotal']; ?>
								</td>
							</tr>
							<tr class="normcell">
								<td colspan="7" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									Principal Arrears
								</td>
								<td colspan="3" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['principal_arrears']; ?>
								</td>
							</tr>
							<tr class="normcell">
								<td colspan="7" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									Interest Arrears ( <?php echo $bill_fetch['intrest'];?>% p.a.)
								</td>
								<td colspan="3" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;"><?php echo $bill_fetch['intrestAmount'];?></td>
							</tr>
							<tr class="normcell">
								<td colspan="7" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									Total Arrears 
								</td>
								<td colspan="3" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									<?php echo $bill_fetch['totalArrears']; ?>
								</td>
							</tr>
							<tr class="normcell">
								<td colspan="4" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									<b>Advance Payment : <?php echo $bill_fetch['MaintenanceAdvance'];?></b>
								</td>
								<td colspan="3" align="right" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;">
									<b>Amount Payble</b>
								</td>
								<td colspan="3" align="right" class="normcell" style="padding: 5 5px 5 5px;font-size: 12px;border-bottom-style: none;">
									<b><?php echo $bill_fetch['MaintenanceGrandTotal']; ?></b>
								</td>
							</tr>
							<tr rowspan="2" class="normcell">
								<td colspan="7" align="left" class="normcell" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;border-right-style: none;">
									<b>Amount In Words:-<br><?php echo $bill_fetch['inwords']; ?></b>
								</td>
								<td colspan="3" style="font-family:Helvetica;padding: 5 5px 5 5px;font-size: 12px;border-left-style: none;">
								</td>
							</tr>
						</tbody>
					</table>
					<br>
					<p style="font-family:Helvetica; font-size: 8px;">
						<strong>NOTES :-</strong><br>
						<?php echo  nl2br($bill_fetch['society_details']['bill_note']); ?>
					</P>
					<?php 
						if($i>14)
						{
					?>
						<div class="page-break"></div> <br>
					<?php 
						}
					?>
					<table width="100%" class="table table-striped table-bordered table-hover nwetbl" id="dataTables-example">
						<thead>
							<td colspan="4" align="center" style="padding: 5 5px 5 5px;font-size: 16px;">
								<b>R E C E I P T</b>
							</td>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<b>Receipt No: trn_2018-0215</b>
								</td>
								<td colspan="2" align="center" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<b>Receipt Date: 14 Mar 2019</b>
								</td>
							</tr>
							<tr rowspan="3">
								<td colspan="4" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									Received with thanks from Sreeraj Nair Flat No. A-102 a sum of Rs. 9768/- (Nine Thousand Seven Hundred Sixty Eight Only) against previous Bill No. Trn_2018-0152 for the period January to March.
									<br>Ch.No.849933 on 16-2-2018.
								</td>
							</tr>
							<tr rowspan="2">
								<td colspan="2" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									Rs. 5125/-<br>Payment by Cheque.</td>
								<td colspan="2" align="right" style="font-size:12px;padding: 5 5px 5 5px;font-size: 12px;">
									<br>Authorised Signature
								</td>
							</tr>
						</tbody>
					</table>

					<center> 
						<p style="font-family:Helvetica; font-size: 12px;">This is Computer generated invoice. no need of Signature.</p>
					</center>
				</div>
				<div class="page-break"></div>
				<div class="row">
					<div class="col-md-12">
			            <center>      
			            	<!-- <button  onClick="PrintElem('#div1');" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background: linear-gradient(60deg, #004D40, #33691E); color: #fff;">print</button> -->
			            	<button class="waves-effect btn btn-social-icon btn-info" data-toggle="tooltip" data-original-title="Print" onclick="PrintElem('#div1');" >
								<i class="fa fa-print"></i>
							</button>
			 			</center>  
			 		</div>
				</div>
			</div>
		</div>
    </div>
</div>

<script language="javascript" type="text/javascript">

	function PrintElem(elem)
	{
	    Popup($('<div/>').append($(elem).clone()).html());
	}

	function Popup(data) 
	{
	    var mywindow = window.open('', '', 'height=400,width=600');
	    mywindow.document.write('<html><head>');
	    mywindow.document.write('<link rel="stylesheet" href="http://www.dynamicdrive.com/ddincludes/mainstyle.css" type="text/css" />');
	     
	    mywindow.document.write('<style type="text/css">' +
	    'table {' +
	    'border-collapse: collapse;font-family:Helvetica;' +
	    
	    '}' + 
	    'table th, table td{' +
	    'border: 1px solid black;font-family:Helvetica;' +
	    
	    '}' + 
	    ' @page {'+
	    ' size: A4; '+' }'+
	    ' @media print {'+
	    '.page-break{' +
	    'display: none;' +
	    
	    '}' + 
	    '.page-break	{' +
	    'display: block; page-break-before: always;' +
	    
	    '}' +
	    '  td.vendorListHeading1 {'+
	    ' background-color: #2196F3 !important;color: #FFFFFF !important;-webkit-print-color-adjust: exact;align:center;font-family:Helvetica;padding: 10 10px 10 10px;'+' }'+' }'+
	    
	    '  tr.vendorListHeading {'+
	    ' background-color: #2196F3 !important;color: #FFFFFF !important;-webkit-print-color-adjust: exact;font-family:Helvetica;'+' }'+' }'+
	    '</style>');

	    mywindow.document.write('</head><body >');
	    mywindow.document.write(data);
	    mywindow.document.write('</body></html>');

	    mywindow.print();
	  	// mywindow.close();
	    return true;
	}

	function printDiv(divID) 
	{
	            //Get the HTML of div
	    var divElements = document.getElementById(divID).innerHTML;
	    //Get the HTML of whole page
	    var oldPage = document.body.innerHTML;
	    
	    //Reset the page's HTML with div's HTML only
	    document.body.innerHTML =
	            '' +
	        '<style type="text/css">' +
	        'table th, table td{' +
	        'border:1px solid #000;' +
	        
	        '}'   +
	        'table th {' +
	        'background: #2196F3; color: #000;' +
	        
	        '}' + 
	        ' @page {'+
	        ' size: auto;  margin: 0mm;'+' }'+
	        '</style>'+
	              "<html><head><title></title></head><body>" +
	      divElements + "</body>";

	    //Print Page
	    window.print();

	    //Restore orignal HTML
	    document.body.innerHTML = oldPage;
	}
</script>s