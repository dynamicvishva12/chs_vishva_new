		<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-6">
				  			<h3 class="box-title">Maintenance Plan</h3>
				  		</div>
				  		<div class="col-6 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addHead">Add Maintenance Head</button>
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#fixedMain">Fixed Maintenance</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr No</th>
								<?php 
									if(!empty($headers_view)):
										foreach($headers_view as $key => $data):
								?>
								<th><?php echo $data['head_name']; ?></th>
								<?php 
										endforeach;
									endif;
								?>
								<th>From date </th>
								<th>To date </th>
								<th>Active </th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$x=1;
								$fromdate="";
								$to_date=""; 
								$flag=""; 
								if(!empty($headers_charges)):
									foreach($headers_charges as $key => $maintdata):
							?>
							<tr>
								<td><?php echo $x++; ?></td>
								<?php
									if(!empty($maintdata)):
										foreach($maintdata as $key => $maint_data):

											$fromdate=$maint_data['from_date'];
											$to_date=$maint_data['to_date']; 
											$flag=$maint_data['flag']; 
								?>                                                            
								<td><?php echo $maint_data['charges']; ?></td>
								<?php 
										endforeach;
									endif;
								?>
								<td><?php echo $fromdate; ?></td>
								<td><?php echo $to_date; ?></td>
								<td>
								<?php 
									if($flag=="Yes")
										echo "Active";
									else
										echo "Inactive";
								?>
								</td>
							</tr>
							<?php 
									endforeach;
								endif;
							?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr No</th>
								<?php 
									if(!empty($headers_view)):
										foreach($headers_view as $key => $data):
								?>
								<th><?php echo $data['head_name']; ?></th>
								<?php 
										endforeach;
									endif;
								?>
								<th>From date </th>
								<th>To date </th>
								<th>Active </th>
							</tr>
						</tfoot>
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->


<!-- Popup Model Plase Here -->
<div id="addHead" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="<?php echo base_url()."maintenance_management/maintenance_plan/add_maintenance_head"; ?>" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Maintenance Head</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group <?php if(form_error('Headname')!=""): ?>has-error<?php endif; ?>">
						<label class="col-md-12">Maintenance Head Name:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<input type="text" name="Headname"  class="form-control" placeholder="Maintenance Head Name" value="<?php echo set_value('Headname'); ?>">
							<?php if(form_error('Headname')!=""): ?>
							<label class="text-danger" ><?php echo form_error('Headname'); ?></label>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group <?php if(form_error('Headcharges')!=""): ?>has-error<?php endif; ?>">
						<label class="col-md-12">Charges:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<input type="text" name="Headcharges" class="form-control" placeholder="Charges" value="<?php echo set_value('Headcharges'); ?>">
							<?php if(form_error('Headcharges')!=""): ?>
							<label class="text-danger" ><?php echo form_error('Headcharges'); ?></label>
							<?php endif; ?>
						</div>
					</div>
				  	<div class="form-group <?php if(form_error('maint_type')!=""): ?>has-error<?php endif; ?>">
						<label class="col-md-12">Maintenance Head Type:<span class="text-danger">*</span></label>
						<select class="form-control select2" name="maint_type" style="width: 100%;">
						  	<option value="">Select Maintenance Type</option>
							<option value="Fixed" <?php echo set_select('maint_type', "Fixed"); ?>>Fixed Maintenance</option>
							<option value="Not fixed" <?php echo set_select('maint_type', "Not fixed"); ?>>Rate Maintenance</option>
						</select>
						<?php if(form_error('maint_type')!=""): ?>
						<label class="text-danger" ><?php echo form_error('maint_type'); ?></label>
						<?php endif; ?>
				  	</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>


<div id="fixedMain" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="max-width: 800px">
		<form action="<?php echo base_url()."maintenance_management/maintenance_plan/save_maintenance"; ?>" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Fixed Maintenance</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<?php 
							if(!empty($headers_view)):
								foreach($headers_view as $key => $data):
						?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-12"><?php echo $data['head_name']; ?></label>
								<div class="col-md-12">
									<input type="text" name="<?php echo $data['maint_headId']; ?>" value="0" class="form-control" value="0" pattern="[0-9]+" title="You can insert only digits" >
								</div>
							</div>
						</div>
						<?php
								endforeach;
							endif;
						?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#addHead').modal('show');

		<?php endif; ?>
	});

</script>