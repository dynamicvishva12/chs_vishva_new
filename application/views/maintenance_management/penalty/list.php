		<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h3 class="box-title">Penalty</h3>
				  		</div>
				  		<div class="col-2 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addPenalty">Add Penalty</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<?php 
			                        if(!empty($data))
			                        {
			                            foreach($data as $key=> $name)
			                            {
			                                if ($key=='from_date') 
			                                {
			                                    $key="Changed On Date";
			                                }
			                    ?>
			                    	<th><?php echo $key; ?></th>
			                    <?php 
			                            }
			                        }
			                    ?>
							</tr>
						</thead>
						<tbody>
							<?php 
		                        $x=1;
		                        if(!empty($result))
		                        {
		                            foreach($result AS $maintdata)
		                            {
		                    ?>
		                    <tr>
		                        <td><?php echo $x++;?></td>
		                        <?php
		                            $val="";
		                                foreach($maintdata as $col=> $val)
		                                {
		                                    if($col=='from_date') 
		                                    {
		                                        $val=date("d-m-Y",strtotime($val));
		                                    }
		                        ?>
		                            <td>
		                                <?php
		                                    if($col=='from_date') 
		                                    {
		                                        echo $val;
		                                    }
		                                    else
		                                    {
		                                        echo $val." %";
		                                    } 
		                                ?>
		                            </td>
		                            <?php } ?>
		                        </tr>
		                        <?php } ?>
		                    <?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr. No.</th>
								<?php 
			                        if(!empty($data))
			                        {
			                            foreach($data as $key=> $name)
			                            {
			                                if ($key=='from_date') 
			                                {
			                                    $key="Changed On Date";
			                                }
			                    ?>
			                    	<th><?php echo $key; ?></th>
			                    <?php 
			                            }
			                        }
			                    ?>
							</tr>
						</tfoot>
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->

<div id="addPenalty" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="<?php echo base_url()."maintenance_management/penalty/save"; ?>" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Penalty</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-12">Penalty %</label>
									<div class="col-md-12">
										<input type="text" name="Penalty" class="form-control" placeholder="Penalty %">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-12">Penalty Stage</label>
									<select class="form-control select2"  name="type" style="width: 100%;">
									   	<option value="Daily">Daily</option>
										<option value="Weekly">Weekly</option>
										<option value="Monthly">Monthly</option>
										<option value="Half">Half Yearly</option>
										<option value="Yearly">Yearly</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-12">Select Maintenance Head</label>
							<div class="col-md-12">
								<input type="checkbox" id="ckbCheckAll" />
								<label for="ckbCheckAll">Select All</label>
							</div>
						</div>

						<hr>

						<div class="row">
							<?php 
								$i=0;
		                        if(!empty($data)):
		                            foreach($data as $key=> $name):
		                    ?>
								<div class="col-md-4">
									<div class="form-group">
										<input type="checkbox" class="checkBoxClass" id="basic_checkbox_<?php echo $i; ?>" name="<?php echo $key; ?>" value="<?php echo $key; ?>" />
										<label for="basic_checkbox_<?php echo $i; ?>"><?php echo $key; ?></label>
									</div>
								</div>
							<?php
									$i++;
									endforeach;
								endif;
							?>
						</div>

						
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>


<script>
    $(document).ready(function () {

        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

    });
</script>