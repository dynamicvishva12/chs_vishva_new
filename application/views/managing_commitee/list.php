			<div class="box-header no-border">
		 		<div class="row">
					<div class="col-8">
			  			
			  		</div>
			  		<div class="col-4 text-right">
			  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addMember">Add Committee member</button>
			  		</div>
				</div>
		 	</div>

			<div class="row">
				<?php  
                    $x=1;
                    if(!empty($managing_commitee_list)):
                        foreach($managing_commitee_list AS $e_mg):
                ?>
					  <div class="col-12 col-lg-4">
						<div class="box ribbon-box">
						  <div class="ribbon-two ribbon-two-primary">
						  	<span>
						  		<?php echo ucwords($e_mg['directoryDesignation']); ?>
						  	</span>
						  </div>
						  <div class="box-header no-border p-0">				
							<a href="#">
								<?php 
                                    $exp_img=explode('/', $e_mg['addimg']);
                                    $user_img=end($exp_img);
                                    if($user_img==""): 
                                ?>
                                    <img class="img-fluid" src="<?php echo $assets_url; ?>tmp_images/avatar/375x200/1.jpg" alt="" >
                                <?php else: ?>
                                    <img class="img-fluid" src="<?php echo $e_mg['addimg']; ?>" alt="" >
                                <?php endif; ?>
							</a>
						  </div>
						  <div class="box-body">
								<div class="user-contact list-inline text-center">
									<a href="tel:<?php echo $e_mg['directoryMobile']; ?>" class="btn btn-circle mb-5 btn-primary" data-toggle="tooltip" data-original-title="Call">
										<i class="fa fa-phone"></i>
									</a>

									<a href="mailto:<?php echo $e_mg['directoryMail']; ?>" class="btn btn-circle mb-5 btn-warning" data-toggle="tooltip" data-original-title="Mail">
										<i class="fa fa-envelope"></i>
									</a>

									<?php if($e_mg['directoryDesignation']=="committee members"): ?>                 

	                                    <a href="#" class="btn btn-circle mb-5 btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."managing_commitee/delete_commitee/".$e_mg['directoryId']; ?>" data-conf="Do you want to delete this member ?" data-accept="Delete" data-btn_text="Yes, delete it" data-accept_res="Your request will be processed accordingly..." data-cancel_res="Your request has been cancelled...">
											<i class="fa fa-trash-o"></i>
										</a>

	                                <?php else: ?>  

	                                    <?php if($e_mg['directoryDesignation']==$sess_designation): ?>  

	                                        <button class="btn btn-circle mb-5 btn-success" data-original-title="Swap" data-toggle="modal" data-target="#userModal">
												<i class="fa fa-exchange"></i>
											</button>
	                                    <?php endif; ?>

	                                <?php endif; ?>
									

													
								</div>
							  <div class="text-center">
								<h3 class="my-10 text-dark"><a href="#"><?php echo ucwords($e_mg['directoryName']); ?></a></h3>
								<h6 class="user-info mt-0 mb-10 text-dark"><?php echo strtoupper($e_mg['directoryDesignation']); ?></h6>
								<p class="text-dark w-p85 mx-auto"><?php echo $e_mg['directoryMobile']; ?><br><?php echo $e_mg['directoryMail']; ?></p>
							  </div>
						  </div>
						</div>
					  </div>

				    <?php endforeach; ?>
                <?php endif; ?>	  
			  
			</div>


<!-- Popup Model Plase Here -->
<div id="addMember" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url(); ?>managing_commitee/add_managing" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Committee Member	</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label>Select Member</label>
						<select class="form-control select2" id="name" name="name" style="width: 100%;">
						<?php   
                            foreach($member_data as $key1 => $value1):
                                if(($value1['usrId']!=$sess_user_name) && empty($value1['designation'])):
                        ?>
                            <option value="<?php echo $value1['usrId'];?>">
                                <?php echo "<tr><td>".$value1['usrFname']."</td><td>".$value1['usrLname']."</td><td>(".$value1['usrId'].")</td><td>";?>
                            </option>
                        <?php
                                endif;
                            endforeach;
                        ?>
						</select>
				  	</div>
				  	<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="post">
						  <option value="committee members">Other Commitee Member </option>
						</select>
				  	</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" >Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>


<!-- Popup Model Plase Here -->
<div id="userModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal"  action="<?php echo base_url(); ?>managing_commitee/add_managing" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Swap Committee Member	</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label>Select Member</label>
						<select class="form-control select2" id="name" name="name" style="width: 100%;">
						<?php   
                            $i=1;
                            foreach($member_data as $key1 => $value1):
                                if(($value1['usrId']!=$sess_user_name) && empty($value1['designation'])):
                        ?>
                            <option value="<?php echo $value1['usrId'];?>">
                                <?php echo "<tr><td>".$value1['usrFname']."</td><td>".$value1['usrLname']."</td><td>(".$value1['usrId'].")</td><td>"; ?>
                            </option>
                        <?php
                                endif;
                                $i++;
                            endforeach;
                        ?>
						</select>
				  	</div>
				  	<div class="form-group">
						<select class="form-control select2" style="width: 100%;" name="post">
						  <option value="<?php echo $sess_designation; ?>"><?php echo $sess_designation; ?></option>
						</select>
				  	</div>
				</div>
				<div class="modal-footer">
					<input value="<?php echo $sess_designation_id; ?>" type="hidden" name="oldId">
					<button type="submit" class="btn btn-success float-right" >Swap</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>