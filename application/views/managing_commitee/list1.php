    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
                    <h4 class="title">Management Committee</h4>
                </div>
                <div class="card-content table-responsive">
                    <table id="table_id" class="display">
                        <thead class="text-primary">
                            <tr>
                                <th>Sr no</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Contact</th>
                                <th>E-mail Id</th>
                                <th>M C Member Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                                $x=1;
                                if(!empty($managing_commitee_list)):
                                    foreach($managing_commitee_list AS $e_mg):
                            ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $x++;?></td>
                                        <td><?php echo $e_mg['directoryName']; ?></td>
                                        <td><?php echo $e_mg['directoryDesignation']; ?></td>
                                        <td><?php echo $e_mg['directoryMobile']; ?></td>
                                        <td><?php echo $e_mg['directoryMail']; ?></td>
                                        <td>
                                            <?php 
                                                $exp_img=explode('/', $e_mg['addimg']);
                                                $user_img=end($exp_img);
                                                if($user_img==""): 
                                            ?>
                                                <img class="img-thumbnail img-responsive" src="<?php echo $assets_url; ?>IMAGES/person.png" style="height: 60px;width: 60px;" >
                                            <?php else: ?>
                                                <a href="<?php echo $e_mg['addimg']; ?>">
                                                    <img class="img-thumbnail img-responsive" src="<?php echo $e_mg['addimg']; ?>" style="height: 60px;width: 60px;" />
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($e_mg['directoryDesignation']=="committee members"): ?>                                
                                                <center>
                                                    <a href="<?php echo base_url()."managing_commitee/delete_commitee/".$e_mg['directoryId']; ?>" class="confirmation">
                                                        <i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
                                                    </a>
                                                </center>
                                            <?php else: ?>  
                                                <?php if($e_mg['directoryDesignation']==$sess_designation): ?>  
                                                    <center>
                                                        <a href="#id04"><i class="material-icons" style="color: #212121; font-size: 40px;" >compare_arrows</i></a>
                                                    </center>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                    <tr>
                                        <td colspan=7><center>No Records</center></td>
                                    </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <a href="#id03">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="position: fixed;top: 85%; right: 5%;  background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
            <i class="material-icons">add</i>
        </button>
    </a>

    <div id="id03" class="modal-rosh">
        <div class="modal-dialog-rosh" >
            <div class="modal-content-rosh" style="width: 500px;">
                <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
                    <a href="#" class="closebtn">×</a>
                    <center><h2 style="margin-top: 5px;">COMMITEE MEMBER</h2></center>
                </header>
                <div class="container-fluid rosh" style="padding-top: 20px;">
                    <form class="" action="<?php echo base_url(); ?>managing_commitee/add_commitee" method="post" enctype="multipart/form-data">
                        <select style="width: 100%; height: 40px;" id="name" name="name">
                            <?php   
                                foreach($member_data as $key1 => $value1):
                                    if(($value1['usrId']!=$sess_user_name) && empty($value1['designation'])):
                            ?>
                                <option value="<?php echo $value1['usrId'];?>">
                                    <?php echo $value1['usrFname']." ".$value1['usrLname']." (".$value1['usrId'].")"; ?>
                                </option>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </select>
                        <select style="width: 100%; height: 40px;" name="post">
                            <option value="committee members">other commitee member </option>
                        </select>
                        <br>
                        <center>
                            <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " type="submit" name="committeeadd" value="Submit" style="width: 200px; background:#212121; ">
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="id04" class="modal-rosh">
        <div class="modal-dialog-rosh" >
            <div class="modal-content-rosh" style="width: 500px;">
                <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
                    <a href="#" class="closebtn">×</a>
                    <center><h2 style="margin-top: 5px;">COMMITEE MEMBER</h2></center>
                </header>
                <div class="container-fluid rosh" style="padding-top: 20px;">
                    <form class="" action="<?php echo base_url(); ?>managing_commitee/add_managing" method="post" enctype="multipart/form-data">
                        <input value="<?php echo $sess_designation_id; ?>" type="text" name="oldId">

                        <select style="width: 100%; height: 40px;" id="name" name="name">
                        <?php   
                            $i=1;
                            foreach($member_data as $key1 => $value1):
                                if(($value1['usrId']!=$sess_user_name) && empty($value1['designation'])):
                        ?>
                            <option value="<?php echo $value1['usrId'];?>">
                                <?php echo "<tr><td>".$value1['usrFname']."</td><td>".$value1['usrLname']."</td><td>(".$value1['usrId'].")</td><td>"; ?>
                            </option>
                        <?php
                                endif;
                                $i++;
                            endforeach;
                        ?>

                        </select>
                        <select style="width: 100%; height: 40px;" name="post">
                            <option value="<?php echo $sess_designation; ?>"><?php echo $sess_designation; ?></option>
                        </select>
                        <br>
                        <center>
                            <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" name="managingadd" value="Submit" style="width: 200px; background:#212121; ">
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 


    <center>
        <div id="id30" class="modal-rosh">
            <div class="modal-dialog-rosh" style="width: 500px;">
                <div class="modal-content-rosh" style="margin-top: 100px;">
                    <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px; padding-top: 0px; padding-bottom: 10px;"> 
                        <button  style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>       
                        <center>
                            <h2 style="margin-top: 0px; font-size: 20px;">Confirm Delete</h2>
                        </center>
                    </header>
                    <div class="container-fluid rosh roshan" style="padding-top: 20px; padding-bottom: 20px;"></div>
                </div>
            </div>
        </div>
    </center>

<script>
  
   $(document).ready(function(){
        $('.openPopup').on('click',function(){

            var dataURL = $(this).attr('data-href');

            console.log(dataURL);
            $('.roshan').load(dataURL,function(){
                $('#id30').modal({show:true});
            });
            
        });
    });
</script>
    
<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>