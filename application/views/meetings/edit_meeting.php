<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
	  		<div class="box-header with-border">
		 		<div class="row">
					<div class="col-10">
			  			<h3 class="box-title">Edit Meeting</h3>
			  		</div>
			  		<div class="col-2 text-right">
			  		</div>
				</div>
		 	</div>
			<!-- /.box-header -->
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<!-- Multiple Radios -->
									<div class="form-group row">
										<label class="col-md-2 control-label">Meeting Topic:<span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input type="text" name="meet1" id="header" class="form-control" placeholder="Meeting Topic" value="<?php echo set_value('meet1', $meeting_data['meetingTitle']); ?>">
											<?php if(form_error('meet1')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet1'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label">Meeting Agenda:<span class="text-danger">*</span></label>
										<div class="col-md-10">
											<textarea class="form-control" name="meet2" id="subheader" rows="8" placeholder=""><?php echo set_value('meet2', $meeting_data['meetingDescr']); ?></textarea>
											<?php if(form_error('meet2')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet2'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label">Meeting Date:<span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input type="date" class="form-control" name="meet3" id="st-date"  placeholder="Meeting Date" value="<?php echo set_value('meet3', date("Y-m-d", strtotime($meeting_data['meeting_date']))); ?>">
											<?php if(form_error('meet3')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet3'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label">Meeting Time:<span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input type="time" name="meet_time" class="form-control" placeholder="Meeting Time" value="<?php echo set_value('meet_time', date('H:i', strtotime($meeting_data['meeting_time']))); ?>">
											<?php if(form_error('meet_time')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet_time'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 text-right">
											<!-- <button type="submit" id="singlebutton" name="editnotice" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  style="background: #212121; color: #fff; ">post</button> -->
											<input type="hidden" name="meetId" id="meetId" value="<?php echo $meeting_data['meetingId']; ?>" required>
											<button type="submit" name="editnotice" class="btn btn-success text-center" >Submit</button>

										</div>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

