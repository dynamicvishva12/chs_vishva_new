<div class="row">
	<div class="col-12">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
						<h4 class="box-title">Meetings</h4>
					</div>
					<div class="col-2 text-right">
						<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#myModal">
							Create Meeting
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="box">
						<div class="table-responsive">
							<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<!--display responsive nowrap    this for responsive datatables-->
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Action</th>
										<th>Available</th>
										<th>Not Available</th>
										<th>Meeting Title</th>
										<th>Meeting Agenda</th>
										<th>Meeting Time</th>
									</tr>
								</thead>
								<tbody>
									<?php
										if(!empty($meeting_data)):
											$i=1;
											foreach($meeting_data as $key => $row):
												$head=$row['meetingId'];
									?>
									<tr>
										<td><?php echo $i; ?></td>
										<td>
											<a href="<?php echo base_url()."meetings/meeting_result/".$head."/Available"; ?>">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="View">
													<i class="fa fa-eye"></i>
												</button>
											</a>

											<a href="<?php echo base_url()."meetings/edit_meeting/".$head; ?>">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
											</a>

											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url()."meetings/delete_meeting/".$head; ?>" id="sa-params" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
										</td>
										<td><?php echo $row['Available']; ?></td>
										<td><?php echo $row['Notavailable']; ?></td>
										<td>
											<?php echo $row['meetingTitle']; ?>
										</td>
										<td>
											<?php echo $row['meetingDescr']; ?>
										</td>
										<td><?php echo date('H:i A', strtotime($row['meeting_time'])); ?></td>
									</tr>
									<?php
											$i++;
											endforeach;
										endif;
									?>
								</tbody>				  
								<tfoot>
									<tr>
										<th>Sr No</th>
										<th>Action</th>
										<th>Available</th>
										<th>Not Available</th>
										<th>Meeting Title</th>
										<th>Meeting Agenda</th>
										<th>Meeting Time</th>
									</tr>
								</tfoot>
							</table>
						</div>              
					</div>
					<!-- /.box-body -->
				</div>
					<!-- /.box --> 
				<!-- Popup Model Plase Here -->
				<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<form action="<?php echo base_url()."meetings/add_meeting"; ?>" class="form-horizontal" method="POST">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Add Meeting</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label class="col-md-12">Meeting Topic:<span class="text-danger">*</span></label>
										<div class="col-md-12">
											<input type="text" name="meet1" id="header" class="form-control" placeholder="Meeting Topic" value="<?php echo set_value('meet1'); ?>">
											<?php if(form_error('meet1')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet1'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">Meeting Agenda:<span class="text-danger">*</span></label>
										<div class="col-md-12">
											<textarea class="form-control" name="meet2" id="subheader" rows="8" placeholder=""><?php echo set_value('meet2'); ?></textarea>
											<?php if(form_error('meet2')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet2'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">Meeting Date:<span class="text-danger">*</span></label>
										<div class="col-md-12">
											<input type="date" class="form-control" name="meet3" id="st-date" value="<?php echo set_value('meet3', date("Y-m-d")); ?>" placeholder="Meeting Date">
											<?php if(form_error('meet3')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet3'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-12">Meeting Time:<span class="text-danger">*</span></label>
										<div class="col-md-12">
											<input type="time" name="meet_time" class="form-control" placeholder="Meeting Time" value="<?php echo set_value('meet_time'); ?>">
											<?php if(form_error('meet_time')!=""): ?>
											<label class="text-danger"><?php echo form_error('meet_time'); ?></label>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-success float-right">Add</button>
									<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
</div>


<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#myModal').modal('show');

		<?php endif; ?>
	});

</script>