<div class="row">
	<div class="col-12">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
						<h4 class="box-title">Attending Member's</h4>
					</div>
					<div class="col-2 text-right">
					</div>
				</div>
				<div class="box-body">
					<div class="box">
						<div class="table-responsive">
							<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<!--display responsive nowrap    this for responsive datatables-->
								<thead>
									<tr>
										<th>SR.No</th>
						                <th>Member's Name</th>
						                <th>Sign</th>
									</tr>
								</thead>
								<tbody>
									<?php
						                $srno=1;
						                if(!empty($meeting_attending)):
						                    foreach($meeting_attending as $key => $getvalue):
						            ?>
						            <tr>
						                <td><?php echo $srno; ?></td>
						                <td><?php echo $getvalue['fname']." ".$getvalue['lname']; ?></td>
						                <td></td>
						            </tr>
						            <?php
						                        $srno++;
						                    endforeach;
						                endif;
						            ?>
								</tbody>				  
								<tfoot>
									<tr>
										<th>SR.No</th>
						                <th>Member's Name</th>
						                <th>Sign</th>
									</tr>
								</tfoot>
							</table>
						</div>              
					</div>
					<!-- /.box-body -->
				</div>
					<!-- /.box --> 
				
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
</div>