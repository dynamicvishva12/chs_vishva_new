<div class="row">
<div class="col-12">
	<div class="box">
		<div class="box-header with-border">
			<div class="row">
				<div class="col-10">
		  			<h4 class="box-title">Active Members</h4>
		  		</div>
		  		<div class="col-2 text-right">
		  			<button class="waves-effect btn btn-social-icon btn-info" data-toggle="tooltip" data-original-title="Print" onclick="printContent('div1')">
						<i class="fa fa-print"></i>
					</button>
		  		</div>
			</div>
		</div>
		<div class="box-body">
		  	<div class="box">
				<div class="table-responsive" id="div1">
			  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
			  				
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Building Name</th>
								<th>Wing</th>
								<th>Flat No.</th>
								<th>Mobile No.</th>
								<th>Reason</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							$sr_no=1;
							if(!empty($member_data)):
								foreach($member_data AS $user):
						?>
							<tr>
								<td><?php echo $sr_no++; ?></td>
								<td>
									<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning confirm_it" data-toggle="tooltip" data-original-title="Inactive" data-link="<?php echo base_url()."member_management/active_member/change_status"; ?>?usrMail=<?php echo $user['usrMail']; ?>&memId=<?php echo $user['usrId']; ?>&memstatus=inactive&url=<?php echo $_SERVER['PHP_SELF']; ?>" data-conf="Do you want to inactive ?" data-accept="Inactive" data-btn_text="Yes, inactive it" data-accept_res="Your request will be approve shortly..." data-cancel_res="Your request has been cancelled...">
										<i class="fa fa-close"></i>
									</button>
									
									<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger confirm_it" data-toggle="tooltip" data-original-title="Disabled" data-link="<?php echo base_url()."member_management/active_member/change_status"; ?>?usrMail=<?php echo $user['usrMail']; ?>&memId=<?php echo $user['usrId']; ?>&memstatus=disabled&url=<?php echo $_SERVER['PHP_SELF']; ?>" data-conf="Do you want to disable ?" data-accept="Disabled" data-btn_text="Yes, disable it" data-accept_res="Your request will be approve shortly..." data-cancel_res="Your request has been cancelled...">
										<i class="fa fa-user-times"></i> 
									</button>
								</td>
								<td>
									<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['usrId']; ?>" class="openPopup">
										<?php echo ucwords($user['usrFname']." ".$user['usrLname']); ?>
									</a>
								</td>
								<td><?php echo $user['usrAppartment']; ?></td>
								<td><?php echo $user['usrWing']; ?></td>
								<td><?php echo $user['usrFlat']; ?></td>
								<td><?php echo $user['usrMobile']; ?></td>	
								<td><?php echo $user['active_reason'];?></td>
							</tr>
							<?php endforeach; ?>
						<?php else: ?>
	                        <tr>
	                            <td colspan=9><center>No Records</center></td>
	                        </tr>
	                    <?php endif; ?>       
						</tbody>			  
						<tfoot>
							<tr>
								<th>Sr No</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Building Name</th>
								<th>Wing</th>
								<th>Flat No.</th>
								<th>Mobile No.</th>
								<th>Reason</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>              
		</div>
		<!-- /.box-body -->
  	</div>
  	<!-- /.box -->
 	<!-- Popup Model Plase Here -->
	<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Notice</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-12">Notice Title</label>
							<div class="col-md-12">
								<input type="text" class="form-control" placeholder="Notice Title">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-12">Notice Expiry Date</label>
							<div class="col-md-12">
								<input type="date" class="form-control" placeholder="Notice Expiry Date">
							</div>
						</div>
						<div class="form-group">
							<label>Notice Type :</label>
									<div class="radio">
										<input name="group1" type="radio" id="option_1">
										<label for="option_1" class="mr-30">Normal</label>
										<input name="group2" type="radio" id="option_2">
										<label for="option_2" class="mr-30">Statutory</label>
									</div>
								</div>
							<div class="form-group">
								<label class="col-md-12">Notice Description</label>
								<div class="col-md-12">
								<textarea class="form-control" placeholder=""></textarea>
								</div>
							</div>
							<div class="form-group">
								  <label>Select File</label>
								  <label class="file">
									<input type="file" id="file">
								  </label>
							</div>

					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="sa-success" data-dismiss="modal">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
</div>
<!-- /.col -->
</div>

<script>

	function printContent(el)
	{
		var restorepage = document.body.innerHTML;
		var printcontent = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcontent;
		window.print();
		document.body.innerHTML = restorepage;
	}

</script>

<script>

	function goBack() 
	{
	    location.reload();
	}

</script>


<script>

	function myFunction() 
	{
	    window.print();
	}

</script>