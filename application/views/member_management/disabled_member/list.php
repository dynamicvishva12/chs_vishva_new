<div class="row">
	<div class="col-12">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
			  			<h4 class="box-title">Disabled Members</h4>
			  		</div>
			  		
				</div>
			</div>
			<div class="box-body">
			  	<div class="box">
					<div class="table-responsive">
				  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				  				
							<thead>
								<tr>
									<th>Sr No</th>
									<th>Member Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Mobile No.</th>
									<th>Email Address</th>
									<th>Reason</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$sr_no=1;
									if(!empty($member_data)):
										foreach($member_data AS $user):
								?>
								<tr>
									<td><?php echo $sr_no++; ?></td>
									<td>
										<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['s-r-username']; ?>" class="openPopup">
											<?php echo ucwords($user['s-r-fname']." ".$user['s-r-lname']); ?>
										</a>
									</td>
									<td><?php echo $user['s-r-appartment']; ?></td>
									<td><?php echo $user['s-r-wing']; ?></td>
									<td><?php echo $user['s-r-floor']; ?></td>
									<td><?php echo $user['s-r-mobile']; ?></td>
									<td><?php echo $user['s-r-email']; ?></td>
									<td><?php echo $user['disabled_reason']; ?></td>
								</tr>
									<?php endforeach; ?>
								<?php else: ?>
			                        <tr>
			                            <td colspan=8><center>No Records</center></td>
			                        </tr>
			                    <?php endif; ?>      
							</tbody>				  
							<tfoot>
								<tr>
									<th>Sr No</th>
									<th>Member Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Mobile No.</th>
									<th>Email Address</th>
									<th>Reason</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>              
			</div>
			<!-- /.box-body -->
	  	</div>
	  	<!-- /.box -->
     	<!-- Popup Model Plase Here -->
		<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Add Notice</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-md-12">Notice Title</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Notice Title">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Notice Expiry Date</label>
								<div class="col-md-12">
									<input type="date" class="form-control" placeholder="Notice Expiry Date">
								</div>
							</div>
							<div class="form-group">
								<label>Notice Type :</label>
										<div class="radio">
											<input name="group1" type="radio" id="option_1">
											<label for="option_1" class="mr-30">Normal</label>
											<input name="group2" type="radio" id="option_2">
											<label for="option_2" class="mr-30">Statutory</label>
										</div>
									</div>
								<div class="form-group">
									<label class="col-md-12">Notice Description</label>
									<div class="col-md-12">
									<textarea class="form-control" placeholder=""></textarea>
									</div>
								</div>
								<div class="form-group">
									  <label>Select File</label>
									  <label class="file">
										<input type="file" id="file">
									  </label>
								</div>

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" id="sa-success" data-dismiss="modal">Add</button>
						<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		</div>
	</div>
	<!-- /.col -->
</div>