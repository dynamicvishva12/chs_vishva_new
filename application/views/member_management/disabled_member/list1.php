<style type="text/css">
	.class66
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>


<center>
	<div id="id32" class="modal-rosh">
		<div class="modal-dialog-rosh" style="width: 1000px;">
			<div class="modal-content-rosh">
				<header class="container-fluid rosh pari"> 
					<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000;" type="button" onclick="goBack()"  >&times;</button>        
					<center><h2 style="margin-top: 5px;">Member Profile</h2></center>
				</header>
				<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
			</div>
		</div>
	</div>
</center>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
				<h4 class="title">Disabled Member</h4>
			</div>
			<div class="card-content table-responsive">
				<table class="table table-bordered" id="table_id">
					<thead class="text-primary">
						<th>Member Name</th>
						<th>Email Address</th>
						<th>Building Name</th>
						<th>Wing</th>
						<th>Floor No</th>
						<th>Mobile No</th>
						<th>Reason</th>
					</thead>
					<tbody>
						<?php 
							if(!empty($member_data)):
								foreach($member_data AS $user):
						?>
						<tr class="odd gradeX" style="width: auto;">
							<td>
								<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['s-r-username']; ?>" class="openPopup">
									<?php echo $user['s-r-fname']." ".$user['s-r-lname']; ?>
								</a>
							</td>
							<td><?php echo $user['s-r-email']; ?></td>
							<td><?php echo $user['s-r-appartment']; ?></td>
							<td><?php echo $user['s-r-wing']; ?></td>
							<td><?php echo $user['s-r-floor']; ?></td>
							<td><?php echo $user['s-r-mobile']; ?></td>
							<td><?php echo $user['disabled_reason']; ?></td>
						</tr>
							<?php endforeach; ?>
						<?php else: ?>
	                        <tr>
	                            <td colspan=8><center>No Records</center></td>
	                        </tr>
	                    <?php endif; ?>      
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


	<center>
		<div id="user_profile_modal_page" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 1200px;">
				<div class="modal-content-rosh">
					<header class="container-fluid rosh pari"> 
						<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000;" type="button" onclick="goBack()"  >&times;</button>        
						<center><h2 style="margin-top: 5px;">User Profile</h2></center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>

<script>

	$(document).ready(function(){
		$('.openPopup').on('click',function(){

			var dataURL = $(this).attr('data-href');
			
			console.log(dataURL);

			$('.roshan').load(dataURL,function(){

				$('#user_profile_modal_page').modal({show:true});

			});

		});
	});
</script>

<script>

	function goBack() 
	{
		location.reload();
	}

</script>