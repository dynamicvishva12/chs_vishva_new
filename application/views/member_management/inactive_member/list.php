<div class="row">
	<div class="col-12">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
			  			<h4 class="box-title">Inactive Members</h4>
			  		</div>
			  		<div class="col-2 text-right">
		  				<button class="waves-effect btn btn-social-icon btn-info" data-toggle="tooltip" data-original-title="Print" onclick="printContent('div1')">
							<i class="fa fa-print"></i>
						</button>
		  			</div>
				</div>
			</div>
			<div class="box-body">
			  	<div class="box">
					<div class="table-responsive" id="div1">
				  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				  				
							<thead>
								<tr>
									<th>Sr No</th>
									<th>Action</th>
									<th>Member Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Mobile No.</th>
									<th>Email Address</th>
									<th>Reason</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$sr_no=1;
									if(!empty($member_data)):
										foreach($member_data AS $user):
							?>
								<tr>
									<td><?php echo $sr_no; ?></td>
									<td>
										
										<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="modal" data-target="#userModal_<?php echo $sr_no; ?>"><i class="fa fa-check"></i></button>

										<div id="userModal_<?php echo $sr_no; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<form class="form-horizontal" method="post" action="<?php echo base_url()."member_management/inactive_member/change_status"; ?>">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title" id="myModalLabel">Change Status</h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														</div>
														<div class="modal-body">
															<div class="form-group">
																<label class="col-md-12">Reason</label>
																<div class="col-md-12">
																	<textarea class="form-control" rows="4" id="textarea" name="reason" placeholder=""></textarea>
																</div>
															</div>
														</div>
														<div class="modal-footer text-right">
															<input type="hidden" name="id" value="<?php echo $user['s-r-username']; ?>">
															<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
															<button type="submit" class="btn btn-info">Yes</button>
														</div>
													</div>
													<!-- /.modal-content -->
												</form>
											</div>
											<!-- /.modal-dialog -->
										</div>
									  <!-- /.modal -->
									</td>
									<td>
										<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['s-r-username']; ?>" class="openPopup">
											<?php echo ucwords($user['s-r-fname']." ".$user['s-r-lname']); ?>
										</a>
									</td>
									<td><?php echo $user['s-r-appartment']; ?></td>
									<td><?php echo $user['s-r-wing']; ?></td>
									<td><?php echo $user['s-r-floor']; ?></td>
									<td><?php echo $user['s-r-mobile']; ?></td>
									<td><?php echo $user['s-r-email']; ?></td>
									<td><?php echo $user['inactive_reason']; ?></td>
								</tr>
									<?php $sr_no++; ?>
								<?php endforeach; ?>
							<?php else: ?>
		                        <tr>
		                            <td colspan=9><center>No Records</center></td>
		                        </tr>
		                    <?php endif; ?>         
							</tbody>			  
							<tfoot>
								<tr>
									<th>Sr No</th>
									<th>Action</th>
									<th>Member Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Mobile No.</th>
									<th>Email Address</th>
									<th>Reason</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>              
			</div>
			<!-- /.box-body -->
	  	</div>
	  	<!-- /.box -->
	</div>
	<!-- /.col -->
</div>


<script>

	function printContent(el)
	{
		var restorepage = document.body.innerHTML;
		var printcontent = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcontent;
		window.print();
		document.body.innerHTML = restorepage;
	}

</script>

<script>

	function goBack() 
	{
	    location.reload();
	}

</script>


<script>

	function myFunction() 
	{
	    window.print();
	}

</script>