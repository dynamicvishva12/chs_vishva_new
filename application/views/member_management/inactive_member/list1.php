<style type="text/css">
	.class66
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
	body
	{
		line-height: 0px;
	}
</style>


	<center>
		<div id="id30" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 500px;">
				<div class="modal-content-rosh" style="margin-top: 100px;">
					<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px; padding-top: 0px; padding-bottom: 10px;"> 
						<button style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>        
						<center>
							<h2 style="margin-top: 0px; font-size: 20px;">Confirm Active</h2>
						</center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>


	<center>
		<div id="id32" class="modal-rosh">
			<div class="modal-dialog-rosh" >
				<div class="modal-content-rosh" style="width: 1000px;">
					<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
						<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000; margin-top: 0px; margin-right: 0px; background-color: #212121; color: #fff; " type="button" onclick="goBack()"  >&times;</button>        
						<center>
							<h3>Member Profile</h3>
						</center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>


	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
					<h4 class="title">
						Inactive Member
						<button  onclick="printContent('div1')" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background: #212121; color: #fff; float: right;">print</button>
					</h4>
				</div>
				<div class="card-content table-responsive" id="div1">
					<table class="table table-bordered" id="table_id">
						<thead class="text-primary">
							<th>Sr.No</th>
							<th>Member Name</th>
							<th>Email Address</th>
							<th>Building Name</th>
							<th>Wing</th>
							<th>Floor No</th>
							<th>Mobile No</th>
							<th>Change Status</th>
							<th>Reason</th>
						</thead>
						<tbody>
						<?php 
							$sr_no=1;
								if(!empty($member_data)):
									foreach($member_data AS $user):
						?>
							<tr class="odd gradeX" style="width: auto;">
								<td><?php echo $sr_no++; ?></td>
								<td>
									<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['s-r-username']; ?>" class="openPopup">
										<?php echo $user['s-r-fname']." ".$user['s-r-lname']; ?>
									</a>
								</td>
								<td><?php echo $user['s-r-email']; ?></td>
								<td><?php echo $user['s-r-appartment']; ?></td>
								<td><?php echo $user['s-r-wing']; ?></td>
								<td><?php echo $user['s-r-floor']; ?></td>
								<td><?php echo $user['s-r-mobile']; ?></td>
								<td>
									<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent openPopup1" style="background: #212121; color: #fff;" href="javascript:void(0);" data-href="<?php echo base_url()."member_management/inactive_member/approve_confirm"; ?>?id=<?php echo $user['s-r-username']; ?>&url=<?php echo base_url()."member_management/inactive_member/change_status"; ?>" >Active</a>
								</td>
								<td><?php echo $user['inactive_reason']; ?></td>
							</tr>
							<?php endforeach; ?>
						<?php else: ?>
	                        <tr>
	                            <td colspan=9><center>No Records</center></td>
	                        </tr>
	                    <?php endif; ?>         
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Active Confirmation</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Profile</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>

	<center>
		<div id="user_profile_modal_page" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 1200px;">
				<div class="modal-content-rosh">
					<header class="container-fluid rosh pari"> 
						<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000;" type="button" onclick="goBack()"  >&times;</button>        
						<center><h2 style="margin-top: 5px;">User Profile</h2></center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>


<!-- <script>

	$(document).ready(function(){
		$('.openPopup').on('click',function(){

			var dataURL = $(this).attr('data-href');
			$('.roshan').load(dataURL,function(){
				$('#id32').modal({show:true});
			});

		});
	});

</script> -->

<script>

	$(document).ready(function(){
		$('.openPopup').on('click',function(){

			var dataURL = $(this).attr('data-href');
			
			console.log(dataURL);

			$('.roshan').load(dataURL,function(){

				$('#user_profile_modal_page').modal({show:true});

			});

		});
	});

</script>


<script>

	$(document).ready(function(){
		$('.openPopup1').on('click',function(){

			var dataURL = $(this).attr('data-href');
			console.log(dataURL);
			$('.roshan').load(dataURL,function(){
				$('#id30').modal({show:true});
			});

		});
	});

</script>


<script>

	function printContent(el)
	{
		var restorepage = document.body.innerHTML;
		var printcontent = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcontent;
		window.print();
		document.body.innerHTML = restorepage;
	}

</script>


<script>

	function goBack() 
	{
		location.reload();
	}

</script>