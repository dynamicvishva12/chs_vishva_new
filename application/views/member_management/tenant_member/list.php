<div class="row">
	<div class="col-12">
		<div class="box">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
			  			<h4 class="box-title">Tenants Members</h4>
			  		</div>
			  		
				</div>
			</div>
			<div class="box-body">
			  	<div class="box">
					<div class="table-responsive">
				  		<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				  				
							<thead>
								<tr>
									<th>Sr No</th>
									<th>Member Name</th>
									<th>Tenant Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Tenant Mobile No.</th>
									<th>Email Address</th>
									<th>Tenant Agreement</th>
									<th>Approve</th>
									<th>Reject</th>
									<th>Reason</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$sr_no=1;
								if(!empty($member_data)):
								foreach($member_data AS $user):
									$user['reject'];
									$user['username'];
							?>
								<tr>
									<td><?php echo $sr_no++; ?></td>
									<td>
										<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['owner']; ?>" class="openPopup"><?php echo $user['owner']; ?></a>
									</td>
									<td>
										<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['owner']; ?>" class="openPopup">
											<?php echo ucwords($user['name']."".$user['last_name']); ?></a>
									</td>
									<td><?php echo $user['s_email']; ?></td>
									<td>
										<?php
											$addres=explode("-", $user['owner']);
											echo $addres[0]; 
										?>
									</td>
									<td><?php echo $addres[1]; ?></td>
									<td><?php echo $addres[2]; ?></td>
									<td><?php echo $user['contact']; ?></td>
									<td>
										<?php 
											$fileexe = strtolower(pathinfo($user['file'],PATHINFO_EXTENSION));
											if ($fileexe=="pdf") 
											{
										?>
											<a href="<?php echo $user['file']; ?>" style="width: 100%;height: 100%;border: none;" download=""></a>
										<?php  
											}
											else
											{
										?>
											<img class="img-thumbnail" src="<?php echo $user['file']; ?>" download="">
										<?php 
											}
										?>
											<a href="<?php echo $user['file']; ?>" download="">
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-primary" data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></button>
											</a>
											
									</td>
									<td>
										<?php 
											if($user['isActive']=="InActive" ||$user['reject_reason']!="")
											{
										?>
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent openPopup1" style="background: #212121; color: #fff;" href="javascript:void(0);" data-approve_id="<?php echo $user['id']; ?>" >Approve</a>
										<?php 
											}
											else
											{
												echo "Approved";
											}
										?>
									</td>
									<td>
										<?php 
											if($user['reject']=="Not Reject" || $user['accept_reason']!="" )
											{
										?>
											<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent openPopup125" style="background: #212121; color: #fff;" href="javascript:void(0);" data-reject_id="<?php echo $user['id']; ?>" >Reject</a>
										<?php
											}
											else
											{
												echo "Rejected";
											} 
										?>
									</td>
									<td>
										<?php 
											if($user['reject_reason']=="") 
											{
												echo $user['accept_reason'];
											}
											else
											{
												echo $user['reject_reason'];
											} 
										?>
									</td>
								</tr>
								<?php endforeach; ?>
							<?php else: ?>
		                        <tr>
		                            <td colspan=12><center>No Records</center></td>
		                        </tr>
		                    <?php endif; ?>      
							</tbody>				  
							<tfoot>
								<tr>
									<th>Sr No</th>
									<th>Member Name</th>
									<th>Tenant Name</th>
									<th>Building Name</th>
									<th>Wing</th>
									<th>Floor No.</th>
									<th>Tenant Mobile No.</th>
									<th>Email Address</th>
									<th>Tenant Agreement</th>
									<th>Approve</th>
									<th>Reject</th>
									<th>Reason</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>              
			</div>
			<!-- /.box-body -->
	  	</div>
	  	<!-- /.box -->
     	<!-- Popup Model Plase Here -->
		<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">Add Notice</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-md-12">Notice Title</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Notice Title">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12">Notice Expiry Date</label>
								<div class="col-md-12">
									<input type="date" class="form-control" placeholder="Notice Expiry Date">
								</div>
							</div>
							<div class="form-group">
								<label>Notice Type :</label>
										<div class="radio">
											<input name="group1" type="radio" id="option_1">
											<label for="option_1" class="mr-30">Normal</label>
											<input name="group2" type="radio" id="option_2">
											<label for="option_2" class="mr-30">Statutory</label>
										</div>
									</div>
								<div class="form-group">
									<label class="col-md-12">Notice Description</label>
									<div class="col-md-12">
									<textarea class="form-control" placeholder=""></textarea>
									</div>
								</div>
								<div class="form-group">
									  <label>Select File</label>
									  <label class="file">
										<input type="file" id="file">
									  </label>
								</div>

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" id="sa-success" data-dismiss="modal">Add</button>
						<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		</div>
	</div>
	<!-- /.col -->
</div>