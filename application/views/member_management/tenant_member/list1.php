<style type="text/css">
	.class66
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>


	<center>
		<div id="id30" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 500px;">
				<div class="modal-content-rosh" style="margin-top: 100px;">
					<div class="modal-header-rosh" style="padding:10px;">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<center><h4 class="modal-title">Approve</h4></center>
					</div>
					<div class="modal-body-rosh">
						<form class="" method="post" action="<?php echo base_url()."member_management/tenant_member/approve_confirm"; ?>"  style=" width: 100%; height:100%">
							<fieldset>
								<textarea style="position: absolute; right: 0%; width: 100%; border: 1px solid #000;" placeholder="Write Reason" name="reason"></textarea>
								<center>
									<input type="hidden" name="id" id="approve_id" value="">
									<input  type="submit" name="confirm" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background-color: #F44336; color: #fff; position: absolute; right: 50%; top: 75%;" value="reject" required>
									<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background-color: #009688; color: #fff; position: absolute; right: 25%; top: 75%; bottom: 10%;" class="close" data-dismiss="modal" aria-label="Close">Cancel</a>
								</center>
							</fieldset>
						</form>
					</div>
					<div class="modal-footer-rosh" style="margin-bottom:109px;"></div>
				</div>
			</div>
		</div>
	</center>


	<center>
		<div id="id31" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 500px;">
				<div class="modal-content-rosh" style="margin-top: 100px;">
					<div class="modal-header-rosh">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<center><h4 class="modal-title">Reject</h4></center>
					</div>
					<div class="modal-body-rosh">
						<form class="" method="post" action="<?php echo base_url()."member_management/tenant_member/reject"; ?>">
							<fieldset>
								<textarea placeholder="Write Reason" name="reason" class="form-control"></textarea>
								<center>
									<input type="hidden" name="id" id="reject_id" value="">
									<input type="submit" name="reject" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background-color: #F44336; color: #fff;" value="Confirm">
									<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="background-color: #009688; color: #fff;" class="close" data-dismiss="modal" aria-label="Close">Cancel</a>
								</center>
							</fieldset>
						</form>
					</div>
					<div class="modal-footer-rosh">

					</div>
				</div>
			</div>
		</div>
	</center>


	<center>
		<div id="id32" class="modal-rosh">
			<div class="modal-dialog-rosh" >
				<div class="modal-content-rosh" style="width: 1000px;">
					<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); padding-bottom: 10px;" > 
						<button  style="height: 40px; width: 40px; float: right; font-size: 20px color: #000; margin-top: 0px; margin-right: 0px; background-color: #212121; color: #fff; " type="button" onclick="goBack()"  >&times;</button>        
						<center><h3>Tenant Profile</h3></center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>


	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
					<h4 class="title">Teanants Management</h4>
				</div>
				<div class="card-content table-responsive">
					<table class="table table-bordered" id="table_id">
						<thead class="text-primary">
							<th>Sr.No</th>
							<th>Member Name</th>
							<th>Tenant Name</th>
							<th>Email Address</th>
							<th>Building Name</th>
							<th>Wing</th>
							<th>Floor No</th>
							<th>Tenant Mobile No</th>
							<th>Tenant Agrement</th>
							<th>Approve</th>
							<th>Reject</th>
							<th>Reason</th>
						</thead>
						<tbody>
						<?php 
							$sr_no=1;
							if(!empty($member_data)):
							foreach($member_data AS $user):
								$user['reject'];
								$user['username'];
						?>
							<tr class="odd gradeX" style="width: auto;">
								<td><?php echo $sr_no++; ?></td>
								<td>
									<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['owner']; ?>" class="openPopup"><?php echo $user['owner']; ?></a>
								</td>
								<td>
									<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$user['owner']; ?>" class="openPopup">
										<?php echo $user['name']."".$user['last_name']; ?></a>
								</td>
								<td><?php echo $user['s_email']; ?></td>
								<td>
									<?php
										$addres=explode("-", $user['owner']);
										echo $addres[0]; 
									?>
								</td>
								<td><?php echo $addres[1]; ?></td>
								<td><?php echo $addres[2]; ?></td>
								<td><?php echo $user['contact']; ?></td>
								<td>
									<?php 
										$fileexe = strtolower(pathinfo($user['file'],PATHINFO_EXTENSION));
										if ($fileexe=="pdf") 
										{
									?>
										<a href="<?php echo $user['file']; ?>" style="width: 100%;height: 100%;border: none;" download=""></a>
									<?php  
										}
										else
										{
									?>
										<img class="img-thumbnail" src="<?php echo $user['file']; ?>" download="">
									<?php 
										}
									?>
										<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background: #212121; color: #fff;" href="<?php echo $user['file']; ?>" download="">Download</a>
								</td>
								<td>
									<?php 
										if($user['isActive']=="InActive" ||$user['reject_reason']!="")
										{
									?>
										<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent openPopup1" style="background: #212121; color: #fff;" href="javascript:void(0);" data-approve_id="<?php echo $user['id']; ?>" >Approve</a>
									<?php 
										}
										else
										{
											echo "Approved";
										}
									?>
								</td>
								<td>
									<?php 
										if($user['reject']=="Not Reject" || $user['accept_reason']!="" )
										{
									?>
										<a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent openPopup125" style="background: #212121; color: #fff;" href="javascript:void(0);" data-reject_id="<?php echo $user['id']; ?>" >Reject</a>
									<?php
										}
										else
										{
											echo "Rejected";
										} 
									?>
								</td>
								<td>
									<?php 
										if($user['reject_reason']=="") 
										{
											echo $user['accept_reason'];
										}
										else
										{
											echo $user['reject_reason'];
										} 
									?>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php else: ?>
	                        <tr>
	                            <td colspan=12><center>No Records</center></td>
	                        </tr>
	                    <?php endif; ?>      
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Profile</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Approved Confirmation</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal1234" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">Reject Confirmation</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>

	<center>
		<div id="user_profile_modal_page" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 1200px;">
				<div class="modal-content-rosh">
					<header class="container-fluid rosh pari"> 
						<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000;" type="button" onclick="goBack()"  >&times;</button>        
						<center><h2 style="margin-top: 5px;">User Profile</h2></center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>

<script>

	$(document).ready(function(){
		$('.openPopup').on('click',function(){

			var dataURL = $(this).attr('data-href');
			
			console.log(dataURL);

			$('.roshan').load(dataURL,function(){

				$('#user_profile_modal_page').modal({show:true});

			});

		});
	});
</script>


<script>

	$(document).ready(function(){
		$('.openPopup1').on('click',function(){

			$('#approve_id').val("");

			var approve_id = $(this).attr('data-approve_id');
			$('#approve_id').val(approve_id);

			$('#id30').modal({show:true});

			// console.log(dataURL);
			// $('.roshan').load(dataURL,function(){
				// $('#id30').modal({show:true});
			// });

		});
	});

</script>

<script>

	$(document).ready(function(){
		$('.openPopup125').on('click',function(){

			$('#reject_id').val("");

			var reject_id = $(this).attr('data-reject_id');
			$('#reject_id').val(reject_id);

			$('#id31').modal({show:true});

			// var dataURL = $(this).attr('data-href');
			// console.log(dataURL);
			// $('.roshan').load(dataURL,function(){
			// 	$('#id31').modal({show:true});
			// });

		});
	});

</script>


<div class="modal fade" id="myModal1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<center><h4 class="modal-title">Prashik Khandare</h4></center>
			</div>
			<div class="modal-body"></div>
		</div>
	</div>
</div>


<!-- <script>

	$(document).ready(function(){
		$('.openPopup').on('click',function(){

			var dataURL = $(this).attr('data-href');
			$('.roshan').load(dataURL,function(){
				$('#id32').modal({show:true});
			});

		});
	});

</script> -->


<script>

	function goBack() 
	{
		location.reload();
	}

</script>