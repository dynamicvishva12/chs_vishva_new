<body class="hold-transition theme-primary bg-img" style="background-image: url(../images/auth-bg/bg-5.jpg)" data-overlay="5">
	
	<section class="error-page h-p100">
		<div class="container h-p100">
		  <div class="row h-p100 align-items-center justify-content-center text-center">
			  <div class="col-lg-7 col-md-10 col-12">
				  <div class="rounded30 bg-white p-50 shadow-lg">
					  <h1 class="font-size-180 font-weight-bold error-page-title"> <i class="fa fa-gear fa-spin"></i></h1>
					  <h1>UNDER MAINTENANCE!</h1>
					  <h3>We're sorry for the inconvenience.</h3>
					  <h4>Please check back later.</h4>	
				  </div>
			  </div>				
		  </div>
		</div>
	</section>

	<!-- Vendor JS -->
	<script src="<?php echo $assets_url; ?>js/vendors.min.js"></script>
    <script src="<?php echo $assets_url; ?>icons/feather-icons/feather.min.js"></script>	
</body>