		<div class="row">

			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-fill" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#birthdays" role="tab"><span></span> <span class="hidden-xs-down ml-15">Birthdays</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#newsletters" role="tab"><span></span> <span class="hidden-xs-down ml-15">Newsletters</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="birthdays" role="tabpanel">
							<div class="p-15">
								<div class="row">
									<?php
										if(!empty($birthday_data)):
											foreach($birthday_data AS $e_bday):
									?>
									<div class="col-md-12 col-lg-4">
									 	<div class="box box-widget widget-user">
											<div class="widget-user-header bg-black" style="background: url('../images/gallery/full/10.jpg') center center;">
												
											</div>

											<div class="widget-user-image">
												<img class="rounded-circle" src="../images/user3-128x128.jpg" alt="User Avatar">
											</div>

											<div class="box-footer">
												<h5 class="text-center pt-10">Many many happy returns of the day <?php echo $e_bday['name']; ?></h5>
												
											</div>
										</div>
									</div>
									<?php 
											endforeach;
										endif;
									?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="newsletters" role="tabpanel">
							<div class="p-15">
								<div class="row mb-10">
									<div class="col-9">
							  			
							  		</div>
							  		<div class="col-3 text-right">
							  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addNewsletter">Add Newsletter</button>
							  		</div>
								</div>
								<div class="row">
									<?php if(!empty($news_data)): ?>
										<?php foreach($news_data as $key => $gnew): ?>
										<div class="col-md-12 col-lg-4">
										  <div class="card">
											<div class="card-body">
												<div class="row">
													<div class="col-md-6 col-lg-4">            	
														<h4 class="card-title"><?php echo $gnew['nl_desc']; ?></h4>
													</div>
													<div class="col-md-6 col-lg-4">    
														<button type="button" class="btn btn-danger float-right remove_it" data-link="<?php echo base_url()."news_letter/delete_news_letter/".$gnew['nwId']; ?>">
															<i class="fa fa-trash-o"></i>
														</button>
													</div>
												</div>
											</div>
										  </div>
										</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->


<div id="addNewsletter" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."news_letter/add_news_letter"; ?>" method="POST" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Newsletter</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Newsletter Description</label>
						<div class="col-md-12">
						<textarea  name="description" class="form-control" placeholder="Newsletter Description"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
					<input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>       
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>