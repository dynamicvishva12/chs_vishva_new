<style type="text/css">
	.class02
	{
		background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
	.class666
	{
		background: linear-gradient(60deg, #004D40, #33691E); color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>


    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 

	<div id="id02" class="modal-rosh">
		<div class="modal-dialog-rosh">
			<div class="modal-content-rosh" style="width: 500px;">
				<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
					<a href="#" class="closebtn">×</a>
					<center><h2 style="margin-top: 5px;">News</h2></center>
				</header>
				<div class="container-fluid rosh">
					<form class="form-horizontal" action="<?php echo base_url()."news_letter/add_news_letter"; ?>" method="POST" enctype="multipart/form-data">
						<fieldset>
							<div class="form-group">
								<div class="col-md-12">
									<textarea name="description" placeholder="Detailed Description" style="width: 100%;"></textarea>    
								</div>
							</div><br>
							<center>
								<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
								<input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
								<button id="singlebutton" name="newsadd" class="btn btn-primary" style="background:#212121; color: #fff;">Submit</button>
							</center>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
					<h4 class="title">BIRTHDAYS</h4>
					<h6 style="color: #fff;">Todays Birthday</h6>
				</div>
				<div class="card-content">
					<ul>
						<?php
							if(!empty($birthday_data)):
								foreach($birthday_data AS $e_bday):
						?>
						<li>
							<h5><?php echo $e_bday['name']; ?></h5>
						</li>
						<?php 
								endforeach;
							endif;
						?>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
					<h4 class="title">NEWS LETTER</h4>
				</div>
				<div class="card-content">
					<ul>
						<?php if(!empty($news_data)): ?>
							<?php foreach($news_data as $key => $gnew): ?>
								<li>
									<h5>
										<?php echo $gnew['nl_desc']; ?> 
										<a href="<?php echo base_url()."news_letter/delete_news_letter/".$gnew['nwId']; ?>" class="confirmation">
											<i class="material-icons" style="color:Tomato; font-size: 15px;">delete</i>
										</a>
									</h5> 
								</li>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<span>
		<a href="#id02" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" data-toggle="tooltip" title="Add New's" style="position: fixed;top: 85%; right: 5%; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;   z-index: 100000">
			<i class="material-icons">add</i>
		</a>
	</span>

	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" style="font-size: 30px; color: #000;" class="close" data-dismiss="modal">&times;</button>
					<center><h4 class="modal-title">News Letter</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>

	<script type="text/javascript">

		var elems = document.getElementsByClassName('confirmation');
		var confirmIt = function (e) {
			if (!confirm('Are you sure?')) e.preventDefault();
		};
		for (var i = 0, l = elems.length; i < l; i++) {
			elems[i].addEventListener('click', confirmIt, false);
		}

	</script>

	<script>
		function goBack()
		{
			location.reload();
		}
	</script>