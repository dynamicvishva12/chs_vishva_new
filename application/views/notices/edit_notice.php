<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
	  		<div class="box-header with-border">
				<div class="row">
					<div class="col-10">
			  			<h4 class="box-title">Edit Notice</h4>
			  		</div>
			  		<div class="col-2 text-right">
			  		</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12">
									<!-- Multiple Radios -->
									<div class="form-group row" >
										<label class="col-md-2 control-label" for="radios">
											<strong>Notice Type:<span class="text-danger">*</span></strong>
										</label>
										<div class="col-md-10">
											<div class="radio">
												<input type="radio" name="radios" id="radios-0" value="All" checked="checked">
												<label for="radios-0"> All</label>
												<?php if(form_error('radios')!=""): ?>
												<label class="text-danger"><?php echo form_error('radios'); ?></label>
												<?php endif; ?>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textarea">
											<strong >Title:<span class="text-danger">*</span></strong>
										</label>
										<div class="col-md-10">                     
											<input type="text" id="textarea" name="hnotice" class="form-control" value="<?php echo set_value('hnotice', $notice_data['noticeTitle']); ?>">
											<?php if(form_error('hnotice')!=""): ?>
											<label class="text-danger"><?php echo form_error('hnotice'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<!-- Text input-->
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textinput">
											<strong >Expiry Date:<span class="text-danger">*</span></strong>
										</label>  
										<div class="col-md-10">
											<input id="textinput" name="expdt" type="date" placeholder="Select Expiry Date" class="form-control" value="<?php echo set_value('expdt', $notice_data['noticeDate']); ?>" >
											<?php if(form_error('expdt')!=""): ?>
											<label class="text-danger"><?php echo form_error('expdt'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<!-- Textarea -->
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textarea">
											<strong>Detailed Notice:<span class="text-danger">*</span></strong>
										</label>
										<div class="col-md-10">                     
											<textarea rows="3" id="textarea" name="dnotice" class="form-control" placeholder="detailed notice"><?php echo set_value('dnotice', $notice_data['noticeDesc']); ?></textarea>
											<?php if(form_error('dnotice')!=""): ?>
											<label class="text-danger"><?php echo form_error('dnotice'); ?></label>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-2"></div>
										<div class="col-md-5 callout callout-warning">
											<h4><i class="icon fa fa-info"></i> Note:</h4>
											<ul>
												<li>File size must be 5MB or less.</li>
												<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
											</ul>
										</div>  
									</div>
									<div class="form-group row">
										<label class="col-md-2 control-label" for="textarea">
											<strong>Upload Document:<span class="text-danger">*</span></strong>
										</label>
										<div class="col-md-10"> 
											<input id="oldfile" name="myfile"  class="input-file" type="file" onclick="switchVisible();">
											<?php if(form_error('myfile')!=""): ?>
											<label class="text-danger"><?php echo form_error('myfile'); ?></label>
											<?php endif; ?>
											<?php 
												// echo $notice_data['noticeFilename']; 
												$temp = explode('/', $notice_data['noticeFilename']);
												$getId = end($temp);
											?>
											<a id="nameold" href="<?php echo $notice_data['noticeFilename']; ?>" target="_blank"><?php echo $getId; ?></a>
											<a href="#" id="removebtn" class="btn btn-danger confirmations" onclick="switchVisible1();">
												<i class="material-icons">Clear</i>
											</a>
											<input id="nameold" name="nameold" class=" input-file" value="<?php echo $getId; ?>" type="hidden">
											<input id="image_status" name="image_status" class=" input-file" value="<?php echo "unchanged"; ?>" type="hidden">
											<input id="nofiletype" name="nofiletype" class=" input-file" value="<?php echo $notice_data['noticeFileType']; ?>" type="hidden">
											<input type="hidden" name="noticeId" id="meetId" value="<?php echo $notice_data['noticeId']; ?>" required>
										<!-- Button -->
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-12 text-right">
											<!-- <button type="submit" id="singlebutton" name="editnotice" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  style="background: #212121; color: #fff; ">post</button> -->
											<button type="submit" name="editnotice" class="btn btn-success text-center" >Submit</button>

										</div>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>


<script>
    function switchVisible() 
    {
        if(document.getElementById('oldfile')) 
        {
            document.getElementById("image_status").value="changed";
           	document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
        }
	}

	function switchVisible1() 
	{
        if(document.getElementById('removebtn')) 
        {
            document.getElementById("image_status").value="changed";
           	document.getElementById('nameold').style.setProperty("text-decoration", "line-through");
        }
	}
</script>
