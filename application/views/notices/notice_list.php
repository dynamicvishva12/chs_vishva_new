
		<!-- Main content -->
		<div class="row">
			<div class="col-12">
				<div class="box">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-10">
					  			<h4 class="box-title">Notices</h4>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#myModal">Create Notice</button>
					  		</div>
						</div>
					</div>
					<div class="box-body">
					  	<div class="box">
							<div class="table-responsive">
					  			<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
					  				<!--display responsive nowrap    this for responsive datatables-->
									<thead>
										<tr>
											<th>Sr No</th>
											<th>Action</th>
											<th>Notice Title</th>
											<th>Notice Type</th>
											<th>Notice Description</th>
											<th>Notice Expiry Date</th>
											<th>Notice Attachments</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$srno=1;
											if(!empty($notice_data)):
												foreach($notice_data as $key => $get_value):
										?>
											<tr>
												<td><?php echo $srno ++; ?></td>
												<td>
													<a href="<?php echo base_url()."notices/edit/".$get_value['noticeId'] ?>">
														<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></button>
													</a>
													<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."notices/delete/".$get_value['noticeId']; ?>"><i class="fa fa-trash-o"></i></button>
												</td>
												<td><?php echo $get_value['noticeTitle']; ?></td>
												<td><?php echo $get_value['no_type1']; ?></td>
												<td><?php echo $get_value['noticeDesc']; ?></td>
												<td><?php echo date("d-m-Y",strtotime($get_value['noticeDate'])); ?></td>
												<td class="center">
													<?php if(!empty($get_value['noticeFilename'])): ?>
														<?php 
															$explodedArr=explode('/', $get_value['noticeFilename']);
															$fileAtmntName=end($explodedArr);
														?>
														<a href="<?php echo $get_value['noticeFilename']; ?>" target="_blank"><?php echo $fileAtmntName; ?></a>
													<?php else: ?>
														N/A
													<?php endif; ?>
												</td>
											</tr>
											<?php endforeach; ?>
										<?php endif; ?>
									</tbody>				  
									<tfoot>
										<tr>
											<th>Sr No</th>
											<th>Action</th>
											<th>Notice Title</th>
											<th>Notice Type</th>
											<th>Notice Description</th>
											<th>Notice Expiry Date</th>
											<th>Notice Attachments</th>
										</tr>
									</tfoot>
								</table>
							</div>              
						</div>
		  			</div>
				</div>
	  		</div>
	  	</div>

 
<!-- Popup Model Plase Here -->
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."notices/add"; ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Create Notice</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Notice Title:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<input type="text" class="form-control" id="textarea" name="hnotice" placeholder="Notice Title" value="<?php echo set_value('hnotice'); ?>">
							<?php if(form_error('hnotice')!=""): ?>
							<label class="text-danger"><?php echo form_error('hnotice'); ?></label>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Notice Expiry Date:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<input type="date" id="textinput" name="expdt" class="form-control" placeholder="Notice Expiry Date" value="<?php echo set_value('expdt', date("Y-m-d")); ?>" >
							<?php if(form_error('expdt')!=""): ?>
							<label class="text-danger"><?php echo form_error('expdt'); ?></label>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<label>Notice Type :<span class="text-danger">*</span></label>
						<div class="radio">
							<input type="radio" name="radios" id="radios-0" value="All" <?php echo set_checkbox('radios', "All", TRUE); ?> >
							<label for="radios-0" class="mr-30">Normal</label>
							<input  type="radio" name="radios" id="radios-2"  value="Emergency" <?php echo set_checkbox('radios', "Emergency"); ?>>
							<label for="radios-2" class="mr-30">Statutory</label>
							<?php if(form_error('radios')!=""): ?>
							<label class="text-danger"><?php echo form_error('radios'); ?></label>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Notice Description:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<textarea class="form-control" rows="8" id="textarea" name="dnotice" placeholder=""><?php echo set_value('dnotice'); ?></textarea>
							<?php if(form_error('dnotice')!=""): ?>
							<label class="text-danger"><?php echo form_error('dnotice'); ?></label>
							<?php endif; ?>
						</div>
					</div>
					<div class="callout callout-warning">
						<h4><i class="icon fa fa-info"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
					<div class="form-group row">
						<label class="col-md-12">Select File:<span class="text-danger">*</span></label>
						<div class="col-md-12">
							<input type="file" id="filebutton" name="myfile">
						</div>
						<?php if(form_error('myfile')!=""): ?>
						<label class="col-md-12 text-danger"><?php echo form_error('myfile'); ?></label>
						<?php endif; ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" id="notice_add_btn">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#myModal').modal('show');

		<?php endif; ?>
	});

</script>


