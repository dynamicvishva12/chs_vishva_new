		<div class="row">
			<div class="col-9">
					
			</div>
			<div class="col-3 text-right">
				<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addPoll">Create Poll</button>
			</div>
		</div>
		<div class="row">

			<?php
	            if(!empty($p_mstr_data)):
	                foreach($p_mstr_data AS $key => $row):
	                    // $polls_option=Fetch_pollesdetails($userName, $societyKey, $row['pollId']);
	        ?>
			<div class="col-md-12 col-lg-6">
				<div class="card">

					<div class="card-body row">
						<div class="col-md-10">
							<h4 class="card-title"><?php echo $row['pollName']; ?> </h4>
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-danger float-right remove_it" data-link="<?php echo base_url()."poll/delete/".$row['pollId']; ?>">
								<i class="fa fa-trash-o"></i>
							</button>
						</div>
					</div>
					<ul class="list-group list-group-flush">
						<?php 
                            if(!empty($poll_option_arr)):

                                if(isset($poll_option_arr[$row['pollId']])):

                                    $poll_opt_arr=$poll_option_arr[$row['pollId']];

                                    foreach($poll_opt_arr AS $e_opt):

                                        if($e_opt!=""):

                                            $opt_vote=0;

                                            if(isset($poll_res[$row['pollId']][$e_opt]))
                                                $opt_vote=$poll_res[$row['pollId']][$e_opt];

                                            $option_votes=$e_opt." (".$opt_vote." votes)";

                                            $totalPercentage=$opt_vote*100/100;

                                           	$percentageVotes=$totalPercentage."%";

                        ?>
						<li class="list-group-item">
							<div class="row" style="align-items: center;">
								<div class="col-md-4">
									<?php echo $option_votes; ?>
								</div>
								<div class="col-md-7">
									<div class="progress" style="margin-bottom: 0;">
										<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $opt_vote; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentageVotes; ?>">
											<span class="sr-only"><?php echo $percentageVotes; ?></span>
										</div>
									</div>
								</div>
								<div class="col-md-1 text-right text-dark">
									<?php echo $percentageVotes; ?>
								</div>
							</div>
						</li>
						<?php 
                                        endif;
                                    endforeach;
                                endif;
                            endif;
                        ?>
					</ul>
					<div class="card-footer">
					</div>
				</div>
			</div>
			<?php   
	                endforeach;
	            endif;
	        ?>
		</div>
		<!-- /.row -->

  <!-- Popup Model Plase Here -->
  <div id="addPoll" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."poll/add_poll"; ?>" method="POST" id="poll_form">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Create Poll</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Poll Title</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="poll_header" id="header" placeholder="Poll Title" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Poll Expiry Date</label>
						<div class="col-md-12">
							<input type="date" class="form-control" name="expr" id="expr" value="<?php echo date('Y-m-d'); ?>" placeholder="Poll Expiry Date" required>
						</div>
					</div>
					<hr>
					<button type="button" class="btn btn-success float-right" id="add_option">Add Option</button>
					<div id="all_options">
						<div class="form-group">
							<label class="col-md-12">Option 1</label>
							<div class="col-md-12">
								<input type="text" class="form-control" name="poll1" id="nor-1" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-12">Option 2</label>
							<div class="col-md-12">
								<input type="text" class="form-control" name="poll2" id="nor-2" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" id="poll-insert" >Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>

<script>
	$(document).ready(function(){
        var i=1; 
        var r=0;
        
        $('#add_option').on('click', function(){ 

        	var j=i+2;

			if(i < 9)
	        {
	            var layout='<div class="form-group" id="row'+i+'"><label class="col-md-12">Option '+j+'</label><div class="row" style="align-items: center;"><div class="col-md-10"><input type="text" class="form-control" id="dyn'+i+'" name="option1"></div><div class="col-md-2" ><button type="button" class="btn btn-danger float-right btn_remove" id="'+i+'"><i class="fa fa-trash-o"></i></button></div></div></div>';
	            i++;
	            r=0;
	        }
	        else
	        {
	           $('#add_option').hide();
	           //$('#dynamic_field').html("<small>Poll cannot be more then 10</small>");
	        }

			$('#all_options').append(layout);
		});

		$(document).on('click', '.btn_remove', function(){  
			var button_id = $(this).attr("id");   
			$('#row'+button_id+'').remove();
			// r++;
			// i=i-r;
			i--;
			$('#add_option').show();
	    });
	});

	
</script>

 <script>

     $("#poll-insert").click(function(){

     	if($('#nor-1').val()=="" || $('#nor-2').val()=="" || $('#header').val()=="" || $('#expr').val()=="")
     	{
     		$('#poll_form').submit();
     	}
     	else
     	{
	        console.log('working');
	        var pollheader = $('#header').val();
	        var expr = $('#expr').val();

	        //var type = $('#type').val();
	        var poll1 = $('#nor-1').val();
	        var poll2 = $('#nor-2').val();
	        var poll3 = $('#dyn1').val();
	        var poll4 = $('#dyn2').val();
	        var poll5 = $('#dyn3').val();
	        var poll6 = $('#dyn4').val();
	        var poll7 = $('#dyn5').val();
	        var poll8 = $('#dyn6').val();
	        var poll9 = $('#dyn7').val();
	        var poll10 = $('#dyn8').val();
	        console.log(expr);

	        var base_url = "<?php echo base_url(); ?>";

	        var add_poll_url=base_url+"poll/add_poll";
	        
	        // $.post("poll_result",
	        // $.post(add_poll_url,
	        // {
	        //   poll_header : pollheader,
	        //   expr : expr,
	        //   poll1: poll1,
	        //   poll2: poll2,
	        //   poll3: poll3,
	        //   poll4: poll4,
	        //   poll5: poll5,
	        //   poll6: poll6,
	        //   poll7: poll7,
	        //   poll8: poll8,
	        //   poll9: poll9,
	        //   poll10: poll10
	        // },
	        // function(data){

	        // 	window.location.reload();

	        //     $('#variable').html(data);
	        // });

	        $.ajax({
		        url : add_poll_url,
		        type : 'POST',
		        data : { 
		        	poll_header : pollheader,
					expr : expr,
					poll1: poll1,
					poll2: poll2,
					poll3: poll3,
					poll4: poll4,
					poll5: poll5,
					poll6: poll6,
					poll7: poll7,
					poll8: poll8,
					poll9: poll9,
					poll10: poll10 
		      	},
		        dataType: 'html',
		        success : function(data) {     

		          window.location.reload();

		        },
		        error : function(request, error)
		        {
		            // alert("Request: "+JSON.stringify(request));
		        }
		    });
		}
    
    });
</script>