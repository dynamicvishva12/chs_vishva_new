		<div class="row">

			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-fill" role="tablist">
						<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#reports" role="tab"><span></span> <span class="hidden-xs-down ml-15">Audit Reports</span></a> </li>
						<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#formo" role="tab"><span></span> <span class="hidden-xs-down ml-15">Form O</span></a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="reports" role="tabpanel">
							<div class="p-15">
								<div class="row">
									<div class="col-10">
							  			
							  		</div>
							  		<div class="col-2 text-right">
							  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addReport">Add Audit Report</button>
							  		</div>
								</div>
								<div class="row">
									<?php if(!empty($auditData)): ?>
									<?php foreach($auditData AS $e_adt): ?>
										<div class="col-md-12 col-lg-4 text-center">
										  <div class="card">
										  	<?php $fileexe = strtolower(pathinfo($e_adt['audit'], PATHINFO_EXTENSION)); ?>
                             				<?php if($fileexe=="pdf"): ?>
											  	<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $assets_url; ?>img/pdf.png" alt="Card image cap">
											<?php else:  ?>
												<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $file['audit']; ?>" alt="Card image cap">
												<?php if($file['audit']==""): ?>
													<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $assets_url; ?>img/doc.png" alt="Card image cap">
												<?php endif; ?>
											<?php endif; ?>
											<div class="card-body">            	
												<h4 class="card-title"><?php echo $e_adt['doc_name']; ?></h4>
												<a href="<?php echo $e_adt['audit']; ?>" class="btn btn-rounded btn-primary" download="">Download</a>
											</div>
										  </div>
										</div>
									<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="formo" role="tabpanel">
							<div class="p-15">
								<div class="row">
									<div class="col-10">
							  			
							  		</div>
							  		<div class="col-2 text-right">
							  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addformo">Add Form O</button>
							  		</div>
								</div>
								<div class="row">
									<?php if(!empty($formoData)): ?>
									<?php foreach($formoData AS $e_formo): ?>
										<div class="col-md-12 col-lg-4 text-center">
										  <div class="card">
										  	<?php $fileexe = strtolower(pathinfo($e_formo['document'], PATHINFO_EXTENSION)); ?>
                             				<?php if($fileexe=="pdf"): ?>
											  	<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $assets_url; ?>img/pdf.png" alt="Card image cap">
											<?php else:  ?>
												<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $e_formo['document']; ?>" alt="Card image cap">
												<?php if($e_formo['document']==""): ?>
													<img class="card-img-top img-responsive w-100 mx-auto mt-10" src="<?php echo $assets_url; ?>img/doc.png" alt="Card image cap">
												<?php endif; ?>
											<?php endif; ?>
											<div class="card-body">            	
												<h4 class="card-title"><?php echo $e_formo['doc_name']; ?></h4>
												<a href="<?php echo $e_formo['document']; ?>" class="btn btn-rounded btn-primary" download="">Download</a>
											</div>
										  </div>
										</div>
									<?php endforeach; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		</div>



<!-- Popup Model Plase Here -->
<div id="addReport" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/upload_audit"; ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Audit Report</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label class="col-md-12">Audit Report Name</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="docname" placeholder="Audit Report Name">
						</div>
					</div>
					<div class="form-group callout callout-warning">
						<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
				  	<div class="form-group">
					  	<label>Report File</label>
					  	<label class="file">
							<input type="file" id="file" name="myfile" >
					  	</label>
				  	</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Upload</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Popup Model Plase Here -->
<div id="addformo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/upload_formo"; ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Form O</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label class="col-md-12">Form O Description</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="docname" placeholder="Form O Description">
						</div>
					</div>
					<div class="form-group callout callout-warning">
						<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div> 
				  	<div class="form-group">
					  	<label>File</label>
					  	<label class="file">
							<input type="file" id="file" name="myfile">
					  	</label>
				  	</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Upload</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>