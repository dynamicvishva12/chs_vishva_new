<div class="row">

	<div class="col-12">
	  	<div class="box box-default">
			<!-- /.box-header -->
			<div class="box-header with-border">
		 		<div class="row">
					<div class="col-10">
			  			<h3 class="box-title">Edit J Form</h3>
			  		</div>
			  		<div class="col-2 text-right">
			  			<a href="<?php echo base_url(); ?>reg_docs/jform">
			  				<button type="button" class="waves-effect waves-light btn btn-secondary mb-5" >Back</button>
			  			</a>
			  		</div>
				</div>
		 	</div>
			<div class="box-body">

				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal" action="<?php echo base_url()."reg_docs/edit_jform"; ?>" method="POST" >
						  	<div class="form-group row">
								<label class="col-md-12">Select Member</label>
								<div class="col-md-12">
									<select class="form-control select2" name="member" style="width: 100%;">
										<?php
											if(!empty($member_data)):
												foreach ($member_data as $key => $value):
										?>
											<option id="<?php echo $value['s-r-username']; ?>" value="<?php echo $value['s-r-username']; ?>" <?php if($value['s-r-username']==$jform_arr['mem_id']): ?> selected <?php endif; ?> >
												<?php echo ucfirst($value['usrFname']." ".$value['usrLname']." (".$value['s-r-username'].")"); ?>
											</option>
										<?php 
												endforeach;
											endif;
										?>
									</select>
						  		</div>
						  	</div>
						  	<div class="form-group row">
						  		<label class="col-md-12">Member Type</label>
						  		<div class="col-md-12">
									<select class="form-control select2" name="classname" style="width: 100%;">
										<option <?php if($jform_arr['member_type']=="Ordinary"): ?> selected <?php endif; ?>>Ordinary</option>
										<option <?php if($jform_arr['member_type']=="Associate"): ?> selected <?php endif; ?>>Associate</option>
										<option <?php if($jform_arr['member_type']=="Normal"): ?> selected <?php endif; ?>>Normal</option>
									</select>
						  		</div>
						  	</div>
						  	<div class="form-group row">
						  		<div class="col-md-12 text-center">
									<button type="button" class="btn btn-warning" id="add_j_member">Add Member</button>
						  		</div>
						  	</div>
						  	<div class="form-group row">
						  		<div class="col-md-12 member_div">
						  			<?php 
						  				for($i=1; $i<11; $i++): 

						  				$mbr_name=$jform_arr['j_member_name_'.$i];
						  				$mbr_address=$jform_arr['j_member_address_'.$i];
						  				$count=$i;
						  				if($mbr_name=="" && $i!="1")
						  				{
						  					break;
						  				}
						  			?>
								  	<div class="form-group row" id="each_mbr_<?php echo $i; ?>">
								  		<label class="col-md-6">Joint Member <?php echo $i; ?>: </label>
								  		<?php if($i!='1'): ?>
								  		<div class="col-md-6 text-right">
								  			<button type="button" class="btn btn-danger remove_mbr" id="<?php echo $i; ?>">
								  				<i class="fa fa-remove"></i>
								  			</button>
								  		</div>
								  		<?php endif; ?>
								  		<label class="col-md-12">Name</label>
										<div class="col-md-12">
											<input type="text" class="form-control" name="j_member_name[]" placeholder="Joint Member Name" value="<?php echo $mbr_name; ?>" required>
										</div>
										<label class="col-md-12">Address</label>
										<div class="col-md-12">
											<textarea class="form-control" rows="3" name="j_member_address[]" placeholder="Joint Member Address" required><?php echo $mbr_address; ?></textarea>
										</div>
									</div>
								<?php endfor; ?>
								</div>
							</div>
							<div class="form-group row">
						  		<div class="col-md-12 text-right">
						  			<input type="hidden" name="member_id" value="<?php echo $id; ?>" >
								  	<button type="submit" class="btn btn-success text-center" >Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		var j_mbr="<?php echo $count; ?>";

		$('#add_j_member').on('click', function(){

			if(j_mbr>10)
				return false;

			$('.member_div').append('<div class="form-group row" id="each_mbr_'+j_mbr+'"><label class="col-md-6">Joint Member '+j_mbr+': </label><div class="col-md-6 text-right"><button type="button" class="btn btn-danger remove_mbr" id="'+j_mbr+'"><i class="fa fa-remove"></i></button></div><label class="col-md-12">Name</label><div class="col-md-12"><input type="text" class="form-control" name="j_member_name[]" placeholder="Joint Member Name" required></div><label class="col-md-12">Address</label><div class="col-md-12"><textarea class="form-control" rows="3" name="j_member_address[]" placeholder="Joint Member Address" required></textarea></div></div>');

			j_mbr++;
		});

		$('body').on('click', '.remove_mbr', function(){

			var mbr_id = $(this).attr('id');

			if(mbr_id!='1')
				$('#each_mbr_'+mbr_id).remove();
			
		});

	});

</script>

