		<div class="row">
			<div class="col-12">
			 	<div class="box">
					<div class="box-header with-border">
				 		<div class="row">
							<div class="col-10">
					  			<h3 class="box-title">J Form list</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addFormj">Add Form J</button>
					  		</div>
						</div>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
							<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
								<thead>
									<tr>
										<th colspan="5"><center>Member Detail</center></th>
										<th colspan="2"><center>First Joint Member</center></th>
										<th colspan="2"><center>Second Joint Member</center></th>
										<th colspan="2"><center>Third Joint Member</center></th>
										<th colspan="2"><center>Fourth Joint Member</center></th>
										<th colspan="2"><center>Fifth Joint Member</center></th>
										<th colspan="2"><center>Sixth Joint Member</center></th>
										<th colspan="2"><center>Seventh Joint Member</center></th>
										<th colspan="2"><center>Eighth Joint Member</center></th>
										<th colspan="2"><center>Ninth Joint Member</center></th>
										<th colspan="2"><center>Tenth Joint Member</center></th>
									</tr>
									<tr>
										<th>Sr. No</th>
										<th>Action</th>
										<th>Member ID</th>
										<th>Member Name</th>
										<th>Request Type</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Sr. No</th>
										<th>Action</th>
										<th>Member ID</th>
										<th>Member Name</th>
										<th>Request Type</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
										<th>Joint Member Name</th>
										<th>Address</th>
									</tr>
								</tfoot>
								<tbody>
									<?php 
		                                if(!empty($jform_arr)):
		                                    $x=1;
		                                    foreach($jform_arr AS $user):
		                            ?>
										<tr>
											<td><?php echo $x; ?></td>
											<td>
												<center>
													<a href="<?php echo base_url(); ?>reg_docs/edit_jform/<?php echo $user['sr_id']; ?>">
														<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit">
															<i class="fa fa-edit"></i>
														</button>
													</a>
													<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-link="<?php echo base_url(); ?>reg_docs/del_jform/<?php echo $user['sr_id']; ?>" data-toggle="tooltip" data-original-title="Delete">
														<i class="fa fa-trash-o" ></i>
													</button>
												</center>
											</td>
											<td><?php echo $user['mem_id']; ?></td>
											<td><?php echo $user['s-r-fname']." ".$user['s-r-lname']; ?></td>
											<td><?php echo $user['member_type']; ?></td>
											<td><?php echo $user['j_member_name_1']; ?></td>
											<td><?php echo $user['j_member_address_1']; ?></td>
											<td><?php echo $user['j_member_name_2']; ?></td>
											<td><?php echo $user['j_member_address_2']; ?></td>
											<td><?php echo $user['j_member_name_3']; ?></td>
											<td><?php echo $user['j_member_address_3']; ?></td>
											<td><?php echo $user['j_member_name_4']; ?></td>
											<td><?php echo $user['j_member_address_4']; ?></td>
											<td><?php echo $user['j_member_name_5']; ?></td>
											<td><?php echo $user['j_member_address_5']; ?></td>
											<td><?php echo $user['j_member_name_6']; ?></td>
											<td><?php echo $user['j_member_address_6']; ?></td>
											<td><?php echo $user['j_member_name_7']; ?></td>
											<td><?php echo $user['j_member_address_7']; ?></td>
											<td><?php echo $user['j_member_name_8']; ?></td>
											<td><?php echo $user['j_member_address_8']; ?></td>
											<td><?php echo $user['j_member_name_9']; ?></td>
											<td><?php echo $user['j_member_address_9']; ?></td>
											<td><?php echo $user['j_member_name_10']; ?></td>
											<td><?php echo $user['j_member_address_10']; ?></td>
										</tr>
									    <?php $x++; ?>
									    <?php endforeach; ?>
		                            <?php else: ?>
	                                    <tr>
	                                        <td colspan=15><center>No Records</center></td>
	                                    </tr>
		                            <?php endif; ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->      
			</div>
			<!-- /.col -->
		</div>


<!-- Popup Model Plase Here -->
<div id="addFormj" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/add_jform"; ?>" method="POST" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Form J</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group row">
						<label class="col-md-12">Select Member</label>
						<div class="col-md-12">
							<select class="form-control select2" name="member" style="width: 100%;">
								<?php
									if(!empty($member_data)):
										foreach ($member_data as $key => $value):
								?>
									<option id="<?php echo $value['s-r-username']; ?>" value="<?php echo $value['s-r-username']; ?>">
										<?php echo ucfirst($value['usrFname']." ".$value['usrLname']." (".$value['s-r-username'].")"); ?>
									</option>
								<?php 
										endforeach;
									endif;
								?>
							</select>
				  		</div>
				  	</div>
				  	<div class="form-group row">
				  		<label class="col-md-12">Member Type</label>
				  		<div class="col-md-12">
							<select class="form-control select2" name="classname" style="width: 100%;">
								<option selected="selected">Ordinary</option>
								<option>Associate</option>
								<option>Normal</option>
							</select>
				  		</div>
				  	</div>
				  	<div class="form-group row">
				  		<div class="col-md-12 text-center">
							<button type="button" class="btn btn-warning" id="add_j_member">Add Member</button>
				  		</div>
				  	</div>
				  	<div class="form-group row">
				  		<div class="col-md-12 member_div">
						  	<div class="form-group row" id="each_mbr_1">
						  		<label class="col-md-6">Joint Member 1: </label>
						  		<label class="col-md-12">Name</label>
								<div class="col-md-12">
									<input type="text" class="form-control" name="j_member_name[]" placeholder="Joint Member Name" required>
								</div>
								<label class="col-md-12">Address</label>
								<div class="col-md-12">
									<textarea class="form-control" rows="3" name="j_member_address[]" placeholder="Joint Member Address" required></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		var j_mbr=2;

		$('#add_j_member').on('click', function(){

			if(j_mbr>10)
				return false;

			$('.member_div').append('<div class="form-group row" id="each_mbr_'+j_mbr+'"><label class="col-md-6">Joint Member '+j_mbr+': </label><div class="col-md-6 text-right"><button type="button" class="btn btn-danger remove_mbr" id="'+j_mbr+'"><i class="fa fa-remove"></i></button></div><label class="col-md-12">Name</label><div class="col-md-12"><input type="text" class="form-control" name="j_member_name[]" placeholder="Joint Member Name" required></div><label class="col-md-12">Address</label><div class="col-md-12"><textarea class="form-control" rows="3" name="j_member_address[]" placeholder="Joint Member Address" required></textarea></div></div>');

			j_mbr++;
		});

		$('body').on('click', '.remove_mbr', function(){

			var mbr_id = $(this).attr('id');

			if(mbr_id!='1')
				$('#each_mbr_'+mbr_id).remove();
			
		});

	});

</script>