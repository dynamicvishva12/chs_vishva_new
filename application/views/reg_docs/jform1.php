    <div class="row" style="padding-left: 20px;">
        <div class="col-md-12">
            <div class="card" style="width: 100%;">
                <div class="card-header" style=" background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
                    <h4 class="title">
                        FORM J
                        <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="float: right; background:#212121;color: #fff;" href="<?php echo htmlspecialchars('notifications'); ?>">
                            BACK
                        </a>
                    </h4>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-bordered table-responsive" style="">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Member ID</th>
                                <th>Member Name</th>
                                <th>Request Type</th>
                                <th>Nominal/Associate Member Name</th>
                                <th>Relation</th>
                                <th>Date of birth</th>
                                <th>E-mail</th>
                                <th>Landline</th>
                                <th>Mobile No.</th>
                                <th>Document</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if(!empty($cl_mem_reg_arr))
                                    $x=1;
                                    foreach($cl_mem_reg_arr AS $user):
                            ?>
                                <tr class="odd gradeX" style="width: auto;">
                                    <td><?php echo $x++;  ?></td>
                                    <td><?php echo $user['mem_id']; ?></td>
                                    <td>
                                        <a href="javascript:void(0);" data-href="<?php echo htmlspecialchars('user_profile'); ?>?id=<?php echo $user['mem_id'];?>" class="openPopup">
                                            <?php echo $user['s-r-fname']." ".$user['s-r-lname']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="collapse" data-target="#demo"><?php echo $user['member_type'];?></a> 
                                    </td>
                                    <td id="demo" class="collapse">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </td>
                                    <td><?php echo $user['na_mem']; ?></td>
                                    <td><?php echo $user['relation']; ?></td>
                                    <td><?php echo date("d-m-Y",strtotime($user['dt_db'])); ?></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php echo $user['landline']; ?></td>
                                    <td><?php echo $user['contact']; ?></td>
                                    <td>
                                        <a href="<?php echo $user['class_doc']; ?>"><?php echo $user['class_doc']; ?></a>
                                    </td>
                                    <td>
                                        <center>
                                            <a class="openPopup1" href="javascript:void(0);" data-href="<?php echo htmlspecialchars('delete_conformation'); ?>?id=<?php echo $jform['sr_id']; ?>&url=<?php echo $_SERVER['PHP_SELF']; ?>">
                                                <i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
                                            </a>
                                        </center>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                    <tr>
                                        <td colspan=12><center>No Records</center></td>
                                    </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <a href="#id13">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"  style=" position: fixed; top: 85%; right: 5%; z-index: 3000; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
            <i class="material-icons">add</i>
        </button>
    </a>

    <center>
        <div id="id31" class="modal-rosh">
            <div class="modal-dialog-rosh" style="width: 500px;">
                <div class="modal-content-rosh" style="margin-top: 100px;">
                    <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px;padding-top: 0px; padding-bottom: 10px;"> 
                        <button style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>        
                        <center>
                            <h2 style="margin-top: 0px; font-size: 20px;">Confirm Delete</h2>
                        </center>
                    </header>
                    <div class="container-fluid rosh roshan" style="padding-top: 20px; padding-bottom: 20px;"></div>
                </div>
            </div>
        </div>
    </center>