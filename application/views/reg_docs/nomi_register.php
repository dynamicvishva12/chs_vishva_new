		<div class="row">

			<div class="col-12">

			 <div class="box">
			 	<div class="box-header with-border">
			 		<div class="row">
						<div class="col-8">
				  			
				  		</div>
				  		<div class="col-4 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addNomination">Add Nomination Register</button>
				  		</div>
					</div>
			 	</div>
				
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Member ID</th>
								<th>Date of Nomination</th>
								<th>Name of Nominee</th>
								<th>Date of Mang. Comm. meeting</th>
								<th>Date of subsequent revocation</th>
								<th>Remarks</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php if(!empty($nmRegData)): ?>
								<?php foreach($nmRegData AS $e_nm_data): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td>
										<center>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning" data-toggle="tooltip" data-original-title="Edit">
												<i class="fa fa-edit" data-toggle="modal" data-target="#editNomination_<?php echo $e_nm_data['nominee_register_id']; ?>"></i>
											</button>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it"  data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."reg_docs/del_nomi_register/".$e_nm_data['nominee_register_id']; ?>">
												<i class="fa fa-trash-o"></i>
											</button>
										</center>

										<!-- Popup Model Plase Here -->
										<div id="editNomination_<?php echo $e_nm_data['nominee_register_id']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<form class="form-horizontal" action="<?php echo base_url()."reg_docs/edit_nomi_register"; ?>" method="POST">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title" id="myModalLabel">Edit Nomination Register</h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														</div>
														<div class="modal-body">
															<div class="form-group">
																<label class="col-md-12">Name of member making nomination</label>
																<div class="col-md-12">
																	<select class="form-control select2" name="edit_nomi_member_id" style="width: 100%;">
																		<?php if(!empty($userData)): ?>
																			<?php foreach($userData AS $e_usr): ?>
																	  		<option value="<?php echo $e_usr['s-r-username']; ?>" <?php if($e_nm_data['nomi_member_id']==$e_usr['s-r-username']): ?> selected <?php endif; ?> >
																	  			<?php echo $e_usr['s-r-fname']." ".$e_usr['s-r-lname']; ?> (<?php echo $e_usr['s-r-username']; ?>)
																	  		</option>
																	  		<?php endforeach; ?>
																	  	<?php endif; ?>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-12">Name of nominee</label>
																<div class="col-md-12">
																	<input type="text" class="form-control" name="edit_nomi_name" placeholder="Name of nominee" value="<?php echo $e_nm_data['nomi_name']; ?>">
																</div>
															</div>

															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-12">Date of Mang. Comm. meeting</label>
																		<div class="col-md-12">
																			<input type="date" class="form-control" name="edit_mang_date" placeholder="Select Date" value="<?php echo date("Y-m-d", strtotime($e_nm_data['mang_date'])); ?>">
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-12">Date of nomination</label>
																		<div class="col-md-12">
																			<input type="date" class="form-control" name="edit_nomi_date" placeholder="Select Date" value="<?php echo date("Y-m-d", strtotime($e_nm_data['nomi_date'])); ?>">
																		</div>
																	</div>
																</div>
															</div>

															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-12">Date of subsequent revocation</label>
																		<div class="col-md-12">
																			<input type="date" class="form-control" name="edit_subs_revocation_date" placeholder="Select Date" value="<?php echo date("Y-m-d", strtotime($e_nm_data['subs_revocation_date'])); ?>">
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-12">Remarks</label>
																		<div class="col-md-12">
																			<input type="text" class="form-control" name="edit_remarks" placeholder="Remarks" value="<?php echo $e_nm_data['remarks']; ?>">
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<input type="hidden" name="nomiRegisterId" value="<?php echo $e_nm_data['nominee_register_id']; ?>">
															<button type="submit" class="btn btn-success float-right">Update</button>
															<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</td>
									<td><?php echo $e_nm_data['s-r-fname']." ".$e_nm_data['s-r-lname']; ?></td>
									<td><?php echo $e_nm_data['s-r-username']; ?></td>
									<td><?php echo date("d-m-Y", strtotime($e_nm_data['nomi_date'])); ?></td>
									<td><?php echo $e_nm_data['nomi_name']; ?></td>
									<td><?php echo date("d-m-Y", strtotime($e_nm_data['mang_date'])); ?></td>
									<td><?php echo date("d-m-Y", strtotime($e_nm_data['subs_revocation_date'])); ?></td>
									<td><?php echo $e_nm_data['remarks']; ?></td>
								</tr>
								<?php $i++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Member Name</th>
								<th>Member ID</th>
								<th>Date of Nomination</th>
								<th>Name of Nominee</th>
								<th>Date of Mang. Comm. meeting</th>
								<th>Date of subsequent revocation</th>
								<th>Remarks</th>
							</tr>
						</tfoot>
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>


<!-- Popup Model Plase Here -->
<div id="addNomination" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/add_nomi_register"; ?>" method="POST">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Nomination Register</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Name of member making nomination</label>
						<select class="form-control select2" name="nomi_member_id" style="width: 100%;">
							<?php if(!empty($userData)): ?>
								<?php foreach($userData AS $e_usr): ?>
						  		<option value="<?php echo $e_usr['s-r-username']; ?>" >
						  			<?php echo $e_usr['s-r-fname']." ".$e_usr['s-r-lname']; ?> (<?php echo $e_usr['s-r-username']; ?>)
						  		</option>
						  		<?php endforeach; ?>
						  	<?php endif; ?>
						</select>
					</div>
					<div class="form-group">
						<label class="col-md-12">Name of nominee</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="nomi_name" placeholder="Name of nominee">
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Date of Mang. Comm. meeting</label>
								<div class="col-md-12">
									<input type="date" class="form-control" name="mang_date" placeholder="Select Date">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Date of nomination</label>
								<div class="col-md-12">
									<input type="date" class="form-control" name="nomi_date" placeholder="Select Date">
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Date of subsequent revocation</label>
								<div class="col-md-12">
									<input type="date" class="form-control" name="subs_revocation_date" placeholder="Select Date">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Remarks</label>
								<div class="col-md-12">
									<input type="text" class="form-control" name="remarks" placeholder="Remarks">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>