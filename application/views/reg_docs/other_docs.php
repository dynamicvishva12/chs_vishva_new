<div class="box-header no-border">
	<div class="row">
		<div class="col-10">
				
		</div>
		<div class="col-2 text-right">
			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addDoc">Add Document</button>
		</div>
	</div>
</div>

<!-- START Card With Image -->
<div class="row">
	<?php if(!empty($docsArr)): ?>
		<?php foreach($docsArr AS $e_doc): ?>
			<?php $fileexe = strtolower(pathinfo($e_doc['document'],PATHINFO_EXTENSION)); ?>

			<div class="col-md-12 col-lg-4">
			  <div class="card">
			  	<?php if($fileexe=="pdf"): ?>
				  <img class="card-img-top img-responsive w-200 mx-auto mt-10" src="<?php echo $assets_url; ?>img/pdf.png" alt="Card image cap" width="500px" height="230px">
				<?php else: ?>
				  <img class="card-img-top img-responsive w-200 mx-auto mt-10" src="<?php echo $assets_url.'IMAGESDRIVE/'.$e_doc['document']; ?>" alt="Card image cap" width="500px" height="230px">
				<?php endif; ?>
				<div class="card-body">            	
					<h4 class="card-title"><?php echo $e_doc['doc_name']; ?></h4>
					<a href="<?php echo $assets_url.'IMAGESDRIVE/'.$e_doc['document']; ?>" class="btn btn-rounded btn-primary" download="" >Download</a>
				</div>
			  </div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<!-- /.row -->

<!-- Popup Model Plase Here -->
<div id="addDoc" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/upload_other_docs"; ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Document</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
				  	<div class="form-group">
						<label class="col-md-12">Document Name</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="docname" placeholder="Document Name">
						</div>
					</div>
					<div class="form-group callout callout-warning">
						<h4><i class="icon fa fa-info-circle"></i> Note:</h4>
						<ul>
							<li>File size must be 5MB or less.</li>
							<li>Only .png, .jpg, .jpeg & .pdf files are acceptable.</li>
						</ul>
					</div>  
				  	<div class="form-group">
					  	<label>File</label>
					  	<label class="file">
							<input type="file" id="file" name="myfile" />
					  	</label>
				  	</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>