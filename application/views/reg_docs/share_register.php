		

<div class="row">
	<div class="col-12">

	 <div class="box">
	 	<div class="box-header with-border">
	 		<div class="row">
				<div class="col-10">
		  			
		  		</div>
		  		<div class="col-2 text-right">
		  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addShare">Add Share Register</button>
		  		</div>
			</div>
	 	</div>
		
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
			  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				<thead>
					<tr>
						<th>Sr. No</th>
						<th>Action</th>
						<th>Date of Shares Allotment</th>
						<th>Cashbook Folio</th>
						<th>Share Cert & Sr. No</th>
						<th>No. of shares</th>
						<th>Value of shares</th>
						<th>Member Name</th>
						<th>Date of TRF</th>
						<th>Cashbook Jr. Folio</th>
						<th>Share Cert No.</th>
						<th>Share Value Tr.</th>
						<th>Name of Transferee</th>
						<th>Authority for Tr. Sign</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; ?>
					<?php if(!empty($shRegData)): ?>
						<?php foreach($shRegData AS $e_sh_data): ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-success" data-toggle="tooltip" data-original-title="Transfer">
										<i class="fa fa-exchange" data-toggle="modal" data-target="#transferShare_<?php echo $e_sh_data['shareRegisterId']; ?>"></i>
									</button>
									<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-warning"  data-toggle="tooltip" data-original-title="Edit" >
										<i class="fa fa-edit" data-toggle="modal" data-target="#edit_modal_<?php echo $e_sh_data['shareRegisterId']; ?>"></i>
									</button>
									<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."reg_docs/del_share_register/".$e_sh_data['shareRegisterId']; ?>">
										<i class="fa fa-trash-o"></i>
									</button>

									  <!-- Popup Model Plase Here -->
									<div id="edit_modal_<?php echo $e_sh_data['shareRegisterId']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<form class="form-horizontal" action="<?php echo base_url()."reg_docs/edit_share_register"; ?>" method="POST" enctype="multipart/form-data">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myModalLabel">Edit Share Register</h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Date of Allot Shares</label>
																	<div class="col-md-12">
																		<input type="date" class="form-control" placeholder="Select Date" name="edit_dateofallot" value="<?php echo date('Y-m-d', strtotime($e_sh_data['dateOfAllotedShares'])); ?>">
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Cashbook Folio</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Cashbook Folio" name="edit_CashBook" value="<?php echo $e_sh_data['cashbookFolio']; ?>">
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Share Certificate No.</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Share Certificate No." name="edit_certno" value="<?php echo $e_sh_data['shareCertNo']; ?>">
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">No. of Shares</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="No. of Shares" name="edit_shareno" value="<?php echo $e_sh_data['noOfShares']; ?>">
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Value of shares</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Value of shares" name="edit_sharevalue" value="<?php echo $e_sh_data['valueOfShares']; ?>">
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Name of member</label>
																	<div class="col-md-12">
																		<select class="form-control select2" name="edit_WhomShare" style="width: 100%;">
																			<?php if(!empty($userData)): ?>
																				<?php foreach($userData AS $e_usr): ?>
																		  		<option value="<?php echo $e_usr['s-r-username']; ?>" <?php if($e_usr['s-r-username']==$e_sh_data['memberId']): ?> selected <?php endif; ?> >
																		  			<?php echo $e_usr['s-r-fname']." ".$e_usr['s-r-lname']; ?> (<?php echo $e_usr['s-r-username']; ?>)
																		  		</option>
																		  		<?php endforeach; ?>
																		  	<?php endif; ?>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<input type="hidden" name="shareRegisterId" value="<?php echo $e_sh_data['shareRegisterId']; ?>" />
														<button type="submit" class="btn btn-success float-right" >Update</button>
														<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
													</div>
												</div>
											</form>
										</div>
									</div>

									<div id="transferShare_<?php echo $e_sh_data['shareRegisterId']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<form class="form-horizontal" action="<?php echo base_url()."reg_docs/trans_share_register"; ?>" method="POST" enctype="multipart/form-data">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myModalLabel">Transfer Share</h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Date of Transfer</label>
																	<div class="col-md-12">
																		<input type="date" class="form-control" placeholder="Select Date" name="transfer_date" >
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Cashbook Jr. Folio</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Cashbook Jr. Folio" name="cashbook_folio" >
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Share Certificate No.</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Share Certificate No." name="share_cert_no" >
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Share Value Tr.</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Share Value Tr." name="share_value">
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Name of Transferee</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Name of Transferee" name="transferee">
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label class="col-md-12">Authority for Tr. Sign</label>
																	<div class="col-md-12">
																		<input type="text" class="form-control" placeholder="Authority for Tr. Sign" name="authority_sign">
																	</div>
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-12">Remarks</label>
															<div class="col-md-12">
																<input type="text" class="form-control" placeholder="Remarks" name="remarks" >
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<input type="hidden" name="transShareRegisterId" value="<?php echo $e_sh_data['shareRegisterId']; ?>" />
														<button type="submit" class="btn btn-success float-right">Transfer</button>
														<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
													</div>
												</div>
											</form>
										</div>
									</div>

						 		</td>
								<td><?php echo date('d-m-Y', strtotime($e_sh_data['dateOfAllotedShares'])); ?></td>
								<td><?php echo $e_sh_data['cashbookFolio']; ?></td>
								<td><?php echo $e_sh_data['shareCertNo']; ?> - (<?php echo $e_sh_data['count_share_reg']; ?>)</td>
								<td><?php echo $e_sh_data['noOfShares']; ?></td>
								<td><?php echo $e_sh_data['valueOfShares']; ?></td>
								<td><?php echo $e_sh_data['s-r-fname']." ".$e_sh_data['s-r-lname']; ?></td>
								<td><?php echo date('d-m-Y', strtotime($e_sh_data['transfer_date'])); ?></td>
								<td><?php echo $e_sh_data['cashbook_jr_folio']; ?></td>
								<td><?php echo $e_sh_data['share_cert_no']; ?></td>
								<td><?php echo $e_sh_data['share_value']; ?></td>
								<td><?php echo $e_sh_data['transferee']; ?></td>
								<td><?php echo $e_sh_data['authority_sign']; ?></td>
								<td><?php echo $e_sh_data['remarks']; ?></td>
							</tr>
						<?php $i++; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
				<tfoot>
					<tr>
						<th>Sr. No</th>
						<th>Action</th>
						<th>Date of Shares Allotment</th>
						<th>Cashbook Folio</th>
						<th>Share Cert & Sr. No</th>
						<th>No. of shares</th>
						<th>Value of shares</th>
						<th>Member Name</th>
						<th>Date of TRF</th>
						<th>Cashbook Jr. Folio</th>
						<th>Share Cert No.</th>
						<th>Share Value Tr.</th>
						<th>Name of Transferee</th>
						<th>Authority for Tr. Sign</th>
						<th>Remarks</th>
					</tr>
				</tfoot>
			</table>
			</div>
		</div>
		<!-- /.box-body -->
	  </div>
	  <!-- /.box -->      
	</div>
	<!-- /.col -->
  </div>


  <!-- Popup Model Plase Here -->
<div id="addShare" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."reg_docs/add_share_register"; ?>" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Share Register</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Date of Allot Shares</label>
								<div class="col-md-12">
									<input type="date" class="form-control" placeholder="Select Date" name="dateofallot">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Cashbook Folio</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Cashbook Folio" name="CashBook">
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Share Certificate No.</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Share Certificate No." name="certno">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">No. of Shares</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="No. of Shares" name="shareno">
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Value of shares</label>
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Value of shares" name="sharevalue">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Name of member</label>
								<select class="form-control select2" name="WhomShare" style="width: 100%;">
									<?php if(!empty($userData)): ?>
										<?php foreach($userData AS $e_usr): ?>
								  		<option value="<?php echo $e_usr['s-r-username']; ?>" >
								  			<?php echo $e_usr['s-r-fname']." ".$e_usr['s-r-lname']; ?> (<?php echo $e_usr['s-r-username']; ?>)
								  		</option>
								  		<?php endforeach; ?>
								  	<?php endif; ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
