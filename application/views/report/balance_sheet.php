		<!-- tabs -->
		  <div class="row">

			<div class="col-12">

				 <div class="box">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-12 text-center">
					  			<h4 class="box-title">
					  				<STRONG><?php echo $soc_details['soc_name']; ?>.</STRONG><br>
									<?php echo nl2br($soc_details['soc_address']); ?>,<BR>
									REGISTRATION NO:<?php echo $soc_details['soc_register_no']; ?>
								</h4>
					  		</div>
						</div>
				 		<div class="row p-15">
							<div class="col-10">
					  			<h3 class="box-title">
					  				BALANCE SHEET AS ON 31st MARCH
								</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  		</div>
						</div>
						<form class="form-horizontal p-15" method="post" action="">
							<div class="form-group row">
								<label class="col-md-2">Financial Year:</label>
						  		<div class="col-md-8">
						  			<select class="form-control select2" name="financial" id="selectbasic" style="width: 100%;">
										<?php 
											$currmon1= date("m");
											if ($currmon1>=4) 
											{
												$curryear1= date("Y")."-04-01";
												$nxt_yr1=(date("Y")+1)."-03-31";
											}
											else
											{
												$curryear1= (date("Y")-1)."-04-01";
												$nxt_yr1=date("Y")."-03-31";
											}
											$year1=date("Y",strtotime($curryear1));
											$year2=date("Y",strtotime($nxt_yr1));
											$view_yr=$year1."-".$year2;
											$value=$year1."-04-01"."_".$year2."-03-31";
										?>
										<!-- <option value="<?php //echo $value; ?>" <?php //echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php //echo $view;  ?></option> -->
										<option value="<?php echo $value; ?>" <?php echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php echo $view_yr;  ?></option>
										<?php
											for($i=1;$i<10;$i++)
											{
												$year1=date("Y",strtotime($curryear1))-$i;
												$year2=date("Y",strtotime($nxt_yr1))-$i;
												$view_yr=$year1."-".$year2;
												$value=$year1."-04-01"."_".$year2."-03-31";
										?>
										<!-- <option value="<?php //echo $value; ?>" <?php //echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php //echo $view;  ?></option> -->
										<option value="<?php echo $value; ?>" <?php echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php echo $view_yr;  ?></option>
										<?php 
											}
										?>
									</select>
						  		</div>
						  		<div class="col-md-2">
						  			<button type="submit" name="Bal_search" class="btn btn-success float-right">View Records</button>
						  		</div>
							</div>
						</form>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
						  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr No</th>
									<th>LAST YEAR RS</th>
									<th>LIABILITIES</th>
									<th>CURRENT YEAR RS</th>
									<th>LAST YEAR RS</th>
									<th>ASSETS</th>
									<th>CURRENT YEAR RS</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$x=1;
								$ass_total=0;
								$lib_total=0;
								$ass_curr_total=0;
								$lib_curr_total=0;
								if(!empty($balance_sheet_data))
								{
									foreach($balance_sheet_data as $key => $value)
									{
							?>  
							<tr>
								<td><?php echo $x++; ?></td>
								<?php if($value['lib_heading']=="Group"){ ?>
								<td align="center">
									<?php
										if(!empty($value['lib_Name']))
										{
											if($value['lib_balance']!="0")
											{
												echo "<b>".number_format($value['lib_balance'])."</b>"; 
											}
											else
											{
												echo "-";  
											}
										}
								?>
								</td>
								<?php 
									}
									else
									{ 
								?>
								<td align="right">
								<?php
									if(!empty($value['lib_Name']))
									{
										if($value['lib_balance']!="0")
										{
											echo number_format($value['lib_balance']); 
										}
										else
										{
											echo "-";  
										}
									}
								?> 
								</td>
								<?php  } ?>
								<td align="center">
									<?php
										if($value['lib_heading']=="Group")
										{
											echo "<b>".$value['lib_Name']."</b>"; 
											$lib_total+=$value['lib_balance'];
											$lib_curr_total+=$value['lib_crr_balance'];
										}
										else
										{
											echo $value['lib_Name']; 
										} 
									?>
								</td>
								<?php 
									if($value['lib_heading']=="Group")
									{ 
								?>
								<td align="center">
									<?php
										if(!empty($value['lib_Name']))
										{
											echo "<b>".number_format($value['lib_crr_balance'])."</b>"; 
										}
									?>
								</td>
								<?php 
									}
									else
									{ 
								?>
								<td align="left">
									<?php
										if(!empty($value['lib_Name']))
										{
											echo number_format($value['lib_crr_balance']); 
										}
									?> 
								</td>
								<?php  
									}
								?>
								<?php 
									if($value['asst_heading']=="Group")
									{ 
								?>
								<td align="center">
									<?php
										if(!empty($value['asst_Name']))
										{
											echo "<b>".number_format($value['asst_balance'])."</b>"; 
										}
									?>
								</td>
								<?php 
									}
									else
									{ 
								?>
								<td align="right">
									<?php
										if(!empty($value['asst_Name']))
										{
											echo number_format($value['asst_balance']); 
										}
									?> 
								</td>
								<?php  
									} 
								?>
								<td align="center">
									<?php 
										if($value['asst_heading']=="Group")
										{
											echo "<b>".$value['asst_Name']."</b>"; 
											$ass_total+=$value['asst_balance'];
											$ass_curr_total+=$value['asst_crr_balance'];
										}
										else
										{
											echo $value['asst_Name']; 
										} 
									?>
								</td>
								<?php 
									if($value['asst_heading']=="Group")
									{ 
								?>
								<td align="center">
									<?php
										if(!empty($value['asst_Name']))
										{
											echo "<b>".number_format($value['asst_crr_balance'])."</b>"; 
										}
									?>
								</td>
								<?php 
									}
									else
									{ 
								?>
								<td align="left">
									<?php
										if(!empty($value['asst_Name']))
										{
											echo number_format($value['asst_crr_balance']); 
										}
									?> 
								</td>
								<?php  
									}
								?>
							</tr>
							<?php  
									} 
								} 
							?>
							</tbody>
							<tbody>
								<tr>
									<td></td>
									<td align="center"><b><?php echo number_format($lib_total); ?></b></td>
									<td align="center"><b>TOTAL LIABILITIES</b></td>
									<td align="center"><b><?php echo number_format($lib_curr_total); ?></b></td>
									<td align="center"><b><?php echo number_format($ass_total); ?></b></td>
									<td align="center"><b>TOTAL ASSETS</b></td>
									<td align="center"><b><?php echo number_format($ass_curr_total); ?></b></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th>Sr No</th>
									<th>LAST YEAR RS</th>
									<th>LIABILITIES</th>
									<th>CURRENT YEAR RS</th>
									<th>LAST YEAR RS</th>
									<th>ASSETS</th>
									<th>CURRENT YEAR RS</th>
								</tr>
							</tfoot>
						</table>
						</div>
					</div>
					<!-- /.box-body -->
				  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->