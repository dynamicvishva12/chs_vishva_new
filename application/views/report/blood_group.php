		<!-- tabs -->
		  <div class="row">

			<div class="col-12">

				 <div class="box">
					<div class="box-header with-border">
				 		<div class="row">
							<div class="col-10">
					  			<h3 class="box-title">Blood Groups</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button class="waves-effect btn btn-social-icon btn-info" data-toggle="tooltip" data-original-title="Print" onclick="printContent('div1')">
									<i class="fa fa-print"></i>
								</button>
					  		</div>
						</div>
						<form class="form-horizontal" method="post" action="" id="blood_group_div">
							<div class="form-group row">
								<label class="col-md-2">Blood Group:</label>
						  		<div class="col-md-8">
						  			<select class="form-control select2" name="blood" id="selectbasic" style="width: 100%;">
										<option value="show">Show All</option>
										<option value="A+" <?php if($sel_blood_grp=="A+"): ?> selected <?php endif; ?>>A+</option>
										<option value="A-" <?php if($sel_blood_grp=="A-"): ?> selected <?php endif; ?>>A-</option>
										<option value="B+" <?php if($sel_blood_grp=="B+"): ?> selected <?php endif; ?>>B+</option>
										<option value="B-" <?php if($sel_blood_grp=="B-"): ?> selected <?php endif; ?>>B-</option>
										<option value="AB+" <?php if($sel_blood_grp=="AB+"): ?> selected <?php endif; ?>>AB+</option>
										<option value="AB-" <?php if($sel_blood_grp=="AB-"): ?> selected <?php endif; ?>>AB-</option>
										<option value="O+" <?php if($sel_blood_grp=="O+"): ?> selected <?php endif; ?>>O+</option>
										<option value="O-" <?php if($sel_blood_grp=="O-"): ?> selected <?php endif; ?>>O-</option>
									</select>
						  		</div>
							</div>
						</form>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive" id="div1">
						  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr. No.</th>
									<th>Family's Main member</th>
									<th>Membername</th>
									<th>Contact No</th>
									<th>Blood Group</th>
									<th>Wish To Donate</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; ?>
								<?php if(!empty($result)): ?>
									<?php foreach($result AS $no): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $no['user_name']; ?></td>
									<td><?php echo $no['name']; ?></td>
									<td><?php echo $no['contact']; ?></td>
									<td><?php echo $no['blood_type']; ?></td>
									<td><?php echo $no['wish_to_donate']; ?></td>
								</tr>
								<?php $i++; ?>
								<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Sr. No.</th>
									<th>Family's Main member</th>
									<th>Membername</th>
									<th>Contact No</th>
									<th>Blood Group</th>
									<th>Wish To Donate</th>
								</tr>
							</tfoot>
						</table>
						</div>
					</div>
					<!-- /.box-body -->
				  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->

<script>

	function printContent(el)
	{
		var restorepage = document.body.innerHTML;
		var printcontent = document.getElementById(el).innerHTML;
		document.body.innerHTML = printcontent;
		window.print();
		document.body.innerHTML = restorepage;
	}

</script>

<script>
  $(document).ready(function(){

	$("#selectbasic").change(function(){
		$('#blood_group_div').submit();
	});
});
</script>