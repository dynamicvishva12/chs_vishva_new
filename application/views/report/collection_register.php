		<!-- tabs -->
		  <div class="row">

			<div class="col-12">

				 <div class="box">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-12 text-center">
					  			<h4 class="box-title">
					  				<STRONG><?php echo $soc_details['soc_name']; ?>.</STRONG><br>
									<?php echo nl2br($soc_details['soc_address']); ?>,<BR>
									REGISTRATION NO:<?php echo $soc_details['soc_register_no']; ?>
								</h4>
					  		</div>
						</div>
				 		<div class="row p-15">
							<div class="col-10">
					  			<h3 class="box-title">
					  				MEMBER MONTHLY COLLECTION REGISTER <?php echo date("d-m-Y",strtotime($m_date1)); ?> TO <?php echo date("d-m-Y",strtotime($m_date2)); ?>
								</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  		</div>
						</div>
						<form class="form-horizontal p-15" method="post" action="" id="blood_group_div">
							<div class="form-group row">
								<label class="col-md-2">Financial Year:</label>
						  		<div class="col-md-8">
						  			<select class="form-control select2" name="financial" id="selectbasic" style="width: 100%;">
										<option selected value='0'>--Select Month--</option>
										<?php 
											for($i=0;$i<12;$i++)
											{
										?>
										<!-- <option value="<?php //echo date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?>" <?php //if(date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))==($m_date1."_".$m_date2)){ echo "selected"; } ?>><?php //echo date("F",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?></option> -->

										<option value="<?php echo date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?>" <?php if(date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))==set_value('financial')){ echo "selected"; } ?>><?php echo date("F",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?></option>
										<?php 
											}
										?>
									</select>
						  		</div>
						  		<div class="col-md-2">
						  			<button type="submit" name="collection_search" class="btn btn-success float-right">View Records</button>
						  		</div>
							</div>
						</form>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
						  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr No</th>
									<th>Month</th>
									<th>Bill Number</th>
									<th>Bill Date</th>
									<?php 
										if(!empty($headers_view))
										{
											foreach($headers_view as $key=> $name)
											{ 
									?>
									<th><?php echo $name['head_name']; ?> </th>
									<?php 
											} 
										} 
									?>
									<th>Intrest on Arrears </th>
									<th>Total dues</th>
									<th> Balance due</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$x=1;
								if(!empty($resultsCash))
								{
									foreach($resultsCash as $list => $sview)
									{
							?>
								<tr>
									<td><?php echo $x++;  ?></td>
									<td><?php echo date("F",strtotime($sview['bill_dt']));  ?></td>
									<td><?php echo $sview['tansaction_no1'];  ?></td>
									<td><?php echo  $sview['bill_dt'];  ?></td>
									<?php 
										if(isset($MaintenanceParticular[$list]))
										{
											$maint_charge=$MaintenanceParticular[$list];
											if($sview['tansaction_no1']=="trn_2019-04")
											{
												print_r($sview);
												print_r($maint_charge);
											}

											foreach($maint_charge as $list1 => $view1)
											{
									?>
									<td><?php echo $view1['Charges'];  ?></td>
									<?php 
											} 
										} 
									?>
									<td><?php echo $sview['intrest']; ?></td>
									<td><?php echo $sview['MaintenanceSubTotal'];  ?></td>
									<td><?php echo $sview['MaintenanceGrandTotal'];  ?></td>
								</tr>
							<?php 
									} 
								} 
							?>
							</tbody>
							<tfoot>
								<tr>
									<th>Sr No</th>
									<th>Month</th>
									<th>Bill Number</th>
									<th>Bill Date</th>
									<?php 
										if(!empty($headers_view))
										{
											foreach($headers_view as $key=> $name)
											{ 
									?>
									<th><?php echo $name['head_name']; ?> </th>
									<?php 
											} 
										} 
									?>
									<th>Intrest on Arrears </th>
									<th>Total dues</th>
									<th> Balance due</th>
								</tr>
							</tfoot>
						</table>
						</div>
					</div>
					<!-- /.box-body -->
				  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->