<style type="text/css">
	.class88
	{
		background:#212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>

<div class="row">
	<div class="col-md-12">
		<center>
			<h3>
				<STRONG><?php echo $soc_details['soc_name']; ?>.</STRONG><br>
				<?php echo nl2br($soc_details['soc_address']); ?>,<BR>
				REGISTRATION NO:<?php echo $soc_details['soc_register_no']; ?>
			</h3>
		</center>
		<div class="card">
			<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
				<h4 class="title">
					MEMBER MONTHLY COLLECTION REGISTER <?php echo date("d-m-Y",strtotime($m_date1)); ?> TO <?php echo date("d-m-Y",strtotime($m_date2)); ?>
				</h4>
			</div>
			<?php $exptotal=$inctotal=0; ?>
			<div class="card-content table-responsive">
				<form class="form-group" method="post" action="<?php echo base_url()."report/collection_register"; ?>">
					<div class="row" style="padding-left: 30px;">
						<div class="col-md-5">
							<select id="selectbasic" name="financial" style="height: 40px; width: 100%;">
								<option selected value='0'>--Select Month--</option>
								<?php 
									for($i=0;$i<12;$i++)
									{
								?>
								<!-- <option value="<?php //echo date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?>" <?php //if(date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))==($m_date1."_".$m_date2)){ echo "selected"; } ?>><?php //echo date("F",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?></option> -->

								<option value="<?php echo date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?>" <?php if(date("Y-m-d",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))."_".date("Y-m-t",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))))==set_value('financial')){ echo "selected"; } ?>><?php echo date("F",strtotime("+$i Months",strtotime(date("Y-m-d",strtotime($curryear)))));  ?></option>
								<?php 
									}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" name="collection_search" type="submit" style="background: #212121; color: #fff;">
								View Records
							</button>
						</div>
					</div>
				</form>
				<table class="table table-bordered" id="table_id">
					<thead>
						<th>Sr No</th>
						<th>Month</th>
						<th>Bill Number</th>
						<th>Bill Date</th>
						<?php 
							if(!empty($headers_view))
							{
								foreach($headers_view as $key=> $name)
								{ 
						?>
						<th><?php echo $name['head_name']; ?> </th>
						<?php 
								} 
							} 
						?>
						<th>Intrest on Arrears </th>
						<th>Total dues</th>
						<th> Balance due</th>
					</thead>
					<tbody> 
					<?php 
						$x=1;
						if(!empty($resultsCash))
						{
							foreach($resultsCash as $list => $sview)
							{
					?>
						<tr>
							<td><?php echo $x++;  ?></td>
							<td><?php echo date("F",strtotime($sview['bill_dt']));  ?></td>
							<td><?php echo $sview['tansaction_no1'];  ?></td>
							<td><?php echo  $sview['bill_dt'];  ?></td>
							<?php 
								if(isset($MaintenanceParticular[$list]))
								{
									$maint_charge=$MaintenanceParticular[$list];
									if($sview['tansaction_no1']=="trn_2019-04")
									{
										print_r($sview);
										print_r($maint_charge);
									}

									foreach($maint_charge as $list1 => $view1)
									{
							?>
							<td><?php echo $view1['Charges'];  ?></td>
							<?php 
									} 
								} 
							?>
							<td><?php echo $sview['intrest']; ?></td>
							<td><?php echo $sview['MaintenanceSubTotal'];  ?></td>
							<td><?php echo $sview['MaintenanceGrandTotal'];  ?></td>
						</tr>
					<?php 
							} 
						} 
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>