<style type="text/css">
	.class88
	{
		background:#212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>

<div class="row">
	<div class="col-md-12">
		<center>
			<h3>
				<STRONG><?php echo $soc_details['soc_name']; ?>.</STRONG><br>
				<?php echo nl2br($soc_details['soc_address']); ?>,<BR>
				REGISTRATION NO:<?php echo $soc_details['soc_register_no']; ?>
			</h3>
		</center>
		<div class="card">
			<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
				<h4 class="title">
					PROFIT LOSS A/C FOR THE PERIOD <?php echo date("d-m-Y",strtotime($curryear)); ?> TO <?php echo date("d-m-Y",strtotime($nxt_yr)); ?>
				</h4>
			</div>
			<?php 
				$exptotal=$inctotal=0;
			?>
			<div class="card-content table-responsive">
				<form class="form-group" method="post" action="<?php echo base_url()."report/profit_loss"; ?>">
					<div class="row" style="padding-left: 30px;">
						<div class="col-md-5">
							<select id="selectbasic" name="financial" style="height: 40px; width: 100%;">
								<option selected value='0'>--Select Month--</option>
								<?php 
									$currmon1= date("m");
									if($currmon1>=4) 
									{
										$curryear1= date("Y")."-04-01";
										$nxt_yr1=(date("Y")+1)."-03-31";
									}
									else
									{
										$curryear1= (date("Y")-1)."-04-01";
										$nxt_yr1=date("Y")."-03-31";
									}
									$year1=date("Y",strtotime($curryear1));
									$year2=date("Y",strtotime($nxt_yr1));
									$view=$year1."-".$year2;
									$value=$year1."-04-01"."_".$year2."-03-31";
								?>, 
								<!-- <option value="<?php //echo $value; ?>" <?php //echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php //echo $view;  ?></option> -->
								<option value="<?php echo $value; ?>" <?php echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php echo $view;  ?></option>
								<?php
									for($i=1;$i<10;$i++)
									{
										$year1=date("Y",strtotime($curryear1))-$i;
										$year2=date("Y",strtotime($nxt_yr1))-$i;
										$view=$year1."-".$year2;
										$value=$year1."-04-01"."_".$year2."-03-31";
								?>
								<!-- <option value="<?php //echo $value; ?>" <?php //echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php //echo $view;  ?></option> -->
								<option value="<?php echo $value; ?>" <?php echo set_select('financial', $value, $value==$curryear."_".$nxt_yr ? TRUE:FALSE); ?> ><?php echo $view;  ?></option>
								<?php 
									}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" name="pnl_search" type="submit" style="background: #212121; color: #fff;">
								View Records
							</button>
						</div>
					</div>
				</form>
				<table class="display" id="table_id" style="width: 100%;">
					<thead>
						<th>Sr No</th>
						<th>LAST YEAR RS</th>
						<th>Expense</th>
						<th>LAST YEAR RS</th>
						<th>Income</th>
						<th>CURRENT YEAR RS</th>
					</thead>
					<tbody>
					<?php 
						$x=1;
						$ass_total=0;
						$lib_total=0;
						$ass_curr_total=0;
						$lib_curr_total=0;
						if(!empty($pnl_sheet_data))
						{
							foreach($pnl_sheet_data as $key => $value)
							{
					?>  
						<tr>
							<td><?php echo $x++; ?></td>
							<?php if($value['lib_heading']=="Group"){ ?>
							<td align="center">
								<?php
									if(!empty($value['lib_Name']))
									{
										if($value['lib_balance']!="0")
										{
											echo "<b>".number_format($value['lib_balance'])."</b>"; 
										}
										else
										{
											echo "-";  
										}
									}
								?>
							</td>
							<?php }else{ ?>
							<td align="right">
								<?php
									if(!empty($value['lib_Name']))
									{
										if($value['lib_balance']!="0")
										{
											echo number_format($value['lib_balance']); 
										}
										else
										{
											echo "-";  
										}
									}
								?> 
							</td>
							<?php } ?>
							<td align="center">
								<?php
									if($value['lib_heading']=="Group")
									{
										echo "<b>".$value['lib_Name']."</b>"; 
										$lib_total+=$value['lib_balance'];
										$lib_curr_total+=$value['lib_crr_balance'];
									}
									else
									{
										echo $value['lib_Name']; 
									}  
								?>
							</td>
							<?php if($value['lib_heading']=="Group"){ ?>
							<td align="center">
								<?php
									if(!empty($value['lib_Name']))
									{
										echo "<b>".number_format($value['lib_crr_balance'])."</b>"; 
									}
								?>
							</td>
							<?php }else{ ?>
							<td align="left">
								<?php
									if(!empty($value['lib_Name']))
									{
										echo number_format($value['lib_crr_balance']); 
									}
								?> 
							</td>
							<?php } ?>
							<?php if($value['asst_heading']=="Group"){ ?>
							<td align="center">
								<?php
									if(!empty($value['asst_Name']))
									{
										echo "<b>".number_format($value['asst_balance'])."</b>"; 
									}
								?>
							</td>
							<?php }else{ ?>
							<td align="right">
								<?php
									if(!empty($value['asst_Name']))
									{
										echo number_format($value['asst_balance']); 
									}
								?> 
							</td>
							<?php } ?>
							<td align="center">
								<?php 
									if($value['asst_heading']=="Group")
									{
										echo "<b>".$value['asst_Name']."</b>"; 
										$ass_total+=$value['asst_balance'];
										$ass_curr_total+=$value['asst_crr_balance'];
									}
									else
									{
										echo $value['asst_Name']; 
									} 
								?>
							</td>
							<?php if($value['asst_heading']=="Group"){ ?>
							<td align="center">
								<?php
									if(!empty($value['asst_Name']))
									{
										echo "<b>".number_format($value['asst_crr_balance'])."</b>"; 
									}
								?>
							</td>
							<?php }else{ ?>
							<td align="left">
								<?php
									if(!empty($value['asst_Name']))
									{
										echo number_format($value['asst_crr_balance']); 
									}
								?> 
							</td>
							<?php } ?>
						</tr>
						<?php  
								} 
							} 
						?>
						</tbody>
						<tbody>
						<?php
							$net_profit=$ass_total-$lib_total;
							$headprofit=$headprofitvalue=$headlossname=$headlossvalue="-";

							if($net_profit>0)
							{
								$headprofit="Net Profit";
								$headprofitvalue=$net_profit;
								$lib_total+=$net_profit;
							}
							else
							{
								$headlossname="Net Loss";
								$headlossvalue=abs($net_profit);
								$ass_total+=$net_profit;
							}
						?>
						<tr>
							<td> </td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($headprofitvalue))
							                echo number_format($headprofitvalue); 
							            else
							                echo $headprofitvalue; 
							        ?>
							    </b>
							</td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($headprofit))
							                echo number_format($headprofit); 
							            else
							                echo $headprofit; 
							        ?>
							    </b>
							</td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($ass_curr_total))
							                echo number_format($ass_curr_total); 
							            else
							                echo $ass_curr_total; 
							        ?>
							    </b>
							</td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($headlossvalue))
							                echo number_format($headlossvalue); 
							            else
							                echo $headlossvalue; 
							        ?>
							    </b>
							</td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($headlossname))
							                echo number_format($headlossname); 
							            else
							                echo $headlossname; 
							        ?>
							    </b>
							</td>
							<td align="center">
							    <b>
							        <?php 
							            if(is_numeric($ass_curr_total))
							                echo number_format($ass_curr_total); 
							            else
							                echo $ass_curr_total; 
							        ?>
							    </b>
							</td>
						</tr>
						<tr>
							<td> </td>
							<td align="center"><b><?php echo number_format($lib_total); ?></b></td>
							<td align="center"><b>TOTAL Expense</b></td>
							<td align="center"><b><?php echo number_format($lib_curr_total); ?></b></td>
							<td align="center"><b><?php echo number_format($ass_total); ?></b></td>
							<td align="center"><b>TOTAL Income</b></td>
							<td align="center"><b><?php echo number_format($ass_curr_total); ?></b></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>