		<!-- tabs -->
		  <div class="row">

			<div class="col-12">

				 <div class="box">
					<div class="box-header with-border">
				 		<div class="row">
							<div class="col-10">
					  			<h3 class="box-title">TRIAL BALANCE (Financial Year <?php echo $curryear."-".$nxt_yr; ?>)</h3>
					  		</div>
					  		<div class="col-2 text-right">
					  		</div>
						</div>
						<form class="form-horizontal p-15" method="post" action="" id="blood_group_div">
							<div class="form-group row">
								<label class="col-md-2">Financial Year:</label>
						  		<div class="col-md-8">
						  			<select class="form-control select2" name="financial" id="selectbasic" style="width: 100%;">
										<option selected value='0'>--Select Month--</option>
										<?php 
											$currmon1= date("m");
											if ($currmon1>=4) 
											{
												$curryear1= date("Y")."-04-01";
												$nxt_yr1=(date("Y")+1)."-03-31";
											}
											else
											{
												$curryear1= (date("Y")-1)."-04-01";
												$nxt_yr1=date("Y")."-03-31";
											}
											$year1=date("Y",strtotime($curryear1));
											$year2=date("Y",strtotime($nxt_yr1));
											$view_yr=$year1."-".$year2;
											$value=$year1."-04-01"."_".$year2."-03-31";
										?>
										<option value="<?php echo $value; ?>" <?php if($value==($curryear."_".$nxt_yr)){ echo "selected"; } ?>>	<?php echo $view_yr;  ?>
										</option>
										<?php
											for($i=1;$i<10;$i++)
											{
												$year1=date("Y",strtotime($curryear1))-$i;
												$year2=date("Y",strtotime($nxt_yr1))-$i;
												$view_yr=$year1."-".$year2;
												$value=$year1."-04-01"."_".$year2."-03-31";
										?>
											<option value="<?php echo $value;  ?>" <?php if($value==($curryear."_".$nxt_yr)){ echo "selected"; } ?>>
												<?php echo $view_yr;  ?>
											</option>
										<?php 
											}
										?>
									</select>
						  		</div>
						  		<div class="col-md-2">
						  			<button type="submit" name="search" class="btn btn-success float-right">View Records</button>
						  		</div>
							</div>
						</form>
				 	</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="table-responsive">
						  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>SR NO.</th>
									<th>ACCOUNT HEAD</th>
									<th>L.F. No.</th>
									<th>DEBIT</th>
									<th>CREDIT</th>
								</tr>
							</thead>
							<tbody>
								<?php  
									$x=1;
									//print_r(json_encode($trail_balance_data));
									$drsum=$crsum=0;
									foreach($trail_balance_data as $key => $value)
									{
								?>
								<tr>
									<td><?php echo $x++; ?></td>
									<td>
										<?php 
											if($value['ladger_type']=="Group")
											{ 
												echo  "<b>".$value['accounting_name']."</b>"; 
											}
											else
											{ 
												echo $value['accounting_name']; 
											} 
										?>
									</td>
									<td><?php echo $value['accounting_id']; ?></td>
									<td>
										<?php 
											if($value['drsum']=="0")
											{ 
												echo "-";  
											}
											else
											{ 
												if($value['ladger_type']=="Group")
												{
													$drsum+=$value['drsum'];
													echo  "<b><u>".number_format($value['drsum'])."</u></b>"; 
												}
												else
												{ 
													echo number_format($value['drsum']); 
												} 
											}
										?>
									</td>
									<td>
										<?php 
											if($value['crsum']=="0")
											{ 
												echo "-";  
											}
											else
											{
												if($value['ladger_type']=="Group")
												{
													$crsum+=$value['crsum'];
													echo  "<b><u>".number_format($value['crsum'])."</u></b>"; 
												}
												else
												{ 
													echo number_format($value['crsum']); 
												}
											} 
										?>
									</td>
								</tr>
								<?php 	} 	?>
							</tbody>
							<tfoot>
								<tr>
									<th>SR NO.</th>
									<th>ACCOUNT HEAD</th>
									<th>L.F. No.</th>
									<th>DEBIT</th>
									<th>CREDIT</th>
								</tr>
							</tfoot>
						</table>
						</div>
					</div>
					<!-- /.box-body -->
				  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->