<style type="text/css">
	.class88
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>


<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
				<h4 class="title">TRIAL BALANCE (Financial Year <?php echo $curryear."-".$nxt_yr; ?>)</h4>
			</div>
			<div class="card-content">
				<form class="form-group" method="post" action="<?php echo base_url()."report/trail_balance"; ?>">
					<div class="row" style="padding-left: 30px;">
						<div class="col-md-5">
							<select id="selectbasic" name="financial" style="height: 40px; width: 100%;">
								<option selected value='0'>--Select Month--</option>
								<?php 
									$currmon1= date("m");
									if ($currmon1>=4) 
									{
										$curryear1= date("Y")."-04-01";
										$nxt_yr1=(date("Y")+1)."-03-31";
									}
									else
									{
										$curryear1= (date("Y")-1)."-04-01";
										$nxt_yr1=date("Y")."-03-31";
									}
									$year1=date("Y",strtotime($curryear1));
									$year2=date("Y",strtotime($nxt_yr1));
									$view_yr=$year1."-".$year2;
									$value=$year1."-04-01"."_".$year2."-03-31";
								?>
								<option value="<?php echo $value; ?>" <?php if($value==($curryear."_".$nxt_yr)){ echo "selected"; } ?>>	<?php echo $view_yr;  ?>
								</option>
								<?php
									for($i=1;$i<10;$i++)
									{
										$year1=date("Y",strtotime($curryear1))-$i;
										$year2=date("Y",strtotime($nxt_yr1))-$i;
										$view_yr=$year1."-".$year2;
										$value=$year1."-04-01"."_".$year2."-03-31";
								?>
									<option value="<?php echo $value;  ?>" <?php if($value==($curryear."_".$nxt_yr)){ echo "selected"; } ?>>
										<?php echo $view_yr;  ?>
									</option>
								<?php 
									}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" name="search" type="submit" style="background: #212121; color: #fff;">
							View Records
							</button>
						</div>
					</div>
				</form>
				<table class="display" id="mytable" style="width: 100%;">
					<thead>
						<th>SR NO.</th>
						<th>ACCOUNT HEAD</th>
						<th>L.F. No.</th>
						<th>DEBIT</th>
						<th>CREDIT</th>
					</thead>
					<tbody>
						<?php  
							$x=1;
							//print_r(json_encode($trail_balance_data));
							$drsum=$crsum=0;
							foreach($trail_balance_data as $key => $value)
							{
						?>
						<tr>
							<td><?php echo $x++; ?></td>
							<td>
								<?php 
									if($value['ladger_type']=="Group")
									{ 
										echo  "<b>".$value['accounting_name']."</b>"; 
									}
									else
									{ 
										echo $value['accounting_name']; 
									} 
								?>
							</td>
							<td><?php echo $value['accounting_id']; ?></td>
							<td>
								<?php 
									if($value['drsum']=="0")
									{ 
										echo "-";  
									}
									else
									{ 
										if($value['ladger_type']=="Group")
										{
											$drsum+=$value['drsum'];
											echo  "<b><u>".number_format($value['drsum'])."</u></b>"; 
										}
										else
										{ 
											echo number_format($value['drsum']); 
										} 
									}
								?>
							</td>
							<td>
								<?php 
									if($value['crsum']=="0")
									{ 
										echo "-";  
									}
									else
									{
										if($value['ladger_type']=="Group")
										{
											$crsum+=$value['crsum'];
											echo  "<b><u>".number_format($value['crsum'])."</u></b>"; 
										}
										else
										{ 
											echo number_format($value['crsum']); 
										}
									} 
								?>
							</td>
						</tr>
						<?php 	} 	?>
					</tbody>
					<tbody>
					<tr>
						<td><?php $x++; ?></td>
						<td><b><u>GRAND TOTAL</u></b></td>
						<td></td>
						<td><?php echo "<b><u>".number_format($drsum)."</u></b>"; ?></td>
						<td><?php echo "<b><u>".number_format($crsum)."</u></b>"; ?></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready( function(){
		$('#mytable').DataTable();
	});
	
</script>