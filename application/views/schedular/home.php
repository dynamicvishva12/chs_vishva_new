		<div class="row">
			<div class="col-12">

			 <div class="box">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			
				  		</div>
				  		<div class="col-2 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#schedularModal">Create Schedular</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Schedule For</th>
								<th>Schedule Topic</th>
								<th>Date.</th>
								<th>Status</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Sr. No</th>
								<th>Action</th>
								<th>Schedule For</th>
								<th>Schedule Topic</th>
								<th>Date.</th>
								<th>Status</th>
							</tr>
						</tfoot>
						<tbody>
							<?php 
								if(!empty($schedular_arr)):
									$sr_no=1;
									foreach($schedular_arr AS $e_sch):
							?>
							<tr>
								<td><?php echo $sr_no++; ?></td>
								<td>
									<?php if ($e_sch['schedular_close']!="closed"): ?>
										<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."schedular/delete_schedule/".$e_sch['sr_id']; ?>">
											<i class="fa fa-trash-o"></i>
										</button>
									<?php endif; ?>
								</td>
								<td><?php echo $e_sch['schedular_for']; ?></td>
								<td>
									<?php 
										if($e_sch['schedular_for']=='Event')
											echo $e_sch['ev_name'];

										if($e_sch['schedular_for']=='Notice')
											echo $e_sch['notice_header'];
									?>
								</td>
								<td><?php echo date("d-m-Y",strtotime($e_sch['schedular_date'])); ?></td>
								<td><?php echo $e_sch['schedular_close']; ?></td>
							</tr>
							<?php 
									endforeach;
								else:
							?>   
								<tr>
									<td colspan=6>
										<center>No records</center>
									</td>
								</tr>
							<?php endif; ?>  
						</tbody>
						
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->      
			</div>
			<!-- /.col -->
		  </div>


<div id="schedularModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."schedular/add_schedule"; ?>" method="POST">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Create Schedular</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Schedular For</label>
						<select class="form-control select2" name="schedular_for" style="width: 100%;">
						  	<option value="Maintenance bill">Maintenance bill</option>
							<option value="Notice">Notice</option>
							<option value="Event">Event</option>
						</select>
					</div>
					<div class="form-group" style="display:none">
						<label>Schedular Notice</label>
						<select class="form-control select2" name="schedulr_nid" style="width: 100%;">
						  	<?php 
								if(!empty($notices_arr)):
									foreach($notices_arr AS $e_notice):
							?>
							<option value="<?php echo $e_notice['no_id']; ?>">
								<?php echo $e_notice['notice_header']; ?>
							</option>
							<?php 
									endforeach;
								endif;
							?>
						</select>
					</div>
					<div class="form-group" style="display:none">
						<label>Schedular Event</label>
						<select class="form-control select2" name="schedulr_eid" style="width: 100%;">
						  	<?php 
								if(!empty($event_arr)):
									foreach($event_arr AS $e_ev):
							?>
							<option value="<?php echo $e_ev['ev_id']; ?>">
								<?php echo $e_ev['ev_name']; ?>
							</option>
							<?php 
									endforeach;
								endif;
							?>
						</select>
					  </div>
					<div class="form-group">
						<label class="col-md-12">Date</label>
						<div class="col-md-12">
							<input type="date" name="sdate" class="form-control" placeholder="Date" pattern="\d{1,2}/\d{1,2}/\d{4}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">From Time</label>
						<div class="col-md-12">
							<input type="time" name="f_time" class="form-control" placeholder="From Time" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" >Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>