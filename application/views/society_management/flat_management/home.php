<style type="text/css">

	.class55
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
    
</style>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div> 


	<div class="row">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
					<h4 class="title" style="color: #000;">Add Appartment</h4>
				</div>
				<div class="card-content table-responsive">
					<input class="input-build" type="text" id="building-name" style="width: 350px; border-radius: 10px; height: 50px; min-height: 10px; border-color: #000;">
					<button class="button-submit" type="button" id="building" style="height: 50px; width: 100px; border-radius: 10px;background: #212121; color: #fff; font-size: 18px; border-color: #000;">submit</button>
					<h2 class="text-center">Appartment</h2>
					<hr>
					<div id="build-show">
						<ol>
						<?php
							if(!empty($bldg_data)):
								foreach($bldg_data AS $e_bldg):
						?>
						<center> 
							<li class="show-theme">
								<?php echo str_replace("_", " ", $e_bldg['buildingname']); ?>

								<form method="post" action="<?php echo base_url()."society_management/flat_management/delete_building"; ?>">

									<input type="hidden" name="delete-build" value="<?php echo $e_bldg['b-id']; ?>">
									<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" name="deletebuild" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
										<i class="fa fa-minus-square"></i>
									</button>

								</form>

							</li>
						</center>
						<?php
								endforeach;
							endif;
						?>
						</ol>
					</div>
				</div>
			</div>
		</div>     
		<div class="col-md-6">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
					<h4 class="title" style="color: #000;">Add Wings</h4>
				</div>
				<div class="card-content table-responsive">

					<input class="input-build" type="text" id="wing-name" style="width: 350px; border-radius: 10px; height: 50px; min-height: 10px; border-color: #000;">
					<button class="button-submit" type="button" id="wing"  style="height: 50px; width: 100px; border-radius: 10px; background: #212121; color: #fff; font-size: 18px; border-color: #000;">submit</button>
					<h2 class="text-center">Wings</h2>
					<hr>
				<div>
				<div id="wing-show">
					<ol>
					<?php
						if(!empty($wing_data)):
							foreach($wing_data AS $e_wing):
					?>
					<center>
						<li class="show-theme">
							<?php echo $e_wing['wingname']; ?>
							<form method="post" action="<?php echo base_url()."society_management/flat_management/delete_wing"; ?>">
								<input type="hidden" name="delete-wing" value="<?php echo $row['wing-id']; ?>">

								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" name="deletewing" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
									<i class="fa fa-minus-square"></i>
								</button>

							</form>

						</li>
					</center>
					<?php
							endforeach;
						endif;
					?>
					</ol>
				</div>
			</div>
		</div>           
	</div>


<script>
	$("#building").click(function(){
	    
	    console.log('working');
	    var build = $('#building-name').val();
	    
	    $.post("wing-building",
	    {
	      building: build
	    },
	    function(data){
	        $('#build-show').html(data);
	        $('#building-name').val('');
	    });
	    
	});
</script>

<script>
	$("#wing").click(function(){
	    
	    console.log('working');
	    var wing = $('#wing-name').val();
	    
	    $.post("wing-building",
	    {
	      wing: wing
	    },
	    function(data){
	        $('#wing-show').html(data);
	        $('#wing-name').val('');
	    });
	    
	});
</script>