
		<!-- Main content -->
		<div class="row">
			<div class="col-12">
				<div class="box">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-10">
					  			<h4 class="box-title">Parking Management</h4>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#myModal">Allot Parking</button>
					  		</div>
						</div>
					</div>
					<div class="box-body">
					  	<div class="box">
							<div class="table-responsive">
					  			<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
					  				<!--display responsive nowrap    this for responsive datatables-->
									<thead>
										<tr>
											<th>Action</th>
											<th>Parking Spot Name</th>
											<th>Building Name</th>
											<th>Wing Name</th>
											<th>Alloted to</th>
											<th>Vehical Model No</th>                                        
										</tr>
									</thead>
									<tbody>
										<?php 
											if(!empty($pkgSpot_data)):
												foreach($pkgSpot_data AS $e_pkg):
										?>
										<tr>
											<td>
												<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."society_management/parking_allotment/delete_parking_allotment/".$e_pkg['p-id']."/".$e_pkg['parking_id']; ?>">
													<i class="fa fa-trash-o"></i>
												</button>
											</td>
											<td><?php echo $e_pkg['name']; ?></td>
											<td><?php echo $e_pkg['building_name']; ?></td>
											<td><?php echo $e_pkg['wing_name']; ?></td>
											<td>
												<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$e_pkg['user_name']; ?>" class="openPopup">
													<?php echo $e_pkg['user_name']; ?>
												</a>
											</td>
											<td><?php echo $e_pkg['v_numplate']; ?></td>
										</tr>
											<?php endforeach; ?>
										<?php else: ?>
				                            <tr>
				                                <td colspan=6><center>No Records</center></td>
				                            </tr>
				                        <?php endif; ?>   
									</tbody>				  
									<tfoot>
										<tr>
											<th>Action</th>
											<th>Parking Spot Name</th>
											<th>Building Name</th>
											<th>Wing Name</th>
											<th>Alloted to</th>
											<th>Vehical Model No</th>         
										</tr>
									</tfoot>
								</table>
							</div>              
						</div>
		  			</div>
				</div>
	  		</div>
	  	</div>


<!-- Popup Model Plase Here -->
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal"  method="post" action="<?php echo base_url()."society_management/parking_allotment/allot_parking"; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Allot Parking</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Parking Spots</label>
						<div class="col-md-12">
							<select class="form-control select2" name="uname" style="width: 100%;" required>
							<?php 
								if(!empty($prkng_spot_data)):
									foreach($prkng_spot_data AS $e_row):
							?>
								<option value="<?php echo $e_row['ps-id']; ?>"><?php echo $e_row['name']; ?></option>
							<?php 
									endforeach;
								endif;
							?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Member Selection</label>
						<select class="form-control select2" name="bname" style="width: 100%;" required>
							<option>Member Selection</option>
							<?php 
								if(!empty($vechical_data)):
									foreach($vechical_data AS $e_data):
							?>
								<option value="<?php echo $e_data['v_id']; ?>" style="text-transform: capitalize; "><?php echo  $e_data['s-r-fname']." ".$e_data['s-r-lname']." ".$e_data['s-r-username'].""; ?></option>
							<?php 
									endforeach;
								endif;
							?>
							</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" id="notice_add_btn">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>