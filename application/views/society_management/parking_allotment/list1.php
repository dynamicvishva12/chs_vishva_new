<style type="text/css">
	.class55
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
</style>

	<div id="id12" class="modal-rosh">
		<div class="modal-dialog-rosh">
			<div class="modal-content-rosh">
				<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
					<a href="#" class="closebtn">×</a>
					<center><h2 style="margin-top: 5px;">Allot Parking</h2></center>
				</header>
				<div class="container-fluid rosh" style="padding-top: 20px;">
					<form class="" method="post" action="<?php echo base_url()."society_management/parking_allotment/allot_parking"; ?>">
						<fieldset>
							<div class="form-group">
								<label class="col-md-12 control-label" for="selectbasic"></label>
								<div class="col-md-12">
									<label>Parking Spots</label>
									<select id="selectbasic" name="uname" class="form-control input-hello">
									<?php 
										if(!empty($prkng_spot_data)):
											foreach($prkng_spot_data AS $e_row):
									?>
										<option value="<?php echo $e_row['ps-id']; ?>"><?php echo $e_row['name']; ?></option>
									<?php 
											endforeach;
										endif;
									?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12 control-label" for="selectbasic"></label>
								<div class="col-md-12">
									<select id="selectbasic" name="bname" class="form-control input-hello">
									<option>Member Selection</option>
									<?php 
										if(!empty($vechical_data)):
											foreach($vechical_data AS $e_data):
									?>
										<option value="<?php echo $e_data['v_id']; ?>" style="text-transform: capitalize; "><?php echo  $e_data['s-r-fname']." ".$e_data['s-r-lname']."        ".$e_data['s-r-username'].""; ?></option>
									<?php 
											endforeach;
										endif;
									?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12 control-label" for="post"></label>
								<div class="col-md-12">
									<center> 
										<input type="submit" id="post" name="post-id" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" value="Allot Parking" style="background: #212121; color: #fff;">
									</center>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>


	<center>
		<div id="id30" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 700px;">
				<div class="modal-content-rosh">
					<header class="container-fluid rosh pari"> 
						<button style="height: 40px; width: 40px; float: right; font-size: 20px color: #000;" type="button" onclick="goBack()"  >&times;</button>        
						<center><h2 style="margin-top: 5px;">User Profile</h2></center>
					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px;"></div>
				</div>
			</div>
		</div>
	</center>

	<a href="#id12">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"  style="float: right; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
			Allot Parking
		</button>
	</a>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
					<h4 class="title" style="color: #000;">Parking Management</h4>
				</div>
				<div class="card-content table-responsive">
					<table id="table_id" class="display">
						<thead class="text-primary">
							<th>Parking Spot Name</th>
							<th>Building Name</th>
							<th>Wing Name</th>
							<th>Alloted to</th>
							<th>Vehical Model No</th>                                        
							<th>Action</th>
						</thead>
						<tbody>
						<?php 
							if(!empty($pkgSpot_data)):
								foreach($pkgSpot_data AS $e_pkg):
						?>
						<tr class="odd gradeX">
							<td><?php echo $e_pkg['name']; ?></td>
							<td><?php echo $e_pkg['building_name']; ?></td>
							<td><?php echo $e_pkg['wing_name']; ?></td>
							<td>
								<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/parking_allotment/user_profile/".$e_pkg['user_name']; ?>" class="openPopup">
									<?php echo $e_pkg['user_name']; ?>
								</a>
							</td>
							<td><?php echo $e_pkg['v_numplate']; ?></td>
							<td class="center">
								<center>
									<a href="<?php echo base_url()."society_management/parking_allotment/delete_parking_allotment/".$e_pkg['p-id']."/".$e_pkg['parking_id']; ?>">
										<i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
									</a>
								</center>
							</td>
						</tr>
							<?php endforeach; ?>
						<?php else: ?>
                            <tr>
                                <td colspan=6><center>No Records</center></td>
                            </tr>
                        <?php endif; ?>   
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>



	<script>

		$(document).ready(function(){
			$('.openPopup').on('click',function(){

				var dataURL = $(this).attr('data-href');
				
				console.log(dataURL);

				$('.roshan').load(dataURL,function(){

					$('#id30').modal({show:true});

				});

			});
		});
	</script>

	<script>
		function goBack() 
		{
			location.reload();
		}
	</script>