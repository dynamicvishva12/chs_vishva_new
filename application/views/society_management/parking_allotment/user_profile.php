    <div class="row">
        <center><h4><strong>MEMBER PROFILE</strong></h4></center>
    </div>
    <div class="row">
        <?php if(!empty($userdetails)): ?>
        <div class="col-lg-6">
            <img class="img-responsive" src="<?php echo $assets_url."IMAGES/".$userdetails['s-r-profile']; ?>" class="img-circle">
        </div>
        <div class="col-lg-6">
            <p><strong>NAME:</strong><?php echo $userdetails['s-r-fname'];?>&nbsp;<?php echo $userdetails['no-of-family-member']; ?>&nbsp;<?php echo $userdetails['s-r-lname']; ?></p>
            <p><strong>EMAIL-ID:</strong><?php echo $userdetails['s-r-email']; ?> </p>
            <p><strong>MOBILE NO:</strong><?php echo $userdetails['s-r-mobile']; ?></p>
            <p><strong>APPARTMENT:</strong><?php echo $userdetails['s-r-appartment']; ?></p>
            <p><strong>WING:</strong><?php echo $userdetails['s-r-wing']; ?></p>
            <p><strong>FLAT NO:</strong><?php echo $userdetails['s-r-flat']; ?> </p>
            <p><strong>DATE OF BIRTH:</strong><?php echo $userdetails['s-r-bod']; ?></p>
            <p><strong>MOTHER TONGUE:</strong><?php echo $userdetails['s-r-language']; ?> </p>
            <p><strong>BLOOD GROUP:</strong><?php echo $userdetails['s-r-blood']; ?></p>
        </div>
    <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    FAMILY MEMBERS
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Reletion</th>
                                <th>Blood Group</th>
                                <th>DOB</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(!empty($userfamily)):
                                foreach($userfamily AS $e_fmly):
                        ?>
                            <tr class="odd gradeX">
                                <?php
                                    if($e_fmly['relation'] === 'self')
                                    {
                                    }
                                    else
                                    {
                                ?>
                                <td><?php echo $e_fmly['name']; ?></td>
                                <td><?php echo $e_fmly['relation'];?></td>
                                <td><?php echo $e_fmly['blood_type']; ?></td>
                                <td><?php echo $e_fmly['dob']; ?></td>
                                <td><?php echo $e_fmly['contact']; ?></td>
                                <?php
                                    }
                                ?>
                            </tr>
                        <?php 
                                endforeach;
                            endif;
                        ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    WORKING STAFF
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Type</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Staff Documents</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(!empty($userstaff)):
                                foreach($userstaff AS $e_staff):
                        ?>
                            <tr class="odd gradeX">
                                <td><?php echo $e_staff['name']; ?></td>
                                <td><?php echo $e_staff['age']; ?></td>
                                <td><?php echo $e_staff['relation']; ?></td>
                                <td><?php echo $e_staff['address']; ?></td>
                                <td><?php echo $e_staff['contact']; ?></td>
                                <td><a href="<?php echo $assets_url."IMAGES/".$e_staff['st_doc']; ?>"><?php echo $e_staff['st_doc']; ?></a></td>
                            </tr>
                        <?php
                                endforeach;
                            endif;
                        ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    VEHICAL
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Vehical Type</th>
                                <th>Vehical Make Model</th>
                                <th>Vehical Registration Number</th>
                                <th>Vehical Spot Id</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!empty($uservehical)):
                                    foreach($uservehical AS $e_vch):
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $e_vch['v-type']; ?></td>
                                <td><?php echo $e_vch['make_model']; ?></td>
                                <td><?php echo $e_vch['v_numplate']; ?></td>
                                <td><?php echo $e_vch['v-park']; ?></td>
                            </tr>
                            <?php
                                    endforeach;
                                endif;
                            ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>