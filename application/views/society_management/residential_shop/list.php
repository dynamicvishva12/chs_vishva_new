		<div class="row">
			<div class="col-12">
				<div class="box">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-10">
					  			<h4 class="box-title">Residential Shops</h4>
					  		</div>
					  		<div class="col-2 text-right">
					  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#myModal">Add Residential Shop</button>
					  		</div>
						</div>
					</div>
					<div class="box-body">
					  	<div class="box">
							<div class="table-responsive">
					  			<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
					  				<!--display responsive nowrap    this for responsive datatables-->
									<thead>
										<tr>
											<th>Sr.No</th>
											<th>Action</th>
											<th>Owener Name</th>
											<th>Shop Name</th>
											<th>Wing</th>
											<th>Gala No</th>
											<th>Contact</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$sr_no=1;
											if(!empty($res_shop_data)):
												foreach($res_shop_data AS $e_data):
										?>
											<tr>
												<td><?php echo $sr_no++; ?></td>
												<td>
													<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."society_management/residential_shop/delete_residential_shop/".$e_data['id']; ?>"><i class="fa fa-trash-o"></i></button>
												</td>                      
												<td><?php echo $e_data['shop_name']; ?></td>
												<td><?php echo $e_data['owner_name']; ?></td>
												<td><?php echo $e_data['wing']; ?></td>
												<td><?php echo $e_data['gala']; ?></td>
												<td><?php echo $e_data['contact']; ?></td>
											</tr>
											<?php endforeach; ?>
										<?php else: ?>
				                            <tr>
				                                <td colspan=7><center>No Records</center></td>
				                            </tr>
				                        <?php endif; ?>   
									</tbody>				  
									<tfoot>
										<tr>
											<th>Sr.No</th>
											<th>Action</th>
											<th>Owener Name</th>
											<th>Shop Name</th>
											<th>Wing</th>
											<th>Gala No</th>
											<th>Contact</th>
										</tr>
									</tfoot>
								</table>
							</div>              
						</div>
		  			</div>
				</div>
	  		</div>
	  	</div>

<!-- Popup Model Plase Here -->
<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" action="<?php echo base_url()."society_management/residential_shop/add_residential_shop"; ?>"  method="POST" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Residential Shop</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="col-md-12">Owner Name</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="Owner-name" placeholder="Owner Name" value="<?php echo set_value('Owner-name'); ?>">
							<label class="text-danger"><?php echo form_error('Owner-name'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Owner Description</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="Owener-description" placeholder="Owner Description" value="<?php echo set_value('Owener-description'); ?>">
							<label class="text-danger"><?php echo form_error('Owener-description'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Wing</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="wing" placeholder="Wing" value="<?php echo set_value('wing'); ?>">
							<label class="text-danger"><?php echo form_error('wing'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Gala No</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="gala-No"  placeholder="Gala No" value="<?php echo set_value('gala-No'); ?>" >
							<label class="text-danger"><?php echo form_error('gala-No'); ?></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Mobile Number</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="Owner-mobile" placeholder="Enter Mobile Number"  value="<?php echo set_value('Owner-mobile'); ?>">
							<label class="text-danger"><?php echo form_error('Owner-mobile'); ?></label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#myModal').modal('show');

		<?php endif; ?>
	});

</script>