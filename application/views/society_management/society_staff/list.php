<div class="row">
	<div class="col-12">

	 <div class="box">
		<div class="box-header with-border">
	 		<div class="row">
				<div class="col-9">
		  			
		  		</div>
		  		<div class="col-3 text-right">
		  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#addStaff">Add Staff</button>
		  		</div>
			</div>
	 	</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
			  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
				<thead>
					<tr>
						<th>Sr. No</th>
						<th>Action</th>
						<th>Staff Type</th>
						<th>Staff Name</th>
						<th>Address</th>
						<th>Contact No.</th>
						<th>Timing</th>
					</tr>
				</thead>
				<tbody>
				<?php
					if(!empty($soc_stf_data)):
						$sr_no=1;
						foreach($soc_stf_data AS $e_data):
				?>
					<tr class="">
						<td><?php echo $sr_no++; ?></td>
						<td>
							<center>
								<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."society_management/society_staff/delete_society_staff/".$e_data['id']; ?>"><i class="fa fa-trash-o"></i></button>
							</center>
						</td>
						<td><?php echo $e_data['vendor_type']; ?></td>
						<td><?php echo $e_data['name']; ?></td>
						<td><?php echo $e_data['address']; ?></td>
						<td><?php echo $e_data['mobile_no']; ?></td> 
						<td><?php echo date("h:i:sa", strtotime($e_data['extra']))." - ".date("h:i:sa", strtotime($e_data['extra1'])); ?></td>
					</tr>
					<?php endforeach; ?>
				<?php else: ?>
                    <tr>
                        <td colspan=7><center>No Records</center></td>
                    </tr>
                <?php endif; ?>   
				</tbody>
				<tfoot>
					<tr>
						<th>Sr. No</th>
						<th>Action</th>
						<th>Staff Type</th>
						<th>Staff Name</th>
						<th>Address</th>
						<th>Contact No.</th>
						<th>Timing</th>
					</tr>
				</tfoot>
			</table>
			</div>
		</div>
		<!-- /.box-body -->
	  </div>
	  <!-- /.box -->      
	</div>
</div>


<div id="addStaff" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url()."society_management/society_staff/add_society_staff"; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Society Staffs</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<?php $society_type=set_value('select-type'); ?>
							<div class="form-group">
								<label class="col-md-12">Type</label>
								<select class="form-control select2" name="select-type" style="width: 100%;">
									<option value="Cleaning Staff" <?php if($society_type=="Cleaning Staff"): ?> selected <?php endif; ?>>Cleaning Staff</option>
									<option value="Pest Control" <?php if($society_type=="Pest Control"): ?> selected <?php endif; ?>>Pest Control</option>
									<option value="Appliance" <?php if($society_type=="Appliance"): ?> selected <?php endif; ?>>CCTV</option>
									<option value="CCTV" <?php if($society_type=="CCTV"): ?> selected <?php endif; ?>>CCTV</option>
									<option value="Gardener" <?php if($society_type=="Gardener"): ?> selected <?php endif; ?>>Gardener</option>
									<option value="Security" <?php if($society_type=="Security"): ?> selected <?php endif; ?>>Security</option>
									<option value="Others" <?php if($society_type=="Others"): ?> selected <?php endif; ?>>Others</option>
								</select>
								<label class="text-danger"><?php echo form_error('select-type'); ?></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Name</label>
								<div class="col-md-12">
									<input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo set_value('name'); ?>">
									<label class="text-danger"><?php echo form_error('name'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12">Address</label>
						<div class="col-md-12">
							<textarea type="text" name="b-address" class="form-control" placeholder="Address"><?php echo set_value('b-address'); ?></textarea>
							<label class="text-danger"><?php echo form_error('b-address'); ?></label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12">Mobile No.</label>
						<div class="col-md-12">
							<input type="text" name="b-mobile" class="form-control" value="<?php echo set_value('b-mobile'); ?>">
							<!-- <input type="text" name="b-mobile" class="form-control" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$"> -->
							<label class="text-danger"><?php echo form_error('b-mobile'); ?></label>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Time From</label>
								<div class="col-md-12">
									<input type="time" name="from" class="form-control" placeholder="Setect Time"  value="<?php echo set_value('from'); ?>">
									<label class="text-danger"><?php echo form_error('from'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Time To</label>
								<div class="col-md-12">
									<input type="time" name="to" class="form-control" placeholder="Setect Time"  value="<?php echo set_value('to'); ?>">
									<label class="text-danger"><?php echo form_error('to'); ?></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success float-right" >Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>

<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#addStaff').modal('show');

		<?php endif; ?>
	});

</script>
