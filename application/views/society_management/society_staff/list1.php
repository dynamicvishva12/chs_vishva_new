<style type="text/css">

	.class55
	{
		background: #212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}   

</style>



	<div id="id10" class="modal-rosh">
		<div class="modal-dialog-rosh">
			<div class="modal-content-rosh" style="width: 400px;">
				<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
					<a href="#" class="closebtn">×</a>
					<center><h2 style="margin-top: 5px;">Society Staff</h2></center>
				</header>
				<div class="container-fluid rosh" style="padding-top: 20px;">
					<center>
						<form method="post" action="<?php echo base_url()."society_management/society_staff/add_society_staff"; ?>">

							<select name="select-type" style="width: 100%; height: 40px;">
								<option value="Cleaning Staff">Cleaning Staff</option>
								<option value="Pest Control">Pest Control</option>
								<option value="Appliance">CCTV</option>
								<option value="Gardener">Gardener</option>
								<option value="Security">Security</option>
								<option value="Others">Others</option>
							</select><br>

							<input type="text" name="name" placeholder="Vendor Name" pattern="[a-zA-Z\s]+" style="width: 100%; height: 40px;" required><br>

							<input type="text" placeholder="Address" name="b-address" pattern="[a-zA-Z0-9,\s]+" style="width: 100%; height: 40px;" required><br>

							<input type="text" name="b-mobile" placeholder="Enter Mobile Number" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" title="Enter Valid mobile number" maxlength="10" style="width: 100%; height: 40px;" required><br>

							<label>TIME:</label><BR>

							<label>From:</label>
							<input style="width: 40%; height: 40px; text-align: center;" type="time" name="from" required><br>

							<label>To:</label>
							<input type="time" name="to" style="width: 40%; height: 40px; text-align: center;" required></label><br><br>

							<center>
								<input type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" name="submit-b" value="SUBMIT" style="background:#212121; color: #fff;">
							</center>
						</form>
					</center>
				</div>
			</div>
		</div>
	</div>

	<center>
		<div id="id30" class="modal-rosh">
			<div class="modal-dialog-rosh" style="width: 500px;">
				<div class="modal-content-rosh" style="margin-top: 100px;">
					<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px; padding-top: 0px; padding-bottom: 10px;"> 

						<button style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>

						<center>
							<h2 style="margin-top: 0px; font-size: 20px;">Confirm Delete</h2>
						</center>

					</header>
					<div class="container-fluid rosh roshan" style="padding-top: 20px; padding-bottom: 20px;"></div>
				</div>
			</div>
		</div>
	</center>
	
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style=" background: linear-gradient(60deg, #26c6da, #00acc1); color: #000;">
					<h4 class="title" style="color: #000;">Society Staff</h4>
				</div>
				<div class="card-content table-responsive">
					<table id='table_id'  class="table table-bordered">
						<thead>
							<tr>
								<th>Sr.No</th>
								<th>Staff Type</th>
								<th>Staff Name</th>
								<th>Address</th>
								<th>Contact No</th>
								<th>Timming</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							if(!empty($soc_stf_data)):
								$sr_no=1;
								foreach($soc_stf_data AS $e_data):
						?>
							<tr class="">
								<td><?php echo $sr_no++; ?></td>
								<td><?php echo $e_data['vendor_type']; ?></td>
								<td><?php echo $e_data['name']; ?></td>
								<td><?php echo $e_data['address']; ?></td>
								<td><?php echo $e_data['mobile_no']; ?></td> 
								<td><?php echo date("h:i:sa", strtotime($e_data['extra']))." - ".date("h:i:sa", strtotime($e_data['extra1'])); ?></td>
								<td>
									<center>
										<a href="javascript:void(0);" data-href="<?php echo base_url()."society_management/society_staff/delete_society_staff/".$e_data['id']; ?>" class="openPopup">
											<i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
										</a>
									</center>
								</td>
							</tr>
							<?php endforeach; ?>
						<?php else: ?>
                            <tr>
                                <td colspan=7><center>No Records</center></td>
                            </tr>
                        <?php endif; ?>   
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<a href="#id10">
		<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"   style="position: fixed;top: 85%; right: 5%; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
      		<i class="material-icons">add</i>
    	</button>
	</a>

	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center>
						<h4 class="modal-title">Delete Confirmation</h4>
					</center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>

	<script>

		$(document).ready(function(){
			$('.openPopup').on('click',function(){

				var dataURL = $(this).attr('data-href');
				console.log(dataURL);

				$('.roshan').load(dataURL,function(){

					$('#id30').modal({show:true});

				});
			});
		});

	</script>

	<script>
		function goBack() 
		{
			location.reload();
		}
	</script>