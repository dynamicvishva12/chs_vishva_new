<div class="row">
	<div class="col-12 col-lg-7 col-xl-8">
		
	  <div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
		  <li><a href="#family" class="active" data-toggle="tab">Family Members</a></li>
		  <li><a href="#working" data-toggle="tab">Working Staff</a></li>
		  <li><a href="#vehicle" data-toggle="tab">Vehicle Details</a></li>
		</ul>

		<div class="tab-content">

			<div class="active tab-pane" id="family">

				<div class="box-body">
					<div class="row">
						<?php
                            if(!empty($userfamily)):
                                foreach($userfamily AS $e_fmly):
                        ?>
	                        <?php
	                            if($e_fmly['relation'] === 'self')
	                            {
	                            }
	                            else
	                            {
	                        ?>
							 <div class="col-12 col-lg-4">
								<div class="box ribbon-box">
								  <div class="box-header no-border p-0">				
									<a href="#">
									  <img class="img-fluid" src="<?php echo $assets_url; ?>tmp_images/avatar/375x200/1.jpg" alt="">
									</a>
								  </div>
								  <div class="box-body">
									  	<div class="text-dark">
											<p>Name :<span class="pl-10"><?php echo $e_fmly['name']; ?></span> </p>
											<p>Relation :<span class="pl-10"><?php echo $e_fmly['relation']; ?></span></p>
											<p>Blood Group :<span class="pl-10"><?php echo $e_fmly['blood_type']; ?></span></p>
											<p>Date of Birth :<span class="pl-10"><?php echo date('d-M-Y', strtotime($e_fmly['dob'])); ?></span></p>
											<p>Contact :<span class="pl-10"><?php echo $e_fmly['contact']; ?></span></p>
										</div>
								  </div>
								</div>
							  </div>
							<?php
	                            }
	                        ?>
	                    <?php 
                                endforeach;
                            endif;
                        ?>
					</div>
				</div>
		 	</div>

		  	<div class="tab-pane" id="working">			
				<div class="box-body">
					<div class="row">
						<?php
                            if(!empty($userstaff)):
                                foreach($userstaff AS $e_staff):
                        ?>
						<div class="col-12 col-lg-4">
							<div class="box ribbon-box">
							  	<div class="box-body">
								  	<div class="text-dark">
										<p>Name :<span class="pl-10"><?php echo $e_staff['name']; ?></span> </p>
										<p>Age :<span class="pl-10"><?php echo $e_staff['age']; ?></span></p>
										<p>Type :<span class="pl-10"><?php echo $e_staff['relation']; ?></span></p>
										<p>Address :<span class="pl-10"><?php echo $e_staff['address']; ?></span></p>
										<p>Contact :<span class="pl-10"><?php echo $e_staff['contact']; ?></span></p>
										<p>Documents :<span class="pl-10"><a href="<?php echo $assets_url."IMAGESDRIVE/".$e_staff['st_doc']; ?>"><?php echo $e_staff['st_doc']; ?></a></span></p>
									</div>
							  	</div>
							</div>
						</div>
						<?php
                                endforeach;
                            endif;
                        ?>
					</div>
				</div>
		 	</div>

		  	<div class="tab-pane" id="vehicle">		
	  			<div class="box-body">
					<div class="row">
						<?php
                            if(!empty($uservehical)):
                                foreach($uservehical AS $e_vch):
                        ?>
						 <div class="col-12 col-lg-4">
							<div class="box ribbon-box">
							  <div class="box-body">
								  	<div class="text-dark">
										<p>Vehicle Type :<span class="pl-10"><?php echo $e_vch['v-type']; ?></span> </p>
										<p>Vehicle Make Model :<span class="pl-10"><?php echo $e_vch['make_model']; ?></span></p>
										<p>Registration No :<span class="pl-10"><?php echo $e_vch['v_numplate']; ?></span></p>
										<p>Spot Id :<span class="pl-10"><?php echo $e_vch['v-park']; ?></span></p>
									</div>
							  </div>
							</div>
						  </div>
						<?php
                                endforeach;
                            endif;
                        ?>
					</div>
				</div>
		  	</div>

		</div>
		<!-- /.tab-content -->
	  </div>
	  <!-- /.nav-tabs-custom -->
	</div>
	<!-- /.col -->		
	<?php if(!empty($userdetails)): ?>
	  <div class="col-12 col-lg-5 col-xl-4">
		 <div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header bg-black" style="background: url('<?php echo $assets_url.'IMAGESDRIVE/'.$userdetails['s-r-profile']; ?>') center center;">
			  
			</div>
			<div class="widget-user-image">
			  <img class="rounded-circle w-100" src="<?php echo $assets_url.'IMAGESDRIVE/'.$userdetails['s-r-profile']; ?>" alt="User Avatar">
			</div>
			<div class="box-footer">
			  <h5 class="text-center pt-10">
			  	<?php echo ucfirst($userdetails['s-r-fname']." ".$userdetails['no-of-family-member']." ".$userdetails['s-r-lname']); ?>
			  </h5>
			</div>
		  </div>
		  <div class="box">
			<div class="box-body box-profile">            
			  <div class="row">
				<div class="col-12">
					<div class="text-dark">
						<p>Email :<span class="pl-10"><?php echo $userdetails['s-r-email']; ?></span> </p>
						<p>Mobile No :<span class="pl-10"><?php echo $userdetails['s-r-mobile']; ?></span></p>
						<p>Appartment No :<span class="pl-10"><?php echo $userdetails['s-r-appartment']; ?></span></p>
						<p>Wing :<span class="pl-10"><?php echo $userdetails['s-r-wing']; ?></span></p>
						<p>Flat No :<span class="pl-10"><?php echo $userdetails['s-r-flat']; ?></span></p>
						<p>Date of Birth :<span class="pl-10"><?php echo $userdetails['s-r-bod']; ?></span></p>
						<p>Mother Tongue :<span class="pl-10"><?php echo $userdetails['s-r-language']; ?></span></p>
						<p>Blood Group :<span class="pl-10"><?php echo $userdetails['s-r-blood']; ?></span></p>
					</div>
				</div>
				
				
			  </div>
			</div>
			<!-- /.box-body -->
		  </div>

	  </div>
	 <?php endif; ?>

  </div>
  <!-- /.row -->