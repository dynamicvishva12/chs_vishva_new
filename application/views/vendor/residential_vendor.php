		  <div class="row">

			<div class="col-12">
			  <div class="box box-default">
				<!-- /.box-header -->
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h3 class="box-title">Residential Vendors</h3>
				  		</div>
				  		<div class="col-2 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#resVendor">Add Vendor</button>
				  		</div>
					</div>
			 	</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Vendor Type</th>
								<th>Name</th>
								<th>Address</th>
								<th>Contact No.</th>
								<th>Additional Services</th>
								<th>Timing</th>
								<th>Visit Fees</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$sr=1;
                                if(!empty($res_vnd_arr)):
                                    foreach($res_vnd_arr as $key => $vendor):
                            ?>
							<tr>
                                <td><?php echo $sr; ?></td>
                                <td>
                                    <center>
                                        <button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."vendor/delete_residential_vendor/".$vendor['venId']; ?>">
                                        	<i class="fa fa-trash-o"></i>
                                        </button>
                                    </center>
                                </td>
                                <td><?php echo $vendor['venType']; ?></td>
                                <td><?php echo $vendor['venOwner']; ?></td>
                                <td><?php echo $vendor['venAdd']; ?></td>
                                <td><?php echo $vendor['venContact']; ?></td>
                                <td><?php echo $vendor['venService']; ?></td>
                                <td><?php echo $vendor['venTime']; ?></td>
                                <td><?php echo $vendor['venCharges']; ?></td>
                            </tr>
							<?php 
									$sr++;
                                    endforeach;
                                else:
                            ?>   
                                <tr>
                                    <td colspan=8>
                                        <center>No records</center>
                                    </td>
                                </tr>
                            <?php endif; ?>  
						</tbody>
						<tfoot>
							<tr>
								<th>Sr. No.</th>
								<th>Action</th>
								<th>Vendor Type</th>
								<th>Name</th>
								<th>Address</th>
								<th>Contact No.</th>
								<th>Additional Services</th>
								<th>Timing</th>
								<th>Visit Fees</th>
							</tr>
						</tfoot>
					</table>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->

<div id="resVendor" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url()."vendor/add_residential_vendor"; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Residential Vendor</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Type</label>
								<div class="col-md-12">
									<select class="form-control select2" name="ventype" style="width: 100%;">
									 	<?php if(!empty($res_vnd_ty_arr)): ?>
	                                        <?php foreach ($res_vnd_ty_arr as $key => $vendortp): ?>
	                                            <option value="<?php echo $vendortp['vendorId']; ?>" <?php if(set_value('ventype')==$vendortp['vendorId']): ?> selected <?php endif; ?>><?php echo $vendortp['vendorType']; ?></option>
	                                        <?php endforeach; ?>
	                                    <?php endif; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Name</label>
								<div class="col-md-12">
									<input type="text" name="venname" class="form-control" placeholder="Name" value="<?php echo set_value('venname'); ?>">
									<label class="text-danger"><?php echo form_error('venname'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-md-12">Address</label>
						<div class="col-md-12">
							<textarea type="text" class="form-control" name="venAddress" placeholder="Address"><?php echo set_value('venAddress'); ?></textarea>
							<label class="text-danger"><?php echo form_error('venAddress'); ?></label>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Time From</label>
								<div class="col-md-12">
									<input type="time" name="venfrom" class="form-control" placeholder="Setect Time" value="<?php echo set_value('venfrom'); ?>">
									<label class="text-danger"><?php echo form_error('venfrom'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Time To</label>
								<div class="col-md-12">
									<input type="time" name="vento" class="form-control" placeholder="Setect Time" value="<?php echo set_value('vento'); ?>">
									<label class="text-danger"><?php echo form_error('vento'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Mobile No.</label>
								<div class="col-md-12">
									<input type="text" name="venmobile" class="form-control" value="<?php echo set_value('venmobile'); ?>">
									<label class="text-danger"><?php echo form_error('venmobile'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-md-12">Charges</label>
								<div class="col-md-12">
									<input type="text" name="vencharge" class="form-control" value="<?php echo set_value('vencharge'); ?>">
									<label class="text-danger"><?php echo form_error('vencharge'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-md-12">Services</label>
						<div class="col-md-12">
							<textarea type="text" name="venservice" class="form-control"><?php echo set_value('venservice'); ?></textarea>
							<label class="text-danger"><?php echo form_error('venservice'); ?></label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                    <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>


<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#resVendor').modal('show');

		<?php endif; ?>
	});

</script>