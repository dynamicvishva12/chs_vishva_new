<style>
    .class10
    {
        background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    .class444
    {
        background:#212121; color: #fff;
        box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
    }
    label
    {
        color: #000;
    }
    .mdl-button:hover
    {
        background-color: #212121;
        color: #fff;
    }

</style>
    <div class="row" style="padding-left: 10px;">
        <div class="col-md-6">
            <a href="<?php echo base_url()."vendor/society_vendor"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class333" style="width: 100%;">
                    Society Vendors
                </button>
            </a>
        </div>
        <div class="col-md-6">
            <a href="<?php echo base_url()."vendor/residential_vendor"; ?>">
                <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class444" style="width: 100%;">
                    Residential Vendors
                </button>
            </a>
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
                    <h4 class="title">Residential Vendors</h4>
                </div>
                <div class="card-content table-responsive">
                    <table id="table_id" class="display">
                        <thead>
                            <tr>
                                <th>Vendor Type</th>
                                <th>Vendor Name</th>
                                <th>Address</th>
                                <th>Contact No</th>
                                <th>Additional Services</th>
                                <th>Timming</th>
                                <th>Visit Fees</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if(!empty($res_vnd_arr)):
                                    foreach($res_vnd_arr as $key => $vendor):
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $vendor['venType']; ?></td>
                                <td><?php echo $vendor['venOwner']; ?></td>
                                <td><?php echo $vendor['venAdd']; ?></td>
                                <td><?php echo $vendor['venContact']; ?></td>
                                <td><?php echo $vendor['venService']; ?></td>
                                <td><?php echo $vendor['venTime']; ?></td>
                                <td><?php echo $vendor['venCharges']; ?></td>
                                <td>
                                    <center>
                                        <a href="<?php echo base_url()."vendor/delete_residential_vendor/".$vendor['venId']; ?>" class="confirmation">
                                            <i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
                                        </a>
                                    </center>
                                </td>
                            </tr>
                            <?php 
                                    endforeach;
                                else:
                            ?>   
                                <tr>
                                    <td colspan=8>
                                        <center>No records</center>
                                    </td>
                                </tr>
                            <?php endif; ?>      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <a href="#id08">
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"  style="position: fixed;top: 85%; right: 5%;z-index: 99999;  background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
            <i class="material-icons">add</i>
        </button>
    </a>

    

    <div id="id08" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="width: 1000px;">
                <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
                    <a href="<?php echo htmlspecialchars('javascript:history.back()'); ?>" class="closebtn">×</a>
                    <center><h2 style="margin-top: 5px;"> Residential Vendors</h2></center>
                </header>
                <div class="container-fluid rosh" style="padding-top: 20px;">
                    <form method="post" action="<?php echo base_url()."vendor/add_residential_vendor"; ?>">
                        <div class="row">
                            <div class="col-md-4">
                                <select name="ventype" style="width: 100%; height:40px;">
                                    <?php if(!empty($res_vnd_ty_arr)): ?>
                                        <?php foreach ($res_vnd_ty_arr as $key => $vendortp): ?>
                                            <option value="<?php echo $vendortp['vendorId']; ?>"><?php echo $vendortp['vendorType']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input type="text" style="width:100%; height:40px;" name="venname" placeholder="Vendor Name" pattern="[a-zA-Z\s]+" style="width: 100%; height:40px;" required>
                            </div>
                            <div class="col-md-4">
                                <input type="text" style="width:100%; height:40px;" placeholder="Address" name="venAddress" style="width: 100%; " required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <label>Time From : </label>
                                    <select id="selectbasic" name="venfrom" style="width: 100%; height:40px;">
                                    <?php 
                                        $val=0;
                                        while($val<= 12):
                                    ?>
                                    <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                    <?php
                                        $val=$val+1;
                                        endwhile;
                                    ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>AM/PM : </label>
                                    <select id="selectbasic" name="ampm" style="width: 100%; height:40px;">
                                        <option value="AM">AM</option>
                                        <option value="PM">PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <label>Time To : </label>
                                    <select id="selectbasic" name="vento" style="width: 100%; height:40px;">
                                        <?php 
                                            $val=0;
                                            while($val<= 12):
                                        ?>
                                        <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                        <?php
                                            $val=$val+1;
                                            endwhile;
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>AM/PM : </label>
                                    <select id="selectbasic" name="ampm1" style="width: 100%; height:40px;">
                                        <option value="AM">AM</option>
                                        <option value="PM">PM</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Contact Number : </label>
                                <input type="text" style="width:100%; height:40px;" name="venmobile" placeholder="Enter Mobile Number" maxlength="10" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" title="Enter Valid mobile number" style="width: 100%; height:40px;" required>
                            </div>
                            <div class="col-md-4">
                                <label>Service : </label>
                                <input type="text" style="width:100%; height:40px;" name="venservice" placeholder="Sevice eg: clean and dry cloath, iron cloath,etc" pattern="[a-zA-Z0-9,\s]+" title="You can use only Comma To saperate services" style="width: 100%; height:40px;" required>
                            </div>
                            <div class="col-md-4">
                                <label>service charge</label>
                                <input type="text" style="width:100%; height:40px;" name="vencharge" placeholder="Enter Visiting Charge" pattern="[0-9]+" title="You can insert only Number" style="width: 100%; height:40px;" required>
                            </div>
                        </div><br>
                        <center>
                            <input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
                            <input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
                            <input type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="width:25%; height:40px; background:#212121; color: #fff;" name="addRvendor" value="SUBMIT">
                        </center>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>  

    <center>
        <div id="id30" class="modal-rosh">
            <div class="modal-dialog-rosh" style="width: 500px;">
                <div class="modal-content-rosh" style="margin-top: 100px;">
                    <header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1); height: 30px; padding-top: 0px; padding-bottom: 10px;"> 
                        <button style="height: 40px; border: transparent; position: absolute; right: 0px; box-shadow: 0 4px 4px 0 rgba(0,0,0,.14), 0 5px 3px -4px rgba(0,0,0,.2), 0 3px 7px 0 rgba(0,0,0,.12); width: 30px; height: 30px;  float: right; font-size: 30px color: #fff; background:#212121; border: transparent; " type="button" onclick="goBack()"  >&times;</button>        
                        <center>
                            <h2 style="margin-top: 0px; font-size: 20px;">Confirm Delete</h2>
                        </center>
                    </header>
                    <div class="container-fluid rosh roshan" style="padding-top: 20px; padding-bottom: 20px;"></div>
                </div>
            </div>
        </div>
    </center>


    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="<?php echo htmlspecialchars('javascript:history.back()'); ?>" class="closebtn">×</a>
                    <center><h4 class="modal-title">Delete Confirmation</h4></center>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>

<script>
      
    $(document).ready(function(){
       $('.openPopup1').on('click',function(){
           var dataURL = $(this).attr('data-href');

           $('.roshan').load(dataURL,function(){
               $('#id30').modal({show:true});
           });
       });
    });

</script>

<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>

<script>
    function goBack() 
    {
        location.reload();
    }
</script>