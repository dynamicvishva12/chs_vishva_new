		<div class="row">
			<div class="col-12">
			  <div class="box box-default">
				<div class="box-header with-border">
			 		<div class="row">
						<div class="col-10">
				  			<h3 class="box-title">Society Vendors</h3>
				  		</div>
				  		<div class="col-2 text-right">
				  			<button type="button" class="waves-effect waves-light btn btn-info mb-5" data-toggle="modal" data-target="#socVendor">Add Vendor</button>
				  		</div>
					</div>
			 	</div>
				<div class="box-body">
					<div class="box-body">
						<div class="table-responsive">
						   <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100 display responsive nowrap">
							<thead>
								<tr>
									<th>Sr. No.</th>
									<th>Action</th>
									<th>Vendor Type</th>
									<th>Name</th>
									<th>Contract Start Date</th>
									<th>Contract End Date</th>
									<th>Address</th>
									<th>Contact No.</th>
									<th>Charges</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$sr=1;
									if(!empty($soc_vnd_arr)):
										foreach($soc_vnd_arr as $key => $vendor):
								?>
								<tr>
									<td><?php echo $sr; ?></td>
									<td>
										<center>
											<button class="waves-effect waves-circle btn btn-social-icon btn-circle btn-danger remove_it" data-toggle="tooltip" data-original-title="Delete" data-link="<?php echo base_url()."vendor/delete_society_vendor/".$vendor['vendorId']; ?>">
												<i class="fa fa-trash-o"></i>
											</button>
										</center>
									</td>
									<td><?php echo $vendor['vendor_type']; ?></td>
									<td><?php echo $vendor['vendoName']; ?></td>
									<td><?php echo date("d-m-Y",strtotime($vendor['contract_start'])); ?></td>
									<td><?php echo date("d-m-Y",strtotime($vendor['contract_end'])); ?></td>
									<td><?php echo $vendor['vendorAddress']; ?></td>
									<td><?php echo $vendor['mobile_no']; ?></td>
									<td><?php echo $vendor['charges']; ?></td>
								</tr>
								<?php 
										$sr++;
										endforeach;
									else:
								?>   
									<tr>
										<td colspan=10>
											<center>No records</center>
										</td>
									</tr>
								<?php endif; ?>  
							</tbody>
							<tfoot>
								<tr>
									<th>Sr. No.</th>
									<th>Action</th>
									<th>Vendor Type</th>
									<th>Name</th>
									<th>Contract Start Date</th>
									<th>Contract End Date</th>
									<th>Address</th>
									<th>Contact No.</th>
									<th>Charges</th>
								</tr>
							</tfoot>
						</table>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->

		  </div>
		  <!-- /.row -->
		  <!-- END tabs -->

<div id="socVendor" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal" method="post" action="<?php echo base_url()."vendor/add_society_vendor"; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add Socity Vendor</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php $vendor_type=set_value('vendor_type'); ?>
								<label class="col-md-12">Type</label>
								<select class="form-control select2" name="vendor_type" style="width: 100%;">
								  	<option value="Cleaning Staff" <?php if($vendor_type=="Cleaning Staff"): ?> selected <?php endif; ?>>Cleaning Staff</option>
									<option value="Pest Control" <?php if($vendor_type=="Pest Control"): ?> selected <?php endif; ?>>Pest Control</option>
									<option value="CCTV" <?php if($vendor_type=="CCTV"): ?> selected <?php endif; ?>>CCTV</option>
									<option value="Gardener" <?php if($vendor_type=="Gardener"): ?> selected <?php endif; ?>>Gardener</option>
									<option value="Security" <?php if($vendor_type=="Security"): ?> selected <?php endif; ?>>Security</option>
									<option value="Others" <?php if($vendor_type=="Others"): ?> selected <?php endif; ?>>Others</option>
								</select>
								<label class="text-danger"><?php echo form_error('vendor_type'); ?></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Name</label>
								<div class="col-md-12">
									<input type="text" name="vendor_name" class="form-control" placeholder="Name"  value="<?php echo set_value('vendor_name'); ?>">
									<label class="text-danger"><?php echo form_error('vendor_name'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12">Address</label>
						<div class="col-md-12">
							<textarea type="text" name="vendor_address" class="form-control" placeholder="Address"><?php echo set_value('vendor_address'); ?></textarea>
							<label class="text-danger"><?php echo form_error('vendor_address'); ?></label>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Start Date</label>
								<div class="col-md-12">
									<input type="date" name="vendor_Sdate" class="form-control" placeholder="Setect Date"  value="<?php echo set_value('vendor_Sdate'); ?>">
									<label class="text-danger"><?php echo form_error('vendor_Sdate'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">End Date</label>
								<div class="col-md-12">
									<input type="date" name="vendor_Edate" class="form-control" placeholder="Setect Date"  value="<?php echo set_value('vendor_Edate'); ?>">
									<label class="text-danger"><?php echo form_error('vendor_Edate'); ?></label>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Mobile No.</label>
								<div class="col-md-12">
									<input type="text" name="vendor_mobile" class="form-control"  value="<?php echo set_value('vendor_mobile'); ?>">
									<label class="text-danger"><?php echo form_error('vendor_mobile'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-12">Charges</label>
								<div class="col-md-12">
									<input type="text" name="charges" class="form-control" value="<?php echo set_value('charges'); ?>">
									<label class="text-danger"><?php echo form_error('charges'); ?></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
					<input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
					<button type="submit" class="btn btn-success float-right">Add</button>
					<button type="button" class="btn btn-default float-right" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>


<script type="text/javascript">

	$(document).ready(function(){
		<?php if(!empty(validation_errors())): ?>

			$('#socVendor').modal('show');

		<?php endif; ?>
	});

</script>