<style>
	.class10
	{
		background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
	.class333
	{
		background:#212121; color: #fff;
		box-shadow: 0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2);
	}
	label
	{
		color: #000;
	}
	.mdl-button:hover
	{
		background-color: #212121;
		color: #fff;
	}
</style>


	<div class="row" style="padding-left: 10px;">
		<div class="col-md-6">
			<a href="<?php echo base_url()."vendor/society_vendor"; ?>"><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect class333" style="width: 100%;">
			Society Vendors
			</button></a>
		</div>
		<div class="col-md-6">
			<a href="<?php echo base_url()."vendor/residential_vendor"; ?>"><button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%;">
			Residential Vendors
			</button></a>
		</div>
	</div><br>



	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header" style="background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
					<h4 class="title">Society Vendors</h4>
				</div>
				<div class="card-content table-responsive">
					<table id="table_id" class="display">
						<thead>
							<tr>
								<th>Vendor Type</th>
								<th>Vendor Name</th>
								<th>Contract Start Date</th>
								<th>Contract End Date</th>
								<th>Address</th>
								<th>Contact No</th>
								<th>Charges</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							if(!empty($soc_vnd_arr)):
								foreach($soc_vnd_arr as $key => $vendor):
						?>
						<tr class="odd gradeX">
							<td><?php echo $vendor['vendor_type']; ?></td>
							<td><?php echo $vendor['vendoName']; ?></td>
							<td><?php echo date("d-m-Y",strtotime($vendor['contract_start'])); ?></td>
							<td><?php echo date("d-m-Y",strtotime($vendor['contract_end'])); ?></td>
							<td><?php echo $vendor['vendorAddress']; ?></td>
							<td><?php echo $vendor['mobile_no']; ?></td>
							<td><?php echo $vendor['charges']; ?></td>
							<td>
								<center>
									<a href="<?php echo base_url()."vendor/delete_society_vendor/".$vendor['vendorId']; ?>" class="confirmation">
										<i class="material-icons" style="color: #212121; font-size: 40px;">delete</i>
									</a>
								</center>
							</td>
						</tr>
						<?php 
								endforeach;
							else:
						?>   
							<tr>
								<td colspan=8>
									<center>No records</center>
								</td>
							</tr>
						<?php endif; ?>  
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<a href="#id07">
		<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" style="position: fixed;top: 85%; right: 5%; z-index: 100; background: linear-gradient(60deg, #26c6da, #00acc1); color: #fff;">
				<i class="material-icons">add</i>
		</button>
	</a>

	<div class="modal fade" id="myModal1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a href="<?php echo htmlspecialchars('javascript:history.back()'); ?>" class="closebtn">×</a>
					<center><h4 class="modal-title">Delete Confirmation</h4></center>
				</div>
				<div class="modal-body"></div>
			</div>
		</div>
	</div>

    <div id="id01" class="modal-rosh">
        <div class="modal-dialog-rosh">
            <div class="modal-content-rosh" style="min-width: 230px; max-width: 315px; border: transparent;">
                <a href="#" class="closebtn btn" style="color: #000;">×</a>
                <figure class="snip1344">
                    <img src="<?php echo $sess_user_profile; ?>" alt="profile-sample1" class="background"/><img src="<?php echo $assets_url."IMAGES/".$sess_user_profile; ?>" alt="profile-sample1" class="profile"/>
                    <figcaption>
                        <h3>
                            <?php echo $society_userdata['s-r-fname']." ".$society_userdata['s-r-lname']; ?>
                            <span>Secretary</span>
                            <span><?php echo $society_userdata['s-r-email']; ?></span>
                            <span><?php echo $society_userdata['s-r-mobile']; ?></span>
                        </h3>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>  



	<div id="id07" class="modal-rosh">
		<div class="modal-dialog-rosh">
			<div class="modal-content-rosh">
				<header class="container-fluid rosh pari" style="background: linear-gradient(60deg, #26c6da, #00acc1);"> 
					<a href="<?php echo htmlspecialchars('javascript:history.back()'); ?>" class="closebtn">×</a>
					<center><h2 style="margin-top: 5px;">Society Vendors</h2></center>
				</header>
				<div class="container-fluid rosh" style="padding-top: 20px;">
					<form method="post" action="<?php echo base_url()."vendor/add_society_vendor"; ?>">
						<div class="row">
							<div class="col-md-4">
								<select name="vendor_type" style="width: 100%; height:40px;">
									<option value="Cleaning Staff">Cleaning Staff</option>
									<option value="Pest Control">Pest Control</option>
									<option value="CCTV">CCTV</option>
									<option value="Gardener">Gardener</option>
									<option value="Security">Security</option>
									<option value="Others">Others</option>
								</select>
							</div>
							<div class="col-md-4">
								<label>Name:</label>
								<input type="text" name="vendor_name" placeholder="Vendor Name" pattern="[a-zA-Z0-9\s]+" style="width: 100%; height:40px;" required>
							</div>
							<div class="col-md-4">
								<label>Address:</label>
								<input type="text" placeholder="Address" name="vendor_address" pattern="[a-zA-Z0-9,-\s]+" style="width: 100%; height:40px;" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label>Start Date:</label>
								<input type="Date" name="vendor_Sdate" placeholder="Start Date" max="2047-01-01" min="<?php echo date("Y-m-d");?>" style="width: 100%; height:40px;"  required>
							</div>
							<div class="col-md-4">
								<label>End Date:</label>
								<input type="Date" name="vendor_Edate" placeholder="End Date" max="2047-01-01" min="<?php echo date("Y-m-d");?>" style="width: 100%; height:40px;" required>
							</div>
							<div class="col-md-4">
								<label>Mobile No:</label>
								<input type="text" name="vendor_mobile" placeholder="Enter Mobile Number" pattern="^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$" title="Enter Valid mobile number" maxlength="10" style="width: 100%; height:40px;" required>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label>Charges</label>
								<input type="text" placeholder="vendor_charges" name="charges" pattern="[0-9.]+" style="width: 100%; height:40px;" required>
							</div>
						</div>          
						<center>
							<input type="hidden" name="society_key" value="<?php echo $society_key; ?>" required>
							<input type="hidden" name="user_name" value="<?php echo $user_name; ?>" required>
							<input type="submit" class="btn " style="text-align: center; width: 30%;  background:#212121; color: #fff;" name="vendorAdd" value="SUBMIT">
						</center>
					</form> 
				</div>
			</div>
		</div>
	</div>


<script type="text/javascript">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>

<script>
      
	$(document).ready(function(){
	   $('.openPopup1').on('click',function(){
	       var dataURL = $(this).attr('data-href');

	       $('.roshan').load(dataURL,function(){
	           $('#id30').modal({show:true});
	       });
	   });
	});

</script>

<script>
    function goBack() 
    {
        location.reload();
    }
</script>