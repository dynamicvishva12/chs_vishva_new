-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2020 at 10:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dynam135_society_parameter`
--

-- --------------------------------------------------------

--
-- Table structure for table `society_list`
--

CREATE TABLE `society_list` (
  `sr_id` int(11) NOT NULL,
  `society_name` varchar(100) NOT NULL,
  `society_key` text NOT NULL,
  `society_dbname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `society_list`
--

INSERT INTO `society_list` (`sr_id`, `society_name`, `society_key`, `society_dbname`) VALUES
(1, 'Safavi Manzil', '1e4cc533342d2842e0d2abe8edf076cc', 'dynam135_gatekeeper'),
(2, 'CHS Vishva', '1e4cc533342d2842e0d2abe8edf076cc1', 'dynam135_safavi');

-- --------------------------------------------------------

--
-- Table structure for table `society_table`
--

CREATE TABLE `society_table` (
  `sr_id` int(20) NOT NULL,
  `society_Id` varchar(100) NOT NULL,
  `society_name` varchar(150) NOT NULL,
  `society_database` varchar(100) NOT NULL,
  `society_key` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `society_table`
--

INSERT INTO `society_table` (`sr_id`, `society_Id`, `society_name`, `society_database`, `society_key`, `status`) VALUES
(1, 'society_01', 'CHS Visshva', 'dynam135_society', '1e4cc533342d2842e0d2abe8edf076cc', 'Active'),
(2, 'society_02', 'Testing society', 'dynam135_testing_society', '577d367795c202a0f6e3bbd0680e6fc9', 'Active'),
(3, 'society_03', 'Panchvati society', 'dynam135_panchvati', 'E62BC7EC57532EB89AD335E812131027', 'Active'),
(4, 'society_04', 'Shree Shanti Niketen', 'dynam135_shriShantiNik', '165a9dc33a486c6ef7cf6d4babdfe733', 'Active'),
(5, 'society_5', 'Ronak Heights', 'dynam135_ronakheights', '56c34508d7086c67ded0e73e8a9101c6', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `society_list`
--
ALTER TABLE `society_list`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `society_table`
--
ALTER TABLE `society_table`
  ADD PRIMARY KEY (`sr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `society_list`
--
ALTER TABLE `society_list`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `society_table`
--
ALTER TABLE `society_table`
  MODIFY `sr_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
