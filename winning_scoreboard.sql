-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2020 at 05:49 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `winning_scoreboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_head_tbl`
--

CREATE TABLE `account_head_tbl` (
  `accountHeadId` int(11) NOT NULL,
  `accountHeadName` varchar(100) NOT NULL,
  `headType` tinyint(4) NOT NULL COMMENT '1 - Normal, 2 - Bank, 3 - Cash',
  `fk_ffId` int(11) NOT NULL,
  `fkUserId` int(11) NOT NULL,
  `fkCompanyId` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `addedBy` int(11) NOT NULL COMMENT '1 - Admin or Other Number is UserId',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_head_tbl`
--

INSERT INTO `account_head_tbl` (`accountHeadId`, `accountHeadName`, `headType`, `fk_ffId`, `fkUserId`, `fkCompanyId`, `status`, `addedBy`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, 'Sales', 1, 1, 1, 0, 1, 1, '2020-03-30 13:02:38', 2, '0000-00-00 00:00:00', 0),
(2, 'Service Charge Received', 1, 1, 1, 0, 1, 1, '2020-03-30 13:02:48', 2, '0000-00-00 00:00:00', 0),
(3, 'Interest Earn', 1, 2, 1, 0, 1, 1, '2020-03-30 13:03:27', 2, '0000-00-00 00:00:00', 0),
(4, 'Purchase', 1, 3, 1, 0, 1, 1, '2020-03-30 13:03:56', 2, '0000-00-00 00:00:00', 0),
(5, 'Travelling', 1, 4, 1, 0, 1, 1, '2020-03-30 13:04:18', 2, '0000-00-00 00:00:00', 0),
(6, 'Partners Current Account', 1, 5, 1, 0, 1, 1, '2020-03-30 13:04:43', 2, '0000-00-00 00:00:00', 0),
(7, 'Surplus From PnL', 1, 6, 1, 0, 1, 1, '2020-03-30 13:05:06', 2, '0000-00-00 00:00:00', 0),
(8, 'Secured Loan', 1, 7, 1, 0, 1, 1, '2020-03-30 13:05:34', 2, '0000-00-00 00:00:00', 0),
(9, 'Creditors', 1, 8, 1, 0, 1, 1, '2020-03-30 13:05:59', 2, '0000-00-00 00:00:00', 0),
(10, 'Bank FD', 1, 9, 1, 0, 1, 1, '2020-03-30 13:06:24', 2, '0000-00-00 00:00:00', 0),
(11, 'Office Deposit', 1, 10, 1, 0, 1, 1, '2020-03-30 13:06:48', 2, '0000-00-00 00:00:00', 0),
(12, 'Debitor', 1, 11, 1, 0, 1, 1, '2020-03-30 13:07:20', 2, '0000-00-00 00:00:00', 0),
(13, 'Bank', 2, 12, 1, 0, 1, 1, '2020-03-30 13:07:45', 2, '0000-00-00 00:00:00', 0),
(14, 'Cash', 3, 13, 1, 0, 1, 1, '2020-03-30 13:08:06', 2, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `budget_tbl`
--

CREATE TABLE `budget_tbl` (
  `budgetId` int(11) NOT NULL,
  `budgetType` tinyint(4) NOT NULL COMMENT '1 - Income, 2 - Expenditure',
  `jan` int(11) NOT NULL,
  `feb` int(11) NOT NULL,
  `mar` int(11) NOT NULL,
  `apr` int(11) NOT NULL,
  `may` int(11) NOT NULL,
  `jun` int(11) NOT NULL,
  `jul` int(11) NOT NULL,
  `aug` int(11) NOT NULL,
  `sep` int(11) NOT NULL,
  `oct` int(11) NOT NULL,
  `nov` int(11) NOT NULL,
  `december` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `amount` float NOT NULL,
  `fkAccountHeadId` int(11) NOT NULL,
  `fk_ffId` int(11) NOT NULL,
  `fkLedgerId` int(11) NOT NULL,
  `fkUserId` int(11) NOT NULL,
  `fkCompanyId` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_tbl`
--

CREATE TABLE `client_tbl` (
  `clientId` int(11) NOT NULL,
  `clientName` varchar(50) NOT NULL,
  `dbName` int(50) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_tbl`
--

CREATE TABLE `company_tbl` (
  `companyId` int(11) NOT NULL,
  `companyName` varchar(50) NOT NULL,
  `companyRegistrationNo` varchar(50) NOT NULL,
  `companyPanNo` varchar(50) NOT NULL,
  `fkUserId` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_tbl`
--

INSERT INTO `company_tbl` (`companyId`, `companyName`, `companyRegistrationNo`, `companyPanNo`, `fkUserId`, `status`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, 'Infosys', 'REG4534534', 'PAN654634', 2, 1, '2020-03-29 20:16:58', 1, '0000-00-00 00:00:00', 0),
(2, 'TCS', 'REG6546547', 'PAN654657', 2, 1, '2020-03-30 15:56:07', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fund_flow_tbl`
--

CREATE TABLE `fund_flow_tbl` (
  `ffId` int(11) NOT NULL,
  `ffName` varchar(50) NOT NULL,
  `shortName` varchar(20) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1 - In, 2 - Out',
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund_flow_tbl`
--

INSERT INTO `fund_flow_tbl` (`ffId`, `ffName`, `shortName`, `type`, `status`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, 'Direct Income', 'DI', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(2, 'Indirect Income', 'II', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(3, 'Direct Expense', 'DE', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(4, 'Indirect Expense', 'IE', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(5, 'Capital', 'CAP', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(6, 'Reserve & Surplus', 'RS', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(7, 'Loans', 'LN', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(8, 'Current Liability', 'CL', 1, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(9, 'Investments', 'INV', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(10, 'Fix Asset', 'FA', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(11, 'Current Asset', 'CA', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(12, 'Bank', 'Bank', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0),
(13, 'Cash', 'Cash', 2, 1, '2020-03-12 00:00:00', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ledger_tbl`
--

CREATE TABLE `ledger_tbl` (
  `ledgerId` int(11) NOT NULL,
  `ledgerName` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `fkAccountId` int(11) NOT NULL,
  `gstNo` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `otherDetails` varchar(200) NOT NULL,
  `openingBal` int(11) NOT NULL,
  `addedBy` int(11) NOT NULL COMMENT '1 - Admin or Other Number is UserId 	',
  `fkUserId` int(11) NOT NULL,
  `fkCompanyId` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ledger_tbl`
--

INSERT INTO `ledger_tbl` (`ledgerId`, `ledgerName`, `type`, `fkAccountId`, `gstNo`, `address`, `otherDetails`, `openingBal`, `addedBy`, `fkUserId`, `fkCompanyId`, `status`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, 'Sales', 'default', 1, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:02:38', 2, '0000-00-00 00:00:00', 0),
(2, 'Service Charge Recei', 'default', 2, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:02:48', 2, '0000-00-00 00:00:00', 0),
(3, 'Interest Earn', 'default', 3, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:03:27', 2, '0000-00-00 00:00:00', 0),
(4, 'Purchase', 'default', 4, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:03:56', 2, '0000-00-00 00:00:00', 0),
(5, 'Travelling', 'default', 5, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:04:18', 2, '0000-00-00 00:00:00', 0),
(6, 'Partners Current Acc', 'default', 6, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:04:43', 2, '0000-00-00 00:00:00', 0),
(7, 'Surplus From PnL', 'default', 7, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:05:06', 2, '0000-00-00 00:00:00', 0),
(8, 'Secured Loan', 'default', 8, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:05:34', 2, '0000-00-00 00:00:00', 0),
(9, 'Creditors', 'default', 9, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:05:59', 2, '0000-00-00 00:00:00', 0),
(10, 'Bank FD', 'default', 10, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:06:24', 2, '0000-00-00 00:00:00', 0),
(11, 'Office Deposit', 'default', 11, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:06:48', 2, '0000-00-00 00:00:00', 0),
(12, 'Debitor', 'default', 12, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:07:20', 2, '0000-00-00 00:00:00', 0),
(13, 'Bank', 'default', 13, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:07:45', 2, '0000-00-00 00:00:00', 0),
(14, 'Cash', 'default', 14, '', '', '', 0, 1, 1, 0, 1, '2020-03-30 13:08:06', 2, '0000-00-00 00:00:00', 0),
(15, 'Interest Earn 2', '', 3, '', '', '', 0, 2, 2, 1, 1, '2020-03-31 17:28:53', 2, '0000-00-00 00:00:00', 0),
(16, 'Purchase 1', '', 4, '', '', '', 0, 2, 2, 1, 1, '2020-03-31 17:31:10', 2, '0000-00-00 00:00:00', 0),
(17, 'Creditors 1', '', 9, '', '', '', 0, 2, 2, 1, 1, '2020-03-31 17:34:28', 2, '0000-00-00 00:00:00', 0),
(18, 'Bank FD 1', '', 10, '', '', '', 0, 2, 2, 1, 1, '2020-03-31 19:49:47', 2, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_tbl`
--

CREATE TABLE `log_tbl` (
  `logId` int(11) NOT NULL,
  `tableName` varchar(25) NOT NULL,
  `logData` varchar(200) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdDatetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_tbl`
--

INSERT INTO `log_tbl` (`logId`, `tableName`, `logData`, `userId`, `createdDatetime`) VALUES
(1, 'user_tbl', 'User Logged Out by System IP: ::1', 2, '2020-03-29 19:36:30'),
(2, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-29 20:17:03'),
(3, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 20:59:26'),
(4, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:00:07'),
(5, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:00:59'),
(6, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:01:17'),
(7, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:04:54'),
(8, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:04:56'),
(9, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:05:54'),
(10, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-29 21:08:30'),
(11, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-30 12:53:10'),
(12, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 12:54:40'),
(13, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 12:55:17'),
(14, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:01:56'),
(15, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:02:38'),
(16, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:02:38'),
(17, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:02:38'),
(18, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:02:38'),
(19, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:02:48'),
(20, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:02:49'),
(21, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:02:49'),
(22, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:02:49'),
(23, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:03:27'),
(24, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:03:27'),
(25, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:03:27'),
(26, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:03:28'),
(27, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:03:56'),
(28, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:03:56'),
(29, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:03:56'),
(30, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:03:56'),
(31, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:04:18'),
(32, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:04:18'),
(33, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:04:18'),
(34, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:04:19'),
(35, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:04:43'),
(36, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:04:43'),
(37, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:04:43'),
(38, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:04:43'),
(39, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:05:06'),
(40, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:05:06'),
(41, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:05:06'),
(42, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:05:06'),
(43, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:05:34'),
(44, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:05:34'),
(45, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:05:34'),
(46, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:05:35'),
(47, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:05:59'),
(48, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:06:00'),
(49, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:06:00'),
(50, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:06:00'),
(51, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:06:24'),
(52, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:06:24'),
(53, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:06:24'),
(54, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:06:24'),
(55, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:06:48'),
(56, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:06:48'),
(57, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:06:48'),
(58, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:06:49'),
(59, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:07:20'),
(60, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:07:20'),
(61, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:07:20'),
(62, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:07:21'),
(63, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:07:45'),
(64, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:07:45'),
(65, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:07:45'),
(66, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:07:45'),
(67, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:08:06'),
(68, 'account_head_tbl', 'New Account Head created', 2, '2020-03-30 13:08:06'),
(69, 'ledger_tbl', 'New default Ledger created', 2, '2020-03-30 13:08:06'),
(70, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:08:06'),
(71, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:09:24'),
(72, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-30 13:23:44'),
(73, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:27:03'),
(74, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:30:44'),
(75, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:30:56'),
(76, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:32:57'),
(77, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:33:51'),
(78, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:33:59'),
(79, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:34:09'),
(80, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:34:35'),
(81, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:34:39'),
(82, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:36:27'),
(83, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 13:36:27'),
(84, 'user_tbl', 'User Logged Out by System IP: ::1', 2, '2020-03-30 13:42:51'),
(85, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-30 13:42:53'),
(86, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 13:54:18'),
(87, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 14:07:19'),
(88, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 14:07:46'),
(89, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 14:08:29'),
(90, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:30:49'),
(91, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:37:48'),
(92, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:41:56'),
(93, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:42:48'),
(94, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:43:19'),
(95, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:44:03'),
(96, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:44:57'),
(97, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:45:44'),
(98, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:46:14'),
(99, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:46:50'),
(100, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 15:47:21'),
(101, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:48:19'),
(102, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:49:26'),
(103, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:50:02'),
(104, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:50:36'),
(105, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:51:31'),
(106, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:52:04'),
(107, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:52:29'),
(108, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:53:01'),
(109, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:53:26'),
(110, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:53:52'),
(111, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-30 15:54:24'),
(112, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 15:54:55'),
(113, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 1, '2020-03-30 15:54:57'),
(114, 'user_tbl', 'User Logged Out by System IP: ::1', 2, '2020-03-30 15:56:38'),
(115, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-30 15:56:42'),
(116, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 15:58:04'),
(117, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-30 15:59:23'),
(118, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 16:00:10'),
(119, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-30 16:00:52'),
(120, 'user_tbl', 'User Logged In by System IP: 127.0.0.1', 2, '2020-03-31 12:11:29'),
(121, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-31 12:37:24'),
(122, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-31 17:19:31'),
(123, 'ledger_tbl', 'New Ledger created', 2, '2020-03-31 17:28:53'),
(124, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-31 17:28:59'),
(125, 'ledger_tbl', 'New Ledger created', 2, '2020-03-31 17:31:10'),
(126, 'ledger_tbl', 'New Ledger created', 2, '2020-03-31 17:34:28'),
(127, 'transaction_tbl', 'New Applicable transaction created', 2, '2020-03-31 17:34:35'),
(128, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-31 17:34:42'),
(129, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-31 17:35:25'),
(130, 'account_head_tbl|ledger_t', 'Fetched Account Head & Ledger data', 2, '2020-03-31 17:35:50'),
(131, 'ledger_tbl', 'New Ledger created', 2, '2020-03-31 19:49:47'),
(132, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-31 19:50:30'),
(133, 'transaction_tbl', 'New Bank transaction created', 2, '2020-03-31 19:51:54'),
(134, 'user_tbl', 'User Logged In by System IP: ::1', 2, '2020-03-31 20:11:00'),
(135, 'transaction_tbl', 'New Cash transaction created', 2, '2020-03-31 21:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_tbl`
--

CREATE TABLE `transaction_tbl` (
  `transactionId` int(11) NOT NULL,
  `transId` varchar(55) NOT NULL,
  `transType` tinyint(4) NOT NULL COMMENT '1 - Income, 2 - Expenditure',
  `transSection` tinyint(4) NOT NULL COMMENT '1 - Applicable-IN, 2 - Applicable-OUT, 3 - Bank-IN, 4 - Bank-OUT, 5 - Cash-IN, 6 - Cash-OUT, 7 - BankCashContra',
  `refNo` varchar(200) NOT NULL,
  `invoiceNo` varchar(200) NOT NULL,
  `f_ffId` int(11) NOT NULL COMMENT 'From Fund Flow',
  `fromAccHeadId` int(11) NOT NULL,
  `fromLedgerId` int(11) NOT NULL,
  `t_ffId` int(11) NOT NULL COMMENT 'To Fund Flow',
  `toAccHeadId` int(11) NOT NULL,
  `toLedgerId` int(11) NOT NULL,
  `amount` float NOT NULL,
  `isGst` int(11) NOT NULL COMMENT '1-Yes, 2-No',
  `cgst` float NOT NULL,
  `sgst` float NOT NULL,
  `igst` float NOT NULL,
  `cgst_amt` float NOT NULL,
  `sgst_amt` float NOT NULL,
  `igst_amt` float NOT NULL,
  `isTds` int(11) NOT NULL COMMENT '1-Yes, 2-No',
  `tds` float NOT NULL,
  `tds_amt` float NOT NULL,
  `isRounded` int(11) NOT NULL COMMENT '1-Yes, 2-No',
  `roundedAmt` float NOT NULL,
  `total` float NOT NULL,
  `date` date NOT NULL,
  `year` int(11) NOT NULL,
  `comment` text NOT NULL,
  `narration` text NOT NULL,
  `fkUserId` int(11) NOT NULL,
  `fkCompanyId` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_tbl`
--

INSERT INTO `transaction_tbl` (`transactionId`, `transId`, `transType`, `transSection`, `refNo`, `invoiceNo`, `f_ffId`, `fromAccHeadId`, `fromLedgerId`, `t_ffId`, `toAccHeadId`, `toLedgerId`, `amount`, `isGst`, `cgst`, `sgst`, `igst`, `cgst_amt`, `sgst_amt`, `igst_amt`, `isTds`, `tds`, `tds_amt`, `isRounded`, `roundedAmt`, `total`, `date`, `year`, `comment`, `narration`, `fkUserId`, `fkCompanyId`, `status`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, '2020_00001', 1, 1, '', '', 11, 12, 12, 2, 3, 3, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 13:54:17', 2, '0000-00-00 00:00:00', 0),
(2, '2020_00002', 1, 1, '', '', 11, 12, 12, 1, 1, 1, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 14:07:19', 2, '0000-00-00 00:00:00', 0),
(3, '2020_00003', 2, 2, '', '', 8, 9, 9, 3, 4, 4, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 14:07:46', 2, '0000-00-00 00:00:00', 0),
(4, '2020_00004', 2, 2, '', '', 8, 9, 9, 4, 5, 5, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 14:08:29', 2, '0000-00-00 00:00:00', 0),
(5, '2020_00005', 1, 3, '', '', 1, 1, 1, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:30:49', 2, '0000-00-00 00:00:00', 0),
(6, '2020_00006', 1, 3, '', '', 2, 3, 3, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:37:48', 2, '0000-00-00 00:00:00', 0),
(7, '2020_00007', 1, 3, '', '', 7, 8, 8, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:41:56', 2, '0000-00-00 00:00:00', 0),
(8, '2020_00008', 1, 3, '', '', 5, 6, 6, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:42:47', 2, '0000-00-00 00:00:00', 0),
(9, '2020_00009', 1, 3, '', '', 9, 10, 10, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:43:19', 2, '0000-00-00 00:00:00', 0),
(10, '2020_00010', 1, 3, '', '', 10, 11, 11, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:44:03', 2, '0000-00-00 00:00:00', 0),
(11, '2020_00011', 2, 4, '', '', 3, 4, 4, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:44:57', 2, '0000-00-00 00:00:00', 0),
(12, '2020_00012', 2, 4, '', '', 4, 5, 5, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:45:44', 2, '0000-00-00 00:00:00', 0),
(13, '2020_00013', 2, 4, '', '', 9, 10, 10, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:46:14', 2, '0000-00-00 00:00:00', 0),
(14, '2020_00014', 2, 4, '', '', 10, 11, 11, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:46:50', 2, '0000-00-00 00:00:00', 0),
(15, '2020_00015', 2, 4, '', '', 7, 8, 8, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:47:20', 2, '0000-00-00 00:00:00', 0),
(16, '2020_00016', 1, 5, '', '', 1, 1, 1, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:48:18', 2, '0000-00-00 00:00:00', 0),
(17, '2020_00017', 1, 5, '', '', 2, 3, 3, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:49:26', 2, '0000-00-00 00:00:00', 0),
(18, '2020_00018', 1, 5, '', '', 7, 8, 8, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:50:02', 2, '0000-00-00 00:00:00', 0),
(19, '2020_00019', 1, 5, '', '', 5, 6, 6, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:50:36', 2, '0000-00-00 00:00:00', 0),
(20, '2020_00020', 1, 5, '', '', 9, 10, 10, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:51:31', 2, '0000-00-00 00:00:00', 0),
(21, '2020_00021', 1, 5, '', '', 10, 11, 11, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:52:04', 2, '0000-00-00 00:00:00', 0),
(22, '2020_00022', 2, 6, '', '', 3, 4, 4, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:52:29', 2, '0000-00-00 00:00:00', 0),
(23, '2020_00023', 2, 6, '', '', 4, 5, 5, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:53:01', 2, '0000-00-00 00:00:00', 0),
(24, '2020_00024', 2, 6, '', '', 9, 10, 10, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:53:26', 2, '0000-00-00 00:00:00', 0),
(25, '2020_00025', 2, 6, '', '', 10, 11, 11, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:53:52', 2, '0000-00-00 00:00:00', 0),
(26, '2020_00026', 2, 6, '', '', 7, 8, 8, 13, 14, 14, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 1, 1, '2020-03-30 15:54:23', 2, '0000-00-00 00:00:00', 0),
(27, '2020_00001', 1, 1, '', '', 11, 12, 12, 1, 1, 1, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 2, 1, '2020-03-30 15:58:04', 2, '0000-00-00 00:00:00', 0),
(28, '2020_00002', 1, 1, '', '', 11, 12, 12, 1, 2, 2, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 2, 1, '2020-03-30 15:59:23', 2, '0000-00-00 00:00:00', 0),
(29, '2020_00003', 1, 3, '', '', 9, 10, 10, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 2, 1, '2020-03-30 16:00:10', 2, '0000-00-00 00:00:00', 0),
(30, '2020_00004', 1, 3, '', '', 5, 6, 6, 12, 13, 13, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-30', 2020, '', '', 2, 2, 1, '2020-03-30 16:00:51', 2, '0000-00-00 00:00:00', 0),
(31, '2020_00027', 1, 1, '', '', 11, 12, 12, 2, 3, 15, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-31', 2020, '', '', 2, 1, 1, '2020-03-31 17:28:59', 2, '0000-00-00 00:00:00', 0),
(32, '2020_00028', 2, 2, '', '', 8, 9, 17, 3, 4, 4, 1000, 2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 0, 1000, '2020-03-31', 2020, '', '', 2, 1, 1, '2020-03-31 17:34:35', 2, '0000-00-00 00:00:00', 0),
(33, '2020_00029', 1, 3, '', '', 9, 10, 18, 12, 13, 13, 1000, 1, 1, 1, 1, 10, 10, 10, 1, 1, 10, 2, 0, 1020, '2020-03-31', 2020, '', '', 2, 1, 1, '2020-03-31 19:50:30', 2, '0000-00-00 00:00:00', 0),
(34, '2020_00030', 2, 4, '', '', 9, 10, 18, 12, 13, 13, 1000, 1, 1, 1, 1, 10, 10, 10, 1, 1, 10, 2, 0, 1020, '2020-03-31', 2020, '', '', 2, 1, 1, '2020-03-31 19:51:54', 2, '0000-00-00 00:00:00', 0),
(35, '2020_00031', 2, 6, '', '', 9, 10, 18, 13, 14, 14, 1000, 1, 1.76, 1, 1, 17.6, 10, 10, 1, 1, 10, 2, 0, 1027.6, '2020-03-31', 2020, '', '', 2, 1, 1, '2020-03-31 21:08:22', 2, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `userId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobileNo` varchar(15) NOT NULL,
  `emailId` varchar(50) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `userType` varchar(10) NOT NULL,
  `addressLine1` varchar(200) NOT NULL,
  `addressLine2` varchar(200) NOT NULL,
  `pinCode` int(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `fkCompanyId` int(11) NOT NULL,
  `ipAddress` varchar(30) NOT NULL,
  `loginStatus` int(11) NOT NULL COMMENT '1 - Logged In, 2 - Log Out',
  `status` int(11) NOT NULL COMMENT '1 - Active, 2 - Inactive',
  `createdDatetime` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedDatetime` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`userId`, `name`, `mobileNo`, `emailId`, `userName`, `password`, `userType`, `addressLine1`, `addressLine2`, `pinCode`, `city`, `state`, `fkCompanyId`, `ipAddress`, `loginStatus`, `status`, `createdDatetime`, `createdBy`, `updatedDatetime`, `updatedBy`) VALUES
(1, 'Admin', '9876543210', 'admin@example.com', '9876543210', '25f9e794323b453885f5181f1b624d0b', 'admin', '', '', 0, '', '', 0, '::1', 1, 1, '2020-03-26 14:08:20', 3, '2020-03-30 15:55:38', 0),
(2, 'Chirag', '8888888888', 'chirag@example.com', '8888888888', '25f9e794323b453885f5181f1b624d0b', 'user', 'Andheri', 'Mumbai', 400068, 'Mumbai', 'Maharashtra', 0, '::1', 1, 1, '2020-03-29 19:56:56', 1, '2020-03-31 20:11:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_head_tbl`
--
ALTER TABLE `account_head_tbl`
  ADD PRIMARY KEY (`accountHeadId`);

--
-- Indexes for table `budget_tbl`
--
ALTER TABLE `budget_tbl`
  ADD PRIMARY KEY (`budgetId`);

--
-- Indexes for table `client_tbl`
--
ALTER TABLE `client_tbl`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `company_tbl`
--
ALTER TABLE `company_tbl`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `fund_flow_tbl`
--
ALTER TABLE `fund_flow_tbl`
  ADD PRIMARY KEY (`ffId`);

--
-- Indexes for table `ledger_tbl`
--
ALTER TABLE `ledger_tbl`
  ADD PRIMARY KEY (`ledgerId`);

--
-- Indexes for table `log_tbl`
--
ALTER TABLE `log_tbl`
  ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `transaction_tbl`
--
ALTER TABLE `transaction_tbl`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_head_tbl`
--
ALTER TABLE `account_head_tbl`
  MODIFY `accountHeadId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `budget_tbl`
--
ALTER TABLE `budget_tbl`
  MODIFY `budgetId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client_tbl`
--
ALTER TABLE `client_tbl`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_tbl`
--
ALTER TABLE `company_tbl`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fund_flow_tbl`
--
ALTER TABLE `fund_flow_tbl`
  MODIFY `ffId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ledger_tbl`
--
ALTER TABLE `ledger_tbl`
  MODIFY `ledgerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `log_tbl`
--
ALTER TABLE `log_tbl`
  MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `transaction_tbl`
--
ALTER TABLE `transaction_tbl`
  MODIFY `transactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
